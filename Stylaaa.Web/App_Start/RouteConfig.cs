﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Services;
using Stylaaa.Web.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Stylaaa.Web
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
              name: "StylistCheckoutPayment",
              url: "Stylist/{username}/{UpgradeAccountId}/UpdateToPremium/{stripetype}/{InvoiceNo}",
              defaults: new { controller = "Stripe", action = "CheckoutPayment" }
          );

            routes.MapRoute(
                name: "StylistUpgardeAccount",
                url: "Stylist/{username}/{month}/UpdateToPremium",
                defaults: new { controller = "Payment", action = "BuySubscription" },
                constraints: new { username = new UserNameConstraint(), month = new MonthConstraint() }
            );

            routes.MapRoute(
                name: "StylistChangePassword",
                url: "Stylist/{username}/ChangePassword",
                defaults: new { controller = "Account", action = "UserEditPassword" },
                constraints: new { username = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "UserChangePassword",
                url: "User/{username}/ChangePassword",
                defaults: new { controller = "Account", action = "UserEditPassword" },
                constraints: new { username = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "Stylist",
                url: "Stylist/{username}/{action}",
                //defaults: new { controller = "Stylist", action = typeof(string) },
                defaults: new { controller = "Stylist", action = "About" },
                constraints: new { username = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "User",
                url: "User/{username}/{action}",
                //defaults: new { controller = "User", action = typeof(string) },
                defaults: new { controller = "User", action = "About" },
                constraints: new { username = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "Chat",
                url: "Chat/{username}/{action}",
                defaults: new { controller = "Chat", action = typeof(string) },
                constraints: new { username = new UserNameConstraint() }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Account", action = "Index", id = UrlParameter.Optional }
            );
        }

        public class UserNameConstraint : IRouteConstraint
        {
            public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
                object username;
                if (values.TryGetValue(parameterName, out username) && username != null)
                {
                    UserService objData = new UserService();
                    bool validRoute = objData.CheckValidUserName(username.ToString());
                    return validRoute;
                }
                return false;
            }
        }

        public class MonthConstraint : IRouteConstraint
        {
            public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
            {
                object month;
                if (values.TryGetValue(parameterName, out month) && month != null)
                {
                    GeneralServices _service = new GeneralServices();
                    bool validRoute = _service.CheckValidMonthForUpgradeAccount(Convert.ToInt32(month.ToString()));
                    return validRoute;
                }
                return false;
            }
        }
    }
}
