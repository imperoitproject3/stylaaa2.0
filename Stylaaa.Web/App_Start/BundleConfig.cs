﻿using System.Web;
using System.Web.Optimization;

namespace Stylaaa.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/css/select2.css",
                      "~/Content/css/jquery.fancybox.css",
                      "~/Content/Plugins/UploadImage/cropper.css",
                      "~/Content/css/custom.css",
                      "~/Content/css/style.css",                      
                      //"~/Content/Plugins/tagmanager/tagmanager.css",
                      "~/Content/Plugins/Toaster/toastr.css",
                      "~/Content/Plugins/sweetalert/sweet-alert.css",
                      "~/Content/Plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"));

            bundles.Add(new ScriptBundle("~/js/contentjs").Include(
              "~/Content/Plugins/Toaster/toastr.js",
               "~/Content/js/select2.full.min.js",
               //"~/Content/js/jquery.selectit.js",
               //"~/Content/Plugins/tagmanager/tagmanager.js",
               "~/Content/Plugins/bootstrap-tagsinput/bootstrap-tagsinput.js",
               "~/Content/js/jquery.fancybox.min.js",
               "~/Content/Plugins/UploadImage/cropper.js",
               "~/Content/Plugins/sweetalert/sweet-alert.js",
               "~/Content/js/pullToRefresh.js",
               "~/Content/js/RequestAnimationFrame.js",
               "~/Content/js/jquery.AshAlom.gaugeMeter-2.0.0.min.js",
               "~/Content/js/custom.js"));

            bundles.Add(new ScriptBundle("~/js/registerjs").Include(
             "~/Content/Plugins/Toaster/toastr.js",
              "~/Content/js/select2.register.js",
              "~/Content/js/jquery.selectit.js",
               //"~/Content/Plugins/bootstrap-tagsinput/bootstrap-tagsinput.js",
               "~/Content/js/jquery.fancybox.min.js",
               //"~/Content/Plugins/UploadImage/cropper.js",
               "~/Content/Plugins/sweetalert/sweet-alert.js",
               "~/Content/js/pullToRefresh.js",
               //"~/Content/js/RequestAnimationFrame.js",
               //"~/Content/js/jquery.AshAlom.gaugeMeter-2.0.0.min.js",
               "~/Content/js/custom.js"));

            bundles.Add(new ScriptBundle("~/scripts/custom").Include(
              "~/Scripts/custom/common.js"));

            bundles.Add(new ScriptBundle("~/scripts/addditpost").Include(
              "~/Scripts/custom/addditpost.js"));

            bundles.Add(new ScriptBundle("~/scripts/postactivity").Include(
             "~/Scripts/custom/postactivity.js"));

            bundles.Add(new ScriptBundle("~/scripts/addditjob").Include(
             "~/Scripts/custom/jobAddEdit.js"));

            bundles.Add(new ScriptBundle("~/scripts/jobactivity").Include(
              "~/Scripts/custom/JobActivity.js"));                    

            bundles.Add(new ScriptBundle("~/scripts/profile").Include(
              "~/Scripts/custom/profile.js"));

            bundles.Add(new ScriptBundle("~/scripts/setupCall").Include(
              "~/Scripts/custom/setupCall.js"));

            bundles.Add(new ScriptBundle("~/bundles/GoogleMap").Include(
                   "~/Content/Plugins/GoogleMap/mapmodal.js"
                   ));

            bundles.Add(new ScriptBundle("~/bundles/SignalR").Include(
                   "~/Content/Plugins/SignalR/jquery.signalR-2.2.3.min.js"
                   ));

            bundles.Add(new ScriptBundle("~/bundles/template").Include(
                "~/Content/Plugins/jquery_template/jquery.tmpl.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
               "~/Content/js/1.12.4.min.js"              
             ));

            bundles.Add(new ScriptBundle("~/jQuery/validate").Include(
                    "~/Content/Plugins/Validate/jquery.validate.min.js",
                    "~/Content/Plugins/Validate/jquery.validate.unobtrusive.min.js",
                    "~/Content/Plugins/Validate/jquery.unobtrusive-ajax.min.js"
                    ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
