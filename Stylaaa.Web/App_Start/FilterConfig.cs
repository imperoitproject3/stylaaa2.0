﻿using Stylaaa.Web.Filter;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new CompleteProfileSetup());
            //filters.Add(new HandleErrorAttribute());
        }
    }
}
