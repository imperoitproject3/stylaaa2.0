﻿using Newtonsoft.Json;
using Stripe;
using Stylaaa.Core.Models.StripeModels;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    public class StripeWebhookController : Controller
    {
        private GeneralServices _service;
        public StripeWebhookController()
        {
            _service = new GeneralServices();
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
           
            // MVC3/4: Since Content-Type is application/json in HTTP POST from Stripe
            // we need to pull POST body from request stream directly
            Stream req = Request.InputStream;
            req.Seek(0, SeekOrigin.Begin);

            string json = new StreamReader(req).ReadToEnd();
            await WriteLog("Stripe event call update " + json);
            StripeEvent stripeEvent = null;
            try
            {
                // as in header, you need httpsddd://github.com/jaymedavis/stripe.net
                // it's a great library that should have been offered by Stripe directly
                stripeEvent = StripeEventUtility.ParseEvent(json);
                //System.IO.File.WriteAllText(@"C:\ResponceUpdate.txt", json);

            }
            catch (Exception ex)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Unable to parse incoming event");
            }

            if (stripeEvent == null)
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Incoming event empty");
            var dserializeObject = JsonConvert.DeserializeObject<StripeResponceModel>(json);
            var objResponce = false;
            switch (stripeEvent.Type)
            {
                case "charge.expired":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
                case "charge.failed":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
                case "charge.pending":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
                case "charge.refund.updated":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
                case "charge.refunded":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
                case "charge.succeeded":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
                case "charge.updated":
                    objResponce = await _service.UpdateStylistPremiumTransactionStatus(dserializeObject);
                    break;
            }

            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public async Task<bool> WriteLog(string Content)
        {
            try
            {
                string logFileName = Path.Combine(System.Web.HttpRuntime.AppDomainAppPath, "Logs.txt");

                if (!System.IO.File.Exists(logFileName))
                    Directory.CreateDirectory(Path.Combine(System.Web.HttpRuntime.AppDomainAppPath));
                StreamWriter sw = new StreamWriter(logFileName, true);
                await sw.WriteLineAsync(string.Format("******* {0} *******", System.DateTime.Now));
                await sw.WriteLineAsync(Content);
                await sw.WriteLineAsync("*************************");
                sw.Close();
            }
            catch
            {
            }
            return false;
        }

    }
}
