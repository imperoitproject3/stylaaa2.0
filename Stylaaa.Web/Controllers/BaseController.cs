﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            var usertype = default(object);

            Role MyRole = Role.Stylist;
            if (RouteData.Values.ContainsKey("usertype"))
            {
                if (RouteData.Values.TryGetValue("usertype", out usertype))
                {
                    MyRole = (Role)Enum.Parse(typeof(Role), usertype?.ToString(), true);
                    ViewBag.MyRole = MyRole;
                }
            }

            UserService _user = new UserService();
            GeneralServices _general = new GeneralServices();
            string strController = "";
            if (User.Identity.IsAuthenticated)
            {
                ClaimsIdentity MyClaim = ((ClaimsIdentity)User.Identity);
                if (MyClaim != null)
                {
                    string claimRole = MyClaim.Claims
                   .FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
                    Role UserRole = (Role)Enum.Parse(typeof(Role), claimRole, true);
                    ViewBag.MyRole = UserRole;
                }
                CategoryService _catService = new CategoryService();
                string UserId = User.Identity.GetUserId();
                strController = RouteData.Values["controller"].ToString();
                if (strController == "Stylist" || strController == "User")
                {

                    string strMonth = (RouteData.Values["month"] != null ? RouteData.Values["month"].ToString() : "");
                    if (!String.IsNullOrWhiteSpace(strMonth))
                    {
                        UpgradeAccountModel objData = _general.GetUpgradeAccountDetail(Convert.ToInt32(strMonth));
                        ViewBag.UpgradAccountID = objData.UpgradeAccountId;
                        ViewBag.UpgradAmount = objData.Amount;
                        ViewBag.UpgradMonth = objData.Month;
                    }
                }

                ViewData["CategoryList"] = _catService.GetPostCategoryList(_user.GetLanguage(UserId));

                int CreditCount = 0;
                CreditServices _creditSer = new CreditServices();
                if (ViewBag.MyRole == Role.Stylist)
                {
                    CreditCount = _creditSer.GetStylistCedit(UserId);
                }
                ViewBag.MyCredit = CreditCount;
            }


            strController = RouteData.Values["controller"].ToString();
            if (strController == "Stylist" || strController == "User")
            {
                string strUserName = (RouteData.Values["username"] != null ? RouteData.Values["username"].ToString() : "");
                if (!String.IsNullOrWhiteSpace(strUserName))
                {
                    ViewBag.UserID = _user.GetUserID(strUserName);
                }
            }
        }

        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;

            HttpCookie langCookie = Request.Cookies["culture"];
            //langCookie.Expires = DateTime.Now;
            //langCookie.Expires = DateTime.Now.AddDays(-1);
            //HttpContext.Response.Cookies.Set(new HttpCookie("culture") { Value =null });
            //langCookie = null;
            //var langCookie = "da";
            if (langCookie != null)
            {
                lang = langCookie.Value;
                //lang = langCookie;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage[0] : "";
                if (userLang != "")
                {
                    lang = userLang;
                }
                else
                {
                    lang = LanguageManager.GetDefaultLanguage();
                }
            }
            new LanguageManager().SetLanguage(lang);
            return base.BeginExecuteCore(callback, state);
        }
    }
}