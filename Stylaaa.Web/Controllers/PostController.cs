﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using Stylaaa.Web.Filter;
using Stylaaa.Web.Helper;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    [CompleteProfileSetup(Role.Stylist, Role.User)]
    public class PostController : BaseController
    {
        private PostServices _service;
        private UserService _user;
        private CategoryService _catService;
        private GeneralServices _general;
        private StylishServices _stylist;

        public PostController()
        {
            _stylist = new StylishServices();
            _general = new GeneralServices();
            _catService = new CategoryService();
            _service = new PostServices();
            _user = new UserService();
        }

        #region post activity..

        public ActionResult AddPost()
        {
            string UserId = User.Identity.GetUserId();
            AddPostModel model = new AddPostModel() { PostCategoryList = _catService.GetPostCategoryList(_user.GetLanguage(UserId)) };
            return PartialView("_AddEditPost", model);
        }

        public async Task<ActionResult> EditPost(long id)
        {
            AddPostModel model = new AddPostModel();
            string UserId = User.Identity.GetUserId();
            if (id > 0)
            {
                model = await _service.GetPostdetailEdit(_user.GetLanguage(UserId), id, UserId);
            }
            return PartialView("_AddEditPost", model);
        }

        [HttpPost]
        public async Task<JsonResult> AddEditPost(AddPostModel model, FormCollection frm)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                //string strHashtags = frm["hidden-HashtagsArray"].ToString();
                //model.HashtagsArray = null;
                //if (!string.IsNullOrWhiteSpace(strHashtags))
                //{
                //    model.HashtagsArray = strHashtags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToString()).ToArray();
                //}

                string strHashtags = model.HashtagsArray[0].ToString();
                model.HashtagsArray = null;
                if (!string.IsNullOrWhiteSpace(strHashtags))
                {
                    model.HashtagsArray = strHashtags.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToString()).ToArray();
                }

                if (!string.IsNullOrWhiteSpace(model.HiddenPostFileName))
                {
                    model.PostFilesNamesArray = model.HiddenPostFileName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToString()).ToArray();

                    //var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                    //ffMpeg.GetVideoThumbnail(GlobalConfig.PostImagePath + model.HiddenPostFileName, GlobalConfig.PostImagePath + shortGuid + ".jpg");
                }
                model.UserId = User.Identity.GetUserId();
                mResult = await _service.AddEditPost(model);
            }
            else
            {
                mResult.Message = "please fill the required data.";
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> AddRemoveLikePost()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            int i = 0;
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                long postid = Convert.ToInt64(Request.Form["postid"]);
                i = await _service.addOrRemoveLike(postid, UserId);
                mResult.Message = i.ToString();
            }
            else
            {
                mResult.Message = i.ToString();
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> AddPostCommentByUser()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string UserId = User.Identity.GetUserId();
            long postid = Convert.ToInt64(Request.Form["postid"]);
            string message = Request.Form["message"].ToString();
            long CommentId = Convert.ToInt64(Request.Form["CommentId"].ToString());

            mResult = await _service.AddPostCommentByUser(postid, UserId, message, CommentId);
            mResult.Result = mResult.Result + "-" + _user.GetName(UserId);
            string strProImg = _user.GetProfileImage(UserId);
            mResult.Message = (!String.IsNullOrWhiteSpace(strProImg) ? GlobalConfig.UserImageUrl + strProImg : ("content/images/profile-placeholder.png"));
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteComment()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            long commentid = Convert.ToInt64(Request.Form["commentid"]);
            long replyid = Convert.ToInt64(Request.Form["replyid"]);

            mResult = await _service.DeleteComment(commentid, replyid);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> SpamComment()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            long commentid = Convert.ToInt64(Request.Form["commentid"]);

            mResult = await _service.SpamComment(commentid);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> DeletePost(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string UserId = User.Identity.GetUserId();
            if (id > 0)
            {
                mResult = await _service.DeletePost(id);
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddPostImage(FormCollection frm)
        {
            MediaResponseModel<object> mResult = new MediaResponseModel<object>();
            var userid = User.Identity.GetUserId();

            string pngimageData = frm["pngimageData"].ToString();
            string strFileName = frm["filename"].ToString();
            string strExistFileName = frm["existFileName"].ToString();
            string shortGuid = ShortGuid.NewGuid().ToString();
            if (!String.IsNullOrWhiteSpace(strExistFileName))
            {
                string strExistFilePath = GlobalConfig.PostImagePath + strExistFileName;
                if (System.IO.File.Exists(strExistFilePath))
                {
                    System.IO.File.Delete(strExistFilePath);
                }
            }
            strFileName = userid + "_" + shortGuid + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetExtension(strFileName);
            string strFilePath = Path.Combine(GlobalConfig.PostImagePath, strFileName);

            string img = pngimageData.Replace("data:image/png;base64,", "").Replace(' ', '+');
            //byte[] decodedData = Convert.FromBase64String(img);
            //System.IO.File.WriteAllBytes(strFilePath, decodedData);

            Utility.SaveCropImage(img, strFilePath, Path.GetExtension(strFileName));

            mResult.FileName = strFileName;
            mResult.MediaPath = GlobalConfig.PostImageUrl + strFileName;

            mResult.Result = ResponseStatus.Success;
            mResult.Message = "Success";

            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddPostMedia(IEnumerable<HttpPostedFileBase> uploadedFile)
        {
            MediaResponseModel<object> mResult = new MediaResponseModel<object>();
            string strFileName = "";
            var userid = User.Identity.GetUserId();

            try
            {
                foreach (HttpPostedFileBase fileMedia in uploadedFile)
                {
                    if (fileMedia != null && fileMedia.ContentLength > 0)
                    {
                        string shortGuid = ShortGuid.NewGuid().ToString();
                        strFileName = userid + "_" + shortGuid + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetExtension(fileMedia.FileName);
                        fileMedia.SaveAs(Path.Combine(GlobalConfig.PostImagePath, strFileName));

                        //var fffMpeg = new NReco.VideoConverter.FFMpegConverter();
                        //fffMpeg.ConvertMedia(GlobalConfig.PostImagePath + strFileName, GlobalConfig.PostImagePath + "con" + strFileName, "mp4");

                        string[] mediaExtensions = { ".MP4", ".WMV", ".MOV" };
                        mResult.FileName += strFileName + ",";
                        if (IsMediaFile(mediaExtensions, strFileName))
                        {
                            var ffMpeg = new NReco.VideoConverter.FFMpegConverter();
                            //var filename = Path.GetFileNameWithoutExtension(GlobalConfig.PostImageUrl + strFileName);
                            ffMpeg.GetVideoThumbnail(GlobalConfig.PostImagePath + strFileName, GlobalConfig.PostImagePath + strFileName + ".jpg", 10);
                            mResult.MediaPath += GlobalConfig.PostImageUrl + strFileName + ",";
                        }
                        else
                        {
                            mResult.MediaPath += GlobalConfig.PostImageUrl + strFileName + ",";
                        }

                        mResult.Result = ResponseStatus.Success;
                        mResult.Message = "Success";
                    }
                    else
                    {
                        mResult.Result = ResponseStatus.Failed;
                        mResult.Message = "Failed";
                        mResult.Message = "please upload file";
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Result = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteMedia()
        {
            MediaResponseModel<object> mResult = new MediaResponseModel<object>();
            string strFileName = Request.Form["filename"].ToString();
            string FilePath = GlobalConfig.PostImagePath + strFileName;
            if (System.IO.File.Exists(FilePath))
            {
                System.IO.File.Delete(FilePath);
            }

            if (System.IO.File.Exists(FilePath + ".jpg"))
            {
                System.IO.File.Delete(FilePath + ".jpg");
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public static bool IsMediaFile(string[] mediaExtensions, string path)
        {
            return -1 != Array.IndexOf(mediaExtensions, Path.GetExtension(path).ToUpperInvariant());
        }

        [HttpPost]
        public async Task<JsonResult> AddPostReportByUser()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            PostReportModel model = new PostReportModel();
            model.Reason = Request.Form["ReasonText"];
            model.PostId = Convert.ToInt64(Request.Form["PostID"]);
            model.UserId = User.Identity.GetUserId();

            mResult = await _service.AddPostReportByUser(model);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> ViewPostDetail()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                long PostID = Convert.ToInt64(Request.Form["postid"].ToString());

                PostViewDetailModel model = await _service.GetPostViewDetailModel(_user.GetLanguage(UserId), PostID, UserId);
                model.CurrentUserRole = _user.GetRoleById(UserId);
                return PartialView("_PostDetailAndComments", model);
            }
            else
            {
                return Json("1", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> AddSharePost()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string UserId = User.Identity.GetUserId();
            long id = Convert.ToInt64(Request.Form["hdnSharePostId"]);
            mResult = await _service.AddSharePost(id, UserId);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        #endregion post activity..

        #region get post list by condition..

        public async Task<ActionResult> GetAllPostData(int id, string hashtag, long postid)
        {
            List<string> lstHashtag = new List<string>(1);
            lstHashtag.Add(hashtag);
            string UserId = User.Identity.GetUserId();
            GetPostViewListModel getPostViewListModel = new GetPostViewListModel();
            getPostViewListModel.RecentPostPageIndex = id;
            getPostViewListModel.PopularPostPageIndex = id;

            getPostViewListModel.RecentPost = await _service.RecentPost(new PaginationModel { PageIndex = id, UserId = UserId }, lstHashtag, postid);
            getPostViewListModel.PopularPost = await _service.PopularPost(new PaginationModel { PageIndex = id, UserId = UserId }, lstHashtag, postid);
            getPostViewListModel.CategoryList = _catService.GetPostCategoryList(_user.GetLanguage(UserId));

            List<GetPostModelRecent> totalrecentpost = await _service.RecentPost(new PaginationModel { PageIndex = 0, UserId = UserId }, lstHashtag, postid);
            List<GetPostModelPopular> totalpopularpost = await _service.PopularPost(new PaginationModel { PageIndex = 0, UserId = UserId }, lstHashtag, postid);

            decimal totarecentpostCount = totalrecentpost.Count;
            decimal totalpopularpostCount = totalpopularpost.Count;

            getPostViewListModel.RecentPostPageCount = Math.Ceiling((decimal)(totarecentpostCount > 0 ? (totarecentpostCount / GlobalConfig.PageSize) : 0));
            getPostViewListModel.PopularPostPageCount = Math.Ceiling((decimal)(totalpopularpostCount > 0 ? (totalpopularpostCount / GlobalConfig.PageSize) : 0));

            return PartialView("_PostList", getPostViewListModel);
        }

        [AllowAnonymous]
        public async Task<ActionResult> PartialPost(int intPageIndex, string username)
        {
            string UserId = _user.GetUserID(username);
            GetPostListByUser getPostListModel = new GetPostListByUser();
            getPostListModel.RecentPostPageIndex = intPageIndex;
            getPostListModel = await _service.PostListByUser(new PaginationModel { PageIndex = getPostListModel.RecentPostPageIndex, UserId = UserId });

            GetPostListByUser totalrecentpost = await _service.PostListByUser(new PaginationModel { PageIndex = 0, UserId = UserId });
            decimal totarecentpostCount = totalrecentpost.RecentPost.Count;

            getPostListModel.RecentPostPageCount = Math.Ceiling((decimal)(totarecentpostCount > 0 ? (totarecentpostCount / GlobalConfig.PageSize) : 0));

            return PartialView("_ProfilePost", getPostListModel);
        }

        public async Task<ActionResult> GetRecentPostData(int id, string hashtag, long postid)
        {
            List<string> lstHashtag = new List<string>(1);
            lstHashtag.Add(hashtag);
            string UserId = User.Identity.GetUserId();
            GetPostViewListModel getPostViewListModel = new GetPostViewListModel();
            getPostViewListModel.RecentPostPageIndex = id;

            getPostViewListModel.RecentPost = await _service.RecentPost(new PaginationModel { PageIndex = id, UserId = UserId }, lstHashtag, postid);

            List<GetPostModelRecent> totalrecentpost = await _service.RecentPost(new PaginationModel { PageIndex = 0, UserId = UserId }, lstHashtag, postid);
            decimal totarecentpostCount = totalrecentpost.Count;

            getPostViewListModel.RecentPostPageCount = Math.Ceiling((decimal)(totarecentpostCount > 0 ? (totarecentpostCount / GlobalConfig.PageSize) : 0));

            return PartialView("_RecentPostList", getPostViewListModel);
        }

        public async Task<ActionResult> GetPopularPostData(int id, string hashtag, long postid)
        {
            List<string> lstHashtag = new List<string>(1);
            lstHashtag.Add(hashtag);
            string UserId = User.Identity.GetUserId();
            GetPostViewListModel getPostViewListModel = new GetPostViewListModel();
            getPostViewListModel.PopularPostPageIndex = id;

            getPostViewListModel.PopularPost = await _service.PopularPost(new PaginationModel { PageIndex = id, UserId = UserId }, lstHashtag, postid);
            List<GetPostModelPopular> totalpopularpost = await _service.PopularPost(new PaginationModel { PageIndex = 0, UserId = UserId }, lstHashtag, postid);
            decimal totalpopularpostCount = totalpopularpost.Count;

            getPostViewListModel.PopularPostPageCount = Math.Ceiling((decimal)(totalpopularpostCount > 0 ? (totalpopularpostCount / GlobalConfig.PageSize) : 0));

            return PartialView("_PopularPostList", getPostViewListModel);
        }

        [HttpPost]
        public async Task<JsonResult> SearchPostbyCategory()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            PostFilterModel model = new PostFilterModel();
            string UserId = User.Identity.GetUserId();

            string category = Request.Form["CheckedCategory"];

            model.CategoryIds = null;
            if (!string.IsNullOrWhiteSpace(category))
            {
                model.CategoryIds = category.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt64(x.ToString())).ToList();
            }
            model.PageIndex = Convert.ToInt32(Request.Form["intSearchPostPageIndex"]);
            model.SearchText = Request.Form["SearchPostText"];

            bool isCount = Convert.ToBoolean(Request.Form["isCount"]);
            bool isData = Convert.ToBoolean(Request.Form["isData"]);

            string strLocalLatitude = Request.Form["localLatitude"];
            string strLocalLongitude = Request.Form["localLongitude"];

            SearchPopularPostModel data = new SearchPopularPostModel();
            if (isCount == true)
            {
                List<GetPostModelPopular> result = await _service.FilterPostlist(model, UserId, true, strLocalLatitude, strLocalLongitude);
                decimal totalPost = result.Count;
                data.PageSize = Math.Ceiling((decimal)(totalPost > 0 ? (totalPost / GlobalConfig.PageSize) : 0)).ToString();
            }
            if (isData == true)
            {
                List<GetPostModelPopular> result = await _service.FilterPostlist(model, UserId, false, strLocalLatitude, strLocalLongitude);

                List<GetPostModelPopular> data1 = result.Select(post => new GetPostModelPopular()
                {
                    PostLinkUrl = Url.Action("Index", "Home") + "?postid=" + post.PostId,
                    UserName = post.UserName,
                    Description = post.Description,
                    FileName = post.FileName,
                    PostId = post.PostId,
                    UserLinkUrl = Url.RouteUrl((post.PostByRole == Role.Stylist ? "Stylist" : "User"), new { username = post.NickName, action = "About" })
                }).ToList();

                data.GetPostModelPopular = data1;
            }

            mResult.Result = JsonConvert.SerializeObject(data);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> SearchStylishbyCategory()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            PostFilterModel model = new PostFilterModel();
            string UserId = User.Identity.GetUserId();

            string category = Request.Form["CheckedCategory"];

            model.CategoryIds = null;
            if (!string.IsNullOrWhiteSpace(category))
            {
                model.CategoryIds = category.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt64(x.ToString())).ToList();
            }
            model.PageIndex = Convert.ToInt32(Request.Form["intSearchStylistPageIndex"]);
            model.SearchText = Request.Form["SearchPostText"];

            bool isCount = Convert.ToBoolean(Request.Form["isCount"]);
            bool isData = Convert.ToBoolean(Request.Form["isData"]);

            string strLocalLatitude = Request.Form["localLatitude"];
            string strLocalLongitude = Request.Form["localLongitude"];

            SearchPopularStylishModel data = new SearchPopularStylishModel();
            if (isCount == true)
            {
                List<PopularStylishFilterModel> result = await _stylist.FilterStylistlist(model, UserId, true, strLocalLatitude, strLocalLongitude);
                decimal totalPost = result.Count;
                data.PageSize = Math.Ceiling((decimal)(totalPost > 0 ? (totalPost / GlobalConfig.PageSize) : 0)).ToString();
            }
            if (isData == true)
            {
                List<PopularStylishFilterModel> result = await _stylist.FilterStylistlist(model, UserId, false, strLocalLatitude, strLocalLongitude);

                List<PopularStylishFilterModel> data1 = result.Select(user => new PopularStylishFilterModel()
                {
                    UserId = user.UserId,
                    UserName = user.UserName,
                    UserRole = user.UserRole,
                    Name = user.Name,
                    UserProfile = user.UserProfile,
                    Distance = user.Distance,
                    IsPremium = user.IsPremium,
                    UserLinkUrl = Url.RouteUrl((user.UserRole == Role.Stylist ? "Stylist" : "User"), new { username = user.UserName.Replace("@", ""), action = "About" })
                }).ToList();

                data.PopularStylishFilterModel = data1;
            }

            mResult.Result = JsonConvert.SerializeObject(data);
            return Json(mResult, JsonRequestBehavior.AllowGet);
            //return PartialView("_SearchStylist", result);
        }

        [HttpPost]
        public async Task<JsonResult> SearchHashtagbyCategory()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            PostFilterModel model = new PostFilterModel();
            string category = Request.Form["CheckedCategory"];

            model.CategoryIds = null;
            if (!string.IsNullOrWhiteSpace(category))
            {
                model.CategoryIds = category.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => Convert.ToInt64(x.ToString())).ToList();
            }
            model.PageIndex = Convert.ToInt32(Request.Form["intSearchHashtagPageIndex"]);
            model.SearchText = Request.Form["SearchPostText"];
            bool isCount = Convert.ToBoolean(Request.Form["isCount"]);
            bool isData = Convert.ToBoolean(Request.Form["isData"]);

            string strLocalLatitude = Request.Form["localLatitude"];
            string strLocalLongitude = Request.Form["localLongitude"];

            SearchHashtagModel data = new SearchHashtagModel();
            if (isCount == true)
            {
                List<HashtagFilterModel> result = await _service.FilterHashtaglist(model, true, strLocalLatitude, strLocalLongitude);
                decimal totalPost = result.Count;
                data.PageSize = Math.Ceiling((decimal)(totalPost > 0 ? (totalPost / GlobalConfig.PageSize) : 0)).ToString();
            }
            if (isData == true)
            {
                List<HashtagFilterModel> result = await _service.FilterHashtaglist(model, false, strLocalLatitude, strLocalLongitude);

                List<HashtagFilterModel> data1 = result.Select(user => new HashtagFilterModel()
                {
                    Hashtag = user.Hashtag,
                    PostCount = user.PostCount,
                    PostLinkUrl = Url.Action("Index", "Home") + "?0&hashtag=" + user.Hashtag,
                    PostText = (user.PostCount > 1 ? "Posts" : "Post")
                }).ToList();

                data.HashtagFilterModel = data1;
            }
            mResult.Result = JsonConvert.SerializeObject(data);
            return Json(mResult, JsonRequestBehavior.AllowGet);
            //return PartialView("_SearchHashtag", result);
        }

        public async Task<ActionResult> ViewLikeActivityPageSize()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string UserId = User.Identity.GetUserId();
            FindLikeModel model = new FindLikeModel();
            model.PostId = Convert.ToInt64(Request.Form["intPostId"]);

            List<ViewLikeModel> result = await _service.ViewLikeActivity(model, true);
            decimal totalHashtag = result.Count;
            mResult.Message = Math.Ceiling((decimal)(totalHashtag > 0 ? (totalHashtag / GlobalConfig.PageSize) : 0)).ToString();

            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ViewLikeActivity()
        {
            string UserId = User.Identity.GetUserId();
            FindLikeModel model = new FindLikeModel();
            model.UserId = UserId;
            model.PostId = Convert.ToInt64(Request.Form["intPostId"]);
            model.PageIndex = Convert.ToInt32(Request.Form["intLikePageIndex"]);

            List<ViewLikeModel> result = await _service.ViewLikeActivity(model, false);
            return PartialView("_likesActivity", result);
        }

        #endregion get post list by condition..
    }
}