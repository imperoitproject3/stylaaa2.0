﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    public class ChatController : BaseController
    {
        private UserService _user;
        private CoreUserServices _coreuser;
        private ChatRepository _chat;
        private MessageService _message;
        //private MessageService _msg;
        public ChatController()
        {
            _message = new MessageService();
            _chat = new ChatRepository();
            _user = new UserService();
            _coreuser = new CoreUserServices();
        }

        public async Task<ActionResult> Index(MessageReceiverModel model)
        {
            model.UserID = User.Identity.GetUserId();
            ViewBag.UserProfile = await _coreuser.GetUserDetail(model.UserID);
            bool isrequested = await _chat.IsRequested(model.UserID, model.ReceiverID);
            bool isAccepted = await _chat.IsRequestedAccepted(model.UserID, model.ReceiverID);

            int count = await _chat.CountChat(model.UserID, model.ReceiverID);

            HubConnectionModel hubConnectionModel = new HubConnectionModel()
            {
                ConnectionUrl = GlobalConfig.HubConnectionUrl,
                UserID = model.UserID,
                ReceiverID = model.ReceiverID,
                ReceiverName = model.ReceiverName,
                ReceiverImage = model.ReceiverImage,
                IsRequested = isrequested,
                IsRequestAccepted = isAccepted,
                MessageCount = count,
                isFirstTwoUser = _chat.isFirstTwoUser(model.UserID, model.ReceiverID),
                SenderRole = _user.GetRoleById(model.ReceiverID),
                JobId = model.JobId
            };
            return PartialView("_ChatScreen", hubConnectionModel);
        }


        [HttpPost]
        public async Task<ActionResult> RenderMobileChatScreen(MessageReceiverModel model)
        {
            model.UserID = User.Identity.GetUserId();
            ViewBag.UserProfile = await _coreuser.GetUserDetail(model.UserID);
            bool isrequested = await _chat.IsRequested(model.UserID, model.ReceiverID);
            int count = await _chat.CountChat(model.UserID, model.ReceiverID);
            bool isAccepted = await _chat.IsRequestedAccepted(model.UserID, model.ReceiverID);

            HubConnectionModel hubConnectionModel = new HubConnectionModel()
            {
                ConnectionUrl = GlobalConfig.HubConnectionUrl,
                UserID = model.UserID,
                ReceiverID = model.ReceiverID,
                ReceiverName = model.ReceiverName,
                ReceiverImage = model.ReceiverImage,
                IsRequested = isrequested,
                IsRequestAccepted = isAccepted,
                MessageCount = count,
                isFirstTwoUser = _chat.isFirstTwoUser(model.UserID, model.ReceiverID),
                SenderRole = _user.GetRoleById(model.ReceiverID),
                JobId = model.JobId
            };
            return PartialView("_MobChatScreen", hubConnectionModel);
        }


        //public async Task<ActionResult> Chat()
        //{
        //    if (User.Identity.IsAuthenticated)
        //    {
        //        string UserId = User.Identity.GetUserId();
        //        bool IsProfileComplete = _user.checkprofilesetup(UserId);
        //        ViewBag.isProfileCompleted = IsProfileComplete;
        //        ViewUserDetail userDetail = new ViewUserDetail();
        //        userDetail = await _coreuser.GetUserDetail(UserId);
        //        ViewBag.UserProfile = userDetail;
        //        if (!string.IsNullOrWhiteSpace(UserId))
        //        {
        //            //string UserId = Request.Cookies["UserId"].Value;
        //            if (!string.IsNullOrWhiteSpace(UserId))
        //            {
        //                //long User = Convert.ToInt64(UserId);
        //                IQueryable<UserModel> objUsers = _user.GetAllUsers_IQueryable(UserId, userDetail.IsUserExpire);
        //                ViewBag.UserId = UserId;
        //                return View(objUsers);
        //            }
        //        }
        //        return RedirectToAction("Index", "Home");
        //    }
        //    else
        //    {
        //        return RedirectToAction("Login", "Account");
        //    }
        //}

        public async Task<ActionResult> ChatJob()
        {
            bool IsMobile = Request.Browser.IsMobileDevice;
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                bool IsProfileComplete = _user.checkprofilesetup(UserId);
                ViewBag.isProfileCompleted = IsProfileComplete;
                ViewUserDetail userDetail = new ViewUserDetail();
                userDetail = await _coreuser.GetUserDetail(UserId);
                ViewBag.UserProfile = userDetail;
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    //string UserId = Request.Cookies["UserId"].Value;
                    if (!string.IsNullOrWhiteSpace(UserId))
                    {
                        //long User = Convert.ToInt64(UserId);
                        ChatModel model = new ChatModel();
                        model.ChatMessageModel = _user.GetAllUsers_IQueryable(UserId, userDetail.IsUserExpire);
                        model.JobMessageModel = await _user.GetAllJobRequest_IQueryable(UserId);

                        ViewBag.UserId = UserId;
                        if (IsMobile)
                        {
                            return View("MobileJobChat", model);
                        }
                        else
                        {
                            return View(model);
                        }
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        //public async Task<ActionResult> MobChat()
        //{
        //    string UserId = User.Identity.GetUserId();
        //    ViewBag.UserProfile = await _coreuser.GetUserDetail(UserId);
        //    bool IsProfileComplete = _user.checkprofilesetup(UserId);
        //    ViewBag.isProfileCompleted = IsProfileComplete;
        //    ViewUserDetail userDetail = new ViewUserDetail();
        //    userDetail = await _coreuser.GetUserDetail(UserId);
        //    ViewBag.UserProfile = userDetail;
        //    if (!string.IsNullOrWhiteSpace(UserId))
        //    {
        //        //string UserId = Request.Cookies["UserId"].Value;
        //        if (!string.IsNullOrWhiteSpace(UserId))
        //        {
        //            //long User = Convert.ToInt64(UserId);
        //            IQueryable<UserModel> objUsers = _user.GetAllUsers_IQueryable(UserId, userDetail.IsUserExpire);
        //            ViewBag.UserId = UserId;
        //            return View(objUsers);
        //        }
        //    }
        //    return RedirectToAction("Index", "Home");
        //}

        public async Task<ActionResult> MobileJobChat()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                bool IsProfileComplete = _user.checkprofilesetup(UserId);
                ViewBag.isProfileCompleted = IsProfileComplete;
                ViewUserDetail userDetail = new ViewUserDetail();
                userDetail = await _coreuser.GetUserDetail(UserId);
                ViewBag.UserProfile = userDetail;
                if (!string.IsNullOrWhiteSpace(UserId))
                {
                    //string UserId = Request.Cookies["UserId"].Value;
                    if (!string.IsNullOrWhiteSpace(UserId))
                    {
                        //long User = Convert.ToInt64(UserId);
                        ChatModel model = new ChatModel();
                        model.ChatMessageModel = _user.GetAllUsers_IQueryable(UserId, userDetail.IsUserExpire);
                        model.JobMessageModel = await _user.GetAllJobRequest_IQueryable(UserId);

                        ViewBag.UserId = UserId;
                        return View(model);
                    }
                }
                return RedirectToAction("Index", "Home");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public JsonResult AddImage(ImageSendModel model)
        {
            #region Upload video
            string shortGuid = ShortGuid.NewGuid().ToString();
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                //get filename
                string ogFileName = file.FileName.Trim('\"');
                var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                file.SaveAs(Path.Combine(GlobalConfig.ChatImagePath, thisFileName));
                model.FileName = thisFileName;
            }
            #endregion
            //model.path = Url.Content("~/Files/ChatImages/" + model.FileName);
            model.path = GlobalConfig.ChatImageurl + model.FileName;
            return Json(model, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> SendMessage(MessageSendModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                mResult = await _chat.SendMessage(model, UserId);
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Failed to send message";
                //mResult.Result = "No";
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public async Task<JsonResult> SendJobRequest(JobRequestModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                mResult = await _message.SendJobRequest(model, UserId);
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Failed to send request";
                //mResult.Result = "No";
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> ApproveRequest()
        {
            string UserId = User.Identity.GetUserId();
            string ReceiverId = Request.Form["ReceiverID"].ToString();
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _chat.ApproveRequest(UserId, ReceiverId);
            return Json(ReceiverId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteChatMessage(long ChatId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _message.DeleteChatMessage(ChatId);
            return Json(mResult.Message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> DeleteChatConversastion(string receiverId)
        {
            string userid = User.Identity.GetUserId();
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _message.DeleteConversationForChat(userid, receiverId);
            return Json(mResult.Message, JsonRequestBehavior.AllowGet); 
        }

        [HttpPost]
        public async Task<JsonResult> DeleteJobConversation(string receiverId,long JobId)
        {
            string userid = User.Identity.GetUserId();
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _message.DeleteConversastionForJob(userid, receiverId, JobId);
            return Json(mResult.Message, JsonRequestBehavior.AllowGet);
        }
    }
}