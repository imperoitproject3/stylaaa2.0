﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Stripe;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using Stylaaa.Data.Services;
using Stylaaa.Services;
using Stylaaa.Stripe;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    public class StripeController : BaseController
    {
        private UserService _userservice;
        private stPaymentChargeService _service;
        private GeneralServices _general;
        private CoreUserServices _coreservice;
        public static int intAmount = 0, intMonth = 0, IntCredit = 0;
        public static string strPaymentMothod = "Card";

        public StripeController()
        {
            _userservice = new UserService();
            _service = new stPaymentChargeService();
            _general = new GeneralServices();
            _coreservice = new CoreUserServices();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> UpgradePlan(string stripeToken, string stripeEmail, string UpgradeAccountId, bool blnPayInInstallment = false)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var UserId = User.Identity.GetUserId();

                var Id = Convert.ToInt32(UpgradeAccountId);
                var objData = _general.GetUpgradeAccountDetailByID(Id);
                var StripeAccoutId = await _userservice.GetStripeAccoutId(UserId);

                intAmount = (int)objData.Amount;
                intMonth = objData.Month;

                var mResponce = await _service.CapturePaymentCharge(new StripePaymentChargeRequestModel
                {
                    chargeAmount = intAmount,
                    stripeCustomerId = StripeAccoutId,
                    stripePaymentToken = stripeToken,
                    receiptEmail = stripeEmail
                });
                if (mResponce.Status == ResponseStatus.Success)
                {

                    await _general.AddStylistPremiumTransaction(new StylistPremiumTransactionModel()
                    {
                        StylistId = UserId,
                        PaymentMothod = strPaymentMothod,
                        month = intMonth,
                        // invoicefilename = xeroResult.Result.ToString()
                    });
                    //string strXeroReceiptTitle = "";
                    //if (!String.IsNullOrWhiteSpace(strInvoiceNo))
                    //{
                    //var taxamount = Math.Round(((intAmount * 0.833)), 2);
                    //var FinalAmount = Math.Round(intAmount - taxamount, 2);
                    //strXeroReceiptTitle = XeroInvoice.FindByInvoiceAndMarkAsPaid(strInvoiceNo);
                    ////****************************Xero Reciept*****************************//
                    //XeroReceiptSendModel xeroReceiptSendModel = new XeroReceiptSendModel()
                    //{
                    //    Amount = Convert.ToDouble(intAmount),
                    //    Title = strXeroReceiptTitle,
                    //    TokenID = mResponce.Result.stripePaymentChargeToken, //objResponce.pspReference,
                    //    Vat = FinalAmount,
                    //    userData = GetUserInfoForXeroReceipt(UserId),
                    //};

                    //ResponseModel<object> xeroResult = new ResponseModel<object>();
                    //xeroResult = await XeroReceipt.Add(xeroReceiptSendModel);
                    //if (xeroResult.Status == ResponseStatus.Failed)
                    //{
                    //    ViewBag.status = ResponseStatus.Failed;                            
                    //    ViewBag.successmessage = Resource.paymentFailedMessage;
                    //}
                    //else
                    //{                   
                    //insert stylist primum transaction
                    var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                    await _general.UpdateStylistPremiumTransaction(new StylistPremiumTransactionModel()
                    {
                        StylistId = UserId,
                        PaymentMothod = strPaymentMothod,
                        month = intMonth,
                        //receiptfilename = xeroResult.Result.ToString(),
                        stripeResponce = JsonResult,
                        paymentStatus = ChargeStatus.succeeded,
                        transactionId = mResponce.Result.stripePaymentChargeToken
                    });
                    ViewBag.status = ResponseStatus.Success;
                    ViewBag.successmessage = Resource.paymentSuccessMsg2;
                    //}
                    //}
                }
                else
                {
                    ViewBag.status = ResponseStatus.Failed;
                    ViewBag.successmessage = Resource.PaymentFailedDetail;
                }
            }
            catch (Exception ex)
            {
                //ViewBag.successmessage = "error:" + ex.Message.ToString();
                ViewBag.successmessage = Resource.PaymentFailedDetail;
                ViewBag.status = ResponseStatus.Failed;

            }
            mResult.Message = ViewBag.successmessage;
            TempData["status"] = ViewBag.status;
            TempData["successmessage"] = ViewBag.successmessage;
            return RedirectToRoute("Stylist", new { username = User.Identity.GetUserName(), action = "UpdateToPremium" });
        }


        //public async Task<string> sentInvoice(int intAmount, int month, string strEmail, string strpspReference)
        //{
        //    var UserId = User.Identity.GetUserId();

        //    string strMerchantAccount = WebConfigurationManager.AppSettings["merchantAccount"];
        //    var taxamount = Math.Round(((intAmount * 0.833)), 2);
        //    var FinalAmount = Math.Round(intAmount - taxamount, 2);
        //    XeroInvoiceSendModel xeroInvoiceSendModel = new XeroInvoiceSendModel()
        //    {
        //        Amount = Convert.ToDouble(taxamount),
        //        Title = month + " month subscription",
        //        TokenID = strpspReference,
        //        Vat = FinalAmount,
        //        userData = GetUserInfoForXeroInvoice(UserId)
        //    };
        //    if (Convert.ToInt32(month) > 1)
        //    {
        //        xeroInvoiceSendModel.Title = month + " months subscription";
        //    }
        //    ResponseModel<object> xeroResult = await XeroInvoice.Add(xeroInvoiceSendModel);
        //    if (xeroResult.Status == ResponseStatus.Failed)
        //    {
        //        return "error:" + xeroResult.Message.ToString();
        //        // mResult.Result
        //    }
        //    else
        //    {
        //        string strInvoiceNo = xeroResult.Result.ToString().Replace(".pdf", "");
        //        await _general.AddStylistPremiumTransaction(new StylistPremiumTransactionModel()
        //        {
        //            StylistId = UserId,
        //            PaymentMothod = strPaymentMothod,
        //            month = intMonth,
        //            invoicefilename = xeroResult.Result.ToString()
        //        });
        //        //InsertStylistPremiumTransaction(xeroResult.Result.ToString());
        //        return strInvoiceNo;
        //    }
        //}

        //public async Task<string> sentInvoiceForCredit(int intAmount, int Credit, string strEmail, string strpspReference)
        //{
        //    var UserId = User.Identity.GetUserId();

        //    string strMerchantAccount = WebConfigurationManager.AppSettings["merchantAccount"];
        //    var taxamount = Math.Round(((intAmount * 0.833)), 2);
        //    var FinalAmount = Math.Round(intAmount - taxamount, 2);
        //    XeroInvoiceSendModel xeroInvoiceSendModel = new XeroInvoiceSendModel()
        //    {
        //        Amount = Convert.ToDouble(taxamount),
        //        Title = Credit + " Credit",
        //        TokenID = strpspReference,
        //        Vat = FinalAmount,
        //        userData = GetUserInfoForXeroInvoice(UserId)
        //    };
        //    if (Convert.ToInt32(Credit) > 1)
        //    {
        //        xeroInvoiceSendModel.Title = Credit + " Credit";
        //    }
        //    ResponseModel<object> xeroResult = await XeroInvoice.Add(xeroInvoiceSendModel);
        //    if (xeroResult.Status == ResponseStatus.Failed)
        //    {
        //        return "error:" + xeroResult.Message.ToString();
        //        // mResult.Result
        //    }
        //    else
        //    {
        //        string strInvoiceNo = xeroResult.Result.ToString().Replace(".pdf", "");
        //        await _general.AddStylistCreditTransaction(new StylistCreditTransactionModel()
        //        {
        //            StylistId = UserId,
        //            PaymentMothod = strPaymentMothod,
        //            Credit = Credit,
        //            //invoicefilename = xeroResult.Result.ToString()
        //        });
        //        //InsertStylistPremiumTransaction(xeroResult.Result.ToString());
        //        return strInvoiceNo;
        //    }
        //}

        public XeroInvoiceUserModel GetUserInfoForXeroInvoice(string UserId)
        {
            var xeroInvoiceUserModel = new XeroInvoiceUserModel();
            var userdetail = _coreservice.GetUserProfileModel(UserId);

            xeroInvoiceUserModel.CityName = userdetail.CityName;
            xeroInvoiceUserModel.CompanyEmailID = userdetail.Email;
            xeroInvoiceUserModel.CompanyName = GlobalConfig.ProjectName;
            xeroInvoiceUserModel.ContactNumber = userdetail.ContactNumber;
            xeroInvoiceUserModel.EmailID = userdetail.Email;
            xeroInvoiceUserModel.FirstName = userdetail.Name;
            xeroInvoiceUserModel.FullAddress = userdetail.Address;
            xeroInvoiceUserModel.LastName = "";
            xeroInvoiceUserModel.StreetAddress = userdetail.StreetAddress;
            xeroInvoiceUserModel.Zipcode = userdetail.zipcode.ToString();

            return xeroInvoiceUserModel;
        }

        public XeroReceiptUserModel GetUserInfoForXeroReceipt(string UserId)
        {
            XeroReceiptUserModel xeroReceiptUserModel = new XeroReceiptUserModel();

            UserProfileModel userdetail = _coreservice.GetUserProfileModel(UserId);

            xeroReceiptUserModel.CityName = userdetail.CityName;
            xeroReceiptUserModel.CompanyEmailID = userdetail.Email;
            xeroReceiptUserModel.CompanyName = GlobalConfig.ProjectName;
            xeroReceiptUserModel.ContactNumber = userdetail.ContactNumber;
            xeroReceiptUserModel.EmailID = userdetail.Email;
            xeroReceiptUserModel.FirstName = userdetail.Name;
            xeroReceiptUserModel.FullAddress = userdetail.Address;
            xeroReceiptUserModel.LastName = "";
            xeroReceiptUserModel.StreetAddress = userdetail.StreetAddress;
            xeroReceiptUserModel.Zipcode = userdetail.zipcode.ToString();

            return xeroReceiptUserModel;
        }

        public async Task<ActionResult> CheckoutPayment(string UpgradeAccountId, StripeType StripeType, string InvoiceNo)
        {
            var UpgradeAccountIdInt = Convert.ToInt32(UpgradeAccountId);

            var objData = _general.GetUpgradeAccountDetailByID(UpgradeAccountIdInt);
            var UserId = User.Identity.GetUserId();
            var UserDetail = await _coreservice.GetUserDetail(UserId);
            var Totalamount = (int)objData.Amount;

            intAmount = (int)objData.Amount;
            intMonth = objData.Month;

            var mResponce = await _service.CreatePaymentSource(new StripePaymentSourceRequestModel
            {
                chargeAmount = Totalamount,
                stripeCustomerId = UserDetail.StripeAccountId,
                CustomerEmail = UserDetail.Email,
                CustomerName = UserDetail.Name,
                stripeType = StripeType                
            });
            //VerifyObjectModel objResponce = new VerifyObjectModel();
            if (mResponce.Status == ResponseStatus.Success)
            {
                await _general.AddStylistPremiumTransaction(new StylistPremiumTransactionModel()
                {
                    StylistId = UserId,
                    PaymentMothod = strPaymentMothod,
                    month = intMonth                   
                });

                ViewBag.status = ResponseStatus.Success;
                intAmount = Totalamount;
                return Redirect(mResponce.Result.Redirect.Url);
            }
            else
            {
                ViewBag.status = ResponseStatus.Failed;
                ViewBag.successmessage = mResponce.Message;
            }
            TempData["status"] = ViewBag.status;
            TempData["successmessage"] = ViewBag.successmessage;
            return RedirectToRoute("Stylist", new { username = User.Identity.GetUserName(), action = "UpdateToPremium" });
        }

        public async Task<ActionResult> CheckoutPaymentCredit(string UpgradeCreditId, StripeType StripeType, string InvoiceNo)
        {
            var UpgradeCreditIdInt = Convert.ToInt32(UpgradeCreditId);

            var objData = _general.GetUpgradeCreditDetailById(UpgradeCreditIdInt);
            var UserId = User.Identity.GetUserId();
            var UserDetail = await _coreservice.GetUserDetail(UserId);
            var Totalamount = (int)objData.Amount;

            intAmount = (int)objData.Amount;
            IntCredit = objData.Credit;

            //string strInvoiceNo = InvoiceNo;
            //if (String.IsNullOrWhiteSpace(strInvoiceNo))
            //{
            //    strInvoiceNo = await sentInvoice(Totalamount, objData.Credit, UserDetail.Email, "");
            //    ViewBag.invoiceNo = strInvoiceNo;
            //}
            var mResponce = await _service.CreatePaymentSourceForCredit(new StripePaymentSourceRequestModel
            {
                chargeAmount = Totalamount,
                stripeCustomerId = UserDetail.StripeAccountId,
                CustomerEmail = UserDetail.Email,
                CustomerName = UserDetail.Name,
                stripeType = StripeType,
                //invoiceNo = strInvoiceNo
            });
            VerifyObjectModel objResponce = new VerifyObjectModel();
            if (mResponce.Status == ResponseStatus.Success)
            {
                await _general.AddStylistCreditTransaction(new StylistCreditTransactionModel()
                {
                    StylistId = UserId,
                    PaymentMothod = strPaymentMothod,
                    Credit = IntCredit,
                    //invoicefilename = xeroResult.Result.ToString()
                });

                ViewBag.status = ResponseStatus.Success;
                intAmount = Totalamount;
                return Redirect(mResponce.Result.Redirect.Url);
            }
            else
            {
                ViewBag.status = ResponseStatus.Failed;
                ViewBag.successmessage = mResponce.Message;
            }
            TempData["status"] = ViewBag.status;
            TempData["successmessage"] = ViewBag.successmessage;
            return RedirectToRoute("Stylist", new { username = User.Identity.GetUserName(), action = "UpdateToCredit" });
        }

        public async Task<ActionResult> PaymentSuccess(int amount, string stripetype, string client_secret, string source, bool livemode = false)
        {
            //string strXeroReceiptTitle = "";
            var mResponce = await _service.CreatePaymentCharge(new StripePaymentChargeResultModel
            {
                chargeAmount = amount,
                stripePaymentChargeToken = source,
            });

            if (mResponce.Status == ResponseStatus.Success)
            {
                var responceStatus = Utility.getChargeStaus(mResponce.Result.Status);
                var UserId = User.Identity.GetUserId();
                string strPaymentMothod = stripetype;
                
                var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                await _general.UpdateStylistPremiumTransaction(new StylistPremiumTransactionModel()
                {
                    StylistId = UserId,
                    PaymentMothod = strPaymentMothod,
                    month = intMonth,
                    //receiptfilename = xeroResult.Result.ToString(),
                    stripeResponce = JsonResult,
                    paymentStatus = responceStatus,
                    transactionId = mResponce.Result.Id
                });
                ViewBag.status = ResponseStatus.Success;
                ViewBag.successmessage = Resource.paymentSuccessMsg2;
                //}

            }
            else
            {
                ViewBag.status = ResponseStatus.Failed;
                ViewBag.successmessage = Resource.PaymentFailedDetail;
            }
            TempData["successmessage"] = ViewBag.successmessage;
            TempData["status"] = ViewBag.status;
            return RedirectToRoute("Stylist", new { username = User.Identity.GetUserName(), action = "UpdateToPremium" });
        }

        public ActionResult RenderCheckout(UpgradeAccountModel model)
        {
            intAmount = (int)model.Amount;
            //intAmount = (int)model.Amount * model.Month;
            intMonth = model.Month;
            //if (string.IsNullOrWhiteSpace(model.InvoiceNo))
            //model.InvoiceNo = await sentInvoice(intAmount, model.Month, model.UserEmail, string.Empty);
            return PartialView("_StripeCheckout", model);
        }

        public ActionResult RenderCheckoutForCredit(UpgradeCreditModel model)
        {
            intAmount = (int)model.Amount;
            IntCredit = model.Credit;            
            return PartialView("_StripeCheckoutForCredit", model);
        }


        //For Credit
        [HttpPost]
        public async Task<ActionResult> UpgradeCredit(string stripeToken, string stripeEmail, string UpgradeCreditId, bool blnPayInInstallment = false)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var UserId = User.Identity.GetUserId();

                var Id = Convert.ToInt32(UpgradeCreditId);
                var objData = _general.GetUpgradeCreditDetailById(Id);
                var StripeAccoutId = await _userservice.GetStripeAccoutId(UserId);

                intAmount = (int)objData.Amount;
                IntCredit = objData.Credit;

                var mResponce = await _service.CapturePaymentCharge(new StripePaymentChargeRequestModel
                {
                    chargeAmount = intAmount,
                    stripeCustomerId = StripeAccoutId,
                    stripePaymentToken = stripeToken
                });
                if (mResponce.Status == ResponseStatus.Success)
                {

                    await _general.AddStylistCreditTransaction(new StylistCreditTransactionModel()
                    {
                        StylistId = UserId,
                        PaymentMothod = strPaymentMothod,
                        Credit = IntCredit,
                        //invoicefilename = xeroResult.Result.ToString()
                    });
                    //string strXeroReceiptTitle = "";
                    //if (!String.IsNullOrWhiteSpace(strInvoiceNo))
                    //{
                    //var taxamount = Math.Round(((intAmount * 0.833)), 2);
                    //var FinalAmount = Math.Round(intAmount - taxamount, 2);
                    //strXeroReceiptTitle = XeroInvoice.FindByInvoiceAndMarkAsPaid(strInvoiceNo);
                    //****************************Xero Reciept*****************************//
                    //XeroReceiptSendModel xeroReceiptSendModel = new XeroReceiptSendModel()
                    //{
                    //    Amount = Convert.ToDouble(intAmount),
                    //    Title = strXeroReceiptTitle,
                    //    TokenID = mResponce.Result.stripePaymentChargeToken, //objResponce.pspReference,
                    //    Vat = FinalAmount,
                    //    userData = GetUserInfoForXeroReceipt(UserId),
                    //};

                    //ResponseModel<object> xeroResult = new ResponseModel<object>();
                    //xeroResult = await XeroReceipt.Add(xeroReceiptSendModel);
                    //if (xeroResult.Status == ResponseStatus.Failed)
                    //{
                    //    ViewBag.status = ResponseStatus.Failed;
                    //    //ViewBag.successmessage = "error:" + xeroResult.Message.ToString();
                    //    ViewBag.successmessage = Resource.paymentFailedMessage;
                    //}
                    //else
                    //{

                    //insert stylist primum transaction
                    var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                    await _general.UpdateStylistCreditTransaction(new StylistCreditTransactionModel()
                    {
                        StylistId = UserId,
                        PaymentMothod = strPaymentMothod,
                        Credit = IntCredit,
                        //receiptfilename = xeroResult.Result.ToString(),
                        stripeResponce = JsonResult,
                        paymentStatus = ChargeStatus.succeeded,
                        transactionId = mResponce.Result.stripePaymentChargeToken
                    });

                    ViewBag.status = ResponseStatus.Success;
                    ViewBag.successmessage = Resource.paymentSuccessMsg2;
                    //}
                    //}
                }
                else
                {
                    ViewBag.status = ResponseStatus.Failed;
                    ViewBag.successmessage = Resource.PaymentFailedDetail;
                }
            }
            catch (Exception ex)
            {
                //ViewBag.successmessage = "error:" + ex.Message.ToString();
                ViewBag.successmessage = Resource.PaymentFailedDetail;
                ViewBag.status = ResponseStatus.Failed;

            }
            mResult.Message = ViewBag.successmessage;
            TempData["status"] = ViewBag.status;
            TempData["successmessage"] = ViewBag.successmessage;
            return RedirectToRoute("Stylist", new { username = User.Identity.GetUserName(), action = "UpdateToCredit" });
        }

        public async Task<ActionResult> CreditPaymentSuccess(int amount, string stripetype, string client_secret, string source, bool livemode = false)
        {
            //string strXeroReceiptTitle = "";
            var mResponce = await _service.CreatePaymentChargeForCredit(new StripePaymentChargeResultModel
            {
                chargeAmount = amount,
                stripePaymentChargeToken = source,
            });
            if (mResponce.Status == ResponseStatus.Success)
            {
                //if (!String.IsNullOrWhiteSpace(invoiceno))
                //{
                var responceStatus = Utility.getChargeStaus(mResponce.Result.Status);
                var UserId = User.Identity.GetUserId();
                string strPaymentMothod = stripetype;
                //strXeroReceiptTitle = XeroInvoice.FindByInvoiceAndMarkAsPaid(invoiceno);

                var taxamount = Math.Round(((intAmount * 0.833)), 2);
                var FinalAmount = Math.Round(intAmount - taxamount, 2);
                //****************************Xero Receipt*****************************//
                //XeroReceiptSendModel xeroReceiptSendModel = new XeroReceiptSendModel()
                //{
                //    Amount = Convert.ToDouble(intAmount),
                //    Title = strXeroReceiptTitle,
                //    ReceiptID = "",
                //    Vat = FinalAmount,
                //    userData = GetUserInfoForXeroReceipt(UserId)
                //};

                //ResponseModel<object> xeroResult = new ResponseModel<object>();
                //xeroResult = await XeroReceipt.Add(xeroReceiptSendModel);
                //if (xeroResult.Status == ResponseStatus.Failed)
                //{
                //    //ViewBag.successmessage = "error:" + xeroResult.Message.ToString();
                //    ViewBag.successmessage = Resource.PaymentFailedDetail;
                //}
                //else
                //{
                var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                await _general.UpdateStylistCreditTransaction(new StylistCreditTransactionModel()
                {
                    StylistId = UserId,
                    PaymentMothod = strPaymentMothod,
                    Credit = IntCredit,
                    //receiptfilename = xeroResult.Result.ToString(),
                    stripeResponce = JsonResult,
                    paymentStatus = ChargeStatus.succeeded,
                    transactionId = mResponce.Result.Id
                });
                ViewBag.status = ResponseStatus.Success;
                ViewBag.successmessage = Resource.paymentSuccessMsg2;
                //}
                //}

            }
            else
            {
                ViewBag.status = ResponseStatus.Failed;
                ViewBag.successmessage = Resource.PaymentFailedDetail;
            }
            TempData["successmessage"] = ViewBag.successmessage;
            TempData["status"] = ViewBag.status;
            return RedirectToRoute("Stylist", new { username = User.Identity.GetUserName(), action = "UpdateToCredit" });
        }
    }
}