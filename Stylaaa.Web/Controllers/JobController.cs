﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using Stylaaa.Web.Filter;
using System.IO;
using System.Threading.Tasks;

namespace Stylaaa.Web.Controllers
{
    [CompleteProfileSetup(Role.Stylist, Role.User)]
    public class JobController : BaseController
    {
        private JobServices _service;
        private UserService _user;
        private CategoryService _catService;
        private GeneralServices _general;
        private StylishServices _stylist;

        public JobController()
        {
            _stylist = new StylishServices();
            _general = new GeneralServices();
            _catService = new CategoryService();
            _service = new JobServices();
            _user = new UserService();
        }

        public ActionResult AddJob()
        {
            string UserId = User.Identity.GetUserId();
            AddJobModel model = new AddJobModel() { JobCategoryList = _catService.GetJobCategoryList(_user.GetLanguage(UserId)) };
            return PartialView("_AddEditJob", model);
        }

        [HttpPost]
        public async Task<JsonResult> AddEditJob(AddJobModel model, FormCollection frm)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                //if(string.IsNullOrEmpty(model.HiddenJobFileName))
                //{
                //    model.HiddenJobFileName = "DefaultJob.jpg";
                //}
                if (!string.IsNullOrWhiteSpace(model.HiddenJobFileName))
                {
                    model.JobFilesNamesArray = model.HiddenJobFileName.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).Select(x => x.ToString()).ToArray();
                }
                model.UserId = User.Identity.GetUserId();
                mResult = await _service.AddEditJob(model);
            }
            else
            {
                mResult.Message = "please fill the required data.";
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> EditJob(long id)
        {            
            AddJobModel model = new AddJobModel();
            string UserId = User.Identity.GetUserId();
            if (id > 0)
            {
                model = await _service.GetJobdetailEdit(_user.GetLanguage(UserId), id, UserId);
            }
            return PartialView("_AddEditJob", model);
        }

        [HttpPost]
        public JsonResult AddJobImage(FormCollection frm)
        {
            MediaResponseModel<object> mResult = new MediaResponseModel<object>();
            var userid = User.Identity.GetUserId();

            string pngimageData = frm["pngimageData"].ToString();
            string strFileName = frm["filename"].ToString();
            string strExistFileName = frm["existFileName"].ToString();
            string shortGuid = ShortGuid.NewGuid().ToString();
            if (!String.IsNullOrWhiteSpace(strExistFileName))
            {
                string strExistFilePath = GlobalConfig.JobImagePath + strExistFileName;
                if (System.IO.File.Exists(strExistFilePath))
                {
                    System.IO.File.Delete(strExistFilePath);
                }
            }
            strFileName = userid + "_" + shortGuid + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetExtension(strFileName);
            string strFilePath = Path.Combine(GlobalConfig.JobImagePath, strFileName);

            string img = pngimageData.Replace("data:image/png;base64,", "").Replace(' ', '+');
            //byte[] decodedData = Convert.FromBase64String(img);
            //System.IO.File.WriteAllBytes(strFilePath, decodedData);

            Utility.SaveCropImage(img, strFilePath, Path.GetExtension(strFileName));

            mResult.FileName = strFileName;
            mResult.MediaPath = GlobalConfig.JobImageUrl + strFileName;

            mResult.Result = ResponseStatus.Success;
            mResult.Message = "Success";

            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AddJobMedia(IEnumerable<HttpPostedFileBase> uploadedFile)
        {
            MediaResponseModel<object> mResult = new MediaResponseModel<object>();
            string strFileName = "";
            var userid = User.Identity.GetUserId();

            try
            {
                foreach (HttpPostedFileBase fileMedia in uploadedFile)
                {
                    if (fileMedia != null && fileMedia.ContentLength > 0)
                    {
                        string shortGuid = ShortGuid.NewGuid().ToString();
                        strFileName = userid + "_" + shortGuid + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetExtension(fileMedia.FileName);

                        fileMedia.SaveAs(Path.Combine(GlobalConfig.JobImagePath, strFileName));

                        string[] mediaExtensions = { ".MP4", ".WMV" };
                        mResult.FileName += strFileName + ",";

                        if (IsMediaFile(mediaExtensions, strFileName))
                        {
                            mResult.MediaPath += GlobalConfig.JobImageUrl + strFileName + ",";
                        }
                        else
                        {
                            mResult.MediaPath += GlobalConfig.JobImageUrl + strFileName + ",";
                        }

                        mResult.Result = ResponseStatus.Success;
                        mResult.Message = "Success";
                    }
                    else
                    {
                        mResult.Result = ResponseStatus.Failed;
                        mResult.Message = "Failed";
                        mResult.Message = "please upload file";
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Result = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public static bool IsMediaFile(string[] mediaExtensions, string path)
        {
            return -1 != Array.IndexOf(mediaExtensions, Path.GetExtension(path).ToUpperInvariant());
        }

        [HttpPost]
        public JsonResult DeleteMedia()
        {
            MediaResponseModel<object> mResult = new MediaResponseModel<object>();
            string strFileName = Request.Form["filename"].ToString();
            string FilePath = GlobalConfig.JobImagePath + strFileName;
            if (System.IO.File.Exists(FilePath))
            {
                System.IO.File.Delete(FilePath);
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ViewAllJob()
        {
            string userid = User.Identity.GetUserId();
            List<ViewJobListModel> joblist = new List<ViewJobListModel>();
            jobFilterModel jobfilter = new jobFilterModel { UserId = userid, JobPageIndex = 1 };
            joblist = await _service.GetJobList(jobfilter);
            return PartialView("_JobList", joblist);
        }

        [HttpPost]
        public async Task<ActionResult> LoadMoreJob(jobFilterModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                model.UserId = UserId;
                ViewBag.PageIndex = 1;
                List<ViewJobListModel> data = await _service.GetJobList(model);
                if (model.JobPageIndex == 1)
                    model.JobPageIndex = 2;
                return PartialView("_JobList", data);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public async Task<JsonResult> DeleteJob(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string UserId = User.Identity.GetUserId();
            if (id > 0)
            {
                mResult = await _service.DeleteJob(id);
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public async Task<ActionResult> PartialJob(int intPageIndex, string username)
        {
            string UserId = _user.GetUserID(username);
            GetUserJobs getJobListModel = new GetUserJobs();
            getJobListModel.RecentJobPageIndex = intPageIndex;
            getJobListModel = await _service.JoblistByUser(new PaginationModel { PageIndex = getJobListModel.RecentJobPageIndex, UserId = UserId });

            GetUserJobs totalrecentjob = await _service.JoblistByUser(new PaginationModel { PageIndex = 0, UserId = UserId });
            decimal totarecentjobCount = totalrecentjob.UserJobList.Count;

            getJobListModel.RecentJobPageCount = Math.Ceiling((decimal)(totarecentjobCount > 0 ? (totarecentjobCount / GlobalConfig.PageSize) : 0));

            return PartialView("_ProfileJob", getJobListModel);
        }

        public async Task<JsonResult> jobActiveToggle(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (id > 0)
            {
                mResult = await _service.JobActiveToggle(id);
                if(mResult.Status==ResponseStatus.Success)
                {
                    return Json(mResult, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "please select jobId";
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }
    }
}