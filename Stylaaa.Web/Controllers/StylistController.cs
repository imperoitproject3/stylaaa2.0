﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Services;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;

namespace Stylaaa.Web.Controllers
{
    public class StylistController : BaseController
    {
        private StylishServices _stylist;
        private CountryServices _countryService;
        private CoreUserServices _user;
        private UserService _userService;
        private CategoryService _category;
        private PostServices _postService;
        private GeneralServices _generalService;

        public Page Page { get; private set; }

        public StylistController()
        {
            _category = new CategoryService();
            _countryService = new CountryServices();
            _stylist = new StylishServices();
            _userService = new UserService();
            _user = new CoreUserServices();
            _postService = new PostServices();
            _generalService = new GeneralServices();
        }

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ViewStylists()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                StylishFilterPostModel filter = new StylishFilterPostModel { UserId = UserId, PageIndex = 1 };
                ViewData["StylishList"] = await _stylist.getAllStylish(filter);
                ViewBag.PageIndex = 1;
                StylishFilterPostModel model = new StylishFilterPostModel { PageIndex = 2 };
                return PartialView("_stylistList");
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public async Task<ActionResult> LoadMoreStylist(StylishFilterPostModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                model.UserId = UserId;
                ViewBag.PageIndex = 1;
                List<ViewAllStylishModel> data = await _stylist.getAllStylish(model);
                if (model.PageIndex == 1)
                    model.PageIndex = 2;
                return PartialView("_StylistList", data);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> Post()
        {

            string UserId = ViewBag.UserID;
            ViewBag.UserProfile = await _user.GetUserDetail(UserId);
            GetPostListByUser getPostListModel = new GetPostListByUser();

            getPostListModel = await _postService.PostListByUser(new PaginationModel { PageIndex = 1, UserId = UserId });

            GetPostListByUser totalrecentpost = await _postService.PostListByUser(new PaginationModel { PageIndex = 0, UserId = UserId });
            decimal totarecentpostCount = totalrecentpost.RecentPost.Count;

            getPostListModel.RecentPostPageIndex = 1;
            getPostListModel.RecentPostPageCount = Math.Ceiling((decimal)(totarecentpostCount > 0 ? (totarecentpostCount / GlobalConfig.PageSize) : 0));

            return View(getPostListModel);

        }

        [AllowAnonymous]
        public async Task<ActionResult> About()
        {
            string id = ViewBag.UserID;
            string strCurrentUser = User.Identity.GetUserId();
            if (User.Identity.GetUserId() == id)
            {
                bool IsProfileComplete = _userService.checkprofilesetup(id);
                ViewBag.isProfileCompleted = IsProfileComplete;
            }
            ViewBag.UserProfile = await _user.GetUserDetail(id);
            List<SelectListItem> dropDownList = (from x in _countryService.IQueryable_Country().ToList()
                                                 select new SelectListItem
                                                 {
                                                     Text = x.Name,
                                                     Value = x.CountryId.ToString()
                                                 }).ToList();
            ViewData["country"] = dropDownList;

            //string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name;
            ResponseModel<StylistAboutModel> mResult = new ResponseModel<StylistAboutModel>();
            mResult = await _stylist.GetStylistAbout(id, strCurrentUser);
            return View(mResult.Result);
        }

        [AllowAnonymous]
        [HttpPost]
        public JsonResult IsUserExists()
        {
            string UserName = Request.Form["strUserName"];
            //check if any of the UserName matches the UserName specified in the Parameter using the ANY extension method
            object data = _stylist.IsUserExists(UserName, User.Identity.GetUserName());
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> About(StylistAboutModel model)
        {
            ResponseModel<StylistAboutModel> mResult = new ResponseModel<StylistAboutModel>();
            if (User.Identity.IsAuthenticated)
            {
                mResult = await _stylist.StylistCompleteProfile(model);
            }
            if (mResult.Status == ResponseStatus.Success)
            {
                {
                    // get context of the authentication manager
                    var authenticationManager = HttpContext.GetOwinContext().Authentication;

                    // create a new identity from the old one
                    var identity = new ClaimsIdentity(User.Identity);
                    //identity.Name.Replace(identity.Name.ToString(), model.UserName);

                    // update claim value
                    identity.RemoveClaim(identity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"));
                    identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", model.UserName));

                    // tell the authentication manager to use this new identity
                    authenticationManager.AuthenticationResponseGrant =
                        new AuthenticationResponseGrant(
                            new ClaimsPrincipal(identity),
                            new AuthenticationProperties { IsPersistent = true }
                        );
                }

                TempData["alertModel"] = new AlertModel(AlertStatus.success, Resource.profilecompleted);
                List<SelectListItem> dropDownList = (from x in _countryService.IQueryable_Country().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.CountryId.ToString()
                                                     }).ToList();
                ViewData["country"] = dropDownList;

                ViewBag.UserProfile = await _user.GetUserDetail(mResult.Result.Id);
                //return PartialView("_StylistAbout", mResult.Result);
                return View(mResult.Result);
            }
            //return PartialView("_StylistAbout", mResult.Result);
            return View(mResult.Result);
        }

        public async Task<ActionResult> Following()
        {
            if (User.Identity.IsAuthenticated)
            {
                string id = ViewBag.UserID;
                ResponseModel<UserFollowTransactionModel> mResult = new ResponseModel<UserFollowTransactionModel>();
                if (ModelState.IsValid)
                {
                    mResult = await _stylist.Followings(id, User.Identity.GetUserId());
                    if (mResult.Status == ResponseStatus.Success)
                    {
                        ViewBag.action = Resource.following;
                        ViewBag.UserProfile = mResult.Result.User;
                        return View(mResult.Result);
                    }
                }
                return RedirectToAction("About", new { id = id });
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public async Task<ActionResult> Followers()
        {
            if (User.Identity.IsAuthenticated)
            {
                string id = ViewBag.UserID;
                ResponseModel<UserFollowTransactionModel> mResult = new ResponseModel<UserFollowTransactionModel>();
                if (ModelState.IsValid)
                {
                    mResult = await _stylist.Followers(id, User.Identity.GetUserId());
                    if (mResult.Status == ResponseStatus.Success)
                    {
                        ViewBag.action = Resource.followers;
                        ViewBag.UserProfile = mResult.Result.User;
                        return View("Following", mResult.Result);
                    }
                }
                return RedirectToAction("About", new { id = id });
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public async Task<ActionResult> MyReview()
        {
            string id = ViewBag.UserID;
            ResponseModel<UserReviewModel> mResult = new ResponseModel<UserReviewModel>();
            string UserId = string.Empty;
            if (User.Identity.IsAuthenticated)
                UserId = User.Identity.GetUserId();
            mResult = await _stylist.MyReview(id, UserId);
            if (mResult.Status == ResponseStatus.Success)
            {
                ViewBag.action = Resource.myReview;
                ViewBag.UserProfile = mResult.Result.Stylish;
                return View(mResult.Result);
            }
            return RedirectToAction("About", new { id = id });
        }

        public async Task<ActionResult> StylistReview()
        {
            if (User.Identity.IsAuthenticated)
            {
                string id = ViewBag.UserID;
                ResponseModel<UserReviewModel> mResult = new ResponseModel<UserReviewModel>();
                string UserId = string.Empty;
                if (User.Identity.IsAuthenticated)
                    UserId = User.Identity.GetUserId();
                mResult = await _stylist.MyReview(id, UserId);
                if (mResult.Status == ResponseStatus.Success)
                {
                    ViewBag.action = Resource.review;
                    ViewBag.UserProfile = mResult.Result.Stylish;
                    return View("MyReview", mResult.Result);
                };
                return RedirectToAction("About", new { Id = id });
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public async Task<JsonResult> ReplyToReviw()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            ReplyToReviewModel model = new ReplyToReviewModel();
            model.ReviewId = Convert.ToInt64(Request.Form["ReviewId"]);
            model.ReviewText = Request.Form["ReviewText"].ToString();
            mResult = await _stylist.AddReply(model);
            if (mResult.Status == ResponseStatus.Success)
            {
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteReplyReview()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            long replyid = Convert.ToInt64(Request.Form["replyid"]);
            mResult = await _stylist.DeleteReply(replyid);
            if (mResult.Status == ResponseStatus.Success)
            {
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> AddToSpam()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string Userid = User.Identity.GetUserId();
            AddToSpamModel model = new AddToSpamModel();
            model.UserId = Userid;
            model.ReviewId = Convert.ToInt64(Request.Form["ReviewId"]);
            model.SpamReasonTxt = Request.Form["SpamReasonTxt"].ToString();
            mResult = await _stylist.SpamReview(model);
            if (mResult.Status == ResponseStatus.Success)
            {
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<JsonResult> DeleteReview(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (id > 0)
            {
                mResult = await _stylist.DeleteReviewById(id);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return Json(mResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(mResult, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                mResult.Message = "Review Not found";
                mResult.Status = ResponseStatus.Failed;
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public async Task<ActionResult> EditRateStylist()
        {
            long ReviewId = Convert.ToInt64(Request.Form["ReviewId"].ToString());
            AddUserReviewModel model = await _stylist.GetReview(ReviewId);
            return PartialView("_ReviewPopupChanges", model);
        }

        [HttpPost]
        public async Task<JsonResult> RateStylist(AddUserReviewModel model)
        {
            ResponseModel<UserReviewModel> mResult = new ResponseModel<UserReviewModel>();
            if (User.Identity.IsAuthenticated && ModelState.IsValid)
            {
                model.UserId = User.Identity.GetUserId();

                mResult = await _stylist.AddEditReview(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    return Json(mResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(mResult, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                var objMyReview = await _stylist.MyReview(model.StylishId, model.UserId);
                mResult.Result = objMyReview.Result;
                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> UpdateToPremium()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                ViewBag.UserProfile = await _user.GetUserDetail(UserId);
                ViewBag.UserEmail = _userService.GetEmail(UserId);
                List<UpgradeAccountModel> model = await _generalService.GetUpgradeAccountMasterDetail(0);

                //string strInvoiceNo = Request.QueryString["invoiceNo"];
                //if (!String.IsNullOrWhiteSpace(strInvoiceNo))
                //{
                //    string strMonth = XeroInvoice.FindByInvoiceNumber(strInvoiceNo);
                //    ViewBag.Month = strMonth;
                //    ViewBag.invoiceNo = strInvoiceNo;
                //}

                ViewBag.successmessage = TempData["successmessage"];
                ViewBag.status = TempData["status"];
                ViewBag.IsAbleToBuy = await _generalService.ISAbleToBuy(UserId);
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public async Task<ActionResult> UpgradeToPremium()
        {
            string UserId = User.Identity.GetUserId();
            ViewBag.UserProfile = await _user.GetUserDetail(UserId);
            ViewBag.UserEmail = _userService.GetEmail(UserId);
            List<UpgradeAccountModel> model = await _generalService.GetUpgradeAccountMasterDetail(0);
            return PartialView("_PremiumAccountpopup", model);
        }

        public async Task<ActionResult> JobCredit(string FrmHome)
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                ViewBag.UserProfile = await _user.GetUserDetail(UserId);
                ViewBag.UserEmail = _userService.GetEmail(UserId);

                GetStylistCreditHistoryList getStylistCreditModel = new GetStylistCreditHistoryList();
                getStylistCreditModel.RemainingCredit = _stylist.GetStylistCredit(UserId);
                getStylistCreditModel.CreditHistoryList = await _stylist.GetStylistCreditHistory(new PaginationModel { PageIndex = 1, UserId = UserId });
                getStylistCreditModel.CreditMasterDetail = await _generalService.GetUpgradeCreditMasterDetail(0);

                ViewBag.successmessage = TempData["successmessage"];
                ViewBag.status = TempData["status"];
                ViewBag.IsAbleToBuy = await _generalService.ISAbleToBuy(UserId);
                return View(getStylistCreditModel);

            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [AllowAnonymous]
        public async Task<ActionResult> UpdateToCredit()
        {
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                ViewBag.UserProfile = await _user.GetUserDetail(UserId);
                ViewBag.UserEmail = _userService.GetEmail(UserId);
                List<UpgradeCreditModel> model = await _generalService.GetUpgradeCreditMasterDetail(0);

                //string strInvoiceNo = Request.QueryString["invoiceNo"];
                //if (!String.IsNullOrWhiteSpace(strInvoiceNo))
                //{
                //    string strMonth = XeroInvoice.FindByInvoiceNumberForCredit(strInvoiceNo);
                //    ViewBag.Credit = strMonth;
                //    ViewBag.invoiceNo = strInvoiceNo;
                //}

                ViewBag.successmessage = TempData["successmessage"];
                ViewBag.status = TempData["status"];
                ViewBag.IsAbleToBuy = await _generalService.ISAbleToBuy(UserId);
                return View(model);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public async Task<ActionResult> UpgradeToCredit()
        {
            string UserId = User.Identity.GetUserId();
            ViewBag.UserProfile = await _user.GetUserDetail(UserId);
            ViewBag.UserEmail = _userService.GetEmail(UserId);
            List<UpgradeCreditModel> model = await _generalService.GetUpgradeCreditMasterDetail(0);
            return PartialView("_PremiumCreditPopup", model);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateProfileImage(FormCollection frm)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var userid = User.Identity.GetUserId();

            string pngimageData = frm["pngimageData"].ToString();
            string strFileName = frm["filename"].ToString();
            string shortGuid = ShortGuid.NewGuid().ToString();

            string img = pngimageData.Replace("data:image/png;base64,", "").Replace(' ', '+');
            byte[] decodedData = Convert.FromBase64String(img);

            strFileName = userid + "_" + shortGuid + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetExtension(strFileName);
            string strFilePath = Path.Combine(GlobalConfig.UserImagePath, strFileName);
            System.IO.File.WriteAllBytes(strFilePath, decodedData);
            mResult = await _userService.UpdateProfileImage(strFileName, userid);
            if (mResult.Status == ResponseStatus.Success)
            {
                mResult.Result = strFileName;
                mResult.Message = GlobalConfig.UserImageUrl + strFileName;
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> UpdateCoverPic(FormCollection frm)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var userid = User.Identity.GetUserId();

            string pngimageData = frm["pngimageData"].ToString();
            string strFileName = frm["filename"].ToString();
            string shortGuid = ShortGuid.NewGuid().ToString();

            string img = pngimageData.Replace("data:image/png;base64,", "").Replace(' ', '+');
            byte[] decodedData = Convert.FromBase64String(img);

            strFileName = userid + "_" + shortGuid + "_" + DateTime.Now.ToString("ddMMyyyyhhmmss") + Path.GetExtension(strFileName);
            string strFilePath = Path.Combine(GlobalConfig.UserCoverPicPath, strFileName);
            System.IO.File.WriteAllBytes(strFilePath, decodedData);
            mResult = await _userService.UpdateCoverPic(strFileName, userid);
            if (mResult.Status == ResponseStatus.Success)
            {
                mResult.Result = strFileName;
                mResult.Message = GlobalConfig.UserCoverPicUrl + strFileName;
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> PaymentHistory()
        {
            var userid = User.Identity.GetUserId();

            ViewBag.UserProfile = await _user.GetUserDetail(userid);
            GetStylistPaymenList getStylistPaymenModel = new GetStylistPaymenList();
            getStylistPaymenModel.StylistHistoryList = await _stylist.GetStylistPremiumTransaction(new PaginationModel { PageIndex = 1, UserId = userid });

            List<StylistPaymentHistoryModel> totalStylistHistoryList = await _stylist.GetStylistPremiumTransaction(new PaginationModel { PageIndex = 0, UserId = userid });
            decimal totalStylistHistory = totalStylistHistoryList.Count;

            getStylistPaymenModel.StylistHistoryPageIndex = 1;
            getStylistPaymenModel.StylistHistoryPageCount = Math.Ceiling((decimal)(totalStylistHistory > 0 ? (totalStylistHistory / GlobalConfig.PageSize) : 0));

            return View(getStylistPaymenModel);
        }
        
        [HttpPost]
        public async Task<ActionResult> GetPaymentHistory()
        {
            var userid = User.Identity.GetUserId();

            ViewBag.UserProfile = await _user.GetUserDetail(userid);
            PaginationModel model = new PaginationModel();
            model.PageIndex = Convert.ToInt32(Request.Form["index"]);
            model.UserId = userid;
            List<StylistPaymentHistoryModel> objStylistHistory = await _stylist.GetStylistPremiumTransaction(model);
            return PartialView("_PaymentHistory", objStylistHistory);
        }
    }
}