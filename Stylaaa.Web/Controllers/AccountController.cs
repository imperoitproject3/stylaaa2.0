﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using Stylaaa.Web.Helper;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web;
using System.Collections.Generic;
using Microsoft.AspNet.Identity;
using System.IO;
using Stylaaa.Data.Repository;
using System.Web.Configuration;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text;
using System.Net;
using Stylaaa.Stripe;
using Stylaaa.Core.Models.StripeModels;
using System.Threading;

namespace Stylaaa.Web.Controllers
{
    public class AccountController : LanguageController
    {
        private CountryServices _countryService;
        private UserService _service;
        private CoreUserServices _coreUser;
        private GeneralServices _general;
        private BotsService _bot;
        private CategoryService _catService;
        private SubCategoriesService _subCat;
        private BookStylistService _booking;
        private stPaymentChargeService _stripe;

        public AccountController()
        {
            _countryService = new CountryServices();
            _service = new UserService();
            _coreUser = new CoreUserServices();
            _general = new GeneralServices();
            _bot = new BotsService();
            _catService = new CategoryService();
            _subCat = new SubCategoriesService();
            _booking = new BookStylistService();
            _stripe = new stPaymentChargeService();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Index()
        {
            if (TempData["Isbooking"] != null)
            {
                ViewBag.IsBookingConfirmed = TempData["Isbooking"];
                ViewData["SuccessBooking"] = TempData["BookingSuccess"] as BookingSuccessModel;
            }
            else
            {
                ViewData["SuccessBooking"] = null;
                ViewBag.IsBookingConfirmed = false;
            }


            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }

            ViewData["categories"] = _catService.GetCategroies(lng);
            ViewData["LandingStylist"] = await _booking.GetTopStylist();
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult SubCategoriesByCatId(string SelectedCatId, LandingGender GenderId)
        {
            List<long> catlist = SelectedCatId.Split(',').Select(Int64.Parse).ToList();
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }
            //ViewData["SubCategory"] = _subCat.GetSubCategroies(languageId, CatId);
            List<SubCategoryDropdownModel> model = _subCat.GetSubCategroies(lng, catlist, GenderId);
            List<SelectListItem> dropDownList = (from x in _subCat.GetSubCategroies(lng, catlist, GenderId)
                                                 select new SelectListItem
                                                 {
                                                     Text = x.Name,
                                                     Value = x.Id.ToString()
                                                 }).ToList();
            return PartialView("_SubCatDropDown", dropDownList);
        }

        [AllowAnonymous]
        [HttpPost]
        public ActionResult CategoriesByGender(LandingGender GenId)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }

            IQueryable<CategoryDropdownModel> model = _catService.GetCategroiesByGender(lng, GenId);
            return PartialView("_CategoryDropdown", model);
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<double> GetPriceBySubCategory(string SubCatId)
        {
            List<long> subcatlist = SubCatId.Split(',').Select(Int64.Parse).ToList();
            ResponseModel<object> mResult = new ResponseModel<object>();
            double amount = await _subCat.PriceBySubCategory(subcatlist);
            //double data = mResult.Result;
            return amount;
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<JsonResult> GetPriceListBySubCategory(string SubCatIds)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            List<long> subcatlist = SubCatIds.Split(',').Select(Int64.Parse).ToList();
            List<SubcategoryList> list = await _subCat.PriceListBySubCategory(subcatlist);
            mResult.Result = list;
            return Json(mResult.Result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> BookStylist(string stripeToken, LandingPageModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            BookingSuccessModel book = new BookingSuccessModel();
            TempData["BookingModel"] = model;
            //if (model.IsDiscount)
            //{
            //    double dis = 10 * model.Price / 100;
            //    model.Price = model.Price - dis;
            //}

            try
            {
                if (model.Privacy == true)
                {
                    if (ModelState.IsValid && model.AcceptTerms == true)
                    {
                        if (model.PaymentType == StripeType.none)
                        {
                            mResult = await _booking.AddBookStylist(model);
                            if (mResult.Status == ResponseStatus.Success)
                            {
                                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                                TempData["Isbooking"] = true;
                                
                                book.Date = model.DateTime.Date.ToString("d");
                                book.Time = model.DateTime.ToShortTimeString();
                                book.Gender = model.Gender.ToString();
                                TempData["BookingSuccess"] = book;

                            }
                        }
                        else if (model.PaymentType == StripeType.card)
                        {
                            int intAmount = (int)model.TotalPrice;

                            var mResponce = await _stripe.CapturePaymentCharge(new StripePaymentChargeRequestModel
                            {
                                chargeAmount = intAmount,
                                //stripeCustomerId = StripeAccoutId,
                                stripePaymentToken = stripeToken,
                                receiptEmail = model.Email
                            });
                            if (mResponce.Status == ResponseStatus.Success)
                            {
                                var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                                var transactionId = mResponce.Result.stripePaymentChargeToken;
                                var paymentStatus = ChargeStatus.succeeded;
                                string striperesponse = JsonResult;
                                mResult = await _booking.AddBookStylistWithPayment(model, transactionId, paymentStatus, striperesponse);
                                if (mResult.Status == ResponseStatus.Success)
                                {
                                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                                    TempData["Isbooking"] = true;

                                    book.Date = model.DateTime.Date.ToString("d");
                                    book.Time = model.DateTime.ToShortTimeString();
                                    book.Gender = model.Gender.ToString();
                                    TempData["BookingSuccess"] = book;
                                }
                            }
                        }
                        else
                        {
                            int intAmount = (int)model.TotalPrice;

                            var mResponce = await _stripe.CreatePaymentSourceForBooking(new StripePaymentSourceRequestModel
                            {
                                chargeAmount = intAmount,
                                //stripeCustomerId = UserDetail.StripeAccountId,
                                CustomerEmail = model.Email,
                                CustomerName = model.Email,
                                stripeType = model.PaymentType
                            });

                            if (mResponce.Status == ResponseStatus.Success)
                            {

                                var responceStatus = Utility.getChargeStaus(mResponce.Result.Status);
                                var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                                var transactionId = mResponce.Result.Id;
                                var paymentStatus = responceStatus;
                                string striperesponse = JsonResult;
                                if (paymentStatus == ChargeStatus.pending || paymentStatus == ChargeStatus.succeeded || paymentStatus == ChargeStatus.captured)
                                {
                                    return Redirect(mResponce.Result.Redirect.Url);
                                    //mResult = await _booking.AddBookStylistWithPayment(model, transactionId, paymentStatus, striperesponse);
                                }
                                else
                                {
                                    mResult.Status = ResponseStatus.Failed;
                                    mResult.Message = "failed";
                                }

                                //if (mResult.Status == ResponseStatus.Success)
                                //{


                                //}
                                //else
                                //{
                                //    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                                //}
                            }
                            else
                            {
                                TempData["alertModel"] = new AlertModel(mResponce.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResponce.Message);
                            }
                        }
                    }
                    else
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Resource.registerAcceptTermsValid);
                    }
                }
                else
                {
                    TempData["alertModel"] = new AlertModel(AlertStatus.danger, Resource.registerAcceptPrivacyValid);
                }
            }
            catch (Exception ex) // catches all exceptions
            {
                throw ex;
            }

            return RedirectToAction("Index", "Account");
        }

        [AllowAnonymous]
        public async Task<ActionResult> BookingPaymentSuccess(int amount, string stripetype, string client_secret, string source, bool livemode = false)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            BookingSuccessModel book = new BookingSuccessModel();

            LandingPageModel model = TempData["BookingModel"] as LandingPageModel;


            var mResponce = await _stripe.CreatePaymentChargeForBooking(new StripePaymentChargeResultModel
            {
                chargeAmount = amount,
                stripePaymentChargeToken = source,
            });

            if (mResponce.Status == ResponseStatus.Success)
            {
                if (model != null)
                {
                    var responceStatus = Utility.getChargeStaus(mResponce.Result.Status);
                    var JsonResult = JsonConvert.SerializeObject(mResponce.Result);
                    var transactionId = mResponce.Result.Id;
                    var paymentStatus = responceStatus;
                    string striperesponse = JsonResult;
                    mResult = await _booking.AddBookStylistWithPayment(model, transactionId, paymentStatus, striperesponse);
                }
                TempData["alertModel"] = new AlertModel(mResponce.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResponce.Message);
                TempData["Isbooking"] = true;

                book.Date = model.DateTime.Date.ToString("d");
                book.Time = model.DateTime.ToShortTimeString();
                book.Gender = model.Gender.ToString();
                TempData["BookingSuccess"] = book;
            }
            else
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.danger, mResponce.Message);
                ViewBag.status = ResponseStatus.Failed;
                ViewBag.successmessage = Resource.PaymentFailedDetail;
            }
            return RedirectToAction("Index", "Account");
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            ViewBag.UserRole = Role.User;
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> ChangeLanguage(string lang, string redirecturl)
        {
            new LanguageManager().SetLanguage(lang);
            languageType language = languageType.English;
            if (lang.ToLower() == "de")
            {
                language = languageType.German;
            }
            await _service.SetUserLanguage(language, User.Identity.GetUserId());
            return Redirect(redirecturl);
        }

        [AllowAnonymous]
        public ActionResult Register()
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                List<SelectListItem> dropDownList = (from x in _countryService.IQueryable_Country().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.CountryId.ToString()
                                                     }).ToList();
                ViewData["country"] = dropDownList;
                ViewBag.UserRole = RouteData.Values["usertype"]?.ToString();
                return View();
            }
        }


        [AllowAnonymous]
        [HttpPost]
        public JsonResult IsEmailExists()
        {
            string email = Request.Form["strEmail"];
            //check if any of the UserName matches the UserName specified in the Parameter using the ANY extension method
            object data = _service.IsEmailExists(email);
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> Register(UserRegisterModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());

                ResponseModel<UserResponseModel> mResult = await _service.Register(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                    return RedirectToAction("Login");
                }
                else
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            }
            //ViewBag.UserRole = RouteData.Values["usertype"]?.ToString();
            List<SelectListItem> dropDownList = (from x in _countryService.IQueryable_Country().ToList()
                                                 select new SelectListItem
                                                 {
                                                     Text = x.Name,
                                                     Value = x.CountryId.ToString()
                                                 }).ToList();
            ViewData["country"] = dropDownList;
            return View(model);
        }


        [AllowAnonymous]
        public ActionResult Activate(string UserId)
        {
            ResponseModel<bool> mResult = _service.ActivateAccount(UserId);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            //ViewBag.UserRole = RouteData.Values["usertype"]?.ToString();
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Login(UserLoginModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<UserResponseModel> mResult = await _service.Login(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    if (mResult.Result.IsActive)
                    {
                        TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ViewBag.userId = mResult.Result.UserId;
                    if (ViewBag.userId != null)
                    {
                        ViewBag.IsActive = mResult.Result.IsActive;
                    }
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                    ModelState.AddModelError("", mResult.Message);
                }

            }
            return View("Login", model);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<JsonResult> ForgotPassword(UserForgotPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                _service = new UserService();
                mResult = await _service.ForgotPassword(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    ModelState.Clear();
                    //TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                }
                //else
                //    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = Resource.emailvalid;
            }

            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ResetPassword(string UserId)
        {
            ViewBag.userid = UserId;
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> SetPassword(ResetPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.SetPassword(model);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Login");
        }

        [HttpGet]
        public async Task<ActionResult> UserEditPassword()
        {
            string id = User.Identity.GetUserId();
            ResponseModel<UserEditPasswordModel> mResult = new ResponseModel<UserEditPasswordModel>();
            mResult = await _service.UserEditPassword(id);
            //TempData["alertModel"] = string.Empty;
            if (mResult.Status == ResponseStatus.Success)
            {
                ViewBag.UserProfile = mResult.Result.User;
                return View(mResult.Result);
            }

            CategoryService _catService = new CategoryService();
            UserService _user = new UserService();
            ViewData["CategoryList"] = _catService.GetPostCategoryList(_user.GetLanguage(id));

            return View(mResult.Result);
        }

        [HttpPost]
        public async Task<JsonResult> UpdatePassword(UserEditPasswordModel model)
        {
            ResponseModel<UserEditPasswordModel> mResult = new ResponseModel<UserEditPasswordModel>();
            string UserId = User.Identity.GetUserId();
            if (ModelState.IsValid)
            {
                if (model.oldPassWord != model.ConfirmPassword)
                {
                    mResult = await _service.UserUpdatePassword(model, UserId);
                    if (mResult.Status == ResponseStatus.Success)
                        return Json(mResult, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "old password same as new password please try again";
                    return Json(mResult, JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                mResult.Message = "please enter valid data";
                mResult.Status = ResponseStatus.Failed;
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> ChangeCoverpic()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string FileName = "";
            var userid = User.Identity.GetUserId();
            #region Upload certificate
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                //get filename
                string ogFileName = file.FileName.Trim('\"');
                string shortGuid = ShortGuid.NewGuid().ToString();
                var thisFileName = userid + "_" + shortGuid + Path.GetExtension(ogFileName);

                file.SaveAs(Path.Combine(GlobalConfig.UserCoverPicPath, thisFileName));
                FileName = thisFileName;
            }
            else
            {
                mResult.Message = "please upload file";
            }
            #endregion
            mResult = await _service.AddCoverPic(FileName, userid);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public async Task<JsonResult> DeactiveAccount(string id)
        {
            ResponseModel<bool> mResult = await _service.DeactiveAccount(id);
            if (mResult.Status == ResponseStatus.Success)
            {
                _service = new UserService(Request.GetOwinContext());
                _service.WebLogout();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = Resource.DeactiveAccountSuccess;

                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                return Json(mResult, JsonRequestBehavior.AllowGet);
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> ActiveAccount(string id)
        {
            ResponseModel<bool> mResult = await _service.ActiveAccount(id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Login");
        }

        [AllowAnonymous]
        [HttpGet]
        public ActionResult LogOut()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (!User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Account");
            _service = new UserService(Request.GetOwinContext());
            _service.WebLogout();
            mResult.Status = ResponseStatus.Success;
            mResult.Message = Resource.logoutSuccess;
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Index", "Account");
        }
    }
}