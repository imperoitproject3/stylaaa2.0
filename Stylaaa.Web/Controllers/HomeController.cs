﻿using DataTables.Mvc;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using Stylaaa.Web.Filter;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    [CompleteProfileSetup(Role.Stylist, Role.User)]
    public class HomeController : BaseController
    {
        private PostServices _service;
        private UserService _user;
        private CategoryService _catService;
        private GeneralServices _general;
        private StylishServices _stylist;
        private JobServices _job;

        public HomeController()
        {
            _job = new JobServices();
            _stylist = new StylishServices();
            _general = new GeneralServices();
            _catService = new CategoryService();
            _service = new PostServices();
            _user = new UserService();
        }

        [AllowAnonymous]
        public async Task<ActionResult> TermsConditions()
        {
            string userId = User.Identity.GetUserId();
            languageType languageId = languageType.German;
            if (!String.IsNullOrWhiteSpace(userId))
            {
                languageId = _user.GetLanguage(userId);
            }
            TermsModel model = await _general.GetTerms(languageId);
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> WhatIsStylaaa()
        {           
            string userId = User.Identity.GetUserId();
            languageType languageId = languageType.German;
            if (!String.IsNullOrWhiteSpace(userId))
            {
                languageId = _user.GetLanguage(userId);
            }
            WhatIsStylaaaModel model = await _general.GetWhatIsStylaaa(languageId);
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> AboutUs()
        {
            string UserId = User.Identity.GetUserId();
            languageType languageId = languageType.German;
            if (!String.IsNullOrWhiteSpace(UserId))
            {
                languageId = _user.GetLanguage(UserId);
            }
            AboutUsModel model = await _general.GetAboutUs(languageId);
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> PrivacyPolicy()
        {
            string UserId = User.Identity.GetUserId();
            languageType languageId = languageType.German;
            if (!String.IsNullOrWhiteSpace(UserId))
            {
                languageId = _user.GetLanguage(UserId);
            }
            PrivacyPolicyModel model = await _general.GetPrivacyPolicy(languageId);
            return View(model);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Imprint()
        {
            //ImprintsModel model = await _general.GetImprints();
            //return View(model);
            string UserId = User.Identity.GetUserId();
            languageType languageId = languageType.German;
            if (!String.IsNullOrWhiteSpace(UserId))
            {
                languageId = _user.GetLanguage(UserId);
            }
            ImprintsModel model = await _general.GetImprints(languageId);
            return View(model);
        }

        [AllowAnonymous]
        public ActionResult ContactUs()
        {
            ContactUsModel model = new ContactUsModel();
            if (User.Identity.IsAuthenticated)
            {
                model.Name = _user.GetName(User.Identity.GetUserId());
                model.Email = _user.GetEmail(User.Identity.GetUserId());
            }
            return View(model);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> ContactUs(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                mResult = await _general.AddContactUs(model);

                if (mResult.Status == ResponseStatus.Success)
                {
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                }
                else
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            }
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> Index(long postid = 0, string hashtag = "")
        {
            List<string> lstHashtag = new List<string>(1);
            lstHashtag.Add(hashtag);
            if (!User.Identity.IsAuthenticated && postid > 0)
            {
                GetPostViewListModel getPostViewListModel = new GetPostViewListModel();

                getPostViewListModel.RecentPost = await _service.RecentPost(new PaginationModel { PageIndex = 1, UserId = "" }, lstHashtag, postid);
                getPostViewListModel.PopularPost = await _service.PopularPost(new PaginationModel { PageIndex = 1, UserId = "" }, lstHashtag, postid);
                //getPostViewListModel.CategoryList = _catService.GetPostCategoryList(_user.GetLanguage(UserId));

                getPostViewListModel.metaTitle = "Post by " + getPostViewListModel.RecentPost.Select(x => x.UserName).FirstOrDefault();
                getPostViewListModel.metaImageUrl = GlobalConfig.PostImageUrl + getPostViewListModel.RecentPost.Select(x => x.FileName).FirstOrDefault();
                getPostViewListModel.metaDescription = getPostViewListModel.RecentPost.Select(x => x.Description).FirstOrDefault();
                return View(getPostViewListModel);
            }
            else if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();

                bool IsProfileComplete = _user.checkprofilesetup(UserId);
                ViewBag.isProfileCompleted = IsProfileComplete;

                StylishFilterPostModel filter = new StylishFilterPostModel { UserId = UserId, PageIndex = 1 };
                ViewData["StylishList"] = await _stylist.getAllStylish(filter);
                ViewBag.PageIndex = 1;

                jobFilterModel jobfilter = new jobFilterModel { UserId = UserId, JobPageIndex = 1 };
                List<ViewJobListModel> joblist = await _job.GetJobList(jobfilter);
                ViewData["Joblist"] = joblist;
                ViewBag.jobcount = joblist.Count;
                ViewBag.Credit = ViewBag.MyCredit;

                int intPageIndex = 1;
                GetPostViewListModel getPostViewListModel = new GetPostViewListModel();

                getPostViewListModel.RecentPost = await _service.RecentPost(new PaginationModel { PageIndex = intPageIndex, UserId = UserId }, lstHashtag, postid);
                getPostViewListModel.PopularPost = await _service.PopularPost(new PaginationModel { PageIndex = intPageIndex, UserId = UserId }, lstHashtag, postid);
                getPostViewListModel.CategoryList = _catService.GetPostCategoryList(_user.GetLanguage(UserId));

                GetjobViewListModel Jobmodeldata = new GetjobViewListModel();
                var categorylist = _catService.GetJobCategoryList(_user.GetLanguage(UserId));
                Jobmodeldata.CategoryList = categorylist;
                getPostViewListModel.JobModel = Jobmodeldata;

                //ViewData["CategoryList"] = getPostViewListModel.CategoryList;
                getPostViewListModel.RecentPostPageIndex = intPageIndex;
                getPostViewListModel.PopularPostPageIndex = intPageIndex;

                List<GetPostModelRecent> totalrecentpost = await _service.RecentPost(new PaginationModel { PageIndex = 0, UserId = UserId }, lstHashtag, postid);
                List<GetPostModelPopular> totalpopularpost = await _service.PopularPost(new PaginationModel { PageIndex = 0, UserId = UserId }, lstHashtag, postid);
                decimal totarecentpostCount = totalrecentpost.Count;
                decimal totalpopularpostCount = totalpopularpost.Count;

                getPostViewListModel.RecentPostPageCount = Math.Ceiling((decimal)(totarecentpostCount > 0 ? (totarecentpostCount / GlobalConfig.PageSize) : 0));
                getPostViewListModel.PopularPostPageCount = Math.Ceiling((decimal)(totalpopularpostCount > 0 ? (totalpopularpostCount / GlobalConfig.PageSize) : 0));

                return View(getPostViewListModel);
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        [HttpPost]
        public JsonResult GetNotificationListData()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            int id = Convert.ToInt32(Request.Form["index"].ToString());
            bool isCount = Convert.ToBoolean(Request.Form["isCount"]);
            bool isData = Convert.ToBoolean(Request.Form["isData"]);

            string UserId = User.Identity.GetUserId();
            GetNotificationViewListModel model = new GetNotificationViewListModel();
            model.NotificationPageIndex = id;
            if (isData)
            {
                List<NotificationListModel> data = _general.GetNotificationList(new PaginationModel { PageIndex = id, UserId = UserId });
                //SenderLinkUrl

                List<NotificationListModel> result = data.Select(ntf => new NotificationListModel()
                {
                    Id = ntf.Id,
                    PostId = ntf.PostId,
                    SenderId = ntf.SenderId,
                    SenderName = ntf.SenderName,
                    SenderUserName = ntf.SenderUserName,
                    SenderRole = ntf.SenderRole,
                    SenderProfileImage = Convert.ToString(ntf.SenderProfileImage),
                    ReceiverId = ntf.ReceiverId,
                    PostImage = ntf.PostImage,
                    IsVideo = ntf.IsVideo,
                    IsFollow = ntf.IsFollow,
                    NotificationText = ntf.NotificationText,
                    NotificationTypeID = ntf.NotificationTypeID,
                    Rating = ntf.Rating,
                    AddedOn = ntf.AddedOn,
                    CommentedTimeSpan = ntf.CommentedTimeSpan,
                    SenderLinkUrl = Url.RouteUrl((ntf.SenderRole == Role.Stylist ? "Stylist" : "User"), new { username = ntf.SenderUserName, action = "About" }),
                    SenderChatLinkUrl = Url.RouteUrl("Chat", new { username = User.Identity.GetUserName(), action = "ChatJob" }),
                    SenderMobileLinkUrl = Url.RouteUrl("Chat", new { username = User.Identity.GetUserName(), action = "ChatJob" }),
                    SenderFollowLinkUrl = "'" + Url.Action("FollowTransaction", "Home", new { StylishId = ntf.SenderId }) + "'",
                    FollowText = (ntf.IsFollow ? Resource.following : Resource.follow),
                    FollowTextClass = (ntf.IsFollow ? "" : "defult"),
                    PostlinkUrl = Url.Action("Index", "Home", new { postid = ntf.PostId, hashtag = "" })
                }).ToList();

                model.NotificationList = result;
            }
            if (isCount)
            {
                List<NotificationListModel> totalnotificationlist = _general.GetNotificationList(new PaginationModel { PageIndex = 0, UserId = UserId });
                decimal totalnotificationCount = totalnotificationlist.Count;
                model.NotificationPageCount = Math.Ceiling((decimal)(totalnotificationCount > 0 ? (totalnotificationCount / 20) : 0)).ToString();
            }
            mResult.Result = model;
            //mResult.Result = JsonConvert.SerializeObject(model);
            return Json(mResult.Result, JsonRequestBehavior.AllowGet);
            //return PartialView("_NotificationList", model);
        }

        public async Task<JsonResult> ReadAllNotificationList()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string UserId = User.Identity.GetUserId();

            mResult = await _general.ReadAllNotificationList(UserId);
            return Json(mResult.Result, JsonRequestBehavior.AllowGet);
        }

        public async Task<JsonResult> FollowTransaction(string StylishId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (User.Identity.IsAuthenticated)
            {
                string UserId = User.Identity.GetUserId();
                mResult = await _general.followthis(StylishId, UserId);
            }
            else
            {
                mResult.Message = "Bitte einloggen!";
            }
            return Json(mResult.Result, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> ViewAllFollowings(string id)
        {
            ViewAllFollowModel model = new ViewAllFollowModel();
            ViewBag.action = Resource.following;
            string UserId = User.Identity.GetUserId();
            ViewBag.userId = id;
            int intPageIndex = 1;
            model.UsersList = await _general.ViewAllFollowings(new PaginationModel { PageIndex = intPageIndex, UserId = UserId, Id = id });
            List<UsersFollowModel> listCount = await _general.ViewAllFollowings(new PaginationModel { UserId = UserId, PageIndex = 0, Id = id });
            model.PageIndex = intPageIndex;

            decimal totalUser = listCount.Count;
            model.PageCount = Math.Ceiling((decimal)(totalUser > 0 ? (totalUser / GlobalConfig.PageSize) : 0));
            return View(model);
        }

        public async Task<ActionResult> ViewAllFollowers(string id)
        {
            ViewAllFollowModel model = new ViewAllFollowModel();
            ViewBag.action = Resource.followers;
            string UserId = User.Identity.GetUserId();
            ViewBag.userId = id;
            int intPageIndex = 1;
            model.UsersList = await _general.ViewAllFollowers(new PaginationModel { PageIndex = intPageIndex, UserId = UserId, Id = id });
            List<UsersFollowModel> listCount = await _general.ViewAllFollowers(new PaginationModel { UserId = UserId, PageIndex = 0, Id = id });
            model.PageIndex = intPageIndex;

            decimal totalUser = listCount.Count;
            model.PageCount = Math.Ceiling((decimal)(totalUser > 0 ? (totalUser / GlobalConfig.PageSize) : 0));
            return View("ViewAllFollowings", model);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> LoadMoreFollowing(int pageindex, string id)
        {
            ViewAllFollowModel FollowingModel = new ViewAllFollowModel();
            FollowingModel.PageIndex = pageindex;
            string UserId = User.Identity.GetUserId();
            ViewBag.PageIndex = 1;
            FollowingModel.UsersList = await _general.ViewAllFollowings(new PaginationModel { PageIndex = pageindex, UserId = UserId, Id = id });
            List<UsersFollowModel> listCount = await _general.ViewAllFollowings(new PaginationModel { UserId = UserId, PageIndex = 0, Id = id });
            decimal totalusercount = listCount.Count;

            FollowingModel.PageCount = Math.Ceiling((decimal)(totalusercount > 0 ? (totalusercount / GlobalConfig.PageSize) : 0));

            return PartialView("_ViewAll", FollowingModel);
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult> LoadMoreFollower(int pageindex, string id)
        {
            ViewAllFollowModel FollowerModel = new ViewAllFollowModel();
            FollowerModel.PageIndex = pageindex;
            string UserId = User.Identity.GetUserId();
            FollowerModel.UsersList = await _general.ViewAllFollowers(new PaginationModel { PageIndex = pageindex, UserId = UserId, Id = id });

            List<UsersFollowModel> listCount = await _general.ViewAllFollowers(new PaginationModel { UserId = UserId, PageIndex = 0, Id = id });

            decimal totalusercount = listCount.Count;

            FollowerModel.PageCount = Math.Ceiling((decimal)(totalusercount > 0 ? (totalusercount / GlobalConfig.PageSize) : 0));
            ViewBag.PageIndex = 1;
            return PartialView("_ViewAll", FollowerModel);
        }

        //[AllowAnonymous]
        //public ActionResult Datatable()
        //{
        //    return View();
        //}

        //[AllowAnonymous]
        //public async Task<JsonResult> DatatableLoad()
        //{
        //    string search = Request.Form.GetValues("search[value]")[0];
        //    var draw = Request.Form.GetValues("draw").FirstOrDefault();
        //    var start = Request.Form.GetValues("start").FirstOrDefault();
        //    var length = Request.Form.GetValues("length").FirstOrDefault();

        //    var sortColumn = Request.Form.GetValues("columns[" + Request.Form.GetValues("order[0][column]").FirstOrDefault() + "][name]").FirstOrDefault();
        //    var sortColumnDir = Request.Form.GetValues("order[0][dir]").FirstOrDefault();
        //    int totalRecords = 0;

        //    int pagesize = length != null ? Convert.ToInt32(length) : 0;
        //    int skip = start != null ? Convert.ToInt32(start) : 0;
        //    totalRecords = _general.UserCount();
           
        //    var data = await _general.UserList(new PageModel { skip = skip, pagesize = pagesize });

        //    if (!string.IsNullOrEmpty(search) && !string.IsNullOrWhiteSpace(search))
        //    {
        //        data = await _general.UserList();
        //        data = data.Where(p => p.Name.ToString().ToLower().Contains(search.ToLower()) ||
        //            p.UserName.ToLower().Contains(search.ToLower()) ||
        //            p.Email.ToString().ToLower().Contains(search.ToLower())).ToList();
        //    }
        //    if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
        //    {
        //        if (sortColumnDir == "asc")
        //        {
        //            data = data.OrderBy(x => x.Name).ToList();
        //        }
        //        else
        //        {
        //            data = data.OrderByDescending(x => x.Name).ToList();
        //        }
        //    }
        //    return Json(new { draw = draw, recordsFiltered = totalRecords, recordsTotal = totalRecords, data = data }, JsonRequestBehavior.AllowGet);
        //}

        //public JsonResult DataTableGet([ModelBinder(typeof(DataTablesBinder))] IDataTablesRequest requestModel)
        //{
           
        //    IQueryable<UserTableModel> query = _general.GetUserList();

        //    var totalCount = query.Count();

        //    // Apply filters
        //    if (requestModel.Search.Value != String.Empty)
        //    {
        //        var value = requestModel.Search.Value.Trim();
        //        query = query.Where(p => p.Name.Contains(value) || p.UserName.Contains(value));
        //    }

        //    var filteredCount = query.Count();

        //    // Sort
        //    var sortedColumns = requestModel.Columns.GetSortedColumns();
        //    var orderByString = String.Empty;

        //    foreach (var column in sortedColumns)
        //    {
        //        orderByString += orderByString != String.Empty ? "," : "";
        //        orderByString += (column.Data == "Name" ? "Name" : column.Data) + (column.SortDirection == Column.OrderDirection.Ascendant ? " asc" : " desc");
        //    }

        //    //query = query.OrderBy(orderByString == String.Empty ? "Name asc" : orderByString);

        //    // Paging
        //    query = query.Skip(requestModel.Start).Take(requestModel.Length);

        //    //var data = query.Select(p => new
        //    //{                
        //    //    Name = p.Name,
        //    //    UserName = p.UserName,
        //    //    ZipCode = p.ZipCode
        //    //}).ToList();

        //    var data = query.ToList();

        //    return Json(new DataTablesResponse(requestModel.Draw, data, filteredCount, totalCount), JsonRequestBehavior.AllowGet);
        //}

    }
}