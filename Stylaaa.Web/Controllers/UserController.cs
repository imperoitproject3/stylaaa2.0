﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web.Controllers
{
    public class UserController : BaseController
    {
        private StylishServices _stylist;
        private CountryServices _countryService;
        private UserService _userService;
        private CoreUserServices _user;
        private PostServices _postService;
        private JobServices _service;

        public UserController()
        {
            _service = new JobServices();
            _postService = new PostServices();
            _countryService = new CountryServices();
            _stylist = new StylishServices();
            _userService = new UserService();
            _user = new CoreUserServices();
        }

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> About()
        {
            string id = ViewBag.UserID;

            if (User.Identity.GetUserId() == id)
            {
                bool IsProfileComplete = _userService.checkprofilesetup(id);
                ViewBag.isProfileCompleted = IsProfileComplete;
            }

            ViewBag.UserProfile = await _user.GetUserDetail(id);
            List<SelectListItem> dropDownList = (from x in _countryService.IQueryable_Country().ToList()
                                                 select new SelectListItem
                                                 {
                                                     Text = x.Name,
                                                     Value = x.CountryId.ToString()
                                                 }).ToList();
            ViewData["country"] = dropDownList;
            ResponseModel<UserAboutModel> mResult = new ResponseModel<UserAboutModel>();
            mResult = await _user.GetUserAbout(id);
            return View(mResult.Result);
        }

        public async Task<ActionResult> Job()
        {
            string UserId = User.Identity.GetUserId();
            ViewBag.UserProfile = await _user.GetUserDetail(UserId);

            GetUserJobs jobmodel = new GetUserJobs();
            jobmodel = await _service.JoblistByUser(new PaginationModel { UserId = UserId, PageIndex = 1 });

            return View(jobmodel);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> About(UserAboutModel model)
        {
            ResponseModel<UserAboutModel> mResult = new ResponseModel<UserAboutModel>();
            if (User.Identity.IsAuthenticated)
            {
                mResult = await _user.UserCompleteProfile(model);
            }
            if (mResult.Status == ResponseStatus.Success)
            {
                {
                    // get context of the authentication manager
                    var authenticationManager = HttpContext.GetOwinContext().Authentication;

                    // create a new identity from the old one
                    var identity = new ClaimsIdentity(User.Identity);
                    //identity.Name.Replace(identity.Name.ToString(), model.UserName);

                    // update claim value
                    identity.RemoveClaim(identity.FindFirst("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"));
                    identity.AddClaim(new Claim("http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name", model.UserName));

                    // tell the authentication manager to use this new identity
                    authenticationManager.AuthenticationResponseGrant =
                        new AuthenticationResponseGrant(
                            new ClaimsPrincipal(identity),
                            new AuthenticationProperties { IsPersistent = true }
                        );
                }
                TempData["alertModel"] = new AlertModel(AlertStatus.success, "Profile Completed");
                List<SelectListItem> dropDownList = (from x in _countryService.IQueryable_Country().ToList()
                                                     select new SelectListItem
                                                     {
                                                         Text = x.Name,
                                                         Value = x.CountryId.ToString()
                                                     }).ToList();
                ViewData["country"] = dropDownList;
                //return PartialView("_UserAbout", mResult.Result);
                ViewBag.UserProfile = await _user.GetUserDetail(mResult.Result.Id);
                return View(mResult.Result);
            }
            //return PartialView("_UserAbout", mResult.Result);
            return View(mResult.Result);
        }

        [AllowAnonymous]
        public async Task<ActionResult> Post()
        {
            //if (User.Identity.IsAuthenticated)
            //{
            string UserId = ViewBag.UserID;
            ViewBag.UserProfile = await _user.GetUserDetail(UserId);
            GetPostListByUser getPostListModel = new GetPostListByUser();
            ViewBag.action = Resource.post;
            getPostListModel = await _postService.PostListByUser(new PaginationModel { PageIndex = 1, UserId = UserId });

            GetPostListByUser totalrecentpost = await _postService.PostListByUser(new PaginationModel { PageIndex = 0, UserId = UserId });
            decimal totarecentpostCount = totalrecentpost.RecentPost.Count;

            getPostListModel.RecentPostPageIndex = 1;
            getPostListModel.RecentPostPageCount = Math.Ceiling((decimal)(totarecentpostCount > 0 ? (totarecentpostCount / GlobalConfig.PageSize) : 0));

            return View(getPostListModel);
            //}
            //else
            //{
            //    return RedirectToAction("Login", "Account");
            //}
        }

        public async Task<ActionResult> Following()
        {
            if (User.Identity.IsAuthenticated)
            {
                string id = ViewBag.UserID;
                ResponseModel<UserFollowTransactionModel> mResult = new ResponseModel<UserFollowTransactionModel>();
                if (ModelState.IsValid)
                {
                    mResult = await _stylist.Followings(id, User.Identity.GetUserId());
                    if (mResult.Status == ResponseStatus.Success)
                    {
                        ViewBag.action = Resource.following;
                        ViewBag.UserProfile = mResult.Result.User;
                        return View(mResult.Result);
                    }
                }
                return RedirectToAction("About", new { id = id });
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public async Task<ActionResult> Followers()
        {
            if (User.Identity.IsAuthenticated)
            {
                string id = ViewBag.UserID;
                ResponseModel<UserFollowTransactionModel> mResult = new ResponseModel<UserFollowTransactionModel>();
                if (ModelState.IsValid)
                {
                    mResult = await _stylist.Followers(id, User.Identity.GetUserId());
                    if (mResult.Status == ResponseStatus.Success)
                    {
                        ViewBag.action = Resource.followers;
                        ViewBag.UserProfile = mResult.Result.User;
                        return View("Following", mResult.Result);
                    }
                }
                return RedirectToAction("About", new { id = id });
            }
            else
            {
                return RedirectToAction("Login", "Account");
            }
        }

        public ActionResult UserPost(string id)
        {
            return View();
        }
    }
}