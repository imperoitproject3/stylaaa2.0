$.emojiarea.path = 'packs/basic';
$.emojiarea.icons = {
	':smile:'     : 'smile.png',
	':angry:'     : 'angry.png',
	':flushed:'   : 'flushed.png',
	':neckbeard:' : 'neckbeard.png',
	':laughing:'  : 'laughing.png',
	':smile2:'     : 'smile - Copy.png',
	':angry2:'     : 'angry - Copy.png',
	':flushed2:'   : 'flushed - Copy.png',
	':neckbeard2:' : 'neckbeard - Copy.png',
	':laughing2:'  : 'laughing - Copy.png',
};