﻿var autocomplete;
var autocompleteAddress;
var PrimaryID = 0;
var currentLatitude = 53.7702356, currentLongitude = -2.7671912;
var CurrentAddress = "";
var map;
//Get Current Location

$(document).ready(function () {
    //GetCurrentLocation();
    $('.googlepostcode').on('focus', document, function () {
        BindCurrentLatLong();
    });
    //BindCurrentLatLong();
});

function pan() {
    var panPoint = new google.maps.LatLng($('#LocationLatitude').val(), $('#LocationLongitude').val());
    map.panTo(panPoint)
}

function GetCurrentLocation() {
    try {
        if (navigator.geolocation) {
            var positionOptions = {
                enableHighAccuracy: true,
                timeout: 60000 //1 minutes
            };
            navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
        }
        else
            console.log("Your browser doesn't support the Geolocation API");
    }
    catch (err) {
        console.log(err);
        BindCurrentLatLong();
    }
}

function geolocationSuccess(position) {
    currentLatitude = position.coords.latitude;
    currentLongitude = position.coords.longitude;
    BindCurrentLatLong();
}

function GetCurrentPosition(latitude, longitude) {
    source = new google.maps.LatLng(latitude, longitude);
}

function geolocationError(positionError) {
    console.log("error: " + positionError.message);
    BindCurrentLatLong();
}

function BindCurrentLatLong() {
    //var input = document.getElementById("txtAddressSearch");
    ////autocomplete = new google.maps.places.Autocomplete(input);

    //autocomplete = new google.maps.places.Autocomplete((input), {
    //    //types: ['establishment'],
    //    componentRestrictions: { 'country': 'uk' }
    //});

    //zipcode autocomplete functionality
    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);
    var marker = new google.maps.Marker({
        map: map,
        anchorPoint: new google.maps.Point(0, -29)
    });


    //var txtZipcode = document.getElementsByClassName('googlepostcode')[0];
    if (txtZipcode) {
        var zipcode_autocomplete = new google.maps.places.Autocomplete((txtZipcode), {

        });
        google.maps.event.addListener(zipcode_autocomplete, 'place_changed', function () {
            var place = zipcode_autocomplete.getPlace();
            currentLatitude = place.geometry.location.lat();
            currentLongitude = place.geometry.location.lng();
            var Address = place.formatted_address;
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }

            infowindowContent.children['place-icon'].src = place.icon;
            infowindowContent.children['place-name'].textContent = place.name;
            infowindowContent.children['place-address'].textContent = address;
            infowindow.open(map, marker);
            AutoFillAddressGeomatry(Address);
        });




    }
}


//Open Map Modal
function CloseMapModal() {
    $('#Map_Modal').modal('hide');
    var pathname = window.location.pathname.toLocaleLowerCase();
    if (pathname != "/company/profile.aspx" && pathname != "/contractor/editprofile.aspx" && pathname != "/company/managejob.aspx")
        $("body").addClass("modal-open-custom");
}

function OpenMapModal() {
    return false;
    $("#Map_Modal").modal().on('shown.bs.modal',
        function () {
            InitilizeMap();
        });
}

function InitilizeMap() {
    var geocoder = new google.maps.Geocoder();

    var Latitude = $('#LocationLatitude').val();
    var Longitude = $('#LocationLongitude').val();
    if (Latitude == 0 && Longitude == 0) {
        Latitude = currentLatitude;
        Longitude = currentLongitude;
    }

    var source = new google.maps.LatLng(Latitude, Longitude);
    var map_options = {
        center: source,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), map_options);

    marker = new google.maps.Marker({
        position: source,
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
    });



    //google.maps.event.addListener(autocomplete, 'place_changed', function () {
    //    var place = autocomplete.getPlace();
    //    currentLatitude = place.geometry.location.lat();
    //    currentLongitude = place.geometry.location.lng();
    //    var Address = $("#location").val();//place.formatted_address;
    //    AutoFillAddressGeomatry(Address);

    //    map.setCenter(place.geometry.location);
    //    marker.setPosition(place.geometry.location);
    //    marker.setAnimation(google.maps.Animation.DROP);
    //});

    myListener = google.maps.event.addListener(map, 'click', function (event) {
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    $("#location").val(results[0].formatted_address);
                    $('#LocationLatitude').val(parseFloat(marker.getPosition().lat()));
                    $('#LocationLongitude').val(parseFloat(marker.getPosition().lng()));
                    var bla = $('#location').val();
                    SetAutocompleteAddress(bla);
                }
            }
        });
        placeMarker(event.latLng);
    });

    function placeMarker(location) {
        if (marker) {
            marker.setPosition(location);
        } else {
            marker = new google.maps.Marker({
                position: location,
                map: map,
                draggable: true
            });
            //google.maps.event.addListener(marker, "drag", function (mEvent) {
            //    populateInputs(mEvent.latLng);
            //});

            google.maps.event.addListener(marker, 'drag', function () {
                geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        if (results[0]) {
                            $("#location").val(results[0].formatted_address);
                            $('#LocationLatitude').val(parseFloat(marker.getPosition().lat()));
                            $('#LocationLongitude').val(parseFloat(marker.getPosition().lng()));
                            var bla = $('#location').val();
                            SetAutocompleteAddress(bla);
                        }
                    }
                });
            });
        }
        //populateInputs(location);
    }

    google.maps.event.addListener(marker, 'dragend', function () {
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    currentLatitude = marker.getPosition().lat();
                    currentLongitude = marker.getPosition().lng();
                    var Address = results[0].formatted_address;
                    $("#location").val(results[0].formatted_address);
                    $('#LocationLatitude').val(parseFloat(currentLatitude));
                    $('#LocationLongitude').val(parseFloat(currentLongitude));
                    $('input.googleaddress').val(Address);
                    AutoFillAddressGeomatry(Address);
                }
            }
        });
    });
}


//Set Address
{
    function AutoFillAddressGeomatry(Address) {
        var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
        var geocoder = new google.maps.Geocoder();

        //geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        geocoder.geocode({ 'address': Address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {                
                var address_components = results[0].address_components;

                if (Address == "")
                    CurrentAddress = results[0].formatted_address;
                else
                    CurrentAddress = Address;
                var Street = "";
                var City = "";
                var Country = "";
                var Zipcode = "";

                for (var i = 0; i < address_components.length; i++) {
                    var long_name = address_components[i].long_name;

                    var types = address_components[i].types;

                    for (var j = 0; j < types.length; j++) {
                        var addressType = address_components[i].types[j];

                        switch (addressType) {
                            case 'route':
                            case 'sublocality_level_1':
                            case 'sublocality':
                                {
                                    Street = long_name;
                                    break;
                                }

                            case 'postal_town':
                            case 'locality':
                                {
                                    City = long_name;
                                    break;
                                }

                            case 'country':
                            case 'administrative_area_level_1':
                                {
                                    Country = long_name;
                                    break;
                                }

                            case 'postal_code':
                                {
                                    Zipcode = long_name;
                                    break;
                                }
                        }
                    }
                }
                $("#LocationLatitude").val(parseFloat(currentLatitude));
                $('.googleLatitude').val(parseFloat(currentLatitude));
                $("#LocationLongitude").val(parseFloat(currentLongitude));
                $(".googleLongitude").val(parseFloat(currentLongitude));
                $('#ZipCode').val(Zipcode);
                $('#Country').val(Country);                
                $('.googleaddress').val(Zipcode);
                
                //$('#location').val(CurrentAddress);
                //$(".googleaddress").val(CurrentAddress);
                //$('.googlestreet').val(Street);
                //$('#ZipCode').val(Zipcode);
                //if (Zipcode)
                //    $('.googlepostcode').val(Zipcode);
                //else
                //    $('.googlepostcode').val(CurrentAddress);
                $('.googlecity').val(City);
                //InitilizeMap();
                $('.btnUpdateProfile').removeAttr('disabled');
                $('.btnUpdateProfile').val('Update Profile');
               // InitilizeMap();
            }
        });
    }
}

//Set Address Components
{
    function SetAddressComponents(argID, Latitude, Longitude, EncodeAddress) {
        var Address = Base64Decode(EncodeAddress);
        PrimaryID = argID;
        currentLatitude = Latitude;
        currentLongitude = Longitude;
        CurrentAddress = Address;

        $("#LocationLatitude").val(parseFloat(currentLatitude));
        $("#LocationLongitude").val(parseFloat(currentLongitude));
        //$('#txtAddressSearch').val(CurrentAddress);
    }
}
function BindAddressSearch() {
    var txtAddress = document.getElementsByClassName('googleaddress')[0];   
    if (txtAddress) {        
        autocompleteAddress = new google.maps.places.Autocomplete((txtAddress), {
            types: ['(regions)']
            //componentRestrictions: { 'country': 'at' }
        });       
        google.maps.event.addListener(autocompleteAddress, 'place_changed', function () {
            if ($('.btnUpdateProfile').length) {
                $('.btnUpdateProfile').attr('disabled', 'disabled');
                $('.btnUpdateProfile').val('Please wait...');
            }            
            var place = autocompleteAddress.getPlace();            
            currentLatitude = place.geometry.location.lat();
            console.log(currentLatitude);
            currentLongitude = place.geometry.location.lng();
            console.log(currentLongitude);
            $("#LocationLatitude").val(parseFloat(currentLatitude));
            $("#LocationLongitude").val(parseFloat(currentLongitude));
            //console.log(place.address_components.types)
            var Address = $(txtAddress).val();
            AutoFillAddressGeomatry(Address);
        });
    }
}

function BindAddressSearchforLanding() {    
    var txtAddress = document.getElementsByClassName('googleaddress')[0];
    if (txtAddress) {
        autocompleteAddress = new google.maps.places.Autocomplete((txtAddress), {
            types: ['(regions)']
            //componentRestrictions: { 'country': 'at' }
        });
        google.maps.event.addListener(autocompleteAddress, 'place_changed', function () {            
            if ($('.btnUpdateProfile').length) {
                $('.btnUpdateProfile').attr('disabled', 'disabled');
                $('.btnUpdateProfile').val('Please wait...');
            }
            var place = autocompleteAddress.getPlace();
            currentLatitude = place.geometry.location.lat();
            console.log(currentLatitude);
            currentLongitude = place.geometry.location.lng();
            console.log(currentLongitude);
            //$("#LocationLatitude").val(parseFloat(currentLatitude));
            //$("#LocationLongitude").val(parseFloat(currentLongitude));
            //console.log(place.address_components.types)
            var Address = $(txtAddress).val();           
            $("#Address").val(Address);
            //AutoFillAddressGeomatry(Address);
        });
    }
}

{
    function SetAutocompleteAddress(Address) {
        $("#location" + PrimaryID).val(Address);
        $("#location").val(Address);
    }
}



