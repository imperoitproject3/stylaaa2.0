﻿var txtAddressID = 0;

//Get Current Location
{
    $(document).ready(function () {
        GetCurrentLocation();
    });

    function GetCurrentLocation() {
        if (txtAddressID == 0) {
            try {
                if (navigator.geolocation) {
                    var positionOptions = {
                        enableHighAccuracy: true,
                        timeout: 60000 //1 minutes
                    };
                    navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
                }
                else
                    console.log("Your browser doesn't support the Geolocation API");
            }
            catch (err) {
                console.log(err);
                BindCurrentLatLong(53.7702356, -2.7671912);
            }
        }
    }

    function geolocationSuccess(position) {
        var currentLatitude = position.coords.latitude;
        var currentLongitude = position.coords.longitude;
        BindCurrentLatLong(currentLatitude, currentLongitude);
    }

    function GetCurrentPosition(latitude, longitude) {
        source = new google.maps.LatLng(latitude, longitude);
    }

    function geolocationError(positionError) {
        console.log("error: " + positionError.message);
        BindCurrentLatLong(53.7702356, -2.7671912);
    }

    function BindCurrentLatLong(currentLatitude, currentLongitude) {
        var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
        var geocoder = new google.maps.Geocoder();

        geocoder.geocode({ 'latLng': latlng }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                var address = "";
                if (results[0]) {
                    address = results[0].formatted_address;
                }
                else {
                    address = results[1].formatted_address;
                }
                //address = "";
                $("#location").val(address);
                SetAutocompleteAddress(address);
            }
        });

        var autocomplete = new google.maps.places.Autocomplete(document.getElementById("location"));

        $("#LocationLatitude").val(currentLatitude);
        $("#LocationLongitude").val(currentLongitude);
    }
}

//Open Map Modal
{
    function CloseMapModal() {
        $('#Map_Modal').modal('hide');

        var pathname = window.location.pathname.toLocaleLowerCase();
        if (pathname != "/admin/editbusiness.aspx") {
            $("body").addClass("modal-open-custom");
        }
    }

    function OpenMapModal() {
        $("#Map_Modal").modal().on('shown.bs.modal',
            function () {
                InitilizeMap();
            });
    }

    function InitilizeMap() {
        var geocoder = new google.maps.Geocoder();

        var Latitude = $('#LocationLatitude').val();
        var Longitude = $('#LocationLongitude').val();
        var source = new google.maps.LatLng(Latitude, Longitude);
        var map_options = {
            center: source,
            zoom: 16,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var map = new google.maps.Map(document.getElementById("map"), map_options);
        marker = new google.maps.Marker({
            position: source,
            map: map,
            draggable: true,
            animation: google.maps.Animation.DROP,
        });

        var autocomplete = new google.maps.places.Autocomplete(document.getElementClassName("location"));

        google.maps.event.addListener(autocomplete, 'place_changed', function () {
            var place = autocomplete.getPlace();
            $("#LocationLatitude").val(place.geometry.location.lat());
            $("#LocationLongitude").val(place.geometry.location.lng());
            //$("#location").val(place.formatted_address);
            var bla = $('#location').val();
            SetAutocompleteAddress(bla);

            map.setCenter(place.geometry.location);
            marker.setPosition(place.geometry.location);
            marker.setAnimation(google.maps.Animation.DROP);
        });

        google.maps.event.addListener(marker, 'dragend', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                      //  $('#txtAddressSearch').val(results[0].formatted_address);
                        $('#LocationLatitude').val(marker.getPosition().lat());
                        $('#LocationLongitude').val(marker.getPosition().lng());
                        var bla = $('#location').val();
                        SetAutocompleteAddress(bla);
                    }
                }
            });
        });

        google.maps.event.addListener(marker, 'drag', function () {
            geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (results[0]) {
                       // $("#txtAddressSearch").val(results[0].formatted_address);
                        $('#LocationLatitude').val(marker.getPosition().lat());
                        $('#LocationLongitude').val(marker.getPosition().lng());
                        var bla = $('#location').val();
                        SetAutocompleteAddress(bla);
                    }
                }
            });
        });
    }
}

//Set Address
{
    function SetAutocompleteAddress(Address) {
        $(".txtAddress" + txtAddressID).val(Address);
        $(".txtAddress").val(Address);
    }
}