
$(document).on('click', '#crop .next-button', function () {
    $('.popup-tabs li:nth-child(3) a').trigger('click');
    $('.popup-tabs li:first-child a, .popup-tabs li:nth-child(2) a').addClass('active');

    $("#upload").hide();
    $("#crop").hide();
    $("#description").show();
});

$(document).on('click', '.popup-tabs li:first-child', function () {
    setTimeout(function () {
        $('.popup-tabs li a').removeClass('active');
        $('.popup-tabs li:first-child a').addClass('active');
        $("#upload").show();
        $("#crop").hide();
        $("#description").hide();
    }, 100);
});

function ShowToaster(status, message, title) {
    toastr.remove();

    status = status.toLowerCase();
    if (status == 'error') {
        toastr.error(message, title);
    }
    else if (status == 'success') {
        toastr.success(message, title);
    }
    else if (status == 'info') {
        toastr.info(message, title);
    }
    else if (status == 'warning') {
        toastr.warning(message, title);
    }
}
//$(document).on('click', '.popup-tabs li:nth-child(3)', function () {
//    $('.popup-tabs li a').addClass('active')
//    setTimeout(function () {
//        $('.popup-tabs li a').addClass('active')

//        $("#upload").hide();
//        $("#crop").hide();
//        $("#description").show();
//    }, 100)
//});
(function ($) {

    var header_login = $('.hader-login');
    var header_log = header_login.innerHeight();

    var footer_login = $('.footer-login');
    var footer_log = header_login.innerHeight();

    var header_admin = $('.header-section');
    var header_admin_two = header_admin.innerHeight();


    $('.middle-section-admin').css({
        paddingTop: header_admin_two,
    });

    $('.middle-login').css({
        paddingTop: header_log,
        paddingBottom: footer_log
    });

    $('.ipad-leftbar').css({
        top: header_admin_two,
    });



    $(document).ready(function () {

        // tags
        $('.fancybox').fancybox({
            modal: true,
            helpers: {
                overlay: { locked: false, closeClick: false }
            }
        });

        $('#select-box').tagsinput({
            trimValue: true,
            confirmKeys: [13, 44, 32],
            focusClass: "my-focus-class"
        });

        $(".bootstrap-tagsinput input")
            .on("focus", function () {
                $(this)
                    .closest(".bootstrap-tagsinput")
                    .addClass("has-focus");
            })
            .on("blur", function () {
                $(this)
                    .closest(".bootstrap-tagsinput")
                    .removeClass("has-focus");
            });



        // emoji 

        $(document).on('click', '.accept-btn', function () {
            $(this).parent().parent().parent().find('.ms-chect-box ').removeClass('no-text');
            $(this).parent().parent().find('.table ').removeClass('hide');
            $(this).closest('.footer-chet-btn ').hide();
            $(this).parent().parent().find("#txtMessage").emojioneArea();           
        });

      

        $(".accordion-heading").on('click', function (e) {
            $return = true;
            $("#accordion .panel").slideUp();
            $(".accordion-heading").removeClass('active');
            if ($(this).next('div').is(':visible') === false) {
                $return = false;
                $(this).addClass('active');
                $(this).next('div').stop(true, false).slideDown();
            }
            else {
                $("#accordion  .accordion-heading").removeClass('active');
                $(this).removeClass('active');
            }
            return $return;
        });

        function openFancyboxModal() {
            $('.fancybox').fancybox({
                modal: true,
                helpers: {
                    overlay: { locked: false, closeClick: false }
                }
            });
        }

        $('.fancybox').fancybox({
            modal: true,
            helpers: {
                overlay: { locked: false, closeClick: false }
            }
        });

        function openFancybox(wrapperdiv, maindivid) {
            $(wrapperdiv).html(result);
            $.fancybox.open({
                src: maindivid,
                type: 'inline',
                modal: true,
                helpers: {
                    overlay: { closeClick: false }
                }
            });

            //var jsdata = jQuery.fancybox.open(jQuery(maindivid));
            //$(".fancybox-slide").unbind();
        }


        $(".ps-relative").mCustomScrollbar({
            autoHideScrollbar: true
        });

        //$(".search-post-scroll").mCustomScrollbar({
        //    theme: "dark",
        //    autohidescrollbar: true,
        //    mousewheel: { enable: true }
        //});

        $(".over-scroll").mCustomScrollbar({
            theme: "dark",
            autoHideScrollbar: true,
            mouseWheel: { enable: true }
        });


        $('.over-fllow-scroll').mCustomScrollbar({
            theme: "dark",
            scrollbarPosition: "outside",
            autoHideScrollbar: true,
            mouseWheel: { enable: true },
            callbacks: {
                onScroll: function () {
                    if (this.mcs.top < -20) {
                        $(".back_to_top").css({
                            display: 'block'
                        })
                        setTimeout(function () {
                            $(".back_to_top").fadeOut();
                        }, 2500);

                    } else {
                        $(".back_to_bottom").css({ display: 'block' });
                        setTimeout(function () {
                            $(".back_to_bottom").fadeOut();
                        }, 2500);

                    }

                }
            }
        });

        $('.back_to_top').on('click', function () {
            $(".over-fllow-scroll").mCustomScrollbar("scrollTo", "top");
            $(".back_to_top").css({ display: 'none' });

        });

        $('.back_to_bottom').on('click', function () {
            $(".over-fllow-scroll").mCustomScrollbar("scrollTo", "bottom");
            $(".back_to_bottom").css({ display: 'none' });
        });

        $("#imgInp").change(function () {
            readURL(this);
            $('.profile-pic').addClass('active');
        });

        $("#imgInp1").change(function () {
            readURL(this);
        });

        $("#file-upload, #input1").change(function () {
            readURL2(this);
        });

        

        $('.open-leftbar').click(function () {
            $('.left-admin').animate({
                left: 0
            });
            $('html').addClass('over-fllow-hidden');
            $(this).parents('.left-admin').addClass('active');

        })

        $('.close-leftbar').click(function () {
            $('.left-admin').animate({
                left: -356
            });
            $('html').removeClass('over-fllow-hidden');
            $(this).parents('.left-admin').removeClass('active');

        })


        $(document).mouseup(function (e) {
            var container = $(".left-admin");

            if (container.has(e.target).length === 0) {
                container.animate({
                    left: -356
                });
                $('html').removeClass('over-fllow-hidden');
                container.removeClass('active');
            }
        });

        //$('#homepage').on('click', function () {    
        //    $.cookie('selectedTab', '', { expires: -1, path: '/StylaaaWeb/Home' });
        //    var Tab = $.cookie('selectedTab');
        //    alert(Tab);
        //    Tab = 'post';
        //    $.cookie('selectedTab', Tab, { expires: 7 }); 
        //    var Tab1 = $.cookie('selectedTab');
        //    alert(Tab1);
        //});
        
        $(".GaugeMeter").gaugeMeter();
        
        $('.main-search').click(function () {
            $('.header-section').addClass('active');
            $('.search-box').fadeToggle();
            $(this).toggle();
        });

        $('.titel-choose').click(function () {
            $('.mb-slide').slideToggle();
            $(this).toggleClass('active');
        });

        $('.close-search').click(function () {
            $('.header-section').removeClass('active');
            $('.search-box').hide();
            $('.main-search').show();
            $('.search-popup').fadeOut();
            $('.close-search-dropdown').fadeOut();
        });


        $('.top-notification .open-noty').click(function () {
            //if ($('#divNotificationList .noty-row').length > 0) {
            $('.notification-dropdown').stop(true, false).fadeToggle();
            //}
        });

        $('.search-field input').on('focus', function () {
            $('.search-popup, .close-search-dropdown').fadeIn();
        });

        $('.main-search').on('click', function () {
            $('.search-popup, .close-search-dropdown').fadeIn();
        });


        $(document).mouseup(function (e) {
            var container = $(".notification-dropdown");

            if (container.has(e.target).length === 0) {
                container.fadeOut();
            }

            container = $(".top-search");

            // if the target of the click isn't the container nor a descendant of the container
            if (container.has(e.target).length === 0) {
                //container.hide();
                $(".close-search-dropdown").click();
            }
        });

        $('.close-search-dropdown').click(function () {
            $("#searchpost").val("");
            $("#posts").html("");
            $("#stylist").html("");
            $("#hashtag").html("");
            $(".search-popup .chek-bx input[type='checkbox']").prop("checked", false);
            //$(".close-search").click();

            $('.header-section').removeClass('active');
            $('.search-box').hide();
            $('.main-search').show();
            $('.search-popup').fadeOut();
            $('.close-search-dropdown').hide();
            
            //$('.search-popup, .close-search-dropdown').fadeOut();
        })


        $(".profile-bx .pf-chet").click(function () {
            $(this).parents('.profile-bx').addClass('active');
        });

        $(".profile-bx .close-profile-message").click(function () {
            $(this).parents('.profile-bx').removeClass('active');
        });


        $('.stylist-log').click(function () {
            $('.login-row h1').html('Stylist');
        });

        $('.middle-login .login-row h1').html('Stylist');

        $('.user-log').click(function () {
            $('.login-row h1').html('User');
        });

        $('.menu-open').click(function () {
            $('html').addClass('over-fllow');
        });

        $('.close-top-menu').click(function () {
            $('html').removeClass('over-fllow');
        });

        //if (varLang == 'de') {
        //    $('.country-select').select2({
        //        placeholder: "Land",
        //        allowClear: true
        //    });
        //}
        //else {
        //    $('.country-select').select2({
        //        placeholder: "Country",
        //        allowClear: true
        //    });
        //}


        $('.multy-select').select2();

        initalizeTabs();


        //for tabs
        //.mesage-tab-links ul,
        $('ul.tabs, .new-tab-links ul, .field-radio ul, .tabs-in, .search-tabs ul, .popup-tabs ul, .smily-tab-links ul').each(function () {
            var $active, $content, $links = $(this).find('a');
            $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
            $active.addClass('active');
            if ($active.length > 0) {
                $content = $($active[0].hash);
                $links.not($active).each(function () {
                    $(this.hash).hide();
                });
                $(this).on('click', 'a', function (e) {
                    $active.removeClass('active');
                    $content.hide();
                    $active = $(this);
                    $content = $(this.hash);
                    $active.addClass('active');
                    $content.show();
                    e.preventDefault();
                    equalheight('.product-content, .profile-bx, .product-new-ctn');
                });
            }
        });




        $('.stylists-click').click(function () {
            myFunction_rating();
            $(".GaugeMeter").empty();
            $(".GaugeMeter").gaugeMeter();
        });

    });


    //  for equeal height
    equalheight = function (container) {
        var currentTallest = 0,
            currentRowStart = 0,
            rowDivs = new Array(),
            $el,
            topPosition = 0;
        $(container).each(function () {

            $el = $(this);
            $($el).height('auto');
            topPostion = $el.position().top;

            if (currentRowStart != topPostion) {
                for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                    rowDivs[currentDiv].height(currentTallest);
                }
                rowDivs.length = 0; // empty the array
                currentRowStart = topPostion;
                currentTallest = $el.height();
                rowDivs.push($el);
            } else {
                rowDivs.push($el);
                currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
            }
            for (currentDiv = 0; currentDiv < rowDivs.length; currentDiv++) {
                rowDivs[currentDiv].height(currentTallest);
            }
        });
    };

    $(window).load(function () {
        equalheight('.product-content, .profile-bx, .product-new-ctn');

    });

    $(window).resize(function () {

        equalheight('.product-content, .profile-bx, .product-new-ctn');

        var header_login = $('.hader-login');
        var header_log = header_login.innerHeight();

        var footer_login = $('.footer-login');
        var footer_log = header_login.innerHeight();

        var header_admin = $('.header-section');
        var header_admin_two = header_admin.innerHeight();

        $('.middle-section-admin').css({
            paddingTop: header_admin_two
        });

        $('.middle-login').css({
            paddingTop: header_log,
            paddingBottom: footer_log
        });

        $('.ipad-leftbar').css({
            top: header_admin_two
        });

    });


    function myFunction_rating() {
        $('.ps-rating span').each(function () {
            var $this = $(this);
            jQuery({
                Counter: 0
            }).animate({
                Counter: $this.text()
            }, {
                    duration: 1200,
                    easing: 'swing',
                    step: function () {
                        $this.text(Math.ceil(this.Counter));
                    }
                });
        });
    }



    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.profile-pic').css('background', 'url(' + e.target.result + ') no-repeat  center center');
                $('.profile-pic-img').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function readURL2(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.background-img, .img-upload-max').css('background', 'url(' + e.target.result + ') no-repeat  center center');
                $('.background-img-pic').attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    function chet_img(input) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghhijklmnopqrstuvwxyz";
        for (var i = 0; i < 10; i++) {
            text += possible.charAt(Math.floor(Math.random() * possible.length));
        }
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                $('.' + text).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        }
        if ($(this).change()) {
            var id = $('.mesage-tab-links li a.active').attr('href');
            $(id + ' .ms-chect-box > .mCustomScrollbar').children().children().append('<div class="chet-row send-message"><div class="message-chet"><img class="chet-img ' + text + '" src="" alt=""/></div></div>');

        }
    }


    $("#chet-img").change(function () {
        $(".over-fllow-scroll").mCustomScrollbar("scrollTo", "bottom");
        chet_img(this);

    });



    $('.inner-slide').owlCarousel({
        stagePadding: 4,
        loop: false,
        margin: 0,
        nav: false,
        responsive: {
            0: {
                items: 4
            },
            480: {
                items: 6
            },
            768: {
                items: 6
            }
        },
        //autoWidth: true
    })


    $('.owl-carousel').owlCarousel({
        items: 1,
        loop: true,
    });


    var scrollTop = $(".scrollTop");

    $('.over-fllow-scroll').scroll(function () {
        // declare variable
        var topPos = $(this).scrollTop();

        // if user scrolls down - show scroll to top button
        if (topPos > 100) {
            $(scrollTop).css("opacity", "1");

        } else {
            $(scrollTop).css("opacity", "0");
        }

    }); // scroll END

    //Click event to scroll to top
    $(scrollTop).click(function () {
        $('.over-fllow-scroll').animate({
            scrollTop: 0
        }, 800);
        return false;

    });


    if ($('.copy-button').length > 0) {
        var copyButton = document.querySelector('.copy-button');
        var copyInput = document.querySelector('.copy-link input');
        copyButton.addEventListener('click', function (e) {
            e.preventDefault();
            var text = copyInput.select();
            document.execCommand('copy');
        });

        copyInput.addEventListener('click', function () {
            this.select();
        });

    }




})(jQuery);


function initalizeTabs() {
    $('.popup-tabs ul').each(function () {
        var $active, $content, $links = $(this).find('a');
        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active[0].hash);
        $links.not($active).each(function () {
            $(this.hash).hide();
        });
        $(this).on('click', 'a', function (e) {
            $active.removeClass('active');
            $content.hide();
            $active = $(this);
            $content = $(this.hash);
            $active.addClass('active');
            $content.show();
            e.preventDefault();
            equalheight('.product-content, .profile-bx, .product-new-ctn');

        });

    });
}
var canvas = $("#canvas");
if (canvas.get(0) != null && canvas.get(0) && undefined && canvas.get(0) != '') {
    context = canvas.get(0).getContext("2d")
}


function initalizeTabsForJob() {
    $('.popup-tabs ul').each(function () {
        var $active, $content, $links = $(this).find('a');
        $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
        $active.addClass('active');
        $content = $($active[0].hash);
        $links.not($active).each(function () {
            $(this.hash).hide();
        });
        $(this).on('click', 'a', function (e) {
            $active.removeClass('active');
            $content.hide();
            $active = $(this);
            $content = $(this.hash);
            $active.addClass('active');
            $content.show();
            e.preventDefault();
            equalheight('.product-content, .profile-bx, .product-new-ctn');

        });

    });
    
}
var canvas = $("#jobcanvas");
if (canvas.get(0) != null && canvas.get(0) && undefined && canvas.get(0) != '') {
    context = canvas.get(0).getContext("2d");
}

//$result = $();

$('#file-input').on('change', function () {
    $('#change-cover-photo .ps-upload, #add-post .ps-upload, #add-job .ps-upload').removeClass('hide');
    $('.crop-image-section .upload-button').addClass('hide');

    if (this.files && this.files[0]) {
        if (this.files[0].type.match(/^image\//)) {
            var reader = new FileReader();
            reader.onload = function (evt) {
                var img = new Image();
                img.onload = function () {
                    context.canvas.height = img.height;
                    context.canvas.width = img.width;
                    context.drawImage(img, 0, 0);
                    var cropper = canvas.cropper({
                        aspectRatio: 16 / 9
                    });
                    $('#btnCrop').click(function () {
                        // Get a string base 64 data url
                        var croppedImageDataURL = canvas.cropper('getCroppedCanvas').toDataURL("image/png");
                        $result.append($('.background-img-pic').attr('src', croppedImageDataURL));
                        $result.append($('.bacround-ps-img').css('background', 'url(' + croppedImageDataURL + ') no-repeat  center center'));

                    });

                    $('#btnRestore').click(function () {
                        canvas.cropper('reseAt');
                        $result.empty();
                    });
                };
                img.src = evt.target.result;
            };
            reader.readAsDataURL(this.files[0]);
        }
        else {
            alert("Invalid file type! Please select an image file.");
        }
    }
    else {
        alert('No file(s) selected.');
    }

});


if ($('#profile-canvas').length > 0) {

    //for profile picture
    var canvas2 = $("#profile-canvas"),
        context2 = canvas2.get(0).getContext("2d");
    $('#file-profile-pic').on('change', function () {
        $('#profile-pic-pp .ps-upload').removeClass('hide');
        if (this.files && this.files[0]) {
            if (this.files[0].type.match(/^image\//)) {
                var reader_new = new FileReader();
                reader_new.onload = function (evt) {
                    var img_new = new Image();
                    img_new.onload = function () {
                        context2.canvas.height = img_new.height;
                        context2.canvas.width = img_new.width;
                        context2.drawImage(img_new, 0, 0);
                        var cropper_new = canvas2.cropper({
                            aspectRatio: 16 / 16
                        });

                        $('#btnCrop2').click(function () {
                            // Get a string base 64 data url
                            var croppedImageDataURL = canvas2.cropper('getCroppedCanvas').toDataURL("image/png");
                            $result.append($('.profile-pic-img').attr('src', croppedImageDataURL));
                            $result.append($('.profile-pic').css('background', 'url(' + croppedImageDataURL + ') no-repeat  center center'));
                            $('.profile-pic').addClass('active');
                            $('.ps-table ').addClass('hide');
                        });

                        $('#btnRestore2').click(function () {

                        });

                    };
                    img_new.src = evt.target.result;
                };
                reader_new.readAsDataURL(this.files[0]);
            }
            else {
                alert("Invalid file type! Please select an image file.");
            }
        }
        else {
            alert('No file(s) selected.');
        }

    });
}
if ($('#crop-pp').length > 0) {
    // crop popup 
    var canvas_crop = $("#crop-pp"),
        context_crop = canvas_crop.get(0).getContext("2d");
    $('#file-crop').on('change', function () {
        $('.ps-upload').removeClass('hide');
        $('#crop-popup .upload-button').addClass('hide');

        if (this.files && this.files[0]) {
            if (this.files[0].type.match(/^image\//)) {
                var reader_ne = new FileReader();
                reader_ne.onload = function (evt) {
                    var img_ne = new Image();
                    img_ne.onload = function () {
                        context_crop.canvas.height = img_ne.height;
                        context_crop.canvas.width = img_ne.width;
                        context_crop.drawImage(img_ne, 0, 0);
                        var cropper_ne = canvas_crop.cropper({
                            aspectRatio: 16 / 9
                        });

                        $('#crop-bt').click(function () {
                            // Get a string base 64 data url
                            var croppedImageDataURL = canvas_crop.cropper('getCroppedCanvas').toDataURL("image/png");
                        });

                        $('').click(function () {

                        });

                    };
                    img_ne.src = evt.target.result;
                };
                reader_ne.readAsDataURL(this.files[0]);
            }
            else {
                alert("Invalid file type! Please select an image file.");
            }
        }
        else {
            alert('No file(s) selected.');
        }

    });

}

function initializejsforflip() {
    $(".profile-bx .pf-chet").click(function () {
        $(this).parents('.profile-bx').addClass('active');
    });

    $(".profile-bx .close-profile-message").click(function () {
        $(this).parents('.profile-bx').removeClass('active');
    });
}

function appendAtCursor(html) {
    var sel, range;
    if (window.getSelection) {
        sel = window.getSelection();
        if (sel.getRangeAt && sel.rangeCount) {
            range = sel.getRangeAt(0);
            range.deleteContents();
            var el = document.createElement("div");
            el.innerHTML = html;
            var frag = document.createDocumentFragment(), node, lastNode;
            while ((node = el.firstChild)) {
                lastNode = frag.appendChild(node);
            }
            range.insertNode(frag);
            if (lastNode) {
                range = range.cloneRange();
                range.setStartAfter(lastNode);
                range.collapse(true);
                sel.removeAllRanges();
                sel.addRange(range);
            }
        }
    }
    else if (document.selection && document.selection.type != "Control") {
        document.selection.createRange().pasteHTML(html);
    }
}



$('.send-chet').on('click', function () {
    if ($('.emojionearea-editor').html().length > 0) {

        //alert($('.mesage-tab-links li a.active').attr('href'));
        var id = $('.mesage-tab-links li a.active').attr('href');
        $(id + ' .ms-chect-box > .mCustomScrollbar').children().children().append('<div class="chet-row send-message"><div class="message-chet"><p>' + $('.emojionearea-editor').html() + '</p></div></div>');

        $('#accordion ul li .mesage-tab-list .ms-chect-box').children().children().append('<div class="chet-row send-message"><div class="message-chet"><p>' + $(this).parent().parent().parent().parent().parent().find('.emojionearea-editor').html() + '</p></div></div>');
        $('.emojionearea-editor').html('');
        $(".over-fllow-scroll").mCustomScrollbar("scrollTo", "bottom");
    }
});


//$('.product-img').on('click', function () {
//    //var video = $(this).find('#my_video');    
//    var vid = document.getElementById('my_video');
//    setTimeout(function () {
//        vid.play();
//    }, 500);
//});



$('.owl-carousel').on('click', function () {
    alert('');
    //$(this).removeAttr('muted');
});

$('.owl-next').on('click', function () {
    var vid = document.getElementById('my_video');
    vid.pause();
});
$('.owl-prev').on('click', function () {
    var vid = document.getElementById('my_video');
    vid.pause();
});



if ($('.field-range input').length > 0) {
    const slider = new Rangeable(".field-range input");
}

function displayOverlay() {

    if ($("body").find("#overlay").length <= 0) {
        //$("<table id='overlay'><tbody><tr><td><img id='loading' src='/StylaaWeb/Content/images/rectangle-2.svg'></td></tr></tbody></table>").css({
        //    "position": "fixed",
        //    "top": "0px",
        //    "left": "0px",
        //    "width": "100%",
        //    "height": "100%",
        //    "background-color": "rgba(255,255,255,0.7)",
        //    "z-index": "10000",
        //    "vertical-align": "middle",
        //    "text-align": "center",
        //    "color": "#fff",
        //    "font-size": "40px",
        //    "font-weight": "bold"
        //}).appendTo("body");
        $("<table id='overlay'><tbody><tr><td></td></tr></tbody></table>").appendTo("body");
        $("#loader-post").show();
    }
}

function removeOverlay() {
    $("#overlay").remove();
    $("#loader-post").hide();
}

function displaySearchOverlay() {
    if ($("body").find("#search-post-overlay").length <= 0) {
        //$("div#search-post-list").prepend("<table id='search-post-overlay'><tbody><tr><td><img id='loading' src='/StylaaWeb/Content/images/rectangle-2.svg' style='width:50%; height:50%'></td></tr></tbody></table>").css({
        //    "position": "sticky",
        //    "top": "0px",
        //    "left": "0px",
        //    "width": "100%",
        //    "height": "100%",
        //    "background-color": "rgba(255,255,255,0.7)",
        //    "z-index": "10000",
        //    "vertical-align": "top",
        //    "text-align": "center",
        //    "color": "#fff",
        //    "font-size": "40px",
        //    "font-weight": "bold"
        //});

        $("div#search-post-list").prepend("<table id='search-post-overlay'><tbody><tr><td></td></tr></tbody></table>");
        $("#loader-search-post").show();
    }
}


function removeSearchOverlay() {
    $("#search-post-overlay").remove();
    $("#loader-search-post").hide();
}

function displayWhiteOverlay() {    
    if ($("body").find("#overlay").length <= 0) {
        $("<table id='overlay'><tbody><tr><td><img id='loading' src='/Content/images/rectangle-2.svg'></td></tr></tbody></table>").css({
            "position": "fixed",
            "top": "0px",
            "left": "0px",
            "right": "0px",
            "bottom": "0px",
            "width": "100%",
            "height": "110%",
            "background-color": "rgba(255,255,255,0.7)",
            "z-index": "10000",
            "vertical-align": "middle",
            "text-align": "center",
            "color": "#fff",
            "font-size": "40px",
            "font-weight": "bold",
            "margin": "0",
            "padding": "0"
        }).appendTo("body");
    }
}

function DispalyLoaderPost() {    
    if ($("body").find("#add-post").length) {
        $("<table id='overlay'><tbody><tr><td><img id='loading' src='/Content/images/rectangle-2.svg'></td></tr></tbody></table>").css({
            "position": "fixed",
            "top": "0px",
            "left": "0px",
            "right": "0px",
            "bottom": "0px",
            "width": "100%",
            "height": "110%",
            "background-color": "rgba(255,255,255,0.7)",
            "z-index": "100000",
            "vertical-align": "middle",
            "text-align": "center",
            "color": "#fff",
            "font-size": "40px",
            "font-weight": "bold",
            "margin": "0",
            "padding": "0"
        }).appendTo("#add-post");
    }
}

function DispalyLoaderJob() {
    if ($("body").find("#add-job").length) {
        $("<table id='overlay'><tbody><tr><td><img id='loading' src='/Content/images/rectangle-2.svg'></td></tr></tbody></table>").css({
            "position": "fixed",
            "top": "0px",
            "left": "0px",
            "right": "0px",
            "bottom": "0px",
            "width": "100%",
            "height": "110%",
            "background-color": "rgba(255,255,255,0.7)",
            "z-index": "100000",
            "vertical-align": "middle",
            "text-align": "center",
            "color": "#fff",
            "font-size": "40px",
            "font-weight": "bold",
            "margin": "0",
            "padding": "0"
        }).appendTo("#add-job");
    }
}



function removeWhiteOverlay() {
    $("#overlay").remove();
}
