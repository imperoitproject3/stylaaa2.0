
//jQuery time
var current_fs, next_fs, previous_fs; //fieldsets
var left, opacity, scale; //fieldset properties which we will animate
var animating; //flag to prevent quick multi-click glitches
var isvalidate = false;

function checkvalidation(url) {
    var adrs = $('#Address').val();
    var catgry = $('#CategoryId').val();
    var date = $('#DateTime').val();
    var name = $('#Name').val();
    var subcat = $('#SubCategoryId').val();
    //var terms = $('#acceptterms').val();

    if (name === "") {
        isvalidate = false;
        var nameerr = "Please enter name";
        if (varLng == "de") {
            nameerr = "Bitte Name eingeben";
        }
        toastr.error(nameerr);
    }
    else if (adrs === "") {
        isvalidate = false;
        var err = "Please select address";
        if (varLng == "de") {
            err = "Bitte Adresse eingeben";
        }
        toastr.error(err);
    }
    else if (catgry == "" || catgry == null) {
        isvalidate = false;
        var err3 = "Please  select category";
        if (varLng == "de") {
            err3 = "Bitte Kategorie auswahlen";
        }
        toastr.error(err3);
    }
    else if (subcat == "" || subcat == null) {
        isvalidate = false;
        var sbcat = "Please  select subcategory";
        if (varLng == "de") {
            sbcat = "Bitte Unterkategorie ausw�hlen";
        }
        toastr.error(sbcat);
    }
    else if (date == "") {
        isvalidate = false;
        var err2 = "Please select date&time";
        if (varLng == "de") {
            err2 = "Datum und Uhrzeit eingeben";
        }
        toastr.error(err2);
    }
    else {
        isvalidate = true;
    }

    if (isvalidate) {
        $("#selectedSubCat #selectedName #totalprice #selectedAddress #selectedAddress #selectedDate #selectedTime").text('');  
        SubCatIds = [];
        SubCatIds.push($('#SubCategoryId').val());
        var IDs = SubCatIds.join(',');
        var fileData = new FormData();
        fileData.append('SubCatIds', IDs);

        $.ajax({
            url: url,
            type: 'POST',
            // Form data
            data: fileData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result) {                    
                    $('#totalprice').text('');                  
                    var html = '';

                    $.each(result, function (index, value) {                       
                        html += '<div class="table fs-head"><h6 Style="padding-bottom:9px;word-break:break-all; padding-right:12px; line-height: 25px;">' + value.Name + '</h6><span class="fs-value">' + value.Price + '&euro;</span></div>';                      
                    });
                                        
                    $('#totalprice').append(html);                   
                    SubCatIds = [];
                }
            },
            error: function (err) {

            }
        });


        $("#selectedName").text(name);
        //$("#totalprice").text($("#Price").val());
        $("#selectedAddress").text(adrs);
        $("#selectedGender").text($('ul#radioGender').find('li a.active').data('id'));
        $('#selectedDate').text($('#date').val());
        $('#selectedTime').text($('#time').val());
        $('#TotalAmount').text('');

        var ttl = parseFloat($("#TotalPrice").val());
       // $("#Price").val(ttl);
        //ttl = ttl + 5;
        $('#TotalAmount').text(ttl);


    }
}

$(".next").click(function () {
    var url = $(this).data('url');    
    checkvalidation(url);
    if (isvalidate == true) {
        if (animating) return false;
        animating = true;

        current_fs = $(this).parent();
        next_fs = $(this).parent().next();

        //activate next step on progressbar using the index of next_fs
        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

        //show the next fieldset
        next_fs.show();
        //hide the current fieldset with style
        current_fs.animate({ opacity: 0 }, {
            step: function (now, mx) {
                //as the opacity of current_fs reduces to 0 - stored in "now"
                //1. scale current_fs down to 80%
                // scale = 1 - (1 - now) * 0.2;
                //2. bring next_fs from the right(50%)
                // left = (now * 50)+"%";
                //3. increase opacity of next_fs to 1 as it moves in
                opacity = 1 - now;
                current_fs.css({
                    'transform': 'scale(' + scale + ')',
                    'position': 'absolute'
                });
                next_fs.css({ 'left': left, 'opacity': opacity });
            },
            duration: 800,
            complete: function () {
                current_fs.hide();
                animating = false;
            },
            //this comes from the custom easing plugin
            easing: 'easeInOutBack'
        });

    }

});

$(".previous").click(function () {
    if (animating) return false;
    animating = true;

    current_fs = $(this).parent();
    previous_fs = $(this).parent().prev();

    //de-activate current step on progressbar
    $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

    //show the previous fieldset
    previous_fs.show();
    //hide the current fieldset with style
    current_fs.animate({ opacity: 0 }, {
        step: function (now, mx) {
            //as the opacity of current_fs reduces to 0 - stored in "now"
            //1. scale previous_fs from 80% to 100%
            // scale = 0.8 + (1 - now) * 0.2;
            //2. take current_fs to the right(50%) - from 0%
            // left = ((1-now) * 50)+"%";
            //3. increase opacity of previous_fs to 1 as it moves in
            opacity = 1 - now;
            current_fs.css({ 'left': left });
            previous_fs.css({ 'transform': 'scale(' + scale + ')', 'opacity': opacity });
        },
        duration: 800,
        complete: function () {
            current_fs.hide();
            animating = false;
        },
        //this comes from the custom easing plugin
        easing: 'easeInOutBack'
    });
});

$(".submit").click(function () {
    return false;
});
