﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;
using Stylaaa.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
[assembly: OwinStartup(typeof(Startup))]
namespace Stylaaa.Web
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureOAuth(app);
            //app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
        }

        public void ConfigureOAuth(IAppBuilder app)
        {
            TimeSpan duration = new TimeSpan(23, 59, 59);
            // Enable the application to use a cookie to
            // store information for the signed in user
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {                
                //CookieHttpOnly=false,
                ExpireTimeSpan = duration,
                CookieName = "StylaaaCookie",
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login")
            });
        }
    }
}