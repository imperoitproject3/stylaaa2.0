﻿var cropper;
var canvas;
var uploadedImageURL;

// Import image
var $inputImage;
var $image;
var context;
var myVar = [];
var myVarStop = [];
var isJobPicUloaded = true;

$(document).on('change', '#JobfileInput', function () {
    //$('.crop-image-section .upload-button').addClass('hide');

    var URL = window.URL || window.webkitURL;
    if (this.files && this.files[0]) {
        if (this.files[0].type.match(/^image\//)) {
            displayWhiteOverlay();
            $('#add-job .ps-upload').removeClass('hide');
            $('#jobimage-container #jobcanvas').addClass('hide');
            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];
                var $image = $('#add-job #jobimage');

                if (uploadedImageURL) {
                    URL.revokeObjectURL(uploadedImageURL);
                }

                var options = {
                    //aspectRatio: 1 / 1
                    aspectRatio: 1 / 1,
                    minCropBoxWidth: 304,
                    minCropBoxHeight: 304,
                    viewMode: 3,
                    built: function () {
                        // Strict mode: set crop box data first
                        var container = $(this).cropper('getContainerData');
                        $(this).cropper('setCropBoxData', {
                            width: 304,
                            height: 304,
                            left: (container.width - cropBoxWidth) / 2,
                            top: (container.height - cropBoxHeight) / 2
                        });
                    }
                };
                var img = new Image();
                img.onload = function () {
                    context.canvas.height = img.height;
                    context.canvas.width = img.width;
                    context.drawImage(img, 0, 0);
                    var cropper = canvas.cropper({
                        aspectRatio: 1 / 1,
                        minCropBoxWidth: 304,
                        minCropBoxHeight: 304,
                    });
                };
                uploadedImageURL = URL.createObjectURL(file);
                $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                $("#add-job #jobimage-container").show();

            }
            removeWhiteOverlay();
        }
        else {
            uploadJobVideoFile();
        }
    }
    else {
        var msg = 'No file(s) selected.';
        if (varLang == 'de') {
            msg = 'Keine Datei (en) ausgewählt';
        }
        ShowToastr('error', msg, '');
    }
});

function fnUploadNewJobFile(url) {
    if (isJobPicUloaded) {
        DispalyLoaderJob();
        $("#add-job .data-div-placeholder-content").hide();
        var fileUpload = $("#jobnewFile").get(0);
        var files = fileUpload.files;
        var blnValid = true;
        for (var i = 0; i < files.length; i++) {

            var _size = files[i].size;
            var exactSize = (_size / (1024 * 1024)).toFixed(2);

            //var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
            //j = 0; while (_size > 900) { _size /= 1024; j++; }
            //var exactSize = Math.round(_size * 100) / 100;
            //exactSize = exactSize / 1024;

            var file = files[i];
            var fileType = file["type"];
            var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
            if ($.inArray(fileType, ValidImageTypes) < 0) {
                //file is video

                displayWhiteOverlay();

                var errormsg = "Please upload only Image File";
                if (varLang == "de") {
                    errormsg = "Please upload only Image File";
                }
                ShowToastr("error", errormsg, "");
                blnValid = false;
                removeWhiteOverlay();
                return;

                //displayWhiteOverlay();
                //if (exactSize > 50) {
                //    var errormsg = "Please upload less than 50mb video";
                //    if (varLang == "de") {
                //        errormsg = "Max 50MB hochladen";
                //    }
                //    ShowToastr("error", errormsg, "");
                //    blnValid = false;
                //    removeWhiteOverlay();
                //    return;
                //}
            }
            else {
                if (exactSize > 6) {
                    var errormsg = "Please upload less than 2mb image";
                    if (varLang == "de") {
                        errormsg = "Max 2MB hochladen";
                    }
                    ShowToastr("error", errormsg, "");
                    blnValid = false;
                    removeWhiteOverlay();
                    return;
                }
            }

            //if (files[i].type.match(/^image\//)) {
            //    //file is video
            //    if (exactSize > 2) {
            //        ShowToastr("error", "Please upload less than 2mb image", "");
            //        blnValid = false;
            //        return;
            //    }
            //}
            //else {
            //    //file is video
            //    if (exactSize > 10) {
            //        ShowToastr("error", "Please upload less than 10mb file", "");
            //        blnValid = false;
            //        return;
            //    }
            //}
        }
        if (blnValid == true) {
            // Create FormData object
            var fileData = new FormData();
            for (var i = 0; i < files.length; i++) {
                fileData.append('uploadedFile', files[i]);
            }
            $.ajax({
                url: url,
                type: 'POST',
                data: fileData,
                //cache: false,
                contentType: false, // Not to set any content header
                processData: false, // Not to process data
                success: function (result) {
                    removeWhiteOverlay();
                    $("#add-job .data-div-placeholder-content").hide();
                    if (result.FileName != "" && result.FileName != undefined && result.FileName != null) {
                        addJobImageName(result.FileName);
                        showIconImageJob(result.FileName, result.MediaPath);
                        LoadProgressBarForJob(result.FileName, result.MediaPath);
                        //initVideofunction();
                        isJobPicUloaded = false;
                    }
                },
                error: function (err) {
                    removeWhiteOverlay();
                    ShowToastr('error', err, '');
                }
            });
        }
    }
    else {
        var msg = 'You have already selected one file';
        if (varLang == 'de') {
            msg = 'You have already selected one file';
        }
        ShowToastr('error', msg, '');
    }
   
}

function fnOpenAddJob(url) {
    displayWhiteOverlay();
    var url = url;
    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeWhiteOverlay();
            $('#partial_addEditJob').html(result);

            $.fancybox.open({
                src: '#add-job',
                type: 'inline',
                modal: true,
                helpers: {
                    overlay: { closeClick: false }
                }
            });

            //var jsdata = jQuery.fancybox.open(jQuery('#add-post'));
            //$(".fancybox-slide").unbind();
            initalizeJSFOrJob();
            BindAddressSearch();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}

function fnCropFileforJob(url) {
    displayWhiteOverlay();
    $("#add-job .ps-upload").addClass("hide");
    $("#add-job .loading-label").removeClass("hide");
    var cropcanvas = $image.cropper('getCroppedCanvas');
    var croppng = cropcanvas.toDataURL("image/png");

    var imagename = $("#jobexistFileName").val();
    var imageUrl = $("#jobexistFileUrl").val();

    // Create FormData object
    var formData = new FormData();
    formData.append('pngimageData', croppng);
    formData.append('filename', 'test.png');
    formData.append('existFileName', imagename);

    $.ajax({
        url: url,
        type: "POST",
        contentType: false, // Not to set any content header
        processData: false, // Not to process data
        data: formData,
        success: function (result) {
            removeWhiteOverlay();

            if (imagename != null && imagename != '' && imagename != undefined) {
                RemoveJobImageName(imagename);
            }

            addJobImageName(result.FileName);

            if (imageUrl != '' && imageUrl != null && imageUrl != undefined) {
                ChangeJobIconImage(imageUrl, result.MediaPath);
            }
            else {
                showIconImageJob(result.FileName, result.MediaPath);
            }
            $("#jobexistFileName").val('');
            $("#jobexistFileUrl").val('');

            $("#add-job .ps-upload").removeClass("hide");
            $("#add-job .loading-label").addClass("hide");
            //$('#image-container #canvas').removeClass('hide');

            //var msg = 'Image uploaded successfully.';
            //if (varLang == 'de') {
            //    msg = 'Bild wurde erfolgreich hochgeladen.';
            //}
            //ShowToastr('success', msg, '');

            $("#jobexistFileName").val(result.FileName);
            $("#jobexistFileUrl").val(result.MediaPath);
            //fnCropClear();

            //$("#image-container").hide();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "fail::" + err.statusText, '');
        }
    });
}

var lastUploadRow = [];
var progressbar = [];
var progressLabel = [];
var progressStatus = [];
var eventButton = [];

function LoadProgressBarForJob(imagename, imagePath, progressValue) {
    var imagename_array = imagename.split(',');
    var imagePath_array = imagePath.split(',');

    var uploading = 'Uploading..';
    var msgremove = 'Cancel';

    var uploaded = 'Uploaded';
    var remove = 'Remove';

    if (varLang == 'de') {
        uploading = 'Wird hochgeladen..';
        msgremove = 'beenden';

        uploaded = 'Hochgeladen';
        remove = 'Löschen';
    }

    for (var i = 0; i < imagename_array.length; i++) {
        if (imagename_array[i].length > 0) {
            (function (i) {

                //if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0) {
                //    var videofilepath = imagePath_array[i].split("/");
                //    imagePath_array[i] = imagePath_array[i].replace(videofilepath[videofilepath.length - 1], "DefaultVideoIcon.jpg");
                //}
                var htmlData = "";
                if (progressValue == true) {
                    htmlData = '<div class="upload-row ' + imagename_array[i] + '">' +
                        '<div class="' + imagename_array[i].toString().split('.')[0] + '">' +
                        '<div class="table">' +
                        '<div class="table-cell">' +
                        '<div class="up-img">';

                    if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0) {
                        htmlData += '<div class="wrapper" style="height: 64px;"><video class="video" Height="64"> <source src="' + imagePath_array[i] + '" type="video/mp4"><source src="' + imagePath_array[i] + '" type="video/ogg"></video>';
                        htmlData += '<div class="playpause"></div></div>';
                    }
                    else {
                        htmlData += '<img src="' + imagePath_array[i] + '" alt="">';
                    }
                    htmlData += '</div ></div > <div class="table-cell"><div class="up-content"> <div class="up-first in-progress"> ' + '<h6 class="progressStatus" style="font-weight: 500;color: #000;">' + uploaded + '</h6>' +
                        '<a class= "eventButton remove-up" href = "#" title = "Remove" onclick="return removeUploadedJobImage(this,' + i + ' , \'' + imagename_array[i] + '\', \'' + imagePath_array[i] + '\');">' + remove + '</a>' +
                        '</div > <div class="up-progress ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="100">                        <span style="width: 0%"></span>    <div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="display: block; width: 102%;"></div></div> <p class="up-count">100%</p> </div></div> </div> </div> </div>';
                    $("#add-job #jobupload .popup-middle").append(htmlData);
                }
                else {
                    htmlData = '<div class="upload-row ' + imagename_array[i] + '">' +
                        '<div class="' + imagename_array[i].toString().split('.')[0] + '">' +
                        '    <div class="table">' +
                        '        <div class="table-cell">' +
                        '            <div class="up-img">';

                    if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0) {
                        htmlData += '<div class="wrapper" style="height: 64px;"><video class="video" Height="64"> <source src="' + imagePath_array[i] + '" type="video/mp4"><source src="' + imagePath_array[i] + '" type="video/ogg"></video>';
                        htmlData += '<div class="playpause"></div></div>';
                    }
                    else {
                        htmlData += '<img src="' + imagePath_array[i] + '" alt="">';
                    }
                    htmlData += ' </div>' +
                        '            </div>' +
                        '            <div class="table-cell">' +
                        '                <div class="up-content">' +
                        '                    <div class="up-first in-progress">' +
                        '                        <h6 class="progressStatus">' + uploading + '</h6>' +
                        '                        <a class="eventButton cancel-up" href="#" title="Cancel" onclick="return removeUploadedJobImage(this,' + i + ' , \'' + imagename_array[i] + '\', \'' + imagePath_array[i] + '\');">' + msgremove + '</a>' +
                        '                    </div>' +
                        '                    <div class="up-progress">' +
                        '                        <span style="width: 0%"></span>' +
                        '                    </div>' +
                        '                    <p class="up-count">0%</p>' +
                        '                </div>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '    </div>';
                    $("#add-job #jobupload .popup-middle").append(htmlData);
                }
                //setTimeout(function () {
                lastUploadRow[i] = $("#add-job #jobupload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "");
                progressbar[i] = $("#add-job #jobupload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".up-progress");
                progressLabel[i] = $("#add-job #jobupload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".up-count");
                progressStatus[i] = $("#add-job #jobupload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".progressStatus");
                eventButton[i] = $("#add-job #jobupload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".eventButton");

                progressbar[i].show();
                $("#add-job #jobupload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".up-progress").progressbar({
                    //value: false,
                    change: function () {
                        if (progressValue != true) {
                            if (progressbar[i] != "") {
                                progressLabel[i].text(progressbar[i].progressbar("value") + "%");
                            }
                        }
                    },
                    complete: function () {
                        //progressbar.progressbar("value", 100);
                        var msg = 'Uploaded';
                        var msgremove = 'Remove';
                        if (varLang == 'de') {
                            msg = 'Hochgeladen';
                            msgremove = 'Löschen';
                        }
                        progressLabel[i].text("100%");
                        progressStatus[i].text(msg);
                        progressStatus[i].attr("style", "font-weight: 500;color: #000;");

                        eventButton[i].text(msgremove);
                        eventButton[i].attr("title", "Remove");
                        eventButton[i].removeClass("cancel-up");
                        eventButton[i].addClass("remove-up");

                        $('#add-job #jobnewFile').val('');
                        //$('#newFile').prop('disabled', false);
                    }
                });
                if (progressValue != true) {
                    myVarStop[i] = false;
                    function progress() {
                        if (myVarStop[i] == false) {
                            var val = progressbar[i].progressbar("value") || 0;
                            progressbar[i].progressbar("value", val + 1);
                            if (val < 99) {
                                myVar[i] = setTimeout(progress, 25);
                            }
                        }
                    }
                    myVar[i] = setTimeout(progress, 100);
                }
                //}, 1000 * i);
            })(i);
        }
    }
}

function addJobImageName(imagename) {
    var imagename_array = imagename.split(',');
    for (var i = 0; i < imagename_array.length; i++) {
        var hdnImage = $('#add-job #HiddenJobFileName').val();
        if (hdnImage != '') {
            hdnImage = hdnImage + ",";
        }
        hdnImage = hdnImage + imagename_array[i];
        $('#add-job #HiddenJobFileName').val(hdnImage);
    }
}


function removeUploadedJobImage(thisData, i, filename, imagePath) {

    myVarStop[i] = true;
    isJobPicUloaded = true;
    var dataEvent = $(thisData).attr("title");

    var msg = "Are you sure to " + dataEvent.toLowerCase() + " ?";
    var yesdelete = "Yes, " + dataEvent.toLowerCase() + " it!"
    if (varLang == 'de') {
        if (dataEvent.toLowerCase() == 'delete') {
            msg = "Willst du es löschen?";
            yesdelete = "Löschen!"
        }
        else {
            msg = "Willst du es löschen?";
            yesdelete = "Löschen!"
        }
    }

    swal({
        title: msg,
        //text: "Your will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: yesdelete,
        closeOnConfirm: false,
        closeOnCancel: false,
        allowOutsideClick: false
    },
        function (isConfirm) {
            if (isConfirm) {
                clearTimeout(myVar[i]);
                $(thisData).closest('.upload-row').remove();
                RemoveJobIconImage(imagePath);
                RemoveJobImageName(filename);

                var url = $("#hdnDeleteJobMediaUrl").val();
                var frmData = new FormData();
                frmData.append("filename", filename);
                $.ajax({
                    url: url,
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: frmData,
                    success: function (result) {
                        $('#jobnewFile').val('');
                    },
                    error: function (err) {
                        removeWhiteOverlay();
                        ShowToastr('error', "fail::" + err.statusText, '');
                    }
                });

                var msg = 'Deleted!';
                if (varLang == 'de') {
                    msg = 'Gelöscht!';
                }
                swal(msg, "", "success");
            } else {
                swal.close();
                myVarStop[i] = false;
                //function progress() {
                //    if (myVarStop[i] == false) {
                //        var val = progressbar.progressbar("value") || 0;
                //        progressbar.progressbar("value", val + 1);
                //        if (val < 99) {
                //            myVar[i] = setTimeout(progress, 25);
                //        }
                //    }
                //}
                //myVar[i] = setTimeout(progress, 100);


                function progress() {
                    if (myVarStop[i] == false) {
                        var val = progressbar[i].progressbar("value") || 0;
                        progressbar[i].progressbar("value", val + 1);
                        if (val < 99) {
                            myVar[i] = setTimeout(progress, 25);
                        }
                    }
                }
                myVar[i] = setTimeout(progress, 100);
            }
        });
}

function initalizeJSFOrJob() {
    //for tabs
    initalizeTabsForJob();
    //for image crop
    canvas = $("#add-job #jobcanvas");
    // Import image
    $inputImage = $('#add-job #inputImage');
    $image = $('#add-job #jobimage');
    context = canvas.get(0).getContext("2d");
    //if (canvas.get(0) != null) {
    //    context = canvas.get(0).getContext("2d");
    //}

    var HiddenJobFileName = $("#add-job #HiddenJobFileName").val();
    var HiddenJobFilePath = $("#add-job #HiddenJobFilePath").val();
    if (HiddenJobFileName != undefined && HiddenJobFileName != "" && HiddenJobFileName != null) {
        $("#add-job .data-div-placeholder-content").hide();
        showIconImageJob(HiddenJobFileName, HiddenJobFilePath);
        LoadProgressBarForJob(HiddenJobFileName, HiddenJobFilePath, true);
    }

    ////for tags
    //jQuery(".tm-input").tagsManager({
    //    //prefilled: $("#add-post #HiddenHashtags").val()
    //    prefilled: jQuery.makeArray($("#add-post #HiddenHashtags").val().split(","))
    //});

    //$("#HashtagsArray").keyup(function () {
    //    var hdnHashtagsArray = $("input[name='hidden-HashtagsArray']").val();
    //    if (hdnHashtagsArray != '' && hdnHashtagsArray != null && hdnHashtagsArray != undefined) {
    //        $('#HashtagsArray').removeAttr('placeholder');
    //    }
    //    else {
    //        var msg = 'Type hashtags here';
    //        if (varLang == 'de') {
    //            msg = 'Hier eingeben..';
    //        }
    //        $('#HashtagsArray').attr('placeholder', msg);
    //    }
    //});

    //$("#HashtagsArray").val("");
    //jQuery(".tm-input-select").tagsinput({
    //    //allowClear: true,
    //    tags: true,
    //    confirmKeys: [13, 32, 188]
    //});

    //var tags = $("#add-post #HiddenHashtags").val().split(",");
    //var $el = $('.tm-input-select');
    //for (var i = 0; i < tags.length; i++) {
    //    jQuery(".tm-input-select").tagsinput('add', tags[i]);
    //}

    //jQuery(".bootstrap-tagsinput input").on('keypress', function (e) {
    //    if (e.keyCode == 13) {
    //        e.keyCode = 188;
    //        e.preventDefault();
    //    };
    //});

    //jQuery(".bootstrap-tagsinput input").on('textInput', e => {
    //    var keyCode = e.originalEvent.data.charCodeAt(0);
    //    if (keyCode != undefined && keyCode != "" && keyCode == 32) {
    //        debugger
    //        var e = $.Event("keypress");
    //        e.keyCode = 13; // # Some key code value
    //        $('.bootstrap-tagsinput input').trigger(e);
    //    }
    //});

    //jQuery(".bootstrap-tagsinput input").on('keypress input', function (event) {
    //    if (event.type == "keypress") {
    //        //if (e.which != undefined && e.which != "" && e.which == 32) {
    //        //    debugger
    //        //    var e = $.Event("keypress");
    //        //    e.keyCode = 13; // # Some key code value
    //        //    $('.bootstrap-tagsinput input').trigger(e);
    //        //};
    //    }
    //});
}

function FormJobValidation() {
    var checked = $("#frm-add-job input:checked").length > 0;
    //var JobFileName = $("#HiddenJobFileName").val();
    //JobFileName = JobFileName.replace(",", "");

    var title = $('#Title').val();
    var price = $('#Price').val();
    var zipcode = $('#ZipCode').val();

    if (!checked) {
        var msg = 'Please select at least one service';
        if (varLang == 'de') {
            msg = 'Bitte wählen Sie mindestens einen Service aus';
        }
        ShowToastr('error', msg, '');
        return false;
    }
    else if (title == "") {

        var msg = 'Please enter title';
        if (varLang == 'de') {
            msg = 'Jobbezeichnung';
        }

        ShowToastr('error', msg, '');
        return false;
    }
    else if (price == "") {
        var msg = 'Please enter price';
        if (varLang == 'de') {
            msg = 'Budget eingeben';
        }

        ShowToastr('error', msg, '');
        return false;
    }
    else if (zipcode == "") {
        var msg = 'Please enter Zipcode';
        if (varLang == 'de') {
            msg = 'PLZ eingeben';
        }

        ShowToastr('error', msg, '');
        return false;
    }

    return true;
}

function confirmSaveJobData() {
    if ($("#frm-add-job #Id").val() == "0") {
        var hasvalue = 1;
        $("#add-job input[type='text']").each(function () {
            var empty = $(this).val().length;
            if (empty > 0) { hasvalue = 1 };
        });

        $("#add-job .checkvalue").each(function () {
            var empty = $(this).val().length;
            if (empty > 0) { hasvalue = 1 };
        });

        //$("#add-post input[name='hidden-HashtagsArray']").each(function () {
        //    var empty = $(this).val().length;
        //    if (empty > 0) { hasvalue = 1 };
        //});

        $("#add-job input[type='checkbox']:checked").each(function () {
            hasvalue = 1;
        });

        if (hasvalue == 1) {
            var msg = 'Are you sure to leave the job data?';
            var yesdelete = 'Yes, remove it!'
            if (varLang == 'de') {
                msg = 'Willst du den Job löschen?';
                yesdelete = 'Löschen!';
            }

            swal({
                title: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: yesdelete,
                closeOnConfirm: false,
                closeOnCancel: false,
                allowOutsideClick: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        isJobPicUloaded = true;
                        $.fancybox.close();
                        swal.close();
                    } else {
                        swal.close();
                        return false;
                    }
                });

            //if (confirm('Are you sure to leave the post data?')) {
            //    $.fancybox.close();
            //}
            //else {
            //    return false;
            //}
        }
        else { $.fancybox.close(); }
    }
    else {
        $.fancybox.close();
    }
}

//window.onbeforeunload = confirmJobExit;
function confirmJobExit() {
    var hasvalue = 0;
    $("#add-job input[type='text']").each(function () {
        var empty = $(this).val().length;
        if (empty > 0) { hasvalue = 1 };
    });

    $("#add-job textarea").each(function () {
        var empty = $(this).val().length;
        if (empty > 0) { hasvalue = 1 };
    });

    $("#add-job .checkvalue").each(function () {
        var empty = $(this).val().length;
        if (empty > 0) { hasvalue = 1 };
    });

    //$("#add-post input[name='hidden-HashtagsArray']").each(function () {
    //    var empty = $(this).val().length;
    //    if (empty > 0) { hasvalue = 1 };
    //});

    $("#add-job input[type='checkbox']:checked").each(function () {
        hasvalue = 1;
    });

    if (hasvalue == 1) {

        //swal({
        //    title: "You have attempted to leave this page. Are you sure?",
        //    type: "warning",
        //    showCancelButton: true,
        //    confirmButtonClass: "btn-danger",
        //    confirmButtonText: "Yes, remove it!",
        //    closeOnConfirm: false,
        //    closeOnCancel: false,
        //    allowOutsideClick: false
        //},
        //    function (isConfirm) {
        //        if (isConfirm) {
        //            swal.close();
        //            return true;
        //        } else {
        //            swal.close();
        //            return false;
        //        }
        //    });

        return "You have attempted to leave this page. Are you sure?";
    }
}

$(document).on('click', ".uploadsec", function () {
    $("#add-job #jobcrop").hide();
    $("#add-job #jobdescription").hide();
});

$(document).on('click', "#nextcrop", function () {
    $("#add-job #jobcrop").hide();
    $("#add-job #jobdescription").show();
    $("#add-job #jobdescription").addClass('active');
    $('#Price').val('');
    $('#ZipCode').val('');
});

$(document).on('click', "#skip", function () {
    $("#add-job #jobupload").hide();
    $("#add-job #jobcrop").hide();
    $("#add-job #jobdescription").show();
    $('.popup-tabs li a').addClass('active');
    $('#Price').val('');
    $('#ZipCode').val('');
});

$(document).on('click', '.checkjobFileUpload', function () {
    if ($(".crop-max").length <= 0) {
        var msg = 'Please upload at least one file';
        if (varLang == 'de') {
            msg = 'Bitte Foto/Video auswählen';
        }

        swal({
            title: msg,
            //text: "Please upload image with size 304X306",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            allowOutsideClick: false
        });

        $('.popup-tabs li:nth-child(1) a').trigger('click');
        return false;
    }
    else {
        var litab = $(this).attr("title");

        if (litab == "Description" || litab == "Detail") {
            $('.popup-tabs li a').addClass('active');

            $("#add-job #jobupload").hide();
            $("#add-job #jobcrop").hide();
            $("#add-job #jobdescription").show();
        }
        else {
            $('#add-job .popup-tabs li:first-child a').addClass('active');
            $('#add-job .popup-tabs li:nth-child(2) a').addClass('active');
            $("#add-job #jobcrop").show();
            $("#add-job #jobupload").hide();
            $("#add-job #jobdescription").hide();


            var $image = $('#add-job #jobimage');
            $image.cropper('destroy').attr('src', '');
            $('.img-upload-max').removeClass('hide');
            $('#jobimage-container').hide();

            //var img = $("#add-post #image-container").find("#image").attr("src");
            //if (img == null || img == undefined || img == '') {
            //    $("#add-post .crop-max").first().click();
            //}
            $("#add-job .crop-max img").first().parent().click();
        }
        return false;
    }
});

function RemoveJobImageName(filename) {
    var hdnImage = $('#add-job #HiddenJobFileName').val();
    if (hdnImage != '') {
        hdnImage = hdnImage.replace(filename, "");
        $('#add-job #HiddenJobFileName').val(hdnImage);
    }
}

function showIconImageJob(imagename, hdnCropImagePath) {
    var imagename_array = imagename.split(",");
    var imagePath_array = hdnCropImagePath.split(",");
    for (var i = 0; i < imagename_array.length; i++) {
        if (imagename_array[i].length > 0) {
            var htmlData = "";
            if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0) {
                //var videofilepath = imagePath_array[i].split("/");
                //imagePath_array[i] = imagePath_array[i].replace(videofilepath[videofilepath.length - 1], "DefaultVideoIcon.jpg");
                htmlData = '<div class="crop-max"><div class="wrapper" style="height: 64px;"><video class="video" Height="64"><source src = "' + imagePath_array[i].toString() + '" type = "video/mp4"><source src="' + imagePath_array[i].toString() + '" type="video/ogg"></video><div class="playpause"></div></div></div>';
            }
            else {
                htmlData = '<div class="crop-max" onclick="editJobCropImage(this, \'' + imagename_array[i] + '\');"><img style="height:64px;width:64px;" src="' + imagePath_array[i] + '" alt=""><input type="hidden" class="uplodedimage" value="' + imagePath_array[i] + '" /></div>';
            }
            $("#add-job #job-crop-image-array").append(htmlData);
        }
    }
}
//$(document).on('change', '#ZipCode', function ()
//{
//    alert('');
//    BindAddressSearch();
//})

function RemoveJobIconImage(imageUrl) {
    $("#add-job #job-crop-image-array").find("img[src='" + imageUrl + "']").closest(".crop-max").remove();
    $("#add-job #job-crop-image-array .wrapper").find("source[src='" + imageUrl + "']").closest(".crop-max").remove();
    $("#add-job #job-crop-image-array").find(".uplodedimage[value='" + imageUrl + "']").closest(".crop-max").remove();

    if ($("#add-job .crop-max").length <= 0) {
        $("#add-job .data-div-placeholder-content").show();
    }
}

function ChangeJobIconImage(imageUrl, newUrl) {
    var img = $("#add-job #job-crop-image-array").find("img[src='" + imageUrl + "']");
    img.attr("src", newUrl);
    //img.closest(".crop-max").attr("onclick", "editCropImage(this, '" + newUrl + "');");
    //$(".eventButton").attr("");
}


function editJobCropImage(thisData, imagename) {

    var img = $(thisData).find("img");
    var imageUrl = img.attr("src");
    $("#jovexistFileName").val(imagename);
    $("#jobexistFileUrl").val(imageUrl);
    //imageUrl = imageUrl.replace("localhost", "192.168.0.102");

    //if (imageUrl.indexOf("https") < 0) {
    //    imageUrl = imageUrl.replace("http", "https");
    //}

    var xhr = new XMLHttpRequest();
    xhr.open('GET', imageUrl, true);

    xhr.responseType = 'arraybuffer';

    xhr.onload = function (e) {
        if (this.status == 200) {
            var uInt8Array = new Uint8Array(this.response);
            var i = uInt8Array.length;
            var binaryString = new Array(i);
            while (i--) {
                binaryString[i] = String.fromCharCode(uInt8Array[i]);
            }
            var data = binaryString.join('');

            var base64 = window.btoa(data);

            var uploadedImageURL = "data:image/png;base64," + base64;

            //$('.crop-image-section .upload-button').addClass('hide');
            $('#add-job .ps-upload').removeClass('hide');
            $('#add-job #jobcanvas').addClass('hide');
            var options = {
                aspectRatio: 1 / 1,
                minCropBoxWidth: 304,
                minCropBoxHeight: 304,
                width: 304,
                height: 304,
                viewMode: 3
            };
            var img = new Image();
            img.onload = function () {
                context.canvas.height = img.height;
                context.canvas.width = img.width;
                context.drawImage(img, 0, 0);
                var cropper = canvas.cropper({
                    aspectRatio: 1 / 1,
                    minCropBoxWidth: 304,
                    minCropBoxHeight: 304,
                    width: 304,
                    height: 304,
                    viewMode: 3
                });
            };
            $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
        }
    };

    xhr.send();

    $(".addpost-image-container").show();
}

function rotateLeft() {
    $image.cropper('rotate', -90);
}

function rotateRight() {
    $image.cropper('rotate', 90);
}

function fnCropClear() {
    $image.cropper('reset');
    //$(".ps-upload").addClass("hide");
    //$('#add-post #canvas').removeClass('hide');

    //$(".cropper-container").hide();
    //$("#add-post #image-container").hide();
};

function OnFailure(response) {
    ShowToastr('error', response, '');
}

function editJob(url) {
    displayWhiteOverlay();
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeWhiteOverlay();
            $('#partial_addEditJob').html(result);

            $.fancybox.open({
                src: '#add-job',
                type: 'inline',
                modal: true,
                helpers: {
                    overlay: { closeClick: false }
                }
            });

            //var jsdata = jQuery.fancybox.open(jQuery('#add-post'));
            //$(".fancybox-slide").unbind();
            initalizeJSFOrJob();
            BindAddressSearch();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}

function ActiveTogglejs(url) {
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            if (result.Status == '1') {
                OnSuccessJobStatus(result);
            }
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}





