﻿var cropper;
var canvas;
var uploadedImageURL;

// Import image
var $inputImage;
var $image;
var context;
var myVar = [];
var myVarStop = [];

$(document).on('change', '#fileInput', function () {
    //$('.crop-image-section .upload-button').addClass('hide');

    var URL = window.URL || window.webkitURL;
    if (this.files && this.files[0]) {
        if (this.files[0].type.match(/^image\//)) {
            displayWhiteOverlay();
            $('#add-post .ps-upload').removeClass('hide');
            $('#image-container #canvas').addClass('hide');
            var files = this.files;
            var file;

            if (files && files.length) {
                file = files[0];
                var $image = $('#add-post #image');

                if (uploadedImageURL) {
                    URL.revokeObjectURL(uploadedImageURL);
                }

                var options = {
                    //aspectRatio: 1 / 1
                    aspectRatio: 1 / 1,
                    minCropBoxWidth: 304,
                    minCropBoxHeight: 304,
                    viewMode: 3,
                    built: function () {
                        // Strict mode: set crop box data first
                        var container = $(this).cropper('getContainerData');
                        $(this).cropper('setCropBoxData', {
                            width: 304,
                            height: 304,
                            left: (container.width - cropBoxWidth) / 2,
                            top: (container.height - cropBoxHeight) / 2
                        });
                    }
                };
                var img = new Image();
                img.onload = function () {
                    context.canvas.height = img.height;
                    context.canvas.width = img.width;
                    context.drawImage(img, 0, 0);
                    var cropper = canvas.cropper({
                        aspectRatio: 1 / 1,
                        minCropBoxWidth: 304,
                        minCropBoxHeight: 304,
                    });
                };
                uploadedImageURL = URL.createObjectURL(file);
                $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                $("#add-post #image-container").show();

            }
            removeWhiteOverlay();
        }
        else {
            uploadVideoFile();
        }
    }
    else {
        var msg = 'No file(s) selected.';
        if (varLang == 'de') {
            msg = 'Keine Datei (en) ausgewählt';
        }
        ShowToastr('error', msg, '');
    }
});

function fnUploadNewFile(url) {
    DispalyLoaderPost();
    $("#add-post .data-div-placeholder-content").hide();

    var fileUpload = $("#newFile").get(0);
    var files = fileUpload.files;
    var blnValid = true;
    for (var i = 0; i < files.length; i++) {

        var _size = files[i].size;
        var exactSize = (_size / (1024 * 1024)).toFixed(2);

        //var fSExt = new Array('Bytes', 'KB', 'MB', 'GB'),
        //j = 0; while (_size > 900) { _size /= 1024; j++; }
        //var exactSize = Math.round(_size * 100) / 100;
        //exactSize = exactSize / 1024;

        var file = files[i];
        var fileType = file["type"];
        var ValidImageTypes = ["image/gif", "image/jpeg", "image/png"];
        if ($.inArray(fileType, ValidImageTypes) < 0) {
            //file is video
            displayWhiteOverlay();
            if (exactSize > 50) {
                var errormsg = "Please upload less than 50mb video";
                if (varLang == "de") {
                    errormsg = "Max 50MB hochladen";
                }
                ShowToastr("error", errormsg, "");
                blnValid = false;
                removeWhiteOverlay();
                return;
            }
        }
        else {
            if (exactSize > 6) {
                var errormsg = "Please upload less than 2mb image";
                if (varLang == "de") {
                    errormsg = "Max 2MB hochladen";
                }
                ShowToastr("error", errormsg, "");
                blnValid = false;
                removeWhiteOverlay();
                return;
            }
        }

        //if (files[i].type.match(/^image\//)) {
        //    //file is video
        //    if (exactSize > 2) {
        //        ShowToastr("error", "Please upload less than 2mb image", "");
        //        blnValid = false;
        //        return;
        //    }
        //}
        //else {
        //    //file is video
        //    if (exactSize > 10) {
        //        ShowToastr("error", "Please upload less than 10mb file", "");
        //        blnValid = false;
        //        return;
        //    }
        //}
    }
    if (blnValid == true) {
        // Create FormData object
        var fileData = new FormData();
        for (var i = 0; i < files.length; i++) {
            fileData.append('uploadedFile', files[i]);
        }
        $.ajax({
            url: url,
            type: 'POST',
            data: fileData,
            //cache: false,
            contentType: false, // Not to set any content header
            processData: false, // Not to process data
            success: function (result) {
                removeWhiteOverlay();
                $("#add-post .data-div-placeholder-content").hide();
                if (result.FileName != "" && result.FileName != undefined && result.FileName != null) {
                    addImageName(result.FileName)
                    showIconImage(result.FileName, result.MediaPath);
                    LoadProgressBar(result.FileName, result.MediaPath);
                    //initVideofunction();
                }
            },
            error: function (err) {
                removeWhiteOverlay();
                ShowToastr('error', err, '');
            }
        });
    }
}

function fnCropFile(url) {
    displayWhiteOverlay();

    $("#add-post .ps-upload").addClass("hide");
    $("#add-post .loading-label").removeClass("hide");
    var cropcanvas = $image.cropper('getCroppedCanvas');
    var croppng = cropcanvas.toDataURL("image/png");

    var imagename = $("#existFileName").val();
    var imageUrl = $("#existFileUrl").val();

    // Create FormData object
    var formData = new FormData();
    formData.append('pngimageData', croppng);
    formData.append('filename', 'test.png');
    formData.append('existFileName', imagename);

    $.ajax({
        url: url,
        type: "POST",
        contentType: false, // Not to set any content header
        processData: false, // Not to process data
        data: formData,
        success: function (result) {
            removeWhiteOverlay();

            if (imagename != null && imagename != '' && imagename != undefined) {
                RemoveImageName(imagename);
            }
            addImageName(result.FileName);

            if (imageUrl != '' && imageUrl != null && imageUrl != undefined) {
                ChangeIconImage(imageUrl, result.MediaPath);
            }
            else {
                showIconImage(result.FileName, result.MediaPath);
            }
            $("#existFileName").val('');
            $("#existFileUrl").val('');

            $("#add-post .ps-upload").removeClass("hide");
            $("#add-post .loading-label").addClass("hide");
            //$('#image-container #canvas').removeClass('hide');

            //var msg = 'Image uploaded successfully.';
            //if (varLang == 'de') {
            //    msg = 'Bild wurde erfolgreich hochgeladen.';
            //}
            //ShowToastr('success', msg, '');

            $("#existFileName").val(result.FileName);
            $("#existFileUrl").val(result.MediaPath);
            //fnCropClear();

            //$("#image-container").hide();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "fail::" + err.statusText, '');
        }
    });
};

var lastUploadRow = [];
var progressbar = [];
var progressLabel = [];
var progressStatus = [];
var eventButton = [];

function LoadProgressBar(imagename, imagePath, progressValue) {
    
    var imagename_array = imagename.split(',');
    var imagePath_array = imagePath.split(',');

    var uploading = 'Uploading..';
    var msgremove = 'Cancel';

    var uploaded = 'Uploaded';
    var remove = 'Remove';

    if (varLang == 'de') {
        uploading = 'Wird hochgeladen..';
        msgremove = 'beenden';

        uploaded = 'Hochgeladen';
        remove = 'Löschen';
    }


    for (var i = 0; i < imagename_array.length; i++) {
        if (imagename_array[i].length > 0) {
            (function (i) {

                //if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0) {
                //    var videofilepath = imagePath_array[i].split("/");
                //    imagePath_array[i] = imagePath_array[i].replace(videofilepath[videofilepath.length - 1], "DefaultVideoIcon.jpg");
                //}
                var htmlData = "";
                if (progressValue == true) {
                    htmlData = '<div class="upload-row ' + imagename_array[i] + '">' +
                        '<div class="' + imagename_array[i].toString().split('.')[0] + '">' +
                        '<div class="table">' +
                        '<div class="table-cell">' +
                        '<div class="up-img">';

                    if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0 || imagePath_array[i].toString().indexOf(".MOV") >= 0 || imagePath_array[i].toString().indexOf(".mov") >= 0) {
                        htmlData += '<div class="wrapper" style="height: 64px;"><video class="video" Height="64"> <source src="' + imagePath_array[i] + '" type="video/mp4"><source src="' + imagePath_array[i] + '" type="video/ogg"></video>';
                        htmlData += '<div class="playpause"></div></div>';
                    }
                    else {
                        htmlData += '<img src="' + imagePath_array[i] + '" alt="">';
                    }
                    htmlData += '</div ></div > <div class="table-cell"><div class="up-content"> <div class="up-first in-progress"> ' + '<h6 class="progressStatus" style="font-weight: 500;color: #000;">' + uploaded + '</h6>' +
                        '<a class= "eventButton remove-up" href = "#" title = "Remove" onclick="return removeUploadedImage(this,' + i + ' , \'' + imagename_array[i] + '\', \'' + imagePath_array[i] + '\');">' + remove + '</a>' +
                        '</div > <div class="up-progress ui-progressbar ui-widget ui-widget-content ui-corner-all" role="progressbar" aria-valuemin="0" aria-valuemax="100" aria-valuenow="100">                        <span style="width: 0%"></span>    <div class="ui-progressbar-value ui-widget-header ui-corner-left ui-corner-right" style="display: block; width: 102%;"></div></div> <p class="up-count">100%</p> </div></div> </div> </div> </div>';
                    $("#add-post #upload .popup-middle").append(htmlData);
                }
                else {
                    htmlData = '<div class="upload-row ' + imagename_array[i] + '">' +
                        '<div class="' + imagename_array[i].toString().split('.')[0] + '">' +
                        '    <div class="table">' +
                        '        <div class="table-cell">' +
                        '            <div class="up-img">';

                    if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0 || imagePath_array[i].toString().indexOf(".MOV") >= 0 || imagePath_array[i].toString().indexOf(".mov") >= 0) {
                        htmlData += '<div class="wrapper" style="height: 64px;"><video class="video" Height="64"> <source src="' + imagePath_array[i] + '" type="video/mp4"><source src="' + imagePath_array[i] + '" type="video/ogg"></video>';
                        htmlData += '<div class="playpause"></div></div>';
                    }
                    else {
                        htmlData += '<img src="' + imagePath_array[i] + '" alt="">';
                    }
                    htmlData += ' </div>' +
                        '            </div>' +
                        '            <div class="table-cell">' +
                        '                <div class="up-content">' +
                        '                    <div class="up-first in-progress">' +
                        '                        <h6 class="progressStatus">' + uploading + '</h6>' +
                        '                        <a class="eventButton cancel-up" href="#" title="Cancel" onclick="return removeUploadedImage(this,' + i + ' , \'' + imagename_array[i] + '\', \'' + imagePath_array[i] + '\');">' + msgremove + '</a>' +
                        '                    </div>' +
                        '                    <div class="up-progress">' +
                        '                        <span style="width: 0%"></span>' +
                        '                    </div>' +
                        '                    <p class="up-count">0%</p>' +
                        '                </div>' +
                        '            </div>' +
                        '        </div>' +
                        '    </div>' +
                        '    </div>';
                    $("#add-post #upload .popup-middle").append(htmlData);
                }
                //setTimeout(function () {
                lastUploadRow[i] = $("#add-post #upload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "");
                progressbar[i] = $("#add-post #upload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".up-progress");
                progressLabel[i] = $("#add-post #upload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".up-count");
                progressStatus[i] = $("#add-post #upload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".progressStatus");
                eventButton[i] = $("#add-post #upload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".eventButton");

                progressbar[i].show();
                $("#add-post #upload .popup-middle ." + imagename_array[i].toString().split('.')[0] + "").find(".up-progress").progressbar({
                    //value: false,
                    change: function () {
                        if (progressValue != true) {
                            if (progressbar[i] != "") {
                                progressLabel[i].text(progressbar[i].progressbar("value") + "%");
                            }
                        }
                    },
                    complete: function () {
                        //progressbar.progressbar("value", 100);
                        var msg = 'Uploaded';
                        var msgremove = 'Remove';
                        if (varLang == 'de') {
                            msg = 'Hochgeladen';
                            msgremove = 'Löschen';
                        }
                        progressLabel[i].text("100%");
                        progressStatus[i].text(msg);
                        progressStatus[i].attr("style", "font-weight: 500;color: #000;");

                        eventButton[i].text(msgremove);
                        eventButton[i].attr("title", "Remove");
                        eventButton[i].removeClass("cancel-up");
                        eventButton[i].addClass("remove-up");

                        $('#add-post #newFile').val('');
                        //$('#newFile').prop('disabled', false);
                    }
                });
                if (progressValue != true) {
                    myVarStop[i] = false;
                    function progress() {
                        if (myVarStop[i] == false) {
                            var val = progressbar[i].progressbar("value") || 0;
                            progressbar[i].progressbar("value", val + 1);
                            if (val < 99) {
                                myVar[i] = setTimeout(progress, 25);
                            }
                        }
                    }
                    myVar[i] = setTimeout(progress, 100);
                }
                //}, 1000 * i);
            })(i);
        }
    }
}

function addImageName(imagename) {
    var imagename_array = imagename.split(',');
    for (var i = 0; i < imagename_array.length; i++) {
        var hdnImage = $('#add-post #HiddenPostFileName').val();
        if (hdnImage != '') {
            hdnImage = hdnImage + ",";
        }
        hdnImage = hdnImage + imagename_array[i];
        $('#add-post #HiddenPostFileName').val(hdnImage);
    }
}

$(document).on('click', '.wrapper', function () {
    if ($(this).children(".video").get(0).paused) {
        $(this).children(".video").get(0).play();
        $(this).children(".playpause").fadeOut();
    } else {
        $(this).children(".video").get(0).pause();
        $(this).children(".playpause").fadeIn();
    }
});

//function initVideofunction() {
//    $('.video').parent().click(function () {
//        if ($(this).children(".video").get(0).paused) {
//            $(this).children(".video").get(0).play();
//            $(this).children(".playpause").fadeOut();
//        } else {
//            $(this).children(".video").get(0).pause();
//            $(this).children(".playpause").fadeIn();
//        }
//    });
//}

function RemoveImageName(filename) {
    var hdnImage = $('#add-post #HiddenPostFileName').val();
    if (hdnImage != '') {
        hdnImage = hdnImage.replace(filename, "");
        $('#add-post #HiddenPostFileName').val(hdnImage);
    }
}

function showIconImage(imagename, hdnCropImagePath) {  
    var imagename_array = imagename.split(",");
    var imagePath_array = hdnCropImagePath.split(",");
    for (var i = 0; i < imagename_array.length; i++) {
        if (imagename_array[i].length > 0) {
            var htmlData = "";
            if (imagePath_array[i].toString().indexOf(".mp4") >= 0 || imagePath_array[i].toString().indexOf(".wmv") >= 0 || imagePath_array[i].toString().indexOf(".mov") >= 0 || imagePath_array[i].toString().indexOf(".MOV") >= 0) {
                //var videofilepath = imagePath_array[i].split("/");
                //imagePath_array[i] = imagePath_array[i].replace(videofilepath[videofilepath.length - 1], "DefaultVideoIcon.jpg");
                htmlData = '<div class="crop-max"><div class="wrapper" style="height: 64px;"><video class="video" Height="64"><source src = "' + imagePath_array[i].toString() + '" type = "video/mp4"><source src="' + imagePath_array[i].toString() + '" type="video/ogg"></video><div class="playpause"></div></div></div>';
            }
            else {
                htmlData = '<div class="crop-max" onclick="editCropImage(this, \'' + imagename_array[i] + '\');"><img style="height:64px;width:64px;" src="' + imagePath_array[i] + '" alt=""><input type="hidden" class="uplodedimage" value="' + imagePath_array[i] + '" /></div>';
            }
            $("#add-post #crop-image-array").append(htmlData);
        }
    }
}

function RemoveIconImage(imageUrl) {
    $("#add-post #crop-image-array").find("img[src='" + imageUrl + "']").closest(".crop-max").remove();
    $("#add-post #crop-image-array .wrapper").find("source[src='" + imageUrl + "']").closest(".crop-max").remove();
    $("#add-post #crop-image-array").find(".uplodedimage[value='" + imageUrl + "']").closest(".crop-max").remove();

    if ($("#add-post .crop-max").length <= 0) {
        $("#add-post .data-div-placeholder-content").show();
    }
}

function ChangeIconImage(imageUrl, newUrl) {
    var img = $("#add-post #crop-image-array").find("img[src='" + imageUrl + "']");
    img.attr("src", newUrl);
    //img.closest(".crop-max").attr("onclick", "editCropImage(this, '" + newUrl + "');");
    //$(".eventButton").attr("");
}

function editCropImage(thisData, imagename) {

    var img = $(thisData).find("img");
    var imageUrl = img.attr("src");
    $("#existFileName").val(imagename);
    $("#existFileUrl").val(imageUrl);
    //imageUrl = imageUrl.replace("localhost", "192.168.0.102");

    //if (imageUrl.indexOf("https") < 0) {
    //    imageUrl = imageUrl.replace("http", "https");
    //}

    var xhr = new XMLHttpRequest();
    xhr.open('GET', imageUrl, true);

    xhr.responseType = 'arraybuffer';

    xhr.onload = function (e) {
        if (this.status == 200) {
            var uInt8Array = new Uint8Array(this.response);
            var i = uInt8Array.length;
            var binaryString = new Array(i);
            while (i--) {
                binaryString[i] = String.fromCharCode(uInt8Array[i]);
            }
            var data = binaryString.join('');

            var base64 = window.btoa(data);

            var uploadedImageURL = "data:image/png;base64," + base64;

            //$('.crop-image-section .upload-button').addClass('hide');
            $('#add-post .ps-upload').removeClass('hide');
            $('#add-post #canvas').addClass('hide');
            var options = {
                aspectRatio: 1 / 1,
                minCropBoxWidth: 304,
                minCropBoxHeight: 304,
                width: 304,
                height: 304,
                viewMode: 3
            };
            var img = new Image();
            img.onload = function () {
                context.canvas.height = img.height;
                context.canvas.width = img.width;
                context.drawImage(img, 0, 0);
                var cropper = canvas.cropper({
                    aspectRatio: 1 / 1,
                    minCropBoxWidth: 304,
                    minCropBoxHeight: 304,
                    width: 304,
                    height: 304,
                    viewMode: 3
                });
            };
            $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
        }
    };

    xhr.send();

    $(".addpost-image-container").show();
}

function rotateLeft() {
    $image.cropper('rotate', -90);
}

function rotateRight() {
    $image.cropper('rotate', 90);
}

function fnCropClear() {
    $image.cropper('reset');
    //$(".ps-upload").addClass("hide");
    //$('#add-post #canvas').removeClass('hide');

    //$(".cropper-container").hide();
    //$("#add-post #image-container").hide();
};

function OnFailure(response) {
    ShowToastr('error', response, '');
}

function clearForm() {
    $("#Description").val("");
    $("#add-post input[type='checkbox']").prop('checked', false);
    $("#add-post .crop-max").each(function () { $(this).remove() });
    fnCropClear();
    $("#add-post input:hidden[id='Id']").val('0');

    $("#add-post .popup-tabs ul li a[href='#upload']").click();
    $("a[href='#crop']").removeClass("active");
    $("a[href='#discription']").removeClass("active");
}

function FormPostValidation() {
    var checked = $("#frm-add-post input:checked").length > 0;
    var postFileName = $("#HiddenPostFileName").val();
    postFileName = postFileName.replace(",", "");

    if (!checked) {
        var msg = 'Please select at least one service';
        if (varLang == 'de') {
            msg = 'Bitte wählen Sie mindestens einen Service aus';
        }
        ShowToastr('error', msg, '');
        return false;
    }
    else if (postFileName == "") {

        var msg = 'Please upload at least one file';
        if (varLang == 'de') {
            msg = 'Bitte lade mindestens eine Datei hoch';
        }

        ShowToastr('error', msg, '');
        return false;
    }
    return true;
}

function removeUploadedImage(thisData, i, filename, imagePath) {
    //clearTimeout(myVar[i]);
    myVarStop[i] = true;

    var dataEvent = $(thisData).attr("title");

    var msg = "Are you sure to " + dataEvent.toLowerCase() + " ?";
    var yesdelete = "Yes, " + dataEvent.toLowerCase() + " it!"
    if (varLang == 'de') {
        if (dataEvent.toLowerCase() == 'delete') {
            msg = "Willst du es löschen?";
            yesdelete = "Löschen!"
        }
        else {
            msg = "Willst du es löschen?";
            yesdelete = "Löschen!"
        }
    }

    swal({
        title: msg,
        //text: "Your will not be able to recover this file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: yesdelete,
        closeOnConfirm: false,
        closeOnCancel: false,
        allowOutsideClick: false
    },
        function (isConfirm) {
            if (isConfirm) {
                clearTimeout(myVar[i]);
                $(thisData).closest('.upload-row').remove();
                RemoveIconImage(imagePath);
                RemoveImageName(filename);

                var url = $("#hdnDeleteMediaUrl").val();
                var frmData = new FormData();
                frmData.append("filename", filename);
                $.ajax({
                    url: url,
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: frmData,
                    success: function (result) {

                    },
                    error: function (err) {
                        removeWhiteOverlay();
                        ShowToastr('error', "fail::" + err.statusText, '');
                    }
                });

                var msg = 'Deleted!';
                if (varLang == 'de') {
                    msg = 'Gelöscht!';
                }
                swal(msg, "", "success");
            } else {
                swal.close();
                myVarStop[i] = false;
                //function progress() {
                //    if (myVarStop[i] == false) {
                //        var val = progressbar.progressbar("value") || 0;
                //        progressbar.progressbar("value", val + 1);
                //        if (val < 99) {
                //            myVar[i] = setTimeout(progress, 25);
                //        }
                //    }
                //}
                //myVar[i] = setTimeout(progress, 100);


                function progress() {
                    if (myVarStop[i] == false) {
                        var val = progressbar[i].progressbar("value") || 0;
                        progressbar[i].progressbar("value", val + 1);
                        if (val < 99) {
                            myVar[i] = setTimeout(progress, 25);
                        }
                    }
                }
                myVar[i] = setTimeout(progress, 100);
            }
        });
}

function confirmSaveData() {
    if ($("#frm-add-post #Id").val() == "0") {
        var hasvalue = 1;
        $("#add-post input[type='text']").each(function () {           
            var empty = $(this).val().length;           
            if (empty > 0) { hasvalue = 1; }
        });

        $("#add-post textarea").each(function () {
            var empty = $(this).val().length;            
            if (empty > 0) { hasvalue = 1; }
        });

        $("#add-post .checkvalue").each(function () {
            var empty = $(this).val().length;            
            if (empty > 0) { hasvalue = 1; }
        });

        //$("#add-post input[name='hidden-HashtagsArray']").each(function () {
        //    var empty = $(this).val().length;
        //    if (empty > 0) { hasvalue = 1 };
        //});

        $("#add-post input[type='checkbox']:checked").each(function () {
            hasvalue = 1;
        });

        if (hasvalue == 1) {           
            var msg = 'Are you sure to leave the post data?';
            var yesdelete = 'Yes, remove it!';
            if (varLang == 'de') {
                msg = 'Willst du den Beitrag löschen?';
                yesdelete = 'Löschen!';
            }

            swal({
                title: msg,
                type: "warning",
                showCancelButton: true,
                confirmButtonClass: "btn-danger",
                confirmButtonText: yesdelete,
                closeOnConfirm: false,
                closeOnCancel: false,
                allowOutsideClick: false
            },
                function (isConfirm) {
                    if (isConfirm) {
                        $.fancybox.close();
                        swal.close();
                    } else {
                        swal.close();
                        return false;
                    }
                });

            //if (confirm('Are you sure to leave the post data?')) {
            //    $.fancybox.close();
            //}
            //else {
            //    return false;
            //}
        }
        else { $.fancybox.close(); }
    }
    else {
        $.fancybox.close();
    }
}

window.onbeforeunload = confirmExit;
function confirmExit() {
    var hasvalue = 0;
    $("#add-post input[type='text']").each(function () {
        var empty = $(this).val().length;
        if (empty > 0) { hasvalue = 1 };
    });

    $("#add-post textarea").each(function () {
        var empty = $(this).val().length;
        if (empty > 0) { hasvalue = 1 };
    });

    $("#add-post .checkvalue").each(function () {
        var empty = $(this).val().length;
        if (empty > 0) { hasvalue = 1 };
    });

    //$("#add-post input[name='hidden-HashtagsArray']").each(function () {
    //    var empty = $(this).val().length;
    //    if (empty > 0) { hasvalue = 1 };
    //});

    $("#add-post input[type='checkbox']:checked").each(function () {
        hasvalue = 1;
    });

    if (hasvalue == 1) {

        //swal({
        //    title: "You have attempted to leave this page. Are you sure?",
        //    type: "warning",
        //    showCancelButton: true,
        //    confirmButtonClass: "btn-danger",
        //    confirmButtonText: "Yes, remove it!",
        //    closeOnConfirm: false,
        //    closeOnCancel: false,
        //    allowOutsideClick: false
        //},
        //    function (isConfirm) {
        //        if (isConfirm) {
        //            swal.close();
        //            return true;
        //        } else {
        //            swal.close();
        //            return false;
        //        }
        //    });

        return "You have attempted to leave this page. Are you sure?";
    }
}

function fnOpenAddPost(url) {
    displayWhiteOverlay();
    var url = url;
    $.ajax({
        type: 'GET',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeWhiteOverlay();
            $('#partial_addEditPost').html(result);

            $.fancybox.open({
                src: '#add-post',
                type: 'inline',
                modal: true,
                helpers: {
                    overlay: { closeClick: false }
                }
            });

            //var jsdata = jQuery.fancybox.open(jQuery('#add-post'));
            //$(".fancybox-slide").unbind();
            initalizeJS();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}

function editPost(url) {
    displayWhiteOverlay();
    $.ajax({
        type: 'POST',
        url: url,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeWhiteOverlay();
            $('#partial_addEditPost').html(result);

            $.fancybox.open({
                src: '#add-post',
                type: 'inline',
                modal: true,
                helpers: {
                    overlay: { closeClick: false }
                }
            });

            //var jsdata = jQuery.fancybox.open(jQuery('#add-post'));
            //$(".fancybox-slide").unbind();
            initalizeJS();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}


function initalizeJS() {    
    //for tabs
    initalizeTabs();    
    //for image crop
    canvas = $("#add-post #canvas");
    // Import image
    $inputImage = $('#add-post #inputImage');
    $image = $('#add-post #image');
    context = canvas.get(0).getContext("2d");
    //if (canvas.get(0) != null) {
    //    context = canvas.get(0).getContext("2d");
    //}

    var HiddenPostFileName = $("#add-post #HiddenPostFileName").val();
    var HiddenPostFilePath = $("#add-post #HiddenPostFilePath").val();
    if (HiddenPostFileName != undefined && HiddenPostFileName != "" && HiddenPostFileName != null) {
        $("#add-post .data-div-placeholder-content").hide();

        showIconImage(HiddenPostFileName, HiddenPostFilePath);
        LoadProgressBar(HiddenPostFileName, HiddenPostFilePath, true);
    }

    ////for tags
    //jQuery(".tm-input").tagsManager({
    //    //prefilled: $("#add-post #HiddenHashtags").val()
    //    prefilled: jQuery.makeArray($("#add-post #HiddenHashtags").val().split(","))
    //});

    //$("#HashtagsArray").keyup(function () {
    //    var hdnHashtagsArray = $("input[name='hidden-HashtagsArray']").val();
    //    if (hdnHashtagsArray != '' && hdnHashtagsArray != null && hdnHashtagsArray != undefined) {
    //        $('#HashtagsArray').removeAttr('placeholder');
    //    }
    //    else {
    //        var msg = 'Type hashtags here';
    //        if (varLang == 'de') {
    //            msg = 'Hier eingeben..';
    //        }
    //        $('#HashtagsArray').attr('placeholder', msg);
    //    }
    //});

    $("#HashtagsArray").val("");
    jQuery(".tm-input-select").tagsinput({
        //allowClear: true,
        tags: true,
        confirmKeys: [13, 32, 188]
    });

    var tags = $("#add-post #HiddenHashtags").val().split(",");
    var $el = $('.tm-input-select');
    for (var i = 0; i < tags.length; i++) {
        jQuery(".tm-input-select").tagsinput('add', tags[i]);
    }

    jQuery(".bootstrap-tagsinput input").on('keypress', function (e) {
        if (e.keyCode == 13) {
            e.keyCode = 188;
            e.preventDefault();
        };
    });   
    //jQuery(".bootstrap-tagsinput input").on('textInput', e => {
    //    var keyCode = e.originalEvent.data.charCodeAt(0);
    //    if (keyCode != undefined && keyCode != "" && keyCode == 32) {
    //        //debugger
    //        var e = $.Event("keypress");
    //        e.keyCode = 13; // # Some key code value
    //        $('.bootstrap-tagsinput input').trigger(e);
    //    }
    //});

    //jQuery(".bootstrap-tagsinput input").on('keypress input', function (event) {
    //    if (event.type == "keypress") {
    //        //if (e.which != undefined && e.which != "" && e.which == 32) {
    //        //    debugger
    //        //    var e = $.Event("keypress");
    //        //    e.keyCode = 13; // # Some key code value
    //        //    $('.bootstrap-tagsinput input').trigger(e);
    //        //};
    //    }
    //});
}


function deletePost(url, loadPostUrl) {
    //displayWhiteOverlay();
    var msg = 'Are you sure to delete this post?';
    var yesdelete = 'Yes, Delete it!';
    if (varLang == 'de') {
        msg = 'Beitrag löschen?';
        yesdelete = 'Löschen!';
    }
    swal({
        title: msg,
        //text: "Your will not be able to recover this post!",
        type: "warning",
        showCancelButton: true,
        confirmButtonClass: "btn-danger",
        confirmButtonText: yesdelete,
        closeOnConfirm: false,
        allowOutsideClick: false,
    },
        function () {

            var msg = 'Deleted!';
            if (varLang == 'de') {
                msg = 'Gelöscht!';
            }
            $.ajax({
                type: 'POST',
                url: url,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $.fancybox.close();
                    removeWhiteOverlay();
                    swal(msg, "", "success");
                    loadAllPostList(loadPostUrl, false);
                },
                error: function (err) {
                    removeWhiteOverlay();
                    ShowToastr('error', "err::" + err, '');
                }
            });
        });
}

//$(document).on('click', '#upload .next-button', function (event) {
//    //$('.popup-tabs li:nth-child(2) a').trigger('click');
//    //$('.popup-tabs li:first-child a').addClass('active')
//});

//$(document).on('click', '.popup-tabs li:nth-child(2)', function () {
//    //setTimeout(function () {
//    //    $('.popup-tabs li:first-child a').addClass('active')
//    //}, 100)
//});

//$(document).on('click', '.popup-tabs li:nth-child(3)', function () {
//    setTimeout(function () {
//        $('.popup-tabs li:nth-child(3) a').trigger('click');
//        $('.popup-tabs li:first-child a, .popup-tabs li:nth-child(2) a').addClass('active');

//        $("#upload").hide();
//        $("#crop").hide();
//        $("#description").show();
//    }, 100)
//});

$(document).on('click', '.checkFileUpload', function () {

    if ($(".crop-max").length <= 0) {
        var msg = 'Please upload at least one file';
        if (varLang == 'de') {
            msg = 'Bitte Foto/Video auswählen';
        }

        swal({
            title: msg,
            //text: "Please upload image with size 304X306",
            type: "warning",
            showCancelButton: false,
            confirmButtonClass: "btn-danger",
            confirmButtonText: "Ok",
            closeOnConfirm: false,
            allowOutsideClick: false,
        });

        $('.popup-tabs li:nth-child(1) a').trigger('click');
        return false;
    }
    else {
        var litab = $(this).attr("title");

        if (litab == "Description" || litab == "Detail") {
            $('.popup-tabs li a').addClass('active');

            $("#add-post #upload").hide();
            $("#add-post #crop").hide();
            $("#add-post #description").show();
        }
        else {
            $('#add-post .popup-tabs li:first-child a').addClass('active');
            $('#add-post .popup-tabs li:nth-child(2) a').addClass('active');
            $("#add-post #crop").show();
            $("#add-post #upload").hide();
            $("#add-post #description").hide();


            var $image = $('#add-post #image');
            $image.cropper('destroy').attr('src', '');
            $('.img-upload-max').removeClass('hide');
            $('#image-container').hide();

            //var img = $("#add-post #image-container").find("#image").attr("src");
            //if (img == null || img == undefined || img == '') {
            //    $("#add-post .crop-max").first().click();
            //}
            $("#add-post .crop-max img").first().parent().click();

        }
        return false;
    }
});
//hash-tag-wrapper

