﻿
window._a$checkoutShopperUrl = "https://checkoutshopper-test.adyen.com/checkoutshopper/";
(function (pWindow) {

    "use strict";

    var _a$checkoutShopperUrl = window._a$checkoutShopperUrl = 'https://checkoutshopper-test.adyen.com/checkoutshopper/';

    if (_a$checkoutShopperUrl.indexOf('.com') === -1) {
        window._b$dl = true;
    }

    var _a$deviceFingerprint = null;

    var chckt = window.chckt = window.chckt || {};
    chckt.hooks = chckt.hooks || {};

    chckt.pms = {};

    var noop = function () { };

    chckt.hasGiropayValidations = false;
    chckt.giroPayLookup = function () { };

    chckt.hasIBANValidations = false;
    chckt.isValidIBAN = function () { };

    chckt.getCurrencySymbol = function () { };
    chckt.formatCurrency = function () { };

    // Default fns for if validations.card.holder.name.js fails to load
    chckt.cardHolderNameKeyUp = noop;
    chckt.cardHolderNameKeyPress = noop;
    chckt.cardHolderNameInput = noop;

    // Default fns for if validations.iban.js fails to load
    chckt.cardHolderNameKeyUp = noop;
    chckt.isValidIBAN = noop;
    chckt.isValidName = noop;


    if (window.console && console.log) {
        console.log('##################################################');
        console.log('CheckoutSDK version:1.2.1 using CSF version:1.1.1');
        console.log('##################################################');
    }

    /////// FOR TESTING /////////
    // *** NEEDS TO BE YOUR *IP Address* FROM Settings > Network ***
    // *** ALSO NEEDS TO BE SET IN header.php & setupPayments.php & securedFields.js AND NEEDS TO BE THE URL YOU RUN THE LOCAL PHP SERVER ON (port 3000) ***
    var _a$LOCALHOST = "localhost";
    chckt.__localhost = _a$LOCALHOST;// Needed now checkoutPaymentInitiation is a separate module AND only for local testing in IE
    //    chckt.__testingInIE = true;
    //----------------------------

    // Internal 'global' object, used for testing - gives access to the 2 proceeding properties to templater.js & processSetupJSON.js respectively
    //    var _a$chckt = {};
    //    _a$chckt.__listTemplates = true;
    //    _a$chckt.__testing_limitPaymentMethods = ['card'];//['ideal', 'mc', 'visa', 'amex']; // set to empty array: [], to show all
    //--

    //    var noop = function(){return function(){};};
    chckt.noop = noop;

    var shared = {}, __define = noop, __require = noop;

    // Experiment - how to expose fny for testing
    //    shared.module = {exports : {}};
    /* global shared, __define, __require */
    (function (exports) {

        "use strict";

        var modules = exports.__modules = {};

        function __require(name, optionalCallback) {

            if (!name) {
                return function () {
                };
            }

            if (typeof name === "string") {
                if (!modules.hasOwnProperty(name)) {
                    throw new Error("Adyen Sequencing Exception. Module '" + name + "' is not yet defined");
                }
                return modules[name];
            }

            var result = [];

            while (name.length > 0) {
                result.push(__require(name.shift()));
            }

            if (typeof optionalCallback === "function") {
                optionalCallback.apply({}, result);
            }

            return result;
        }

        function __define(name, deps, item) {
            var args = __require(deps);
            if (typeof item === "function") {
                modules[name] = item.apply({}, args);
            } else {
                modules[name] = item;
            }
        }

        exports.__require = __require;
        exports.__define = __define;

    }(shared));

    __define = shared.__define || __define;
    __require = shared.__require || __require;

    __define('cardType', [], function () {
        "use strict";
        var CardType = {};
        CardType.__NO_BRAND = 'noBrand';

        CardType.cards = [];

        var mc = { cardType: "mc", securityCode: "CVC" };
        CardType.cards.push(mc);

        var visadankort = { cardType: "visadankort" };
        CardType.cards.push(visadankort);

        var visa = { cardType: "visa", securityCode: "CVV" };
        CardType.cards.push(visa);

        var amex = { cardType: "amex", securityCode: "CID" };
        CardType.cards.push(amex);

        var vias = { cardType: "vias", startingRules: [9], permittedLengths: [16] };
        CardType.cards.push(vias);

        var diners = { cardType: "diners" };
        CardType.cards.push(diners);

        var maestrouk = { cardType: "maestrouk" };
        CardType.cards.push(maestrouk);

        var solo = { cardType: "solo" };
        CardType.cards.push(solo);

        var laser = { cardType: "laser", cvcIsOptional: true };//TODO Confirm pattern
        CardType.cards.push(laser);

        var discover = { cardType: "discover" };
        CardType.cards.push(discover);

        var jcb = { cardType: "jcb", securityCode: "CAV" };
        CardType.cards.push(jcb);

        var bcmc = { cardType: "bcmc" };
        CardType.cards.push(bcmc);

        var bijcard = { cardType: "bijcard" };
        CardType.cards.push(bijcard);

        var dankort = { cardType: "dankort" };
        CardType.cards.push(dankort);

        var hipercard = { cardType: "hipercard" };
        CardType.cards.push(hipercard);

        var maestro = { cardType: "maestro", cvcIsOptional: true };// NOTE: this pattern doesn't recognise 50... as a maestro card, so added the '0|'
        CardType.cards.push(maestro);

        var elo = { cardType: "elo" };
        CardType.cards.push(elo);

        var uatp = { cardType: "uatp", cvcIsOptional: true };
        CardType.cards.push(uatp);

        var cup = { cardType: "cup" };
        CardType.cards.push(cup);

        var cartebancaire = { cardType: "cartebancaire" };
        CardType.cards.push(cartebancaire);

        var visaalphabankbonus = { cardType: "visaalphabankbonus" };
        CardType.cards.push(visaalphabankbonus);

        var mcalphabankbonus = { cardType: "mcalphabankbonus" };
        CardType.cards.push(mcalphabankbonus);

        var hiper = { cardType: "hiper" };
        CardType.cards.push(hiper);

        var oasis = { cardType: "oasis", cvcIsOptional: true };
        CardType.cards.push(oasis);

        var karenmillen = { cardType: "karenmillen", cvcIsOptional: true };
        CardType.cards.push(karenmillen);

        var warehouse = { cardType: "warehouse", cvcIsOptional: true };
        CardType.cards.push(warehouse);

        var mir = { cardType: "mir" };
        CardType.cards.push(mir);

        var codensa = { cardType: "codensa" };
        CardType.cards.push(codensa);


        CardType.detectCard = function (pCardNumber, pAvailableCards) {

            var matchedCards, i, len;

            if (pAvailableCards) {

                // Filter CardType.cards down to those that are found in pAvailableCards
                matchedCards = CardType.cards.filter(function (card) {
                    return pAvailableCards.includes(card.cardType);
                }).filter(function (card) {
                    // Further filter them to those with a regEx pattern that matches pCardNumber
                    return (card.hasOwnProperty('pattern') && pCardNumber.match(card.pattern));
                });

                // If we have matched cards: if there's only one - return it; else return the one with the longest startingRule
                if (matchedCards.length) {

                    if (matchedCards.length === 1) {
                        return matchedCards[0];
                    } else {

                        // Find longest rule for each matched card & store it as a property on the card
                        for (i = 0, len = matchedCards.length; i < len; i++) {

                            var longestRule = matchedCards[i].startingRules.reduce(function (a, b) { return a > b ? a : b; });
                            matchedCards[i].longestRule = longestRule;
                        }

                        // Based on each matched cards longest rule - find the card with the longest one!
                        var cardWithLongestRule = matchedCards.reduce(function (a, b) { return a.longestRule > b.longestRule ? a : b; });

                        return cardWithLongestRule;
                    }
                }

                return { cardType: CardType.__NO_BRAND };
            }

            return { cardType: CardType.__NO_BRAND };
        };

        CardType.detectCardLength = function (pCard, pUnformattedVal) {

            var maxLength, shortenedNewValue, lengthDiff = 0, reachedValidLength = false, trimTrailingSpace = false;

            // Find the longest of the permitted card number lengths for this card brand
            var maxPermittedLength = (pCard.cardType !== CardType.__NO_BRAND) ? pCard.permittedLengths[pCard.permittedLengths.length - 1] : 0;

            // If the input value is longer than it's max permitted length then shorten it to that length
            if (maxPermittedLength && pUnformattedVal > maxPermittedLength) {

                lengthDiff = pUnformattedVal.length - maxPermittedLength;

                if (lengthDiff > 0) {

                    pUnformattedVal = pUnformattedVal.substring(0, (pUnformattedVal.length - lengthDiff));
                    shortenedNewValue = pUnformattedVal;
                }
            }

            // If cardNumber has reached one of the cardBrand's 'permitted lengths' - mark it as 'valid'
            pCard.permittedLengths.forEach(function (pItem) {

                if (pUnformattedVal.length === pItem) {

                    reachedValidLength = true;
                }
            });

            // If cardNumber is as long as the cardBrand's maximum permitted length then set the maxLength var
            if (pUnformattedVal.length === maxPermittedLength) {

                // Set maxlength to max + the right amount of spaces (one for every 4 digits, but not on the last block)
                var div = Math.floor(pUnformattedVal.length / 4);
                var mod = pUnformattedVal.length % 4;
                var numSpaces = (mod > 0) ? div : div - 1;

                maxLength = maxPermittedLength + numSpaces;

                if (pCard.cardType.toLowerCase() === 'amex') {
                    maxLength = maxPermittedLength + 2;// = 17 = 15 digits with space after 4th & 10th
                }

                // If cardNumber is the length of the longest of cardBrand's 'permitted lengths'
                // - set the flag to trim the trailing space when we format the string
                trimTrailingSpace = true;
            }

            return {
                shortenedNewValue: shortenedNewValue,
                maxLength: maxLength,
                reachedValidLength: reachedValidLength,
                trimTrailingSpace: trimTrailingSpace
            };
        };

        return CardType;
    });
    /* global __define */

    /**
     * Since SDK is to be a distributable file and run in the merchant's environment
     * we should avoid modifying globals that we didn’t create, including polyfills for browser apis
     *
     * Contains IE8 'polyfills' for:
     *
     *      String.trim
     *      Array.forEach
     *      Array.filter
     *      Array.map
     *      Array.indexOf
     *      NonDocumentTypeChildNode.nextElementSibling
     *      NonDocumentTypeChildNode.previousElementSibling
     *
     *
     * IE polyfill for:
     *
     *      Object.assign
     */
    __define('shims', [], function () {

        "use strict";
        var Shims = {};

        // For IE8
        Shims.trim = function (pStr) {

            if (pStr.trim && (typeof pStr.trim).toLowerCase() === 'function') {

                return pStr.trim();
            } else {
                return pStr.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, '');
            }
        };

        // For IE8
        Shims.forEach = function (pArray, pFn, pThat) {

            if (pArray.forEach && (typeof pArray.forEach).toLowerCase() === 'function') {

                pArray.forEach(pFn, pThat);

            } else {

                for (var i = 0, n = pArray.length; i < n; i++) {
                    if (i in pArray) {
                        pFn.call(pThat, pArray[i], i, pArray);
                    }
                }
            }
        };

        // For IE8
        Shims.filter = function (pArray, pFn, pThisArg) {

            var res;

            if (pArray.filter && (typeof pArray.filter).toLowerCase() === 'function') {

                res = pArray.filter(pFn);

            } else {

                var t = Object(pArray);
                var len = t.length >>> 0;
                if (typeof pFn !== 'function') {
                    throw new TypeError();
                }

                res = [];
                for (var i = 0; i < len; i++) {
                    if (i in t) {
                        var val = t[i];

                        // NOTE: Technically this should Object.defineProperty at
                        //       the next index, as push can be affected by
                        //       properties on Object.prototype and Array.prototype.
                        //       But that method's new, and collisions should be
                        //       rare, so use the more-compatible alternative.
                        if (pFn.call(pThisArg, val, i, t)) {
                            res.push(val);
                        }
                    }
                }
            }

            return res;
        };

        // For IE8
        Shims.map = function (pArray, pFn, pThisArg) {

            if (pArray.map && (typeof pArray.map).toLowerCase() === 'function') {

                return pArray.map(pFn);

            } else {

                var T, A, k;

                if (pArray == null) {
                    throw new TypeError('this is null or not defined');
                }

                // 1. Let O be the result of calling ToObject passing the |this|
                //    value as the argument.
                var O = Object(pArray);

                // 2. Let lenValue be the result of calling the Get internal
                //    method of O with the argument "length".
                // 3. Let len be ToUint32(lenValue).
                var len = O.length >>> 0;

                // 4. If IsCallable(callback) is false, throw a TypeError exception.
                // See: http://es5.github.com/#x9.11
                if (typeof pFn !== 'function') {
                    throw new TypeError(pFn + ' is not a function');
                }

                // 5. If thisArg was supplied, let T be thisArg; else let T be undefined.
                if (pThisArg) {
                    T = pThisArg;
                }

                // 6. Let A be a new array created as if by the expression new Array(len)
                //    where Array is the standard built-in constructor with that name and
                //    len is the value of len.
                A = new Array(len);

                // 7. Let k be 0
                k = 0;

                // 8. Repeat, while k < len
                while (k < len) {

                    var kValue, mappedValue;

                    // a. Let Pk be ToString(k).
                    //   This is implicit for LHS operands of the in operator
                    // b. Let kPresent be the result of calling the HasProperty internal
                    //    method of O with argument Pk.
                    //   This step can be combined with c
                    // c. If kPresent is true, then
                    if (k in O) {

                        // i. Let kValue be the result of calling the Get internal
                        //    method of O with argument Pk.
                        kValue = O[k];

                        // ii. Let mappedValue be the result of calling the Call internal
                        //     method of callback with T as the this value and argument
                        //     list containing kValue, k, and O.
                        mappedValue = pFn.call(T, kValue, k, O);

                        // iii. Call the DefineOwnProperty internal method of A with arguments
                        // Pk, Property Descriptor
                        // { Value: mappedValue,
                        //   Writable: true,
                        //   Enumerable: true,
                        //   Configurable: true },
                        // and false.

                        // In browsers that support Object.defineProperty, use the following:
                        // Object.defineProperty(A, k, {
                        //   value: mappedValue,
                        //   writable: true,
                        //   enumerable: true,
                        //   configurable: true
                        // });

                        // For best browser support, use the following:
                        A[k] = mappedValue;
                    }
                    // d. Increase k by 1.
                    k++;
                }

                // 9. return A
                return A;
            }
        };

        // For IE8
        Shims.arrayIndexOf = function (pArray, pSearchElement, pFromIndex) {

            if (pArray.indexOf && (typeof pArray.indexOf).toLowerCase() === 'function') {

                return pArray.indexOf(pSearchElement, pFromIndex || 0);

            } else {

                var k;

                // 1. Let o be the result of calling ToObject passing
                //    the this value as the argument.
                if (pArray == null) {
                    throw new TypeError('"this" is null or not defined');
                }

                var o = Object(pArray);

                // 2. Let lenValue be the result of calling the Get
                //    internal method of o with the argument "length".
                // 3. Let len be ToUint32(lenValue).
                var len = o.length >>> 0;

                // 4. If len is 0, return -1.
                if (len === 0) {
                    return -1;
                }

                // 5. If argument fromIndex was passed let n be
                //    ToInteger(fromIndex); else let n be 0.
                var n = pFromIndex | 0;

                // 6. If n >= len, return -1.
                if (n >= len) {
                    return -1;
                }

                // 7. If n >= 0, then Let k be n.
                // 8. Else, n<0, Let k be len - abs(n).
                //    If k is less than 0, then let k be 0.
                k = Math.max(n >= 0 ? n : len - Math.abs(n), 0);

                // 9. Repeat, while k < len
                while (k < len) {
                    // a. Let Pk be ToString(k).
                    //   This is implicit for LHS operands of the in operator
                    // b. Let kPresent be the result of calling the
                    //    HasProperty internal method of o with argument Pk.
                    //   This step can be combined with c
                    // c. If kPresent is true, then
                    //    i.  Let elementK be the result of calling the Get
                    //        internal method of o with the argument ToString(k).
                    //   ii.  Let same be the result of applying the
                    //        Strict Equality Comparison Algorithm to
                    //        searchElement and elementK.
                    //  iii.  If same is true, return k.
                    if (k in o && o[k] === pSearchElement) {
                        return k;
                    }
                    k++;
                }
                return -1;
            }
        };

        // For IE8
        Shims.nextElementSibling = function (pEl) {

            if (("nextElementSibling" in document.documentElement)) {
                return pEl.nextElementSibling;
            }

            var e = pEl.nextSibling;
            while (e && 1 !== e.nodeType) {
                e = e.nextSibling;
            }
            return e;
        };

        // For IE8
        Shims.previousElementSibling = function (pEl) {

            if (("previousElementSibling" in document.documentElement)) {
                return pEl.previousElementSibling;
            }

            var e = pEl.previousSibling;
            while (e && 1 !== e.nodeType) {
                e = e.previousSibling;
            }
            return e;
        };

        // For IE
        Shims.assign = function (target, varArgs) {

            if (typeof Object.assign === 'function') {

                return Object.assign.apply(null, arguments);

            } else {

                if (target == null) { // TypeError if undefined or null
                    throw new TypeError('Cannot convert undefined or null to object');
                }

                var to = Object(target);

                for (var index = 1; index < arguments.length; index++) {
                    var nextSource = arguments[index];

                    if (nextSource != null) { // Skip over if undefined or null
                        for (var nextKey in nextSource) {
                            // Avoid bugs when hasOwnProperty is shadowed
                            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                                to[nextKey] = nextSource[nextKey];
                            }
                        }
                    }
                }
                return to;
            }
        };

        return Shims;
    });
    /* global __define */
    __define('log', [], function () {
        "use strict";
        return function () {
            var a = Array.prototype.slice.call(arguments, 0);
            if (window.console && window.console.log) {
                window.console.log(a);
            }
        };
    });
    /* global __define */
    // Specific fny to (TODO) send call to API endpoint in event of error
    // TODO is this module still going to be used
    __define('apiLog', [], function () {
        "use strict";
        return function () {
            var a = Array.prototype.slice.call(arguments, 0);
            var msg = JSON.stringify(a);
            if (window._b$dl && window.console && window.console.log) {
                window.console.log('### utils_v2::APILOG :: MSG=', msg);
            }
        };
    });
    /* global __define */
    __define('Util', ['shims'], function (Shims) {
        "use strict";

        var Util = {};

        Util._isArray = function (prop) {
            return typeof prop === "object" && prop !== null && Object.prototype.toString.call(prop) === "[object Array]";
        };

        Util._contains = function (haystack, needle) {
            if (typeof haystack === "undefined") {
                return false;
            }
            if (typeof haystack === "string") {
                return haystack.indexOf(needle) > -1;
            }
            if (Util._isArray(haystack)) {
                for (var c = haystack.length; c-- > 0;) {
                    if (haystack[c] === needle) {
                        return true;
                    }
                }
            }
            return haystack.hasOwnProperty && haystack.hasOwnProperty(needle);
        };

        // Searches array of objects for an object with a property equal to a value
        Util._collectionContains = function (pColl, pProp, pVal) {

            if (typeof pColl === "undefined") {
                return [];
            }

            var tempArray = Shims.filter(pColl, function (pElem) {

                return pElem[pProp] === pVal;
            });

            if (tempArray.length) {
                return tempArray;
            }
            return [];
        };

        //    Util._clone = function ( original ) {
        //        if ( typeof original === "function" ) {
        //            return undefined;
        //        }
        //        if ( typeof original !== 'object' || original === null ) {
        //            return original;
        //        }
        //        var dest = {};
        //        for ( var i in original ) {
        //            if ( original.hasOwnProperty( i ) ) {
        //                var _cloned = Util._clone( original[ i ] );
        //                if ( typeof _cloned !== "undefined" ) {
        //                    dest[ i ] = _cloned;
        //                }
        //            }
        //        }
        //        return dest;
        //    };

        // Serialize the array/object into a string. When you parse that string, entirely new objects are instantiated.
        // CAVEAT: References are not preserved & won't work with non-JSON serializable content (functions, Number.POSITIVE_INFINITY, etc.)
        Util._deepClone = function (original) {
            if (typeof original === "function") {
                return undefined;
            }
            if (typeof original !== 'object' || original === null) {
                return original;
            }
            return JSON.parse(JSON.stringify(original));
        };

        // Performs Object.assign but at a deep level - i.e. a deep merge of 2 objects
        Util._deepAssign = function (target, source) {

            var output = Shims.assign({}, target);

            if (Util._isObjectNotArray(target) && Util._isObjectNotArray(source)) {

                for (var key in source) {

                    if (Util._isObjectNotArray(source[key])) {
                        if (!(key in target)) {
                            var _Shims$assign;

                            Shims.assign(output, (_Shims$assign = {}, _Shims$assign[key] = source[key], _Shims$assign));
                        } else {
                            output[key] = Util._deepAssign(target[key], source[key]);
                        }
                    } else {
                        var _Shims$assign2;

                        Shims.assign(output, (_Shims$assign2 = {}, _Shims$assign2[key] = source[key], _Shims$assign2));
                    }
                }
            }
            return output;
        };

        // Creates a new object based on an existing object containing only those properties passed in a separate list
        Util._mapObject = function (pDataObj, pChosenPropList) {
            var mappedObj = {};
            for (var prop in pDataObj) {
                if (pDataObj.hasOwnProperty(prop)) {
                    if (Util._contains(pChosenPropList, prop)) {
                        mappedObj[prop] = Util._deepClone(pDataObj[prop]);
                    }
                }
            }
            return mappedObj;
        };

        //    Util._flatten = function ( data, prefix, target ) {
        //
        //
        //        if ( typeof data === 'undefined' || data === null ) {
        //            return data;
        //        }
        //        var res = target || {};
        //
        //        for ( var i in data ) {
        //
        //            if ( data.hasOwnProperty( i ) && null !== data[ i ] ) {
        //                var val = data[ i ];
        //                if ( typeof val === "object" ) {
        //                    Util._flatten( val, (prefix || '') + i + ".", res );
        //                } else {
        //                    res[ (prefix || '') + i ] = val;
        //                }
        //            }
        //
        //        }
        //        return res;
        //    };
        //
        //    Util._capitaliseFirstLetter = function ( string ) {
        //        return string.charAt( 0 ).toUpperCase() + string.slice( 1 );
        //    };

        // Sorts an array of objects based on the value of the specified key (2nd param)
        //    Util._sortObjectArrayByKey = function(array, key){
        //        return array.sort(function(a, b) {
        //            var x = a[key];
        //            var y = b[key];
        //            return ((x < y) ? -1 : ((x > y) ? 1 : 0));
        //        });
        //    };

        /**
         * @function The function existy is meant to define the existence of something.
         * Using the loose inequality operator (!=), it is possible to distinguish between null, undefined, and everything else.
         * @param x
         * @returns {boolean}
         */
        Util._existy = function (x) { return x != null; };

        /**
         * @function Used to determine if something should be considered a synonym for true
         * NOTE: The number zero is considered “truthy” by design as '' & {}.
         * If you wish to retain the behavior where 0 is a synonym for false, then do not use truthy where you might expect 0
         * @param x
         * @returns {boolean}
         */
        Util._truthy = function (x) { return (x !== false) && Util._existy(x); };

        /**
         * @description returns true if x is:
         * null, undefined, false, 0, NaN, empty object or array, empty string
         *
         * @example
         *
         * Util._falsy(0) // => true
         * Util._falsy('') // => true
         * Util._falsy({}) // => true
         * Util._falsy([]) // => true
         * Util._falsy(false) // => true
         * Util._falsy(NaN) // => true
         * Util._falsy(null) // => true
         * Util._falsy(undefined) // => true
         *
         * Util._falsy(1) // => false
         * Util._falsy('d') // => false
         * Util._falsy({type:"kin"}) // => false
         * Util._falsy([6]) // => false
         * Util._falsy(true) // => false
         */
        Util._falsy = function (x) {

            // Is null, undefined or false
            if (!Util._truthy(x)) {

                return true;
            }

            // = 0 || NaN
            if (isNumber(x)) {
                if (x === 0 || isNaN(x)) {
                    return true;
                }
            }

            // empty array or string
            if ((Util._isArray(x) || isString(x)) && x.length === 0) {
                return true;
            }

            // If we don't use Object.keys() then we don't need a Shim for IE8
            function hasKeys(pObj) {
                var numKeys = 0;
                for (var key in pObj) {
                    numKeys++;
                }
                return numKeys;
            }

            // empty object
            if (Util._isObject(x) && hasKeys(x) === 0) {
                return true;
            }

            return false;
        };

        Util._notFalsy = function (x) {
            return !Util._falsy(x);
        };


        ////////////////////////////// FROM lodash.3.10.1 //////////////////////////////////////

        /**
         * Used to resolve the [`toStringTag`](http://ecma-international.org/ecma-262/6.0/#sec-object.prototype.tostring)
         * of values.
         */
        var objToString = Object.prototype.toString;
        var stringTag = '[object String]';
        var numberTag = '[object Number]';

        /**
         * Checks if `value` is classified as a `String` primitive or object.
         *
         * @param {*} value The value to check.
         * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
         * @example
         *
         * isString('abc');
         * // => true
         *
         * isString(1);
         * // => false
         */
        function isString(value) {
            return typeof value === 'string' || (isObjectLike(value) && objToString.call(value) === stringTag);
        }

        /**
         * Checks if `value` is the [language type](https://es5.github.io/#x8) of `Object`.
         * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
         *
         * @param {*} value The value to check.
         * @returns {boolean} Returns `true` if `value` is an object, else `false`.
         * @example
         *
         * Util._isObject({});
         * // => true
         *
         * Util._isObject([1, 2, 3]);
         * // => true
         *
         * Util._isObject(1);
         * // => false
         */
        Util._isObject = function (value) {
            // Avoid a V8 JIT bug in Chrome 19-20.
            // See https://code.google.com/p/v8/issues/detail?id=2291 for more details.
            var type = typeof value;
            return !!value && (type === 'object' || type === 'function');
        };

        Util._isObjectNotArray = function (value) {

            return (Util._isObject(value) && !Util._isArray(value));
        };

        /**
         * Checks if `value` is classified as a `Number` primitive or object.
         *
         * **Note:** `Infinity`, `-Infinity`, and `NaN` are classified as numbers
         *
         * @param {*} value The value to check.
         * @returns {boolean} Returns `true` if `value` is correctly classified, else `false`.
         * @example
         *
         * isNumber(8.4);
         * // => true
         *
         * isNumber(NaN);
         * // => true
         *
         * isNumber('8.4');
         * // => false
         */
        function isNumber(value) {
            return typeof value === 'number' || (isObjectLike(value) && objToString.call(value) === numberTag);
        }

        /**
         * Checks if `value` is object-like.
         *
         * @private
         * @param {*} value The value to check.
         * @returns {boolean} Returns `true` if `value` is object-like, else `false`.
         */
        function isObjectLike(value) {
            return !!value && typeof value === 'object';
        }

        return Util;
    });
    /* global __define */
    __define('DOM', ['log', 'Util'], function (log, Util) {
        "use strict";
        var DOM = {};

        DOM._select = function (root, selector) {

            if (!root) {
                return [];
            }

            // Convert NodeList to array
            if (typeof root.querySelectorAll === "function") {
                return [].slice.call(root.querySelectorAll(selector));
            }

            // ELSE... IE8 - to convert StaticNodeList to array, from: https://jsperf.com/nodelist-to-array-ie8-compatible
            var arr = [];
            var n = root.querySelectorAll(selector);
            for (var z = n.length; z--;) {
                arr.unshift(n[z]);
            }
            return arr;
        };

        DOM._selectOne = function (root, selector) {

            // In IE8 typeof root.querySelector = "object"
            //        if ( !root || typeof root.querySelector !== "function" ) {
            //            return undefined;
            //        }
            //        return root.querySelector( selector );

            if (!root) {
                return undefined;
            }

            return root.querySelector(selector);
        };

        DOM._closest = function (node, selectorString) {

            // Closest function, that CAN match on attribute
            // New fny re. https://gomakethings.com/climbing-up-and-down-the-dom-tree-with-vanilla-javascript/
            // Element.matches() polyfill
            if (!Element.prototype.matches) {
                Element.prototype.matches =
                    Element.prototype.matchesSelector ||
                    Element.prototype.mozMatchesSelector ||
                    Element.prototype.msMatchesSelector ||
                    Element.prototype.oMatchesSelector ||
                    Element.prototype.webkitMatchesSelector ||
                    function (s) {
                        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                            i = matches.length;
                        while (--i >= 0 && matches.item(i) !== this) { }
                        return i > -1;
                    };
            }

            // Get closest match
            for (; node && node !== document; node = node.parentNode) {
                if (node.matches(selectorString)) {
                    return node;
                }
            }

            return null;
        };

        DOM._removeAllChildren = function (node) {

            while (node.firstChild) {
                node.removeChild(node.firstChild);
            }
        };

        DOM._hasClass = function (node, cssClass) {
            if (!node || !node.className) {
                return false;
            }
            return Util._contains(node.className.split(/\s+/), cssClass);
        };

        DOM._replaceClass = function (node, remove, add) {
            if (!node) {
                return false;
            }

            var classes = (node.className || '').split(/\s+/);

            var newClasses = [];

            while (classes.length > 0) {
                var cssClass = classes.shift();
                if (cssClass === remove) {
                    continue;
                }
                if (cssClass === add) {
                    continue;
                }
                newClasses.push(cssClass);
            }

            if (add) {
                newClasses.push(add);
            }

            var className = newClasses.join(' ');

            try {
                if (node.className !== className) {
                    node.className = className;
                }
            } catch (e) {
                log("Error adding class to node", node, className);
            }
        };

        DOM._getFormData = function (form, includeDisabled) {

            var res = {}, elem, c;

            if (!form || !form.elements) {
                return res;
            }

            for (c = form.elements.length; c-- > 0;) {
                elem = form.elements[c];

                // If element disabled OR doesn't have a name attribute then exclude it
                if ((elem.disabled && !includeDisabled) || !elem.name) {
                    continue;
                }

                var type = (elem.type || elem.nodeName || elem.tagName || '').toLowerCase();
                switch (type) {
                    case 'radio':
                    case 'checkbox':
                        if (elem.checked) {
                            res[elem.name] = elem.value;// NOTE: For checkbox if we just set the value directly to "true" c.f. elem.value; we could dispense with the value="true" attribute in the checkboxFieldType template
                        }
                        break;
                    case 'text':
                    case 'textarea':
                    case 'hidden':
                        res[elem.name] = elem.value;
                        break;
                    case 'select':
                    case 'select-one':
                        if (elem.selectedIndex > -1) {
                            res[elem.name] = elem.options[elem.selectedIndex].value;
                        }
                        break;
                    case 'file':
                        log("File input is not supported by the checkout API");
                        break;
                    default:
                        log('Unknown element type ', type, " for element ", elem);
                }
            }

            return res;
        };

        DOM._getAttribute = function (node, attribute) {

            // In IE8 typeof root.getAttribute = "object"
            //        if ( !node || typeof node.getAttribute !== "function" ) {
            //            return '';
            //        }
            //        return node.getAttribute( attribute ) || '';

            if (!node) {
                return;
            }
            return node.getAttribute(attribute) || '';
        };

        DOM._getElementsByClassName = function (pNode, pClassName) {

            if (!pNode) {
                return;
            }

            if (pNode.getElementsByClassName) {
                return pNode.getElementsByClassName(pClassName);
            }

            // ELSE... IE8
            return pNode.querySelectorAll("." + pClassName);
        };

        DOM._on = function (node, event, callback, useCapture) {
            if (typeof node.addEventListener === "function") {
                node.addEventListener(event, callback, useCapture);
            } else {
                if (node.attachEvent) {
                    node.attachEvent("on" + event, callback);
                } else {
                    throw new Error(": Unable to bind " + event + "-event");
                }
            }
        };

        DOM._off = function (node, event, callback, useCapture) {

            if (typeof node.addEventListener === "function") {
                node.removeEventListener(event, callback, useCapture);
            } else {
                if (node.attachEvent) {
                    node.detachEvent("on" + event, callback);
                } else {
                    throw new Error(": Unable to unbind " + event + "-event");
                }
            }
        };

        DOM._getEventTarget = function (e) {
            //http://www.quirksmode.org/js/events_properties.html
            if (!e) {
                e = window.event || {};
            }
            var targ = e.target || e.srcElement || {};
            if (targ.nodeType === 3) {// defeat Safari bug
                targ = targ.parentNode;
            }
            return targ;
        };

        return DOM;
    });
    /* global __define, chckt */
    __define('hook', ['log', 'DOM'], function (log, DOM) {

        "use strict";

        /**
         * Offer merchants an integration point
         * @param {string} type - type of hook: normally blank but can be 'hasHook' to check for a hooks existence
         * @param {string} hookName Name of the hook
         * @param {object} node A dom Node
         *
         * @returns {boolean}
         */
        function _hook(type, hookName, node, additionalData) {

            if (!chckt.hooks && type !== 'hasHook') {

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### utils::_hook:: No custom hooks configured at all. Continuing chckt logic. data=', additionalData);
                }
                return true;
            }

            if (type === 'hasHook') {
                return (chckt.hooks && chckt.hooks.hasOwnProperty(hookName) && typeof chckt.hooks[hookName] === "function");
            }

            if (!chckt.hooks.hasOwnProperty(hookName) || typeof chckt.hooks[hookName] !== "function") {

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### utils::_hook:: chckt.hooks: "' + hookName + '" not configured. Continuing chckt logic. data=', additionalData);
                }
                return true;
            }
            try {
                var res = chckt.hooks[hookName](node, additionalData) !== false;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### utils::_hook:: "' + hookName + '" returned:', res);
                }

                if (res) {
                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### utils::_hook:: "' + hookName + '", Continuing chckt logic. data=', additionalData);
                    }
                } else {
                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### utils::_hook:: "' + hookName + '", Integration prevented chckt logic. data=', additionalData);
                    }
                }

                return res;

            } catch (e) {

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### utils::_hook:: "' + hookName + '", Error while processing hook. Continuing with Adyen logic. data=', additionalData, ' e=', e);
                }
                // Error in the callback.Should we swallow or log it ?
                return true;
            }
        }

        return _hook;


    });
    /* global __define */
    __define('Net', ['Util'], function (Util) {

        "use strict";

        var Net = {};

        Net._ajax = function (options) {

            var params = null;
            var xhr = new XMLHttpRequest();
            xhr.open(options.method || 'GET', options.url);
            xhr.setRequestHeader("Content-type", options.contentType || "application/x-www-form-urlencoded");
            if (options.method === "POST" && options.hasOwnProperty('data')) {

                var data = options.data;
                params = [];
                if (typeof data === "string") {
                    params = data;
                } else {
                    for (var i in Util._flatten(data)) {
                        if (data.hasOwnProperty(i)) {

                            params.push(encodeURIComponent(i) + "=" + encodeURIComponent(data[i]));
                        }
                    }
                    params = params.join("&");
                }
            }

            xhr.onreadystatechange = function () {

                var DONE = 4; // readyState 4 means the request is done.
                var OK = 200; // status 200 is a successful return.

                if (xhr.readyState === DONE) {
                    if (xhr.status === OK) {
                        if (typeof options.success === "function") {
                            options.success(xhr.responseText, xhr);
                        }
                    } else if (typeof options.error === "function") {
                        options.error(xhr.statusText, xhr.status, xhr);
                    }
                }
            };

            xhr.send(params);
        };

        Net._parseQueryString = function (str) {
            var result = {};
            if (!str || typeof str !== "string") {
                return result;
            }
            if (str.substring(0, 1) === "?") {
                str = str.substring(1);
            }
            var parts = str.split('&');
            while (parts.length > 0) {
                var item = parts.shift().split('=');
                var key = item.shift(), value = item.join('=');
                if (key && value) {
                    result[decodeURIComponent(key)] = decodeURIComponent(value);
                }
            }
            return result;
        };

        return Net;
    });
    /* global chckt, __define, _a$chckt */

    __define('templater', ['DOM', 'shims', 'log', 'hook'], function (DOM, Shims, log, hook) {

        'use strict';

        // Runs at 'define' time to put the Handlebars templating engine into the __handlebars var
        var _tplExp = {};
        var templateEngine = function (exports) {
            /**!
    
     @license
     handlebars v4.0.10
    
    Copyright (C) 2011-2016 by Yehuda Katz
    
    Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:
    
    The above copyright notice and this permission notice shall be included in
    all copies or substantial portions of the Software.
    
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
    THE SOFTWARE.
    
    */
            !function (a, b) { "object" == typeof exports && "object" == typeof module ? module.exports = b() : "function" == typeof define && define.amd ? define([], b) : "object" == typeof exports ? exports.Handlebars = b() : a.Handlebars = b() }(this, function () {
                return function (a) { function b(d) { if (c[d]) return c[d].exports; var e = c[d] = { exports: {}, id: d, loaded: !1 }; return a[d].call(e.exports, e, e.exports, b), e.loaded = !0, e.exports } var c = {}; return b.m = a, b.c = c, b.p = "", b(0) }([function (a, b, c) { "use strict"; function d() { var a = r(); return a.compile = function (b, c) { return k.compile(b, c, a) }, a.precompile = function (b, c) { return k.precompile(b, c, a) }, a.AST = i["default"], a.Compiler = k.Compiler, a.JavaScriptCompiler = m["default"], a.Parser = j.parser, a.parse = j.parse, a } var e = c(1)["default"]; b.__esModule = !0; var f = c(2), g = e(f), h = c(35), i = e(h), j = c(36), k = c(41), l = c(42), m = e(l), n = c(39), o = e(n), p = c(34), q = e(p), r = g["default"].create, s = d(); s.create = d, q["default"](s), s.Visitor = o["default"], s["default"] = s, b["default"] = s, a.exports = b["default"] }, function (a, b) { "use strict"; b["default"] = function (a) { return a && a.__esModule ? a : { "default": a } }, b.__esModule = !0 }, function (a, b, c) { "use strict"; function d() { var a = new h.HandlebarsEnvironment; return n.extend(a, h), a.SafeString = j["default"], a.Exception = l["default"], a.Utils = n, a.escapeExpression = n.escapeExpression, a.VM = p, a.template = function (b) { return p.template(b, a) }, a } var e = c(3)["default"], f = c(1)["default"]; b.__esModule = !0; var g = c(4), h = e(g), i = c(21), j = f(i), k = c(6), l = f(k), m = c(5), n = e(m), o = c(22), p = e(o), q = c(34), r = f(q), s = d(); s.create = d, r["default"](s), s["default"] = s, b["default"] = s, a.exports = b["default"] }, function (a, b) { "use strict"; b["default"] = function (a) { if (a && a.__esModule) return a; var b = {}; if (null != a) for (var c in a) Object.prototype.hasOwnProperty.call(a, c) && (b[c] = a[c]); return b["default"] = a, b }, b.__esModule = !0 }, function (a, b, c) { "use strict"; function d(a, b, c) { this.helpers = a || {}, this.partials = b || {}, this.decorators = c || {}, i.registerDefaultHelpers(this), j.registerDefaultDecorators(this) } var e = c(1)["default"]; b.__esModule = !0, b.HandlebarsEnvironment = d; var f = c(5), g = c(6), h = e(g), i = c(10), j = c(18), k = c(20), l = e(k), m = "4.0.10"; b.VERSION = m; var n = 7; b.COMPILER_REVISION = n; var o = { 1: "<= 1.0.rc.2", 2: "== 1.0.0-rc.3", 3: "== 1.0.0-rc.4", 4: "== 1.x.x", 5: "== 2.0.0-alpha.x", 6: ">= 2.0.0-beta.1", 7: ">= 4.0.0" }; b.REVISION_CHANGES = o; var p = "[object Object]"; d.prototype = { constructor: d, logger: l["default"], log: l["default"].log, registerHelper: function (a, b) { if (f.toString.call(a) === p) { if (b) throw new h["default"]("Arg not supported with multiple helpers"); f.extend(this.helpers, a) } else this.helpers[a] = b }, unregisterHelper: function (a) { delete this.helpers[a] }, registerPartial: function (a, b) { if (f.toString.call(a) === p) f.extend(this.partials, a); else { if ("undefined" == typeof b) throw new h["default"]('Attempting to register a partial called "' + a + '" as undefined'); this.partials[a] = b } }, unregisterPartial: function (a) { delete this.partials[a] }, registerDecorator: function (a, b) { if (f.toString.call(a) === p) { if (b) throw new h["default"]("Arg not supported with multiple decorators"); f.extend(this.decorators, a) } else this.decorators[a] = b }, unregisterDecorator: function (a) { delete this.decorators[a] } }; var q = l["default"].log; b.log = q, b.createFrame = f.createFrame, b.logger = l["default"] }, function (a, b) { "use strict"; function c(a) { return k[a] } function d(a) { for (var b = 1; b < arguments.length; b++)for (var c in arguments[b]) Object.prototype.hasOwnProperty.call(arguments[b], c) && (a[c] = arguments[b][c]); return a } function e(a, b) { for (var c = 0, d = a.length; c < d; c++)if (a[c] === b) return c; return -1 } function f(a) { if ("string" != typeof a) { if (a && a.toHTML) return a.toHTML(); if (null == a) return ""; if (!a) return a + ""; a = "" + a } return m.test(a) ? a.replace(l, c) : a } function g(a) { return !a && 0 !== a || !(!p(a) || 0 !== a.length) } function h(a) { var b = d({}, a); return b._parent = a, b } function i(a, b) { return a.path = b, a } function j(a, b) { return (a ? a + "." : "") + b } b.__esModule = !0, b.extend = d, b.indexOf = e, b.escapeExpression = f, b.isEmpty = g, b.createFrame = h, b.blockParams = i, b.appendContextPath = j; var k = { "&": "&amp;", "<": "&lt;", ">": "&gt;", '"': "&quot;", "'": "&#x27;", "`": "&#x60;", "=": "&#x3D;" }, l = /[&<>"'`=]/g, m = /[&<>"'`=]/, n = Object.prototype.toString; b.toString = n; var o = function (a) { return "function" == typeof a }; o(/x/) && (b.isFunction = o = function (a) { return "function" == typeof a && "[object Function]" === n.call(a) }), b.isFunction = o; var p = Array.isArray || function (a) { return !(!a || "object" != typeof a) && "[object Array]" === n.call(a) }; b.isArray = p }, function (a, b, c) { "use strict"; function d(a, b) { var c = b && b.loc, g = void 0, h = void 0; c && (g = c.start.line, h = c.start.column, a += " - " + g + ":" + h); for (var i = Error.prototype.constructor.call(this, a), j = 0; j < f.length; j++)this[f[j]] = i[f[j]]; Error.captureStackTrace && Error.captureStackTrace(this, d); try { c && (this.lineNumber = g, e ? Object.defineProperty(this, "column", { value: h, enumerable: !0 }) : this.column = h) } catch (k) { } } var e = c(7)["default"]; b.__esModule = !0; var f = ["description", "fileName", "lineNumber", "message", "name", "number", "stack"]; d.prototype = new Error, b["default"] = d, a.exports = b["default"] }, function (a, b, c) { a.exports = { "default": c(8), __esModule: !0 } }, function (a, b, c) { var d = c(9); a.exports = function (a, b, c) { return d.setDesc(a, b, c) } }, function (a, b) { var c = Object; a.exports = { create: c.create, getProto: c.getPrototypeOf, isEnum: {}.propertyIsEnumerable, getDesc: c.getOwnPropertyDescriptor, setDesc: c.defineProperty, setDescs: c.defineProperties, getKeys: c.keys, getNames: c.getOwnPropertyNames, getSymbols: c.getOwnPropertySymbols, each: [].forEach } }, function (a, b, c) { "use strict"; function d(a) { g["default"](a), i["default"](a), k["default"](a), m["default"](a), o["default"](a), q["default"](a), s["default"](a) } var e = c(1)["default"]; b.__esModule = !0, b.registerDefaultHelpers = d; var f = c(11), g = e(f), h = c(12), i = e(h), j = c(13), k = e(j), l = c(14), m = e(l), n = c(15), o = e(n), p = c(16), q = e(p), r = c(17), s = e(r) }, function (a, b, c) { "use strict"; b.__esModule = !0; var d = c(5); b["default"] = function (a) { a.registerHelper("blockHelperMissing", function (b, c) { var e = c.inverse, f = c.fn; if (b === !0) return f(this); if (b === !1 || null == b) return e(this); if (d.isArray(b)) return b.length > 0 ? (c.ids && (c.ids = [c.name]), a.helpers.each(b, c)) : e(this); if (c.data && c.ids) { var g = d.createFrame(c.data); g.contextPath = d.appendContextPath(c.data.contextPath, c.name), c = { data: g } } return f(b, c) }) }, a.exports = b["default"] }, function (a, b, c) { "use strict"; var d = c(1)["default"]; b.__esModule = !0; var e = c(5), f = c(6), g = d(f); b["default"] = function (a) { a.registerHelper("each", function (a, b) { function c(b, c, f) { j && (j.key = b, j.index = c, j.first = 0 === c, j.last = !!f, k && (j.contextPath = k + b)), i += d(a[b], { data: j, blockParams: e.blockParams([a[b], b], [k + b, null]) }) } if (!b) throw new g["default"]("Must pass iterator to #each"); var d = b.fn, f = b.inverse, h = 0, i = "", j = void 0, k = void 0; if (b.data && b.ids && (k = e.appendContextPath(b.data.contextPath, b.ids[0]) + "."), e.isFunction(a) && (a = a.call(this)), b.data && (j = e.createFrame(b.data)), a && "object" == typeof a) if (e.isArray(a)) for (var l = a.length; h < l; h++)h in a && c(h, h, h === a.length - 1); else { var m = void 0; for (var n in a) a.hasOwnProperty(n) && (void 0 !== m && c(m, h - 1), m = n, h++); void 0 !== m && c(m, h - 1, !0) } return 0 === h && (i = f(this)), i }) }, a.exports = b["default"] }, function (a, b, c) { "use strict"; var d = c(1)["default"]; b.__esModule = !0; var e = c(6), f = d(e); b["default"] = function (a) { a.registerHelper("helperMissing", function () { if (1 !== arguments.length) throw new f["default"]('Missing helper: "' + arguments[arguments.length - 1].name + '"') }) }, a.exports = b["default"] }, function (a, b, c) { "use strict"; b.__esModule = !0; var d = c(5); b["default"] = function (a) { a.registerHelper("if", function (a, b) { return d.isFunction(a) && (a = a.call(this)), !b.hash.includeZero && !a || d.isEmpty(a) ? b.inverse(this) : b.fn(this) }), a.registerHelper("unless", function (b, c) { return a.helpers["if"].call(this, b, { fn: c.inverse, inverse: c.fn, hash: c.hash }) }) }, a.exports = b["default"] }, function (a, b) { "use strict"; b.__esModule = !0, b["default"] = function (a) { a.registerHelper("log", function () { for (var b = [void 0], c = arguments[arguments.length - 1], d = 0; d < arguments.length - 1; d++)b.push(arguments[d]); var e = 1; null != c.hash.level ? e = c.hash.level : c.data && null != c.data.level && (e = c.data.level), b[0] = e, a.log.apply(a, b) }) }, a.exports = b["default"] }, function (a, b) { "use strict"; b.__esModule = !0, b["default"] = function (a) { a.registerHelper("lookup", function (a, b) { return a && a[b] }) }, a.exports = b["default"] }, function (a, b, c) { "use strict"; b.__esModule = !0; var d = c(5); b["default"] = function (a) { a.registerHelper("with", function (a, b) { d.isFunction(a) && (a = a.call(this)); var c = b.fn; if (d.isEmpty(a)) return b.inverse(this); var e = b.data; return b.data && b.ids && (e = d.createFrame(b.data), e.contextPath = d.appendContextPath(b.data.contextPath, b.ids[0])), c(a, { data: e, blockParams: d.blockParams([a], [e && e.contextPath]) }) }) }, a.exports = b["default"] }, function (a, b, c) { "use strict"; function d(a) { g["default"](a) } var e = c(1)["default"]; b.__esModule = !0, b.registerDefaultDecorators = d; var f = c(19), g = e(f) }, function (a, b, c) { "use strict"; b.__esModule = !0; var d = c(5); b["default"] = function (a) { a.registerDecorator("inline", function (a, b, c, e) { var f = a; return b.partials || (b.partials = {}, f = function (e, f) { var g = c.partials; c.partials = d.extend({}, g, b.partials); var h = a(e, f); return c.partials = g, h }), b.partials[e.args[0]] = e.fn, f }) }, a.exports = b["default"] }, function (a, b, c) { "use strict"; b.__esModule = !0; var d = c(5), e = { methodMap: ["debug", "info", "warn", "error"], level: "info", lookupLevel: function (a) { if ("string" == typeof a) { var b = d.indexOf(e.methodMap, a.toLowerCase()); a = b >= 0 ? b : parseInt(a, 10) } return a }, log: function (a) { if (a = e.lookupLevel(a), "undefined" != typeof console && e.lookupLevel(e.level) <= a) { var b = e.methodMap[a]; console[b] || (b = "log"); for (var c = arguments.length, d = Array(c > 1 ? c - 1 : 0), f = 1; f < c; f++)d[f - 1] = arguments[f]; console[b].apply(console, d) } } }; b["default"] = e, a.exports = b["default"] }, function (a, b) { "use strict"; function c(a) { this.string = a } b.__esModule = !0, c.prototype.toString = c.prototype.toHTML = function () { return "" + this.string }, b["default"] = c, a.exports = b["default"] }, function (a, b, c) { "use strict"; function d(a) { var b = a && a[0] || 1, c = s.COMPILER_REVISION; if (b !== c) { if (b < c) { var d = s.REVISION_CHANGES[c], e = s.REVISION_CHANGES[b]; throw new r["default"]("Template was precompiled with an older version of Handlebars than the current runtime. Please update your precompiler to a newer version (" + d + ") or downgrade your runtime to an older version (" + e + ").") } throw new r["default"]("Template was precompiled with a newer version of Handlebars than the current runtime. Please update your runtime to a newer version (" + a[1] + ").") } } function e(a, b) { function c(c, d, e) { e.hash && (d = p.extend({}, d, e.hash), e.ids && (e.ids[0] = !0)), c = b.VM.resolvePartial.call(this, c, d, e); var f = b.VM.invokePartial.call(this, c, d, e); if (null == f && b.compile && (e.partials[e.name] = b.compile(c, a.compilerOptions, b), f = e.partials[e.name](d, e)), null != f) { if (e.indent) { for (var g = f.split("\n"), h = 0, i = g.length; h < i && (g[h] || h + 1 !== i); h++)g[h] = e.indent + g[h]; f = g.join("\n") } return f } throw new r["default"]("The partial " + e.name + " could not be compiled when running in runtime-only mode") } function d(b) { function c(b) { return "" + a.main(e, b, e.helpers, e.partials, g, i, h) } var f = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1], g = f.data; d._setup(f), !f.partial && a.useData && (g = j(b, g)); var h = void 0, i = a.useBlockParams ? [] : void 0; return a.useDepths && (h = f.depths ? b != f.depths[0] ? [b].concat(f.depths) : f.depths : [b]), (c = k(a.main, c, e, f.depths || [], g, i))(b, f) } if (!b) throw new r["default"]("No environment passed to template"); if (!a || !a.main) throw new r["default"]("Unknown template object: " + typeof a); a.main.decorator = a.main_d, b.VM.checkRevision(a.compiler); var e = { strict: function (a, b) { if (!(b in a)) throw new r["default"]('"' + b + '" not defined in ' + a); return a[b] }, lookup: function (a, b) { for (var c = a.length, d = 0; d < c; d++)if (a[d] && null != a[d][b]) return a[d][b] }, lambda: function (a, b) { return "function" == typeof a ? a.call(b) : a }, escapeExpression: p.escapeExpression, invokePartial: c, fn: function (b) { var c = a[b]; return c.decorator = a[b + "_d"], c }, programs: [], program: function (a, b, c, d, e) { var g = this.programs[a], h = this.fn(a); return b || e || d || c ? g = f(this, a, h, b, c, d, e) : g || (g = this.programs[a] = f(this, a, h)), g }, data: function (a, b) { for (; a && b--;)a = a._parent; return a }, merge: function (a, b) { var c = a || b; return a && b && a !== b && (c = p.extend({}, b, a)), c }, nullContext: l({}), noop: b.VM.noop, compilerInfo: a.compiler }; return d.isTop = !0, d._setup = function (c) { c.partial ? (e.helpers = c.helpers, e.partials = c.partials, e.decorators = c.decorators) : (e.helpers = e.merge(c.helpers, b.helpers), a.usePartial && (e.partials = e.merge(c.partials, b.partials)), (a.usePartial || a.useDecorators) && (e.decorators = e.merge(c.decorators, b.decorators))) }, d._child = function (b, c, d, g) { if (a.useBlockParams && !d) throw new r["default"]("must pass block params"); if (a.useDepths && !g) throw new r["default"]("must pass parent depths"); return f(e, b, a[b], c, 0, d, g) }, d } function f(a, b, c, d, e, f, g) { function h(b) { var e = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1], h = g; return !g || b == g[0] || b === a.nullContext && null === g[0] || (h = [b].concat(g)), c(a, b, a.helpers, a.partials, e.data || d, f && [e.blockParams].concat(f), h) } return h = k(c, h, a, g, d, f), h.program = b, h.depth = g ? g.length : 0, h.blockParams = e || 0, h } function g(a, b, c) { return a ? a.call || c.name || (c.name = a, a = c.partials[a]) : a = "@partial-block" === c.name ? c.data["partial-block"] : c.partials[c.name], a } function h(a, b, c) { var d = c.data && c.data["partial-block"]; c.partial = !0, c.ids && (c.data.contextPath = c.ids[0] || c.data.contextPath); var e = void 0; if (c.fn && c.fn !== i && !function () { c.data = s.createFrame(c.data); var a = c.fn; e = c.data["partial-block"] = function (b) { var c = arguments.length <= 1 || void 0 === arguments[1] ? {} : arguments[1]; return c.data = s.createFrame(c.data), c.data["partial-block"] = d, a(b, c) }, a.partials && (c.partials = p.extend({}, c.partials, a.partials)) }(), void 0 === a && e && (a = e), void 0 === a) throw new r["default"]("The partial " + c.name + " could not be found"); if (a instanceof Function) return a(b, c) } function i() { return "" } function j(a, b) { return b && "root" in b || (b = b ? s.createFrame(b) : {}, b.root = a), b } function k(a, b, c, d, e, f) { if (a.decorator) { var g = {}; b = a.decorator(b, g, c, d && d[0], e, f, d), p.extend(b, g) } return b } var l = c(23)["default"], m = c(3)["default"], n = c(1)["default"]; b.__esModule = !0, b.checkRevision = d, b.template = e, b.wrapProgram = f, b.resolvePartial = g, b.invokePartial = h, b.noop = i; var o = c(5), p = m(o), q = c(6), r = n(q), s = c(4) }, function (a, b, c) { a.exports = { "default": c(24), __esModule: !0 } }, function (a, b, c) { c(25), a.exports = c(30).Object.seal }, function (a, b, c) { var d = c(26); c(27)("seal", function (a) { return function (b) { return a && d(b) ? a(b) : b } }) }, function (a, b) { a.exports = function (a) { return "object" == typeof a ? null !== a : "function" == typeof a } }, function (a, b, c) { var d = c(28), e = c(30), f = c(33); a.exports = function (a, b) { var c = (e.Object || {})[a] || Object[a], g = {}; g[a] = b(c), d(d.S + d.F * f(function () { c(1) }), "Object", g) } }, function (a, b, c) { var d = c(29), e = c(30), f = c(31), g = "prototype", h = function (a, b, c) { var i, j, k, l = a & h.F, m = a & h.G, n = a & h.S, o = a & h.P, p = a & h.B, q = a & h.W, r = m ? e : e[b] || (e[b] = {}), s = m ? d : n ? d[b] : (d[b] || {})[g]; m && (c = b); for (i in c) j = !l && s && i in s, j && i in r || (k = j ? s[i] : c[i], r[i] = m && "function" != typeof s[i] ? c[i] : p && j ? f(k, d) : q && s[i] == k ? function (a) { var b = function (b) { return this instanceof a ? new a(b) : a(b) }; return b[g] = a[g], b }(k) : o && "function" == typeof k ? f(Function.call, k) : k, o && ((r[g] || (r[g] = {}))[i] = k)) }; h.F = 1, h.G = 2, h.S = 4, h.P = 8, h.B = 16, h.W = 32, a.exports = h }, function (a, b) { var c = a.exports = "undefined" != typeof window && window.Math == Math ? window : "undefined" != typeof self && self.Math == Math ? self : Function("return this")(); "number" == typeof __g && (__g = c) }, function (a, b) { var c = a.exports = { version: "1.2.6" }; "number" == typeof __e && (__e = c) }, function (a, b, c) { var d = c(32); a.exports = function (a, b, c) { if (d(a), void 0 === b) return a; switch (c) { case 1: return function (c) { return a.call(b, c) }; case 2: return function (c, d) { return a.call(b, c, d) }; case 3: return function (c, d, e) { return a.call(b, c, d, e) } }return function () { return a.apply(b, arguments) } } }, function (a, b) { a.exports = function (a) { if ("function" != typeof a) throw TypeError(a + " is not a function!"); return a } }, function (a, b) { a.exports = function (a) { try { return !!a() } catch (b) { return !0 } } }, function (a, b) { (function (c) { "use strict"; b.__esModule = !0, b["default"] = function (a) { var b = "undefined" != typeof c ? c : window, d = b.Handlebars; a.noConflict = function () { return b.Handlebars === a && (b.Handlebars = d), a } }, a.exports = b["default"] }).call(b, function () { return this }()) }, function (a, b) { "use strict"; b.__esModule = !0; var c = { helpers: { helperExpression: function (a) { return "SubExpression" === a.type || ("MustacheStatement" === a.type || "BlockStatement" === a.type) && !!(a.params && a.params.length || a.hash) }, scopedId: function (a) { return /^\.|this\b/.test(a.original) }, simpleId: function (a) { return 1 === a.parts.length && !c.helpers.scopedId(a) && !a.depth } } }; b["default"] = c, a.exports = b["default"] }, function (a, b, c) { "use strict"; function d(a, b) { if ("Program" === a.type) return a; h["default"].yy = n, n.locInfo = function (a) { return new n.SourceLocation(b && b.srcName, a) }; var c = new j["default"](b); return c.accept(h["default"].parse(a)) } var e = c(1)["default"], f = c(3)["default"]; b.__esModule = !0, b.parse = d; var g = c(37), h = e(g), i = c(38), j = e(i), k = c(40), l = f(k), m = c(5); b.parser = h["default"]; var n = {}; m.extend(n, l) }, function (a, b) {
                    "use strict"; b.__esModule = !0; var c = function () {
                        function a() { this.yy = {} } var b = {
                            trace: function () { }, yy: {}, symbols_: { error: 2, root: 3, program: 4, EOF: 5, program_repetition0: 6, statement: 7, mustache: 8, block: 9, rawBlock: 10, partial: 11, partialBlock: 12, content: 13, COMMENT: 14, CONTENT: 15, openRawBlock: 16, rawBlock_repetition_plus0: 17, END_RAW_BLOCK: 18, OPEN_RAW_BLOCK: 19, helperName: 20, openRawBlock_repetition0: 21, openRawBlock_option0: 22, CLOSE_RAW_BLOCK: 23, openBlock: 24, block_option0: 25, closeBlock: 26, openInverse: 27, block_option1: 28, OPEN_BLOCK: 29, openBlock_repetition0: 30, openBlock_option0: 31, openBlock_option1: 32, CLOSE: 33, OPEN_INVERSE: 34, openInverse_repetition0: 35, openInverse_option0: 36, openInverse_option1: 37, openInverseChain: 38, OPEN_INVERSE_CHAIN: 39, openInverseChain_repetition0: 40, openInverseChain_option0: 41, openInverseChain_option1: 42, inverseAndProgram: 43, INVERSE: 44, inverseChain: 45, inverseChain_option0: 46, OPEN_ENDBLOCK: 47, OPEN: 48, mustache_repetition0: 49, mustache_option0: 50, OPEN_UNESCAPED: 51, mustache_repetition1: 52, mustache_option1: 53, CLOSE_UNESCAPED: 54, OPEN_PARTIAL: 55, partialName: 56, partial_repetition0: 57, partial_option0: 58, openPartialBlock: 59, OPEN_PARTIAL_BLOCK: 60, openPartialBlock_repetition0: 61, openPartialBlock_option0: 62, param: 63, sexpr: 64, OPEN_SEXPR: 65, sexpr_repetition0: 66, sexpr_option0: 67, CLOSE_SEXPR: 68, hash: 69, hash_repetition_plus0: 70, hashSegment: 71, ID: 72, EQUALS: 73, blockParams: 74, OPEN_BLOCK_PARAMS: 75, blockParams_repetition_plus0: 76, CLOSE_BLOCK_PARAMS: 77, path: 78, dataName: 79, STRING: 80, NUMBER: 81, BOOLEAN: 82, UNDEFINED: 83, NULL: 84, DATA: 85, pathSegments: 86, SEP: 87, $accept: 0, $end: 1 }, terminals_: { 2: "error", 5: "EOF", 14: "COMMENT", 15: "CONTENT", 18: "END_RAW_BLOCK", 19: "OPEN_RAW_BLOCK", 23: "CLOSE_RAW_BLOCK", 29: "OPEN_BLOCK", 33: "CLOSE", 34: "OPEN_INVERSE", 39: "OPEN_INVERSE_CHAIN", 44: "INVERSE", 47: "OPEN_ENDBLOCK", 48: "OPEN", 51: "OPEN_UNESCAPED", 54: "CLOSE_UNESCAPED", 55: "OPEN_PARTIAL", 60: "OPEN_PARTIAL_BLOCK", 65: "OPEN_SEXPR", 68: "CLOSE_SEXPR", 72: "ID", 73: "EQUALS", 75: "OPEN_BLOCK_PARAMS", 77: "CLOSE_BLOCK_PARAMS", 80: "STRING", 81: "NUMBER", 82: "BOOLEAN", 83: "UNDEFINED", 84: "NULL", 85: "DATA", 87: "SEP" }, productions_: [0, [3, 2], [4, 1], [7, 1], [7, 1], [7, 1], [7, 1], [7, 1], [7, 1], [7, 1], [13, 1], [10, 3], [16, 5], [9, 4], [9, 4], [24, 6], [27, 6], [38, 6], [43, 2], [45, 3], [45, 1], [26, 3], [8, 5], [8, 5], [11, 5], [12, 3], [59, 5], [63, 1], [63, 1], [64, 5], [69, 1], [71, 3], [74, 3], [20, 1], [20, 1], [20, 1], [20, 1], [20, 1], [20, 1], [20, 1], [56, 1], [56, 1], [79, 2], [78, 1], [86, 3], [86, 1], [6, 0], [6, 2], [17, 1], [17, 2], [21, 0], [21, 2], [22, 0], [22, 1], [25, 0], [25, 1], [28, 0], [28, 1], [30, 0], [30, 2], [31, 0], [31, 1], [32, 0], [32, 1], [35, 0], [35, 2], [36, 0], [36, 1], [37, 0], [37, 1], [40, 0], [40, 2], [41, 0], [41, 1], [42, 0], [42, 1], [46, 0], [46, 1], [49, 0], [49, 2], [50, 0], [50, 1], [52, 0], [52, 2], [53, 0], [53, 1], [57, 0], [57, 2], [58, 0], [58, 1], [61, 0], [61, 2], [62, 0], [62, 1], [66, 0], [66, 2], [67, 0], [67, 1], [70, 1], [70, 2], [76, 1], [76, 2]], performAction: function (a, b, c, d, e, f, g) { var h = f.length - 1; switch (e) { case 1: return f[h - 1]; case 2: this.$ = d.prepareProgram(f[h]); break; case 3: this.$ = f[h]; break; case 4: this.$ = f[h]; break; case 5: this.$ = f[h]; break; case 6: this.$ = f[h]; break; case 7: this.$ = f[h]; break; case 8: this.$ = f[h]; break; case 9: this.$ = { type: "CommentStatement", value: d.stripComment(f[h]), strip: d.stripFlags(f[h], f[h]), loc: d.locInfo(this._$) }; break; case 10: this.$ = { type: "ContentStatement", original: f[h], value: f[h], loc: d.locInfo(this._$) }; break; case 11: this.$ = d.prepareRawBlock(f[h - 2], f[h - 1], f[h], this._$); break; case 12: this.$ = { path: f[h - 3], params: f[h - 2], hash: f[h - 1] }; break; case 13: this.$ = d.prepareBlock(f[h - 3], f[h - 2], f[h - 1], f[h], !1, this._$); break; case 14: this.$ = d.prepareBlock(f[h - 3], f[h - 2], f[h - 1], f[h], !0, this._$); break; case 15: this.$ = { open: f[h - 5], path: f[h - 4], params: f[h - 3], hash: f[h - 2], blockParams: f[h - 1], strip: d.stripFlags(f[h - 5], f[h]) }; break; case 16: this.$ = { path: f[h - 4], params: f[h - 3], hash: f[h - 2], blockParams: f[h - 1], strip: d.stripFlags(f[h - 5], f[h]) }; break; case 17: this.$ = { path: f[h - 4], params: f[h - 3], hash: f[h - 2], blockParams: f[h - 1], strip: d.stripFlags(f[h - 5], f[h]) }; break; case 18: this.$ = { strip: d.stripFlags(f[h - 1], f[h - 1]), program: f[h] }; break; case 19: var i = d.prepareBlock(f[h - 2], f[h - 1], f[h], f[h], !1, this._$), j = d.prepareProgram([i], f[h - 1].loc); j.chained = !0, this.$ = { strip: f[h - 2].strip, program: j, chain: !0 }; break; case 20: this.$ = f[h]; break; case 21: this.$ = { path: f[h - 1], strip: d.stripFlags(f[h - 2], f[h]) }; break; case 22: this.$ = d.prepareMustache(f[h - 3], f[h - 2], f[h - 1], f[h - 4], d.stripFlags(f[h - 4], f[h]), this._$); break; case 23: this.$ = d.prepareMustache(f[h - 3], f[h - 2], f[h - 1], f[h - 4], d.stripFlags(f[h - 4], f[h]), this._$); break; case 24: this.$ = { type: "PartialStatement", name: f[h - 3], params: f[h - 2], hash: f[h - 1], indent: "", strip: d.stripFlags(f[h - 4], f[h]), loc: d.locInfo(this._$) }; break; case 25: this.$ = d.preparePartialBlock(f[h - 2], f[h - 1], f[h], this._$); break; case 26: this.$ = { path: f[h - 3], params: f[h - 2], hash: f[h - 1], strip: d.stripFlags(f[h - 4], f[h]) }; break; case 27: this.$ = f[h]; break; case 28: this.$ = f[h]; break; case 29: this.$ = { type: "SubExpression", path: f[h - 3], params: f[h - 2], hash: f[h - 1], loc: d.locInfo(this._$) }; break; case 30: this.$ = { type: "Hash", pairs: f[h], loc: d.locInfo(this._$) }; break; case 31: this.$ = { type: "HashPair", key: d.id(f[h - 2]), value: f[h], loc: d.locInfo(this._$) }; break; case 32: this.$ = d.id(f[h - 1]); break; case 33: this.$ = f[h]; break; case 34: this.$ = f[h]; break; case 35: this.$ = { type: "StringLiteral", value: f[h], original: f[h], loc: d.locInfo(this._$) }; break; case 36: this.$ = { type: "NumberLiteral", value: Number(f[h]), original: Number(f[h]), loc: d.locInfo(this._$) }; break; case 37: this.$ = { type: "BooleanLiteral", value: "true" === f[h], original: "true" === f[h], loc: d.locInfo(this._$) }; break; case 38: this.$ = { type: "UndefinedLiteral", original: void 0, value: void 0, loc: d.locInfo(this._$) }; break; case 39: this.$ = { type: "NullLiteral", original: null, value: null, loc: d.locInfo(this._$) }; break; case 40: this.$ = f[h]; break; case 41: this.$ = f[h]; break; case 42: this.$ = d.preparePath(!0, f[h], this._$); break; case 43: this.$ = d.preparePath(!1, f[h], this._$); break; case 44: f[h - 2].push({ part: d.id(f[h]), original: f[h], separator: f[h - 1] }), this.$ = f[h - 2]; break; case 45: this.$ = [{ part: d.id(f[h]), original: f[h] }]; break; case 46: this.$ = []; break; case 47: f[h - 1].push(f[h]); break; case 48: this.$ = [f[h]]; break; case 49: f[h - 1].push(f[h]); break; case 50: this.$ = []; break; case 51: f[h - 1].push(f[h]); break; case 58: this.$ = []; break; case 59: f[h - 1].push(f[h]); break; case 64: this.$ = []; break; case 65: f[h - 1].push(f[h]); break; case 70: this.$ = []; break; case 71: f[h - 1].push(f[h]); break; case 78: this.$ = []; break; case 79: f[h - 1].push(f[h]); break; case 82: this.$ = []; break; case 83: f[h - 1].push(f[h]); break; case 86: this.$ = []; break; case 87: f[h - 1].push(f[h]); break; case 90: this.$ = []; break; case 91: f[h - 1].push(f[h]); break; case 94: this.$ = []; break; case 95: f[h - 1].push(f[h]); break; case 98: this.$ = [f[h]]; break; case 99: f[h - 1].push(f[h]); break; case 100: this.$ = [f[h]]; break; case 101: f[h - 1].push(f[h]) } }, table: [{ 3: 1, 4: 2, 5: [2, 46], 6: 3, 14: [2, 46], 15: [2, 46], 19: [2, 46], 29: [2, 46], 34: [2, 46], 48: [2, 46], 51: [2, 46], 55: [2, 46], 60: [2, 46] }, { 1: [3] }, { 5: [1, 4] }, { 5: [2, 2], 7: 5, 8: 6, 9: 7, 10: 8, 11: 9, 12: 10, 13: 11, 14: [1, 12], 15: [1, 20], 16: 17, 19: [1, 23], 24: 15, 27: 16, 29: [1, 21], 34: [1, 22], 39: [2, 2], 44: [2, 2], 47: [2, 2], 48: [1, 13], 51: [1, 14], 55: [1, 18], 59: 19, 60: [1, 24] }, { 1: [2, 1] }, { 5: [2, 47], 14: [2, 47], 15: [2, 47], 19: [2, 47], 29: [2, 47], 34: [2, 47], 39: [2, 47], 44: [2, 47], 47: [2, 47], 48: [2, 47], 51: [2, 47], 55: [2, 47], 60: [2, 47] }, { 5: [2, 3], 14: [2, 3], 15: [2, 3], 19: [2, 3], 29: [2, 3], 34: [2, 3], 39: [2, 3], 44: [2, 3], 47: [2, 3], 48: [2, 3], 51: [2, 3], 55: [2, 3], 60: [2, 3] }, { 5: [2, 4], 14: [2, 4], 15: [2, 4], 19: [2, 4], 29: [2, 4], 34: [2, 4], 39: [2, 4], 44: [2, 4], 47: [2, 4], 48: [2, 4], 51: [2, 4], 55: [2, 4], 60: [2, 4] }, { 5: [2, 5], 14: [2, 5], 15: [2, 5], 19: [2, 5], 29: [2, 5], 34: [2, 5], 39: [2, 5], 44: [2, 5], 47: [2, 5], 48: [2, 5], 51: [2, 5], 55: [2, 5], 60: [2, 5] }, { 5: [2, 6], 14: [2, 6], 15: [2, 6], 19: [2, 6], 29: [2, 6], 34: [2, 6], 39: [2, 6], 44: [2, 6], 47: [2, 6], 48: [2, 6], 51: [2, 6], 55: [2, 6], 60: [2, 6] }, { 5: [2, 7], 14: [2, 7], 15: [2, 7], 19: [2, 7], 29: [2, 7], 34: [2, 7], 39: [2, 7], 44: [2, 7], 47: [2, 7], 48: [2, 7], 51: [2, 7], 55: [2, 7], 60: [2, 7] }, { 5: [2, 8], 14: [2, 8], 15: [2, 8], 19: [2, 8], 29: [2, 8], 34: [2, 8], 39: [2, 8], 44: [2, 8], 47: [2, 8], 48: [2, 8], 51: [2, 8], 55: [2, 8], 60: [2, 8] }, { 5: [2, 9], 14: [2, 9], 15: [2, 9], 19: [2, 9], 29: [2, 9], 34: [2, 9], 39: [2, 9], 44: [2, 9], 47: [2, 9], 48: [2, 9], 51: [2, 9], 55: [2, 9], 60: [2, 9] }, { 20: 25, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 36, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 4: 37, 6: 3, 14: [2, 46], 15: [2, 46], 19: [2, 46], 29: [2, 46], 34: [2, 46], 39: [2, 46], 44: [2, 46], 47: [2, 46], 48: [2, 46], 51: [2, 46], 55: [2, 46], 60: [2, 46] }, { 4: 38, 6: 3, 14: [2, 46], 15: [2, 46], 19: [2, 46], 29: [2, 46], 34: [2, 46], 44: [2, 46], 47: [2, 46], 48: [2, 46], 51: [2, 46], 55: [2, 46], 60: [2, 46] }, { 13: 40, 15: [1, 20], 17: 39 }, { 20: 42, 56: 41, 64: 43, 65: [1, 44], 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 4: 45, 6: 3, 14: [2, 46], 15: [2, 46], 19: [2, 46], 29: [2, 46], 34: [2, 46], 47: [2, 46], 48: [2, 46], 51: [2, 46], 55: [2, 46], 60: [2, 46] }, { 5: [2, 10], 14: [2, 10], 15: [2, 10], 18: [2, 10], 19: [2, 10], 29: [2, 10], 34: [2, 10], 39: [2, 10], 44: [2, 10], 47: [2, 10], 48: [2, 10], 51: [2, 10], 55: [2, 10], 60: [2, 10] }, { 20: 46, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 47, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 48, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 42, 56: 49, 64: 43, 65: [1, 44], 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 33: [2, 78], 49: 50, 65: [2, 78], 72: [2, 78], 80: [2, 78], 81: [2, 78], 82: [2, 78], 83: [2, 78], 84: [2, 78], 85: [2, 78] }, { 23: [2, 33], 33: [2, 33], 54: [2, 33], 65: [2, 33], 68: [2, 33], 72: [2, 33], 75: [2, 33], 80: [2, 33], 81: [2, 33], 82: [2, 33], 83: [2, 33], 84: [2, 33], 85: [2, 33] }, { 23: [2, 34], 33: [2, 34], 54: [2, 34], 65: [2, 34], 68: [2, 34], 72: [2, 34], 75: [2, 34], 80: [2, 34], 81: [2, 34], 82: [2, 34], 83: [2, 34], 84: [2, 34], 85: [2, 34] }, { 23: [2, 35], 33: [2, 35], 54: [2, 35], 65: [2, 35], 68: [2, 35], 72: [2, 35], 75: [2, 35], 80: [2, 35], 81: [2, 35], 82: [2, 35], 83: [2, 35], 84: [2, 35], 85: [2, 35] }, { 23: [2, 36], 33: [2, 36], 54: [2, 36], 65: [2, 36], 68: [2, 36], 72: [2, 36], 75: [2, 36], 80: [2, 36], 81: [2, 36], 82: [2, 36], 83: [2, 36], 84: [2, 36], 85: [2, 36] }, { 23: [2, 37], 33: [2, 37], 54: [2, 37], 65: [2, 37], 68: [2, 37], 72: [2, 37], 75: [2, 37], 80: [2, 37], 81: [2, 37], 82: [2, 37], 83: [2, 37], 84: [2, 37], 85: [2, 37] }, { 23: [2, 38], 33: [2, 38], 54: [2, 38], 65: [2, 38], 68: [2, 38], 72: [2, 38], 75: [2, 38], 80: [2, 38], 81: [2, 38], 82: [2, 38], 83: [2, 38], 84: [2, 38], 85: [2, 38] }, { 23: [2, 39], 33: [2, 39], 54: [2, 39], 65: [2, 39], 68: [2, 39], 72: [2, 39], 75: [2, 39], 80: [2, 39], 81: [2, 39], 82: [2, 39], 83: [2, 39], 84: [2, 39], 85: [2, 39] }, { 23: [2, 43], 33: [2, 43], 54: [2, 43], 65: [2, 43], 68: [2, 43], 72: [2, 43], 75: [2, 43], 80: [2, 43], 81: [2, 43], 82: [2, 43], 83: [2, 43], 84: [2, 43], 85: [2, 43], 87: [1, 51] }, { 72: [1, 35], 86: 52 }, { 23: [2, 45], 33: [2, 45], 54: [2, 45], 65: [2, 45], 68: [2, 45], 72: [2, 45], 75: [2, 45], 80: [2, 45], 81: [2, 45], 82: [2, 45], 83: [2, 45], 84: [2, 45], 85: [2, 45], 87: [2, 45] }, { 52: 53, 54: [2, 82], 65: [2, 82], 72: [2, 82], 80: [2, 82], 81: [2, 82], 82: [2, 82], 83: [2, 82], 84: [2, 82], 85: [2, 82] }, { 25: 54, 38: 56, 39: [1, 58], 43: 57, 44: [1, 59], 45: 55, 47: [2, 54] }, { 28: 60, 43: 61, 44: [1, 59], 47: [2, 56] }, { 13: 63, 15: [1, 20], 18: [1, 62] }, { 15: [2, 48], 18: [2, 48] }, { 33: [2, 86], 57: 64, 65: [2, 86], 72: [2, 86], 80: [2, 86], 81: [2, 86], 82: [2, 86], 83: [2, 86], 84: [2, 86], 85: [2, 86] }, { 33: [2, 40], 65: [2, 40], 72: [2, 40], 80: [2, 40], 81: [2, 40], 82: [2, 40], 83: [2, 40], 84: [2, 40], 85: [2, 40] }, { 33: [2, 41], 65: [2, 41], 72: [2, 41], 80: [2, 41], 81: [2, 41], 82: [2, 41], 83: [2, 41], 84: [2, 41], 85: [2, 41] }, { 20: 65, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 26: 66, 47: [1, 67] }, { 30: 68, 33: [2, 58], 65: [2, 58], 72: [2, 58], 75: [2, 58], 80: [2, 58], 81: [2, 58], 82: [2, 58], 83: [2, 58], 84: [2, 58], 85: [2, 58] }, { 33: [2, 64], 35: 69, 65: [2, 64], 72: [2, 64], 75: [2, 64], 80: [2, 64], 81: [2, 64], 82: [2, 64], 83: [2, 64], 84: [2, 64], 85: [2, 64] }, { 21: 70, 23: [2, 50], 65: [2, 50], 72: [2, 50], 80: [2, 50], 81: [2, 50], 82: [2, 50], 83: [2, 50], 84: [2, 50], 85: [2, 50] }, { 33: [2, 90], 61: 71, 65: [2, 90], 72: [2, 90], 80: [2, 90], 81: [2, 90], 82: [2, 90], 83: [2, 90], 84: [2, 90], 85: [2, 90] }, { 20: 75, 33: [2, 80], 50: 72, 63: 73, 64: 76, 65: [1, 44], 69: 74, 70: 77, 71: 78, 72: [1, 79], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 72: [1, 80] }, { 23: [2, 42], 33: [2, 42], 54: [2, 42], 65: [2, 42], 68: [2, 42], 72: [2, 42], 75: [2, 42], 80: [2, 42], 81: [2, 42], 82: [2, 42], 83: [2, 42], 84: [2, 42], 85: [2, 42], 87: [1, 51] }, { 20: 75, 53: 81, 54: [2, 84], 63: 82, 64: 76, 65: [1, 44], 69: 83, 70: 77, 71: 78, 72: [1, 79], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 26: 84, 47: [1, 67] }, { 47: [2, 55] }, { 4: 85, 6: 3, 14: [2, 46], 15: [2, 46], 19: [2, 46], 29: [2, 46], 34: [2, 46], 39: [2, 46], 44: [2, 46], 47: [2, 46], 48: [2, 46], 51: [2, 46], 55: [2, 46], 60: [2, 46] }, { 47: [2, 20] }, { 20: 86, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 4: 87, 6: 3, 14: [2, 46], 15: [2, 46], 19: [2, 46], 29: [2, 46], 34: [2, 46], 47: [2, 46], 48: [2, 46], 51: [2, 46], 55: [2, 46], 60: [2, 46] }, { 26: 88, 47: [1, 67] }, { 47: [2, 57] }, { 5: [2, 11], 14: [2, 11], 15: [2, 11], 19: [2, 11], 29: [2, 11], 34: [2, 11], 39: [2, 11], 44: [2, 11], 47: [2, 11], 48: [2, 11], 51: [2, 11], 55: [2, 11], 60: [2, 11] }, { 15: [2, 49], 18: [2, 49] }, { 20: 75, 33: [2, 88], 58: 89, 63: 90, 64: 76, 65: [1, 44], 69: 91, 70: 77, 71: 78, 72: [1, 79], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 65: [2, 94], 66: 92, 68: [2, 94], 72: [2, 94], 80: [2, 94], 81: [2, 94], 82: [2, 94], 83: [2, 94], 84: [2, 94], 85: [2, 94] }, { 5: [2, 25], 14: [2, 25], 15: [2, 25], 19: [2, 25], 29: [2, 25], 34: [2, 25], 39: [2, 25], 44: [2, 25], 47: [2, 25], 48: [2, 25], 51: [2, 25], 55: [2, 25], 60: [2, 25] }, { 20: 93, 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 75, 31: 94, 33: [2, 60], 63: 95, 64: 76, 65: [1, 44], 69: 96, 70: 77, 71: 78, 72: [1, 79], 75: [2, 60], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 75, 33: [2, 66], 36: 97, 63: 98, 64: 76, 65: [1, 44], 69: 99, 70: 77, 71: 78, 72: [1, 79], 75: [2, 66], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 75, 22: 100, 23: [2, 52], 63: 101, 64: 76, 65: [1, 44], 69: 102, 70: 77, 71: 78, 72: [1, 79], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 20: 75, 33: [2, 92], 62: 103, 63: 104, 64: 76, 65: [1, 44], 69: 105, 70: 77, 71: 78, 72: [1, 79], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 33: [1, 106] }, { 33: [2, 79], 65: [2, 79], 72: [2, 79], 80: [2, 79], 81: [2, 79], 82: [2, 79], 83: [2, 79], 84: [2, 79], 85: [2, 79] }, { 33: [2, 81] }, { 23: [2, 27], 33: [2, 27], 54: [2, 27], 65: [2, 27], 68: [2, 27], 72: [2, 27], 75: [2, 27], 80: [2, 27], 81: [2, 27], 82: [2, 27], 83: [2, 27], 84: [2, 27], 85: [2, 27] }, { 23: [2, 28], 33: [2, 28], 54: [2, 28], 65: [2, 28], 68: [2, 28], 72: [2, 28], 75: [2, 28], 80: [2, 28], 81: [2, 28], 82: [2, 28], 83: [2, 28], 84: [2, 28], 85: [2, 28] }, { 23: [2, 30], 33: [2, 30], 54: [2, 30], 68: [2, 30], 71: 107, 72: [1, 108], 75: [2, 30] }, { 23: [2, 98], 33: [2, 98], 54: [2, 98], 68: [2, 98], 72: [2, 98], 75: [2, 98] }, { 23: [2, 45], 33: [2, 45], 54: [2, 45], 65: [2, 45], 68: [2, 45], 72: [2, 45], 73: [1, 109], 75: [2, 45], 80: [2, 45], 81: [2, 45], 82: [2, 45], 83: [2, 45], 84: [2, 45], 85: [2, 45], 87: [2, 45] }, { 23: [2, 44], 33: [2, 44], 54: [2, 44], 65: [2, 44], 68: [2, 44], 72: [2, 44], 75: [2, 44], 80: [2, 44], 81: [2, 44], 82: [2, 44], 83: [2, 44], 84: [2, 44], 85: [2, 44], 87: [2, 44] }, { 54: [1, 110] }, { 54: [2, 83], 65: [2, 83], 72: [2, 83], 80: [2, 83], 81: [2, 83], 82: [2, 83], 83: [2, 83], 84: [2, 83], 85: [2, 83] }, { 54: [2, 85] }, { 5: [2, 13], 14: [2, 13], 15: [2, 13], 19: [2, 13], 29: [2, 13], 34: [2, 13], 39: [2, 13], 44: [2, 13], 47: [2, 13], 48: [2, 13], 51: [2, 13], 55: [2, 13], 60: [2, 13] }, { 38: 56, 39: [1, 58], 43: 57, 44: [1, 59], 45: 112, 46: 111, 47: [2, 76] }, { 33: [2, 70], 40: 113, 65: [2, 70], 72: [2, 70], 75: [2, 70], 80: [2, 70], 81: [2, 70], 82: [2, 70], 83: [2, 70], 84: [2, 70], 85: [2, 70] }, { 47: [2, 18] }, { 5: [2, 14], 14: [2, 14], 15: [2, 14], 19: [2, 14], 29: [2, 14], 34: [2, 14], 39: [2, 14], 44: [2, 14], 47: [2, 14], 48: [2, 14], 51: [2, 14], 55: [2, 14], 60: [2, 14] }, { 33: [1, 114] }, {
                                33: [2, 87], 65: [2, 87], 72: [2, 87], 80: [2, 87], 81: [2, 87], 82: [2, 87], 83: [2, 87], 84: [2, 87],
                                85: [2, 87]
                            }, { 33: [2, 89] }, { 20: 75, 63: 116, 64: 76, 65: [1, 44], 67: 115, 68: [2, 96], 69: 117, 70: 77, 71: 78, 72: [1, 79], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 33: [1, 118] }, { 32: 119, 33: [2, 62], 74: 120, 75: [1, 121] }, { 33: [2, 59], 65: [2, 59], 72: [2, 59], 75: [2, 59], 80: [2, 59], 81: [2, 59], 82: [2, 59], 83: [2, 59], 84: [2, 59], 85: [2, 59] }, { 33: [2, 61], 75: [2, 61] }, { 33: [2, 68], 37: 122, 74: 123, 75: [1, 121] }, { 33: [2, 65], 65: [2, 65], 72: [2, 65], 75: [2, 65], 80: [2, 65], 81: [2, 65], 82: [2, 65], 83: [2, 65], 84: [2, 65], 85: [2, 65] }, { 33: [2, 67], 75: [2, 67] }, { 23: [1, 124] }, { 23: [2, 51], 65: [2, 51], 72: [2, 51], 80: [2, 51], 81: [2, 51], 82: [2, 51], 83: [2, 51], 84: [2, 51], 85: [2, 51] }, { 23: [2, 53] }, { 33: [1, 125] }, { 33: [2, 91], 65: [2, 91], 72: [2, 91], 80: [2, 91], 81: [2, 91], 82: [2, 91], 83: [2, 91], 84: [2, 91], 85: [2, 91] }, { 33: [2, 93] }, { 5: [2, 22], 14: [2, 22], 15: [2, 22], 19: [2, 22], 29: [2, 22], 34: [2, 22], 39: [2, 22], 44: [2, 22], 47: [2, 22], 48: [2, 22], 51: [2, 22], 55: [2, 22], 60: [2, 22] }, { 23: [2, 99], 33: [2, 99], 54: [2, 99], 68: [2, 99], 72: [2, 99], 75: [2, 99] }, { 73: [1, 109] }, { 20: 75, 63: 126, 64: 76, 65: [1, 44], 72: [1, 35], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 5: [2, 23], 14: [2, 23], 15: [2, 23], 19: [2, 23], 29: [2, 23], 34: [2, 23], 39: [2, 23], 44: [2, 23], 47: [2, 23], 48: [2, 23], 51: [2, 23], 55: [2, 23], 60: [2, 23] }, { 47: [2, 19] }, { 47: [2, 77] }, { 20: 75, 33: [2, 72], 41: 127, 63: 128, 64: 76, 65: [1, 44], 69: 129, 70: 77, 71: 78, 72: [1, 79], 75: [2, 72], 78: 26, 79: 27, 80: [1, 28], 81: [1, 29], 82: [1, 30], 83: [1, 31], 84: [1, 32], 85: [1, 34], 86: 33 }, { 5: [2, 24], 14: [2, 24], 15: [2, 24], 19: [2, 24], 29: [2, 24], 34: [2, 24], 39: [2, 24], 44: [2, 24], 47: [2, 24], 48: [2, 24], 51: [2, 24], 55: [2, 24], 60: [2, 24] }, { 68: [1, 130] }, { 65: [2, 95], 68: [2, 95], 72: [2, 95], 80: [2, 95], 81: [2, 95], 82: [2, 95], 83: [2, 95], 84: [2, 95], 85: [2, 95] }, { 68: [2, 97] }, { 5: [2, 21], 14: [2, 21], 15: [2, 21], 19: [2, 21], 29: [2, 21], 34: [2, 21], 39: [2, 21], 44: [2, 21], 47: [2, 21], 48: [2, 21], 51: [2, 21], 55: [2, 21], 60: [2, 21] }, { 33: [1, 131] }, { 33: [2, 63] }, { 72: [1, 133], 76: 132 }, { 33: [1, 134] }, { 33: [2, 69] }, { 15: [2, 12] }, { 14: [2, 26], 15: [2, 26], 19: [2, 26], 29: [2, 26], 34: [2, 26], 47: [2, 26], 48: [2, 26], 51: [2, 26], 55: [2, 26], 60: [2, 26] }, { 23: [2, 31], 33: [2, 31], 54: [2, 31], 68: [2, 31], 72: [2, 31], 75: [2, 31] }, { 33: [2, 74], 42: 135, 74: 136, 75: [1, 121] }, { 33: [2, 71], 65: [2, 71], 72: [2, 71], 75: [2, 71], 80: [2, 71], 81: [2, 71], 82: [2, 71], 83: [2, 71], 84: [2, 71], 85: [2, 71] }, { 33: [2, 73], 75: [2, 73] }, { 23: [2, 29], 33: [2, 29], 54: [2, 29], 65: [2, 29], 68: [2, 29], 72: [2, 29], 75: [2, 29], 80: [2, 29], 81: [2, 29], 82: [2, 29], 83: [2, 29], 84: [2, 29], 85: [2, 29] }, { 14: [2, 15], 15: [2, 15], 19: [2, 15], 29: [2, 15], 34: [2, 15], 39: [2, 15], 44: [2, 15], 47: [2, 15], 48: [2, 15], 51: [2, 15], 55: [2, 15], 60: [2, 15] }, { 72: [1, 138], 77: [1, 137] }, { 72: [2, 100], 77: [2, 100] }, { 14: [2, 16], 15: [2, 16], 19: [2, 16], 29: [2, 16], 34: [2, 16], 44: [2, 16], 47: [2, 16], 48: [2, 16], 51: [2, 16], 55: [2, 16], 60: [2, 16] }, { 33: [1, 139] }, { 33: [2, 75] }, { 33: [2, 32] }, { 72: [2, 101], 77: [2, 101] }, { 14: [2, 17], 15: [2, 17], 19: [2, 17], 29: [2, 17], 34: [2, 17], 39: [2, 17], 44: [2, 17], 47: [2, 17], 48: [2, 17], 51: [2, 17], 55: [2, 17], 60: [2, 17] }], defaultActions: { 4: [2, 1], 55: [2, 55], 57: [2, 20], 61: [2, 57], 74: [2, 81], 83: [2, 85], 87: [2, 18], 91: [2, 89], 102: [2, 53], 105: [2, 93], 111: [2, 19], 112: [2, 77], 117: [2, 97], 120: [2, 63], 123: [2, 69], 124: [2, 12], 136: [2, 75], 137: [2, 32] }, parseError: function (a, b) { throw new Error(a) }, parse: function (a) { function b() { var a; return a = c.lexer.lex() || 1, "number" != typeof a && (a = c.symbols_[a] || a), a } var c = this, d = [0], e = [null], f = [], g = this.table, h = "", i = 0, j = 0, k = 0; this.lexer.setInput(a), this.lexer.yy = this.yy, this.yy.lexer = this.lexer, this.yy.parser = this, "undefined" == typeof this.lexer.yylloc && (this.lexer.yylloc = {}); var l = this.lexer.yylloc; f.push(l); var m = this.lexer.options && this.lexer.options.ranges; "function" == typeof this.yy.parseError && (this.parseError = this.yy.parseError); for (var n, o, p, q, r, s, t, u, v, w = {}; ;) { if (p = d[d.length - 1], this.defaultActions[p] ? q = this.defaultActions[p] : (null !== n && "undefined" != typeof n || (n = b()), q = g[p] && g[p][n]), "undefined" == typeof q || !q.length || !q[0]) { var x = ""; if (!k) { v = []; for (s in g[p]) this.terminals_[s] && s > 2 && v.push("'" + this.terminals_[s] + "'"); x = this.lexer.showPosition ? "Parse error on line " + (i + 1) + ":\n" + this.lexer.showPosition() + "\nExpecting " + v.join(", ") + ", got '" + (this.terminals_[n] || n) + "'" : "Parse error on line " + (i + 1) + ": Unexpected " + (1 == n ? "end of input" : "'" + (this.terminals_[n] || n) + "'"), this.parseError(x, { text: this.lexer.match, token: this.terminals_[n] || n, line: this.lexer.yylineno, loc: l, expected: v }) } } if (q[0] instanceof Array && q.length > 1) throw new Error("Parse Error: multiple actions possible at state: " + p + ", token: " + n); switch (q[0]) { case 1: d.push(n), e.push(this.lexer.yytext), f.push(this.lexer.yylloc), d.push(q[1]), n = null, o ? (n = o, o = null) : (j = this.lexer.yyleng, h = this.lexer.yytext, i = this.lexer.yylineno, l = this.lexer.yylloc, k > 0 && k--); break; case 2: if (t = this.productions_[q[1]][1], w.$ = e[e.length - t], w._$ = { first_line: f[f.length - (t || 1)].first_line, last_line: f[f.length - 1].last_line, first_column: f[f.length - (t || 1)].first_column, last_column: f[f.length - 1].last_column }, m && (w._$.range = [f[f.length - (t || 1)].range[0], f[f.length - 1].range[1]]), r = this.performAction.call(w, h, j, i, this.yy, q[1], e, f), "undefined" != typeof r) return r; t && (d = d.slice(0, -1 * t * 2), e = e.slice(0, -1 * t), f = f.slice(0, -1 * t)), d.push(this.productions_[q[1]][0]), e.push(w.$), f.push(w._$), u = g[d[d.length - 2]][d[d.length - 1]], d.push(u); break; case 3: return !0 } } return !0 }
                        }, c = function () { var a = { EOF: 1, parseError: function (a, b) { if (!this.yy.parser) throw new Error(a); this.yy.parser.parseError(a, b) }, setInput: function (a) { return this._input = a, this._more = this._less = this.done = !1, this.yylineno = this.yyleng = 0, this.yytext = this.matched = this.match = "", this.conditionStack = ["INITIAL"], this.yylloc = { first_line: 1, first_column: 0, last_line: 1, last_column: 0 }, this.options.ranges && (this.yylloc.range = [0, 0]), this.offset = 0, this }, input: function () { var a = this._input[0]; this.yytext += a, this.yyleng++ , this.offset++ , this.match += a, this.matched += a; var b = a.match(/(?:\r\n?|\n).*/g); return b ? (this.yylineno++ , this.yylloc.last_line++) : this.yylloc.last_column++ , this.options.ranges && this.yylloc.range[1]++ , this._input = this._input.slice(1), a }, unput: function (a) { var b = a.length, c = a.split(/(?:\r\n?|\n)/g); this._input = a + this._input, this.yytext = this.yytext.substr(0, this.yytext.length - b - 1), this.offset -= b; var d = this.match.split(/(?:\r\n?|\n)/g); this.match = this.match.substr(0, this.match.length - 1), this.matched = this.matched.substr(0, this.matched.length - 1), c.length - 1 && (this.yylineno -= c.length - 1); var e = this.yylloc.range; return this.yylloc = { first_line: this.yylloc.first_line, last_line: this.yylineno + 1, first_column: this.yylloc.first_column, last_column: c ? (c.length === d.length ? this.yylloc.first_column : 0) + d[d.length - c.length].length - c[0].length : this.yylloc.first_column - b }, this.options.ranges && (this.yylloc.range = [e[0], e[0] + this.yyleng - b]), this }, more: function () { return this._more = !0, this }, less: function (a) { this.unput(this.match.slice(a)) }, pastInput: function () { var a = this.matched.substr(0, this.matched.length - this.match.length); return (a.length > 20 ? "..." : "") + a.substr(-20).replace(/\n/g, "") }, upcomingInput: function () { var a = this.match; return a.length < 20 && (a += this._input.substr(0, 20 - a.length)), (a.substr(0, 20) + (a.length > 20 ? "..." : "")).replace(/\n/g, "") }, showPosition: function () { var a = this.pastInput(), b = new Array(a.length + 1).join("-"); return a + this.upcomingInput() + "\n" + b + "^" }, next: function () { if (this.done) return this.EOF; this._input || (this.done = !0); var a, b, c, d, e; this._more || (this.yytext = "", this.match = ""); for (var f = this._currentRules(), g = 0; g < f.length && (c = this._input.match(this.rules[f[g]]), !c || b && !(c[0].length > b[0].length) || (b = c, d = g, this.options.flex)); g++); return b ? (e = b[0].match(/(?:\r\n?|\n).*/g), e && (this.yylineno += e.length), this.yylloc = { first_line: this.yylloc.last_line, last_line: this.yylineno + 1, first_column: this.yylloc.last_column, last_column: e ? e[e.length - 1].length - e[e.length - 1].match(/\r?\n?/)[0].length : this.yylloc.last_column + b[0].length }, this.yytext += b[0], this.match += b[0], this.matches = b, this.yyleng = this.yytext.length, this.options.ranges && (this.yylloc.range = [this.offset, this.offset += this.yyleng]), this._more = !1, this._input = this._input.slice(b[0].length), this.matched += b[0], a = this.performAction.call(this, this.yy, this, f[d], this.conditionStack[this.conditionStack.length - 1]), this.done && this._input && (this.done = !1), a ? a : void 0) : "" === this._input ? this.EOF : this.parseError("Lexical error on line " + (this.yylineno + 1) + ". Unrecognized text.\n" + this.showPosition(), { text: "", token: null, line: this.yylineno }) }, lex: function () { var a = this.next(); return "undefined" != typeof a ? a : this.lex() }, begin: function (a) { this.conditionStack.push(a) }, popState: function () { return this.conditionStack.pop() }, _currentRules: function () { return this.conditions[this.conditionStack[this.conditionStack.length - 1]].rules }, topState: function () { return this.conditionStack[this.conditionStack.length - 2] }, pushState: function (a) { this.begin(a) } }; return a.options = {}, a.performAction = function (a, b, c, d) { function e(a, c) { return b.yytext = b.yytext.substr(a, b.yyleng - c) } switch (c) { case 0: if ("\\\\" === b.yytext.slice(-2) ? (e(0, 1), this.begin("mu")) : "\\" === b.yytext.slice(-1) ? (e(0, 1), this.begin("emu")) : this.begin("mu"), b.yytext) return 15; break; case 1: return 15; case 2: return this.popState(), 15; case 3: return this.begin("raw"), 15; case 4: return this.popState(), "raw" === this.conditionStack[this.conditionStack.length - 1] ? 15 : (b.yytext = b.yytext.substr(5, b.yyleng - 9), "END_RAW_BLOCK"); case 5: return 15; case 6: return this.popState(), 14; case 7: return 65; case 8: return 68; case 9: return 19; case 10: return this.popState(), this.begin("raw"), 23; case 11: return 55; case 12: return 60; case 13: return 29; case 14: return 47; case 15: return this.popState(), 44; case 16: return this.popState(), 44; case 17: return 34; case 18: return 39; case 19: return 51; case 20: return 48; case 21: this.unput(b.yytext), this.popState(), this.begin("com"); break; case 22: return this.popState(), 14; case 23: return 48; case 24: return 73; case 25: return 72; case 26: return 72; case 27: return 87; case 28: break; case 29: return this.popState(), 54; case 30: return this.popState(), 33; case 31: return b.yytext = e(1, 2).replace(/\\"/g, '"'), 80; case 32: return b.yytext = e(1, 2).replace(/\\'/g, "'"), 80; case 33: return 85; case 34: return 82; case 35: return 82; case 36: return 83; case 37: return 84; case 38: return 81; case 39: return 75; case 40: return 77; case 41: return 72; case 42: return b.yytext = b.yytext.replace(/\\([\\\]])/g, "$1"), 72; case 43: return "INVALID"; case 44: return 5 } }, a.rules = [/^(?:[^\x00]*?(?=(\{\{)))/, /^(?:[^\x00]+)/, /^(?:[^\x00]{2,}?(?=(\{\{|\\\{\{|\\\\\{\{|$)))/, /^(?:\{\{\{\{(?=[^\/]))/, /^(?:\{\{\{\{\/[^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=[=}\s\/.])\}\}\}\})/, /^(?:[^\x00]*?(?=(\{\{\{\{)))/, /^(?:[\s\S]*?--(~)?\}\})/, /^(?:\()/, /^(?:\))/, /^(?:\{\{\{\{)/, /^(?:\}\}\}\})/, /^(?:\{\{(~)?>)/, /^(?:\{\{(~)?#>)/, /^(?:\{\{(~)?#\*?)/, /^(?:\{\{(~)?\/)/, /^(?:\{\{(~)?\^\s*(~)?\}\})/, /^(?:\{\{(~)?\s*else\s*(~)?\}\})/, /^(?:\{\{(~)?\^)/, /^(?:\{\{(~)?\s*else\b)/, /^(?:\{\{(~)?\{)/, /^(?:\{\{(~)?&)/, /^(?:\{\{(~)?!--)/, /^(?:\{\{(~)?![\s\S]*?\}\})/, /^(?:\{\{(~)?\*?)/, /^(?:=)/, /^(?:\.\.)/, /^(?:\.(?=([=~}\s\/.)|])))/, /^(?:[\/.])/, /^(?:\s+)/, /^(?:\}(~)?\}\})/, /^(?:(~)?\}\})/, /^(?:"(\\["]|[^"])*")/, /^(?:'(\\[']|[^'])*')/, /^(?:@)/, /^(?:true(?=([~}\s)])))/, /^(?:false(?=([~}\s)])))/, /^(?:undefined(?=([~}\s)])))/, /^(?:null(?=([~}\s)])))/, /^(?:-?[0-9]+(?:\.[0-9]+)?(?=([~}\s)])))/, /^(?:as\s+\|)/, /^(?:\|)/, /^(?:([^\s!"#%-,\.\/;->@\[-\^`\{-~]+(?=([=~}\s\/.)|]))))/, /^(?:\[(\\\]|[^\]])*\])/, /^(?:.)/, /^(?:$)/], a.conditions = { mu: { rules: [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44], inclusive: !1 }, emu: { rules: [2], inclusive: !1 }, com: { rules: [6], inclusive: !1 }, raw: { rules: [3, 4, 5], inclusive: !1 }, INITIAL: { rules: [0, 1, 44], inclusive: !0 } }, a }(); return b.lexer = c, a.prototype = b, b.Parser = a, new a
                    }(); b["default"] = c, a.exports = b["default"]
                }, function (a, b, c) { "use strict"; function d() { var a = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0]; this.options = a } function e(a, b, c) { void 0 === b && (b = a.length); var d = a[b - 1], e = a[b - 2]; return d ? "ContentStatement" === d.type ? (e || !c ? /\r?\n\s*?$/ : /(^|\r?\n)\s*?$/).test(d.original) : void 0 : c } function f(a, b, c) { void 0 === b && (b = -1); var d = a[b + 1], e = a[b + 2]; return d ? "ContentStatement" === d.type ? (e || !c ? /^\s*?\r?\n/ : /^\s*?(\r?\n|$)/).test(d.original) : void 0 : c } function g(a, b, c) { var d = a[null == b ? 0 : b + 1]; if (d && "ContentStatement" === d.type && (c || !d.rightStripped)) { var e = d.value; d.value = d.value.replace(c ? /^\s+/ : /^[ \t]*\r?\n?/, ""), d.rightStripped = d.value !== e } } function h(a, b, c) { var d = a[null == b ? a.length - 1 : b - 1]; if (d && "ContentStatement" === d.type && (c || !d.leftStripped)) { var e = d.value; return d.value = d.value.replace(c ? /\s+$/ : /[ \t]+$/, ""), d.leftStripped = d.value !== e, d.leftStripped } } var i = c(1)["default"]; b.__esModule = !0; var j = c(39), k = i(j); d.prototype = new k["default"], d.prototype.Program = function (a) { var b = !this.options.ignoreStandalone, c = !this.isRootSeen; this.isRootSeen = !0; for (var d = a.body, i = 0, j = d.length; i < j; i++) { var k = d[i], l = this.accept(k); if (l) { var m = e(d, i, c), n = f(d, i, c), o = l.openStandalone && m, p = l.closeStandalone && n, q = l.inlineStandalone && m && n; l.close && g(d, i, !0), l.open && h(d, i, !0), b && q && (g(d, i), h(d, i) && "PartialStatement" === k.type && (k.indent = /([ \t]+$)/.exec(d[i - 1].original)[1])), b && o && (g((k.program || k.inverse).body), h(d, i)), b && p && (g(d, i), h((k.inverse || k.program).body)) } } return a }, d.prototype.BlockStatement = d.prototype.DecoratorBlock = d.prototype.PartialBlockStatement = function (a) { this.accept(a.program), this.accept(a.inverse); var b = a.program || a.inverse, c = a.program && a.inverse, d = c, i = c; if (c && c.chained) for (d = c.body[0].program; i.chained;)i = i.body[i.body.length - 1].program; var j = { open: a.openStrip.open, close: a.closeStrip.close, openStandalone: f(b.body), closeStandalone: e((d || b).body) }; if (a.openStrip.close && g(b.body, null, !0), c) { var k = a.inverseStrip; k.open && h(b.body, null, !0), k.close && g(d.body, null, !0), a.closeStrip.open && h(i.body, null, !0), !this.options.ignoreStandalone && e(b.body) && f(d.body) && (h(b.body), g(d.body)) } else a.closeStrip.open && h(b.body, null, !0); return j }, d.prototype.Decorator = d.prototype.MustacheStatement = function (a) { return a.strip }, d.prototype.PartialStatement = d.prototype.CommentStatement = function (a) { var b = a.strip || {}; return { inlineStandalone: !0, open: b.open, close: b.close } }, b["default"] = d, a.exports = b["default"] }, function (a, b, c) { "use strict"; function d() { this.parents = [] } function e(a) { this.acceptRequired(a, "path"), this.acceptArray(a.params), this.acceptKey(a, "hash") } function f(a) { e.call(this, a), this.acceptKey(a, "program"), this.acceptKey(a, "inverse") } function g(a) { this.acceptRequired(a, "name"), this.acceptArray(a.params), this.acceptKey(a, "hash") } var h = c(1)["default"]; b.__esModule = !0; var i = c(6), j = h(i); d.prototype = { constructor: d, mutating: !1, acceptKey: function (a, b) { var c = this.accept(a[b]); if (this.mutating) { if (c && !d.prototype[c.type]) throw new j["default"]('Unexpected node type "' + c.type + '" found when accepting ' + b + " on " + a.type); a[b] = c } }, acceptRequired: function (a, b) { if (this.acceptKey(a, b), !a[b]) throw new j["default"](a.type + " requires " + b) }, acceptArray: function (a) { for (var b = 0, c = a.length; b < c; b++)this.acceptKey(a, b), a[b] || (a.splice(b, 1), b-- , c--) }, accept: function (a) { if (a) { if (!this[a.type]) throw new j["default"]("Unknown type: " + a.type, a); this.current && this.parents.unshift(this.current), this.current = a; var b = this[a.type](a); return this.current = this.parents.shift(), !this.mutating || b ? b : b !== !1 ? a : void 0 } }, Program: function (a) { this.acceptArray(a.body) }, MustacheStatement: e, Decorator: e, BlockStatement: f, DecoratorBlock: f, PartialStatement: g, PartialBlockStatement: function (a) { g.call(this, a), this.acceptKey(a, "program") }, ContentStatement: function () { }, CommentStatement: function () { }, SubExpression: e, PathExpression: function () { }, StringLiteral: function () { }, NumberLiteral: function () { }, BooleanLiteral: function () { }, UndefinedLiteral: function () { }, NullLiteral: function () { }, Hash: function (a) { this.acceptArray(a.pairs) }, HashPair: function (a) { this.acceptRequired(a, "value") } }, b["default"] = d, a.exports = b["default"] }, function (a, b, c) { "use strict"; function d(a, b) { if (b = b.path ? b.path.original : b, a.path.original !== b) { var c = { loc: a.path.loc }; throw new q["default"](a.path.original + " doesn't match " + b, c) } } function e(a, b) { this.source = a, this.start = { line: b.first_line, column: b.first_column }, this.end = { line: b.last_line, column: b.last_column } } function f(a) { return /^\[.*\]$/.test(a) ? a.substr(1, a.length - 2) : a } function g(a, b) { return { open: "~" === a.charAt(2), close: "~" === b.charAt(b.length - 3) } } function h(a) { return a.replace(/^\{\{~?\!-?-?/, "").replace(/-?-?~?\}\}$/, "") } function i(a, b, c) { c = this.locInfo(c); for (var d = a ? "@" : "", e = [], f = 0, g = "", h = 0, i = b.length; h < i; h++) { var j = b[h].part, k = b[h].original !== j; if (d += (b[h].separator || "") + j, k || ".." !== j && "." !== j && "this" !== j) e.push(j); else { if (e.length > 0) throw new q["default"]("Invalid path: " + d, { loc: c }); ".." === j && (f++ , g += "../") } } return { type: "PathExpression", data: a, depth: f, parts: e, original: d, loc: c } } function j(a, b, c, d, e, f) { var g = d.charAt(3) || d.charAt(2), h = "{" !== g && "&" !== g, i = /\*/.test(d); return { type: i ? "Decorator" : "MustacheStatement", path: a, params: b, hash: c, escaped: h, strip: e, loc: this.locInfo(f) } } function k(a, b, c, e) { d(a, c), e = this.locInfo(e); var f = { type: "Program", body: b, strip: {}, loc: e }; return { type: "BlockStatement", path: a.path, params: a.params, hash: a.hash, program: f, openStrip: {}, inverseStrip: {}, closeStrip: {}, loc: e } } function l(a, b, c, e, f, g) { e && e.path && d(a, e); var h = /\*/.test(a.open); b.blockParams = a.blockParams; var i = void 0, j = void 0; if (c) { if (h) throw new q["default"]("Unexpected inverse block on decorator", c); c.chain && (c.program.body[0].closeStrip = e.strip), j = c.strip, i = c.program } return f && (f = i, i = b, b = f), { type: h ? "DecoratorBlock" : "BlockStatement", path: a.path, params: a.params, hash: a.hash, program: b, inverse: i, openStrip: a.strip, inverseStrip: j, closeStrip: e && e.strip, loc: this.locInfo(g) } } function m(a, b) { if (!b && a.length) { var c = a[0].loc, d = a[a.length - 1].loc; c && d && (b = { source: c.source, start: { line: c.start.line, column: c.start.column }, end: { line: d.end.line, column: d.end.column } }) } return { type: "Program", body: a, strip: {}, loc: b } } function n(a, b, c, e) { return d(a, c), { type: "PartialBlockStatement", name: a.path, params: a.params, hash: a.hash, program: b, openStrip: a.strip, closeStrip: c && c.strip, loc: this.locInfo(e) } } var o = c(1)["default"]; b.__esModule = !0, b.SourceLocation = e, b.id = f, b.stripFlags = g, b.stripComment = h, b.preparePath = i, b.prepareMustache = j, b.prepareRawBlock = k, b.prepareBlock = l, b.prepareProgram = m, b.preparePartialBlock = n; var p = c(6), q = o(p) }, function (a, b, c) { "use strict"; function d() { } function e(a, b, c) { if (null == a || "string" != typeof a && "Program" !== a.type) throw new k["default"]("You must pass a string or Handlebars AST to Handlebars.precompile. You passed " + a); b = b || {}, "data" in b || (b.data = !0), b.compat && (b.useDepths = !0); var d = c.parse(a, b), e = (new c.Compiler).compile(d, b); return (new c.JavaScriptCompiler).compile(e, b) } function f(a, b, c) { function d() { var d = c.parse(a, b), e = (new c.Compiler).compile(d, b), f = (new c.JavaScriptCompiler).compile(e, b, void 0, !0); return c.template(f) } function e(a, b) { return f || (f = d()), f.call(this, a, b) } if (void 0 === b && (b = {}), null == a || "string" != typeof a && "Program" !== a.type) throw new k["default"]("You must pass a string or Handlebars AST to Handlebars.compile. You passed " + a); b = l.extend({}, b), "data" in b || (b.data = !0), b.compat && (b.useDepths = !0); var f = void 0; return e._setup = function (a) { return f || (f = d()), f._setup(a) }, e._child = function (a, b, c, e) { return f || (f = d()), f._child(a, b, c, e) }, e } function g(a, b) { if (a === b) return !0; if (l.isArray(a) && l.isArray(b) && a.length === b.length) { for (var c = 0; c < a.length; c++)if (!g(a[c], b[c])) return !1; return !0 } } function h(a) { if (!a.path.parts) { var b = a.path; a.path = { type: "PathExpression", data: !1, depth: 0, parts: [b.original + ""], original: b.original + "", loc: b.loc } } } var i = c(1)["default"]; b.__esModule = !0, b.Compiler = d, b.precompile = e, b.compile = f; var j = c(6), k = i(j), l = c(5), m = c(35), n = i(m), o = [].slice; d.prototype = { compiler: d, equals: function (a) { var b = this.opcodes.length; if (a.opcodes.length !== b) return !1; for (var c = 0; c < b; c++) { var d = this.opcodes[c], e = a.opcodes[c]; if (d.opcode !== e.opcode || !g(d.args, e.args)) return !1 } b = this.children.length; for (var c = 0; c < b; c++)if (!this.children[c].equals(a.children[c])) return !1; return !0 }, guid: 0, compile: function (a, b) { this.sourceNode = [], this.opcodes = [], this.children = [], this.options = b, this.stringParams = b.stringParams, this.trackIds = b.trackIds, b.blockParams = b.blockParams || []; var c = b.knownHelpers; if (b.knownHelpers = { helperMissing: !0, blockHelperMissing: !0, each: !0, "if": !0, unless: !0, "with": !0, log: !0, lookup: !0 }, c) for (var d in c) d in c && (this.options.knownHelpers[d] = c[d]); return this.accept(a) }, compileProgram: function (a) { var b = new this.compiler, c = b.compile(a, this.options), d = this.guid++; return this.usePartial = this.usePartial || c.usePartial, this.children[d] = c, this.useDepths = this.useDepths || c.useDepths, d }, accept: function (a) { if (!this[a.type]) throw new k["default"]("Unknown type: " + a.type, a); this.sourceNode.unshift(a); var b = this[a.type](a); return this.sourceNode.shift(), b }, Program: function (a) { this.options.blockParams.unshift(a.blockParams); for (var b = a.body, c = b.length, d = 0; d < c; d++)this.accept(b[d]); return this.options.blockParams.shift(), this.isSimple = 1 === c, this.blockParams = a.blockParams ? a.blockParams.length : 0, this }, BlockStatement: function (a) { h(a); var b = a.program, c = a.inverse; b = b && this.compileProgram(b), c = c && this.compileProgram(c); var d = this.classifySexpr(a); "helper" === d ? this.helperSexpr(a, b, c) : "simple" === d ? (this.simpleSexpr(a), this.opcode("pushProgram", b), this.opcode("pushProgram", c), this.opcode("emptyHash"), this.opcode("blockValue", a.path.original)) : (this.ambiguousSexpr(a, b, c), this.opcode("pushProgram", b), this.opcode("pushProgram", c), this.opcode("emptyHash"), this.opcode("ambiguousBlockValue")), this.opcode("append") }, DecoratorBlock: function (a) { var b = a.program && this.compileProgram(a.program), c = this.setupFullMustacheParams(a, b, void 0), d = a.path; this.useDecorators = !0, this.opcode("registerDecorator", c.length, d.original) }, PartialStatement: function (a) { this.usePartial = !0; var b = a.program; b && (b = this.compileProgram(a.program)); var c = a.params; if (c.length > 1) throw new k["default"]("Unsupported number of partial arguments: " + c.length, a); c.length || (this.options.explicitPartialContext ? this.opcode("pushLiteral", "undefined") : c.push({ type: "PathExpression", parts: [], depth: 0 })); var d = a.name.original, e = "SubExpression" === a.name.type; e && this.accept(a.name), this.setupFullMustacheParams(a, b, void 0, !0); var f = a.indent || ""; this.options.preventIndent && f && (this.opcode("appendContent", f), f = ""), this.opcode("invokePartial", e, d, f), this.opcode("append") }, PartialBlockStatement: function (a) { this.PartialStatement(a) }, MustacheStatement: function (a) { this.SubExpression(a), a.escaped && !this.options.noEscape ? this.opcode("appendEscaped") : this.opcode("append") }, Decorator: function (a) { this.DecoratorBlock(a) }, ContentStatement: function (a) { a.value && this.opcode("appendContent", a.value) }, CommentStatement: function () { }, SubExpression: function (a) { h(a); var b = this.classifySexpr(a); "simple" === b ? this.simpleSexpr(a) : "helper" === b ? this.helperSexpr(a) : this.ambiguousSexpr(a) }, ambiguousSexpr: function (a, b, c) { var d = a.path, e = d.parts[0], f = null != b || null != c; this.opcode("getContext", d.depth), this.opcode("pushProgram", b), this.opcode("pushProgram", c), d.strict = !0, this.accept(d), this.opcode("invokeAmbiguous", e, f) }, simpleSexpr: function (a) { var b = a.path; b.strict = !0, this.accept(b), this.opcode("resolvePossibleLambda") }, helperSexpr: function (a, b, c) { var d = this.setupFullMustacheParams(a, b, c), e = a.path, f = e.parts[0]; if (this.options.knownHelpers[f]) this.opcode("invokeKnownHelper", d.length, f); else { if (this.options.knownHelpersOnly) throw new k["default"]("You specified knownHelpersOnly, but used the unknown helper " + f, a); e.strict = !0, e.falsy = !0, this.accept(e), this.opcode("invokeHelper", d.length, e.original, n["default"].helpers.simpleId(e)) } }, PathExpression: function (a) { this.addDepth(a.depth), this.opcode("getContext", a.depth); var b = a.parts[0], c = n["default"].helpers.scopedId(a), d = !a.depth && !c && this.blockParamIndex(b); d ? this.opcode("lookupBlockParam", d, a.parts) : b ? a.data ? (this.options.data = !0, this.opcode("lookupData", a.depth, a.parts, a.strict)) : this.opcode("lookupOnContext", a.parts, a.falsy, a.strict, c) : this.opcode("pushContext") }, StringLiteral: function (a) { this.opcode("pushString", a.value) }, NumberLiteral: function (a) { this.opcode("pushLiteral", a.value) }, BooleanLiteral: function (a) { this.opcode("pushLiteral", a.value) }, UndefinedLiteral: function () { this.opcode("pushLiteral", "undefined") }, NullLiteral: function () { this.opcode("pushLiteral", "null") }, Hash: function (a) { var b = a.pairs, c = 0, d = b.length; for (this.opcode("pushHash"); c < d; c++)this.pushParam(b[c].value); for (; c--;)this.opcode("assignToHash", b[c].key); this.opcode("popHash") }, opcode: function (a) { this.opcodes.push({ opcode: a, args: o.call(arguments, 1), loc: this.sourceNode[0].loc }) }, addDepth: function (a) { a && (this.useDepths = !0) }, classifySexpr: function (a) { var b = n["default"].helpers.simpleId(a.path), c = b && !!this.blockParamIndex(a.path.parts[0]), d = !c && n["default"].helpers.helperExpression(a), e = !c && (d || b); if (e && !d) { var f = a.path.parts[0], g = this.options; g.knownHelpers[f] ? d = !0 : g.knownHelpersOnly && (e = !1) } return d ? "helper" : e ? "ambiguous" : "simple" }, pushParams: function (a) { for (var b = 0, c = a.length; b < c; b++)this.pushParam(a[b]) }, pushParam: function (a) { var b = null != a.value ? a.value : a.original || ""; if (this.stringParams) b.replace && (b = b.replace(/^(\.?\.\/)*/g, "").replace(/\//g, ".")), a.depth && this.addDepth(a.depth), this.opcode("getContext", a.depth || 0), this.opcode("pushStringParam", b, a.type), "SubExpression" === a.type && this.accept(a); else { if (this.trackIds) { var c = void 0; if (!a.parts || n["default"].helpers.scopedId(a) || a.depth || (c = this.blockParamIndex(a.parts[0])), c) { var d = a.parts.slice(1).join("."); this.opcode("pushId", "BlockParam", c, d) } else b = a.original || b, b.replace && (b = b.replace(/^this(?:\.|$)/, "").replace(/^\.\//, "").replace(/^\.$/, "")), this.opcode("pushId", a.type, b) } this.accept(a) } }, setupFullMustacheParams: function (a, b, c, d) { var e = a.params; return this.pushParams(e), this.opcode("pushProgram", b), this.opcode("pushProgram", c), a.hash ? this.accept(a.hash) : this.opcode("emptyHash", d), e }, blockParamIndex: function (a) { for (var b = 0, c = this.options.blockParams.length; b < c; b++) { var d = this.options.blockParams[b], e = d && l.indexOf(d, a); if (d && e >= 0) return [b, e] } } } }, function (a, b, c) {
                    "use strict"; function d(a) { this.value = a } function e() { } function f(a, b, c, d) { var e = b.popStack(), f = 0, g = c.length; for (a && g--; f < g; f++)e = b.nameLookup(e, c[f], d); return a ? [b.aliasable("container.strict"), "(", e, ", ", b.quotedString(c[f]), ")"] : e } var g = c(1)["default"]; b.__esModule = !0; var h = c(4), i = c(6), j = g(i), k = c(5), l = c(43), m = g(l); e.prototype = {
                        nameLookup: function (a, b) { return e.isValidJavaScriptVariableName(b) ? [a, ".", b] : [a, "[", JSON.stringify(b), "]"] }, depthedLookup: function (a) { return [this.aliasable("container.lookup"), '(depths, "', a, '")'] }, compilerInfo: function () { var a = h.COMPILER_REVISION, b = h.REVISION_CHANGES[a]; return [a, b] }, appendToBuffer: function (a, b, c) { return k.isArray(a) || (a = [a]), a = this.source.wrap(a, b), this.environment.isSimple ? ["return ", a, ";"] : c ? ["buffer += ", a, ";"] : (a.appendToBuffer = !0, a) }, initializeBuffer: function () { return this.quotedString("") }, compile: function (a, b, c, d) { this.environment = a, this.options = b, this.stringParams = this.options.stringParams, this.trackIds = this.options.trackIds, this.precompile = !d, this.name = this.environment.name, this.isChild = !!c, this.context = c || { decorators: [], programs: [], environments: [] }, this.preamble(), this.stackSlot = 0, this.stackVars = [], this.aliases = {}, this.registers = { list: [] }, this.hashes = [], this.compileStack = [], this.inlineStack = [], this.blockParams = [], this.compileChildren(a, b), this.useDepths = this.useDepths || a.useDepths || a.useDecorators || this.options.compat, this.useBlockParams = this.useBlockParams || a.useBlockParams; var e = a.opcodes, f = void 0, g = void 0, h = void 0, i = void 0; for (h = 0, i = e.length; h < i; h++)f = e[h], this.source.currentLocation = f.loc, g = g || f.loc, this[f.opcode].apply(this, f.args); if (this.source.currentLocation = g, this.pushSource(""), this.stackSlot || this.inlineStack.length || this.compileStack.length) throw new j["default"]("Compile completed with content left on stack"); this.decorators.isEmpty() ? this.decorators = void 0 : (this.useDecorators = !0, this.decorators.prepend("var decorators = container.decorators;\n"), this.decorators.push("return fn;"), d ? this.decorators = Function.apply(this, ["fn", "props", "container", "depth0", "data", "blockParams", "depths", this.decorators.merge()]) : (this.decorators.prepend("function(fn, props, container, depth0, data, blockParams, depths) {\n"), this.decorators.push("}\n"), this.decorators = this.decorators.merge())); var k = this.createFunctionContext(d); if (this.isChild) return k; var l = { compiler: this.compilerInfo(), main: k }; this.decorators && (l.main_d = this.decorators, l.useDecorators = !0); var m = this.context, n = m.programs, o = m.decorators; for (h = 0, i = n.length; h < i; h++)n[h] && (l[h] = n[h], o[h] && (l[h + "_d"] = o[h], l.useDecorators = !0)); return this.environment.usePartial && (l.usePartial = !0), this.options.data && (l.useData = !0), this.useDepths && (l.useDepths = !0), this.useBlockParams && (l.useBlockParams = !0), this.options.compat && (l.compat = !0), d ? l.compilerOptions = this.options : (l.compiler = JSON.stringify(l.compiler), this.source.currentLocation = { start: { line: 1, column: 0 } }, l = this.objectLiteral(l), b.srcName ? (l = l.toStringWithSourceMap({ file: b.destName }), l.map = l.map && l.map.toString()) : l = l.toString()), l }, preamble: function () { this.lastContext = 0, this.source = new m["default"](this.options.srcName), this.decorators = new m["default"](this.options.srcName) }, createFunctionContext: function (a) { var b = "", c = this.stackVars.concat(this.registers.list); c.length > 0 && (b += ", " + c.join(", ")); var d = 0; for (var e in this.aliases) { var f = this.aliases[e]; this.aliases.hasOwnProperty(e) && f.children && f.referenceCount > 1 && (b += ", alias" + ++d + "=" + e, f.children[0] = "alias" + d) } var g = ["container", "depth0", "helpers", "partials", "data"]; (this.useBlockParams || this.useDepths) && g.push("blockParams"), this.useDepths && g.push("depths"); var h = this.mergeSource(b); return a ? (g.push(h), Function.apply(this, g)) : this.source.wrap(["function(", g.join(","), ") {\n  ", h, "}"]) }, mergeSource: function (a) { var b = this.environment.isSimple, c = !this.forceBuffer, d = void 0, e = void 0, f = void 0, g = void 0; return this.source.each(function (a) { a.appendToBuffer ? (f ? a.prepend("  + ") : f = a, g = a) : (f && (e ? f.prepend("buffer += ") : d = !0, g.add(";"), f = g = void 0), e = !0, b || (c = !1)) }), c ? f ? (f.prepend("return "), g.add(";")) : e || this.source.push('return "";') : (a += ", buffer = " + (d ? "" : this.initializeBuffer()), f ? (f.prepend("return buffer + "), g.add(";")) : this.source.push("return buffer;")), a && this.source.prepend("var " + a.substring(2) + (d ? "" : ";\n")), this.source.merge() }, blockValue: function (a) { var b = this.aliasable("helpers.blockHelperMissing"), c = [this.contextName(0)]; this.setupHelperArgs(a, 0, c); var d = this.popStack(); c.splice(1, 0, d), this.push(this.source.functionCall(b, "call", c)) }, ambiguousBlockValue: function () { var a = this.aliasable("helpers.blockHelperMissing"), b = [this.contextName(0)]; this.setupHelperArgs("", 0, b, !0), this.flushInline(); var c = this.topStack(); b.splice(1, 0, c), this.pushSource(["if (!", this.lastHelper, ") { ", c, " = ", this.source.functionCall(a, "call", b), "}"]) }, appendContent: function (a) { this.pendingContent ? a = this.pendingContent + a : this.pendingLocation = this.source.currentLocation, this.pendingContent = a }, append: function () { if (this.isInline()) this.replaceStack(function (a) { return [" != null ? ", a, ' : ""'] }), this.pushSource(this.appendToBuffer(this.popStack())); else { var a = this.popStack(); this.pushSource(["if (", a, " != null) { ", this.appendToBuffer(a, void 0, !0), " }"]), this.environment.isSimple && this.pushSource(["else { ", this.appendToBuffer("''", void 0, !0), " }"]) } }, appendEscaped: function () { this.pushSource(this.appendToBuffer([this.aliasable("container.escapeExpression"), "(", this.popStack(), ")"])) }, getContext: function (a) { this.lastContext = a }, pushContext: function () { this.pushStackLiteral(this.contextName(this.lastContext)) }, lookupOnContext: function (a, b, c, d) { var e = 0; d || !this.options.compat || this.lastContext ? this.pushContext() : this.push(this.depthedLookup(a[e++])), this.resolvePath("context", a, e, b, c) }, lookupBlockParam: function (a, b) { this.useBlockParams = !0, this.push(["blockParams[", a[0], "][", a[1], "]"]), this.resolvePath("context", b, 1) }, lookupData: function (a, b, c) { a ? this.pushStackLiteral("container.data(data, " + a + ")") : this.pushStackLiteral("data"), this.resolvePath("data", b, 0, !0, c) }, resolvePath: function (a, b, c, d, e) {
                            var g = this; if (this.options.strict || this.options.assumeObjects) return void this.push(f(this.options.strict && e, this, b, a)); for (var h = b.length; c < h; c++)this.replaceStack(function (e) {
                                var f = g.nameLookup(e, b[c], a); return d ? [" && ", f] : [" != null ? ", f, " : ", e]
                            })
                        }, resolvePossibleLambda: function () { this.push([this.aliasable("container.lambda"), "(", this.popStack(), ", ", this.contextName(0), ")"]) }, pushStringParam: function (a, b) { this.pushContext(), this.pushString(b), "SubExpression" !== b && ("string" == typeof a ? this.pushString(a) : this.pushStackLiteral(a)) }, emptyHash: function (a) { this.trackIds && this.push("{}"), this.stringParams && (this.push("{}"), this.push("{}")), this.pushStackLiteral(a ? "undefined" : "{}") }, pushHash: function () { this.hash && this.hashes.push(this.hash), this.hash = { values: [], types: [], contexts: [], ids: [] } }, popHash: function () { var a = this.hash; this.hash = this.hashes.pop(), this.trackIds && this.push(this.objectLiteral(a.ids)), this.stringParams && (this.push(this.objectLiteral(a.contexts)), this.push(this.objectLiteral(a.types))), this.push(this.objectLiteral(a.values)) }, pushString: function (a) { this.pushStackLiteral(this.quotedString(a)) }, pushLiteral: function (a) { this.pushStackLiteral(a) }, pushProgram: function (a) { null != a ? this.pushStackLiteral(this.programExpression(a)) : this.pushStackLiteral(null) }, registerDecorator: function (a, b) { var c = this.nameLookup("decorators", b, "decorator"), d = this.setupHelperArgs(b, a); this.decorators.push(["fn = ", this.decorators.functionCall(c, "", ["fn", "props", "container", d]), " || fn;"]) }, invokeHelper: function (a, b, c) { var d = this.popStack(), e = this.setupHelper(a, b), f = c ? [e.name, " || "] : "", g = ["("].concat(f, d); this.options.strict || g.push(" || ", this.aliasable("helpers.helperMissing")), g.push(")"), this.push(this.source.functionCall(g, "call", e.callParams)) }, invokeKnownHelper: function (a, b) { var c = this.setupHelper(a, b); this.push(this.source.functionCall(c.name, "call", c.callParams)) }, invokeAmbiguous: function (a, b) { this.useRegister("helper"); var c = this.popStack(); this.emptyHash(); var d = this.setupHelper(0, a, b), e = this.lastHelper = this.nameLookup("helpers", a, "helper"), f = ["(", "(helper = ", e, " || ", c, ")"]; this.options.strict || (f[0] = "(helper = ", f.push(" != null ? helper : ", this.aliasable("helpers.helperMissing"))), this.push(["(", f, d.paramsInit ? ["),(", d.paramsInit] : [], "),", "(typeof helper === ", this.aliasable('"function"'), " ? ", this.source.functionCall("helper", "call", d.callParams), " : helper))"]) }, invokePartial: function (a, b, c) { var d = [], e = this.setupParams(b, 1, d); a && (b = this.popStack(), delete e.name), c && (e.indent = JSON.stringify(c)), e.helpers = "helpers", e.partials = "partials", e.decorators = "container.decorators", a ? d.unshift(b) : d.unshift(this.nameLookup("partials", b, "partial")), this.options.compat && (e.depths = "depths"), e = this.objectLiteral(e), d.push(e), this.push(this.source.functionCall("container.invokePartial", "", d)) }, assignToHash: function (a) { var b = this.popStack(), c = void 0, d = void 0, e = void 0; this.trackIds && (e = this.popStack()), this.stringParams && (d = this.popStack(), c = this.popStack()); var f = this.hash; c && (f.contexts[a] = c), d && (f.types[a] = d), e && (f.ids[a] = e), f.values[a] = b }, pushId: function (a, b, c) { "BlockParam" === a ? this.pushStackLiteral("blockParams[" + b[0] + "].path[" + b[1] + "]" + (c ? " + " + JSON.stringify("." + c) : "")) : "PathExpression" === a ? this.pushString(b) : "SubExpression" === a ? this.pushStackLiteral("true") : this.pushStackLiteral("null") }, compiler: e, compileChildren: function (a, b) { for (var c = a.children, d = void 0, e = void 0, f = 0, g = c.length; f < g; f++) { d = c[f], e = new this.compiler; var h = this.matchExistingProgram(d); if (null == h) { this.context.programs.push(""); var i = this.context.programs.length; d.index = i, d.name = "program" + i, this.context.programs[i] = e.compile(d, b, this.context, !this.precompile), this.context.decorators[i] = e.decorators, this.context.environments[i] = d, this.useDepths = this.useDepths || e.useDepths, this.useBlockParams = this.useBlockParams || e.useBlockParams, d.useDepths = this.useDepths, d.useBlockParams = this.useBlockParams } else d.index = h.index, d.name = "program" + h.index, this.useDepths = this.useDepths || h.useDepths, this.useBlockParams = this.useBlockParams || h.useBlockParams } }, matchExistingProgram: function (a) { for (var b = 0, c = this.context.environments.length; b < c; b++) { var d = this.context.environments[b]; if (d && d.equals(a)) return d } }, programExpression: function (a) { var b = this.environment.children[a], c = [b.index, "data", b.blockParams]; return (this.useBlockParams || this.useDepths) && c.push("blockParams"), this.useDepths && c.push("depths"), "container.program(" + c.join(", ") + ")" }, useRegister: function (a) { this.registers[a] || (this.registers[a] = !0, this.registers.list.push(a)) }, push: function (a) { return a instanceof d || (a = this.source.wrap(a)), this.inlineStack.push(a), a }, pushStackLiteral: function (a) { this.push(new d(a)) }, pushSource: function (a) { this.pendingContent && (this.source.push(this.appendToBuffer(this.source.quotedString(this.pendingContent), this.pendingLocation)), this.pendingContent = void 0), a && this.source.push(a) }, replaceStack: function (a) { var b = ["("], c = void 0, e = void 0, f = void 0; if (!this.isInline()) throw new j["default"]("replaceStack on non-inline"); var g = this.popStack(!0); if (g instanceof d) c = [g.value], b = ["(", c], f = !0; else { e = !0; var h = this.incrStack(); b = ["((", this.push(h), " = ", g, ")"], c = this.topStack() } var i = a.call(this, c); f || this.popStack(), e && this.stackSlot-- , this.push(b.concat(i, ")")) }, incrStack: function () { return this.stackSlot++ , this.stackSlot > this.stackVars.length && this.stackVars.push("stack" + this.stackSlot), this.topStackName() }, topStackName: function () { return "stack" + this.stackSlot }, flushInline: function () { var a = this.inlineStack; this.inlineStack = []; for (var b = 0, c = a.length; b < c; b++) { var e = a[b]; if (e instanceof d) this.compileStack.push(e); else { var f = this.incrStack(); this.pushSource([f, " = ", e, ";"]), this.compileStack.push(f) } } }, isInline: function () { return this.inlineStack.length }, popStack: function (a) { var b = this.isInline(), c = (b ? this.inlineStack : this.compileStack).pop(); if (!a && c instanceof d) return c.value; if (!b) { if (!this.stackSlot) throw new j["default"]("Invalid stack pop"); this.stackSlot-- } return c }, topStack: function () { var a = this.isInline() ? this.inlineStack : this.compileStack, b = a[a.length - 1]; return b instanceof d ? b.value : b }, contextName: function (a) { return this.useDepths && a ? "depths[" + a + "]" : "depth" + a }, quotedString: function (a) { return this.source.quotedString(a) }, objectLiteral: function (a) { return this.source.objectLiteral(a) }, aliasable: function (a) { var b = this.aliases[a]; return b ? (b.referenceCount++ , b) : (b = this.aliases[a] = this.source.wrap(a), b.aliasable = !0, b.referenceCount = 1, b) }, setupHelper: function (a, b, c) { var d = [], e = this.setupHelperArgs(b, a, d, c), f = this.nameLookup("helpers", b, "helper"), g = this.aliasable(this.contextName(0) + " != null ? " + this.contextName(0) + " : (container.nullContext || {})"); return { params: d, paramsInit: e, name: f, callParams: [g].concat(d) } }, setupParams: function (a, b, c) { var d = {}, e = [], f = [], g = [], h = !c, i = void 0; h && (c = []), d.name = this.quotedString(a), d.hash = this.popStack(), this.trackIds && (d.hashIds = this.popStack()), this.stringParams && (d.hashTypes = this.popStack(), d.hashContexts = this.popStack()); var j = this.popStack(), k = this.popStack(); (k || j) && (d.fn = k || "container.noop", d.inverse = j || "container.noop"); for (var l = b; l--;)i = this.popStack(), c[l] = i, this.trackIds && (g[l] = this.popStack()), this.stringParams && (f[l] = this.popStack(), e[l] = this.popStack()); return h && (d.args = this.source.generateArray(c)), this.trackIds && (d.ids = this.source.generateArray(g)), this.stringParams && (d.types = this.source.generateArray(f), d.contexts = this.source.generateArray(e)), this.options.data && (d.data = "data"), this.useBlockParams && (d.blockParams = "blockParams"), d }, setupHelperArgs: function (a, b, c, d) { var e = this.setupParams(a, b, c); return e = this.objectLiteral(e), d ? (this.useRegister("options"), c.push("options"), ["options=", e]) : c ? (c.push(e), "") : e }
                    }, function () { for (var a = "break else new var case finally return void catch for switch while continue function this with default if throw delete in try do instanceof typeof abstract enum int short boolean export interface static byte extends long super char final native synchronized class float package throws const goto private transient debugger implements protected volatile double import public let yield await null true false".split(" "), b = e.RESERVED_WORDS = {}, c = 0, d = a.length; c < d; c++)b[a[c]] = !0 }(), e.isValidJavaScriptVariableName = function (a) { return !e.RESERVED_WORDS[a] && /^[a-zA-Z_$][0-9a-zA-Z_$]*$/.test(a) }, b["default"] = e, a.exports = b["default"]
                }, function (a, b, c) { "use strict"; function d(a, b, c) { if (f.isArray(a)) { for (var d = [], e = 0, g = a.length; e < g; e++)d.push(b.wrap(a[e], c)); return d } return "boolean" == typeof a || "number" == typeof a ? a + "" : a } function e(a) { this.srcFile = a, this.source = [] } b.__esModule = !0; var f = c(5), g = void 0; try { } catch (h) { } g || (g = function (a, b, c, d) { this.src = "", d && this.add(d) }, g.prototype = { add: function (a) { f.isArray(a) && (a = a.join("")), this.src += a }, prepend: function (a) { f.isArray(a) && (a = a.join("")), this.src = a + this.src }, toStringWithSourceMap: function () { return { code: this.toString() } }, toString: function () { return this.src } }), e.prototype = { isEmpty: function () { return !this.source.length }, prepend: function (a, b) { this.source.unshift(this.wrap(a, b)) }, push: function (a, b) { this.source.push(this.wrap(a, b)) }, merge: function () { var a = this.empty(); return this.each(function (b) { a.add(["  ", b, "\n"]) }), a }, each: function (a) { for (var b = 0, c = this.source.length; b < c; b++)a(this.source[b]) }, empty: function () { var a = this.currentLocation || { start: {} }; return new g(a.start.line, a.start.column, this.srcFile) }, wrap: function (a) { var b = arguments.length <= 1 || void 0 === arguments[1] ? this.currentLocation || { start: {} } : arguments[1]; return a instanceof g ? a : (a = d(a, this, b), new g(b.start.line, b.start.column, this.srcFile, a)) }, functionCall: function (a, b, c) { return c = this.generateList(c), this.wrap([a, b ? "." + b + "(" : "(", c, ")"]) }, quotedString: function (a) { return '"' + (a + "").replace(/\\/g, "\\\\").replace(/"/g, '\\"').replace(/\n/g, "\\n").replace(/\r/g, "\\r").replace(/\u2028/g, "\\u2028").replace(/\u2029/g, "\\u2029") + '"' }, objectLiteral: function (a) { var b = []; for (var c in a) if (a.hasOwnProperty(c)) { var e = d(a[c], this); "undefined" !== e && b.push([this.quotedString(c), ":", e]) } var f = this.generateList(b); return f.prepend("{"), f.add("}"), f }, generateList: function (a) { for (var b = this.empty(), c = 0, e = a.length; c < e; c++)c && b.add(","), b.add(d(a[c], this)); return b }, generateArray: function (a) { var b = this.generateList(a); return b.prepend("["), b.add("]"), b } }, b["default"] = e, a.exports = b["default"] }])
            });// Replaced by handlebars.js at build time
        };
        templateEngine(_tplExp);

        var __handlebars = _tplExp.Handlebars;

        //----------------------------------------------------------------------------------------

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // DEFINE, ONCE ONLY, ALL THE FUNCTIONALITY NEEDED TO:
        //
        // 1. INITIALLY GENERATE TEMPLATES (storing them as functions on the __templateCache object)
        // 2. REGISTER TEMPLATE 'PARTIALS' (as read by Handlebars in the template generation process)
        // 3. REGISTER TEMPLATE 'HELPER' FNS (as USED by Handlebars in the template generation process)
        //-------
        // 4. RENDER A SPECIFIC TEMPLATE WHEN REQUESTED
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


        ////////////////////////// 1, 2, & 3: GENERATION & REGISTRATION FNY ////////////////////////////////////////////////////

        var __templateCache;

        // Container for any public members we need to expose
        var that = {};

        var init = function (pSetupJsonObj, pConfigObjPaymentMethods) {

            createTemplates();
            registerPartials();
            registerHelpers(pSetupJsonObj, pConfigObjPaymentMethods);
        };

        // EXAMINE DOM FOR <script> ELEMENTS WITH A data-template ATTRIBUTE
        // STORE AND COMPILE THE TEMPLATES HELD WITHIN THE ELEMENT'S innerHTML
        var createTemplates = function () {

            __templateCache = { keys: [], raw: {} };

            var nodes, node, i, len, templateId;

            // First look for template nodes attached to the head
            var head = DOM._selectOne(document, 'head');
            nodes = DOM._select(head, 'script');

            for (i = 0, len = nodes.length; i < len; i++) {

                node = nodes[i];

                if (DOM._getAttribute(node, 'data-template')) {

                    templateId = DOM._getAttribute(node, 'data-template');

                    if (window._b$dl && typeof _a$chckt !== 'undefined' && _a$chckt.__listTemplates && window.console && window.console.log) {
                        window.console.log('### checkoutSDK::createTemplates:: Adding a new template from head', templateId);
                    }

                    // Compile it, store it in the __templateCache under it's data-template value & also push that value into the 'keys' store
                    // & store the 'raw' html
                    __templateCache[templateId] = __compile(node.innerHTML);
                    __templateCache.keys.push(templateId);
                    __templateCache.raw[templateId] = node.innerHTML;
                }
            }

            // Now look for template nodes in the body (more specifically we expect to find them in our dedicated content div)
            nodes = DOM._select(document.body, 'script');

            for (i = 0, len = nodes.length; i < len; i++) {

                node = nodes[i];

                if (DOM._getAttribute(node, 'data-template')) {

                    templateId = DOM._getAttribute(node, 'data-template');

                    if (window._b$dl && typeof _a$chckt !== 'undefined' && _a$chckt.__listTemplates && window.console && window.console.log) {
                        window.console.log('### checkoutSDK::createTemplates:: Adding a new template', templateId);
                    }

                    // If template doesn't already exist i.e. was read from <head>
                    if (!__templateCache[templateId]) {

                        // Compile and store
                        __templateCache[templateId] = __compile(node.innerHTML);
                        __templateCache.keys.push(templateId);
                        __templateCache.raw[templateId] = node.innerHTML;

                    } else {

                        if (window._b$dl && typeof _a$chckt !== 'undefined' && _a$chckt.__listTemplates && window.console && window.console.log) {
                            window.console.log('### utils::rendering:: template already exists in head:', templateId);
                        }
                    }
                }
            }
        };// createTemplates

        // Register partials - required for Handlebars
        var registerPartials = function () {

            //            var templatePartials = ['selectPaymentMethod', 'paymentMethodDetails', 'cardFieldType', 'textFieldType', 'addressFieldType', 'choiceFieldType', 'issuerListFieldType', 'walletTokenFieldType'];//'checkboxFieldType'
            // NOTE: adding a new (partial) template means updating themes.config.vm to include it AND redeploying checkout
            var templatePartials = ['selectPaymentMethod', 'paymentMethods', 'giropay'];

            var i, key, len = __templateCache.keys.length;
            for (i = 0; i < len; i++) {

                key = __templateCache.keys[i];

                for (var j = 0, tLen = templatePartials.length; j < tLen; j++) {

                    if (key === templatePartials[j]) {

                        var partialScript = __templateCache.raw[key];

                        __handlebars.registerPartial(templatePartials[j], partialScript);
                    }
                }
            }
        };//registerPartials

        var registerHelpers = function (pSetupJsonObj, pConfigObjPaymentMethods) {

            // Evaluates the objects in the inputDetails array to pass on the relevant compiled template
            __handlebars.registerHelper('inputDetailHelper', function (context, options) {

                // 'context' is the inputDetails array (so context[i], below, is each object in the inputDetails array
                // 'this' is each paymentMethods object containing the inputDetails array


                // No inputDetails array on the paymentMethod object
                if (!context) {

                    // return the paymentMethods object for re-evaluation
                    return options.inverse(this);

                    // Continue building rest of template without the inputDetails content
                    //return options.fn();
                }

                var txVariant = this.type;

                var ret = '';

                // KNOWN FIELD TYPES: 'Card', 'IssuerList', 'Text', 'Checkbox', 'Address', 'Choice', 'WalletToken', 'Cvc'??, 'PassThrough'
                var fieldTypes = {
                    cardToken: 'card',
                    'boolean': 'checkbox',
                    'text': 'text',
                    select: 'issuerList'
                };


                for (var i = 0, len = context.length; i < len; i++) {

                    // 'context[i]' is each object in the inputDetails array containing 'key', 'type' & 'optional' properties

                    var type = context[i].type;
                    var key = context[i].key;
                    var shopperLocale = pSetupJsonObj.payment.shopperLocale;

                    context[i].h_shopperLocale = shopperLocale;// used for PMs with an iban text field


                    if (context[i].type === 'text' && context[i].key === 'holderName' && !context[i].placeholder) {

                        context[i].placeholder = chckt.getTranslation('creditCard.holderName.placeholder', chckt.languageKey);
                    }


                    // Detect a recurringDetails card
                    var tmplSuffix = '';
                    if (context[i].recurring) {

                        var month = context[i].recurring.expiryMonth;
                        if (month.length === 1) {
                            context[i].recurring.expiryMonth = '0' + month;
                        }

                        tmplSuffix = 'Recurring';
                    }

                    var templateFN, templateKey;

                    if (type === 'cardToken' && tmplSuffix === '') {// Remove this check on tmplSuffix to load just a 'cardFieldCvc' template in the case of a recurring card

                        // Inspect the key for the exact type of template required
                        templateKey = key.substr(key.indexOf('encrypted') + 9);// i.e. 'CardNumber', 'SecurityCode', 'ExpiryMonth', 'ExpiryYear'


                        //////// ROUTINE TO COMBINE DATE FIELDS, IF REQUIRED ////////
                        if (templateKey === 'ExpiryMonth' || templateKey === 'ExpiryYear') {

                            // Default action, without this routine, would result in separate date fields.
                            // However we invert this: unless a separateDateInputs property is passed, set to true
                            // we will always end up with combined inputs
                            var combineDateFields = false;

                            var hasNamedCardPM = pConfigObjPaymentMethods.hasOwnProperty(txVariant);
                            var hasDefaultCardPM = pConfigObjPaymentMethods.hasOwnProperty('card');

                            // Check for presence of named PM in the configPMs object
                            if (hasNamedCardPM) {

                                if (!pConfigObjPaymentMethods[txVariant].separateDateInputs) {
                                    combineDateFields = true;
                                }
                            } else {

                                // Check for default card entry
                                if (hasDefaultCardPM) {

                                    if (!pConfigObjPaymentMethods.card.separateDateInputs) {
                                        combineDateFields = true;
                                    }
                                }
                            }

                            if (combineDateFields) {

                                // We put 'month' & 'year' keys together in a 'date' template & also force the key to 'encryptedExpiryDate'
                                // This latter makes for a cleaner, more easily explained implementation
                                if (templateKey === 'ExpiryMonth') {
                                    templateKey = 'ExpiryDate';
                                    context[i].key = 'encryptedExpiryDate';
                                }

                                if (templateKey === 'ExpiryYear') {
                                    continue;
                                }
                            }
                        }//----------------


                        // Find compiled template (exists as a Handlebars function)
                        templateFN = __templateCache[fieldTypes[type] + 'Field' + templateKey];// e.g. 'cardFieldNumber'


                    } else {
                        // Recurring
                        templateFN = __templateCache[fieldTypes[type] + 'FieldType' + tmplSuffix];
                    }


                    if (templateFN) {

                        // Execute template function passing in data context
                        ret += new __handlebars.SafeString(templateFN(context[i], options));

                    } else {

                        ret += '<p style="font-size:11px; color:orange;">Template: "' + fieldTypes[type] + 'FieldType" not found</p>';
                    }
                }

                return ret;
            });

            // Examine a paymentMethod's inputDetails array. If all items are optional the PM doesn't need the 'data-additional-required' attribute flag
            __handlebars.registerHelper('inputDetailsMandatoryHelper', function (context, options) {

                var assessInputDetails = function () {

                    var optionalItems = Shims.filter(context, function (pItem) {
                        return pItem.hasOwnProperty('optional');
                    });

                    return (optionalItems.length === context.length) ? '' : 'data-additional-required';
                };

                // No context means no inputDetails array. If there is an inputDetails array assess it!
                return (context) ? assessInputDetails() : '';
            });

            // Check if a payment method is recurring or not, add data based on it
            __handlebars.registerHelper('checkForRecurringDetails', function (context) {

                var assessRecurringDetails = function () {

                    var assessRecurringDetails = Shims.filter(context, function (pItem) {
                        return pItem.hasOwnProperty('recurring');
                    });

                    return (assessRecurringDetails.length === context.length) ? '' : 'recurring';
                };

                // No context means no inputDetails array. If there is an inputDetails array assess it!
                return (context) ? assessRecurringDetails() : '';
            });

            // Decide whether to show the regular 'pay' button or whether the process has an extra 'review order' step
            __handlebars.registerHelper('buttonActionHelper', function (context, options) {

                var templateStr = 'payButton';

                // NOTE: From vn 1.2.1 we only return the payButton template
                // Doing that here, instead of using a partial in the template, means the template
                // stays backwards compatible
                //            if(chckt.hooks.hasReviewStage && typeof chckt.hooks.hasReviewStage === 'function'){
                //                templateStr = 'reviewButton';
                //            }

                var templateFN = __templateCache[templateStr];
                return new __handlebars.SafeString(templateFN(context.data.root));
            });

            // Strips the recurring card indicator: _r@ from the PMs 'type' property so the generic brand image can be loaded
            __handlebars.registerHelper('pmImageHelper', function (type) {
                return type.replace(/_r@\d+/, '');
            });


            //If you want to overwrite the language for a string pass the option locale="nl_NL" to the translatehelper
            __handlebars.registerHelper('translateHelper', function (key, options) {

                // Exception for recurring cards and non-consolidated cards i.e. both of which are tied to a specific card brand
                // If the underlying data object has a securityCode property set - use that instead of a translation.
                // Example values for securityCode: 'CVV', 'CVC', 'CID'
                if (key === 'creditCard.cvcField.title') {

                    // TODO - check this nesting is secure & that we don't need to check for the existence of options OR options.data OR options.data.root
                    if (options.data.root.securityCode) {
                        return options.data.root.securityCode;
                    }
                }

                var translatedString,
                    locale,
                    optionHash = options.hash;

                if (optionHash.locale) {
                    locale = optionHash.locale;
                }

                if (key && locale !== "") {

                    translatedString = chckt.getTranslation(key, locale);

                } else if (key) {

                    translatedString = chckt.getTranslation(key);

                } else {
                    translatedString = false;
                }

                // If translated key is found return it
                if (translatedString) {
                    return translatedString;
                }
            });

            // Checks if lvalue equals rvalue
            __handlebars.registerHelper('equalsHelper', function (lvalue, rvalue, options) {
                if (arguments.length < 3) {
                    // NOTE: options parameter gets added automatically - calls from the template to this helper
                    // needs to specify 2 parameters: the 2 values to compare
                    throw new Error("Handlebars Helper equal needs 2 parameters");
                }
                if (lvalue !== rvalue) {
                    return options.inverse(this);
                } else {
                    return options.fn(this);
                }
            });

            __handlebars.registerHelper('gteHelper', function (lvalue, rvalue, options) {
                if (arguments.length < 3) {
                    throw new Error("Handlebars Helper greater-than-or-equals needs 2 parameters");
                }
                if (Number(lvalue) >= Number(rvalue)) {
                    return options.fn(this);
                } else {
                    return options.inverse(this);
                }
            });

            // Used in textFieldType template to differentiate different input field 'types' - Possibly not needed, now we have the returnTextFieldTypeFny helper??
            __handlebars.registerHelper('returnFieldTypeHelper', function (fieldType) {

                var fieldTypeArray = fieldType.split(".");
                var jsFieldType = 'js-chckt';

                for (var i = 0; i < fieldTypeArray.length; i++) {
                    jsFieldType = jsFieldType + '-' + fieldTypeArray[i];
                }

                return jsFieldType.toLowerCase();
            });

            // Used in textFieldType template to differentiate different keyup functionality based on input field 'types'
            __handlebars.registerHelper('returnTextFieldTypeFnyHelper', function (pFieldType) {

                var fieldType = pFieldType.toLowerCase();

                var pm = fieldType.substring(0, fieldType.indexOf('.'));

                if (fieldType.indexOf('holdername') > -1) {
                    return "chckt.cardHolderNameKeyUp(event, this, '" + 'card' + "');";
                }

                if (fieldType.indexOf('iban') > -1) {
                    return "chckt.isValidIBAN(event, this, '" + pm + "');";
                }

                if (fieldType.indexOf('name') > -1) {
                    return "chckt.isValidName(event, this, '" + pm + "');";
                }

                return "chckt.noop()";
            });

            // keypress
            __handlebars.registerHelper('returnTextFieldKeyPressFnyHelper', function (pFieldType) {

                var fieldType = pFieldType.toLowerCase();

                if (fieldType.indexOf('holdername') > -1) {
                    return "chckt.cardHolderNameKeyPress(event, this, '" + 'card' + "');";
                }
            });

            // input
            __handlebars.registerHelper('returnTextFieldInputFnyHelper', function (pFieldType) {

                var fieldType = pFieldType.toLowerCase();

                if (fieldType.indexOf('holdername') > -1) {
                    return "chckt.cardHolderNameInput(event, this, '" + 'card' + "');";
                }
            });

            // Call amountUtil and replace currencySymbol
            __handlebars.registerHelper('getCurrencySymbolHelper', function (currencyCode) {

                if (currencyCode) {

                    return chckt.getCurrencySymbol(currencyCode);

                }
            });

            // Get divider amount
            var __getDivider = function __getDivider(arrays, currencyCode) {
                for (var i = 0; i < arrays.length; i++) {

                    var array = arrays[i];

                    for (var e = 0; e < array.length; e++) {
                        if (currencyCode === array[e]) {
                            return array.divider;
                        }
                    }
                }

                return 100;
            };

            __handlebars.registerHelper('getLocalisedAmountHelper', function (amount, locale, currencyCode) {

                var ZERO_DECIMAL_COUNTRIES = ["IDR", "JPY", "KRW", "VND", "BYR", "CVE", "DJF", "GHC", "GNF", "KMF", "PYG", "RWF", "UGX", "VUV", "XAF", "XOF", "XPF"];

                var ONE_DECIMAL_COUNTRIES = ["MRO"];

                var THREE_DECIMAL_COUNTRIES = ["BHD", "JOD", "KWD", "OMR", "LYD", "TND"];

                ZERO_DECIMAL_COUNTRIES.divider = 0;
                ONE_DECIMAL_COUNTRIES.divider = 10;
                THREE_DECIMAL_COUNTRIES.divider = 1000;

                var CURRENCY_DECIMAL_ARRAYS = [ZERO_DECIMAL_COUNTRIES, ONE_DECIMAL_COUNTRIES, THREE_DECIMAL_COUNTRIES];

                if (currencyCode && locale && amount) {

                    var divider = __getDivider(CURRENCY_DECIMAL_ARRAYS, currencyCode);

                    if (divider !== 0) {
                        amount = amount / divider;
                    }

                    if (__toLocaleStringSupportsOptions()) {
                        locale = locale.replace('_', '-');

                        var localisedAmount = amount.toLocaleString(locale, { style: 'currency', currency: currencyCode, currencyDisplay: 'symbol' });

                        if (localisedAmount) {
                            return localisedAmount;
                        }
                        return amount;
                    }

                    var ieLocalisedAmount = amount.toLocaleString(),
                        currencySymbol = chckt.getCurrencySymbol(currencyCode);

                    if (ieLocalisedAmount && currencySymbol) {
                        return "" + currencySymbol + ieLocalisedAmount;
                    }
                } else if (amount) {
                    return amount;
                } else {
                    return false;
                }
            });
        };// registerHelpers

        var __compile = function (pTemplate) {

            var opts = { assumeObjects: true, strict: false };
            return __handlebars.compile(pTemplate, opts);
        };

        var __toLocaleStringSupportsOptions = function () {
            return !!((typeof Intl).toLowerCase() === 'object' && Intl && (typeof Intl.NumberFormat).toLowerCase() === 'function');
        };
        //----------------------------------------------------------------------------------------------


        ////////////////////////// 4. RENDERING FNY ////////////////////////////////////////////////////
        var render = function (pTemplate, pDest, pData, pDoAppend) {

            if (!__templateCache.hasOwnProperty(pTemplate)) {

                log("Template '" + pTemplate + "' was not defined. Hint: try one of [" + __templateCache.keys.join(", ") + "]");
                if (hook({}.undef, 'onTemplateError', pDest)) {
                    pDest.innerHTML = 'Template error';
                }
                return;
            }

            // Pass in data (or as Handlebars call it, 'context') to the compiled template
            var tmplt = __templateCache[pTemplate](pData);

            var i, len, div;
            if (!pDoAppend) {

                pDest.innerHTML = tmplt;

            } else {

                div = document.createElement('div');
                div.innerHTML = tmplt;
                var elements = div.childNodes;

                for (i = 0, len = elements.length; i < len; i++) {

                    if (elements[i].nodeName !== '#text') {

                        pDest.appendChild(elements[i]);
                        break;
                    }
                }
            }
        };
        //----------------------------------------------------------------------------------------------


        ///////////////////// EXPOSE FUNCTIOALITY ON PUBLIC INTERFACE /////////////////////////////////
        that.createTemplates = function (pSetupJsonObj, pConfigObjPaymentMethods) {

            init(pSetupJsonObj, pConfigObjPaymentMethods);
        };

        that.renderTemplate = function (pTemplate, pDest, pData, pDoAppend) {
            render(pTemplate, pDest, pData, pDoAppend);
        };

        return that;
    });

    /* global __define */
    __define('df', [], function () {

        "use strict";

        var df;

        // In IE8 typeof document.getElementsByTagName = "object"
        //    if (document && window && typeof document.getElementsByTagName === 'function') {
        if (document && window && document.getElementsByTagName) {

            /* adyen-hpp.df.js */
            var _ = _ ? _ : {};
            _.X = function (d, h, g, f) {
                f = new (window.ActiveXObject ? ActiveXObject : XMLHttpRequest)("Microsoft.XMLHTTP");
                f.open(g ? "POST" : "GET", d, 1);
                g ? f.setRequestHeader("Content-type", "application/x-www-form-urlencoded") : 0;
                f.onreadystatechange = function () {
                    f.readyState > 3 && h ? h(f.responseText, f) : 0
                };
                f.send(g)
            };
            _.E = function (g, f, h, e) {
                if (g.attachEvent ? (e ? g.detachEvent("on" + f, g[f + h]) : !0) : (e ? g.removeEventListener(f, h, !1) : g.addEventListener(f, h, !1))) {
                    g["e" + f + h] = h;
                    g[f + h] = function () {
                        g["e" + f + h](window.event)
                    };
                    g.attachEvent("on" + f, g[f + h])
                }
            };
            _.G = function (b) {
                return b.style ? b : document.getElementById(b)
            };
            _.A = function (g, h, i, c, j) {
                if (c === undefined) {
                    var c = new Object();
                    c.value = 0
                }
                c.value ? 0 : c.value = 0;
                return j.value = setInterval(function () {
                    i(c.value / g);
                    ++c.value > g ? clearInterval(j.value) : 0
                }, h)
            };
            _.F = function (g, d, h, f) {
                g = g == "in";
                _.A(h ? h : 15, f ? f : 50, function (a) {
                    a = (g ? 0 : 1) + (g ? 1 : -1) * a;
                    d.style.opacity = a;
                    d.style.filter = "alpha(opacity=" + 100 * a + ")"
                })
            };
            _.S = function (h, o, i, p, f, d, c) {
                h = h == "in";
                _.A(i ? i : 15, p ? p : 50, function (a) {
                    a = (h ? 0 : 1) + (h ? 1 : -1) * a;
                    o.style.width = parseInt(a * f) + "px"
                }, c, d)
            };
            _.Q = function (k) {
                var i = new Object();
                var m = new Array();
                for (var f = 0; f < k.elements.length; f++) {
                    try {
                        l = k.elements[f];
                        n = l.name;
                        if (n == "") {
                            continue
                        }
                        switch (l.type.split("-")[0]) {
                            case "select":
                                for (var e = 0; e < l.options.length; e++) {
                                    if (l.options[e].selected) {
                                        if (typeof (i[n]) == "undefined") {
                                            i[n] = new Array()
                                        }
                                        i[n][i[n].length] = encodeURIComponent(l.options[e].value)
                                    }
                                }
                                break;
                            case "radio":
                                if (l.checked) {
                                    if (typeof (i[n]) == "undefined") {
                                        i[n] = new Array()
                                    }
                                    i[n][i[n].length] = encodeURIComponent(l.value)
                                }
                                break;
                            case "checkbox":
                                if (l.checked) {
                                    if (typeof (i[n]) == "undefined") {
                                        i[n] = new Array()
                                    }
                                    i[n][i[n].length] = encodeURIComponent(l.value)
                                }
                                break;
                            case "submit":
                                break;
                            default:
                                if (typeof (i[n]) == "undefined") {
                                    i[n] = new Array()
                                }
                                i[n][i[n].length] = encodeURIComponent(l.value);
                                break
                        }
                    } catch (j) {
                    }
                }
                for (x in i) {
                    m[m.length] = x + "=" + i[x].join(",")
                }
                return m.join("&")
            };
            _.R = function (b) {
                ("\v" == "v" || document.documentElement.style.scrollbar3dLightColor != undefined) ? setTimeout(b, 0) : _.E(document, "DOMContentLoaded", b)
            };
            var dfGetPlug = function () {
                var u = "";
                var q = 0;
                try {
                    if (navigator.plugins) {
                        var i = navigator.plugins;
                        var e = [];
                        for (var t = 0; t < i.length; t++) {
                            e[t] = i[t].name + "; ";
                            e[t] += i[t].description + "; ";
                            e[t] += i[t].filename + ";";
                            for (var v = 0; v < i[t].length; v++) {
                                e[t] += " (" + i[t][v].description + "; " + i[t][v].type + "; " + i[t][v].suffixes + ")"
                            }
                            e[t] += ". "
                        }
                        q += i.length;
                        e.sort();
                        for (t = 0; t < i.length; t++) {
                            u += "Plugin " + t + ": " + e[t]
                        }
                    }
                    if (u === "") {
                        var w = [];
                        w[0] = "QuickTime";
                        w[1] = "Shockwave";
                        w[2] = "Flash";
                        w[3] = "WindowsMediaplayer";
                        w[4] = "Silverlight";
                        w[5] = "RealPlayer";
                        var r;
                        for (var y = 0; y < w.length; y++) {
                            r = PluginDetect.getVersion(w[y]);
                            if (r) {
                                u += w[y] + " " + r + "; ";
                                q++
                            }
                        }
                        u += dfGetIEAV();
                        q++
                    }
                } catch (s) {
                }
                var p = { nr: q, obj: u };
                return p
            }

            var dfGetIEAV = function () {
                try {
                    if (window.ActiveXObject) {
                        for (var x = 2; x < 10; x++) {
                            try {
                                oAcro = eval("new ActiveXObject('PDF.PdfCtrl." + x + "');");
                                if (oAcro) {
                                    return "Adobe Acrobat version" + x + ".?"
                                }
                            } catch (ex) {
                            }
                        }
                        try {
                            oAcro4 = new ActiveXObject("PDF.PdfCtrl.1");
                            if (oAcro4) {
                                return "Adobe Acrobat version 4.?"
                            }
                        } catch (ex) {
                        }
                        try {
                            oAcro7 = new ActiveXObject("AcroPDF.PDF.1");
                            if (oAcro7) {
                                return "Adobe Acrobat version 7.?"
                            }
                        } catch (ex) {
                        }
                        return ""
                    }
                } catch (e) {
                }
                return ""
            }

            var dfGetFonts = function () {
                var j = "";
                try {
                    try {
                        var i = document.getElementById("df_jfh");
                        if (i && i !== null) {
                            var p = i.getFontList();
                            for (var k = 0; k < p.length; k++) {
                                j = j + p[k] + ", "
                            }
                            j += " (Java)"
                        }
                    } catch (e) {
                    }
                    if (j === "") {
                        j = "No Flash or Java"
                    }
                } catch (m) {
                }
                var o = { nr: j.split(",").length, obj: j };
                return o
            }

            var dfInitDS = function () {
                try {
                    localStorage.dfValue = "value"
                } catch (b) {
                }
                try {
                    sessionStorage.dfValue = "value"
                } catch (b) {
                }
            }

            var dfGetDS = function () {
                var d = "";
                try {
                    if (localStorage.dfValue === "value") {
                        d += "DOM-LS: Yes"
                    } else {
                        d += "DOM-LS: No"
                    }
                } catch (c) {
                    d += "DOM-LS: No"
                }
                try {
                    if (sessionStorage.dfValue === "value") {
                        d += ", DOM-SS: Yes"
                    } else {
                        d += ", DOM-SS: No"
                    }
                } catch (c) {
                    d += ", DOM-SS: No"
                }
                return d
            }

            var dfGetIEUD = function () {
                try {
                    oPersistDiv.setAttribute("cache", "value");
                    oPersistDiv.save("oXMLStore");
                    oPersistDiv.setAttribute("cache", "new-value");
                    oPersistDiv.load("oXMLStore");
                    if ((oPersistDiv.getAttribute("cache")) == "value") {
                        return ", IE-UD: Yes"
                    } else {
                        return ", IE-UD: No"
                    }
                } catch (b) {
                    return ", IE-UD: No"
                }
            }

            var getWebglFp = function () {
                var z = document.createElement("canvas");
                var t = null;
                try {
                    t = z.getContext("webgl") || z.getContext("experimental-webgl")
                } catch (q) {
                    return padString("", 10)
                }
                if (t === undefined || t === null) {
                    return padString("", 10)
                }
                var o = [];
                var r = "attribute vec2 attrVert;varying vec2 varyTexCoord;uniform vec2 unifOffset;void main(){varyTexCoord=attrVert+unifOffset;gl_Position=vec4(attrVert,0,1);}";
                var w = "precision mediump float;varying vec2 varyTexCoord;void main() {gl_FragColor=vec4(varyTexCoord*0.55,0,1);}";
                var v = -0.7;
                var y = 0.7;
                var u = 0.2;
                var A = t.canvas.width / t.canvas.height;
                try {
                    s(t, v, y, u, A);
                    s(t, v + u, y - u * A, u, A);
                    s(t, v + u, y - 2 * u * A, u, A);
                    s(t, v, y - 2 * u * A, u, A);
                    s(t, v - u, y - 2 * u * A, u, A)
                } catch (q) {
                }
                if (t.canvas !== null) {
                    o.push(t.canvas.toDataURL() + "§")
                }
                try {
                    o.push(t.getParameter(t.RED_BITS));
                    o.push(t.getParameter(t.GREEN_BITS));
                    o.push(t.getParameter(t.BLUE_BITS));
                    o.push(t.getParameter(t.DEPTH_BITS));
                    o.push(t.getParameter(t.ALPHA_BITS));
                    o.push((t.getContextAttributes().antialias ? "1" : "0"));
                    o.push(p(t.getParameter(t.ALIASED_LINE_WIDTH_RANGE)));
                    o.push(p(t.getParameter(t.ALIASED_POINT_SIZE_RANGE)));
                    o.push(p(t.getParameter(t.MAX_VIEWPORT_DIMS)));
                    o.push(t.getParameter(t.MAX_COMBINED_TEXTURE_IMAGE_UNITS));
                    o.push(t.getParameter(t.MAX_CUBE_MAP_TEXTURE_SIZE));
                    o.push(t.getParameter(t.MAX_FRAGMENT_UNIFORM_VECTORS));
                    o.push(t.getParameter(t.MAX_RENDERBUFFER_SIZE));
                    o.push(t.getParameter(t.MAX_TEXTURE_IMAGE_UNITS));
                    o.push(t.getParameter(t.MAX_TEXTURE_SIZE));
                    o.push(t.getParameter(t.MAX_VARYING_VECTORS));
                    o.push(t.getParameter(t.MAX_VERTEX_ATTRIBS));
                    o.push(t.getParameter(t.MAX_VERTEX_TEXTURE_IMAGE_UNITS));
                    o.push(t.getParameter(t.MAX_VERTEX_UNIFORM_VECTORS));
                    o.push(t.getParameter(t.RENDERER));
                    o.push(t.getParameter(t.SHADING_LANGUAGE_VERSION));
                    o.push(t.getParameter(t.STENCIL_BITS));
                    o.push(t.getParameter(t.VENDOR));
                    o.push(t.getParameter(t.VERSION));
                    o.push(t.getSupportedExtensions().join(""))
                } catch (q) {
                    return padString("", 10)
                }
                return o.join("");
                function s(i, b, c, a, d) {
                    var h = new Float32Array([b, c, b, c - a * d, b + a, c - a * d, b, c, b + a, c, b + a, c - a * d]);
                    var f = i.createBuffer();
                    i.bindBuffer(i.ARRAY_BUFFER, f);
                    i.bufferData(i.ARRAY_BUFFER, h, i.STATIC_DRAW);
                    f.itemSize = 2;
                    f.numItems = h.length / f.itemSize;
                    var j = i.createProgram();
                    var g = i.createShader(i.VERTEX_SHADER);
                    var e = i.createShader(i.FRAGMENT_SHADER);
                    i.shaderSource(g, r);
                    i.shaderSource(e, w);
                    i.compileShader(g);
                    i.compileShader(e);
                    i.attachShader(j, g);
                    i.attachShader(j, e);
                    i.linkProgram(j);
                    i.useProgram(j);
                    j.vertexPosAttrib = i.getAttribLocation(j, "attrVert");
                    j.offsetUniform = i.getUniformLocation(j, "unifOffset");
                    i.enableVertexAttribArray(j.vertexPosArray);
                    i.vertexAttribPointer(j.vertexPosAttrib, f.itemSize, i.FLOAT, !1, 0, 0);
                    i.uniform2f(j.offsetUniform, 1, 1);
                    i.drawArrays(i.TRIANGLE_STRIP, 0, f.numItems)
                }

                function p(a) {
                    t.clearColor(0, 0.5, 0, 1);
                    t.enable(t.DEPTH_TEST);
                    t.depthFunc(t.LEQUAL);
                    t.clear(t.COLOR_BUFFER_BIT | t.DEPTH_BUFFER_BIT);
                    return a[0] + a[1]
                }
            }

            var getJsFonts = function () {
                var E = function () {
                    return (new Date()).getTime()
                };
                var D = E() + 3000;
                try {
                    var u = ["monospace", "sans-serif", "serif"];
                    var B = "abcdefghijklmnopqrstuvwxyz";
                    var s = "80px";
                    var C = document.body || document.getElementsByTagName("body")[0];
                    var v = document.createElement("span");
                    v.style.fontSize = s;
                    v.innerHTML = B;
                    var t = {};
                    var I = {};
                    var z = 0;
                    for (z = 0; z < u.length; z++) {
                        v.style.fontFamily = u[z];
                        C.appendChild(v);
                        t[u[z]] = v.offsetWidth;
                        I[u[z]] = v.offsetHeight;
                        C.removeChild(v)
                    }
                    var y = ["Abril Fatface", "Adobe Caslon", "Adobe Garamond", "ADOBE GARAMOND PRO", "Affair", "Ailerons", "Alegreya", "Aller", "Altus", "Amatic", "Ambassador", "American Typewriter", "American Typewriter Condensed", "Americane", "Amsi Pro", "Andale Mono", "Anivers", "Anonymous Pro", "Arca Majora", "Archivo Narrow", "Arial", "Arial Black", "Arial Hebrew", "Arial MT", "Arial Narrow", "Arial Rounded MT Bold", "Arial Unicode MS", "Arimo", "Arvo", "Asfalto", "Asia", "Audimat", "AvantGarde Bk BT", "AvantGarde Md BT", "Bank Gothic", "BankGothic Md BT", "Barkentina", "Baskerville", "Baskerville Old Face", "Bassanova", "Batang", "BatangChe", "Bauhaus 93", "Beauchef", "Bebas Neue", "Bellaboo", "Berlin Sans FB", "Berlin Sans FB Demi", "Betm", "Bitter", "Blackout", "Blox", "Bodoni 72", "Bodoni 72 Oldstyle", "Bodoni 72 Smallcaps", "Bodoni MT", "Bodoni MT Black", "Bodoni MT Condensed", "Bodoni MT Poster Compressed", "Bomb", "Book Antiqua", "Bookman Old Style", "Bookshelf Symbol 7", "Bosque", "Bowling Script", "Box", "Brandon Text", "Brandon Text Medium", "Bree Serif", "Bremen Bd BT", "Britannic Bold", "Broadway", "Brooklyn Samuels", "Brotherhood Script", "Bukhari Script", "Burford", "Byker", "Cabin", "Caecilia", "Calibri", "Cambria", "Cambria Math", "Cathedral", "Century", "Century Gothic", "Century Schoolbook", "Cervo", "Chalfont", "Chaucer", "Chivo", "Chunk", "Clarendon", "Clarendon Condensed", "Clavo", "Clavo Regular", "Clear Sans Screen", "Code", "Comic Sans", "Comic Sans MS", "Conifer", "Copperplate", "Copperplate Gothic", "Copperplate Gothic Bold", "Copperplate Gothic Light", "CopperplGoth Bd BT", "Corbel", "Core Sans NR", "Courier", "Courier New", "Curely", "D Sert", "Delicate", "Delicious", "DIN", "Directors Gothic", "Dogtown", "Domine", "Donau", "Dosis", "Droid Sans", "Droid Serif", "Emblema Headline", "Endless Bummer", "English 111 Vivace BT", "Eras Bold ITC", "Eras Demi ITC", "Eras Light ITC", "Eras Medium ITC", "Exo", "Exo 2", "Fabfelt Script", "Fanwood", "Fedra Sans", "Fela", "Felice", "Felice Regular", "Fertigo Pro", "FFF TUSJ", "Fins", "Fjalla One", "Fontin", "Franchise", "Franklin Gothic", "Franklin Gothic Book", "Franklin Gothic Demi", "Franklin Gothic Demi Cond", "Franklin Gothic Heavy", "Franklin Gothic Medium", "Franklin Gothic Medium Cond", "Free Spirit", "FS Clerkenwell", "Futura", "Futura Bk BT", "Futura Lt BT", "Futura Md BT", "Futura ZBlk BT", "FuturaBlack BT", "Galano Classic", "Garamond", "GEOM", "Georgia", "GeoSlab 703 Lt BT", "GeoSlab 703 XBd BT", "Giant", "Gibbs", "Gill Sans", "Gill Sans MT", "Gill Sans MT Condensed", "Gill Sans MT Ext Condensed Bold", "Gill Sans Ultra Bold", "Gill Sans Ultra Bold Condensed", "Glaser Stencil", "Glober", "Gloucester MT Extra Condensed", "Gotham", "GOTHAM", "GOTHAM BOLD", "Goudy Bookletter 1911", "Goudy Old Style", "Gravitas One", "Hamster", "Harman", "Helena", "Helvetica", "Helvetica Neue", "Herald", "Hero", "Hogshead", "Home Brush", "Horizontes Script", "Hoverage", "Humanst 521 Cn BT", "HWT Artz", "Ikaros", "Impact", "Inconsolata", "Into The Light", "Istok Web", "Itoya", "Ivory", "Jack", "Jekyll and Hyde", "Jimmy", "Josefin Slab", "Junction", "Kapra", "Karla", "Karol", "Karol Regular", "Karol Semi Bold Italic", "Kautiva", "Kelso", "Knewave", "Kurversbrug", "Lato", "League Gothic", "League Script Number One", "League Spartan", "Libre Baskerville", "Linden Hill", "Linotte", "Lobster", "Lombok", "Lora", "Louize", "Louize Italic", "Louize Medium", "Lucida Bright", "Lucida Calligraphy", "Lucida Console", "Lucida Fax", "LUCIDA GRANDE", "Lucida Handwriting", "Lucida Sans", "Lucida Sans Typewriter", "Lucida Sans Unicode", "Lulo Clean", "Manifesto", "Maxwell", "Merel", "Merlo", "Merriweather", "Metro Nova", "Metro Nova Light", "Metro Nova Regular", "Microsoft Himalaya", "Microsoft JhengHei", "Microsoft New Tai Lue", "Microsoft PhagsPa", "Microsoft Sans Serif", "Microsoft Tai Le", "Microsoft Uighur", "Microsoft YaHei", "Microsoft Yi Baiti", "Modern Brush", "Modern No. 20", "MONO", "Monthoers", "Montserrat", "Moon", "Mrs Eaves", "MS Gothic", "MS LineDraw", "MS Mincho", "MS Outlook", "MS PGothic", "MS PMincho", "MS Reference Sans Serif", "MS Reference Specialty", "MS Sans Serif", "MS Serif", "MS UI Gothic", "MTT Milano", "Muli", "Museo Slab", "Myriad Pro", "Neo Sans", "Neo-Noire", "Neutron", "News Gothic", "News GothicMT", "NewsGoth BT", "Nickainley Script", "Nobile", "Old Century", "Old English Text MT", "Old Standard TT", "Open Sans", "Orbitron", "Ostrich Sans", "Oswald", "Palatino", "Palatino Linotype", "Papyrus", "Parchment", "Pegasus", "Perfograma", "Perpetua", "Perpetua Titling MT", "Petala Pro", "Petala Semi Light", "Pipeburn", "Playfair Display", "Prociono", "PT Sans", "PT Serif", "Pythagoras", "Qandon", "Qandon Regular", "Questrial", "Raleway", "Razor", "Reef", "Roboto", "Roboto Slab", "Rockwell", "Rockwell Condensed", "Rockwell Extra Bold", "Runaway", "Sartorius", "Schist", "Scripta Pro", "Seaside Resort", "Selfie", "Serendipity", "Serifa", "Serifa BT", "Serifa Th BT", "Shine Pro", "Shoebox", "Signika", "Silver", "Skolar", "Skyward", "Sniglet", "Sortdecai", "Sorts Mill Goudy", "Source Sans Pro", "Sparkle", "Splandor", "Springtime", "Spruce", "Spumante", "Squoosh Gothic", "Stadt", "Stencil", "Streamster", "Sunday", "Sunn", "Swis721 BlkEx BT", "Swiss911 XCm BT", "Symbol", "Tahoma", "Technical", "Texta", "Ticketbook", "Timber", "Times", "Times New Roman", "Times New Roman PS", "Titillium Web", "Trajan", "TRAJAN PRO", "Trebuchet MS", "Trend Rough", "Troika", "Twist", "Ubuntu", "Uniform", "Univers", "Univers CE 55 Medium", "Univers Condensed", "Unveil", "Uomo", "Varela Round", "Verdana", "Visby", "Vollkorn", "Wahhabi Script", "Waterlily", "Wayback", "Webdings", "Wendy", "Wingdings", "Wingdings 2", "Wingdings 3", "Woodland", "Yonder", "Zodiaclaw"];
                    var F = [];
                    while (y.length > 0) {
                        var G = y.pop();
                        var A = false;
                        for (z = 0; z < u.length && !A; z++) {
                            if (E() > D) {
                                return padString("", 10)
                            }
                            v.style.fontFamily = G + "," + u[z];
                            C.appendChild(v);
                            var H = (v.offsetWidth !== t[u[z]] || v.offsetHeight !== I[u[z]]);
                            C.removeChild(v);
                            A = A || H
                        }
                        if (A) {
                            F.push(G)
                        }
                    }
                    return F.join(";")
                } catch (w) {
                    return padString("", 10)
                }
            }

            var dfGetProp = function () {
                var E = {};
                var s = {};
                s.plugins = 10;
                s.nrOfPlugins = 3;
                s.fonts = 10;
                s.nrOfFonts = 3;
                s.timeZone = 10;
                s.video = 10;
                s.superCookies = 10;
                s.userAgent = 10;
                s.mimeTypes = 10;
                s.nrOfMimeTypes = 3;
                s.canvas = 10;
                s.cpuClass = 5;
                s.platform = 5;
                s.doNotTrack = 5;
                s.webglFp = 10;
                s.jsFonts = 10;
                try {
                    try {
                        var B = dfGetPlug();
                        E.plugins = padString(calculateMd5_b64(B.obj), s.plugins);
                        E.nrOfPlugins = padString(String(B.nr), s.nrOfPlugins)
                    } catch (u) {
                        E.plugins = padString("", s.plugins);
                        E.nrOfPlugins = padString("", s.nrOfPlugins)
                    }
                    E.fonts = padString("", s.fonts);
                    E.nrOfFonts = padString("", s.nrOfFonts);
                    try {
                        var e = new Date();
                        e.setDate(1);
                        e.setMonth(5);
                        var C = e.getTimezoneOffset();
                        e.setMonth(11);
                        var D = e.getTimezoneOffset();
                        E.timeZone = padString(calculateMd5_b64(C + "**" + D), s.timeZone)
                    } catch (u) {
                        E.timeZone = padString("", s.timeZone)
                    }
                    try {
                        E.video = padString(String((screen.width + 7) * (screen.height + 7) * screen.colorDepth), s.video)
                    } catch (u) {
                        E.video = padString("", s.video)
                    }
                    E.superCookies = padString(calculateMd5_b64(dfGetDS()), Math.floor(s.superCookies / 2)) + padString(calculateMd5_b64(dfGetIEUD()), Math.floor(s.superCookies / 2));
                    E.userAgent = padString(calculateMd5_b64(navigator.userAgent), s.userAgent);
                    var v = "";
                    var y = 0;
                    if (navigator.mimeTypes) {
                        y = navigator.mimeTypes.length;
                        for (var z = 0; z < y; z++) {
                            var t = navigator.mimeTypes[z];
                            v += t.description + t.type + t.suffixes
                        }
                    }
                    E.mimeTypes = padString(calculateMd5_b64(v), s.mimeTypes);
                    E.nrOfMimeTypes = padString(String(y), s.nrOfMimeTypes);
                    E.canvas = padString(calculateMd5_b64(dfCanvasFingerprint()), s.canvas);
                    E.cpuClass = (navigator.cpuClass) ? padString(calculateMd5_b64(navigator.cpuClass), s.cpuClass) : padString("", s.cpuClass);
                    E.platform = (navigator.platform) ? padString(calculateMd5_b64(navigator.platform), s.platform) : padString("", s.platform);
                    E.doNotTrack = (navigator.doNotTrack) ? padString(calculateMd5_b64(navigator.doNotTrack), s.doNotTrack) : padString("", s.doNotTrack);
                    E.jsFonts = padString(calculateMd5_b64(getJsFonts()), s.jsFonts);
                    E.webglFp = padString(calculateMd5_b64(getWebglFp()), s.webglFp);
                    var A = 0, i;
                    for (i in E) {
                        if (E.hasOwnProperty(i)) {
                            A = 0;
                            try {
                                A = E[i].length
                            } catch (u) {
                            }
                            if (typeof E[i] === "undefined" || E[i] === null || A !== s[i]) {
                                E[i] = padString("", s[i])
                            }
                        }
                    }
                } catch (w) {
                }
                return E
            }

            var dfCanvasFingerprint = function () {
                var g = document.createElement("canvas");
                if (!!(g.getContext && g.getContext("2d"))) {
                    var h = document.createElement("canvas");
                    var e = h.getContext("2d");
                    var f = "#&*(sdfjlSDFkjls28270(";
                    e.font = "14px 'Arial'";
                    e.textBaseline = "alphabetic";
                    e.fillStyle = "#f61";
                    e.fillRect(138, 2, 63, 20);
                    e.fillStyle = "#068";
                    e.fillText(f, 3, 16);
                    e.fillStyle = "rgba(105, 194, 1, 0.6)";
                    e.fillText(f, 5, 18);
                    return h.toDataURL()
                }
                return padString("", 10)
            }

            var populateFontList = function (b) {
            }

            var dfGetEntropy = function () {
                var e = ["iPad", "iPhone", "iPod"];
                var f = navigator.userAgent;
                if (f) {
                    for (var d = 0; d < e.length; d++) {
                        if (f.indexOf(e[d]) >= 0) {
                            return "20"
                        }
                    }
                }
                return "40"
            }

            var dfSet = function (m, e) {
                try {
                    var i = dfGetProp();
                    var p = dfHashConcat(i);
                    var j = dfGetEntropy();
                    var k = _.G(m);
                    k.value = p + ":" + j
                } catch (o) {
                }
            }

            var dfHashConcat = function (e) {
                try {
                    var f = "";
                    f = e.plugins + e.nrOfPlugins + e.fonts + e.nrOfFonts + e.timeZone + e.video + e.superCookies + e.userAgent + e.mimeTypes + e.nrOfMimeTypes + e.canvas + e.cpuClass + e.platform + e.doNotTrack + e.webglFp + e.jsFonts;
                    f = f.replace(/\+/g, "G").replace(/\//g, "D");
                    return f
                } catch (d) {
                    return ""
                }
            }

            var dfDo = function (d) {
                try {
                    var f = _.G(d);
                    if (!f) {
                        return
                    }
                    if (f.value) {
                        return
                    }
                    dfInitDS();
                    _.R(function () {
                        setTimeout(function () {
                            dfSet(d, 0)
                        }, 500)
                    })
                } catch (e) {
                }
            }

            var padString = function (f, e) {
                if (f.length >= e) {
                    return (f.substring(0, e))
                } else {
                    for (var d = ""; d.length < e - f.length; d += "0") {
                    }
                    return (d.concat(f))
                }
            }

            var calculateMd5_b64 = function (b) {
                return md5_binl2b64(md5_cmc5(md5_s2b(b), b.length * 8))
            }

            var md5_cmc5 = function (g, a) {
                g[a >> 5] |= 128 << ((a) % 32);
                g[(((a + 64) >>> 9) << 4) + 14] = a;
                var h = 1732584193;
                var i = -271733879;
                var j = -1732584194;
                var y = 271733878;
                for (var e = 0; e < g.length; e += 16) {
                    var b = h;
                    var c = i;
                    var d = j;
                    var f = y;
                    h = md5_ff(h, i, j, y, g[e + 0], 7, -680876936);
                    y = md5_ff(y, h, i, j, g[e + 1], 12, -389564586);
                    j = md5_ff(j, y, h, i, g[e + 2], 17, 606105819);
                    i = md5_ff(i, j, y, h, g[e + 3], 22, -1044525330);
                    h = md5_ff(h, i, j, y, g[e + 4], 7, -176418897);
                    y = md5_ff(y, h, i, j, g[e + 5], 12, 1200080426);
                    j = md5_ff(j, y, h, i, g[e + 6], 17, -1473231341);
                    i = md5_ff(i, j, y, h, g[e + 7], 22, -45705983);
                    h = md5_ff(h, i, j, y, g[e + 8], 7, 1770035416);
                    y = md5_ff(y, h, i, j, g[e + 9], 12, -1958414417);
                    j = md5_ff(j, y, h, i, g[e + 10], 17, -42063);
                    i = md5_ff(i, j, y, h, g[e + 11], 22, -1990404162);
                    h = md5_ff(h, i, j, y, g[e + 12], 7, 1804603682);
                    y = md5_ff(y, h, i, j, g[e + 13], 12, -40341101);
                    j = md5_ff(j, y, h, i, g[e + 14], 17, -1502002290);
                    i = md5_ff(i, j, y, h, g[e + 15], 22, 1236535329);
                    h = md5_gg(h, i, j, y, g[e + 1], 5, -165796510);
                    y = md5_gg(y, h, i, j, g[e + 6], 9, -1069501632);
                    j = md5_gg(j, y, h, i, g[e + 11], 14, 643717713);
                    i = md5_gg(i, j, y, h, g[e + 0], 20, -373897302);
                    h = md5_gg(h, i, j, y, g[e + 5], 5, -701558691);
                    y = md5_gg(y, h, i, j, g[e + 10], 9, 38016083);
                    j = md5_gg(j, y, h, i, g[e + 15], 14, -660478335);
                    i = md5_gg(i, j, y, h, g[e + 4], 20, -405537848);
                    h = md5_gg(h, i, j, y, g[e + 9], 5, 568446438);
                    y = md5_gg(y, h, i, j, g[e + 14], 9, -1019803690);
                    j = md5_gg(j, y, h, i, g[e + 3], 14, -187363961);
                    i = md5_gg(i, j, y, h, g[e + 8], 20, 1163531501);
                    h = md5_gg(h, i, j, y, g[e + 13], 5, -1444681467);
                    y = md5_gg(y, h, i, j, g[e + 2], 9, -51403784);
                    j = md5_gg(j, y, h, i, g[e + 7], 14, 1735328473);
                    i = md5_gg(i, j, y, h, g[e + 12], 20, -1926607734);
                    h = md5_hh(h, i, j, y, g[e + 5], 4, -378558);
                    y = md5_hh(y, h, i, j, g[e + 8], 11, -2022574463);
                    j = md5_hh(j, y, h, i, g[e + 11], 16, 1839030562);
                    i = md5_hh(i, j, y, h, g[e + 14], 23, -35309556);
                    h = md5_hh(h, i, j, y, g[e + 1], 4, -1530992060);
                    y = md5_hh(y, h, i, j, g[e + 4], 11, 1272893353);
                    j = md5_hh(j, y, h, i, g[e + 7], 16, -155497632);
                    i = md5_hh(i, j, y, h, g[e + 10], 23, -1094730640);
                    h = md5_hh(h, i, j, y, g[e + 13], 4, 681279174);
                    y = md5_hh(y, h, i, j, g[e + 0], 11, -358537222);
                    j = md5_hh(j, y, h, i, g[e + 3], 16, -722521979);
                    i = md5_hh(i, j, y, h, g[e + 6], 23, 76029189);
                    h = md5_hh(h, i, j, y, g[e + 9], 4, -640364487);
                    y = md5_hh(y, h, i, j, g[e + 12], 11, -421815835);
                    j = md5_hh(j, y, h, i, g[e + 15], 16, 530742520);
                    i = md5_hh(i, j, y, h, g[e + 2], 23, -995338651);
                    h = md5_ii(h, i, j, y, g[e + 0], 6, -198630844);
                    y = md5_ii(y, h, i, j, g[e + 7], 10, 1126891415);
                    j = md5_ii(j, y, h, i, g[e + 14], 15, -1416354905);
                    i = md5_ii(i, j, y, h, g[e + 5], 21, -57434055);
                    h = md5_ii(h, i, j, y, g[e + 12], 6, 1700485571);
                    y = md5_ii(y, h, i, j, g[e + 3], 10, -1894986606);
                    j = md5_ii(j, y, h, i, g[e + 10], 15, -1051523);
                    i = md5_ii(i, j, y, h, g[e + 1], 21, -2054922799);
                    h = md5_ii(h, i, j, y, g[e + 8], 6, 1873313359);
                    y = md5_ii(y, h, i, j, g[e + 15], 10, -30611744);
                    j = md5_ii(j, y, h, i, g[e + 6], 15, -1560198380);
                    i = md5_ii(i, j, y, h, g[e + 13], 21, 1309151649);
                    h = md5_ii(h, i, j, y, g[e + 4], 6, -145523070);
                    y = md5_ii(y, h, i, j, g[e + 11], 10, -1120210379);
                    j = md5_ii(j, y, h, i, g[e + 2], 15, 718787259);
                    i = md5_ii(i, j, y, h, g[e + 9], 21, -343485551);
                    h = md5_safe_add(h, b);
                    i = md5_safe_add(i, c);
                    j = md5_safe_add(j, d);
                    y = md5_safe_add(y, f)
                }
                return Array(h, i, j, y)
            }

            var md5_cmn = function (a, j, k, m, b, i) {
                return md5_safe_add(md5_bit_rol(md5_safe_add(md5_safe_add(j, a), md5_safe_add(m, i)), b), k)
            }

            var md5_ff = function (m, o, a, b, p, c, d) {
                return md5_cmn((o & a) | ((~o) & b), m, o, p, c, d)
            }

            var md5_gg = function (m, o, a, b, p, c, d) {
                return md5_cmn((o & b) | (a & (~b)), m, o, p, c, d)
            }

            var md5_hh = function (m, o, a, b, p, c, d) {
                return md5_cmn(o ^ a ^ b, m, o, p, c, d)
            }

            var md5_ii = function (m, o, a, b, p, c, d) {
                return md5_cmn(a ^ (o | (~b)), m, o, p, c, d)
            }

            var md5_safe_add = function (g, a) {
                var b = (g & 65535) + (a & 65535);
                var h = (g >> 16) + (a >> 16) + (b >> 16);
                return (h << 16) | (b & 65535)
            }

            var md5_bit_rol = function (a, b) {
                return (a << b) | (a >>> (32 - b))
            }

            var md5_s2b = function (c) {
                var h = Array();
                var a = (1 << 8) - 1;
                for (var b = 0; b < c.length * 8; b += 8) {
                    h[b >> 5] |= (c.charCodeAt(b / 8) & a) << (b % 32)
                }
                return h
            }

            var md5_binl2b64 = function (i) {
                var c = "";
                var j = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
                var p = "";
                for (var b = 0; b < i.length * 4; b += 3) {
                    var a = (((i[b >> 2] >> 8 * (b % 4)) & 255) << 16) | (((i[b + 1 >> 2] >> 8 * ((b + 1) % 4)) & 255) << 8) | ((i[b + 2 >> 2] >> 8 * ((b + 2) % 4)) & 255);
                    for (var d = 0; d < 4; d++) {
                        if (b * 8 + d * 6 > i.length * 32) {
                            p += c
                        } else {
                            p += j.charAt((a >> 6 * (3 - d)) & 63)
                        }
                    }
                }
                return p
            }

            var PluginDetect = {
                version: "0.7.5",
                name: "PluginDetect",
                handler: function (a, c, b) {
                    return function () {
                        a(c, b)
                    }
                },
                isDefined: function (b) {
                    return typeof b != "undefined"
                },
                isArray: function (b) {
                    return (/array/i).test(Object.prototype.toString.call(b))
                },
                isFunc: function (b) {
                    return typeof b == "function"
                },
                isString: function (b) {
                    return typeof b == "string"
                },
                isNum: function (b) {
                    return typeof b == "number"
                },
                isStrNum: function (b) {
                    return (typeof b == "string" && (/\d/).test(b))
                },
                getNumRegx: /[\d][\d\.\_,-]*/,
                splitNumRegx: /[\.\_,-]/g,
                getNum: function (d, a) {
                    var b = this, c = b.isStrNum(d) ? (b.isDefined(a) ? new RegExp(a) : b.getNumRegx).exec(d) : null;
                    return c ? c[0] : null
                },
                compareNums: function (b, h, f) {
                    var g = this, e, d, c, a = parseInt;
                    if (g.isStrNum(b) && g.isStrNum(h)) {
                        if (g.isDefined(f) && f.compareNums) {
                            return f.compareNums(b, h)
                        }
                        e = b.split(g.splitNumRegx);
                        d = h.split(g.splitNumRegx);
                        for (c = 0; c < Math.min(e.length, d.length); c++) {
                            if (a(e[c], 10) > a(d[c], 10)) {
                                return 1
                            }
                            if (a(e[c], 10) < a(d[c], 10)) {
                                return -1
                            }
                        }
                    }
                    return 0
                },
                formatNum: function (e, a) {
                    var b = this, d, c;
                    if (!b.isStrNum(e)) {
                        return null
                    }
                    if (!b.isNum(a)) {
                        a = 4
                    }
                    a--;
                    c = e.replace(/\s/g, "").split(b.splitNumRegx).concat(["0", "0", "0", "0"]);
                    for (d = 0; d < 4; d++) {
                        if (/^(0+)(.+)$/.test(c[d])) {
                            c[d] = RegExp.$2
                        }
                        if (d > a || !(/\d/).test(c[d])) {
                            c[d] = "0"
                        }
                    }
                    return c.slice(0, 4).join(",")
                },
                $hasMimeType: function (a) {
                    return function (d) {
                        if (!a.isIE && d) {
                            var c, b, e, f = a.isString(d) ? [d] : d;
                            if (!f || !f.length) {
                                return null
                            }
                            for (e = 0; e < f.length; e++) {
                                if (/[^\s]/.test(f[e]) && (c = navigator.mimeTypes[f[e]]) && (b = c.enabledPlugin) && (b.name || b.description)) {
                                    return c
                                }
                            }
                        }
                        return null
                    }
                },
                findNavPlugin: function (d, k, h) {
                    var b = this, a = new RegExp(d, "i"), j = (!b.isDefined(k) || k) ? /\d/ : 0, c = h ? new RegExp(h, "i") : 0, f = navigator.plugins, A = "", m, g, e;
                    for (m = 0; m < f.length; m++) {
                        e = f[m].description || A;
                        g = f[m].name || A;
                        if ((a.test(e) && (!j || j.test(RegExp.leftContext + RegExp.rightContext))) || (a.test(g) && (!j || j.test(RegExp.leftContext + RegExp.rightContext)))) {
                            if (!c || !(c.test(e) || c.test(g))) {
                                return f[m]
                            }
                        }
                    }
                    return null
                },
                getMimeEnabledPlugin: function (e, d) {
                    var c = this, f, a = new RegExp(d, "i"), b = "";
                    if ((f = c.hasMimeType(e)) && (f = f.enabledPlugin) && (a.test(f.description || b) || a.test(f.name || b))) {
                        return f
                    }
                    return 0
                },
                getPluginFileVersion: function (h, d) {
                    var b = this, g, f, a, c, e = -1;
                    if (b.OS > 2 || !h || !h.version || !(g = b.getNum(h.version))) {
                        return d
                    }
                    if (!d) {
                        return g
                    }
                    g = b.formatNum(g);
                    d = b.formatNum(d);
                    f = d.split(b.splitNumRegx);
                    a = g.split(b.splitNumRegx);
                    for (c = 0; c < f.length; c++) {
                        if (e > -1 && c > e && f[c] != "0") {
                            return d
                        }
                        if (a[c] != f[c]) {
                            if (e == -1) {
                                e = c
                            }
                            if (f[c] != "0") {
                                return d
                            }
                        }
                    }
                    return g
                },
                AXO: window.ActiveXObject,
                getAXO: function (f) {
                    var c = null, b, a = this, d;
                    try {
                        c = new a.AXO(f)
                    } catch (b) {
                    }
                    return c
                },
                convertFuncs: function (b) {
                    var d, c, a, g = /^[\$][\$]/, h = {}, f = this;
                    for (d in b) {
                        if (g.test(d)) {
                            h[d] = 1
                        }
                    }
                    for (d in h) {
                        try {
                            c = d.slice(2);
                            if (c.length > 0 && !b[c]) {
                                b[c] = b[d](b);
                                delete b[d]
                            }
                        } catch (a) {
                        }
                    }
                },
                initScript: function () {
                    var i = this, g = navigator, a = "/", e = g.userAgent || "", c = g.vendor || "", h = g.platform || "", d = g.product || "";
                    i.OS = 100;
                    if (h) {
                        var b, j = ["Win", 1, "Mac", 2, "Linux", 3, "FreeBSD", 4, "iPhone", 21.1, "iPod", 21.2, "iPad", 21.3, "Win.*CE", 22.1, "Win.*Mobile", 22.2, "Pocket\\s*PC", 22.3, "", 100];
                        for (b = j.length - 2; b >= 0; b = b - 2) {
                            if (j[b] && new RegExp(j[b], "i").test(h)) {
                                i.OS = j[b + 1];
                                break
                            }
                        }
                    }
                    i.convertFuncs(i);
                    i.isIE = (function () {
                        var m = (function (u, ae) {
                            var q = "0.7.10", o = "", p = "?", U = "function", ac = "undefined", ag = "object", W = "string", af = "major", R = "model", ad = "name", Y = "type", Q = "vendor", t = "version", ai = "architecture", ab = "console", V = "mobile", s = "tablet", Z = "smarttv", r = "wearable", T = "embedded";
                            var ah = {
                                extend: function (z, w) {
                                    for (var y in w) {
                                        if ("browser cpu device engine os".indexOf(y) !== -1 && w[y].length % 2 === 0) {
                                            z[y] = w[y].concat(z[y])
                                        }
                                    }
                                    return z
                                }, has: function (w, y) {
                                    if (typeof w === "string") {
                                        return y.toLowerCase().indexOf(w.toLowerCase()) !== -1
                                    } else {
                                        return false
                                    }
                                }, lowerize: function (w) {
                                    return w.toLowerCase()
                                }, major: function (w) {
                                    return typeof (w) === W ? w.split(".")[0] : ae
                                }
                            };
                            var v = {
                                rgx: function () {
                                    var w, D = 0, E, F, G, H, C, B, A = arguments;
                                    while (D < A.length && !C) {
                                        var y = A[D], z = A[D + 1];
                                        if (typeof w === ac) {
                                            w = {};
                                            for (G in z) {
                                                if (z.hasOwnProperty(G)) {
                                                    H = z[G];
                                                    if (typeof H === ag) {
                                                        w[H[0]] = ae
                                                    } else {
                                                        w[H] = ae
                                                    }
                                                }
                                            }
                                        }
                                        E = F = 0;
                                        while (E < y.length && !C) {
                                            C = y[E++].exec(this.getUA());
                                            if (!!C) {
                                                for (G = 0; G < z.length; G++) {
                                                    B = C[++F];
                                                    H = z[G];
                                                    if (typeof H === ag && H.length > 0) {
                                                        if (H.length == 2) {
                                                            if (typeof H[1] == U) {
                                                                w[H[0]] = H[1].call(this, B)
                                                            } else {
                                                                w[H[0]] = H[1]
                                                            }
                                                        } else {
                                                            if (H.length == 3) {
                                                                if (typeof H[1] === U && !(H[1].exec && H[1].test)) {
                                                                    w[H[0]] = B ? H[1].call(this, B, H[2]) : ae
                                                                } else {
                                                                    w[H[0]] = B ? B.replace(H[1], H[2]) : ae
                                                                }
                                                            } else {
                                                                if (H.length == 4) {
                                                                    w[H[0]] = B ? H[3].call(this, B.replace(H[1], H[2])) : ae
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        w[H] = B ? B : ae
                                                    }
                                                }
                                            }
                                        }
                                        D += 2
                                    }
                                    return w
                                }, str: function (w, y) {
                                    for (var z in y) {
                                        if (typeof y[z] === ag && y[z].length > 0) {
                                            for (var A = 0; A < y[z].length; A++) {
                                                if (ah.has(y[z][A], w)) {
                                                    return (z === p) ? ae : z
                                                }
                                            }
                                        } else {
                                            if (ah.has(y[z], w)) {
                                                return (z === p) ? ae : z
                                            }
                                        }
                                    }
                                    return w
                                }
                            };
                            var X = {
                                browser: {
                                    oldsafari: {
                                        version: {
                                            "1.0": "/8",
                                            "1.2": "/1",
                                            "1.3": "/3",
                                            "2.0": "/412",
                                            "2.0.2": "/416",
                                            "2.0.3": "/417",
                                            "2.0.4": "/419",
                                            "?": "/"
                                        }
                                    }
                                },
                                device: {
                                    amazon: { model: { "Fire Phone": ["SD", "KF"] } },
                                    sprint: {
                                        model: { "Evo Shift 4G": "7373KT" },
                                        vendor: { HTC: "APA", Sprint: "Sprint" }
                                    }
                                },
                                os: {
                                    windows: {
                                        version: {
                                            ME: "4.90",
                                            "NT 3.11": "NT3.51",
                                            "NT 4.0": "NT4.0",
                                            "2000": "NT 5.0",
                                            XP: ["NT 5.1", "NT 5.2"],
                                            Vista: "NT 6.0",
                                            "7": "NT 6.1",
                                            "8": "NT 6.2",
                                            "8.1": "NT 6.3",
                                            "10": ["NT 6.4", "NT 10.0"],
                                            RT: "ARM"
                                        }
                                    }
                                }
                            };
                            var aa = { browser: [[/(opera\smini)\/([\w\.-]+)/i, /(opera\s[mobiletab]+).+version\/([\w\.-]+)/i, /(opera).+version\/([\w\.]+)/i, /(opera)[\/\s]+([\w\.]+)/i], [ad, t], [/(OPiOS)[\/\s]+([\w\.]+)/i], [[ad, "Opera Mini"], t], [/\s(opr)\/([\w\.]+)/i], [[ad, "Opera"], t], [/(kindle)\/([\w\.]+)/i, /(lunascape|maxthon|netfront|jasmine|blazer)[\/\s]?([\w\.]+)*/i, /(avant\s|iemobile|slim|baidu)(?:browser)?[\/\s]?([\w\.]*)/i, /(?:ms|\()(ie)\s([\w\.]+)/i, /(rekonq)\/([\w\.]+)*/i, /(chromium|flock|rockmelt|midori|epiphany|silk|skyfire|ovibrowser|bolt|iron|vivaldi|iridium|phantomjs)\/([\w\.-]+)/i], [ad, t], [/(trident).+rv[:\s]([\w\.]+).+like\sgecko/i], [[ad, "IE"], t], [/(edge)\/((\d+)?[\w\.]+)/i], [ad, t], [/(yabrowser)\/([\w\.]+)/i], [[ad, "Yandex"], t], [/(comodo_dragon)\/([\w\.]+)/i], [[ad, /_/g, " "], t], [/(chrome|omniweb|arora|[tizenoka]{5}\s?browser)\/v?([\w\.]+)/i, /(qqbrowser)[\/\s]?([\w\.]+)/i], [ad, t], [/(uc\s?browser)[\/\s]?([\w\.]+)/i, /ucweb.+(ucbrowser)[\/\s]?([\w\.]+)/i, /JUC.+(ucweb)[\/\s]?([\w\.]+)/i], [[ad, "UCBrowser"], t], [/(dolfin)\/([\w\.]+)/i], [[ad, "Dolphin"], t], [/((?:android.+)crmo|crios)\/([\w\.]+)/i], [[ad, "Chrome"], t], [/XiaoMi\/MiuiBrowser\/([\w\.]+)/i], [t, [ad, "MIUI Browser"]], [/android.+version\/([\w\.]+)\s+(?:mobile\s?safari|safari)/i], [t, [ad, "Android Browser"]], [/FBAV\/([\w\.]+);/i], [t, [ad, "Facebook"]], [/fxios\/([\w\.-]+)/i], [t, [ad, "Firefox"]], [/version\/([\w\.]+).+?mobile\/\w+\s(safari)/i], [t, [ad, "Mobile Safari"]], [/version\/([\w\.]+).+?(mobile\s?safari|safari)/i], [t, ad], [/webkit.+?(mobile\s?safari|safari)(\/[\w\.]+)/i], [ad, [t, v.str, X.browser.oldsafari.version]], [/(konqueror)\/([\w\.]+)/i, /(webkit|khtml)\/([\w\.]+)/i], [ad, t], [/(navigator|netscape)\/([\w\.-]+)/i], [[ad, "Netscape"], t], [/(swiftfox)/i, /(icedragon|iceweasel|camino|chimera|fennec|maemo\sbrowser|minimo|conkeror)[\/\s]?([\w\.\+]+)/i, /(firefox|seamonkey|k-meleon|icecat|iceape|firebird|phoenix)\/([\w\.-]+)/i, /(mozilla)\/([\w\.]+).+rv\:.+gecko\/\d+/i, /(polaris|lynx|dillo|icab|doris|amaya|w3m|netsurf|sleipnir)[\/\s]?([\w\.]+)/i, /(links)\s\(([\w\.]+)/i, /(gobrowser)\/?([\w\.]+)*/i, /(ice\s?browser)\/v?([\w\._]+)/i, /(mosaic)[\/\s]([\w\.]+)/i], [ad, t]] };
                            var S = function (z, w) {
                                if (!(this instanceof S)) {
                                    return new S(z, w).getResult()
                                }
                                var y = z || ((u && u.navigator && u.navigator.userAgent) ? u.navigator.userAgent : o);
                                var A = w ? ah.extend(aa, w) : aa;
                                this.getBrowser = function () {
                                    var B = v.rgx.apply(this, A.browser);
                                    B.major = ah.major(B.version);
                                    return B
                                };
                                this.getUA = function () {
                                    return y
                                };
                                this.setUA = function (B) {
                                    y = B;
                                    return this
                                };
                                this.setUA(y);
                                return this
                            };
                            S.VERSION = q;
                            S.BROWSER = { NAME: ad, MAJOR: af, VERSION: t };
                            S.CPU = { ARCHITECTURE: ai };
                            S.DEVICE = {
                                MODEL: R,
                                VENDOR: Q,
                                TYPE: Y,
                                CONSOLE: ab,
                                MOBILE: V,
                                SMARTTV: Z,
                                TABLET: s,
                                WEARABLE: r,
                                EMBEDDED: T
                            };
                            S.ENGINE = { NAME: ad, VERSION: t };
                            S.OS = { NAME: ad, VERSION: t };
                            return S
                        })(typeof window === "object" ? window : this);
                        var k = new m();
                        return /^IE|Edge$/.test((k.getBrowser() || {}).name)
                    }());
                    i.verIE = i.isIE && (/MSIE\s*(\d+\.?\d*)/i).test(e) ? parseFloat(RegExp.$1, 10) : null;
                    i.ActiveXEnabled = false;
                    if (i.isIE) {
                        var b, f = ["Msxml2.XMLHTTP", "Msxml2.DOMDocument", "Microsoft.XMLDOM", "ShockwaveFlash.ShockwaveFlash", "TDCCtl.TDCCtl", "Shell.UIHelper", "Scripting.Dictionary", "wmplayer.ocx"];
                        for (b = 0; b < f.length; b++) {
                            if (i.getAXO(f[b])) {
                                i.ActiveXEnabled = true;
                                break
                            }
                        }
                        i.head = i.isDefined(document.getElementsByTagName) ? document.getElementsByTagName("head")[0] : null
                    }
                    i.isGecko = (/Gecko/i).test(d) && (/Gecko\s*\/\s*\d/i).test(e);
                    i.verGecko = i.isGecko ? i.formatNum((/rv\s*\:\s*([\.\,\d]+)/i).test(e) ? RegExp.$1 : "0.9") : null;
                    i.isSafari = (/Safari\s*\/\s*\d/i).test(e) && (/Apple/i).test(c);
                    i.isChrome = (/Chrome\s*\/\s*(\d[\d\.]*)/i).test(e);
                    i.verChrome = i.isChrome ? i.formatNum(RegExp.$1) : null;
                    i.isOpera = (/Opera\s*[\/]?\s*(\d+\.?\d*)/i).test(e);
                    i.verOpera = i.isOpera && ((/Version\s*\/\s*(\d+\.?\d*)/i).test(e) || 1) ? parseFloat(RegExp.$1, 10) : null;
                    i.addWinEvent("load", i.handler(i.runWLfuncs, i))
                },
                init: function (a) {
                    var c = this, b, a;
                    if (!c.isString(a)) {
                        return -3
                    }
                    if (a.length == 1) {
                        c.getVersionDelimiter = a;
                        return -3
                    }
                    a = a.toLowerCase().replace(/\s/g, "");
                    b = c[a];
                    if (!b || !b.getVersion) {
                        return -3
                    }
                    c.plugin = b;
                    if (!c.isDefined(b.installed)) {
                        b.installed = b.version = b.version0 = b.getVersionDone = null;
                        b.$ = c;
                        b.pluginName = a
                    }
                    c.garbage = false;
                    if (c.isIE && !c.ActiveXEnabled) {
                        if (b !== c.java) {
                            return -2
                        }
                    }
                    return 1
                },
                fPush: function (c, b) {
                    var a = this;
                    if (a.isArray(b) && (a.isFunc(c) || (a.isArray(c) && c.length > 0 && a.isFunc(c[0])))) {
                        b.push(c)
                    }
                },
                callArray: function (c) {
                    var a = this, b;
                    if (a.isArray(c)) {
                        for (b = 0; b < c.length; b++) {
                            if (c[b] === null) {
                                return
                            }
                            a.call(c[b]);
                            c[b] = null
                        }
                    }
                },
                call: function (a) {
                    var c = this, b = c.isArray(a) ? a.length : -1;
                    if (b > 0 && c.isFunc(a[0])) {
                        a[0](c, b > 1 ? a[1] : 0, b > 2 ? a[2] : 0, b > 3 ? a[3] : 0)
                    } else {
                        if (c.isFunc(a)) {
                            a(c)
                        }
                    }
                },
                getVersionDelimiter: ",",
                $getVersion: function (a) {
                    return function (e, h, g) {
                        var b = a.init(e), d, c, f;
                        if (b < 0) {
                            return null
                        }
                        d = a.plugin;
                        if (d.getVersionDone != 1) {
                            d.getVersion(null, h, g);
                            if (d.getVersionDone === null) {
                                d.getVersionDone = 1
                            }
                        }
                        a.cleanup();
                        c = (d.version || d.version0);
                        c = c ? c.replace(a.splitNumRegx, a.getVersionDelimiter) : c;
                        return c
                    }
                },
                cleanup: function () {
                    var a = this;
                    if (a.garbage && a.isDefined(window.CollectGarbage)) {
                        window.CollectGarbage()
                    }
                },
                isActiveXObject: function (a, g) {
                    var b = this, d = false, c, f = "<", h = f + 'object width="1" height="1" style="display:none" ' + a.getCodeBaseVersion(g) + ">" + a.HTML + f + "/object>";
                    if (!b.head) {
                        return d
                    }
                    if (b.head.firstChild) {
                        b.head.insertBefore(document.createElement("object"), b.head.firstChild)
                    } else {
                        b.head.appendChild(document.createElement("object"))
                    }
                    b.head.firstChild.outerHTML = h;
                    try {
                        b.head.firstChild.classid = a.classID
                    } catch (c) {
                    }
                    try {
                        if (b.head.firstChild.object) {
                            d = true
                        }
                    } catch (c) {
                    }
                    try {
                        if (d && b.head.firstChild.readyState < 4) {
                            b.garbage = true
                        }
                    } catch (c) {
                    }
                    b.head.removeChild(b.head.firstChild);
                    return d
                },
                codebaseSearch: function (h, b) {
                    var c = this;
                    if (!c.ActiveXEnabled || !h) {
                        return null
                    }
                    if (h.BIfuncs && h.BIfuncs.length && h.BIfuncs[h.BIfuncs.length - 1] !== null) {
                        c.callArray(h.BIfuncs)
                    }
                    var e, f = h.SEARCH, d;
                    if (c.isStrNum(b)) {
                        if (f.match && f.min && c.compareNums(b, f.min) <= 0) {
                            return true
                        }
                        if (f.match && f.max && c.compareNums(b, f.max) >= 0) {
                            return false
                        }
                        e = c.isActiveXObject(h, b);
                        if (e && (!f.min || c.compareNums(b, f.min) > 0)) {
                            f.min = b
                        }
                        if (!e && (!f.max || c.compareNums(b, f.max) < 0)) {
                            f.max = b
                        }
                        return e
                    }
                    var g = [0, 0, 0, 0], o = [].concat(f.digits), G = f.min ? 1 : 0, m, k, j, i, F, a = function (r, q) {
                        var p = [].concat(g);
                        p[r] = q;
                        return c.isActiveXObject(h, p.join(","))
                    };
                    if (f.max) {
                        i = f.max.split(c.splitNumRegx);
                        for (m = 0; m < i.length; m++) {
                            i[m] = parseInt(i[m], 10)
                        }
                        if (i[0] < o[0]) {
                            o[0] = i[0]
                        }
                    }
                    if (f.min) {
                        F = f.min.split(c.splitNumRegx);
                        for (m = 0; m < F.length; m++) {
                            F[m] = parseInt(F[m], 10)
                        }
                        if (F[0] > g[0]) {
                            g[0] = F[0]
                        }
                    }
                    if (F && i) {
                        for (m = 1; m < F.length; m++) {
                            if (F[m - 1] != i[m - 1]) {
                                break
                            }
                            if (i[m] < o[m]) {
                                o[m] = i[m]
                            }
                            if (F[m] > g[m]) {
                                g[m] = F[m]
                            }
                        }
                    }
                    if (f.max) {
                        for (m = 1; m < o.length; m++) {
                            if (i[m] > 0 && o[m] == 0 && o[m - 1] < f.digits[m - 1]) {
                                o[m - 1] += 1;
                                break
                            }
                        }
                    }
                    for (m = 0; m < o.length; m++) {
                        j = {};
                        for (k = 0; k < 20; k++) {
                            if (o[m] - g[m] < 1) {
                                break
                            }
                            e = Math.round((o[m] + g[m]) / 2);
                            if (j["a" + e]) {
                                break
                            }
                            j["a" + e] = 1;
                            if (a(m, e)) {
                                g[m] = e;
                                G = 1
                            } else {
                                o[m] = e
                            }
                        }
                        o[m] = g[m];
                        if (!G && a(m, g[m])) {
                            G = 1
                        }
                        if (!G) {
                            break
                        }
                    }
                    return G ? g.join(",") : null
                },
                addWinEvent: function (b, a) {
                    var c = this, d = window, e;
                    if (c.isFunc(a)) {
                        if (d.addEventListener) {
                            d.addEventListener(b, a, false)
                        } else {
                            if (d.attachEvent) {
                                d.attachEvent("on" + b, a)
                            } else {
                                e = d["on" + b];
                                d["on" + b] = c.winHandler(a, e)
                            }
                        }
                    }
                },
                winHandler: function (d, c) {
                    return function () {
                        d();
                        if (typeof c == "function") {
                            c()
                        }
                    }
                },
                WLfuncs0: [],
                WLfuncs: [],
                runWLfuncs: function (a) {
                    a.winLoaded = true;
                    a.callArray(a.WLfuncs0);
                    a.callArray(a.WLfuncs);
                    if (a.onDoneEmptyDiv) {
                        a.onDoneEmptyDiv()
                    }
                },
                winLoaded: false,
                $onWindowLoaded: function (a) {
                    return function (b) {
                        if (a.winLoaded) {
                            a.call(b)
                        } else {
                            a.fPush(b, a.WLfuncs)
                        }
                    }
                },
                div: null,
                divWidth: 50,
                pluginSize: 1,
                emptyDiv: function () {
                    var a = this, d, c, e, b = 0;
                    if (a.div && a.div.childNodes) {
                        for (d = a.div.childNodes.length - 1; d >= 0; d--) {
                            e = a.div.childNodes[d];
                            if (e && e.childNodes) {
                                if (b == 0) {
                                    for (c = e.childNodes.length - 1; c >= 0; c--) {
                                        e.removeChild(e.childNodes[c])
                                    }
                                    a.div.removeChild(e)
                                } else {
                                }
                            }
                        }
                    }
                },
                DONEfuncs: [],
                onDoneEmptyDiv: function () {
                    var a = this, b, c;
                    if (!a.winLoaded) {
                        return
                    }
                    if (a.WLfuncs && a.WLfuncs.length && a.WLfuncs[a.WLfuncs.length - 1] !== null) {
                        return
                    }
                    for (b in a) {
                        c = a[b];
                        if (c && c.funcs) {
                            if (c.OTF == 3) {
                                return
                            }
                            if (c.funcs.length && c.funcs[c.funcs.length - 1] !== null) {
                                return
                            }
                        }
                    }
                    for (b = 0; b < a.DONEfuncs.length; b++) {
                        a.callArray(a.DONEfuncs)
                    }
                    a.emptyDiv()
                },
                getWidth: function (a) {
                    if (a) {
                        var b = a.scrollWidth || a.offsetWidth, c = this;
                        if (c.isNum(b)) {
                            return b
                        }
                    }
                    return -1
                },
                getTagStatus: function (f, m, g, h) {
                    var i = this, k, c = f.span, d = i.getWidth(c), A = g.span, b = i.getWidth(A), j = m.span, a = i.getWidth(j);
                    if (!c || !A || !j || !i.getDOMobj(f)) {
                        return -2
                    }
                    if (b < a || d < 0 || b < 0 || a < 0 || a <= i.pluginSize || i.pluginSize < 1) {
                        return 0
                    }
                    if (d >= a) {
                        return -1
                    }
                    try {
                        if (d == i.pluginSize && (!i.isIE || i.getDOMobj(f).readyState == 4)) {
                            if (!f.winLoaded && i.winLoaded) {
                                return 1
                            }
                            if (f.winLoaded && i.isNum(h)) {
                                if (!i.isNum(f.count)) {
                                    f.count = h
                                }
                                if (h - f.count >= 10) {
                                    return 1
                                }
                            }
                        }
                    } catch (k) {
                    }
                    return 0
                },
                getDOMobj: function (d, f) {
                    var c, b = this, a = d ? d.span : 0, g = a && a.firstChild ? 1 : 0;
                    try {
                        if (g && f) {
                            a.firstChild.focus()
                        }
                    } catch (c) {
                    }
                    return g ? a.firstChild : null
                },
                setStyle: function (g, d) {
                    var c = g.style, f, b, a = this;
                    if (c && d) {
                        for (f = 0; f < d.length; f = f + 2) {
                            try {
                                c[d[f]] = d[f + 1]
                            } catch (b) {
                            }
                        }
                    }
                },
                insertDivInBody: function (b) {
                    var i, f = this, a = "pd33993399", d = null, h = document, g = "<", c = (h.getElementsByTagName("body")[0] || h.body);
                    if (!c) {
                        try {
                            h.write(g + 'div id="' + a + '">o' + g + "/div>");
                            d = h.getElementById(a)
                        } catch (i) {
                        }
                    }
                    c = (h.getElementsByTagName("body")[0] || h.body);
                    if (c) {
                        if (c.firstChild && f.isDefined(c.insertBefore)) {
                            c.insertBefore(b, c.firstChild)
                        } else {
                            c.appendChild(b)
                        }
                        if (d) {
                            c.removeChild(d)
                        }
                    } else {
                    }
                },
                insertHTML: function (k, f, m, b, D) {
                    var E, a = document, q = this, i, h = a.createElement("span"), c, o, j = "<";
                    var g = ["outlineStyle", "none", "borderStyle", "none", "padding", "0px", "margin", "0px", "visibility", "visible"];
                    if (!q.isDefined(b)) {
                        b = ""
                    }
                    if (q.isString(k) && (/[^\s]/).test(k)) {
                        i = j + k + ' width="' + q.pluginSize + '" height="' + q.pluginSize + '" ';
                        for (c = 0; c < f.length; c = c + 2) {
                            if (/[^\s]/.test(f[c + 1])) {
                                i += f[c] + '="' + f[c + 1] + '" '
                            }
                        }
                        i += ">";
                        for (c = 0; c < m.length; c = c + 2) {
                            if (/[^\s]/.test(m[c + 1])) {
                                i += j + 'param name="' + m[c] + '" value="' + m[c + 1] + '" />'
                            }
                        }
                        i += b + j + "/" + k + ">"
                    } else {
                        i = b
                    }
                    if (!q.div) {
                        q.div = a.createElement("div");
                        o = a.getElementById("plugindetect");
                        if (o) {
                            q.div = o
                        } else {
                            q.div.id = "plugindetect";
                            q.insertDivInBody(q.div)
                        }
                        q.setStyle(q.div, g.concat(["width", q.divWidth + "px", "height", (q.pluginSize + 3) + "px", "fontSize", (q.pluginSize + 3) + "px", "lineHeight", (q.pluginSize + 3) + "px", "verticalAlign", "baseline", "display", "block"]));
                        if (!o) {
                            q.setStyle(q.div, ["position", "absolute", "right", "0px", "top", "0px"])
                        }
                    }
                    if (q.div && q.div.parentNode) {
                        q.div.appendChild(h);
                        q.setStyle(h, g.concat(["fontSize", (q.pluginSize + 3) + "px", "lineHeight", (q.pluginSize + 3) + "px", "verticalAlign", "baseline", "display", "inline"]));
                        try {
                            if (h && h.parentNode) {
                                h.focus()
                            }
                        } catch (E) {
                        }
                        try {
                            h.innerHTML = i
                        } catch (E) {
                        }
                        if (h.childNodes.length == 1 && !(q.isGecko && q.compareNums(q.verGecko, "1,5,0,0") < 0)) {
                            q.setStyle(h.firstChild, g.concat(["display", "inline"]))
                        }
                        return { span: h, winLoaded: q.winLoaded, tagName: (q.isString(k) ? k : "") }
                    }
                    return { span: null, winLoaded: q.winLoaded, tagName: "" }
                },
                quicktime: {
                    mimeType: ["video/quicktime", "application/x-quicktimeplayer", "image/x-macpaint", "image/x-quicktime"],
                    progID: "QuickTimeCheckObject.QuickTimeCheck.1",
                    progID0: "QuickTime.QuickTime",
                    classID: "clsid:02BF25D5-8C17-4B23-BC80-D3488ABDDC6B",
                    minIEver: 7,
                    HTML: ("<") + 'param name="src" value="" />' + ("<") + 'param name="controller" value="false" />',
                    getCodeBaseVersion: function (a) {
                        return 'codebase="#version=' + a + '"'
                    },
                    SEARCH: { min: 0, max: 0, match: 0, digits: [16, 128, 128, 0] },
                    getVersion: function (a) {
                        var d = this, b = d.$, e = null, c = null, f;
                        if (!b.isIE) {
                            if (b.hasMimeType(d.mimeType)) {
                                c = b.OS != 3 ? b.findNavPlugin("QuickTime.*Plug-?in", 0) : null;
                                if (c && c.name) {
                                    e = b.getNum(c.name)
                                }
                            }
                        } else {
                            if (b.isStrNum(a)) {
                                f = a.split(b.splitNumRegx);
                                if (f.length > 3 && parseInt(f[3], 10) > 0) {
                                    f[3] = "9999"
                                }
                                a = f.join(",")
                            }
                            if (b.isStrNum(a) && b.verIE >= d.minIEver && d.canUseIsMin() > 0) {
                                d.installed = d.isMin(a);
                                d.getVersionDone = 0;
                                return
                            }
                            d.getVersionDone = 1;
                            if (!e && b.verIE >= d.minIEver) {
                                e = d.CDBASE2VER(b.codebaseSearch(d))
                            }
                            if (!e) {
                                c = b.getAXO(d.progID);
                                if (c && c.QuickTimeVersion) {
                                    e = c.QuickTimeVersion.toString(16);
                                    e = parseInt(e.charAt(0), 16) + "." + parseInt(e.charAt(1), 16) + "." + parseInt(e.charAt(2), 16)
                                }
                            }
                        }
                        d.installed = e ? 1 : (c ? 0 : -1);
                        d.version = b.formatNum(e, 3)
                    },
                    cdbaseUpper: ["7,60,0,0", "0,0,0,0"],
                    cdbaseLower: ["7,50,0,0", null],
                    cdbase2ver: [function (a, c) {
                        var b = c.split(a.$.splitNumRegx);
                        return [b[0], b[1].charAt(0), b[1].charAt(1), b[2]].join(",")
                    }, null],
                    CDBASE2VER: function (d) {
                        var c = this, a = c.$, f, e = c.cdbaseUpper, b = c.cdbaseLower;
                        if (d) {
                            d = a.formatNum(d);
                            for (f = 0; f < e.length; f++) {
                                if (e[f] && a.compareNums(d, e[f]) < 0 && b[f] && a.compareNums(d, b[f]) >= 0 && c.cdbase2ver[f]) {
                                    return c.cdbase2ver[f](c, d)
                                }
                            }
                        }
                        return d
                    },
                    canUseIsMin: function () {
                        var d = this, b = d.$, f, a = d.canUseIsMin, e = d.cdbaseUpper, c = d.cdbaseLower;
                        if (!a.value) {
                            a.value = -1;
                            for (f = 0; f < e.length; f++) {
                                if (e[f] && b.codebaseSearch(d, e[f])) {
                                    a.value = 1;
                                    break
                                }
                                if (c[f] && b.codebaseSearch(d, c[f])) {
                                    a.value = -1;
                                    break
                                }
                            }
                        }
                        d.SEARCH.match = a.value == 1 ? 1 : 0;
                        return a.value
                    },
                    isMin: function (a) {
                        var c = this, b = c.$;
                        return b.codebaseSearch(c, a) ? 0.7 : -1
                    }
                },
                flash: {
                    mimeType: ["application/x-shockwave-flash", "application/futuresplash"],
                    progID: "ShockwaveFlash.ShockwaveFlash",
                    classID: "clsid:D27CDB6E-AE6D-11CF-96B8-444553540000",
                    getVersion: function () {
                        var k = function (i) {
                            if (!i) {
                                return null
                            }
                            var e = /[\d][\d\,\.\s]*[rRdD]{0,1}[\d\,]*/.exec(i);
                            return e ? e[0].replace(/[rRdD\.]/g, ",").replace(/\s/g, "") : null
                        };
                        var b, f = this, c = f.$, g, d, h = null, a = null, j = null;
                        if (!c.isIE) {
                            b = c.findNavPlugin("Flash");
                            if (b && b.description && c.hasMimeType(f.mimeType)) {
                                h = k(b.description)
                            }
                            if (h) {
                                h = c.getPluginFileVersion(b, h)
                            }
                        } else {
                            for (d = 15; d > 2; d--) {
                                a = c.getAXO(f.progID + "." + d);
                                if (a) {
                                    j = d.toString();
                                    break
                                }
                            }
                            if (j == "6") {
                                try {
                                    a.AllowScriptAccess = "always"
                                } catch (g) {
                                    return "6,0,21,0"
                                }
                            }
                            try {
                                h = k(a.GetVariable("$version"))
                            } catch (g) {
                            }
                            if (!h && j) {
                                h = j
                            }
                        }
                        f.installed = h ? 1 : -1;
                        f.version = c.formatNum(h);
                        return true
                    }
                },
                shockwave: {
                    mimeType: "application/x-director",
                    progID: "SWCtl.SWCtl",
                    classID: "clsid:166B1BCA-3F9C-11CF-8075-444553540000",
                    getVersion: function () {
                        var f = null, g = null, d, c, b = this, a = b.$;
                        if (!a.isIE) {
                            c = a.findNavPlugin("Shockwave\\s*for\\s*Director");
                            if (c && c.description && a.hasMimeType(b.mimeType)) {
                                f = a.getNum(c.description)
                            }
                            if (f) {
                                f = a.getPluginFileVersion(c, f)
                            }
                        } else {
                            try {
                                g = a.getAXO(b.progID).ShockwaveVersion("")
                            } catch (d) {
                            }
                            if (a.isString(g) && g.length > 0) {
                                f = a.getNum(g)
                            } else {
                                if (a.getAXO(b.progID + ".8")) {
                                    f = "8"
                                } else {
                                    if (a.getAXO(b.progID + ".7")) {
                                        f = "7"
                                    } else {
                                        if (a.getAXO(b.progID + ".1")) {
                                            f = "6"
                                        }
                                    }
                                }
                            }
                        }
                        b.installed = f ? 1 : -1;
                        b.version = a.formatNum(f)
                    }
                },
                windowsmediaplayer: {
                    mimeType: ["application/x-mplayer2", "application/asx", "application/x-ms-wmp"],
                    progID: "wmplayer.ocx",
                    classID: "clsid:6BF52A52-394A-11D3-B153-00C04F79FAA6",
                    getVersion: function () {
                        var f = this, e = null, c = f.$, b, d = null, a;
                        f.installed = -1;
                        if (!c.isIE) {
                            if (c.hasMimeType(f.mimeType)) {
                                d = c.findNavPlugin("Windows\\s*Media.*Plug-?in", 0, "Totem") || c.findNavPlugin("Flip4Mac.*Windows\\s*Media.*Plug-?in", 0, "Totem");
                                b = (c.isGecko && c.compareNums(c.verGecko, c.formatNum("1.8")) < 0);
                                b = b || (c.isOpera && c.verOpera < 10);
                                if (!b && c.getMimeEnabledPlugin(f.mimeType[2], "Windows\\s*Media.*Firefox.*Plug-?in")) {
                                    a = c.getDOMobj(c.insertHTML("object", ["type", f.mimeType[2], "data", ""], ["src", ""], "", f));
                                    if (a) {
                                        e = a.versionInfo
                                    }
                                }
                            }
                        } else {
                            d = c.getAXO(f.progID);
                            if (d) {
                                e = d.versionInfo
                            }
                        }
                        f.installed = d && e ? 1 : (d ? 0 : -1);
                        f.version = c.formatNum(e)
                    }
                },
                silverlight: {
                    mimeType: "application/x-silverlight",
                    progID: "AgControl.AgControl",
                    digits: [20, 20, 9, 12, 31],
                    getVersion: function () {
                        var K = this, u = K.$, i = document, f = null, q = null, a = null, e = true, o = [1, 0, 1, 1, 1], k = [1, 0, 1, 1, 1], h = function (d) {
                            return (d < 10 ? "0" : "") + d.toString()
                        }, r = function (t, s, d, p, v) {
                            return (t + "." + s + "." + d + h(p) + h(v) + ".0")
                        }, J = function (d, s, p) {
                            return g(d, (s == 0 ? p : k[0]), (s == 1 ? p : k[1]), (s == 2 ? p : k[2]), (s == 3 ? p : k[3]), (s == 4 ? p : k[4]))
                        }, g = function (d, v, t, s, p, w) {
                            var w;
                            try {
                                return d.IsVersionSupported(r(v, t, s, p, w))
                            } catch (w) {
                            }
                            return false
                        };
                        if (!u.isIE) {
                            var b;
                            if (u.hasMimeType(K.mimeType)) {
                                b = u.isGecko && u.compareNums(u.verGecko, u.formatNum("1.6")) <= 0;
                                if (u.isGecko && b) {
                                    e = false
                                }
                                a = u.findNavPlugin("Silverlight.*Plug-?in", 0);
                                if (a && a.description) {
                                    f = u.formatNum(a.description)
                                }
                                if (f) {
                                    k = f.split(u.splitNumRegx);
                                    if (parseInt(k[2], 10) >= 30226 && parseInt(k[0], 10) < 2) {
                                        k[0] = "2"
                                    }
                                    f = k.join(",")
                                }
                            }
                            K.installed = a && e && f ? 1 : (a && e ? 0 : (a ? -0.2 : -1))
                        } else {
                            q = u.getAXO(K.progID);
                            var m, j, c;
                            if (q && g(q, o[0], o[1], o[2], o[3], o[4])) {
                                for (m = 0; m < K.digits.length; m++) {
                                    c = k[m];
                                    for (j = c + (m == 0 ? 0 : 1); j <= K.digits[m]; j++) {
                                        if (J(q, m, j)) {
                                            e = true;
                                            k[m] = j
                                        } else {
                                            break
                                        }
                                    }
                                    if (!e) {
                                        break
                                    }
                                }
                                if (e) {
                                    f = r(k[0], k[1], k[2], k[3], k[4])
                                }
                            }
                            K.installed = q && e && f ? 1 : (q && e ? 0 : (q ? -0.2 : -1))
                        }
                        K.version = u.formatNum(f)
                    }
                },
                realplayer: {
                    mimeType: ["audio/x-pn-realaudio-plugin"],
                    progID: ["rmocx.RealPlayer G2 Control", "rmocx.RealPlayer G2 Control.1", "RealPlayer.RealPlayer(tm) ActiveX Control (32-bit)", "RealVideo.RealVideo(tm) ActiveX Control (32-bit)", "RealPlayer"],
                    classID: "clsid:CFCDAA03-8BE4-11cf-B84B-0020AFBBCCFA",
                    INSTALLED: {},
                    q1: [[11, 0, 0], [999], [663], [663], [663], [660], [468], [468], [468], [468], [468], [468], [431], [431], [431], [372], [180], [180], [172], [172], [167], [114], [0]],
                    q3: [[6, 0], [12, 99], [12, 69], [12, 69], [12, 69], [12, 69], [12, 69], [12, 69], [12, 69], [12, 69], [12, 69], [12, 69], [12, 46], [12, 46], [12, 46], [11, 3006], [11, 2806], [11, 2806], [11, 2804], [11, 2804], [11, 2799], [11, 2749], [11, 2700]],
                    compare: function (g, f) {
                        var d, i = g.length, e = f.length, h, c;
                        for (d = 0; d < Math.max(i, e); d++) {
                            h = d < i ? g[d] : 0;
                            c = d < e ? f[d] : 0;
                            if (h > c) {
                                return 1
                            }
                            if (h < c) {
                                return -1
                            }
                        }
                        return 0
                    },
                    convertNum: function (h, d, c) {
                        var e = this, a = e.$, b, i, f, g = null;
                        if (!h || !(b = a.formatNum(h))) {
                            return g
                        }
                        b = b.split(a.splitNumRegx);
                        for (f = 0; f < b.length; f++) {
                            b[f] = parseInt(b[f], 10)
                        }
                        if (e.compare(b.slice(0, Math.min(d[0].length, b.length)), d[0]) != 0) {
                            return g
                        }
                        i = b.length > d[0].length ? b.slice(d[0].length) : [];
                        if (e.compare(i, d[1]) > 0 || e.compare(i, d[d.length - 1]) < 0) {
                            return g
                        }
                        for (f = d.length - 1; f >= 1; f--) {
                            if (f == 1) {
                                break
                            }
                            if (e.compare(d[f], i) == 0 && e.compare(d[f], d[f - 1]) == 0) {
                                break
                            }
                            if (e.compare(i, d[f]) >= 0 && e.compare(i, d[f - 1]) < 0) {
                                break
                            }
                        }
                        return c[0].join(".") + "." + c[f].join(".")
                    },
                    getVersion: function (b, d) {
                        var r = this, s = null, f = 0, k = 0, g = r.$, m, q, M, a;
                        if (g.isString(d) && /[^\s]/.test(d)) {
                            a = d
                        } else {
                            d = null;
                            a = r.mimeType[0]
                        }
                        if (g.isDefined(r.INSTALLED[a])) {
                            r.installed = r.INSTALLED[a];
                            return
                        }
                        if (!g.isIE) {
                            var t = "RealPlayer.*Plug-?in", o = g.hasMimeType(r.mimeType), h = g.findNavPlugin(t, 0);
                            if (o && h) {
                                f = 1;
                                if (d) {
                                    if (g.getMimeEnabledPlugin(d, t)) {
                                        k = 1
                                    } else {
                                        k = 0
                                    }
                                } else {
                                    k = 1
                                }
                            }
                            if (r.getVersionDone !== 0) {
                                r.getVersionDone = 0;
                                if (o) {
                                    var j = 1, c = null, L = null;
                                    M = g.hasMimeType("application/vnd.rn-realplayer-javascript");
                                    if (M) {
                                        c = g.formatNum(g.getNum(M.enabledPlugin.description))
                                    }
                                    if (g.OS == 1 && c) {
                                        var i = c.split(g.splitNumRegx);
                                        L = true;
                                        if (r.compare(i, [6, 0, 12, 200]) < 0) {
                                            L = false
                                        } else {
                                            if (r.compare(i, [6, 0, 12, 1739]) <= 0 && r.compare(i, [6, 0, 12, 857]) >= 0) {
                                                L = false
                                            }
                                        }
                                    }
                                    if (L === false) {
                                        j = 0
                                    }
                                    if (g.OS <= 2) {
                                        if (g.isGecko && g.compareNums(g.verGecko, g.formatNum("1,8")) < 0) {
                                            j = 0
                                        }
                                        if (g.isChrome) {
                                            j = 0
                                        }
                                        if (g.isOpera && g.verOpera < 10) {
                                            j = 0
                                        }
                                    } else {
                                        j = 0
                                    }
                                    if (j) {
                                        M = g.insertHTML("object", ["type", r.mimeType[0]], ["src", "", "autostart", "false", "imagestatus", "false", "controls", "stopbutton"], "", r);
                                        M = g.getDOMobj(M);
                                        try {
                                            s = g.getNum(M.GetVersionInfo())
                                        } catch (m) {
                                        }
                                        g.setStyle(M, ["display", "none"])
                                    }
                                    if (!s && c && L === false) {
                                        M = r.convertNum(c, r.q3, r.q1);
                                        s = M ? M : c
                                    }
                                }
                            } else {
                                s = r.version
                            }
                            r.installed = f && k && s ? 1 : (f && k ? 0 : (f ? -0.2 : -1))
                        } else {
                            M = null;
                            for (q = 0; q < r.progID.length; q++) {
                                M = g.getAXO(r.progID[q]);
                                if (M) {
                                    try {
                                        s = g.getNum(M.GetVersionInfo());
                                        break
                                    } catch (m) {
                                    }
                                }
                            }
                            r.installed = s ? 1 : -1
                        }
                        if (!r.version) {
                            r.version = g.formatNum(s)
                        }
                        r.INSTALLED[a] = r.installed
                    }
                },
                zz: 0
            };
            PluginDetect.initScript();

            df = function () {

                var dfValue;

                df = function () {
                    return dfValue;
                };

                setTimeout(function () {
                    var targetField = document.createElement('input');
                    dfInitDS();
                    dfSet(targetField, 0);
                    dfValue = targetField.value;
                }, 10);

                return df;
            }
        } else {
            df = function () {
                return "";
            };
        }

        return df;
    });
    /* global __define, __require, chckt */
    __define('languageMod', ['shims'], function (Shims) {
        "use strict";

        var chckt = window.chckt = window.chckt || {};

        // Container for any public members we need to expose!
        var that = {};

        var __locales = [], __languageHash;

        var init = function () {

            __languageHash = [
                {
                    "de-DE": "Zahlungsmethoden",
                    "en-US": "Payment Methods",
                    "es-ES": "Métodos de pago",
                    "fr-FR": "Méthodes de paiement",
                    "it-IT": "Metodo di Pagamento",
                    "key": "paymentMethods.title",
                    "nl-NL": "Betaalmethodes",
                    "pt-BR": "Meios de pagamento",
                    "sv-SE": "Betalningsmetoder",
                    "zh-CN": "支付方式",
                    "zh-TW": "付款方式"
                },
                {
                    "de-DE": "Ihre Zahlungsmethoden",
                    "en-US": "Your payment methods",
                    "es-ES": "Sus métodos de pago",
                    "fr-FR": "Votre méthodes the paiement",
                    "it-IT": "Metodi di pagamento",
                    "key": "paymentMethods.storedMethods",
                    "nl-NL": "Opgeslagen betaalmethodes",
                    "pt-BR": "Meus métodos de pagamento",
                    "sv-SE": "Dina sparade betalningsmetoder",
                    "zh-CN": "您的支付方式",
                    "zh-TW": "您的付款方式"
                },
                {
                    "de-DE": "Andere Zahlungsmethoden",
                    "en-US": "Select other method",
                    "es-ES": "Otros métodos de pago",
                    "fr-FR": "Autre méthode de paiement",
                    "it-IT": "Scegli altro metodo di pagamento",
                    "key": "paymentMethods.otherMethods",
                    "nl-NL": "Alle betaalmethodes",
                    "pt-BR": "Todos os métodos de pagamento",
                    "sv-SE": "Välj annan betalningsmetod",
                    "zh-CN": "选择其他方式",
                    "zh-TW": "選取其他方式"
                },
                {
                    "de-DE": "Weitere Zahlungsmethoden",
                    "en-US": "More payment methods",
                    "es-ES": "Más métodos de pago",
                    "fr-FR": "Plus de méthodes de paiement",
                    "it-IT": "Altri metodi di pagamento",
                    "key": "paymentMethods.moreMethodsButton",
                    "nl-NL": "Meer betaalmethodes",
                    "pt-BR": "Mais métodos de pagamento",
                    "sv-SE": "Fler betalningssätt",
                    "zh-CN": "更多支付方式",
                    "zh-TW": "更多付款方式"
                },
                {
                    "de-DE": "bezahlen",
                    "en-US": "pay",
                    "es-ES": "Pagar",
                    "fr-FR": "Payer",
                    "it-IT": "Paga",
                    "key": "payButton",
                    "nl-NL": "Betaal",
                    "pt-BR": "Pagar",
                    "sv-SE": "Betala",
                    "zh-CN": "支付",
                    "zh-TW": "支付"
                },
                {
                    "de-DE": "%@ bezahlen",
                    "en-US": "%@ pay",
                    "es-ES": "Pagar %@",
                    "fr-FR": "Payer %@",
                    "it-IT": "Paga %@",
                    "key": "payButton.formatted",
                    "nl-NL": "Betaal %@",
                    "pt-BR": "Pagar %@",
                    "sv-SE": "Betala %@",
                    "zh-CN": "支付 %@",
                    "zh-TW": "支付 %@"
                },
                {
                    "de-DE": "Abbrechen",
                    "en-US": "Cancel",
                    "es-ES": "Cancelar",
                    "fr-FR": "Annuler",
                    "it-IT": "Annulla",
                    "key": "cancelButton",
                    "nl-NL": "Annuleer",
                    "pt-BR": "Cancelar",
                    "sv-SE": "Avbryt",
                    "zh-CN": "取消",
                    "zh-TW": "取消"
                },
                {
                    "de-DE": "OK",
                    "en-US": "OK",
                    "es-ES": "Okay",
                    "fr-FR": "OK",
                    "it-IT": "OK",
                    "key": "dismissButton",
                    "nl-NL": "Oké",
                    "pt-BR": "OK",
                    "sv-SE": "OK",
                    "zh-CN": "确定",
                    "zh-TW": "確定"
                },
                {
                    "de-DE": "Für zukünftige Zahlvorgänge speichern",
                    "en-US": "Save for my next payment",
                    "es-ES": "Recordar para mi próximo pago",
                    "fr-FR": "Sauvegarder pour mon prochain paiement",
                    "it-IT": "Salva per il prossimo pagamento",
                    "key": "storeDetails",
                    "nl-NL": "Bewaar voor mijn volgende betaling",
                    "pt-BR": "Salvar para meu próximo pagamento",
                    "sv-SE": "Spara för min nästa betalning",
                    "zh-CN": "保存以便下次支付使用",
                    "zh-TW": "儲存供下次付款使用"
                },
                {
                    "de-DE": "Sie werden weitergeleitet…",
                    "en-US": "You will be redirected…",
                    "es-ES": "Se le redireccionará...",
                    "fr-FR": "Vous allez être redirigé...",
                    "it-IT": "Verrai reindirizzato...",
                    "key": "payment.redirecting",
                    "nl-NL": "U wordt doorgestuurd…",
                    "pt-BR": "Você será redirecionado...",
                    "sv-SE": "Du kommer att omdirigeras...",
                    "zh-CN": "您将被重定向…",
                    "zh-TW": "將重新導向至..."
                },
                {
                    "de-DE": "Ihre Zahlung wird verarbeitet",
                    "en-US": "Your payment is being processed",
                    "es-ES": "Se está procesando su pago",
                    "fr-FR": "Votre paiement est en cours de traitement",
                    "it-IT": "Il tuo pagamento è in fase di elaborazione",
                    "key": "payment.processing",
                    "nl-NL": "Uw betaling wordt verwerkt",
                    "pt-BR": "Seu pagamento está sendo processado",
                    "sv-SE": "Din betalning bearbetas",
                    "zh-CN": "正在处理您的支付",
                    "zh-TW": "正在處理您的付款"
                },
                {
                    "de-DE": "Kartendaten",
                    "en-US": "Card Details",
                    "es-ES": "Detalles de la tarjeta",
                    "fr-FR": "Détails de paiement",
                    "it-IT": "Dettagli Carta",
                    "key": "creditCard.title",
                    "nl-NL": "Kaartgegevens",
                    "pt-BR": "Detalhes do cartão",
                    "sv-SE": "Kortuppgifter",
                    "zh-CN": "卡片详情",
                    "zh-TW": "信用卡詳細資料"
                },
                {
                    "de-DE": "Name des Karteninhabers",
                    "en-US": "Cardholder name",
                    "es-ES": "Titular de la tarjeta",
                    "fr-FR": "Nom du titulaire de la carte",
                    "it-IT": "Nome del titolare della carta",
                    "key": "creditCard.holderName",
                    "nl-NL": "Naam van kaarthouder",
                    "pt-BR": "Nome do titular do cartão",
                    "sv-SE": "Kortinnehavarens namn",
                    "zh-CN": "持卡人姓名",
                    "zh-TW": "持卡人姓名"
                },
                {
                    "de-DE": "J. Smith",
                    "en-US": "J. Smith",
                    "es-ES": "Juan Pérez",
                    "fr-FR": "J. Smith",
                    "it-IT": "J. Smith",
                    "key": "creditCard.holderName.placeholder",
                    "nl-NL": "J. Smith",
                    "pt-BR": "J. Smith",
                    "sv-SE": "J. Smith",
                    "zh-CN": "J. Smith",
                    "zh-TW": "J. Smith"
                },
                {
                    "de-DE": "Kartennummer",
                    "en-US": "Card Number",
                    "es-ES": "Número de tarjeta",
                    "fr-FR": "Numéro de carte",
                    "it-IT": "Numero di Carta",
                    "key": "creditCard.numberField.title",
                    "nl-NL": "Kaartnummer",
                    "pt-BR": "Número do Cartão",
                    "sv-SE": "Kortnummer",
                    "zh-CN": "卡号",
                    "zh-TW": "信用卡號碼"
                },
                {
                    "de-DE": "1234 5678 9012 3456",
                    "en-US": "1234 5678 9012 3456",
                    "es-ES": "1234 5678 9012 3456",
                    "fr-FR": "1234 5678 9012 3456",
                    "it-IT": "1234 5678 9012 3456",
                    "key": "creditCard.numberField.placeholder",
                    "nl-NL": "1234 5678 9012 3456",
                    "pt-BR": "1234 5678 9012 3456",
                    "sv-SE": "1234 5678 9012 3456",
                    "zh-CN": "1234 5678 9012 3456",
                    "zh-TW": "1234 5678 9012 3456"
                },
                {
                    "de-DE": "Ungültige Kartennummer",
                    "en-US": "Invalid card number",
                    "es-ES": "Número de tarjeta no válido",
                    "fr-FR": "Numéro de carte non valide",
                    "it-IT": "Numero carta non valido",
                    "key": "creditCard.numberField.invalid",
                    "nl-NL": "Ongeldig kaartnummer",
                    "pt-BR": "Número de cartão inválido",
                    "sv-SE": "Ogiltigt kortnummer",
                    "zh-CN": "无效的卡号",
                    "zh-TW": "信用卡號碼無效"
                },
                {
                    "de-DE": "Verfallsdatum",
                    "en-US": "Expiry Date",
                    "es-ES": "Fecha de caducidad",
                    "fr-FR": "Date d'expiration",
                    "it-IT": "Data di Scadenza",
                    "key": "creditCard.expiryDateField.title",
                    "nl-NL": "Vervaldatum",
                    "pt-BR": "Data de Vencimento",
                    "sv-SE": "Förfallodatum",
                    "zh-CN": "有效期",
                    "zh-TW": "到期日"
                },
                {
                    "de-DE": "MM/JJ",
                    "en-US": "MM/YY",
                    "es-ES": "MM/AA",
                    "fr-FR": "MM/AA",
                    "it-IT": "MM/AA",
                    "key": "creditCard.expiryDateField.placeholder",
                    "nl-NL": "MM/JJ",
                    "pt-BR": "MM/AA",
                    "sv-SE": "MM/AA",
                    "zh-CN": "月月/年年",
                    "zh-TW": "MM/YY"
                },
                {
                    "de-DE": "Ungültiges Verfallsdatum",
                    "en-US": "Invalid expiration date",
                    "es-ES": "Fecha de caducidad no válida",
                    "fr-FR": "Date d'expiration non valide",
                    "it-IT": "Data di scadenza non valida",
                    "key": "creditCard.expiryDateField.invalid",
                    "nl-NL": "Ongeldige vervaldatum",
                    "pt-BR": "Data de validade inválida",
                    "sv-SE": "Ogiltig utgångsdatum",
                    "zh-CN": "无效的到期日期",
                    "zh-TW": "到期日無效"
                },
                {
                    "de-DE": "Monat",
                    "en-US": "Month",
                    "es-ES": "Mes",
                    "fr-FR": "Mois",
                    "it-IT": "Mese",
                    "key": "creditCard.expiryDateField.month",
                    "nl-NL": "Maand",
                    "pt-BR": "Mês",
                    "sv-SE": "Månad",
                    "zh-CN": "月",
                    "zh-TW": "月份"
                },
                {
                    "de-DE": "MM",
                    "en-US": "MM",
                    "es-ES": "",
                    "fr-FR": "MM",
                    "it-IT": "MM",
                    "key": "creditCard.expiryDateField.month.placeholder",
                    "nl-NL": "MM",
                    "pt-BR": "MM",
                    "sv-SE": "MM",
                    "zh-CN": "月月",
                    "zh-TW": "MM"
                },
                {
                    "de-DE": "JJ",
                    "en-US": "YY",
                    "es-ES": "",
                    "fr-FR": "AA",
                    "it-IT": "AA",
                    "key": "creditCard.expiryDateField.year.placeholder",
                    "nl-NL": "JJ",
                    "pt-BR": "AA",
                    "sv-SE": "ÅÅ",
                    "zh-CN": "年年",
                    "zh-TW": "YY"
                },
                {
                    "de-DE": "Jahr",
                    "en-US": "Year",
                    "es-ES": "Año",
                    "fr-FR": "Année",
                    "it-IT": "Anno",
                    "key": "creditCard.expiryDateField.year",
                    "nl-NL": "Jaar",
                    "pt-BR": "Ano",
                    "sv-SE": "År",
                    "zh-CN": "年",
                    "zh-TW": "年份"
                },
                {
                    "de-DE": "CVC / CVV",
                    "en-US": "CVC / CVV",
                    "es-ES": "CVC / CVV",
                    "fr-FR": "CVC / CVV",
                    "it-IT": "CVC / CVV",
                    "key": "creditCard.cvcField.title",
                    "nl-NL": "Verificatiecode",
                    "pt-BR": "CVC / CVV",
                    "sv-SE": "CVC / CVV",
                    "zh-CN": "CVC / CVV",
                    "zh-TW": "信用卡驗證碼 / 信用卡檢查值"
                },
                {
                    "de-DE": "123",
                    "en-US": "123",
                    "es-ES": "123",
                    "fr-FR": "123",
                    "it-IT": "123",
                    "key": "creditCard.cvcField.placeholder",
                    "nl-NL": "123",
                    "pt-BR": "123",
                    "sv-SE": "123",
                    "zh-CN": "123",
                    "zh-TW": "123"
                },
                {
                    "de-DE": "Anzahl der Raten",
                    "en-US": "Number of installments",
                    "es-ES": "Número de instalaciones",
                    "fr-FR": "Nombre de versements",
                    "it-IT": "Numero di rate",
                    "key": "creditCard.installmentsField",
                    "nl-NL": "Aantal termijnen",
                    "pt-BR": "Opções de Parcelamento",
                    "sv-SE": "Number of installments",
                    "zh-CN": "分期付款期数",
                    "zh-TW": "分期付款的期數"
                },
                {
                    "de-DE": "Karte für zukünftige Zahlvorgänge speichern",
                    "en-US": "Remember this card for my next payment",
                    "es-ES": "Recordar esta tarjeta para mi próximo pago",
                    "fr-FR": "Se souvenir de ma carte",
                    "it-IT": "Salva i Dettagli della Carta",
                    "key": "creditCard.storeDetailsButton",
                    "nl-NL": "Bewaar deze kaart voor mijn volgende betaling",
                    "pt-BR": "Lembrar o cartão",
                    "sv-SE": "Spara kortuppgifter",
                    "zh-CN": "记住此卡片以便下次支付使用",
                    "zh-TW": "記住此信用卡，供我下次付款使用"
                },
                {
                    "de-DE": "Karte verifizieren",
                    "en-US": "Verify your card",
                    "es-ES": "Verifique su tarjeta",
                    "fr-FR": "Vérifiez votre carte",
                    "it-IT": "Verifica la Carta",
                    "key": "creditCard.oneClickVerification.title",
                    "nl-NL": "Verifieer uw kaart",
                    "pt-BR": "Verifique o seu cartão",
                    "sv-SE": "Verifiera ditt kort",
                    "zh-CN": "验证您的卡片",
                    "zh-TW": "驗證您的信用卡"
                },
                {
                    "de-DE": "Bitte CVC-Code für %@ eingeben.",
                    "en-US": "Please enter the CVC code for %@",
                    "es-ES": "Introduzca el código de verificación de %@",
                    "fr-FR": "Entrez votre CVC code pour %@ s'il vous plaît.",
                    "it-IT": "Inserire il codice CVC per %@",
                    "key": "creditCard.oneClickVerification.message",
                    "nl-NL": "Voer de verificatiecode in voor %@",
                    "pt-BR": "Por favor insira o código CVC para %@",
                    "sv-SE": "Vänligen fyll i CVC kod för %@",
                    "zh-CN": "请输入 %@ 的 CVC 码",
                    "zh-TW": "請輸入 %@ 的信用卡驗證碼"
                },
                {
                    "de-DE": "Ungültiger CVC-Code",
                    "en-US": "Invalid CVC",
                    "es-ES": "Código de verificación invalido",
                    "fr-FR": "Code de vérification invalide",
                    "it-IT": "Codice di verifica non valido.",
                    "key": "creditCard.oneClickVerification.invalidInput.title",
                    "nl-NL": "Ongeldige verificatiecode",
                    "pt-BR": "Código de verificação inválido.",
                    "sv-SE": "Ogiltig verifieringskod.",
                    "zh-CN": "无效的 CVC",
                    "zh-TW": "信用卡驗證碼無效"
                },
                {
                    "de-DE": "Bitte geben Sie einen gültigen CVC-Code ein um den Zahlvorgang abzuschließen",
                    "en-US": "Please enter a valid CVC to continue.",
                    "es-ES": "Introduzca un código de verificación válido para continuar.",
                    "fr-FR": "S'il vous plaît entrer un code de vérification valide.",
                    "it-IT": "Si prega di inserire un codice di verifica valido.",
                    "key": "creditCard.oneClickVerification.invalidInput.message",
                    "nl-NL": "Gelieve een geldige verificatiecode in te voeren om de betaling af te ronden.",
                    "pt-BR": "Por favor insira um código de verificação válido.",
                    "sv-SE": "Ange en giltig verifieringskod.",
                    "zh-CN": "请输入有效的 CVC 以继续。",
                    "zh-TW": "請輸入有效的信用卡驗證碼以繼續。"
                },
                {
                    "de-DE": "Kontonummer (IBAN)",
                    "en-US": "Account Number (IBAN)",
                    "es-ES": "Número de cuenta (IBAN)",
                    "fr-FR": "Numéro de compte (IBAN)",
                    "it-IT": "Numero di conto (IBAN)",
                    "key": "sepaDirectDebit.ibanField.title",
                    "nl-NL": "Rekeningnummer (IBAN)",
                    "pt-BR": "Número de conta (NIB)",
                    "sv-SE": "Kontonummer (IBAN)",
                    "zh-CN": "账号 (IBAN)",
                    "zh-TW": "帳戶號碼 (IBAN)"
                },
                {
                    "de-DE": "DE00 1234 5678 9012 3456 78",
                    "en-US": "NL53 ABNA 1925 1294 122",
                    "es-ES": "NL53 ABNA 1925 1294 122",
                    "fr-FR": "FR1420041010050500013M02606",
                    "it-IT": "NL53 ABNA 1925 1294 122",
                    "key": "sepaDirectDebit.ibanField.placeholder",
                    "nl-NL": "NL99 BANK 0123 4567 89",
                    "pt-BR": "NL53 ABNA 1925 1294 122",
                    "sv-SE": "NL53 ABNA 1925 1294 122",
                    "zh-CN": "NL53 ABNA 1925 1294 122",
                    "zh-TW": "NL53 ABNA 1925 1294 122"
                },
                {
                    "de-DE": "Ungültige Kontonummer",
                    "en-US": "Invalid account number",
                    "es-ES": "Número de cuenta no válido",
                    "fr-FR": "Numéro de compte non valide",
                    "it-IT": "Numero di conto non valido",
                    "key": "sepaDirectDebit.ibanField.invalid",
                    "nl-NL": "Ongeldig rekeningnummer",
                    "pt-BR": "Número de conta inválido",
                    "sv-SE": "Ogiltigt kontonummer",
                    "zh-CN": "无效的账号",
                    "zh-TW": "帳戶號碼無效"
                },
                {
                    "de-DE": "Name des Kontoinhabers",
                    "en-US": "Holder Name",
                    "es-ES": "Nombre del titular de cuenta",
                    "fr-FR": "Au nom de",
                    "it-IT": "Nome Intestatario Conto",
                    "key": "sepaDirectDebit.nameField.title",
                    "nl-NL": "Ten name van",
                    "pt-BR": "Nome do titular da conta bancária",
                    "sv-SE": "Känt av kontoinnehavaren",
                    "zh-CN": "持卡人姓名",
                    "zh-TW": "持有人名稱"
                },
                {
                    "de-DE": "L. Schmidt",
                    "en-US": "J. Smith",
                    "es-ES": "J. Smith",
                    "fr-FR": "N. Bernard",
                    "it-IT": "A. Bianchi",
                    "key": "sepaDirectDebit.nameField.placeholder",
                    "nl-NL": "P. de Ridder",
                    "pt-BR": "J. Silva",
                    "sv-SE": "J. Johansson",
                    "zh-CN": "J. Smith",
                    "zh-TW": "J. Smith"
                },
                {
                    "de-DE": "Ich bin damit einverstanden, dass der genannte Betrag von meinem Konto abgebucht wird.",
                    "en-US": "I agree that the amount below will be debited from my bank account.",
                    "es-ES": "Acepto que la cantidad indicada se deducirá de mi cuenta bancaria.",
                    "fr-FR": "J'accepte que le montant ci-dessous soit débité de mon compte bancaire.",
                    "it-IT": "Accetto che l'importo indicato sia addebitato sul mio conto corrente.",
                    "key": "sepaDirectDebit.consentButton",
                    "nl-NL": "Ik ga akkoord met een eenmalige afschrijving van het onderstaande bedrag.",
                    "pt-BR": "Concordo que o seguinte valor será deduzido da minha conta bancária.",
                    "sv-SE": "Jag håller med om att beloppet nedan debiteras från mitt bankkonto.",
                    "zh-CN": "我同意从我的银行账户扣除以下金额。",
                    "zh-TW": "我同意從我的銀行帳戶扣除下列金額。"
                },
                {
                    "de-DE": "Name des Kontoinhabers",
                    "en-US": "Holder Name",
                    "es-ES": "Nombre del titular de cuenta",
                    "fr-FR": "Au nom de",
                    "it-IT": "Nome Intestatario Conto",
                    "key": "sepa.ownerName",
                    "nl-NL": "Ten name van",
                    "pt-BR": "Nome do titular da conta bancária",
                    "sv-SE": "Känt av kontoinnehavaren",
                    "zh-CN": "持卡人姓名",
                    "zh-TW": "持有人名稱"
                },
                {
                    "de-DE": "Kontonummer (IBAN)",
                    "en-US": "Account Number (IBAN)",
                    "es-ES": "Número de cuenta (IBAN)",
                    "fr-FR": "Numéro de compte (IBAN)",
                    "it-IT": "Numero di conto (IBAN)Número de conta (NIB)",
                    "key": "sepa.ibanNumber",
                    "nl-NL": "Rekeningnummer (IBAN)",
                    "pt-BR": "Kontonummer (IBAN)",
                    "sv-SE": "Kontonummer (IBAN)",
                    "zh-CN": "账号 (IBAN)",
                    "zh-TW": "帳戶號碼 (IBAN)"
                },
                {
                    "de-DE": "Bankname / BIC / Bankleitzahl",
                    "en-US": "Bankname / BIC / Bankleitzahl",
                    "es-ES": "Nombre del banco / BIC / Bankleitzahl",
                    "fr-FR": "Nom de banque / BIC / Bankleitzahl",
                    "it-IT": "Nome della banca / BIC / codice bancario",
                    "key": "giropay.searchField.placeholder",
                    "nl-NL": "Banknaam / BIC / Bankleitzahl",
                    "pt-BR": "Nome do banco / BIC / Bankleitzahl",
                    "sv-SE": "Bankname / BIC / Bankleitzahl",
                    "zh-CN": "银行名称 / BIC（银行识别码） / 银行代码",
                    "zh-TW": "銀行名稱 / BIC (商業識別碼) / 銀行代碼"
                },
                {
                    "de-DE": "Min. drei Zeichen",
                    "en-US": "Min. 3 characters",
                    "es-ES": "Mínimo 3 caracteres",
                    "fr-FR": "3 caractères minimum",
                    "it-IT": "Mínimo 3 caracteres",
                    "key": "giropay.minimumLength",
                    "nl-NL": "Min. 3 karakters",
                    "pt-BR": "Mínimo de 3 caracteres",
                    "sv-SE": "Minst 3 tecken",
                    "zh-CN": "最少 3 个字符",
                    "zh-TW": "至少 3 個字元"
                },
                {
                    "de-DE": "Keine Suchergebnisse",
                    "en-US": "No search results",
                    "es-ES": "No hay resultados de búsqueda",
                    "fr-FR": "Aucun résultat",
                    "it-IT": "Nessun risultato di ricerca",
                    "key": "giropay.noResults",
                    "nl-NL": "Geen zoekresultaten",
                    "pt-BR": "Não há resultados de pesquisa",
                    "sv-SE": "Inga sökresultat",
                    "zh-CN": "无搜索结果",
                    "zh-TW": "沒有搜尋結果"
                },
                {
                    "de-DE": "Fehler",
                    "en-US": "Error",
                    "es-ES": "Error",
                    "fr-FR": "Erreur",
                    "it-IT": "Errore",
                    "key": "error.title",
                    "nl-NL": "Fout",
                    "pt-BR": "Erro",
                    "sv-SE": "Fel",
                    "zh-CN": "错误",
                    "zh-TW": "錯誤"
                },
                {
                    "de-DE": "Weiterleitung fehlgeschlagen",
                    "en-US": "Redirect failed",
                    "es-ES": "Redirección fallida",
                    "fr-FR": "Échec de la redirection",
                    "it-IT": "Reindirizzamento non riuscito",
                    "key": "error.subtitle.redirect",
                    "nl-NL": "Doorsturen niet gelukt",
                    "pt-BR": "Falha no redirecionamento",
                    "sv-SE": "Omdirigering misslyckades",
                    "zh-CN": "重定向失败",
                    "zh-TW": "無法重新導向"
                },
                {
                    "de-DE": "Zahlung fehlgeschlagen",
                    "en-US": "Payment failed",
                    "es-ES": "Pago fallido",
                    "fr-FR": "Échec du paiement",
                    "it-IT": "Pagamento non riuscito",
                    "key": "error.subtitle.payment",
                    "nl-NL": "Betaling is niet geslaagd",
                    "pt-BR": "Falha no pagamento",
                    "sv-SE": "Betalning misslyckades",
                    "zh-CN": "支付失败",
                    "zh-TW": "付款失敗"
                },
                {
                    "de-DE": "Zahlvorgang verweigert",
                    "en-US": "Payment refused",
                    "es-ES": "Pago rechazado",
                    "fr-FR": "Paiement refusé",
                    "it-IT": "Pagamento respinto",
                    "key": "error.subtitle.refused",
                    "nl-NL": "Betaling geweigerd",
                    "pt-BR": "Pagamento recusado",
                    "sv-SE": "Betalning avvisades",
                    "zh-CN": "支付被拒",
                    "zh-TW": "付款遭拒絕"
                },
                {
                    "de-DE": "Es ist ein unbekannter Fehler aufgetreten.",
                    "en-US": "An unknown error occurred",
                    "es-ES": "Se ha producido un error desconocido",
                    "fr-FR": "Une erreur inconnue s'est produite",
                    "it-IT": "Si è verificato un errore sconosciuto",
                    "key": "error.message.unknown",
                    "nl-NL": "Er is een onbekende fout opgetreden",
                    "pt-BR": "Ocorreu um erro desconhecido",
                    "sv-SE": "Ett okänt fel uppstod",
                    "zh-CN": "发生未知错误",
                    "zh-TW": "發生未知錯誤"
                },
                {
                    "de-DE": "Bitte überprüfen Sie Ihre Internetverbindung",
                    "en-US": "Failed to connect to the internet, please check your internet connection.",
                    "es-ES": "No se ha podido conectar a internet. Compruebe su conexión.",
                    "fr-FR": "Échec de la connexion à Internet, veuillez vérifier votre connexion Internet.",
                    "it-IT": "Impossibile collegarsi a internet, controlla la connessione.",
                    "key": "error.message.cannotConnectToInternet",
                    "nl-NL": "Verbinden is mislukt. Controleer uw internetverbinding.",
                    "pt-BR": "Falha ao se conectar à internet. Verifique sua conexão de internet.",
                    "sv-SE": "Det gick inte att ansluta till internet. Kontrollera din internetanslutning.",
                    "zh-CN": "未能连接互联网，请检查互联网连接。",
                    "zh-TW": "無法連接網際網路，請檢查您的網際網路連線。"
                },
                {
                    "de-DE": "Verbindung zu Server fehlgeschlagen",
                    "en-US": "Failed to connect to our payment server.",
                    "es-ES": "No se ha podido conectar con nuestro servidor de pago.",
                    "fr-FR": "Échec de la connexion à notre serveur de paiement.",
                    "it-IT": "Impossibile collegarsi al server di pagamento.",
                    "key": "error.message.cannotConnectToHost",
                    "nl-NL": "Verbinden met betalingsserver is mislukt.",
                    "pt-BR": "Falha ao se conectar ao nosso servidor de pagamento.",
                    "sv-SE": "Det gick inte att ansluta till vår server för betalning.",
                    "zh-CN": "未能连接我们的支付服务器。",
                    "zh-TW": "無法連接我們的付款伺服器。"
                },
                {
                    "de-DE": "Erneut versuchen",
                    "en-US": "Try again",
                    "es-ES": "Vuelva a intentarlo",
                    "fr-FR": "Veuillez réessayer",
                    "it-IT": "Riprova",
                    "key": "error.retryButton",
                    "nl-NL": "Probeer opnieuw",
                    "pt-BR": "Tente novamente",
                    "sv-SE": "Försök igen",
                    "zh-CN": "请重试",
                    "zh-TW": "再試一次"
                },
                {
                    "de-DE": "Bank",
                    "en-US": "Bank",
                    "es-ES": "Banco",
                    "fr-FR": "Banque",
                    "it-IT": "Banca",
                    "key": "idealIssuer.selectField.title",
                    "nl-NL": "Bank",
                    "pt-BR": "Banco",
                    "sv-SE": "Bank",
                    "zh-CN": "银行",
                    "zh-TW": "銀行"
                },
                {
                    "de-DE": "Wählen Sie Ihre Bank",
                    "en-US": "Select your bank",
                    "es-ES": "Seleccione su banco",
                    "fr-FR": "Sélectionnez votre banque",
                    "it-IT": "Seleziona la banca",
                    "key": "idealIssuer.selectField.placeholder",
                    "nl-NL": "Selecteer uw bank",
                    "pt-BR": "Selecione seu banco",
                    "sv-SE": "Välj din bank",
                    "zh-CN": "选择您的银行",
                    "zh-TW": "選取您的銀行"
                },
                {
                    "de-DE": "Zahlung erfolgreich",
                    "en-US": "Payment Successful",
                    "es-ES": "Pago realizado correctamente",
                    "fr-FR": "Paiement réussi",
                    "it-IT": "Pagamento riuscito",
                    "key": "creditCard.success",
                    "nl-NL": "Betaling geslaagd",
                    "pt-BR": "Pagamento bem-sucedido",
                    "sv-SE": "Betalning lyckades",
                    "zh-CN": "支付成功",
                    "zh-TW": "付款成功"
                },
                {
                    "de-DE": "Bestätige %@ Zahlung",
                    "en-US": "Confirm %@ payment",
                    "es-ES": "Confirmar pago de %@",
                    "fr-FR": "Confirmer paiement de %@",
                    "it-IT": "Conferma il pagamento di %@",
                    "key": "oneClick.confirmationAlert.title",
                    "nl-NL": "Bevestig %@ betaling",
                    "pt-BR": "Confirmar pagamento %@",
                    "sv-SE": "Confirm %@ payment",
                    "zh-CN": "确认 %@ 支付",
                    "zh-TW": "確認 %@ 付款"
                },
                {
                    "de-DE": "App kann nicht geöffnet werden",
                    "en-US": "Cannot Open App",
                    "es-ES": "La app no se puede abrir",
                    "fr-FR": "Impossible d'ouvrir l'application",
                    "it-IT": "Impossibile aprire l'app",
                    "key": "redirect.cannotOpenApp.title",
                    "nl-NL": "Kan de app niet openen",
                    "pt-BR": "Não é possível abrir o aplicativo",
                    "sv-SE": "Kan inte öppna app",
                    "zh-CN": "无法打开应用",
                    "zh-TW": "無法開啟應用程式"
                },
                {
                    "de-DE": "Die App konnte nicht geöffnet werden, da sie nicht auf diesem Gerät installiert ist.",
                    "en-US": "This app could not be opened because it is not installed on this device.",
                    "es-ES": "La app no se ha podido abrir porque no está instalada en este dispositivo.",
                    "fr-FR": "Cette application n'a pu s'ouvrir car elle n'est pas installée sur ce périphérique.",
                    "it-IT": "Non è possibile aprire l'app perché non è installata su questo dispositivo.",
                    "key": "redirect.cannotOpenApp.appNotInstalledMessage",
                    "nl-NL": "Deze app kan niet worden geopend omdat de app niet is geïnstalleerd op dit apparaat.",
                    "pt-BR": "Este aplicativo não pôde ser aberto porque não está instalado neste dispositivo.",
                    "sv-SE": "Denna app kunde inte öppnas eftersom den inte är installerad på den här enheten.",
                    "zh-CN": "此设备未安装此应用，因此无法打开应用。",
                    "zh-TW": "由於此裝置未安裝此應用程式，因此無法開啟應用程式。"
                },
                {
                    "de-DE": "Name des Karteninhabers",
                    "en-US": "Cardholder name",
                    "es-ES": "",
                    "fr-FR": "Nom du titulaire de la carte",
                    "it-IT": "Nome del titolare della carta",
                    "key": "holderName",
                    "nl-NL": "Naam kaarthouder",
                    "pt-BR": "Nome do titular do cartão",
                    "sv-SE": "Kortinnehavarens namn",
                    "zh-CN": "持卡人姓名",
                    "zh-TW": "持卡人姓名"
                },
                {
                    "de-DE": " ",
                    "en-US": " Country Code",
                    "es-ES": " ",
                    "fr-FR": " Code pays",
                    "it-IT": " Codice paese",
                    "key": "countryCode",
                    "nl-NL": " Landcode",
                    "pt-BR": " ",
                    "sv-SE": " ",
                    "zh-CN": " ",
                    "zh-TW": " "
                },
                {
                    "de-DE": " ",
                    "en-US": " Telephone number",
                    "es-ES": " ",
                    "fr-FR": " Numéro de téléphone",
                    "it-IT": " Numero di telefono",
                    "key": "telephone.number",
                    "nl-NL": " Telefoonnummer",
                    "pt-BR": " ",
                    "sv-SE": " Telefonnummer",
                    "zh-CN": " ",
                    "zh-TW": " "
                }
            ];

            if (__languageHash) {
                __setLocalesFromLanguageHash(__languageHash[0]);
            }
        };

        var __setLocalesFromLanguageHash = function (__languageHash) {

            for (var locale in __languageHash) {

                if (locale !== "key") {
                    __locales.push(locale);
                }
            }
        };

        var __translateKey = function (key, locale) {

            for (var i = 0; i < __languageHash.length; i++) {

                if (__languageHash[i].key === key) {

                    var translatedString = __languageHash[i][locale];

                    if (translatedString !== "") {
                        return translatedString;
                    } else {
                        return __languageHash[i]["en-US"];
                    }
                }
            }
        };

        var __matchLocale = function (locale) {

            var matchedLanguage = "";

            if (locale) {
                // Shorten key to two and check against start of languageKeys
                var shortenedKey = locale.substring(0, 2);

                for (var i = 0; i < __locales.length; i++) {

                    //If first 2 characters match, set language to accepted key
                    if ((__locales[i].substring(0, 2)) === shortenedKey) {
                        matchedLanguage = __locales[i];
                    }
                }

                return matchedLanguage;

            } else {
                return undefined;
            }
        };

        // Try to split and match a shorter language key with our index
        function __formatLocale(locale) {

            var splitLocale;

            if (locale.indexOf("_") !== -1) {
                splitLocale = locale.split("_");
            } else if (locale.indexOf("-") !== -1) {
                splitLocale = locale.split("-");
            } else {
                return undefined;
            }

            if (splitLocale[0] && splitLocale[1]) {

                //lowerCase the first part
                splitLocale[0] = splitLocale[0].toLowerCase();
                splitLocale[1] = splitLocale[1].toUpperCase();

                var formattedLocale = splitLocale.join("-");

                if (formattedLocale) {
                    return formattedLocale;
                } else {
                    return undefined;
                }

            } else {
                return undefined;
            }
        }

        var __parseLanguageFormat = function (passedLocale, testLanguageHash) {

            var defaultLanguageFormat = new RegExp("([a-z]{2})([-])([A-Z]{2})"),
                standardFormat = defaultLanguageFormat.test(passedLocale),
                locale = "";

            if (standardFormat) {
                //Match with keys
                if (Shims.arrayIndexOf(locale, passedLocale) === 1) {

                    locale = passedLocale;

                } else if (locale === "") {
                    locale = __matchLocale(passedLocale);
                }

            } else if (passedLocale.length === 5) {
                // Try to format properly
                var formattedLocale = __formatLocale(passedLocale);

                // Was able to format and will now try to match
                if (formattedLocale) {

                    if (Shims.arrayIndexOf(__locales, passedLocale) === 1) {
                        // If we get a match, use formatted version
                        locale = formattedLocale;

                    } else {
                        // Try to find a match
                        locale = __matchLocale(formattedLocale);
                    }
                }

                // Check if shorter than 5
            } else if (passedLocale.length < 5) {

                // Run it against the matcher
                passedLocale = passedLocale.toLowerCase();
                locale = __matchLocale(passedLocale)
            }

            // If everything fails default to English
            if (locale === "" || locale === undefined) {

                // Fall back to English by default
                if (!testLanguageHash) {
                    locale = "en-US";
                }
            }

            return locale;
        };

        chckt.getTranslation = function (key, locale) {

            var checkedLocale,
                tempLocale;

            if (locale) {
                tempLocale = locale;
            }

            if (tempLocale) {
                checkedLocale = __parseLanguageFormat(tempLocale);
            } else {
                checkedLocale = __parseLanguageFormat(chckt.locale);
            }

            var translatedString = __translateKey(key, checkedLocale);

            if (translatedString) {
                return translatedString;
            } else {
                return key;
            }
        };

        chckt.setTranslation = function (translationObject) {

            if (__languageHash) {

                var translationObjectKeys = [];

                for (var key in translationObject) {
                    translationObjectKeys.push(key);
                }

                // Go through list of the keys in the translationObject
                for (var i = 0; i < translationObjectKeys.length; i++) {

                    // Read current key
                    var currentKey = translationObjectKeys[i];
                    // Loop through __languageHash and match
                    for (var e = 0; e < __languageHash.length; e++) {

                        if (__languageHash[e].key === currentKey) {

                            for (var locale in translationObject[currentKey]) {

                                // Replace languageHash translation with this translation
                                if (locale !== "key") {

                                    var checkedLocale = __parseLanguageFormat(locale, true);

                                    __languageHash[e][checkedLocale] = translationObject[currentKey][locale];
                                }
                            }
                        }
                    }
                }
            }
        };

        init();

    });
    /* global __define, chckt */
    __define('checkoutProcessHandler', ['DOM', 'templater', 'hook'], function (DOM, templater, hook) {

        "use strict";

        return function () {

            var __rootNode = void 0,
                __actionBtn = void 0,
                __selectedPMNode = void 0,
                // <div>
                __initiationForm = void 0; // <form>

            var that = {
                init: function init(pRootNode) {

                    __rootNode = pRootNode;
                    return that;
                },


                ///////////////////////// HANDLERS FOR INITIATE-PAYMENT AJAX CALL ///////////////////

                handleInitiationResponse: function handleInitiationResponse() {
                    var pResp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};


                    //TODO?? Input(?) validate the resp object. Might be hacked.

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('\n### checkoutUI::__handleInitiationResponse:: resp=', pResp);
                        window.console.log('### checkoutUI::__handleInitiationResponse:: resp.resultCode=', pResp.resultCode);
                        window.console.log('### checkoutUI::__handleInitiationResponse:: resp.type=', pResp.type);
                    }

                    that.setPendingInitiation(false); // remove the 'pending' template & classes

                    switch (pResp.type) {

                        case 'redirect':

                            __setPendingRedirect('redirect-details', { response: pResp });

                            if (window._b$dl && window.console && window.console.log) {
                                window.console.log('### checkoutUI::__handleInitiationResponse:: REDIRECT ');
                            }

                            if (!_hook('beforeRedirect', __rootNode, pResp.url || '')) {
                                return false;
                            }

                            // We can redirect
                            if (!pResp._uiEventBased) {
                                document.location.href = pResp.url;
                            }
                            return true;

                        case 'complete':

                            if (window._b$dl && window.console && window.console.log) {
                                window.console.log('### checkoutUI::__handleInitiationResponse:: COMPLETE ');
                            }

                            if (!_hook('beforeComplete', __rootNode, { url: pResp.url || '', payload: pResp.payload || {}, resultCode: pResp.resultCode || '' })) {
                                return false;
                            }

                            // Add a  message
                            var resultText = void 0;

                            switch (pResp.resultCode) {

                                case 'authorised':

                                    resultText = chckt.getTranslation('creditCard.success');
                                    break;

                                case 'received':

                                    resultText = chckt.getTranslation('payment.processing');
                                    break;

                                default:
                                    resultText = chckt.getTranslation('error.subtitle.refused');
                            }

                            if (__rootNode) {
                                __rootNode.innerHTML = resultText;
                            }

                            return true;

                        case 'error':

                        case 'validation':
                            if (window._b$dl && window.console && window.console.log) {
                                window.console.log('### checkoutUI::__handleInitiationResponse:: calls __handleInitiationError [case error||validation]');
                            }

                            that.handleInitiationError(pResp.errorMessage, pResp.fieldMessages || []);
                            break;

                        default:
                            if (window._b$dl && window.console && window.console.log) {
                                window.console.log('### checkoutUI::__handleInitiationResponse:: calls __handleInitiationError [default]');
                            }
                            that.handleInitiationError("Illegal response format");
                    }
                },
                handleInitiationError: function handleInitiationError(pResp, fieldMessages) {

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutUI::__handleInitiationError:: resp=', pResp);
                        window.console.log('### checkoutUI::__handleInitiationError:: fieldMessages=', fieldMessages);
                    }

                    that.setPendingInitiation(false); // remove the 'pending' template & classes

                    __setInitiationError(false); // remove any pre-exisitng error template


                    if (typeof pResp === "string" || typeof pResp === "undefined") {

                        pResp = fieldMessages && fieldMessages.length ? { message: fieldMessages[0].message } : { message: pResp };
                    }

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutUI::__handleInitiationError:: resp=', pResp.message);
                    }

                    __setInitiationError('error', pResp);

                    __toggleButtonState(__actionBtn, 'chckt-pm-list__button--loading', 'chckt-pm-list__button--error', chckt.getTranslation('error.retryButton'));
                },


                /////////////////////////// PUBLIC PROCESS STATE INDICATOR /////////////////////////////////////

                // Add 'pending' template & css class
                // Called from chekoutUI.initiatePayment prior to making Ajax call
                setPendingInitiation: function setPendingInitiation(pTemplateName, pData) {

                    var pendingClass = 'chckt-pm--pending';

                    if (pTemplateName) {

                        if (!_hook('beforePendingInitiation', __selectedPMNode, { actionButton: __actionBtn })) {
                            return;
                        }
                        __toggleProcessIndicators(pTemplateName, pData, pendingClass);
                    } else {

                        if (!_hook('afterPendingInitiation', __initiationForm)) {
                            return;
                        }
                        __toggleProcessIndicators(null, null, pendingClass, '.js-chckt-pending-details');
                    }
                },


                //////////////////// GETTERS / SETTERS ///////////////////////////////////////////////

                setActionButton: function setActionButton(pActionBtn) {
                    __actionBtn = pActionBtn;
                },
                setSelectedForm: function setSelectedForm(pFormEl) {
                    __initiationForm = pFormEl;
                },
                getSelectedForm: function getSelectedForm() {
                    return __initiationForm;
                },
                setSelectedPMNode: function setSelectedPMNode(pNode) {
                    __selectedPMNode = pNode;
                }
            };

            /////////////////////////// PRIVATE PROCESS STATE INDICATORS /////////////////////////////////////


            // Add 'redirect' template & css class
            // Called from __handleInitiationResponse (the 'success' state of the initiate-payment Ajax call in that.initiatePayment)
            var __setPendingRedirect = function __setPendingRedirect(pTemplateName, pData) {

                if (!_hook('beforePendingRedirect', __selectedPMNode, { actionButton: __actionBtn })) {
                    return false;
                }

                var redirectingClass = 'chckt-pm--redirecting';
                pTemplateName ? __toggleProcessIndicators(pTemplateName, pData, redirectingClass) : __toggleProcessIndicators(null, null, redirectingClass, '.js-chckt-redirect');
            };

            // Add 'error' template & css class
            // Called from __handleInitiationError
            var __setInitiationError = function __setInitiationError(pTemplateName, pData) {

                var errorClass = 'chckt-pm--error';
                pTemplateName ? __toggleProcessIndicators(pTemplateName, pData, errorClass) : __toggleProcessIndicators(null, null, errorClass, '.js-chckt-error');
                __toggleButtonState(__actionBtn, 'chckt-pm-list__button--loading', 'chckt-pm-list__button--error', chckt.getTranslation('error.retryButton'));
            };

            // Helper fn for the '__set[process-state]' fns above
            // Either adds a class to the specified node and appends a template to it
            // OR removes a class from the node and removes the added template from it
            var __toggleProcessIndicators = function __toggleProcessIndicators(pTemplateName, pData, pPendingClass, pTemplateClass) {

                var hasTemplateName = function hasTemplateName() {

                    DOM._replaceClass(__selectedPMNode, '', pPendingClass);

                    templater.renderTemplate(pTemplateName, __selectedPMNode, pData, true);
                };

                var noTemplateName = function noTemplateName() {

                    DOM._replaceClass(__selectedPMNode, pPendingClass, '');

                    var template = DOM._selectOne(__selectedPMNode, pTemplateClass);
                    if (template) {
                        template.parentNode.removeChild(template);
                    }
                };

                pTemplateName ? hasTemplateName() : noTemplateName();
            };
            //-------------------------- end PRIVATE PROCESS STATE INDICATORS ---------------------------


            /////////////////////////////////// Utils ///////////////////////////////////////////

            // Helper fn for the button states
            // Either adds or removes the loading state from the button + adds / removes correct wording
            var __toggleButtonState = function __toggleButtonState(pButton, replaceClass, pClass, pMessage) {

                if (!replaceClass) {
                    replaceClass = '';
                }

                DOM._replaceClass(pButton, replaceClass, pClass);

                if (pMessage) {
                    var buttonText = DOM._getElementsByClassName(pButton, 'chckt-button__text');
                    if (buttonText) {
                        buttonText[0].innerHTML = pMessage;
                    }
                }
            };

            var _hook = function _hook(name, node, data) {
                return hook(that, name, node, data);
            };
            //---------------------------------------------------------------------------------

            return that;
        }; // return checkoutProcessHandler
    }); // __define
    /* global __define, chckt, chcktPay, _a$deviceFingerprint */
    __define('checkoutUI', ['checkoutProcessHandler', 'log', 'DOM', 'Util', 'hook', 'templater', 'shims', 'apiLog', 'Net'], function (checkoutProcessHandler, log, DOM, Util, hook, templater, Shims, apiLog, Net) {

        "use strict";

        return function () {

            var __model = void 0,
                __paymentMethodNodes = void 0,
                __paymentsHolder = void 0,
                __rootNode = void 0,
                // The top level container <div> in the html returned by the response (currently has class 'chckt-sdk')
                __selectedPMNode = void 0,
                // <div>
                __selectedPM = void 0,
                // String
                __excessPMBtn = void 0,
                __actionBtn = void 0,
                __checkoutProcessHandler = void 0,
                __initialPMCount = void 0,
                __originalPMCount = void 0,
                __excessPMBtnIsToggle = void 0,
                __excessPMsVisible = true;

            // PUBLIC FACING INTERFACE EXPOSING PUBLIC METHODS & PROPERTIES
            var that = {

                // Initialise vars, create <div> that will hold payment list
                init: function init(pRootNode, pModel, pInitialPMCount, pOriginalPMCount) {

                    pRootNode.__checkoutUI = pRootNode.__checkoutUI || that;

                    __rootNode = pRootNode;

                    __model = pModel;

                    __checkoutProcessHandler = checkoutProcessHandler().init(__rootNode);

                    if (!__model || !__model.paymentMethods || __model.paymentMethods.length === 0) {

                        log("Could not find payment methods in the payment model", __model);
                        pRootNode.innerHTML = '<div class="co-error">Unable to instantiate the payment screen.</div>';
                        return false;
                    }

                    __initialPMCount = pInitialPMCount;
                    __originalPMCount = pOriginalPMCount;

                    __paymentsHolder = document.createElement('div');
                    __paymentsHolder.className = 'chckt-payment-holder';
                    pRootNode.appendChild(__paymentsHolder);

                    return true;
                },
                // that.init

                // CALLED AS PART OF SETUP PROCESS: checkoutAPI.initPMList
                // (re)Render PM list (hiding excess), initialise buttons and select initial PM
                renderTemplates: function renderTemplates(pConfigObjPaymentMethods, pIsReRender) {

                    // Render payment template
                    templater.renderTemplate('payment', __paymentsHolder, __model);

                    // Check if we now have PM nodes in the DOM
                    __paymentMethodNodes = DOM._select(__rootNode, '.js-chckt-pm');

                    if (!__paymentMethodNodes) {
                        apiLog('No payment method nodes', { merchant: __model.payment.reference });
                        return false;
                    }

                    __setPMValidities();

                    __setUpButtons();

                    // re. re-rendering
                    var reRender = function reRender() {

                        // If excess PMs are still hidden - reset flag and hide them again now template has re-rendered
                        if (!__excessPMsVisible) {

                            __excessPMsVisible = true;
                            __hideExcessPMs();
                        }

                        // If "show more PMs" button is a toggle make sure it's reshown
                        if (__excessPMBtnIsToggle) {
                            that.toggleShowMorePMsButton(true);
                        }
                    };
                    //--

                    !pIsReRender ? __hideExcessPMs() : reRender();

                    __setCardHolderNameVisibilty(pConfigObjPaymentMethods);

                    __makeInitialPMSelection();

                    that.checkEnableActionButton();

                    // Pass a ref to the pay/review btn to the process handler
                    __checkoutProcessHandler.setActionButton(__actionBtn);
                },
                // that.reRenderTemplates

                /**
                 * Perform a payment initiation call and handle the result to continue the workflow.
                 *
                 * @param data
                 * @returns {boolean}
                 * @private
                 */
                initiatePayment: function initiatePayment(pUseInitiateEndpoint) {

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutUI::initiatePayment:: pUseInitiateEndpoint=', pUseInitiateEndpoint);
                    }

                    // TODO handle having the data passed as a param?

                    // Add 'pending' template & css class
                    __checkoutProcessHandler.setPendingInitiation('pending');

                    var initPayConfigObj = {
                        responseData: __model,
                        pmType: __selectedPM,
                        formEl: __checkoutProcessHandler.getSelectedForm(),
                        onSuccess: __checkoutProcessHandler.handleInitiationResponse,
                        onError: __checkoutProcessHandler.handleInitiationError,
                        deviceFingerprint: _a$deviceFingerprint,
                        preventPost: !pUseInitiateEndpoint
                    };

                    return chcktPay(initPayConfigObj);
                },
                // that.initiatePayment

                // Handle click on a PM - to set a selected state on the chosen PM (& a deselected state on the others)
                // Also store the form that the chosen PM contains
                // Called as part of setup process (unless blocked by "showPaymentMethodSelection" hook) and also whenever a PM is clicked
                selectPaymentMethod: function selectPaymentMethod(pPMName, pNode) {

                    var selectedPMClass = "js-chckt-pm--selected";

                    // Routine to detect first click on PM or second click on same PM
                    var paymentMethodClick = function paymentMethodClick() {

                        __selectedPM = pPMName;

                        // If we haven't been passed a node, then match the passed PM method name to a node
                        __selectedPMNode = pNode ? pNode : DOM._selectOne(__paymentMethodNodes[0].parentNode, '[data-pm="' + __selectedPM + '"]');

                        // Pass the selected node to the process handler
                        __checkoutProcessHandler.setSelectedPMNode(__selectedPMNode);

                        // Loop through all PM nodes - to remove selected state...
                        __removeClassOnPMs(selectedPMClass);

                        // ...then set a selected state on the selectedNode and check it's radio button
                        DOM._replaceClass(__selectedPMNode, '', selectedPMClass);

                        __setCheckOnRadioBtn(true);
                        //--

                        // Use selected node to identify and store the related form
                        __checkoutProcessHandler.setSelectedForm(DOM._selectOne(__selectedPMNode, '.js-chckt-pm__pm-holder'));
                    };

                    // KEEP until we know if this might me desired fny for some merchants
                    //                        function secondClickOnSamePM(){
                    //                            // Comment in these lines to deselect the selected PM when it is clicked a 2nd time
                    //                              __selectedPM = null; __removeClassOnPMs(selectedPMClass); __setCheckOnRadioBtn(false);
                    //                        }

                    var paramIsNotSelectedPM = pPMName !== __selectedPM;

                    //                    paramIsNotSelectedPM ? paymentMethodClick() : secondClickOnSamePM();
                    if (paramIsNotSelectedPM) {
                        paymentMethodClick();
                    }
                },
                //that.selectPaymentMethod


                /////////////////////////////// PUBLIC UTILS ///////////////////////////////////////////

                // Hides/Shows excess PMs.
                // Called during setup process & then from checkoutAPI when user clicks on the 'Show more payment methods' button
                toggleExcessPaymentMethods: function toggleExcessPaymentMethods() {

                    var toggleExcessPms = function toggleExcessPms(pShow) {

                        var exPms = __paymentMethodNodes.slice(__initialPMCount);
                        Shims.forEach(exPms, function (pItem) {
                            pShow ? DOM._replaceClass(pItem, 'chckt-pm--hidden', '') : DOM._replaceClass(pItem, '', 'chckt-pm--hidden');
                        });

                        __excessPMsVisible = pShow;
                        if (window._b$dl && window.console && window.console.log) {
                            window.console.log('### checkoutUI::toggleExcessPaymentMethods:: __excessPMsVisible=', __excessPMsVisible);
                        }
                        return pShow;
                    };

                    return toggleExcessPms(!__excessPMsVisible);
                },
                // that.toggleExcessPaymentMethods

                // Give all paymentMethod divs a disabled state
                // Called when pay/review button clicked
                toggleEnableAllPaymentMethods: function toggleEnableAllPaymentMethods(pEnable) {

                    var addDisabledClasses = function addDisabledClasses(pPm, pClassToAdd) {
                        DOM._replaceClass(pPm, '', pClassToAdd);
                        DOM._replaceClass(pPm, '', 'js-' + pClassToAdd);
                    };

                    var removeDisabledClasses = function removeDisabledClasses(pPm, pClassToRemove) {
                        DOM._replaceClass(pPm, pClassToRemove, '');
                        DOM._replaceClass(pPm, 'js-' + pClassToRemove, '');
                    };

                    if (__paymentMethodNodes) {

                        Shims.forEach(__paymentMethodNodes, function (pItem) {

                            !pEnable ? addDisabledClasses(pItem, 'chckt-pm--disabled') : removeDisabledClasses(pItem, 'chckt-pm--disabled');
                        });
                    }
                },
                // that.toggleEnableAllPaymentMethods

                // Shows/hides the 'Show more payment methods' button
                // Called during initial setup and then when pay/review button clicked
                toggleShowMorePMsButton: function toggleShowMorePMsButton(pShow) {

                    if (__excessPMBtn) {

                        !pShow ? DOM._replaceClass(__excessPMBtn, 'chckt-more-pm-button--shown', '') : DOM._replaceClass(__excessPMBtn, '', 'chckt-more-pm-button--shown');
                    }
                },
                // that.toggleShowMorePMsButton

                // Shows/hides the 'Pay/Review' button
                // Can be called in the event that we have a 'review your order' step in the submit process
                toggleActionButton: function toggleActionButton(pShow) {

                    if (__actionBtn) {

                        pShow ? DOM._replaceClass(__actionBtn, 'chckt-button--submit--hidden', '') : DOM._replaceClass(__actionBtn, '', 'chckt-button--submit--hidden');
                    }
                },
                // that.toggleShowMorePMsButton

                // Called during initial setup and then whenever a payment method is selected
                // Enable/disable action (pay) button based on the stored validity of the PM
                checkEnableActionButton: function checkEnableActionButton() {

                    __selectedPM ? chckt.togglePayButton(chckt.pms[__selectedPM]) : chckt.togglePayButton(false);
                },
                // that.checkEnableActionButton

                setClassOnActionButton: function setClassOnActionButton(pReplaceClass, pShowClass) {

                    __toggleButtonState(__actionBtn, pReplaceClass, pShowClass);
                },


                // Util to match txVariant to an object in the paymentMethods array
                // Could abstract to a fn: selectOneObjectFromArray(pArr, pProp, pMatch)
                // Called from checkoutAPI when setting focus on a particular field
                pmFromType: function pmFromType(pTxVariant) {
                    // Will either return the matching pm OR undefined
                    return Shims.filter(__model.paymentMethods, function (pItem) {
                        return pItem.type === pTxVariant;
                    }) // find the single item that matches
                        .shift(); // remove it from the array that filter created
                },


                // Getter
                // Called from checkoutAPI when setting focus on a particular field
                getSelectedPM: function getSelectedPM() {

                    return __selectedPM;
                },


                // Take the mappedRecurringCards array (of objects with type & paymentMethodData props)
                // and add a node property whose value is the associated paymnetMethod node (.js-chckt-pm)
                getRecurringCards: function getRecurringCards(pRecurringCards) {

                    var newArrray = [];

                    Shims.forEach(pRecurringCards, function (pItem) {

                        var newItem = Util._deepClone(pItem);

                        newArrray.push(newItem);

                        var recType = pItem.type;

                        newItem.node = Shims.filter(__paymentMethodNodes, function (pmnItem) {
                            return DOM._getAttribute(pmnItem, 'data-pm') === recType;
                        }).shift();
                    });

                    return newArrray;
                },
                disableRecurringContract: function disableRecurringContract(pRecurringCards, pPaymentMethodData, pType, pCallback) {

                    var disableURL = __model.disableRecurringDetailUrl;

                    var data = {
                        paymentMethodData: pPaymentMethodData,
                        paymentData: __model.paymentData
                    };

                    var jsonData = JSON.stringify(data);

                    // Do logic to call server etc.
                    Net._ajax({
                        url: disableURL + "?token=" + __model.originKey,
                        method: "POST",
                        contentType: "application/json",
                        data: jsonData,
                        success: function success(text, xhr) {
                            if (window._b$dl && window.console && window.console.log) {
                                window.console.log('### checkoutUI_es6::success:: removed recurring contract for:', pType);
                            }

                            // Remove PM from __model.paymentMethods
                            __removePMFromArray(__model.paymentMethods, pType);

                            // Remove PM from recurringCards list
                            __removePMFromArray(pRecurringCards, pType);

                            // IN THE EVENT THAT THE PMLIST IS GOING TO BE RE-RENDERED - CERTAIN VARS NEED TO BE PREPARED

                            // Recurring card must have been selected in order to be removed
                            __selectedPM = null;

                            // Reduce the number of PM's to show at the start
                            --__initialPMCount >= 0 ? __initialPMCount : 0; // Faster eqivalent of: Math.max()

                            // If we have recurring cards, that all get removed, & the PM list gets re-rendered...
                            // ...then show the original number of PMs (Edge case!!)
                            if (__initialPMCount === 0) {
                                __initialPMCount = __originalPMCount;
                            }

                            //---------


                            // Default behaviour, if the merchant hasn't specified a callback, hide the CVC field
                            var hideField = function hideField() {

                                var recNode = DOM._selectOne(__rootNode, '[data-pm="' + pType + '"]');
                                var labelEl = DOM._selectOne(recNode, '.js-chckt-hosted-input-field');
                                if (labelEl) {
                                    labelEl.style.display = 'none';
                                }
                            };

                            pCallback ? pCallback.call(null, false, text, pType) : hideField();
                        },
                        error: function error(text, status, xhr) {
                            // pConfigObj.onError( text, [{message: text, status: status, xhr: xhr}] );
                            pCallback.call(null, status, text, pType);
                        }
                    });
                },


                // See explanation in __setupButtons
                fixExcessPMBtnToggleClassification: function fixExcessPMBtnToggleClassification() {
                    __excessPMBtnIsToggle = false;
                }

                // TODO - is this useful?
                //                getRecurringCardNodes(){
                //
                //                    return Shims.filter(__paymentMethodNodes, (pItem) => (DOM._getAttribute(pItem, 'data-pm').indexOf('_r@') > -1) );
                //                },

                //                toggleInfoText(targetInfo) {
                //                    var targetInfoContainer = DOM._select( __rootNode, targetInfo);
                //                    log("Testing the onclick of toggleinfo" + targetInfoContainer);
                //                }

                //---------------------- end PUBLIC UTILS -----------------------------------------------

            }; // that

            that.type = 'CheckoutUI';

            ///////////////////////////////////////////////////////////////////////////////////////////////////////
            ////////////////////////////////////// PRIVATE MEMBERS ////////////////////////////////////////////////
            ///////////////////////////////////////////////////////////////////////////////////////////////////////

            /////////////////////////////////// SETUP ////////////////////////////////////

            // Called as part of that.renderTemplates process
            var __setPMValidities = function __setPMValidities() {

                // If a PM node has the attribute 'data-additional-required' then it means the
                // corresponding PM in the model.paymentMethods has an inputDetails array *(see NOTE, below)
                // i.e. the shopper is required to full in additional data... so mark the pms
                // as already valid, or not, based on the absence/presence of this attribute
                Shims.forEach(__paymentMethodNodes, function (pItem) {

                    var hasDataRequiredAttr = pItem.hasAttribute("data-additional-required");

                    var pm = DOM._getAttribute(pItem, 'data-pm');
                    chckt.pms[pm] = !hasDataRequiredAttr;

                    // If pm has input details that need to be completed e.g. card, sepa direct debit: mark it as such on the actual pm object
                    // *NOTE: some PMs, like paypal, have input details but this is only the "remember for next time" checkbox
                    // & the shopper is not obliged to do anything
                    var pmObj = that.pmFromType(pm);
                    pmObj.requiresShopperInfo = hasDataRequiredAttr;
                });
            };

            // Called as part of that.renderTemplates process
            var __setUpButtons = function __setUpButtons() {

                __excessPMBtn = DOM._selectOne(__rootNode, '.js-chckt-extra-pms-button') || DOM._selectOne(__rootNode, '[data-pm="extra-pms-button"]');

                __actionBtn = DOM._selectOne(__rootNode, '.js-chckt-button--submit');

                // Store whether "show more PMs" button is acting as a toggle or can only be clicked once
                if (__excessPMBtn) {

                    // If toggle fny is defined...
                    if (hook('hasHook', 'toggleExcessPaymentMethods')) {

                        // ..set boolean demarking the button as a toggle
                        // NOTE: technically if the hook is defined but returns true then the button isn't supposed to
                        // act as a toggle, so we shouldn't mark it as such. In the case of a wrongly marked btn, re-rendering
                        // the templates would cause it to reappear and strange bevaviour will ensue if it is then clicked.
                        // So when the hook is actually run, if it returns true, checkoutAPI calls the
                        // fixExcessPMBtnToggleClassification function

                        // Only if the boolean has NOT been explicity defined as false should we allow it to be set to true
                        if (__excessPMBtnIsToggle !== false) {
                            __excessPMBtnIsToggle = true;
                        }
                    }
                }

                if (__actionBtn) {

                    // Remove any prexisting listener i.e. in the event we are re-rendering after a language change
                    DOM._off(__actionBtn, 'click', __firstSubmitActionClickHandler);
                    DOM._on(__actionBtn, 'click', __firstSubmitActionClickHandler);
                }
            };

            // Called as part of that.renderTemplates process
            var __hideExcessPMs = function __hideExcessPMs() {

                // MORE THAN SPECIFIED NUMBER OF PAYMENT METHODS - SO HIDE REMAINDER
                if (__initialPMCount && __paymentMethodNodes.length > __initialPMCount) {

                    // Hide 'excess' PMs
                    that.toggleExcessPaymentMethods();

                    // Show 'show more PMs' button
                    that.toggleShowMorePMsButton(true);
                }
            };

            // Called as part of that.renderTemplates process
            // Hide the optional card-holder-name field for Credit card PMs - unless, per PM, it is to be shown
            var __setCardHolderNameVisibilty = function __setCardHolderNameVisibilty(pShowFieldObj) {

                Shims.forEach(__paymentMethodNodes, function (pItem) {

                    var pm = DOM._getAttribute(pItem, 'data-pm');

                    var showField = false;

                    var hasNamedCardPM = pShowFieldObj.hasOwnProperty(pm);
                    var hasDefaultCardPM = pShowFieldObj.hasOwnProperty('card');

                    // Check for presence of named PM in the 'showField' object
                    if (hasNamedCardPM) {

                        if (pShowFieldObj[pm].showOptionalHolderNameField) {
                            showField = true;
                        }
                    } else {

                        // Check for default card entry
                        if (hasDefaultCardPM) {

                            if (pShowFieldObj.card.showOptionalHolderNameField) {
                                showField = true;
                            }
                        }
                    }

                    if (!showField) {

                        var holderNameEl = DOM._selectOne(pItem, '[data-enrich="' + 'holderName' + '"]');

                        DOM._replaceClass(holderNameEl, '', 'chckt-form-label--hidden');
                    }
                });
            };
            //--


            // Helper fn for the button states
            // Either adds or removes the loading state from the button + adds / removes correct wording
            var __toggleButtonState = function __toggleButtonState(pButton, replaceClass, pClass, pMessage) {

                if (!replaceClass) {
                    replaceClass = '';
                }

                DOM._replaceClass(pButton, replaceClass, pClass);

                if (pMessage) {
                    var buttonText = DOM._getElementsByClassName(pButton, 'chckt-button__text');
                    if (buttonText) {
                        buttonText[0].innerHTML = pMessage;
                    }
                }
            };

            var __showLoadingAnim = function __showLoadingAnim() {

                if (!_hook('showProcessingAnimation', __actionBtn)) {
                    return false;
                }
                __toggleButtonState(__actionBtn, '', 'chckt-pm-list__button--loading');
            };

            // Button handler for the pay button
            // Added in __setUpButtons
            // Checks 'onSubmitAction' hook otherwise performs default behaviour
            // and initates the payment via chckt.submitPaymentForm
            var __firstSubmitActionClickHandler = function __firstSubmitActionClickHandler(event) {

                // Check for 'onSubmitAction' hook - a chance for the merchant to prevent the default behaviour
                if (!_hook('onSubmitAction', __actionBtn, { amount: __model.payment.amount, selectedPMNode: __selectedPMNode, pmNodes: __paymentMethodNodes })) {
                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutAPI::submitPaymentForm:: delayed payment hook (\'onSubmitAction\')');
                    }
                    return;
                }

                ///////////// DEFAULT BEHAVIOUR: ////////////////

                // Hide 'show more PMs' btn
                that.toggleShowMorePMsButton(false);

                // Give all paymentMethod divs a disabled state
                that.toggleEnableAllPaymentMethods(false);

                // Disable button
                __toggleButtonState(__actionBtn, '', 'js-chckt-pm-list__button--disabled');
                __actionBtn.setAttribute('disabled', 'true'); // IE <=10

                // Show loading animation on button
                __showLoadingAnim();

                // Trigger payment initiation
                chckt.submitPaymentForm();
            };
            //--

            // Called as part of that.renderTemplates process
            var __makeInitialPMSelection = function __makeInitialPMSelection() {

                // Block selecting first PM (thus leaving __selectedPM undefined)
                // TODO - should this be a hook or a SDK config option - "preselectFirstPaymentMethod" ??
                if (!_hook('selectFirstPaymentMethod', __rootNode, __model.paymentMethods)) {
                    return;
                }

                // Scenario: templates being re-rendered after language module loaded - keep open the PM that has already been selected
                var lastSelectedPM = __selectedPM;

                __selectedPM = null; // 'de-select' existing PM

                if (lastSelectedPM) {

                    // Find the node whose data-pm attr = lastSelectedPM
                    var lastSelectedNode = DOM._selectOne(__paymentMethodNodes[0].parentNode, '[data-pm="' + lastSelectedPM + '"]');

                    that.selectPaymentMethod(lastSelectedPM, lastSelectedNode);

                    return;
                }
                //--

                var selectFirstPaymentMethod = function selectFirstPaymentMethod() {

                    that.selectPaymentMethod(DOM._getAttribute(__paymentMethodNodes[0], 'data-pm'), __paymentMethodNodes[0]);
                };

                var multiPaymentMethods = function multiPaymentMethods() {

                    if (__paymentMethodNodes.length > 1) {
                        selectFirstPaymentMethod();
                    }
                };

                var onlyOnePM = __paymentMethodNodes.length === 1;
                onlyOnePM ? selectFirstPaymentMethod() : multiPaymentMethods();
            };
            //--

            //----------------------------------- end SETUP ------------------------------------------


            ////////////////////////// PRIVATE UTILS ///////////////////////////////////////////////

            var __removePMFromArray = function __removePMFromArray(pArray, pTxVariant) {

                var pm = Shims.filter(pArray, function (pItem) {
                    return pItem.type === pTxVariant;
                }).shift();

                var index = Shims.arrayIndexOf(pArray, pm);

                if (index > -1) {
                    pArray.splice(index, 1);
                }
            };

            // Loop through all PM nodes - to remove a class
            var __removeClassOnPMs = function __removeClassOnPMs(pClass) {

                Shims.forEach(__paymentMethodNodes, function (pItem) {
                    DOM._replaceClass(pItem, pClass, '');
                });
            };
            //--

            // Sets PM's radio button to checked. Can also be used to toggle radio button (tho' this fny is currently disabled)
            // Called whenever a payment method is selected
            var __setCheckOnRadioBtn = function __setCheckOnRadioBtn(pCheck) {

                var pmCheckbox = DOM._getElementsByClassName(__selectedPMNode, 'js-chckt-pm-radio-button');

                if (pCheck === true && pmCheckbox[0].checked === true) {
                    pmCheckbox[0].checked = false;

                    // Set a timeout, otherwise the dom wil try to set true and false at the same time
                    setTimeout(function () {
                        pmCheckbox[0].checked = pCheck;
                    }, 1);
                } else {
                    pmCheckbox[0].checked = pCheck;
                }
            };

            // Shorthand for hook fny
            var _hook = function _hook(name, node, data) {
                return hook('', name, node, data);
            };

            //--------------------- end PRIVATE UTILS -------------------------------------------

            // Experiment - how to expose fny for testing
            //            const doubler = (n) => n*2;
            //            shared.module.exports.checkoutUI = {checkoutUI : that, doubler : doubler};

            // RETURN PUBLIC INTERFACE
            return that;
        }; // return checkoutUI
    }); // __define
    /* global __define, chckt */
    __define('checkoutAPI', ['log', 'DOM', 'hook', 'checkoutUI', 'shims', 'Util'], function (log, DOM, hook, CheckoutUI, Shims, Util) {

        "use strict";

        var __checkoutUIRef = void 0,
            __rootNode = void 0,
            __useInitiateEndpoint = void 0,
            __recurringCards = void 0,
            __configPMs = void 0,
            __setupSecureFieldsFn = void 0;

        // Public interface for checkoutAPI module
        var that = void 0;

        //////////////////// METHODS AVAILABLE ON THE GLOBAL 'CHECKOUT' OBJECT ///////////////////////////////////////

        // Called by clicking on the header <div> of a PM in selectPaymentMethod template
        chckt.selectPaymentMethod = function (pNode, pMethodName) {

            try {

                // If methodName not specified - 'Cancel' button has been pressed BUT we could still send the
                // methodName with the cancel button: selectPaymentMethod already handles the methodName getting sent a second time
                // However this does act as a fall back
                if (!pMethodName) {
                    pMethodName = DOM._closest(pNode, '.js-chckt-pm').getAttribute('data-pm');
                }
                //--

                var pmNode = DOM._closest(pNode, '.js-chckt-pm');

                __checkoutUIRef.selectPaymentMethod(pMethodName, pmNode);
                __checkoutUIRef.checkEnableActionButton();

                that.selectedPMSetFocus(pMethodName);
            } catch (e) {
                log("[Error] Error while handling payment method selection: ", e);
            }

            return false;
        };

        // Called by clicking on the 'More payment methods' button (<a> element) in selectPaymentMethod template
        chckt.toggleExcessPaymentMethods = function (pNode, pNodeName) {

            var showingPMs = __checkoutUIRef.toggleExcessPaymentMethods();

            var btnNode = DOM._closest(pNode, '.' + pNodeName) || DOM._closest(pNode, '[data-pm="extra-pms-button"]');

            // Hook could use showingPMs boolean to perform other behaviour based on whether we have just revealed or hidden the PMs
            if (hook('', 'toggleExcessPaymentMethods', btnNode, showingPMs)) {

                // If a hook doesn't exist OR the hook doesn't explicitly return false - then continue with the default fny of hiding the toggle button after it's clicked once
                btnNode.parentNode.removeChild(btnNode);

                // See comment about toggle btn fny in checkoutUI > __setupButtons fn
                __checkoutUIRef.fixExcessPMBtnToggleClassification();
            }

            if (pNode.tagName.toLowerCase() === 'a') {
                return false; // prevent <a> default behaviour
            }
        };

        /**
         * Alternative functionality for pay button.
         * The pay buttons in the PMs can drop their type="submit" and instead have onclick="chckt.submitPaymentForm()"
         * The best use case for this is a single pay button at the bottom of the PM list that submits the form of whichever
         * PM is selected.
         *
         * @param pDelaySubmit (boolean) - true under normal circumstances, false when the 'Pay' button first leads
         *                                  to a 'review your order' stage
         */
        chckt.submitPaymentForm = function (pDelaySubmit, e) {

            // From vn 1.2.1 onwards - prevent action if it comes from a (pay/review) button.
            // The event, e, param doesn't exist prior to this version.
            // Now the handler for these button clicks, added programmatically in checkoutUI, calls this function to
            // initiate the payment.
            // Alternatively this function can be called directly by the merchant e.g. if they're implementing their own 'pay' button
            if (Util._existy(e) && e instanceof MouseEvent) {
                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutAPI_es6::submitPaymentForm:: event param exists & is MouseEvent - returning');
                }
                return;
            }

            if (window._b$dl && window.console && window.console.log) {
                window.console.log('### checkoutAPI::submitPaymentForm:: regular payment submit');
            }

            // Check that selected PM is in a valid state
            // - necessary if chckt.submitPaymentForm(true) is ever called directly e.g. from merchant's JS
            var pm = __checkoutUIRef.getSelectedPM();
            if (!chckt.checkPaymentMethodIsValid()) {
                if (window._b$dl && window.console && window.console.warn) {
                    window.console.warn('WARNING: Submitted payment method is not valid. [pm=', pm, 'is valid=', chckt.pms[pm], ']');
                }
                return false;
            }

            try {
                var result = __checkoutUIRef.initiatePayment(__useInitiateEndpoint);

                if (!__useInitiateEndpoint && !hook('', 'getDataFromSubmissionProcess', __rootNode, result)) {
                    return true;
                }

                // We return true if initiatePayment runs successfully & is allowed to go on to make the initiatePayment AJAX call
                // However without a Promise we have no way to know whether the AJAX call was successful
                return result;
            } catch (e) {
                log("[ERROR] Checkout offer handling failed", e);
                return false;
            }
        };

        // Check that selected PM is in a valid state OR pass in pm you wish to check
        chckt.checkPaymentMethodIsValid = function (pPm) {
            var pm = pPm || __checkoutUIRef.getSelectedPM();
            return Util._notFalsy(chckt.pms[pm]);
        };

        // For iDeal payment method - called from select onchange in issuerListFieldType template
        // Changes image when issuer is selected, potentially change the name after iDeal too
        chckt.selectIssuer = function (pElement) {

            var parentElement = DOM._closest(pElement, '.js-chckt-pm'),
                brandImg = parentElement.querySelector('img'),
                selectedElement = pElement.options[pElement.selectedIndex];

            var issuerImg = selectedElement.getAttribute('data-issuer-image');

            var pm = DOM._selectOne(parentElement, '[name="txvariant"]').value;

            // Check if parent has an initial data image attribute
            if (!parentElement.hasAttribute("data-initial-image-url")) {

                var parentImageSrc = parentElement.querySelector('img').src;
                parentElement.setAttribute('data-initial-image-url', parentImageSrc);
            }

            if (pElement.options.selectedIndex === 0) {

                issuerImg = parentElement.getAttribute('data-initial-image-url');
                brandImg.src = issuerImg;

                chckt.pms[pm] = false; // Store valid state in central storage

                chckt.togglePayButton(false);
            } else {

                brandImg.src = issuerImg;

                chckt.pms[pm] = true; // Store valid state in central storage

                chckt.togglePayButton(true);
            }
        };

        // Toggles disabled state on all payment method nodes
        // For custom use e.g. index.php demo-ing 'review' stage - called from 'onSubmitAction' hook
        chckt.toggleEnableAllPaymentMethods = function (pEnable) {

            __checkoutUIRef.toggleEnableAllPaymentMethods(pEnable);
        };

        // Toggles display state on 'show more payment methods' button.
        // For custom use e.g. index.php demo-ing 'review' stage - called from 'onSubmitAction' hook
        chckt.toggleShowMorePMsButton = function (pShow) {

            __checkoutUIRef.toggleShowMorePMsButton(pShow);
        };

        // Toggles enabled state on pay button
        // For custom use.
        // Called from chckt.selectIssuer, checkoutUI & checkoutMain
        chckt.togglePayButton = function (pEnable) {

            var payBtn = DOM._selectOne(__rootNode, '.js-chckt-button--submit');

            if (!pEnable) {

                if (payBtn.className.indexOf('chckt-button--disabled') === -1) {
                    payBtn.className += " chckt-button--disabled";
                }
            } else {

                if (payBtn.className.indexOf('chckt-button--disabled') > -1) {

                    var newClassName = payBtn.className.replace('chckt-button--disabled', '');
                    payBtn.className = Shims.trim(newClassName);
                }
            }
        };

        // Toggles display state on 'pay/review' button.
        // For custom use e.g. index.php demo-ing 'review' stage - called from  'onSubmitAction' hook
        chckt.toggleActionButton = function (pShow) {
            __checkoutUIRef.toggleActionButton(pShow);
        };

        // Toggles focus state on textFieldType template - called from onfocus & onblur events on input
        chckt.textFieldSetFocus = function (pElement, pFocus) {

            pFocus ? DOM._replaceClass(pElement, '', 'chckt-input-field--focus') : DOM._replaceClass(pElement, 'chckt-input-field--focus', '');
        };

        // Toggles focus state on issuerListFieldType template - called from onfocus & onblur events on input
        chckt.selectFieldSetFocus = function (pElement, pFocus) {

            pFocus ? DOM._replaceClass(pElement, '', 'chckt-select-box--focus') : DOM._replaceClass(pElement, 'chckt-select-box--focus', '');
        };

        chckt.getRecurringCards = function () {

            return __checkoutUIRef.getRecurringCards(__recurringCards);
        };

        // Called from a recurring card contract
        chckt.disableRecurringContract = function (pPaymentMethodData, pType, pCallback) {

            // Check if paymentMethodData & PM type are sent along and disable the contract if so
            if (!pPaymentMethodData || !pType) {
                if (window.console && window.console.warn) {
                    window.console.warn('WARNING: disableRecurringContract expects at least 2 arguments: paymentMethodData & payment method type');
                }
                return;
            }

            __checkoutUIRef.disableRecurringContract(__recurringCards, pPaymentMethodData, pType, pCallback);
        };

        chckt.reRenderPMList = function () {
            __checkoutUIRef.renderTemplates(__configPMs, true);
            __setupSecureFieldsFn.call(null);
        };
        //---------------- end METHODS AVAILABLE ON THE GLOBAL 'ADYEN' OBJECT ------------------------

        // Called from onclick on label elements in credit card templates
        chckt.setFocusOnField = function (pElement, setFocus) {

            if (setFocus === true) {

                var formHolder = DOM._closest(pElement, '.js-chckt-pm__pm-holder');
                if (!formHolder) {
                    formHolder = DOM._closest(pElement, 'form');
                }

                var txVariant = DOM._selectOne(formHolder, '[name="txvariant"]').value;

                // Get closest sibling
                var targetHostedFieldElement = Shims.nextElementSibling(pElement);

                if (targetHostedFieldElement) {

                    var LABEL_FOCUS_CLASS = " chckt-form-label__text--focus";

                    //Get data-hosted-id attribute
                    var hostedFieldId = DOM._getAttribute(targetHostedFieldElement, 'data-hosted-id');

                    // Set focus on the field and focus class on the label
                    if (hostedFieldId) {

                        // Set focus on field
                        that.csfRef.setFocusOnFrame(txVariant, hostedFieldId);

                        if (pElement.className.indexOf(LABEL_FOCUS_CLASS) === -1) {
                            pElement.className += LABEL_FOCUS_CLASS;
                        }
                    }
                }
            }
        };

        // checkoutAPI define fn
        return function () {

            that = {

                // Called from checkoutMain as part of the 'boot' process
                initPMList: function initPMList(pPmListConfigObj) {

                    __rootNode = pPmListConfigObj.sdkRootNode;

                    __useInitiateEndpoint = pPmListConfigObj.initiatePayment;

                    __recurringCards = pPmListConfigObj.recurringCards;

                    __configPMs = pPmListConfigObj.paymentMethodsConfig;

                    __setupSecureFieldsFn = pPmListConfigObj.setupSecureFields;

                    // INITIALIZE PAYMENT METHOD LIST
                    __checkoutUIRef = CheckoutUI();

                    if (__checkoutUIRef.init(__rootNode, pPmListConfigObj.setupJsonObj, pPmListConfigObj.initialPMCount, pPmListConfigObj.originalPMCount)) {
                        __checkoutUIRef.renderTemplates(__configPMs);
                    }

                    return true;
                },
                selectedPMSetFocus: function selectedPMSetFocus(pTxVariant) {

                    var pm = void 0;

                    // Exception for when templates are rendered (on first load)
                    // Get selected pm and recurse
                    if (!pTxVariant) {

                        pm = __checkoutUIRef.getSelectedPM();
                        if (pm) {
                            this.selectedPMSetFocus(pm);
                        }
                        return;
                    }

                    var holder = void 0,
                        firstInput = void 0;

                    pm = __checkoutUIRef.pmFromType(pTxVariant);

                    var isCardType = pm && pm.inputDetails && Util._collectionContains(pm.inputDetails, 'type', 'cardToken').length; // Array.find not supported in IE

                    if (isCardType) {

                        // Regular use - set focus on the first input field
                        if (this.csfRef) {

                            // Get the first input field
                            holder = DOM._selectOne(__rootNode, '[data-pm="' + pTxVariant + '"]');
                            firstInput = DOM._selectOne(holder, '.chckt-input-field');

                            // If the first input is the holderName field & it's not in its default hidden state - set focus on it...
                            if (DOM._hasClass(firstInput, 'js-chckt-holdername') && !DOM._hasClass(firstInput.parentNode, 'chckt-form-label--hidden')) {

                                firstInput.focus();
                                return;
                            }

                            // ...otherwise we're setting focus on either the number field or, in case of recurring card, the CVC field
                            var isRecurringCard = isCardType && pm.inputDetails[0].recurring;

                            var hostedFieldId = isRecurringCard ? 'hostedSecurityCodeField' : 'hostedCardNumberField';

                            this.csfRef.setFocusOnFrame(pTxVariant, hostedFieldId);
                        }

                        return;
                    }

                    // NON - CARD SCENARIOS
                    if (pm.requiresShopperInfo) {
                        holder = DOM._selectOne(__rootNode, '[data-pm="' + pTxVariant + '"]');
                        firstInput = DOM._selectOne(holder, '.chckt-input-field') || DOM._selectOne(holder, '.chckt-select-box');
                        if (firstInput) {
                            firstInput.focus();
                        }
                    }
                }
            };

            that.csfRef = null;

            return that;
        }; // return checkoutAPI
    }); // __define
    'use strict';

    /* global __define */
    __define('parseSetupJSON', ['apiLog'], function (apiLog) {

        "use strict";

        var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

        return function (pData, pDocOrigin) {

            var that = {
                init: function init() {

                    if (!pData || typeof pData === 'string' && pData.length === 0) {
                        apiLog('Setup data object empty. Site origin: ' + pDocOrigin);
                    }

                    // Parse once: if data was a result of $.ajax, type='json' this will be sufficient
                    var parsedData = void 0;

                    try {
                        parsedData = JSON.parse(pData);
                    } catch (e) {
                        // What if it already a JSON object and, if it contains the right properties (like publicKeyToken), the JSON object we were expecting...
                        if ((typeof pData === 'undefined' ? 'undefined' : _typeof(pData)) === 'object' && pData.publicKeyToken) {

                            // ...then use it as is
                            parsedData = pData;
                        } else {

                            if (window.console && window.console.error) {
                                window.console.error('ERROR: ### parseSetupJSON::init:: e=', e);
                            }
                        }
                    }

                    // Parse again: data was a result of standard XMLHttpRequest or type='json' wasn't specified on a $.ajax call
                    if (typeof parsedData === 'string') {

                        parsedData = JSON.parse(parsedData);
                    }

                    var parseSuccess = false,
                        failureCause = void 0,
                        typeOfData = typeof parsedData === 'undefined' ? 'undefined' : _typeof(parsedData);

                    // Check state of parsed data object
                    switch (typeOfData) {

                        case 'undefined':
                            parseSuccess = false;
                            failureCause = 'not JSON';

                            apiLog('Parsing setup data object failed (undefined, data was not JSON). Site origin: ', pDocOrigin);
                            throw new Error("The JSON passed to chckt.checkout was not properly formatted as JSON (typeof data: undefined, data was not JSON)");

                        case 'boolean':
                            parseSuccess = false;
                            failureCause = 'loaded';

                            apiLog('Parsing setup data object failed (boolean, loading failure). Site origin: ' + pDocOrigin);
                            throw new Error("The JSON passed to chckt.checkout was not properly formatted as JSON (typeof data: boolean, loading failure)");

                        case 'string':
                            parseSuccess = false;
                            failureCause = 'parsed';

                            apiLog('Parsing setup data object failed (string, couldn\'t parse to object). Site origin: ' + pDocOrigin);
                            throw new Error("The JSON passed to chckt.checkout was not properly formatted as JSON (typeof data: string, couldn't parse to object)");

                        //case 'object':
                        default:

                            parseSuccess = true;
                            break;
                    }

                    if (!parseSuccess) {

                        return { success: parseSuccess, failureCause: failureCause, type: typeOfData };
                    } else {

                        return { success: parseSuccess, data: parsedData };
                    }
                }
            };

            return that;
        };
    });
    'use strict';

    /* global __define, chckt, _a$chckt */
    __define('processSetupJSON', ['Util', 'cardType', 'shims'], function (Util, CardType, Shims) {

        "use strict";

        return function (pPMData, pRecurringData, pConsolidateCards, pLoadingContext) {

            var __cardGroupTypes = [];

            // Clone paymentMethods array
            var __paymentMethods = Util._deepClone(pPMData);

            // Clone recurringDetails array
            var __recurringDetails = Util._deepClone(pRecurringData);

            var that = {

                // public method declared using "Method definitions"
                init: function init() {

                    var configOptionConsolidateCards = pConsolidateCards; //A CONFIG OPTION - whether all PM's with group.type === 'card' get lumped together into one 'Credit Card' PM

                    // LOOP THRU ALL PMs LOOKING FOR RECURRING CARDS
                    var recurringPMs = __findRecurringPMs(__paymentMethods, __recurringDetails, configOptionConsolidateCards);
                    var recurringCards = recurringPMs.recurringCards;
                    var mappedRecurringCards = recurringPMs.mappedRecurringCards;

                    // If we don't consolidate the cards but do have recurring cards of the type - store the unadultered version in this array
                    var originalCards = recurringPMs.originalCards;
                    //--

                    // CONSOLIDATE ALL PM 'CARD' TYPES INTO A SINGLE 'CREDIT CARD' PM
                    // By default excludes recurringCards (which get removed from the PM array)
                    if (configOptionConsolidateCards) {

                        __paymentMethods = __consolidateCards(__paymentMethods, __cardGroupTypes);
                    }
                    //--

                    // OPTION TO RE-ADD RECURRIING CARDS TYPES TO THE CONSOLIDATED CARD TYPE
                    // We need to do this to allow the shopper to pay with another card of the same type as a recurring one
                    var allowRecurringCCsInConsolidateCardType = true;
                    if (allowRecurringCCsInConsolidateCardType && configOptionConsolidateCards) {
                        __cardGroupTypes = __addRecurringCCsToConsoliated(mappedRecurringCards, __cardGroupTypes);
                    }

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### processSetupJSON::init:: __cardGroupTypes=', __cardGroupTypes);
                    }

                    // IF WE'RE NOT CONSOLIDATING THE CARDS...
                    if (!configOptionConsolidateCards) {

                        // ...RE-ADD ANY THAT WERE REMOVED DURING THE 'FIND RECURRING' PROCESS
                        __paymentMethods = __readdPMs(__paymentMethods, originalCards);

                        // ...AND ADD REFERENCE TO CARD SPECIFIC SECURITY CODE TEXT (a key the language module will use)
                        __paymentMethods = __giveIndivCardsSecurityCode(__paymentMethods);
                    }

                    // REARRANGE inputDetails ARRAYS OF CARDS SO THAT iframes WILL BE ADDED
                    // IN SUCH A WAY THAT WE INFLUENCE HOW TABBING THRU INPUTS WILL WORK (number>date>cvc)
                    __paymentMethods = __rearrangeCCInputDetails(__paymentMethods);
                    //--


                    // RE-ADD REMOVED RECURRING CARD PM'S TO THE FRONT OF THE PM ARRAY
                    __paymentMethods = __readdPMs(__paymentMethods, recurringCards);

                    //------------

                    //FILTER OUT UNSUPPORTED PAYMENT METHODS
                    __filterOutUnsupportedPms(['applepay', 'androidpay', 'samsungpay'], __paymentMethods);
                    //--

                    // FOR TESTING - limit num of paymentMethods
                    if (pLoadingContext && pLoadingContext.indexOf('localhost') > -1) {
                        __paymentMethods = __limitPMs(__paymentMethods);
                    }
                    //--

                    // Check for apple pay validity if ApplePay exists


                    // Look for creditcard payment methods
                    // - search all payment methods' inputDetails arrays to see if any have a 'cardToken' type
                    // This is a more generic way to look for credit cards than searching on a specific brand e.g. 'visa'
                    var hasCards = __paymentMethodsContains(__paymentMethods, 'cardToken', 'inputDetails');
                    var hasGiropay = __paymentMethodsContains(__paymentMethods, 'giropay');
                    var hasIBAN = __paymentMethodsContains(__paymentMethods, 'sepadirectdebit');

                    return {
                        processedPaymentMethods: __paymentMethods,
                        numRecurringCards: mappedRecurringCards.length,
                        cardGroupTypes: __cardGroupTypes,
                        mappedRecurringCards: mappedRecurringCards,
                        hasCards: hasCards,
                        hasGiropay: hasGiropay,
                        hasIBAN: hasIBAN
                    };
                } //that.init

            }; //that

            // Loop thru all pms looking for recurring pms. See comments below for details of what exactly happens...
            var __findRecurringPMs = function __findRecurringPMs(pPaymentMethods, pRecurringDetails, pConsoliateCards) {

                var recurringCards = [],
                    originalCards = [];
                var i = void 0,
                    j = void 0,
                    len = void 0,
                    pItem = void 0,
                    recPM = void 0,
                    tempRecPM = void 0;

                if (pRecurringDetails && pRecurringDetails.length) {

                    for (i = pPaymentMethods.length - 1; i > -1; i--) {

                        pItem = pPaymentMethods[i];

                        // TODO: currently we are saying that only PM's with group.type='card' can be recurring - now though there are other recurring types e.g. payPal

                        // If the PM's *group.type* = 'card'...
                        if (pItem.group && pItem.group.type && pItem.group.type === 'card') {

                            // ...check if the PM's top-level type (e.g. 'visa') equals the type of any item in the recurringDetails array...
                            var detailArray = Util._collectionContains(pRecurringDetails, 'type', pItem.type);

                            // ...if it does match...
                            if (detailArray.length) {

                                // ...remove it from the PM array so we can manipulate it as a 'recurring' PM
                                recPM = pPaymentMethods.splice(i, 1)[0];

                                // If we're not going to consolidate the cards we want to store an unadultered version of the PM that can be added back to the PM array later
                                if (!pConsoliateCards) {
                                    originalCards.push(Util._deepClone(recPM));
                                }
                            }

                            // For each recurring card of this type
                            for (var _iterator = detailArray.entries(), _isArray = Array.isArray(_iterator), _i = 0, _iterator = _isArray ? _iterator : _iterator[Symbol.iterator](); ;) {
                                var _ref;

                                if (_isArray) {
                                    if (_i >= _iterator.length) break;
                                    _ref = _iterator[_i++];
                                } else {
                                    _i = _iterator.next();
                                    if (_i.done) break;
                                    _ref = _i.value;
                                }

                                var _ref2 = _ref,
                                    index = _ref2[0],
                                    elem = _ref2[1];


                                // Clone the original recurring PM
                                tempRecPM = Util._deepClone(recPM);

                                // Replace the existing inputDetails array with the one stored on the recurringDetails object.
                                // NOTE: this only contains one item, a 'encryptedSecurityCode' object, so we can refer to it by index 0 in subsequent manipulations
                                tempRecPM.inputDetails = Util._deepClone(elem.inputDetails);

                                var origType = tempRecPM.type;

                                // Mark the type as 'recurring' using the _r@ suffix + a count var (there could be more than one recurring of the same card type)
                                tempRecPM.type = tempRecPM.type + '_r@' + (index + 1);

                                // The presence of a 'recurring' property will help the template-helper differentiate this from a normal card
                                // whilst the actual content of this property will contain the stored values needed to populate the non-editable parts of the template
                                tempRecPM.inputDetails[0].recurring = elem.card;

                                // Add reference to card specific security code text - if it is specified in the array of card types
                                var cardArr = Util._collectionContains(CardType.cards, 'cardType', origType);
                                if (cardArr.length) {
                                    tempRecPM.inputDetails[0].securityCode = cardArr[0].securityCode;
                                }

                                //                            tempRecPM.inputDetails[0].optional = true;//TODO - for TESTING ONLY

                                // Overwrite the payment method name with the number - for display in the header element of the PM
                                tempRecPM.name = '&#8226;&#8226;&#8226;&#8226; &#8226;&#8226;&#8226;&#8226; &#8226;&#8226;&#8226;&#8226; ' + tempRecPM.inputDetails[0].recurring.number;

                                // Overwrite the pm's paymentMethodData with that stored in the recurringDetail item
                                tempRecPM.paymentMethodData = elem.paymentMethodData;

                                // Store the removed PM item (to be readded later at the front of the PM array)
                                recurringCards.push(tempRecPM);
                            }
                        }
                        // TODO - handle other types of recurring pms (ideal, giropay?, sepa?)
                        else { }
                    }
                }

                // Map recurring cards to a simpler form - for merchant use in recurring card detection
                var mappedRecurringCards = Shims.map(recurringCards, function (pItem) {
                    // Destructure the heck out of pItem!
                    var type = pItem.type,
                        paymentMethodData = pItem.paymentMethodData;

                    return { type: type, paymentMethodData: paymentMethodData };
                });

                return { recurringCards: recurringCards, originalCards: originalCards, mappedRecurringCards: mappedRecurringCards };
            }; //__findRecurringPMs

            // Consolidate all pm 'card' types into a single 'credit card' pm
            var __consolidateCards = function __consolidateCards(pPaymentMethods, pCardGroupTypes) {

                var i = void 0,
                    pItem = void 0,
                    cardGroup = null;

                for (i = pPaymentMethods.length - 1; i > -1; i--) {

                    pItem = pPaymentMethods[i];

                    // If the PM's *group.type* = 'card'
                    if (pItem.group && pItem.group.type && pItem.group.type === 'card') {

                        pCardGroupTypes.push(pItem.type); // store types to help with identifying brands when validating the card no.

                        // If we haven't yet made a consolidated 'Credit card' PM - hijack the first one...
                        if (cardGroup === null) {

                            if (window._b$dl && window.console && window.console.log) {
                                window.console.log('### processSetupJSON::__consolidateCards:: Consolidating cards using', pItem.type);
                            }

                            cardGroup = pItem;
                            // Overwrite the PM's paymentMethodData with the group's paymentMethodData (which is the same for all items that share the group.type)
                            cardGroup.paymentMethodData = pItem.group.paymentMethodData;
                            cardGroup.name = pItem.group.name;
                            cardGroup.type = pItem.group.type;

                            var cvcItem = Util._collectionContains(cardGroup.inputDetails, 'key', 'encryptedSecurityCode');
                            if (cvcItem.length) {
                                cvcItem[0].optional = false; // Always false for Consolidated card (it's possible that the card we use as a 'template' has an optional cvc)
                            }

                            //                        cardGroup.inputDetails[1].optional = true;//TODO - for TESTING ONLY

                            //                        cardGroup.inputDetails.forEach(function(pItem){
                            //                            if(window.console && window.console.log){
                            //                                window.console.log('### processSetupJSON:::: cardGroup.inputDetails item=',pItem);
                            //                            }
                            //                        })
                        } else {
                            // ... and remove all the others
                            pPaymentMethods.splice(i, 1);
                        }
                    }
                }

                return pPaymentMethods;
            }; //__consolidateCards

            // When not consolidating the cards: add reference to card specific security code text - if it is specified in the array of card types
            var __giveIndivCardsSecurityCode = function __giveIndivCardsSecurityCode(pPaymentMethods) {

                for (var _iterator2 = pPaymentMethods, _isArray2 = Array.isArray(_iterator2), _i2 = 0, _iterator2 = _isArray2 ? _iterator2 : _iterator2[Symbol.iterator](); ;) {
                    var _ref3;

                    if (_isArray2) {
                        if (_i2 >= _iterator2.length) break;
                        _ref3 = _iterator2[_i2++];
                    } else {
                        _i2 = _iterator2.next();
                        if (_i2.done) break;
                        _ref3 = _i2.value;
                    }

                    var elem = _ref3;


                    // If the PM's *group.type* = 'card'
                    if (elem.group && elem.group.type && elem.group.type === 'card') {

                        var cvcItem = Util._collectionContains(elem.inputDetails, 'key', 'encryptedSecurityCode');
                        if (cvcItem.length) {

                            var cardArr = Util._collectionContains(CardType.cards, 'cardType', elem.type);
                            if (cardArr.length) {
                                cvcItem[0].securityCode = cardArr[0].securityCode;
                            }
                        }
                    }
                }

                return pPaymentMethods;
            }; //__giveIndivCardsSecurityCode

            // Add recurring cards into the types recognised by the general, consolidated card type
            var __addRecurringCCsToConsoliated = function __addRecurringCCsToConsoliated(pRecurringCards, pCardGroupTypes) {

                var recCCsArr = Shims.map(pRecurringCards, function (pItem) {
                    return pItem.type;
                });

                return pCardGroupTypes.concat(recCCsArr);
            };

            // REARRANGE inputDetails ARRAYS OF CARDS so that iframes will be added
            // in such a way that we influence how tabbing thru inputs will work (number>date>cvc)
            var __rearrangeCCInputDetails = function __rearrangeCCInputDetails(pPaymentMethods) {

                var pms = Util._deepClone(pPaymentMethods);

                // Identify which PMs have inputDetails relating to the 'cardToken' type of PM...
                // TODO - relies on the first item in the inputDetails array having type = 'cardToken'
                // TODO - if storeDetails or holderName ended up first then this would break
                var cardTypePMs = Shims.filter(pms, function (pPM) {
                    return pPM.inputDetails && pPM.inputDetails[0].type === 'cardToken';
                });

                // ...place all the inputDetails arrays from those PMs into a single array (an array-of-arrays)
                var allInputDetails = Shims.map(cardTypePMs, function (pCard) {
                    return pCard.inputDetails;
                });

                var inputSort = function inputSort(pInputDetailsArr) {

                    var len = pInputDetailsArr.length;

                    // Clone the original inputDetails array-of-arrays
                    var clonedInputDetails = Util._deepClone(pInputDetailsArr);

                    // Clear the original inputDetails array-of-arrays
                    pInputDetailsArr.splice(0, len);

                    var order = ['holderName', 'encryptedCardNumber', 'encryptedExpiryMonth', 'encryptedExpiryYear', 'encryptedSecurityCode', 'storeDetails'];

                    // Repopulate the original inputDetails array in the order we want
                    Shims.forEach(order, function (pItem) {

                        var tempArr = Util._collectionContains(clonedInputDetails, 'key', pItem);

                        if (tempArr.length) {
                            pInputDetailsArr.push(tempArr[0]);
                        }
                    });
                };

                Shims.forEach(allInputDetails, inputSort);

                // We have been manipulating the internal, inputDetails, arrays of each payment method - but the references
                // have stayed intact, so we can return the whole, cloned, paymentMethods array which now contains PMs with
                // correctly sorted inputDetails arrays
                return pms;
            }; //__rearrangeCCInputDetails


            /////////////// UTILS //////////////////////////////////////////

            // Util to concat 2 arrays
            var __readdPMs = function __readdPMs(pPaymentMethods, pOtherArray) {

                if (pOtherArray.length) {

                    return pOtherArray.concat(pPaymentMethods);
                }

                return pPaymentMethods;
            };

            // Util to Filter out unsupported payment methods
            var __filterOutUnsupportedPms = function __filterOutUnsupportedPms(pFilterArray, pPaymentMethods) {

                var i = void 0;

                for (i = pPaymentMethods.length - 1; i > -1; i--) {

                    var paymentMethod = pPaymentMethods[i];

                    if (paymentMethod.type) {

                        if (Shims.arrayIndexOf(pFilterArray, paymentMethod.type) > -1) {
                            pPaymentMethods.splice(i, 1);
                        }
                    }
                }
            };

            // FOR TESTING - limit num of paymentMethods
            var __limitPMs = function __limitPMs(pPaymentMethods) {

                var tempPmArray = [];

                if (typeof _a$chckt !== 'undefined' && _a$chckt.__testing_limitPaymentMethods) {

                    Shims.forEach(_a$chckt.__testing_limitPaymentMethods, function (pItem) {

                        var ltdPmArray = Shims.filter(pPaymentMethods, function (pPM) {
                            return pPM.type === pItem;
                        });
                        tempPmArray = tempPmArray.concat(ltdPmArray);
                    });
                    pPaymentMethods = tempPmArray.length ? tempPmArray : pPaymentMethods;
                }
                return pPaymentMethods;
            };

            // Util to inspect paymentMethods array to see if a one has a type with a specific value
            var __paymentMethodsContains = function __paymentMethodsContains(pPaymentMethods, pPMType, pSubArray) {

                var checkArrayForPMType = function checkArrayForPMType(pArray) {

                    return Shims.filter(pArray, function (pItem) {
                        return pItem.type === pPMType;
                    });
                };

                var checkSubArrayForPMType = function checkSubArrayForPMType() {

                    var subArr = Shims.map(pPaymentMethods, function (pPM) {

                        // If payment method has a sub array of the specified name...
                        if (pPM[pSubArray]) {

                            // ...filter this array for items with the right PM type
                            var filteredArr = checkArrayForPMType(pPM[pSubArray]);

                            return filteredArr.length ? filteredArr[0] : [];
                        }

                        return [];
                    });

                    // Filter the mapped array again to weed out empty arrays
                    return checkArrayForPMType(subArr);
                };

                var tempArr = pSubArray ? checkSubArrayForPMType() : checkArrayForPMType(__paymentMethods);

                return !!tempArr.length; // force to type Boolean
            };
            //---------------- end UTILS ------------------------------------


            // Return public 'interface' for this module
            return that;
        }; // returned function
    });
    /* global chckt*/

    (function (pWindow) {

        "use strict";

        var chckt = window.chckt = window.chckt || {};

        // Use onload to replace currency code with symbol?


        // Transforms currency code to html tag
        chckt.getCurrencySymbol = function (currencyCode) {

            if (typeof currencyCode !== 'string') {

                return undefined;

            } else {


                var code = currencyCode.toUpperCase();

                if (!__currencySymbolMap.hasOwnProperty(code)) {
                    return undefined;
                } else {
                    return __currencySymbolMap[code];
                }
            }
        };

        // Transform amount to currency specific format
        chckt.formatCurrency = function (currencyCode, locale, amount) {
            locale.replace("_", "-");
        };

        var __currencySymbolMap = {
            'AED': 'د.إ',
            'AFN': '؋',
            'ALL': 'L',
            'ANG': 'ƒ',
            'AOA': 'Kz',
            'ARS': '$',
            'AUD': '$',
            'AWG': 'ƒ',
            'AZN': '₼',
            'BAM': 'KM',
            'BBD': '$',
            'BDT': '৳',
            'BGN': 'лв',
            'BHD': '.د.ب',
            'BIF': 'FBu',
            'BMD': '$',
            'BND': '$',
            'BOB': 'Bs.',
            'BRL': 'R$',
            'BSD': '$',
            'BTC': '฿',
            'BTN': 'Nu.',
            'BWP': 'P',
            'BYR': 'p.',
            'BZD': 'BZ$',
            'CAD': '$',
            'CDF': 'FC',
            'CHF': 'Fr.',
            'CLP': '$',
            'CNY': '¥',
            'COP': '$',
            'CRC': '₡',
            'CUC': '$',
            'CUP': '₱',
            'CVE': '$',
            'CZK': 'Kč',
            'DJF': 'Fdj',
            'DKK': 'kr',
            'DOP': 'RD$',
            'DZD': 'دج',
            'EEK': 'kr',
            'EGP': '£',
            'ERN': 'Nfk',
            'ETB': 'Br',
            'EUR': '€',
            'FJD': '$',
            'FKP': '£',
            'GBP': '£',
            'GEL': '₾',
            'GGP': '£',
            'GHC': '₵',
            'GHS': 'GH₵',
            'GIP': '£',
            'GMD': 'D',
            'GNF': 'FG',
            'GTQ': 'Q',
            'GYD': '$',
            'HKD': '$',
            'HNL': 'L',
            'HRK': 'kn',
            'HTG': 'G',
            'HUF': 'Ft',
            'IDR': 'Rp',
            'ILS': '₪',
            'IMP': '£',
            'INR': '₹',
            'IQD': 'ع.د',
            'IRR': '﷼',
            'ISK': 'kr',
            'JEP': '£',
            'JMD': 'J$',
            'JPY': '¥',
            'KES': 'KSh',
            'KGS': 'лв',
            'KHR': '៛',
            'KMF': 'CF',
            'KPW': '₩',
            'KRW': '₩',
            'KYD': '$',
            'KZT': '₸',
            'LAK': '₭',
            'LBP': '£',
            'LKR': '₨',
            'LRD': '$',
            'LSL': 'M',
            'LTL': 'Lt',
            'LVL': 'Ls',
            'MAD': 'MAD',
            'MDL': 'lei',
            'MGA': 'Ar',
            'MKD': 'ден',
            'MMK': 'K',
            'MNT': '₮',
            'MOP': 'MOP$',
            'MUR': '₨',
            'MVR': 'Rf',
            'MWK': 'MK',
            'MXN': '$',
            'MYR': 'RM',
            'MZN': 'MT',
            'NAD': '$',
            'NGN': '₦',
            'NIO': 'C$',
            'NOK': 'kr',
            'NPR': '₨',
            'NZD': '$',
            'OMR': '﷼',
            'PAB': 'B/.',
            'PEN': 'S/.',
            'PGK': 'K',
            'PHP': '₱',
            'PKR': '₨',
            'PLN': 'zł',
            'PYG': 'Gs',
            'QAR': '﷼',
            'RMB': '￥',
            'RON': 'lei',
            'RSD': 'Дин.',
            'RUB': '₽',
            'RWF': 'R₣',
            'SAR': '﷼',
            'SBD': '$',
            'SCR': '₨',
            'SDG': 'ج.س.',
            'SEK': 'kr',
            'SGD': '$',
            'SHP': '£',
            'SLL': 'Le',
            'SOS': 'S',
            'SRD': '$',
            'SSP': '£',
            'STD': 'Db',
            'SVC': '$',
            'SYP': '£',
            'SZL': 'E',
            'THB': '฿',
            'TJS': 'SM',
            'TMT': 'T',
            'TND': 'د.ت',
            'TOP': 'T$',
            'TRL': '₤',
            'TRY': '₺',
            'TTD': 'TT$',
            'TVD': '$',
            'TWD': 'NT$',
            'TZS': 'TSh',
            'UAH': '₴',
            'UGX': 'USh',
            'USD': '$',
            'UYU': '$U',
            'UZS': 'лв',
            'VEF': 'Bs',
            'VND': '₫',
            'VUV': 'VT',
            'WST': 'WS$',
            'XAF': 'FCFA',
            'XBT': 'Ƀ',
            'XCD': '$',
            'XOF': 'CFA',
            'XPF': '₣',
            'YER': '﷼',
            'ZAR': 'R',
            'ZWD': 'Z$'
        };

    })(this);

    /* global chckt, __define, _a$deviceFingerprint, _a$LOCALHOST, csf, _a$checkoutShopperUrl */
    // NOTE: _a$[var name] are not actually global but exist at the top level scope that all the SDK code is run in. Including them above however keeps jsHint quiet(ish)
    __define('Checkout',
        ['DOM', 'apiLog', 'df', 'parseSetupJSON', 'processSetupJSON', 'templater', 'checkoutAPI', 'Util', 'shims', 'hook'],
        function (DOM, apiLog, dF, parseSetupJSON, processSetupJSON, templater, checkoutAPI, Util, Shims, hook) {
            "use strict";

            var checkout = function () {

                var that, __contentDiv, __origin, __loadingContext, __originKey, __checkoutAPI, __setupJsonObj, __csfPaymentMethodsConfig, __hasPayments = false, __isTestLoadingContext = false;
                var docOrigin = document.location.origin || (document.location.protocol + "//" + document.location.host);

                var __jsVn = '1.2.1';

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('*** checkoutMain::checkout:: document.location=', document.location);
                    window.console.log('*** checkoutMain::checkout:: docOrigin=', docOrigin);
                }

                // PUBLIC FACING OBJECT EXPOSING PUBLIC METHODS & PROPERTIES
                that = {};

                var init = function (pData, pEl, pConfigObj) {

                    var defaultConfig = {

                        // GLOBAL CONFIG VARS
                        initiatePayment: true,// Whether shopper pressing 'Pay' button goes on to make an Ajax call to the 'initiatePayment' endpoint
                        consolidateCards: true,// Whether all PM's with group.type === 'card' get lumped together into one 'Credit Card' PM
                        useDefaultCSS: true,// Whether we load the default css
                        initialPMCount: 3,// How many payment methods are initially shown in the list, if no recurring PMs are present
                        //                    context : 'test',// Whether to load validations, default css & iframe from 'test' context rather than the default 'live'

                        // PAYMENT METHOD SPECIFIC CONFIG VARS
                        paymentMethods: {
                            card: {
                                separateDateInputs: false,// Whether credit card pms will have separate month & year input fields
                                showOptionalHolderNameField: false,// Whether the optional card-holder-name field is shown for Credit card PMs
                                //                            showOptionalCvcField : false,
                                //                            showStoreDetails : true,
                                sfStyles: {},
                                placeholders: {}
                            }
                        }
                    };

                    // Merge properties from passed config obj into default
                    var configObj = Util._deepAssign(defaultConfig, pConfigObj);

                    // Overwrite strings on translations object
                    if (configObj.translations) {
                        chckt.setTranslation(configObj.translations);
                    }

                    // Check for silly business on user set initialPMCount (minus numbers, floats, 0)
                    configObj.initialPMCount = Math.abs(configObj.initialPMCount) | 0; // Math.floor
                    if (configObj.initialPMCount === 0) {
                        configObj.initialPMCount = 1;
                    }

                    // Store in case we have recurring cards, that all get removed, & the PM list gets re-rendered (Edge case!!)
                    var originalPMCount = configObj.initialPMCount;


                    //////// SET LOADING CONTEXT (WHERE TO LOAD VALIDATIONS, DEFAULT CSS & IFRAME FROM) ///////////////

                    // A mechanism whereby we can tell the SDK to load it's resources (css, validations, iframes) from
                    // the test environment rather than the (soon to be) default live environment
                    if (configObj.context && configObj.context.toLowerCase() === 'test') {

                        __isTestLoadingContext = true;

                        // NOTE: _a$checkoutShopperUrl is initialised when the SDK asset is served
                        _a$checkoutShopperUrl = window._a$checkoutShopperUrl = _a$checkoutShopperUrl.replace(/live/, 'test');
                    }
                    __loadingContext = _a$checkoutShopperUrl;


                    // USE GET PARAM TO OVERRIDE WHERE TO LOAD VALIDATIONS, DEFAULT CSS & IFRAME FROM
                    // (Useful whilst in development: allows us to run the SDK locally but load content from
                    // another environment e.g. 'checkoutshopper-test') (see wrappers/mainScopeCheck_scope.js for inserted code)
                    // RUN IN BROWSER WITH: localhost:3000?scope=test
                    var noopScopeCheck;


                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('\n*** checkoutMain::init:: __loadingContext=', __loadingContext);
                        window.console.log('\n*** checkoutMain::init:: _a$checkoutShopperUrl=', _a$checkoutShopperUrl);
                        window.console.log('*** checkoutMain::init:: window._a$checkoutShopperUrl=', window._a$checkoutShopperUrl);
                    }
                    //------------- end SET LOADING CONTEXT ----------------------------------------------------

                    var cssTag, head;

                    // ADD PROPRIETORIAL STYLESHEET, if it's not already there & merchant hasn't said not to use it
                    if (configObj.useDefaultCSS) {
                        var defChcktCss = document.querySelectorAll('link[href*="chckt-default.' + '1.2.1' + '.css"]');
                        if (!defChcktCss.length) {
                            cssTag = document.createElement('link');
                            cssTag.rel = 'stylesheet';
                            cssTag.type = 'text/css';
                            cssTag.href = __loadingContext + 'css/chckt-default.' + '1.2.1' + '.css';

                            // Add as first element to the head so user's stylesheets will override it
                            head = DOM._selectOne(document, 'head');
                            head.insertBefore(cssTag, head.childNodes[0]);
                        }
                    }
                    //--


                    // EXAMINE DOM FOR HOLDER
                    __contentDiv = DOM._selectOne(document.body, pEl);

                    if (!__contentDiv) {
                        if (window.console && window.console.error) {
                            window.console.error("ERROR: Can not find a node to attach Checkout to.\nPlease pass in a valid selector for an HTML node to hold the checkout elements e.g. checkout.chckt(JSON_RECEIVED_FROM_SETUP_CALL, '#checkout-div')");
                            return;
                        }
                    }
                    //--


                    // PARSE DATA OBJECT
                    var jsonParser = parseSetupJSON(pData, docOrigin);
                    var parsedData = jsonParser.init();

                    if (parsedData.success) {

                        __setupJsonObj = parsedData.data;

                        if (window._b$dl && window.console && window.console.log) {
                            window.console.log('### checkoutSDK::getVersionNumber:: __jsVn=', __jsVn);
                        }

                        var vnAsNumber = Number(__jsVn.replace(/\./g, ''));

                        // Properties present from vn 1.1.3 on
                        __setupJsonObj.SDKVersion = vnAsNumber; // used in paymentMethods template to add either <div> or <form>

                        __setupJsonObj.configByVersion = {};
                        //--


                        // Present from vn 1.1.3 on
                        switch (true) {

                            case vnAsNumber >= 113:
                                __setupJsonObj.addExtraTextEvents = true; // used in textFieldType & issuerListFieldType template to add extra key listeners
                                break;
                        }
                        //--

                        var cd = Util._deepClone(parsedData.data);// create clone here else regEx can't remove the console.log for some reason
                        if (window._b$dl && window.console && window.console.log) {
                            window.console.log('*** checkoutMain::init:: original cloned data=', cd);
                            window.console.log('*** checkoutMain::init:: parsedData=', parsedData.data);
                        }

                        // Catch any errors sent back from the returned json, log them & proceed no further
                        if (__setupJsonObj.errorCode) {
                            var errorMessage = 'unknown';

                            if (__setupJsonObj.message) {
                                errorMessage = __setupJsonObj.message;
                            }

                            throw new Error("Status: " + __setupJsonObj.status + " " + errorMessage + " errorCode: " + __setupJsonObj.errorCode);
                        }


                        // Instantiate API object - gives us access points to the PM list rendered by checkoutUI
                        __checkoutAPI = checkoutAPI();


                        // EXTRACT & STORE KEY VALUES FROM THE DATA OBJECT
                        __originKey = __setupJsonObj.originKey;
                        __origin = __setupJsonObj.origin;


                        // CHECK ORIGIN FROM SETUP RESPONSE MATCHES THE DOCUMENT ORIGIN
                        if (typeof __origin === 'undefined' || __origin && docOrigin !== __origin) {

                            apiLog("CORSViolation: siteOrigin doesn't match origin", { siteOrigin: docOrigin, origin: __origin, merchant: __setupJsonObj.payment.reference });
                            if (window.console && window.console.error) {
                                window.console.error("ERROR CORSViolation: siteOrigin doesn't match origin.", 'siteOrigin=', docOrigin, 'origin=', __origin);
                            }

                            return false;
                        }


                        // PLACE RETURNED HTML INTO THE DESIGNATED HOLDER
                        var tempHtml = __setupJsonObj.html;

                        // IE8 - won't put <link> or <script> tags into innerHTML see: http://stackoverflow.com/questions/1068517/why-cant-i-add-a-string-containing-a-script-tag-to-innerhtml-in-ie
                        // The key, it turns out is preceding the script tags with some non-script markup AND *including the line break* to separate them!??
                        tempHtml = '<p style="display:none;"></p><br />' + tempHtml;// FOR IE8

                        __contentDiv.innerHTML = tempHtml;

                        delete __setupJsonObj.html;// Now we have the html we can remove it from the model

                        if (parsedData.data.payment.shopperLocale) {
                            chckt.locale = parsedData.data.payment.shopperLocale;
                        } else {
                            chckt.locale = "en-US";
                        }

                        // Now we have locale we can find the placeholder texts for the CC inputs
                        for (var pm in configObj.paymentMethods) {
                            if (configObj.paymentMethods.hasOwnProperty(pm)) {
                                configObj.paymentMethods[pm].placeholders = setCCPlaceholders(configObj.paymentMethods[pm].placeholders);
                            }
                        }

                        // Now we have set the default placeholders we can store a 'mapped' version of
                        // the config.paymentMethods object ready to be sent to CSF
                        var propList = ['sfStyles', 'placeholders'];
                        __csfPaymentMethodsConfig = mapConfigPMsObject(configObj.paymentMethods, propList);


                        // Store copy of original paymentMethods object before we process/mutate it
                        __setupJsonObj.rawPaymentMethods = Util._deepClone(__setupJsonObj.paymentMethods);


                        // SEND DATA OBJECT OFF TO BE INSPECTED - for recurring & unsupported PMs & to consolidate cards into a single PM, if required
                        var processJSON = processSetupJSON(__setupJsonObj.paymentMethods, __setupJsonObj.recurringDetails, configObj.consolidateCards, __loadingContext);
                        var processResultsObj = processJSON.init();

                        // Overwrite the original paymentMethods with the new, processed version
                        __setupJsonObj.paymentMethods = processResultsObj.processedPaymentMethods;

                        // Store list of cards globally, built in version of CSF will access it
                        chckt.cardGroupTypes = processResultsObj.cardGroupTypes;

                        // Store list of recurringCard objects - contains 'type' & 'paymentMethodData'
                        var recurringCards = processResultsObj.mappedRecurringCards;

                        // If we have recurringDetails - only show these in the initial display of PMs
                        if (processResultsObj.numRecurringCards) {
                            configObj.initialPMCount = Math.min(processResultsObj.numRecurringCards, configObj.initialPMCount);
                        }


                        var desProps = ['paymentMethods', 'paymentData', 'payment', 'disableRecurringDetailUrl', 'originKey', 'initiationUrl', 'SDKVersion', 'addExtraTextEvents', 'logoBaseUrl'];
                        var mappedResponseObj = Util._mapObject(__setupJsonObj, desProps);
                        //                    if(window.console && window.console.log){
                        //                        window.console.log('### checkoutMain::init::(line:249):: mappedResponseObj=',mappedResponseObj);
                        //                    }

                        // SETUP TEMPLATES
                        templater.createTemplates(mappedResponseObj, configObj.paymentMethods);
                        //--

                        // ADD STYLESHEET PROVIDING CLASSES LINKED TO INTERNAL FUNCTIONALITY AROUND THE SELECTION OF PAYMENT METHODS
                        var chcktCss = document.querySelectorAll('link[href*="assets/css/' + __originKey + '/chckt.css"]');
                        if (!chcktCss.length) {

                            cssTag = document.createElement('link');
                            cssTag.rel = 'stylesheet';
                            cssTag.type = 'text/css';
                            cssTag.href = __loadingContext + 'assets/css/' + __originKey + '/chckt.css';

                            // Add as first element to the head so user's stylesheets will override it
                            head = DOM._selectOne(document, 'head');
                            head.insertBefore(cssTag, head.childNodes[0]);
                        }
                        //--

                        // BOOT PROCESS
                        var boot = function () {

                            var sdkRootNode = DOM._selectOne(document.body, '.chckt-sdk');

                            if (!sdkRootNode) {
                                if (window.console && window.console.log) {
                                    window.console.log('### checkoutMain::boot:: NO ROOT NODE - SETTING TIMEOUT');
                                }
                                return setTimeout(boot, 100);
                            }

                            // TODO - For ES6 - destructure configObj & simplify below (sdkRootNode, recurringCards etc)
                            var pmListConfigObj = {
                                sdkRootNode: sdkRootNode,
                                setupJsonObj: mappedResponseObj,
                                recurringCards: recurringCards,
                                originalPMCount: originalPMCount,
                                initiatePayment: configObj.initiatePayment,
                                paymentMethodsConfig: configObj.paymentMethods,
                                initialPMCount: configObj.initialPMCount,
                                setupSecureFields: setupSecureFields
                            };

                            __initPMList(processResultsObj, pmListConfigObj);

                            // Now they've been rendered and stored we can remove the script tags containing the templates
                            removeTemplateNodes(sdkRootNode, 'chckt-templates');

                        };// boot

                        if (!hook('', "beforePMListRender", DOM._selectOne(document.body, '.chckt-sdk'), mappedResponseObj.paymentMethods)) {
                            return false;
                        }

                        boot();

                    } else {
                        __contentDiv.innerHTML = '<span style="font-size:11px;">Error: data cannot be ' + parsedData.failureCause + ' (data type:' + parsedData.type + ')</span>';
                    }
                };// init


                var __initPMList = function (pProcessResultsObj, pPmListConfig) {

                    // Initialise the PM list
                    try {

                        __checkoutAPI.initPMList(pPmListConfig);

                        __hasPayments = true;

                    } catch (e) {

                        if (window._b$dl && window.console && window.console.log) {
                            window.console.log('*** checkoutSDK::boot:: [InitPayment error]', e);
                        }

                        apiLog('Initialising payment list failed (try failed)', { siteOrigin: docOrigin, merchant: __setupJsonObj.payment.reference });

                        __contentDiv.innerHTML = '<p class="co-error">Error loading payment form</p>';

                        return;
                    }

                    if (__hasPayments) {

                        // If there are creditcards...
                        if (pProcessResultsObj.hasCards) {

                            // Init the secureFields - now built into the SDK with Ant. Doing this fixes the issue where IE9 was loading all the content twice (the SDK, setupPaymemts.php etc as a result of a 'frame navigate' Initiator)
                            setupSecureFields();

                            loadScript(__loadingContext + 'js/validations/validations.card.holder.name.js?token=' + __originKey);

                        } else {

                            // Usually card process will trigger the first focus event - so only if there are no cards trigger it here
                            __checkoutAPI.selectedPMSetFocus();
                        }

                        if (pProcessResultsObj.hasGiropay) {
                            //                        loadScript('/js/sdk/release/validations/validations.giropay.js');// FOR TESTING w/o redeploy
                            loadScript(__loadingContext + 'js/validations/validations.giropay.js?token=' + __originKey);
                        }


                        if (pProcessResultsObj.hasIBAN) {
                            //                        loadScript('/js/sdk/release/validations/validations.iban.js');// FOR TESTING w/o redeploy
                            loadScript(__loadingContext + 'js/validations/validations.iban.js?token=' + __originKey);
                        }
                    }
                };// __initPMList


                // Init the secureFields
                var setupSecureFields = function () {

                    if (csf) {

                        if (window._b$dl && window.console && window.console.log) {
                            window.console.log('### checkoutMain::setupSecureFields:: __isTestLoadingContext=', __isTestLoadingContext);
                            window.console.log('### checkoutMain::setupSecureFields:: __loadingContext=', __loadingContext);
                        }

                        var csfSetupObj = {
                            configObject: __setupJsonObj,
                            rootNode: __contentDiv,
                            paymentMethods: __csfPaymentMethodsConfig, // Contains styling object & placeholders for securedFields inputs
                            // JUST FOR DEVELOPMENT - pass 'scoped' loadingContext to CSF
                            loadingContext: (__isTestLoadingContext) ? __loadingContext : null
                        };

                        var csfRef = csf(csfSetupObj)
                            .onLoad(function () {
                                __checkoutAPI.selectedPMSetFocus();
                            })
                            .onAllValid(function (pCallbackObj) {
                                chckt.togglePayButton(pCallbackObj.allValid);
                                chckt.pms[pCallbackObj.txVariant] = pCallbackObj.allValid;// Store valid state in central storage
                            })
                            .onBrand(function (pCallbackObj) {
                                setCCBrandImageAndText(pCallbackObj);
                            })
                            .onError(function (pCallbackObj) {
                                setCCErrors(pCallbackObj);
                            })
                            .onFocus(function (pCallbackObj) {
                                setFocus(pCallbackObj);
                            })
                            .onFieldValid(function (pCallbackObj) {

                                // Check any valid field has any errors cleared
                                if (pCallbackObj.status === 'valid') {
                                    setCCErrors({ fieldType: pCallbackObj.fieldType, error: '', markerNode: pCallbackObj.markerNode });
                                }
                            });

                        __checkoutAPI.csfRef = csfRef;// Inform checkoutAPI about CSF
                    }
                };//setupSecureFields

                var setCCBrandImageAndText = function (pCallbackObj) {

                    var parentPM = DOM._closest(pCallbackObj.markerNode, '.js-chckt-pm');

                    if (typeof pCallbackObj.brandImage !== 'undefined') {

                        // Set brand logo
                        var brandImg = parentPM.querySelector('img');

                        brandImg.src = __setupJsonObj.logoBaseUrl + pCallbackObj.brandImage;
                    }

                    if (typeof pCallbackObj.brandText !== 'undefined') {

                        var labelElem = parentPM.querySelector('.js-chckt-cvc-field-label');

                        labelElem.innerText = (pCallbackObj.brandText !== 'Security_code') ? pCallbackObj.brandText + ':' : chckt.getTranslation('creditCard.cvcField.title') + ':';
                    }
                };// setCCBrandImageAndText

                var setCCErrors = function (pCallbackObj) {

                    var holderDiv = DOM._selectOne(pCallbackObj.markerNode.parentNode, '[data-hosted-id="' + pCallbackObj.fieldType + '"]');

                    if (pCallbackObj.error !== '') {

                        // Add error classes
                        setErrorClasses(holderDiv, true);

                    } else if (pCallbackObj.error === '') {

                        setErrorClasses(holderDiv, false);
                    }
                };//setCCErrors

                var setFocus = function (pCallbackObj) {

                    var holderDiv = DOM._selectOne(pCallbackObj.markerNode.parentNode, '[data-hosted-id="' + pCallbackObj.fieldType + '"]'),
                        inputFieldLabel = Shims.previousElementSibling(holderDiv),
                        inputFieldFocusClass = ' chckt-input-field--focus',
                        labelFocusClass = ' chckt-form-label__text--focus';

                    setFocusClasses(holderDiv, pCallbackObj.focus, inputFieldFocusClass);
                    setFocusClasses(inputFieldLabel, pCallbackObj.focus, labelFocusClass);
                };


                var setErrorClasses = function (pNode, pSetErrors) {

                    if (pSetErrors) {

                        if (pNode.className.indexOf('js-chckt-input-field-error') === -1) {
                            pNode.className += " chckt-input-field--error";
                            pNode.className += " js-chckt-input-field-error";
                        }
                        return;
                    }

                    var newClassName;

                    // Remove errors
                    if (pNode.className.indexOf('js-chckt-input-field-error') > -1) {

                        newClassName = pNode.className.replace('chckt-input-field--error', '');
                        pNode.className = Shims.trim(newClassName);

                        newClassName = pNode.className.replace('js-chckt-input-field-error', '');
                        pNode.className = Shims.trim(newClassName);
                    }
                };//setErrorClasses

                var setFocusClasses = function (pNode, pSetFocus, focusClass) {

                    if (pSetFocus) {

                        if (pNode.className.indexOf(focusClass) === -1) {
                            pNode.className += focusClass;
                        }

                        return;
                    }

                    var newClassName;

                    // Remove focus
                    if (pNode.className.indexOf(focusClass) > -1) {

                        newClassName = pNode.className.replace(focusClass, '');
                        pNode.className = Shims.trim(newClassName);
                    }
                };//setFocusClasses

                var setCCPlaceholders = function (pPlaceholders) {

                    var cardPH = {};
                    var locale = chckt.locale;
                    var cgt = chckt.getTranslation;
                    var ph = pPlaceholders;

                    // If placeholder has been specified in the config object use that - otherwise pull it from the language file
                    cardPH.hostedCardNumberField = (ph && ph.hostedCardNumberField) ? ph.hostedCardNumberField : cgt('creditCard.numberField.placeholder', locale);
                    cardPH.hostedExpiryDateField = (ph && ph.hostedExpiryDateField) ? ph.hostedExpiryDateField : cgt('creditCard.expiryDateField.placeholder', locale);
                    cardPH.hostedExpiryMonthField = (ph && ph.hostedExpiryMonthField) ? ph.hostedExpiryMonthField : cgt('creditCard.expiryDateField.month.placeholder', locale);
                    cardPH.hostedExpiryYearField = (ph && ph.hostedExpiryYearField) ? ph.hostedExpiryYearField : cgt('creditCard.expiryDateField.year.placeholder', locale);
                    cardPH.hostedSecurityCodeField = (ph && ph.hostedSecurityCodeField) ? ph.hostedSecurityCodeField : cgt('creditCard.cvcField.placeholder', locale);

                    return cardPH;
                };

                // Copy configObj.paymentMethods object but include only certain properties from each PM found
                var mapConfigPMsObject = function (pConfigPMObj, pChosenPropList) {
                    var mappedObj = {};
                    for (var pm in pConfigPMObj) {
                        if (pConfigPMObj.hasOwnProperty(pm)) {
                            mappedObj[pm] = {};
                            for (var prop in pConfigPMObj[pm]) {
                                if (Util._contains(pChosenPropList, prop) && pConfigPMObj[pm].hasOwnProperty(prop)) {
                                    mappedObj[pm][prop] = Util._deepClone(pConfigPMObj[pm][prop]);
                                }
                            }
                        }
                    }
                    return mappedObj;
                };

                var removeTemplateNodes = function (rootNode, templateClass) {

                    var templateDiv = DOM._getElementsByClassName(rootNode, templateClass);

                    if (templateDiv[0]) {
                        rootNode.removeChild(templateDiv[0]);
                    }
                };

                var loadScript = function (pSrc) {

                    var valTag = document.createElement('script');
                    valTag.type = 'text/javascript';

                    valTag.src = pSrc;

                    DOM._selectOne(document, 'head').appendChild(valTag);
                };

                that.init = function (pData, pEl, pConfigObj) {

                    if (arguments.length < 2) {
                        if (window.console && window.console.error) {
                            window.console.error("ERROR: The expected arguments were not passed to checkout.\nPlease initiate checkout with at least 2 arguments: the JSON response object and an HTML node to hold checkout elements e.g. checkout.chckt(JSON_RECEIVED_FROM_SETUP_CALL, '.checkout-div')");
                            return;
                        }
                    }

                    init(pData, pEl, pConfigObj);

                    return that;
                };

                return that;
            };// checkout

            var getDeviceFingerprint = function () {

                // Call once to initiate the df process
                var df = dF();

                // Call a second time to get the actual df
                setTimeout(function () {

                    _a$deviceFingerprint = df();

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('*** checkoutMain::getDeviceFingerprint=', _a$deviceFingerprint);
                    }

                }, 20);
            };

            // GET DEVICE FINGERPRINT
            getDeviceFingerprint();
            //--

            return checkout().init;// = that.init
        });

    var checkout = __require('Checkout');// = that.init
    chckt.checkout = checkout;

}(this));
// NOTE: Babel transpiled version (https://babeljs.io/repl/) of checkoutInitiatePayment_es6.js
// Additional work required to move generated _typeof function down into the IIFE closure

/* global chckt, define, exports, module, self */
(function () {

    'use strict';
    var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

    /////////////////// Check globals - detection & export pattern from lodash 4.10.0 //////////////////////////////////////////

    /**
     * Checks if `value` is a global object.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {null|Object} Returns `value` if it's a global object, else `null`.
     */

    var checkGlobal = function checkGlobal(value) {
        return value && value.Object === Object ? value : null;
    };

    /** Used to determine if values are of the language type `Object`. */
    var objectTypes = {
        'function': true,
        'object': true
    };

    /** Detect free variable `exports`. */
    var freeExports = objectTypes[typeof exports === 'undefined' ? 'undefined' : _typeof(exports)] && exports && !exports.nodeType ? exports : undefined;

    /** Detect free variable `module`. */
    var freeModule = objectTypes[typeof module === 'undefined' ? 'undefined' : _typeof(module)] && module && !module.nodeType ? module : undefined;

    /** Detect the popular CommonJS extension `module.exports`. */
    var moduleExports = freeModule && freeModule.exports === freeExports ? freeExports : undefined;

    /** Detect free variable `global` from Node.js. */
    var freeGlobal = checkGlobal(freeExports && freeModule && (typeof global === 'undefined' ? 'undefined' : _typeof(global)) === 'object' && global);

    /** Detect free variable `self`. */
    var freeSelf = checkGlobal(objectTypes[typeof self === 'undefined' ? 'undefined' : _typeof(self)] && self);

    /** Detect free variable `window`. */
    var freeWindow = checkGlobal(objectTypes[typeof window === 'undefined' ? 'undefined' : _typeof(window)] && window);

    /** Detect `this` as the global object. */
    var thisGlobal = checkGlobal(objectTypes[_typeof(undefined)] && undefined);

    /**
     * Used as a reference to the global object.
     *
     * The `this` value is used if it's the global object to avoid Greasemonkey's
     * restricted `window` object, otherwise the `window` object is used.
     */
    var root = freeGlobal || freeWindow !== (thisGlobal && thisGlobal.window) && freeWindow || freeSelf || thisGlobal; // || Function('return this')();

    //---------------------------------------------------------------------------


    ///////////////////////// Utils /////////////////////////////////////////

    // Generator that allows iteration over an object using a for-of loop
    //    function* objValues(obj) {
    //        for (const key of Object.keys(obj)) {
    //            yield {prop : key, value : obj[key]};
    //        }
    //    }

    var Util = {};
    Util._flatten = function (data) {
        var prefix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : '';
        var target = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};


        if (typeof data === 'undefined' || data === null) {
            return data;
        }
        var res = target;

        // Uses generator function
        //        for ( const elem of objValues(data) ) {
        //
        //            if ( data.hasOwnProperty( elem.prop ) && null !== elem.value ) {
        //                const val = elem.value;
        //                if ( typeof val === "object" ) {
        //                    Util._flatten( val, (prefix) + elem.prop + ".", res );
        //                } else {
        //                    res[ (prefix) + elem.prop ] = val;
        //                }
        //            }
        //        }

        for (var i in data) {

            if (data.hasOwnProperty(i) && null !== data[i]) {
                var val = data[i];
                if ((typeof val === 'undefined' ? 'undefined' : _typeof(val)) === "object") {
                    Util._flatten(val, (prefix || '') + i + ".", res);
                } else {
                    res[(prefix || '') + i] = val;
                }
            }
        }

        return res;
    };

    Util._isArray = function (prop) {
        return (typeof prop === 'undefined' ? 'undefined' : _typeof(prop)) === "object" && prop !== null && Object.prototype.toString.call(prop) === "[object Array]";
    };

    Util._contains = function (haystack, needle) {
        if (typeof haystack === "undefined") {
            return false;
        }
        if (typeof haystack === "string") {
            return haystack.indexOf(needle) > -1;
        }
        if (Util._isArray(haystack)) {
            for (var c = haystack.length; c-- > 0;) {
                if (haystack[c] === needle) {
                    return true;
                }
            }
        }
        return haystack.hasOwnProperty && haystack.hasOwnProperty(needle);
    };

    // Inspect node, creating an array of all its child nodes
    // (limited to certain types/tagNames if an array of limiting types is specified)
    // e.g. var elems = Util._getChildElements(myNode, ['input', 'checkbox']);
    Util._getChildElements = function (pNode, pLtdTypesArray) {

        var childEls = [];

        var children = pNode.childNodes;
        // FOR IE use a "for-loop" rather than "for-of" - since the transpiling of the latter relies on Symbol.iterator
        // & Symbol isn't supported in IE
        for (var i = 0; i < children.length; i++) {

            var el = children[i];


            if (el.nodeType === 1 /*Node.ELEMENT_NODE*/) {

                if (!pLtdTypesArray) {

                    childEls.push(el);
                } else {

                    var type = (el.type || el.nodeName || el.tagName || '').toLowerCase();

                    // Avoid use of Array.find since it is not supported in IE
                    if (Util._contains(pLtdTypesArray, type)) {
                        childEls.push(el);
                    }
                }

                childEls = childEls.concat(Util._getChildElements(el, pLtdTypesArray));
            }
        }

        return childEls;
    };

    var Net = {};
    Net._ajax = function (options) {

        var params = null;
        var xhr = new XMLHttpRequest();
        xhr.open(options.method || 'GET', options.url);
        xhr.setRequestHeader("Content-type", options.contentType || "application/x-www-form-urlencoded");
        if (options.method === "POST" && options.hasOwnProperty('data')) {

            var data = options.data;
            params = [];
            if (typeof data === "string") {
                params = data;
            } else {
                // If data is not already a string - creates a url encoded query string
                for (var i in Util._flatten(data)) {
                    if (data.hasOwnProperty(i)) {

                        params.push(encodeURIComponent(i) + "=" + encodeURIComponent(data[i]));
                    }
                }
                params = params.join("&");
            }
        }

        xhr.onreadystatechange = function () {

            var DONE = 4; // readyState 4 means the request is done.
            var OK = 200; // status 200 is a successful return.

            if (xhr.readyState === DONE) {
                if (xhr.status === OK) {
                    if (typeof options.success === "function") {
                        options.success(xhr.responseText, xhr);
                    }
                } else if (typeof options.error === "function") {
                    options.error(xhr.statusText, xhr.status, xhr);
                }
            }
        };

        xhr.send(params);
    };

    var Shims = {};
    // IE8 Polyfill for Array.filter from: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
    Shims.filter = function (pArray, pFn, pThisArg) {

        var res = void 0;

        if (pArray.filter) {

            res = pArray.filter(pFn);
        } else {

            var t = Object(pArray);
            var len = t.length >>> 0;
            if (typeof pFn !== 'function') {
                throw new TypeError();
            }

            res = [];

            for (var i = 0; i < len; i++) {
                if (i in t) {

                    var val = t[i];
                    // NOTE: Technically this should Object.defineProperty at
                    //       the next index, as push can be affected by
                    //       properties on Object.prototype and Array.prototype.
                    //       But that method's new, and collisions should be
                    //       rare, so use the more-compatible alternative.
                    if (pFn.call(pThisArg, val, i, t)) {
                        res.push(val);
                    }
                }
            }
        }

        return res;
    };

    //---------------------------------------------------------------------------


    ///////////////////////// CheckoutSecureFields //////////////////////////////////////


    var chcktInitPayment = function chcktInitPayment() {

        var that = {};

        var init = function init(pConfigObj) {

            var responseData = pConfigObj.responseData;
            var pmType = pConfigObj.pmType;

            var configFormEl = pConfigObj.formEl;

            var formEl = configFormEl && (configFormEl.jquery || Util._isArray(configFormEl)) ? configFormEl[0] : configFormEl;

            //////////// NOTE: *** THE "7 STEPS" TO INITIATING A PAYMENT AGAINST THE ADYEN SERVER *** //////////////////////////////////

            // Check we've been passed a txvariant
            if (!pmType || typeof pmType !== 'string' || pmType.length === 0) {
                if (window.console && window.console.log) {
                    window.console.log('### checkoutInitiatePayment::init:: Cannot do an payment initiation without a txvariant');
                }
                return;
            }

            // Check we have a valid "form" element
            if (!formEl || !formEl.hasChildNodes()) {
                if (window.console && window.console.warn) {
                    window.console.warn('### checkoutInitiatePayment::init:: Cannot do a payment initiation - the form element is missing');
                }
                return;
            }

            // Check we have a url to POST to
            if (!responseData.initiationUrl) {
                if (window.console && window.console.warn) {
                    window.console.warn('### checkoutInitiatePayment::init:: Cannot do a payment initiation - the initiationUrl is missing');
                }
                return;
            }

            // 1. Create a request and a formData object
            var requestObj = {},
                formData = {},
                c = void 0,
                elem = void 0;

            // 2. Take the originKey from the setup response and add it to the request object as 'token'
            requestObj.token = responseData.originKey;

            // 3. Take the paymentData from the setup response and add it to the request object
            requestObj.paymentData = responseData.paymentData;

            var pm = Shims.filter(responseData.paymentMethods, function (pItem) {
                return pItem.type === pmType;
            }) // find the single item that matches
                .shift(); // remove it from the array that filter created

            if (!pm) {
                if (window.console && window.console.warn) {
                    window.console.warn('### checkoutInitiatePayment::init:: The method details for this method are not specified in the JSON');
                }
                return;
            }

            // 4. Take the chosen payment method's paymentMethodData and add it to the request object
            requestObj.paymentMethodData = pm.paymentMethodData;

            var formElTypes = ['radio', 'checkbox', 'text', 'textarea', 'hidden', 'select', 'select-one'];

            var formElements = Util._getChildElements(formEl, formElTypes);
            //            if(window.console && window.console.log){
            //                window.console.log('### checkoutInitiatePayment_es6::init:: formElements=',formElements);
            //            }


            // 5. Add the form's element's values to the formData object (name|value pairs)
            for (c = formElements.length - 1; c > -1; c--) {
                elem = formElements[c];

                // If element disabled OR doesn't have a name attribute then exclude it
                if (elem.disabled || !elem.name) {
                    continue;
                }

                var type = (elem.type || elem.nodeName || elem.tagName || '').toLowerCase();
                switch (type) {
                    case 'radio':
                    case 'checkbox':
                        if (elem.checked) {
                            formData[elem.name] = elem.value;
                        }
                        break;
                    case 'text':
                    case 'textarea':
                    case 'hidden':
                        var elementValue = void 0;

                        // TODO Exception for sepa, clean up later
                        if (elem.name === "sepa.ibanNumber") {
                            elementValue = elem.value.replace(/\s/g, "").toUpperCase();
                        } else {
                            elementValue = elem.value;
                        }

                        // TODO?? strip out spaces - elementValue still has length, if it does create formData prop
                        formData[elem.name] = elementValue;
                        break;
                    case 'select':
                    case 'select-one':
                        if (elem.selectedIndex > -1) {
                            formData[elem.name] = elem.options[elem.selectedIndex].value;
                        }
                        break;
                    //                    case 'file':
                    //                        return 'File input is not supported by the checkout API';
                    //                    default:
                    //                        return 'Unknown element type ' + type + ' for element ' + elem;
                }
            }

            //            if(window.console && window.console.log){
            //                window.console.log('### checkoutInitiatePayment_es6::init:: formData=',formData);
            //            }

            delete formData.txvariant;

            // If we don't use Object.keys() then we don't need a Shim for IE8
            //            var numDataKeys = 0;
            //            for(let key in formData){
            //                numDataKeys ++;
            //            }
            //            // NOTE: redirect PMs have no formData to submit
            //            if(numDataKeys === 0){
            //                if(window.console && window.console.warn){
            //                    window.console.warn('### checkoutInitiatePayment::init:: Cannot do a payment initiation - there is no formData to submit. formData=',formData);
            //                }
            //            }


            // 6. Add the formData object to the request object as 'paymentDetails'
            requestObj.paymentDetails = formData;

            // Add device fingerprint, if it exists (it's an SDK thang!)
            requestObj.deviceFingerprint = pConfigObj.deviceFingerprint || '';

            var initiationUrl = responseData.initiationUrl;

            // TODO - just for local testing in IE in SDK
            if (window.chckt && window.chckt.__testingInIE) {
                initiationUrl = responseData.initiationUrl.replace('localhost', window.chckt.__localhost);
            }

            var jsonData = JSON.stringify(requestObj);

            // 7a. Option to exit at this stage and just return the JSON string, or raw object, to the merchant so they can do their own server to server call
            if (pConfigObj.preventPost === true) {

                return jsonData;
            }

            // 7b. Submit the request object, as JSON, to the initiationURl from the setup response
            Net._ajax({
                url: initiationUrl,
                method: "POST",
                contentType: "application/json",
                data: jsonData,
                success: function success(text, xhr) {
                    try {
                        pConfigObj.onSuccess(JSON.parse(text));
                    } catch (e) {
                        pConfigObj.onError(e.message, [{ message: e.message, status: xhr.status }]);
                    }
                },
                error: function error(text, status, xhr) {
                    pConfigObj.onError(text, [{ message: text, status: status, xhr: xhr }]);
                }
            });

            return true;
        };

        // Public method
        that.init = function (pConfigObj) {
            return init(pConfigObj);
        };

        return that;
    };

    //---------------------------------------------------------------------------


    ///////////////////////// Export ////////////////////////////////////////////
    var cip = chcktInitPayment().init;

    // Expose checkoutSecureFields on the free variable `window` or `self` when available. This
    // prevents errors in cases where checkoutSecureFields is loaded by a script tag in the presence
    // of an AMD loader. See http://requirejs.org/docs/errors.html#mismatch for more details.
    (freeWindow || freeSelf || {}).chcktPay = cip;

    // Some AMD build optimizers like r.js check for condition patterns like the following:
    if (typeof define === 'function' && _typeof(define.amd) === 'object' && define.amd) {
        // Define as an anonymous module so, through path mapping, it can be
        // referenced as the "checkoutInitiatePayment" module.
        // e.g.
        // require.config({
        //   paths: { checkoutInitiatePayment : '...url to checkoutInitiatePayment WITHOUT the ".js" file extension'}
        // });
        //
        // require(['checkoutInitiatePayment'], function(cip){ cip(configObj); });
        define(function () {
            return cip;
        });
    }
    // Check for `exports` after `define` in case a build optimizer adds an `exports` object.
    else if (freeExports && freeModule) {
        // Export for Node.js.
        if (moduleExports) {
            (freeModule.exports = cip).chcktPay = cip;
        }
        // Export for CommonJS support.
        freeExports.chcktPay = cip;
    } else {
        // Export to the global object.
        root.chcktPay = cip;
    }
})();
/* global chckt, define, exports, module, self */
(function () {

    'use strict';

    /////////////////// Check globals - detection & export pattern from lodash 4.10.0 //////////////////////////////////////////

    /** Used to determine if values are of the language type `Object`. */
    var objectTypes = {
        'function': true,
        'object': true
    };

    /** Detect free variable `exports`. */
    var freeExports = (objectTypes[typeof exports] && exports && !exports.nodeType) ? exports : undefined;

    /** Detect free variable `module`. */
    var freeModule = (objectTypes[typeof module] && module && !module.nodeType) ? module : undefined;

    /** Detect the popular CommonJS extension `module.exports`. */
    var moduleExports = (freeModule && freeModule.exports === freeExports) ? freeExports : undefined;

    /** Detect free variable `global` from Node.js. */
    var freeGlobal = checkGlobal(freeExports && freeModule && typeof global == 'object' && global);

    /** Detect free variable `self`. */
    var freeSelf = checkGlobal(objectTypes[typeof self] && self);

    /** Detect free variable `window`. */
    var freeWindow = checkGlobal(objectTypes[typeof window] && window);

    /** Detect `this` as the global object. */
    var thisGlobal = checkGlobal(objectTypes[typeof this] && this);

    /**
     * Used as a reference to the global object.
     *
     * The `this` value is used if it's the global object to avoid Greasemonkey's
     * restricted `window` object, otherwise the `window` object is used.
     */
    var root = freeGlobal ||
        ((freeWindow !== (thisGlobal && thisGlobal.window)) && freeWindow) ||
        freeSelf || thisGlobal;// || Function('return this')();

    /**
     * Checks if `value` is a global object.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {null|Object} Returns `value` if it's a global object, else `null`.
     */
    function checkGlobal(value) {
        return (value && value.Object === Object) ? value : null;
    }

    //---------------------------------------------------------------------------


    // Create local, self-contained 'AMD' system to facilate modularisation of CSF code
    var noop = function () { return function () { }; };

    var shared = {}, __define = noop, __require = noop;


    ///////////////////////// CheckoutSecureFields //////////////////////////////////////

    var _a$listenerRef = null;// Holder for a permanent ref to the 'message' listener we will use

    // NOTE: _window._a$checkoutShopperUrl is initialised when the (SDK) asset is served
    var _a$adyenURL = window._a$checkoutShopperUrl;
    /* global shared, __define, __require */
    (function (exports) {

        "use strict";

        var modules = exports.__modules = {};

        function __require(name, optionalCallback) {

            if (!name) {
                return function () {
                };
            }

            if (typeof name === "string") {
                if (!modules.hasOwnProperty(name)) {
                    throw new Error("Adyen Sequencing Exception. Module '" + name + "' is not yet defined");
                }
                return modules[name];
            }

            var result = [];

            while (name.length > 0) {
                result.push(__require(name.shift()));
            }

            if (typeof optionalCallback === "function") {
                optionalCallback.apply({}, result);
            }

            return result;
        }

        function __define(name, deps, item) {
            var args = __require(deps);
            if (typeof item === "function") {
                modules[name] = item.apply({}, args);
            } else {
                modules[name] = item;
            }
        }

        exports.__require = __require;
        exports.__define = __define;

    }(shared));

    __define = shared.__define || __define;
    __require = shared.__require || __require;

    /* global __define */
    __define('Constants', [], function () {

        "use strict";
        var constants = {};

        // Constants matching the data-hosted-ids used in the html to identify hosted input field holders
        // These coincide with the ids of the inputs in the iframe html
        constants.__HOSTED_NUMBER_FIELD_STR = 'hostedCardNumberField';
        constants.__HOSTED_DATE_FIELD_STR = 'hostedExpiryDateField';
        constants.__HOSTED_MONTH_FIELD_STR = 'hostedExpiryMonthField';
        constants.__HOSTED_YEAR_FIELD_STR = 'hostedExpiryYearField';
        constants.__HOSTED_CVC_FIELD_STR = 'hostedSecurityCodeField';

        return constants;
    });
    /* global __define */

    /**
     * Since SDK is to be a distributable file and run in the merchant's environment
     * we should avoid modifying globals that we didn’t create, including polyfills for browser apis
     */
    __define('shims', [], function () {

        "use strict";
        var Shims = {};

        // For IE8
        Shims.forEach = function (pArray, pFn, pThisArg) {

            if (pArray.forEach) {

                pArray.forEach(pFn, pThisArg);

            } else {

                for (var i = 0, n = pArray.length; i < n; i++) {
                    if (i in pArray) {
                        pFn.call(pThisArg, pArray[i], i, pArray);
                    }
                }
            }
        };

        // For IE8
        Shims.filter = function (pArray, pFn, pThisArg) {

            var res;

            if (pArray.filter) {

                res = pArray.filter(pFn);

            } else {

                var t = Object(pArray);
                var len = t.length >>> 0;
                if (typeof pFn !== 'function') {
                    throw new TypeError();
                }

                res = [];
                for (var i = 0; i < len; i++) {
                    if (i in t) {
                        var val = t[i];

                        // NOTE: Technically this should Object.defineProperty at
                        //       the next index, as push can be affected by
                        //       properties on Object.prototype and Array.prototype.
                        //       But that method's new, and collisions should be
                        //       rare, so use the more-compatible alternative.
                        if (pFn.call(pThisArg, val, i, t)) {
                            res.push(val);
                        }
                    }
                }
            }

            return res;
        };

        return Shims;
    });
    /* global define */
    __define('DOM', [], function () {

        "use strict";
        var DOM = {};
        DOM._select = function (root, selector) {

            if (!root) {
                return [];
            }

            // Convert NodeList to array
            if (typeof root.querySelectorAll === "function") {
                return [].slice.call(root.querySelectorAll(selector));
            }

            // ELSE... IE8 - to convert StaticNodeList to array, from: https://jsperf.com/nodelist-to-array-ie8-compatible
            var arr = [];
            var n = root.querySelectorAll(selector);
            for (var z = n.length; z--;) {
                arr.unshift(n[z]);
            }
            return arr;
        };

        DOM._selectOne = function (root, selector) {

            if (!root) {
                return undefined;
            }

            return root.querySelector(selector);
        };

        DOM._closest = function (node, selectorString) {

            if (!Element.prototype.matches) {
                Element.prototype.matches =
                    Element.prototype.matchesSelector ||
                    Element.prototype.mozMatchesSelector ||
                    Element.prototype.msMatchesSelector ||
                    Element.prototype.oMatchesSelector ||
                    Element.prototype.webkitMatchesSelector ||
                    function (s) {
                        var matches = (this.document || this.ownerDocument).querySelectorAll(s),
                            i = matches.length;
                        while (--i >= 0 && matches.item(i) !== this) { }
                        return i > -1;
                    };
            }

            // Get closest match
            for (; node && node !== document; node = node.parentNode) {
                if (node.matches(selectorString)) {
                    return node;
                }
            }

            return null;
        };

        DOM._getAttribute = function (node, attribute) {

            if (!node) {
                return;
            }
            return node.getAttribute(attribute) || '';
        };

        DOM._on = function (node, event, callback, useCapture) {
            if (typeof node.addEventListener === "function") {
                node.addEventListener(event, callback, useCapture);
            } else {
                if (node.attachEvent) {
                    node.attachEvent("on" + event, callback);
                } else {
                    throw new Error(": Unable to bind " + event + "-event");
                }
            }
        };

        return DOM;
    });
    /* global define */
    __define('Utils', [], function () {

        "use strict";
        var Utils = {};

        Utils._isArray = function (prop) {
            return typeof prop === "object" && prop !== null && Object.prototype.toString.call(prop) === "[object Array]";
        };

        // Util class to check if a set of required elements are defined on an object TODO: make it pretty (if more than one property isn't specified, add to array and throw error after looping through)
        Utils.__checkSetupObject = function (configObject, necessaryPropertyArray) {

            for (var i = 0; i < necessaryPropertyArray.length; i++) {
                var property = necessaryPropertyArray[i];

                if (!configObject[property]) {
                    throw new Error("The property " + necessaryPropertyArray[i] + " is undefined");
                }
            }
        };

        Utils._capitaliseFirstLetter = function (string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        };

        return Utils;
    });
    /* global __define, _a$adyenURL, _a$listenerRef */
    __define('checkoutSecuredFields_config', ['DOM', 'Utils', 'Constants', 'shims'], function (DOM, Utils, Constants, Shims) {

        "use strict";

        /**
         *
         * @param pSharedScope - Object for sharing functionality between CSF modules
         * @param pSetupObj - CSF config object, passed when CSF is initiated against the global var csf
         * @param pSharedState - Object for sharing state between CSF modules
         */
        var module = function (pSharedScope, pSetupObj, pSharedState) {

            var DEFAULT_CARD_GROUP_TYPES = ['amex', 'mc', 'visa'];

            var __cardGroupTypes = [];

            var __configPMs;

            var __numIframes = 0;
            var __iframeCount = 0;

            var __sfLogAtStart = false;

            var __noop = function () { return null; };

            /**
             * Generate iframe
             * Find/Store rootNode - the element specified by the merchant as the parent for all checkout elements
             * Send call to place iframes in each hosted input field
             * If that is successful add listener for the window 'message' event
             */
            var __init = function () {

                if (!pSetupObj) {
                    if (window.console && window.console.error) {
                        window.console.error('ERROR: No securedFields configuration object defined');
                    }
                    return;
                }

                if (!pSetupObj.rootNode) {
                    if (window.console && window.console.error) {
                        window.console.error('ERROR: SecuredFields configuration object does not have a rootNode property');
                    }
                    return;
                }

                if (!pSetupObj.configObject) {
                    if (window.console && window.console.error) {
                        window.console.error('ERROR: SecuredFields configuration object does not have a configObject property');
                    }
                    return;
                }

                if (pSetupObj._b$dl === true) {
                    __sfLogAtStart = true;
                }

                // NOTE: pSetupObj = {rootNode, configObject, paymentMethods}; // AND loadingContext if 'scoping' loadingContext for testing in development
                var configObj = pSetupObj.configObject;

                // Configuration object for individual txVariants: based on default, overwritable by merchant
                // Contains styling object & placeholders for securedFields inputs
                __configPMs = pSetupObj.paymentMethods;


                // Just for development - use 'scoped' loadingContext from configObject
                // Only applies in situation where CSF is part of SDK: _a$adyenURL is undefined if SDK is run from 'local', compiled directory
                // AND exists but doesn't contain '.com' if SDK is served from localhost
                if (pSetupObj.loadingContext) {

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutSecuredFields_config::__init:: _a$adyenURL=', _a$adyenURL);
                        window.console.log('### checkoutSecuredFields_config::__init:: pSetupObj.loadingContext=', pSetupObj.loadingContext);
                    }
                    _a$adyenURL = pSetupObj.loadingContext;
                }

                // If running as part of SDK window.chckt will contain a cardGroupTypes object...
                // ...otherwise check for passed cardGroupTypes OR create cardGroupTypes array by processing the passed paymentMethods object

                // c. Agents
                var checkSDKCardGroupTypes = function (pCardGroupTypes) {

                    return (Utils._isArray(pCardGroupTypes)) ? pCardGroupTypes : [];
                };

                // CSF not running as part of SDK: see if we've been passed an array of cardGroupTypes. If not - create one from the list of paymentMethods
                var createOwnCardGroupTypes = function (pCardGroupTypes, pPaymentMethods) {

                    return (Utils._isArray(pCardGroupTypes)) ? pCardGroupTypes : __getCardPms(pPaymentMethods);
                };

                // a. Situation
                var hasCardListFromSDK = (window.chckt && window.chckt.cardGroupTypes);

                // b. Decision
                __cardGroupTypes = (hasCardListFromSDK) ? checkSDKCardGroupTypes(window.chckt.cardGroupTypes) : createOwnCardGroupTypes(configObj.cardGroupTypes, configObj.paymentMethods);

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_config::init:: cardGroupTypes=', __cardGroupTypes);
                }


                ////////// COMMENT IN FOR IE TESTING OF CSF-STANDALONE //////////
                // e.g. 192.168.56.1 (Terminal > ifconfig > vboxnet0 > inet)
                //            _a$adyenURL = 'http://192.168.56.1:8080/checkoutshopper/';
                //---------------------------------------------------------------

                var csfPublicKeyToken = '';

                if (configObj.publicKeyToken) {
                    csfPublicKeyToken = '?pkt=' + configObj.publicKeyToken;
                }

                var iframeSrc = _a$adyenURL + 'assets/html/' + configObj.originKey + '/securedFields.' + '1.1.1' + '.html' + csfPublicKeyToken;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('\n############################');
                    window.console.log('### checkoutSecuredFields_config::init:: configObj= + csfPubliKeyToken', configObj + ' csfPKT= ' + csfPublicKeyToken);
                    window.console.log('### checkoutSecuredFields_config::init:: iframeSrc=', iframeSrc);
                    window.console.log('### checkoutSecuredFields_config::init:: _a$adyenURL=', _a$adyenURL);
                }

                var iframeHtml = '<iframe src="' + iframeSrc + '" class="js-iframe" frameborder="0" scrolling="no" allowtransparency="true" style="border: none; height: 100%; width: 100%;"><p>Your browser does not support iframes.</p></iframe>';

                // Expect to be sent the actual html node...
                if (typeof pSetupObj.rootNode === 'object') {

                    pSharedState.rootNode = pSetupObj.rootNode;

                } else if (typeof pSetupObj.rootNode === 'string') {

                    // ... but if only sent a string - find it ourselves
                    pSharedState.rootNode = document.querySelector(pSetupObj.rootNode);

                    if (!pSharedState.rootNode) {
                        if (window.console && window.console.error) {
                            window.console.error('ERROR: SecuredFields cannot find a valid rootNode element');
                        }
                        return;
                    }
                }
                //--

                __numIframes = __populateIframes(iframeHtml);

                (__numIframes) ? __addMessageListeners() : __noop();

                //TEST
                //            setTimeout(function(){ pScope.__sendValueToFrame('card', 'hostedSecurityCodeField', '737');},2000);
            };

            /**
             * Create & populate state object - mostly used to store and detect validity of individual fields and the PM form as a whole
             * Detect hosted input fields & place iframes in each one
             * Store loaded iframe's contentWindow (the Window object of the <iframe> element)
             * Add listener for iframe 'load' event
             *
             * @param pIframeHtml: the iframe html, pointing to Adyen hosted content, that will be placed into each hosted input field
             * @private
             */
            var __populateIframes = function (pIframeHtml) {

                var secureFields = DOM._select(pSharedState.rootNode, '[data-hosted-id]');

                // Need a shim for IE8
                Shims.forEach(secureFields, function (pItem) {

                    // Check that the SF holder is located within an element with the correct identifying class...
                    var form = DOM._closest(pItem, '.js-chckt-pm__pm-holder');

                    // ...if this is not the case, for backward compatibility, check that the SF holder sits in a form element
                    if (!form) {
                        form = DOM._closest(pItem, 'form');
                    }

                    var txVariant = form.querySelector('[name="txvariant"]').value;// e.g. 'card' OR 'mc', 'visa' etc...

                    var hostedId = DOM._getAttribute(pItem, 'data-hosted-id');// e.g. 'hostedCardNumberField', 'hostedExpiryDateField', 'hostedSecurityCodeField'
                    var cseKey = DOM._getAttribute(pItem, 'data-cse');// e.g. 'encryptedCardNumber', 'encryptedExpiryDate', 'encryptedSecurityCode'

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('\n### checkoutSecuredFields_config:::: hostedId=', hostedId, 'cseKey=', cseKey);
                    }

                    var optional = DOM._getAttribute(pItem, 'data-optional'); // true | false

                    // Usually false for single cards but with exceptions e.g. maestro
                    // Always false for consolidated cards at start up.
                    // Subsequent information about whether cvc is optional now comes from SF, in the Brand information (as the shopper inputs the CC number)
                    var cvcIsOptional = (hostedId === Constants.__HOSTED_CVC_FIELD_STR && optional === 'true');

                    // MAKE *ONE* STORAGE OBJECT FOR EACH txVariant. (See comments in 'core' file, where pSharedState.txVariantStateObject variable is declared)
                    __createStateObject(pSharedState.txVariantStateObject, txVariant);

                    // MAKE *ONE* STORAGE OBJECT FOR EACH txVariant TO STORE iframe REFS
                    // NOTE: kept separate from state so we can, if we want, make all state change functions 'pure' i.e. non-mutating.
                    // (That can't work while we have the iframe refs since these are Window objects and not so not
                    // deep-clonable - we need to maintain the original ref)
                    if (!pSharedState.txVariantIframeStore[txVariant]) {
                        pSharedState.txVariantIframeStore[txVariant] = {};
                    }

                    //////// INITIALLY POPULATE STATE STORAGE OBJECT with false values, indicating field validity ///////////
                    __populateStateObject(pSharedState.txVariantStateObject, txVariant, hostedId, cvcIsOptional);

                    var iframe, iframeContent;

                    // Place the iframe into the holder
                    pItem.innerHTML = pIframeHtml;

                    // Now examine the holder to get an actual DOM node
                    iframe = DOM._selectOne(pItem, '.js-iframe');

                    if (iframe) {

                        iframeContent = iframe.contentWindow;

                        // Store ref to specific, secureField, iframeContent
                        pSharedState.txVariantIframeStore[txVariant][hostedId + '_iframe'] = iframeContent;

                        DOM._on(iframe, 'load', __onIFrameLoaded(txVariant, hostedId, cseKey, iframeContent), false);
                    }
                });// forEach

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('\n### checkoutSecuredFields_config::__populateIframes:: pSharedState.txVariantStateObject=', pSharedState.txVariantStateObject);
                    window.console.log('\n### checkoutSecuredFields_config::__populateIframes:: pSharedState.txVariantIframeStore=', pSharedState.txVariantIframeStore);
                }

                return secureFields.length;
            };//__populateIframes

            /**
             * Listener for iframe 'load' event
             * Creates and sends config object to the secureField script that is intialised when the iframe loads
             * Calls callback function if all iframes have loaded
             *
             * @param pTxVariant - config var for secureFields
             * @param pHostedId - config var for secureFields
             * @param pCseKey - config var for secureFields
             * @returns {Function}
             * @private
             */
            var __onIFrameLoaded = function (pTxVariant, pHostedId, pCseKey) {

                return function () {

                    var dataObj = {
                        txVariant: pTxVariant,
                        fieldType: pHostedId,
                        cseKey: pCseKey,
                        cardGroupTypes: __cardGroupTypes,
                        pmConfig: (__configPMs) ? __configPMs[pTxVariant] || __configPMs.card : {},
                        sfLogAtStart: __sfLogAtStart
                    };
                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('\n############################');
                        window.console.log('### checkoutSecuredFields_config::iframeLoaded:: dataObj=', dataObj);
                        window.console.log('### checkoutSecuredFields_config::iframeLoaded:: _a$adyenURL=', _a$adyenURL);
                    }

                    pSharedScope.postMessageToIframe(pTxVariant, pHostedId, dataObj);

                    __iframeCount++;

                    if (__iframeCount === __numIframes) {

                        pSharedScope.onLoadCallback({ iframesLoaded: true });
                    }
                };
            };

            var __addMessageListeners = function () {

                var isNotIE8 = (window.addEventListener) ? true : false;

                if (_a$listenerRef) {

                    (isNotIE8) ? window.removeEventListener("message", _a$listenerRef, false) : window.detachEvent("onmessage", _a$listenerRef);
                }

                // Create reference to listener we're about to use. We will need it if we want to remove the listener at any stage
                _a$listenerRef = pSharedScope.iframePostMessageListener;

                (isNotIE8) ? window.addEventListener("message", pSharedScope.iframePostMessageListener, false) : window.attachEvent("onmessage", pSharedScope.iframePostMessageListener);
            };

            var __createStateObject = function (pTxVariantStateObject, pTxVariant) {

                //TODO - implement a 'pure function' approach of not mutating anything outside of the fn's scope

                if (!pTxVariantStateObject[pTxVariant]) {

                    pTxVariantStateObject[pTxVariant] = {
                        brand: (pTxVariant !== 'card') ? pTxVariant : null,
                        actualValidStates: {},
                        currentValidStates: {},
                        allValid: false
                    };
                }
                return pTxVariantStateObject;
            };

            var __populateStateObject = function (pStateObject, pTxVariant, pHostedId, pCvcIsOptional) {

                // Store whether CVC is a required field
                if (pHostedId === Constants.__HOSTED_CVC_FIELD_STR) {
                    pStateObject[pTxVariant].cvcIsOptional = pCvcIsOptional;
                }

                // Exception for 'hostedExpiryDateField' - make separate field
                function makeYearAndMonthFields() {

                    pSharedScope.setValidState(pStateObject, pTxVariant, 'year', false);
                    pSharedScope.setValidState(pStateObject, pTxVariant, 'month', false);

                    return pStateObject;
                }

                var isDateField = (pHostedId === Constants.__HOSTED_DATE_FIELD_STR);

                // Regular, non-date, use case is for 'hostedCardNumberField', 'hostedSecurityCodeField', // 'hostedHolderField' (not yet implemented)
                return (isDateField) ? makeYearAndMonthFields() : pSharedScope.setValidState(pStateObject, pTxVariant, pHostedId, false);
            };

            // Create an array of cardGroupTypes from the list of paymentMethods, else resort to a default list
            var __getCardPms = function (pPaymentMethods) {

                var i, pItem, cardGroupTypes = [], len = (pPaymentMethods) ? pPaymentMethods.length : 0;

                for (i = 0; i < len; i++) {

                    pItem = pPaymentMethods[i];

                    // If the PM's *group.type* = 'card'
                    if (pItem.group && pItem.group.type && pItem.group.type === 'card') {

                        cardGroupTypes.push(pItem.type);// store types to help with identifying brands when validating the card no.
                    }
                }

                if (!cardGroupTypes.length) {
                    cardGroupTypes = DEFAULT_CARD_GROUP_TYPES;
                }

                return cardGroupTypes;
            };

            __init();
        };

        return module;
    });
    /* global __define, _a$adyenURL */
    __define('checkoutSecuredFields_handleSFNew', ['DOM', 'Utils', 'Constants', 'shims'], function (DOM, Utils, Constants, Shims) {

        "use strict";

        /**
         *
         * @param pSharedScope - Object for sharing functionality between CSF modules
         * @param pSharedState - Object for sharing state between CSF modules
         */
        var module = function (pSharedScope, pSharedState) {

            var __noop = function () { return null; };

            var that = {};

            /**
             * Listener for Window.postMessage.
             * A procedural routine that takes the data sent from secureFields and performs a set of actions based on the that data
             * The main driver for action after a user input information into a secureField
             *
             * @param event - the event sent from the postMessage from a secureFields instance
             * @private
             */
            that.iframePostMessageListener = function (event) {

                // DO ORIGIN CHECK - EXIT IF FAILED /////////////
                var origin = event.origin || event.originalEvent.origin;

                var adyenDomain = _a$adyenURL.substring(0, _a$adyenURL.indexOf('/checkoutshopper/'));

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('\n############################');
                    window.console.log('### checkoutSecuredFields_handleSFNew::__iframePostMessageListener:: event origin=', origin);
                    window.console.log('### checkoutSecuredFields_handleSF::__iframePostMessageListener:: page origin=', adyenDomain);
                }

                if (origin !== adyenDomain) {

                    if (window.console && window.console.error) {
                        window.console.error('ERROR checkoutSecuredFields::__iframePostMessageListener:: origin mismatch event origin=', origin, ' page origin=', adyenDomain);
                    }
                    return;
                }
                //--

                // PARSE DATA OBJECT
                var feedbackObj = JSON.parse(event.data);

                // AGENT TO DISTINGUISH BETWEEN VALIDATION & FOCUS EVENTS
                var handleValidationOrFocus = function (pFeedbackObj) {

                    var isFocusEvent = (pFeedbackObj.action === 'focus');

                    (isFocusEvent) ? __handleFocus(pFeedbackObj) : __handleValidation(pFeedbackObj);
                };

                // DISTINGUISH BETWEEN VALIDATION/FOCUS & ENCRYPTION EVENTS
                var isEncryptionEvent = (feedbackObj.action === 'encryption' && feedbackObj.encryptionSuccess === true);

                (isEncryptionEvent) ? __handleSuccessfulEncryption(feedbackObj) : handleValidationOrFocus(feedbackObj);
            };


            var __handleValidation = function (pFeedbackObj) {

                var callbackObj, isExpiryDateField;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('\n### checkoutSecuredFields_handleSF::__handleValidation:: pFeedbackObj=', pFeedbackObj);
                }

                // EXTRACT VARS
                var txVariant = pFeedbackObj.txVariant;
                var originalFieldType = pFeedbackObj.fieldType;

                // If we are dealing with a single expiryDate field a successful encryption of the date-input will yields 2 calls with originalFieldType values 'month' & 'year' respectively.
                // Any other date-input related processing i.e. errors, will have a single originalFieldType: 'hostedExpiryDateField'
                // Either way - make sure the local adjustedFieldType var is given the value 'hostedExpiryDateField' if it's a related to a date input
                // For other inputs, including separate month & year fields, it can just keep its original value
                var adjustedFieldType = (originalFieldType === 'month' || originalFieldType === 'year') ? Constants.__HOSTED_DATE_FIELD_STR : originalFieldType;


                // CHECK IF CVC IS OPTIONAL
                __checkOptionalCvcStatus(pFeedbackObj);


                // FIND FORM ELEMENTS
                var markerResObj = __getMarkerAndForm(txVariant);
                var markerNode = markerResObj.markerNode;
                var parentForm = markerResObj.parentForm;


                // PROCESS ERRORS
                callbackObj = __processErrors(pFeedbackObj, markerNode, adjustedFieldType);
                pSharedScope.onErrorCallback(callbackObj);

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__handleValidation:: error callbackObj=', callbackObj);
                }


                // PROCESS CARD BRANDS
                callbackObj = __processBrand(pFeedbackObj, markerNode);

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__handleValidation:: brand callbackObj=', callbackObj);
                }

                if (callbackObj) {
                    pSharedScope.onBrandCallback(callbackObj);
                }



                // SET VALID STATE OF INDIVIDUAL INPUT TO FALSE, REMOVE ANY EXISTING ENCRYPTED ELEMENT & CHECK VALIDITY OF THE FORM AS A WHOLE
                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__handleValidation:: 5c removing valid states!!!!');
                    window.console.log('### checkoutSecuredFields_handleSF::__handleValidation:: 5c adjustedFieldType=', adjustedFieldType);
                }

                // c. Agent(s)
                var removeValidStatesOnExpiryDates = function () {

                    // For hostedExpiryDateField removes valid states/encrypted elemetns for BOTH month & year
                    var callbackObj1 = __removeValidState(pSharedState.txVariantStateObject, 'month', parentForm, txVariant, adjustedFieldType, markerNode);
                    var callbackObj2 = __removeValidState(pSharedState.txVariantStateObject, 'year', parentForm, txVariant, adjustedFieldType, markerNode);

                    // If the 'invalid' state we're setting represents a change of state - call Callback fn
                    (callbackObj1) ? pSharedScope.onFieldValidCallback(callbackObj1) : __noop();
                    (callbackObj2) ? pSharedScope.onFieldValidCallback(callbackObj2) : __noop();
                };

                var removeValidStateOnField = function () {

                    callbackObj = __removeValidState(pSharedState.txVariantStateObject, originalFieldType, parentForm, txVariant, adjustedFieldType, markerNode);

                    // If the 'invalid' state we're setting represents a change of state - call Callback fn
                    (callbackObj) ? pSharedScope.onFieldValidCallback(callbackObj) : __noop();
                };
                //-- end Agents

                // a. Situation
                isExpiryDateField = (adjustedFieldType === Constants.__HOSTED_DATE_FIELD_STR);

                // b. Decision using Agents(c)
                callbackObj = (isExpiryDateField) ? removeValidStatesOnExpiryDates() : removeValidStateOnField();


                // STORE VALID STATE OF THE FORM AS A WHOLE ///////
                callbackObj = __assessFormValidity(txVariant);


                // BROADCAST VALID STATE OF THE FORM AS A WHOLE
                pSharedScope.onAllValidCallback(callbackObj);
            };


            var __handleSuccessfulEncryption = function (pFeedbackObj) {

                var isExpiryDateField, callbackObj;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('\n### checkoutSecuredFields_handleSF::__handleSuccessfulEncryption:: pFeedbackObj=', pFeedbackObj);
                }

                // EXTRACT VARS
                var txVariant = pFeedbackObj.txVariant;
                var originalFieldType = pFeedbackObj.fieldType;
                var cseKey = pFeedbackObj.cseKey;

                // If we are dealing with a single expiryDate field a successful encryption of the date-input will yields 2 calls with originalFieldType values 'month' & 'year' respectively.
                // So make sure the local adjustedFieldType var is given the value 'hostedExpiryDateField' if it's a related to a date input
                // For other inputs, including separate month & year fields, it can just keep its original value
                var adjustedFieldType = (originalFieldType === 'month' || originalFieldType === 'year') ? Constants.__HOSTED_DATE_FIELD_STR : originalFieldType;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__handleSuccessfulEncryption:: encryptionSuccess - originalFieldType=', originalFieldType);
                }


                // FIND FORM ELEMENTS
                var markerResObj = __getMarkerAndForm(txVariant);
                var markerNode = markerResObj.markerNode;
                var parentForm = markerResObj.parentForm;


                // SET FOCUS ON OTHER INPUT - If user has just typed a correct date/year - set focus on the cvc field OR correct month - focus on year field
                (originalFieldType === 'year' || originalFieldType === Constants.__HOSTED_YEAR_FIELD_STR) ? pSharedScope.setFocusOnFrame(txVariant, Constants.__HOSTED_CVC_FIELD_STR, 'yearSet') : __noop();
                (originalFieldType === Constants.__HOSTED_MONTH_FIELD_STR) ? pSharedScope.setFocusOnFrame(txVariant, Constants.__HOSTED_YEAR_FIELD_STR) : __noop();
                //--


                // ADD HIDDEN INPUT TO PARENT FORM ELEMENT
                // The cseKey for the 'year' & 'month' inputDetail objects ends up as 'encryptedExpiryDate'
                // (since we use a single 'date' field template, forcing the key to 'encryptedExpiryDate')
                // So for the year & month: construct a new key, rather than just using the cseKey, in order to create
                // an encrypted field with the expected name (as defined by the setup response > inputDetails)
                isExpiryDateField = (originalFieldType === 'year' || originalFieldType === 'month');
                var myCSEKey = (isExpiryDateField) ? 'encryptedExpiry' + Utils._capitaliseFirstLetter(originalFieldType) : cseKey;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__handleSuccessfulEncryption:: myCSEKey=', myCSEKey);
                }

                __addEncryptedElement(parentForm, myCSEKey, pFeedbackObj[cseKey], 'encrypted-' + originalFieldType);
                //--


                // REMOVE ANY ERRORS ON FIELD e.g. was a full number that failed the luhnCheck, then we corrected the number and now it passes
                callbackObj = __processErrors({ error: '' }, markerNode, adjustedFieldType);
                pSharedScope.onErrorCallback(callbackObj);

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__handleSuccessfulEncryption:: error callbackObj=', callbackObj);
                }


                // SET VALID STATE OF INDIVIDUAL INPUT TO TRUE
                pSharedScope.setValidState(pSharedState.txVariantStateObject, txVariant, originalFieldType, true);


                // BROADCAST VALID STATE OF INDIVIDUAL INPUT
                pSharedScope.onFieldValidCallback({
                    singleField: true,
                    fieldType: adjustedFieldType,
                    status: 'valid',
                    type: txVariant,
                    markerNode: markerNode,// What do we do with this? - see comments on __getMarkerAndForm
                    originalFieldType: originalFieldType// necessary for distinguishing between month and year in expiryDate field
                });


                // CHECK FOR CARD BRAND INFORMATION (Only a number related feedbackObj will contain brand info & then only after pasting a full number)
                if (pFeedbackObj.hasBrandInfo) {

                    // Remake feedbackObj dropping encryption data (roll on ES6 destructuring!!)
                    // We just need this object to set brand through the normal route, checking cvcOptional status along the way
                    var simplifedFeedbackObj = {
                        fieldType: adjustedFieldType,
                        txVariant: txVariant,
                        imageSrc: pFeedbackObj.imageSrc,
                        brand: pFeedbackObj.brand,
                        cvcText: pFeedbackObj.cvcText,
                        cvcIsOptional: pFeedbackObj.cvcIsOptional
                    };

                    // CHECK IF CVC IS OPTIONAL
                    __checkOptionalCvcStatus(simplifedFeedbackObj);


                    // PROCESS CARD BRANDS
                    callbackObj = __processBrand(simplifedFeedbackObj, markerNode);

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutSecuredFields_handleSF::__handleSuccessfulEncryption:: brand callbackObj=', callbackObj);
                    }

                    if (callbackObj) {
                        pSharedScope.onBrandCallback(callbackObj);
                    }
                }


                // STORE VALID STATE OF THE FORM AS A WHOLE ///////
                callbackObj = __assessFormValidity(txVariant);


                // BROADCAST VALID STATE OF THE FORM AS A WHOLE
                pSharedScope.onAllValidCallback(callbackObj);
            };


            var __handleFocus = function (pFeedbackObj) {

                var markerResObj = __getMarkerAndForm(pFeedbackObj.txVariant);

                pFeedbackObj.markerNode = markerResObj.markerNode;
                pSharedScope.onFocusCallback(pFeedbackObj);
            };

            ///////////////////////// HELPERS FOR VALIDATION & ENCRYPTION ROUTINES ////////////////////////////

            // CHECK IF CVC IS OPTIONAL: Brand information (from setting the CC number) now contains information about
            // whether cvc is optional for that brand e.g. maestro
            // If it is optional, and we're dealing with the consolidated card type,
            // (re)set the flags that indicate this & affect when the form is considered valid
            var __checkOptionalCvcStatus = function (pFeedbackObj) {

                var txVariant = pFeedbackObj.txVariant;
                var isConsolidatedCard = (txVariant === 'card');

                if (isConsolidatedCard && pFeedbackObj.hasOwnProperty('cvcIsOptional')) {

                    // Note if passed value is different
                    var isDiff = (pFeedbackObj.cvcIsOptional !== pSharedState.txVariantStateObject[txVariant].cvcIsOptional);

                    // Set the overall flag for this txvariant - used by pSharedScope.setValidState to see if it should force a 'true' value on the cvc field (because it is optional)
                    pSharedState.txVariantStateObject[txVariant].cvcIsOptional = pFeedbackObj.cvcIsOptional;

                    // Set 'base' validity of the cvc field
                    // Use case: in a consolidated card, switching between txvariants where the cvc is, & isn't, optional...
                    // ... but only if the new broadcast value is different from the stored value should we update the state object
                    if (isDiff) {

                        if (window._b$dl && window.console && window.console.log) {
                            window.console.log('### checkoutSecuredFields_handleSF::__handleValidation:: BASE VALUE OF cvcIsOptional HAS CHANGED feedbackObj.cvcIsOptional=', pFeedbackObj.cvcIsOptional);
                        }

                        pSharedScope.setValidState(pSharedState.txVariantStateObject, txVariant, Constants.__HOSTED_CVC_FIELD_STR, pFeedbackObj.cvcIsOptional, true);
                    }
                }
            };

            // If consolidated card type AND passed brand doesn't equal stored brand - send the new brand to the cvc input
            // Create object for CSF Brand Callback fn with image & text details
            var __processBrand = function (pFeedbackObj, pMarkerNode) {

                var callbackObj;

                var txVariant = pFeedbackObj.txVariant;
                var originalFieldType = pFeedbackObj.fieldType;

                if (originalFieldType === Constants.__HOSTED_NUMBER_FIELD_STR) {

                    var isConsolidatedCard = (txVariant === 'card');

                    // If we have a new brand send it to the CVC input
                    var newBrand = __checkForBrandChange(pFeedbackObj.brand, txVariant, pSharedState.txVariantStateObject);
                    if (isConsolidatedCard && newBrand) {
                        pSharedState.txVariantStateObject[txVariant].brand = newBrand;
                        __sendBrandToFrame(txVariant, Constants.__HOSTED_CVC_FIELD_STR, newBrand);
                    }

                    // Check for brand related properties
                    //                var shouldProcessBrand = (originalFieldType === Constants.__HOSTED_NUMBER_FIELD_STR && isConsolidatedCard);
                    //                callbackObj = (shouldProcessBrand) ? __setBrandImageAndText(pFeedbackObj) : __noop();
                    callbackObj = (isConsolidatedCard) ? __setBrandImageAndText(pFeedbackObj) : __noop();

                    // Return object to send to Callback fn
                    if (callbackObj) {

                        callbackObj.markerNode = pMarkerNode;

                        return callbackObj;
                    }
                }

                return null;
            };

            // NOTE: the primary purpose of the markerNode is for CSF to use it identify
            // the containing form element in order to  attach the hidden inputs holding the encrypted card data.
            // A secondary purpose is, that once detected, we can send this 'markerNode' to the CSF callback functions (error, brand, fieldValid, focus)
            // - where it can be used to identify where the securedFields holding elements are e.g. in order to attach error states
            var __getMarkerAndForm = function (pTxVariant) {

                var markerNode, parentForm,
                    pmNodes = DOM._select(pSharedState.rootNode, '[name="txvariant"]');

                // Identify the hidden input further up the DOM with the correct txvariant value - we will use this as a marker to identify the correct form element
                markerNode = Shims.filter(pmNodes, function (pItem) {
                    return pItem.value === pTxVariant;// find the single item that matches
                }).shift();// remove it from the array that Shims.filter created

                // Use the markerNode to identify the element to which we want to append (or remove) the fields containing encrypted data
                // It should be an element with the correct identifying class...
                parentForm = DOM._closest(markerNode, '.js-chckt-pm__pm-holder');

                // ...if this is not the case, for backward compatibility, check for a form element
                if (!parentForm) {
                    parentForm = DOM._closest(markerNode, 'form');
                }

                return { markerNode: markerNode, parentForm: parentForm };
            };

            var __processErrors = function (pFeedbackObj, pMarkerNode, pFieldType) {

                var dataObj = { markerNode: pMarkerNode, fieldType: pFieldType };

                var isError = pFeedbackObj.hasOwnProperty('error') && pFeedbackObj.error !== '';
                dataObj.error = (isError) ? pFeedbackObj.error : '';

                return dataObj;
            };

            // Should always refer to the 'current' valid states
            // Set validity flags back to false & remove any encrypted fields for a particular field type
            var __removeValidState = function (pStateObject, pFieldType, pParentForm, pTxVariant, pAdjustedFieldType, pMarkerNode) {

                if (pStateObject[pTxVariant].currentValidStates[pFieldType]) {

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('### checkoutSecuredFields_handleSF:: __removeValidState:: REMOVE :: pFieldType=', pFieldType);
                    }

                    pSharedScope.setValidState(pStateObject, pTxVariant, pFieldType, false);

                    var encryptedElem = DOM._selectOne(pParentForm, '#encrypted-' + pFieldType);

                    if (encryptedElem) {
                        pParentForm.removeChild(encryptedElem);
                    }

                    return {
                        singleField: true,
                        fieldType: pAdjustedFieldType,
                        status: 'invalid',
                        type: pTxVariant,
                        markerNode: pMarkerNode,
                        originalFieldType: pFieldType
                    };
                }

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('### checkoutSecuredFields_handleSF::__removeValidState:: NOTHING TO REMOVE :: pFieldType=', pFieldType);
                }

                return null;
            };

            // Adds hidden input to parent form element.
            // This input has a attribute 'name' whose value equals a 'cseKey' e.g. encryptedCardNumber
            // and an attribute 'value' whose value is the encrypted data blob for that field
            var __addEncryptedElement = function (pForm, pName, pData, pId) {

                var element = DOM._selectOne(pForm, '#' + pId);

                if (!element) {
                    element = document.createElement('input');
                    element.type = 'hidden';
                    element.name = pName;
                    element.id = pId;
                    pForm.appendChild(element);
                }

                element.setAttribute('value', pData);
            };

            var __assessFormValidity = function (pTxVariant) {

                var isValid = __checkFormIsValid(pSharedState.txVariantStateObject, pTxVariant);
                pSharedState.txVariantStateObject[pTxVariant].allValid = isValid;

                if (window._b$dl && window.console && window.console.log) {
                    window.console.log('\n### checkoutSecuredFields_handleSF::__assessFormValidity:: assesing valid states of the form as a whole isValid=', isValid);
                }

                return { allValid: isValid, txVariant: pTxVariant };
            };


            ///////////////////////////////// TASKS (TO HELP THE HELPERS!) ////////////////////////////////////

            // If brand sent with feedbackObj doesn't equal stored brand - extract the new brand ready to send to the cvc field
            var __checkForBrandChange = function (pBrand, pTxVariant, pStateObj) {

                if (pBrand && pBrand !== pStateObj[pTxVariant].brand) {

                    return pBrand;
                }
                return false;
            };

            var __setBrandImageAndText = function (pFeedbackObj) {

                var dataObj = {};
                var hasProps = false;

                if (typeof pFeedbackObj.brand !== 'undefined') {

                    dataObj.brandImage = pFeedbackObj.imageSrc;
                    dataObj.brand = pFeedbackObj.brand;
                    hasProps = true;
                }

                if (typeof pFeedbackObj.cvcText !== 'undefined') {

                    dataObj.brandText = pFeedbackObj.cvcText;
                    hasProps = true;
                }

                return (hasProps) ? dataObj : null;
            };

            var __sendBrandToFrame = function (pTxVariant, pField, pBrand) {

                // ...perform postMessage to send brand on specified field
                var dataObj = { txVariant: pTxVariant, fieldType: pField, brand: pBrand };
                pSharedScope.postMessageToIframe(pTxVariant, pField, dataObj);
            };

            // Should always refer to the 'actual' valid states
            var __checkFormIsValid = function (pStateObject, pTxVariant) {

                for (var key in pStateObject[pTxVariant].actualValidStates) {

                    if (pStateObject[pTxVariant].actualValidStates.hasOwnProperty(key) && // first part keeps jsHint happy
                        !pStateObject[pTxVariant].actualValidStates[key]) {// 2nd part checks if any validities are false

                        return false;
                    }
                }

                return true;
            };

            return that;
        };

        return module;
    });
    /* global _a$adyenURL, __define */
    __define('checkoutSecureFields_core',
        ['checkoutSecuredFields_config', 'checkoutSecuredFields_handleSFNew', 'Constants'],
        function (CSFConfig, CSFHandleSF, Constants) {

            var checkoutSecureFields = function () {

                //        var chckt = window.chckt || {};
                //        var localhost = chckt.__localhost || 'localhost';

                // Object for sharing functionality between modules
                var __sharedScope = {};

                // Object for sharing state between modules
                var __sharedState = void 0;

                // Object providing public interface for CSF
                var that = {};

                //**** FUNCTION THAT IS EXPOSED ON THE GLOBAL VAR csf ****
                var __init = function (pSetupObj) {

                    if (!pSetupObj) {
                        throw new Error('No securedFields configuration object defined');
                    }

                    // Set here in case of re-rendering templates (SDK use-case)
                    __sharedState = {};
                    __sharedState.rootNode = null;// set in config process

                    // STORAGE FOR WHICH FIELD TYPES ARE IN USE FOR WHICH PARTICULAR VARIATION OF THE CC FORM
                    // - an object will be stored under txVariant (e.g. 'visa', 'mc', 'card') containing a 'actualValidStates' object which will have some or all of the keys: hostedCardNumberField, hostedSecurityCodeField, hostedHolderField, year, month
                    // We can use the booleans stored under these keys to assess whether a form has been fully completed or not
                    // Another object is also stored, 'currentValidStates', describing whether a particular field is currently valid.
                    // NOTE: A field can be 'actually' valid whilst still be currently 'invalid' e.g. in the case of an optional cvc field - it is always 'actually' valid since it is not required, but if the shopper starts to fill it out
                    // anyway then it will switch current states from 'invalid' to 'valid' as it is completed.
                    // The 'current' state is used to assess whether to remove existing encrypted inputs and whether to callback reporting the state of a particular field.
                    // The 'actual' state is used to assess whether a form has been sufficiently completed such that it can now be submitted.
                    //
                    // This object also stores the txVariant's current cardBrand & a ref to each of the secureField's iframe's contentWindows
                    __sharedState.txVariantStateObject = {};
                    __sharedState.txVariantIframeStore = {};
                    //--

                    // Handle communication from SF
                    var sfHandler = CSFHandleSF(__sharedScope, __sharedState);

                    // Store iframe listener on shared object
                    __sharedScope.iframePostMessageListener = sfHandler.iframePostMessageListener;

                    // Setup & config
                    CSFConfig(__sharedScope, pSetupObj, __sharedState);
                };


                ////////////////////////// TASKS /////////////////////////////////////////////////

                var __sendValueToFrame = function (pTxVariant, pField, pInputTxt) {

                    // ...perform postMessage to send value to set in specified field
                    var dataObj = { txVariant: pTxVariant, fieldType: pField, setValue: pInputTxt };
                    __sharedScope.postMessageToIframe(pTxVariant, pField, dataObj);
                };

                var __set_b$dl = function (pTxVariant, pField, pB$dl) {

                    var dataObj = { txVariant: pTxVariant, fieldType: pField, _b$dl: pB$dl };
                    __sharedScope.postMessageToIframe(pTxVariant, pField, dataObj);
                };


                /////// SHARED TASKS - USED BY Core, CSFConfig & CSFHandleSF MODULES ///////
                // By exposing these methods on the __sharedScope object they remain accessible
                // to the core as well as to the config & handleSF modules

                // Core & handleSF
                __sharedScope.setFocusOnFrame = function (pTxVariant, pField, pReason) {

                    // ...perform postMessage to set focus on specified field
                    var dataObj = { txVariant: pTxVariant, fieldType: pField, focus: true };

                    var doSetFocus = (pField === Constants.__HOSTED_CVC_FIELD_STR && __sharedState.txVariantStateObject[pTxVariant].cvcIsOptional) ? false : true;

                    if (window._b$dl && window.console && window.console.log) {
                        window.console.log('\n############################');
                        window.console.log('### checkoutSecuredFields_core::setFocusOnFrame:: doSetFocus=', doSetFocus);
                    }

                    if (doSetFocus) {

                        __sharedScope.postMessageToIframe(pTxVariant, pField, dataObj);
                    }
                };

                // Core, config & handleSF
                __sharedScope.postMessageToIframe = function (pTxVariant, pField, pDataObj) {

                    var iframe = __sharedState.txVariantIframeStore[pTxVariant][pField + '_iframe'];

                    // In some cases the iframe might not exist e.g. bcmc which has no cvc field
                    if (iframe) {

                        var dataObjStr = JSON.stringify(pDataObj);
                        __sharedState.txVariantIframeStore[pTxVariant][pField + '_iframe'].postMessage(dataObjStr, _a$adyenURL);
                    }
                };


                // Config & handleSF
                __sharedScope.setValidState = function (pStateObject, pTxVariant, pHostedId, pVal, pDontSetCurrentState) {

                    pStateObject[pTxVariant].actualValidStates[pHostedId] = pVal;

                    if (!pDontSetCurrentState) {

                        pStateObject[pTxVariant].currentValidStates[pHostedId] = pVal;
                    }

                    // Exception: If cvc is optional always mark its 'actual' validState as true
                    if (pStateObject[pTxVariant].cvcIsOptional && pHostedId === Constants.__HOSTED_CVC_FIELD_STR) {
                        pStateObject[pTxVariant].actualValidStates[pHostedId] = true;
                    }

                    return pStateObject;
                };

                // Config
                __sharedScope.onLoadCallback = function () { };

                // handleSF
                __sharedScope.onAllValidCallback = function () { };
                __sharedScope.onFieldValidCallback = function () { };
                __sharedScope.onBrandCallback = function () { };
                __sharedScope.onErrorCallback = function () { };
                __sharedScope.onFocusCallback = function () { };

                //------ end SHARED TASKS - USED BY CSFConfig &/OR CSFHandleSF MODULES --------


                //////////////// PUBLIC METHODS - EXPOSED WHEN CSF IS INITIALISED ///////////////
                that.init = function (pSetupObj) {
                    __init(pSetupObj);
                    return that;
                };

                // Set focus on speciifc frame e.g. when form first loads
                that.setFocusOnFrame = function (pTxVariant, pHostedFieldId) {

                    __sharedScope.setFocusOnFrame(pTxVariant, pHostedFieldId, 'Card PM selected');
                };

                ////// EVENTS THE USER (the one who initialises CSF) CAN SUBSCRIBE TO
                that.onLoad = function (pCallbackFn) {
                    __sharedScope.onLoadCallback = pCallbackFn;
                    return that;
                };

                that.onAllValid = function (pCallbackFn) {
                    __sharedScope.onAllValidCallback = pCallbackFn;
                    return that;
                };

                that.onFieldValid = function (pCallbackFn) {
                    __sharedScope.onFieldValidCallback = pCallbackFn;
                    return that;
                };

                that.onBrand = function (pCallbackFn) {
                    __sharedScope.onBrandCallback = pCallbackFn;
                    return that;
                };

                that.onError = function (pCallbackFn) {
                    __sharedScope.onErrorCallback = pCallbackFn;
                    return that;
                };

                that.onFocus = function (pCallbackFn) {
                    __sharedScope.onFocusCallback = pCallbackFn;
                    return that;
                };

                that._b$dl = function (pTxVariant, pHostedFieldId, pB$dl) {
                    __set_b$dl(pTxVariant, pHostedFieldId, pB$dl);
                };
                //--

                // Util
                that.getPaymentMethodDataByPm = function (pPms, pBrand) {

                    var len = pPms.length, pm = null;

                    for (var i = len; i-- > 0;) {

                        if (pPms[i].type === pBrand) {

                            pm = pPms[i];
                            break;
                        }
                    }

                    return pm.paymentMethodData;
                };

                //------- end PUBLIC METHODS - EXPOSED WHEN CSF IS INITIALISED -------

                return that;
            };

            return checkoutSecureFields;
        }
    );
    ///////////////////////// Export ////////////////////////////////////////////
    var chktSF = __require('checkoutSecureFields_core');// require CSF
    var sf = chktSF().init;// initialise CSF and get ref to that.init

    // Expose checkoutSecureFields on the free variable `window` or `self` when available. This
    // prevents errors in cases where checkoutSecureFields is loaded by a script tag in the presence
    // of an AMD loader. See http://requirejs.org/docs/errors.html#mismatch for more details.
    (freeWindow || freeSelf || {}).csf = sf;

    // Some AMD build optimizers like r.js check for condition patterns like the following:
    if (typeof define === 'function' && typeof define.amd === 'object' && define.amd) {
        // Define as an anonymous module so, through path mapping, it can be
        // referenced as the "checkoutSecureFields" module.
        // e.g.
        // require.config({
        //   paths: { checkoutSecureFields : '...url to checkoutSecureFields WITHOUT the ".js" file extension'}
        // });
        //
        // require(['checkoutSecureFields'], function(sF){ sF(configObj); });
        define(function () {
            return sf;
        });
    }
    // Check for 'exports' after 'define' in case a build optimizer adds an 'exports' object.
    else if (freeExports && freeModule) {
        // Export for Node.js.
        if (moduleExports) {
            (freeModule.exports = sf).csf = sf;
        }
        // Export for CommonJS support.
        freeExports.csf = sf;
    }
    else {
        // Export to the global object.
        root.csf = sf;
    }

}());