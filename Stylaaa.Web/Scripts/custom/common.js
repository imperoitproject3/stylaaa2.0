﻿
function ShowToastr(status, message, title) {
    toastr.remove();

    status = status.toLowerCase();
    if (status == 'error') {
        toastr.error(message, title);
    }
    else if (status == 'success') {
        toastr.success(message, title);
    }
    else if (status == 'info') {
        toastr.info(message, title);
    }
    else if (status == 'warning') {
        toastr.warning(message, title);
    }
}
$(function () {
    var url = window.location.pathname,
        urlRegExp = new RegExp(url.replace(/\/$/, '') + "$");
    $('.admin-menu ul li a').each(function () {
        if (urlRegExp.test(this.href.replace(/\/$/, ''))) {
            $('.admin-menu ul li').removeClass('active');
            $(this).closest("li").addClass('active');
        }
    });

    $('.admin-menu-mobile .owl-item').each(function () {
        var anchertag = $(this).find(".item a");
        var aHref = $(anchertag).attr("href");
        if (urlRegExp.test(aHref.replace(/\/$/, ''))) {
            $('.admin-menu-mobile .owl-item').removeClass('active');
            $('.admin-menu-mobile .owl-item .item a').removeClass('active');

            $(anchertag).closest(".owl-item").addClass('active');
            $(anchertag).addClass('active');
        }
    });
});

/*************************search-box************************************************/

var searching = 'Searching';
if (varLang == 'de') {
    searching = 'wird gesucht';
}

function loadPostlist(PostListUrl) {
    GetSiteCurrentLocation(PostListUrl, 'post');
}

function loadLocationPostlist(PostListUrl) {

    document.getElementById("topSearchUpBtn").style.display = "none";
    var searchpost = $("#searchpost").val();
    var content = '<div class="post-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>' + searching + '...</p></div></div></div>';
    $('#posts').html(content);

    //if (searchpost != null && searchpost != undefined && searchpost != "") {
    displaySearchOverlay();

    var formData = new FormData();
    formData.append('intSearchPostPageIndex', postindex);

    var hdnLocalLatitude = $("#hdnLocalLatitude").val();
    var hdnLocalLongitude = $("#hdnLocalLongitude").val();
    if (hdnLocalLatitude == null || hdnLocalLatitude == undefined) {
        hdnLocalLatitude = "";
    }
    if (hdnLocalLongitude == null || hdnLocalLongitude == undefined) {
        hdnLocalLongitude = "";
    }
    formData.append('localLatitude', hdnLocalLatitude);
    formData.append('localLongitude', hdnLocalLongitude);

    var CheckedCategory = "";
    $('.search-popup input[class="check-search-category"]:checked').each(function () {
        if (CheckedCategory != "") {
            CheckedCategory = CheckedCategory + ",";
        }
        CheckedCategory = CheckedCategory + this.id;

    });

    formData.append('CheckedCategory', CheckedCategory);
    formData.append('SearchPostText', searchpost);
    formData.append('isCount', 'true');
    formData.append('isData', 'true');

    $.ajax({
        url: PostListUrl,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeSearchOverlay();
            var data = JSON.parse(result.Result);
            if (parseInt(data.PageSize) > 1) {
                $('#search-posts').removeClass('without-after-element');
            }
            else {
                $('#search-posts').addClass('without-after-element');
            }
            if (parseInt(data.PageSize) > 0) {
                $('#hdnSearchPostPageSize').val(data.PageSize);

                //console.log(data.GetPostModelPopular);
                var MessagesList = { items: data.GetPostModelPopular };
                $('#posts').html("");
                $("#search-post-template").tmpl(MessagesList).appendTo("#posts");
            }
            else {
                var noPostAvail = 'No post available';
                if (varLang == 'de') {
                    noPostAvail = 'Kein Beitrag gefunden!';
                }
                removeSearchOverlay();
                var content = ' <div class="post-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>' + noPostAvail + '</p></div></div></div>';
                $('#posts').html(content);
            }
        },
        error: function (err) {
            removeSearchOverlay();
        }
    });
}

function loadStylistlist(StylistListUrl) {
    GetSiteCurrentLocation(StylistListUrl, 'stylist');
}

function loadLocationStylistlist(StylistListUrl) {
    document.getElementById("topSearchUpBtn").style.display = "none";

    var content = '<div class="stylist-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>' + searching + '...</p></div></div></div>';
    $('#stylist').html(content);

    displaySearchOverlay();
    var stylistIndex = 1;
    $('#hdnSearchStylistPageIndex').val(stylistIndex);

    var formData = new FormData();
    formData.append('intSearchStylistPageIndex', stylistIndex);
    var CheckedCategory = "";
    var checked = $(".search-popup input:checked").length;
    if ($(".search-popup input:checked").length > 0) {
        CheckedCategory = "";
        $('.search-popup input[class="check-search-category"]:checked').each(function () {
            if (CheckedCategory != "") {
                CheckedCategory = CheckedCategory + ",";
            }
            CheckedCategory = CheckedCategory + this.id;

        });
    }
    formData.append('CheckedCategory', CheckedCategory);
    var searchpost = $("#searchpost").val();
    formData.append('SearchPostText', searchpost);
    formData.append('isCount', 'true');
    formData.append('isData', 'true');

    var hdnLocalLatitude = $("#hdnLocalLatitude").val();
    var hdnLocalLongitude = $("#hdnLocalLongitude").val();
    if (hdnLocalLatitude == null || hdnLocalLatitude == undefined) {
        hdnLocalLatitude = "";
    }
    if (hdnLocalLongitude == null || hdnLocalLongitude == undefined) {
        hdnLocalLongitude = "";
    }
    formData.append('localLatitude', hdnLocalLatitude);
    formData.append('localLongitude', hdnLocalLongitude);

    $.ajax({
        url: StylistListUrl,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeSearchOverlay();
            var data = JSON.parse(result.Result);
            $('#hdnSearchStylistPageSize').val(data.PageSize);

            if (parseInt(data.PageSize) > 1) {
                $('#search-stylist').removeClass('without-after-element');
            }
            else {
                $('#search-stylist').addClass('without-after-element');
            }
            if (parseInt(data.PageSize) > 0) {
                var MessagesList = { items: data.PopularStylishFilterModel };
                $('#stylist').html("");
                $("#search-stylist-template").tmpl(MessagesList).appendTo("#stylist");

                $("#search-new-stylist").val("1");
            }
            else {
                var noStylistAvail = 'No stylist available';
                if (varLang == 'de') {
                    noStylistAvail = 'Keine Stylisten gefunden!';
                }
                removeSearchOverlay();
                var content = ' <div class="post-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>' + noStylistAvail + '</p></div></div></div>';
                $('#stylist').html(content);
            }
        },
        error: function (err) {
            removeSearchOverlay();
            //ShowToastr('error', err, '');
        }
    });

}

function loadHashtaglist(HashtagListUrl) {
    GetSiteCurrentLocation(HashtagListUrl, 'hashtag');
}


function loadLocationHashtaglist(HashtagListUrl) {
    //GetSiteCurrentLocation();

    document.getElementById("topSearchUpBtn").style.display = "none";

    //var searchdata = $("#search-new-hashtag").val(); //|| searchdata == "0"
    //if ($("#hashtag .stylist-row").length <= 0) {
    var content = '<div class="stylist-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>' + searching + '...</p></div></div></div>';
    $('#hashtag').html(content);

    displaySearchOverlay();
    var hashtagIndex = 1;
    $('#hdnSearchHashtagPageIndex').val(hashtagIndex);

    var formData = new FormData();
    formData.append('intSearchHashtagPageIndex', hashtagIndex);
    var CheckedCategory = "";
    var checked = $(".search-popup input:checked").length;
    if ($(".search-popup input:checked").length > 0) {
        CheckedCategory = "";
        $('.search-popup input[class="check-search-category"]:checked').each(function () {
            if (CheckedCategory != "") {
                CheckedCategory = CheckedCategory + ",";
            }
            CheckedCategory = CheckedCategory + this.id;

        });
    }
    formData.append('CheckedCategory', CheckedCategory);
    var searchpost = $("#searchpost").val();
    formData.append('SearchPostText', searchpost);
    formData.append('isCount', 'true');
    formData.append('isData', 'true');

    var hdnLocalLatitude = $("#hdnLocalLatitude").val();
    var hdnLocalLongitude = $("#hdnLocalLongitude").val();
    if (hdnLocalLatitude == null || hdnLocalLatitude == undefined) {
        hdnLocalLatitude = "";
    }
    if (hdnLocalLongitude == null || hdnLocalLongitude == undefined) {
        hdnLocalLongitude = "";
    }
    formData.append('localLatitude', hdnLocalLatitude);
    formData.append('localLongitude', hdnLocalLongitude);

    $.ajax({
        url: HashtagListUrl,
        type: 'POST',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeSearchOverlay();
            var data = JSON.parse(result.Result);
            $('#hdnSearchHashtagPageSize').val(data.PageSize);

            if (parseInt(data.PageSize) > 1) {
                $('#search-hashtag').removeClass('without-after-element');
            }
            else {
                $('#search-hashtag').addClass('without-after-element');
            }
            if (parseInt(data.PageSize) > 0) {
                var MessagesList = { items: data.HashtagFilterModel };
                $('#hashtag').html("");
                $("#search-hashtag-template").tmpl(MessagesList).appendTo("#hashtag");

                //$('#hashtag').html(result);
                $("#search-new-hashtag").val("1")
            }
            else {
                removeSearchOverlay();
                var content = ' <div class="post-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>No hashtag available</p></div></div></div>';
                $('#hashtag').html(content);
            }
        },
        error: function (err) {
            removeSearchOverlay();
            //ShowToastr('error', err, '');
        }
    });

    //$.ajax({
    //    url: HashtagCountUrl,
    //    type: 'POST',
    //    data: formData,
    //    cache: false,
    //    contentType: false,
    //    processData: false,
    //    success: function (result) {
    //        var pagesize = parseInt(result.Message);
    //        $('#hdnSearchHashtagPageSize').val(pagesize);

    //        $('#search-stylist').addClass('without-after-element');
    //        $('#search-hashtag').addClass('without-after-element');
    //        $('#search-posts').addClass('without-after-element');

    //        if (pagesize > 1) {
    //            $('#search-hashtag').removeClass('without-after-element');
    //        }
    //        else {
    //            $('#search-hashtag').addClass('without-after-element');
    //        }

    //        if (pagesize > 0) {

    //            url = HashtagListUrl;
    //            $.ajax({
    //                url: url,
    //                type: 'POST',
    //                data: formData,
    //                cache: false,
    //                contentType: false,
    //                processData: false,
    //                success: function (result) {
    //                    removeSearchOverlay();
    //                    $('#hashtag').html(result);
    //                    $("#search-new-hashtag").val("1")
    //                },
    //                error: function (err) {
    //                    removeSearchOverlay();
    //                    ShowToastr('error', err, '');
    //                }
    //            });
    //        }
    //        else {
    //            removeSearchOverlay();
    //            var content = ' <div class="post-row"><div class="table"><div class="product-discription" style="text-align: center;"><p>No hashtag available</p></div></div></div>';
    //            $('#hashtag').html(content);
    //        }
    //    },
    //    error: function (err) {
    //        removeSearchOverlay();
    //        ShowToastr('error', err, '');
    //    }
    //});

}