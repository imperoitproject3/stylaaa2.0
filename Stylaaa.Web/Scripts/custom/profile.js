﻿//for profile picture
var canvas2 = $("#profile-canvas"),
    context2 = canvas2.get(0).getContext("2d");
var cropper;

//function fnCropClear() {
//    $("#profile-pic-pp .ps-upload").addClass("hide");
//    $("#profile-pic-pp .upload-button").removeClass("hide");
//    $('#profile-pic-pp #profile-canvas').removeClass('hide');

//    $("#profile-pic-pp .cropper-container").hide();
//};

$('#file-input-profile-pic').on('change', function () {
    $('#profile-pic-pp .ps-upload').removeClass('hide');
    var URL = window.URL || window.webkitURL;
    if (this.files && this.files[0]) {
        if (this.files[0].type.match(/^image\//)) {
            var file = this.files[0];
            var options = {
                //aspectRatio: 1 / 1
                aspectRatio: 1 / 1,
                minCropBoxWidth: 356,
                minCropBoxHeight: 356,
                viewMode: 3,
            };
            var reader_new = new FileReader();
            reader_new.onload = function (evt) {
                var img_new = new Image();
                img_new.onload = function () {
                    context2.canvas.height = img_new.height;
                    context2.canvas.width = img_new.width;
                    context2.drawImage(img_new, 0, 0);
                    var cropper_new = canvas2.cropper({
                        aspectRatio: 1 / 1
                    });
                };
                //img_new.src = evt.target.result;
                canvas2.cropper('replace', evt.target.result);
            };
            canvas2.cropper(options);
            reader_new.readAsDataURL(this.files[0]);
        }
        else {
            alert("Invalid file type! Please select an image file.");
        }
    }
    else {
        alert('No file(s) selected.');
    }
});

$('#btnRestore-ProfilePic').click(function () {
    canvas2.cropper('reset');
});

var canvas = $("#canvas");
if (canvas.get(0) != null && canvas.get(0) && undefined && canvas.get(0) != '') {
    context = canvas.get(0).getContext("2d")
}
$result = $();

$('#file-input-coverpic').on('change', function () {
    $('#change-cover-photo .ps-upload').removeClass('hide');
    $('.crop-image-section .upload-button').addClass('hide');

    $(".ps-upload")
    if (this.files && this.files[0]) {
        if (this.files[0].type.match(/^image\//)) {
            var options = {
                //aspectRatio: 16 / 9
                aspectRatio: 993 / 480,
                minCropBoxWidth: 993,
                minCropBoxHeight: 480,
                viewMode: 3,
            };
            var reader = new FileReader();
            reader.onload = function (evt) {
                var img = new Image();
                img.onload = function () {
                    context.canvas.height = img.height;
                    context.canvas.width = img.width;
                    context.drawImage(img, 0, 0);
                    var cropper = canvas.cropper({
                        aspectRatio: 16 / 9
                    });
                };
                //img.src = evt.target.result;
                canvas.cropper('replace', evt.target.result);
            };
            canvas.cropper(options);
            reader.readAsDataURL(this.files[0]);
        }
        else {
            alert("Invalid file type! Please select an image file.");
        }
    }
    else {
        alert('No file(s) selected.');
    }
});

$('#btnRestore-CoverPic').click(function () {
    canvas.cropper('reset');
    $result.empty();
});

function rotateLeftProfilePic() {
    canvas2.cropper('rotate', -90);
}

function rotateRightProfilePic() {
    canvas2.cropper('rotate', 90);
}

function rotateLeft() {
    canvas.cropper('rotate', -90);
}

function rotateRight() {
    canvas.cropper('rotate', 90);
}

function loadMorePostList(url, pagenum) {
    $.ajax({
        url: url,
        type: "GET",
        beforeSend: function () {
            displayOverlay();
            //$('#loader-icon').show();
        },
        complete: function () {
            removeOverlay();
            $("#RecentPostPageIndex").val(pagenum);
            
            //$('#loader-icon').hide();
        },
        success: function (result) {
            $('#profile-post').append(result);
            removeOverlay();
            if (pagenum == $("#RecentPostPageCount").val()) {
                $(".view-all").hide();
            }
            //$("#faq-result").append(result);
        },
        error: function () { removeOverlay(); }
    });
}