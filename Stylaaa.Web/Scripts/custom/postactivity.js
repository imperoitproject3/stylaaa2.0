﻿
function loadAllPostList(url, addpost, successMessage) {

    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeWhiteOverlay();
            removeOverlay();
            $('#post-list').html(result);

            if (addpost == true) {
                ShowToastr('success', successMessage, '');
            }
            //for tabs
            $('.new-tab-links ul').each(function () {
                var $active, $content, $links = $(this).find('a');
                $active = $($links.filter('[href="' + location.hash + '"]')[0] || $links[0]);
                $active.addClass('active');
                $content = $($active[0].hash);
                $links.not($active).each(function () {
                    $(this.hash).hide();
                });
                $(this).on('click', 'a', function (e) {
                    $active.removeClass('active');
                    $content.hide();
                    $active = $(this);
                    $content = $(this.hash);
                    $active.addClass('active');
                    $content.show();
                    e.preventDefault();
                    equalheight('.product-content, .profile-bx, .product-new-ctn');

                });
            });

            $("a[href='#new-post']").attr("class", "active");
            $("a[href='#popular-post']").removeClass("active");
        },
        error: function (err) {
            removeOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}

function loadPopularPost(url, pagenum) {

    if ($("body").find("#overlay").length <= 0) {
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                displayOverlay();
                //$('#loader-icon').show();
            },
            complete: function () {
                removeOverlay();
                $("#PopularPostPageIndex").val(pagenum);
                //$('#loader-icon').hide();
            },
            success: function (result) {
                $('#popular-post .mrg-box').append(result);
                removeOverlay();
                //$("#faq-result").append(result);
                blngetdata = false;
            },
            error: function () { removeOverlay(); }
        });
    }
}

function loadRecentPost(url, pagenum) {
    if ($("body").find("#overlay").length <= 0) {
        $.ajax({
            url: url,
            type: "GET",
            beforeSend: function () {
                displayOverlay();
                //$('#loader-icon').show();
            },
            complete: function () {
                removeOverlay();
                $("#RecentPostPageIndex").val(pagenum);

                //$('#loader-icon').hide();
            },
            success: function (result) {
                $('#new-post .mrg-box').append(result);
                removeOverlay();
                blngetdata = false;
                //$("#faq-result").append(result);
            },
            error: function () { removeOverlay(); }
        });
    }
}

function likePostbyUser(thisData, postid, url) {
    if (intLogged == 0) {
        var msg = "Please login first";
        var yes = 'Login';
        if (varLang == 'de') {
            msg = "Bitte einloggen!";
            yes = 'Login';
        }
        swal({
            title: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: yes,
            closeOnConfirm: false,
            closeOnCancel: false,
            allowOutsideClick: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    //redirect to login page
                    location.href = loginLink;

                    swal.close();
                } else {
                    swal.close();
                    return false;
                }
            });
    }
    else {
        // Create FormData object
        var fileData = new FormData();
        fileData.append('postid', postid);

        $.ajax({
            url: url,
            type: 'POST',
            data: fileData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                if (result.Message == "0") {
                    //var msg = "Please login first";
                    //var yes = 'Login';
                    //if (varLang == 'de') {
                    //    msg = "Please login first";
                    //    yes = 'Login';
                    //}
                    //swal({
                    //    title: msg,
                    //    type: "warning",
                    //    showCancelButton: true,
                    //    confirmButtonClass: "btn-danger",
                    //    confirmButtonText: yes,
                    //    closeOnConfirm: false,
                    //    closeOnCancel: false,
                    //    allowOutsideClick: false
                    //},
                    //    function (isConfirm) {
                    //        if (isConfirm) {
                    //            //redirect to login page
                    //            location.href = loginLink;

                    //            swal.close();
                    //        } else {
                    //            swal.close();
                    //            return false;
                    //        }
                    //    });
                }
                else {

                    var likecount = $(thisData).next("a[href='#like-activity']").html();
                    var otherpagelikecontrol = $("#hdnPostBoxId_" + postid + "").next("a.pd-linke");
                    var totallike = 0;
                    if (result.Message == "1") {
                        if (parseInt(likecount) > 0) {
                            totallike = parseInt(likecount) - 1;
                        }
                        //$(thisData).removeClass("pd-linke-like");
                        //$(thisData).addClass("pd-linke-unlike");

                        $(".product-bx-" + postid + " .pd-linke").removeClass("pd-linke-like");
                        $(".product-bx-" + postid + " .pd-linke").addClass("pd-linke-unlike");
                    }

                    else {
                        totallike = parseInt(likecount) + 1;
                        //$(thisData).removeClass("pd-linke-unlike");
                        //$(thisData).addClass("pd-linke-like");

                        $(".product-bx-" + postid + " .pd-linke").removeClass("pd-linke-unlike");
                        $(".product-bx-" + postid + " .pd-linke").addClass("pd-linke-like");
                    }

                    //$(thisData).next("a[href='#like-activity']").html(totallike);
                    $(".product-bx-" + postid + " .pd-linke").next("a[href='#like-activity']").html(totallike);
                }
            },
            error: function (err) {
                removeWhiteOverlay();
                ShowToastr('error', err, '');
            }
        });
    }
}

function showLikeActivity(thisData, postid, counturl, url, totallike) {
    
    if (intLogged == 0) {
        var msg = "Please login first";
        var yes = 'Login';
        if (varLang == 'de') {
            msg = "Bitte einloggen!";
            yes = 'Login';
        }
        swal({
            title: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: yes,
            closeOnConfirm: false,
            closeOnCancel: false,
            allowOutsideClick: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    //redirect to login page
                    location.href = loginLink;

                    swal.close();
                } else {
                    swal.close();
                    return false;
                }
            });
    }
    else {      
        if (parseInt($(thisData).html()) > 0) {
            //displayWhiteOverlay();

            var formData = new FormData();
            formData.append('intPostId', postid);
            $.ajax({
                url: counturl,
                type: 'POST',
                data: formData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {                    
                    var pagesize = parseInt(result.Message);
                    $('#hdnLikePageSize').val(pagesize);
                    $('.like-count').html(totallike);
                    if (pagesize > 0) {
                        //var liekindex = $('#hdnLikePageIndex').val();
                        formData.append('intLikePageIndex', 1);

                        $.ajax({
                            url: url,
                            type: 'POST',
                            data: formData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (result) {                                
                                removeWhiteOverlay();
                                $('#partial_likes_activity').html(result);

                                $.fancybox.open({
                                    src: $(thisData).attr("href"),
                                    type: 'inline'
                                    //modal: true,
                                    //helpers: {
                                    //    overlay: { closeClick: false }
                                    //}
                                });
                                $("#hdnLikePostId").val(postid);
                                //var jsdata = jQuery.fancybox.open(jQuery($(thisData).attr("href")));
                                //$(".fancybox-slide").unbind();

                                if (pagesize > 1) {
                                    $('#like-activity').removeClass('without-after-element');
                                }
                                else {
                                    $('#like-activity').addClass('without-after-element');
                                }
                            },
                            error: function (err) {
                                removeWhiteOverlay();
                                ShowToastr('error', err, '');
                            }
                        });
                    }
                    else {
                        removeWhiteOverlay();
                        ShowToastr('warning', 'no likes', '');
                    }
                },
                error: function (err) {
                    removeWhiteOverlay();
                    ShowToastr('error', err, '');
                }
            });
        }
    }

}

function followUser(thisData, url) {
    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            var btnFollowTxt = 'Follow';
            var btnFollowingTxt = 'Following';
            if (varLang == 'de') {
                btnFollowTxt = 'Folgen';
                btnFollowingTxt = 'Abonniert';
            }
            if ($(thisData).hasClass("defult")) {
                $(thisData).removeClass("defult");
                $(thisData).html(btnFollowTxt);
            }
            else {
                $(thisData).addClass("defult");
                $(thisData).html(btnFollowingTxt);
            }
            removeWhiteOverlay();
        },
        error: function (err) {
            removeWhiteOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}

function fnPopupReport(thisData, postid) {
    $("#hdnReportPostID").val(postid);
    $("#ReportPostReasonText").val("");

    $.fancybox.open({
        src: "#report-post",
        type: 'inline',
        modal: true,
        padding: 0,
        helpers: {
            overlay: { locked: false, closeClick: false }
        },
        beforeShow: function () {
            disable_scroll();
        },
        afterClose: function () {
            enable_scroll();
        }
    });
}

//var keys = [37, 38, 39, 40];

//function preventDefault(e) {
//    e = e || window.event;
//    if (e.preventDefault) e.preventDefault();
//    e.returnValue = false;
//}

//function keydown(e) {
//    for (var i = keys.length; i--;) {
//        if (e.keyCode === keys[i]) {
//            preventDefault(e);
//            return;
//        }
//    }
//}

//function wheel(e) {
//    preventDefault(e);
//}

//function disable_scroll() {
//    if (window.addEventListener) {
//        window.addEventListener('DOMMouseScroll', wheel, false);
//    }
//    window.onmousewheel = document.onmousewheel = wheel;
//    document.onkeydown = keydown;
//}

//function enable_scroll() {
//    if (window.removeEventListener) {
//        window.removeEventListener('DOMMouseScroll', wheel, false);
//    }
//    window.onmousewheel = document.onmousewheel = document.onkeydown = null;
//}

function disable_scroll() {
    //$("body").css({
    //    "overflow-y": "hidden",
    //    "position": "fixed"
    //});

    $("html,body").css({
        "overflow-y": "scroll",
        "height": "auto",
        "-webkit-text-size-adjust": "100%",
        "-ms-text-size-adjust": "100%"
    });
}

function enable_scroll() {
    //    $("body").removeAttr("style");
    $("html,body").removeAttr("style");
}

function reportpostbyuser(url) {

    var postid = $("#hdnReportPostID").val();
    var ReasonText = $("#ReportPostReasonText").val();
    if (ReasonText != "") {
        var formData = new FormData();
        formData.append('ReasonText', ReasonText);
        formData.append('PostID', postid);

        var msg = 'Post reported done.';
        if (varLang == 'de') {
            msg = 'Beitrag gemeldet';
        }

        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                removeWhiteOverlay();
                //$(".cancel-pp").click();
                $.fancybox.close();
                ShowToastr('success', msg, '');
            },
            error: function (err) {
                removeWhiteOverlay();
                //$(".cancel-pp").click();
                $.fancybox.close();
                //ShowToastr('error', "err::" + err, '');
            }
        });
    }
    else {
        var errorMsg = "Please write reason for report";
        if (varLang == "de") {
            errorMsg = "Bitte schreiben Sie den Grund für den Bericht";
        }
        ShowToastr('error', errorMsg, '');
        $("#ReportPostReasonText").focus();
    }
}

function showPostDetail(thisData, postid, url, userid) {
    if (intLogged == 0) {
        var msg = "Please login first";
        var yes = 'Login';
        if (varLang == 'de') {
            msg = "Bitte einloggen!";
            yes = 'Login';
        }
        swal({
            title: msg,
            type: "warning",
            showCancelButton: true,
            confirmButtonClass: "btn-danger",
            confirmButtonText: yes,
            closeOnConfirm: false,
            closeOnCancel: false,
            allowOutsideClick: false
        },
            function (isConfirm) {
                if (isConfirm) {
                    //redirect to login page
                    location.href = loginLink;

                    swal.close();
                } else {
                    swal.close();
                    return false;
                }
            });
    }
    else {
        if ($("body").find("#overlay").length <= 0 && userid != '') {
            displayWhiteOverlay();
            // Create FormData object
            var fileData = new FormData();
            fileData.append('postid', postid);
            $.ajax({
                url: url,
                type: 'POST',
                data: fileData,
                cache: false,
                contentType: false,
                processData: false,
                success: function (result) {
                    $('#partial_postdetail').html(result);
                    $.fancybox.open({
                        src: "#comment-popup",
                        type: 'inline',
                        modal: true,
                        helpers: {
                            overlay: { closeClick: false }
                        }
                    });

                    //var jsdata = jQuery.fancybox.open(jQuery("#comment-popup"));
                    //$(".fancybox-slide").unbind();
                    removeWhiteOverlay();
                    fnInitializePostDetail();
                },
                error: function (err) {
                    removeWhiteOverlay();
                    ShowToastr('error', err, '');
                }
            });
        }
    }

}

function fnInitializePostDetail() {    

    $('.owl-carousel').owlCarousel({
        items: 1,
        loop: false
    });

    $('.owl-next').on('click', function () {
        var vid = document.getElementById('my_video');
        if (vid != null) {
            vid.pause();
        }
    });

    $('.owl-prev').on('click', function () {
        var vid = document.getElementById('my_video');
        if (vid != null) {
            vid.pause();
        }
    });

    $("#divCommentList").mCustomScrollbar({
        theme: "dark",
        autoHideScrollbar: true,
        mouseWheel: { enable: true },
        advanced: {
            updateOnContentResize: true
        }
    });

}

function fnSendComment(postid, url, morehorizontalPath, userrouteurl) {
    if ($('#txtComment').val() != "" && $("body").find("#overlay").length <= 0) {

        displayWhiteOverlay();
        // Create FormData object
        var CommentId = $("#hdnCommentID").val();
        if (CommentId == null || CommentId == undefined || CommentId == "") {
            CommentId = "0";
        }
        var message = $("#txtComment").val();

        var fileData = new FormData();
        fileData.append('postid', postid);
        fileData.append('message', message);
        fileData.append('CommentId', CommentId);

        $.ajax({
            url: url,
            type: 'POST',
            data: fileData,
            cache: false,
            contentType: false,
            processData: false,
            success: function (result) {
                showComments(message, result.Result.split("-")[1], result.Message, result.Result.split("-")[0], morehorizontalPath, CommentId, userrouteurl, postid);
                //ShowToastr('success', 'comment posted successfully', '');
                removeWhiteOverlay();
            },
            error: function (err) {
                removeWhiteOverlay();
                ShowToastr('error', err, '');
            }
        });
    }
}

function showComments(message, username, userprofileurl, commentid, morehorizontalPath, mainCommentId, userrouteurl, postid) {
    if (parseInt(mainCommentId) > 0) {
        var content = '<div class="review-row-reply" style="display:inline-block;padding-left: 76px;padding-right: 32px;margin-top: 8px;">' +
            '<div class="table">' +
            '<div class="table-cell-img">' +
            '<img src="' + userprofileurl + '" alt="" class="mCS_img_loaded">' +
            '</div>' +
            '<div class="table-cell-content">' +
            '<div class="table">' +
            '<div class="review-had">' +
            '<h6> <a href="' + userrouteurl + '">' + username + '</a> @' + $("#hdnCommentByName").val() + '<div class="more-link">' +
            '<img src="' + morehorizontalPath + '" alt="" class="mCS_img_loaded">' +
            '<ul class="more-link-bx-comment">' +
            '<li><a href="#" title="Delete" onclick="fnDeleteComment(this,' + mainCommentId + ', ' + commentid + ', ' + postid + ')">Delete</a></li>' +
            '</ul>' +
            '</div>' +
            '</h6>' +
            '<span>1 sec</span>' +
            '</div>' +
            '</div>' +
            '<div class="discription-review">' +
            '<p>' + message + '</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        $("#divCommentList").find("#" + mainCommentId).after(content);
        hideReplyingLabel();
    }
    else {
        var content = '<div class="review-row">' +
            '<div class="table">' +
            '<div class="table-cell-img">' +
            '<img src="' + userprofileurl + '" alt="" class="mCS_img_loaded">' +
            '</div>' +
            '<div class="table-cell-content">' +
            '<div class="table">' +
            '<div class="review-had">' +
            '<h6> <a href="' + userrouteurl + '">' + username + '</a><div class="more-link">' +
            '<img src="' + morehorizontalPath + '" alt="" class="mCS_img_loaded">' +
            '<ul class="more-link-bx-comment">' +
            '<li><a href="#" title="Delete" onclick="fnDeleteComment(this,' + commentid + ', 0,' + postid + ')">Delete</a></li>' +
            '</ul>' +
            '</div>' +
            '</h6>' +
            '<span>1 sec</span>' +
            '</div>' +
            '</div>' +
            '<div class="discription-review">' +
            '<p>' + message + '</p>' +
            '</div>' +
            '</div>' +
            '</div>' +
            '</div>';

        $("#divCommentList").find(".mCSB_container").prepend(content);
    }

    $(".product-bx-" + postid + " .pd-chet").html(parseInt($('#divCommentList .review-row').length) + parseInt($('#divCommentList .review-row-reply').length));

    $("#txtComment").val('');
    $("#hdnCommentID").val('');
    $("#hdnCommentByName").val('');
}

function showAllComment() {
    $(".review-row").show();
    $(".review-row-reply").show();
    $(".review-row-reply").attr("style", "display:inline-block;padding-left: 76px;padding-right: 32px;margin-top: 8px;");
    $("#link-morecomment").hide();
}

$(document).on("keypress", "#txtComment", function (e) {
    if (e.keyCode == 13) {
        if ($(this).val() != "") {
            $("#sendComment").click();
        }
    }
});

function fnReplyComment(thisData, CommentId, CommentByName) {
    $("#hdnCommentID").val(CommentId);
    $("#hdnCommentByName").val(CommentByName);
    $("#spnCommentByName").html(CommentByName);
    //$("#txtComment").val("@" + CommentByName + " ");
    $("#txtComment").focus();
    $("#divReplyingLabel").show();
}

function hideReplyingLabel() {
    $("#divReplyingLabel").hide();

    $("#hdnCommentID").val('');
    $("#hdnCommentByName").val('');
    $("#spnCommentByName").html('');
    $("#txtComment").val("");
}