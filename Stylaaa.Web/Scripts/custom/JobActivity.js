﻿function LoadJobLIst(url, job, successMessage) {
    console.log('LoadJobLIst');
    $.ajax({
        url: url,
        type: 'GET',
        cache: false,
        contentType: false,
        processData: false,
        success: function (result) {
            removeWhiteOverlay();
            removeOverlay();
            $('#joblist').html(result);
            $('#jobs').click();
            if (job == true) {
                ShowToastr('success', successMessage, '');
            }            
        },
        error: function (err) {
            removeOverlay();
            ShowToastr('error', "err::" + err, '');
        }
    });
}