﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Stylaaa.Web.Filter
{
    public class UserRoleMenuModel
    {
        public int Id { get; set; }
        public Role Role { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }

    public static class UserRoleMenu
    {
        public static List<UserRoleMenuModel> GetUserRoleMenu(string UserId, Role UserRole)
        {
            List<UserRoleMenuModel> objUserRoles = new List<UserRoleMenuModel> {
                new UserRoleMenuModel{ Role=Role.User, Controller="Profile", Action="ProfileSetup" },
                new UserRoleMenuModel{ Role=Role.Stylist, Controller="Profile", Action="ProfileSetup" }
            };
            return objUserRoles;
        }
    }

    public class CompleteProfileSetup : AuthorizeAttribute
    {
        private UserService _service;
        bool IsAuthenticated = false;
        bool IsProfileComplete = false;
        //bool IsStripeConnected = false;
        Role UserRole;
        private Role[] UserRoles;

        public CompleteProfileSetup(params Role[] roles) : base()
        {
            UserRoles = roles;
        }

        //private bool isLoggedIn = false;

        private bool SkipAuthorization(HttpContextBase httpContext)
        {
            Contract.Assert(httpContext != null);
            return httpContext.Request.RequestContext.HttpContext.SkipAuthorization;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            //IsAuthenticated = false;
            if (httpContext.User.Identity.IsAuthenticated)
            {
                IsAuthenticated = true;
                ClaimsIdentity userIdentity = (ClaimsIdentity)httpContext.User.Identity;
                if (userIdentity != null)
                {
                    IEnumerable<Claim> claims = userIdentity.Claims;
                    if (claims.Any())
                    {
                        UserRole = (Role)Enum.Parse(typeof(Role), claims.Where(c => c.Type == "UserRole").FirstOrDefault()?.Value);

                        string UserId = httpContext.User.Identity.GetUserId();

                        _service = new UserService();
                        ResponseModel<WebUserResponseModel> mResult = _service.CheckProfileSetupCompleted(UserId);

                        IsProfileComplete = mResult.Result.IsProfileSetup;                         
                        //IsStripeConnected = mResult.Result.IsStripeConnected;
                    }
                }
            }
            //_service = new UserService(HttpContext.Current.Request.GetOwinContext());
            //string UserId = httpContext.User.Identity.GetUserId();
            //ResponseModel<WebUserResponseModel> mResult = _service.CheckProfileSetupCompleted(UserId);
            //IsProfileComplete = mResult.Result.IsProfileSetup;
            //UserRole = mResult.Result.Role;

            bool isRoleExists = UserRoles.Contains(UserRole);

            //string Controller = httpContext.Request.RequestContext.RouteData.Values["controller"].ToString();
            //string Action = httpContext.Request.RequestContext.RouteData.Values["action"].ToString();

            return (SkipAuthorization(httpContext) && isRoleExists == false) ||
                (IsProfileComplete && isRoleExists);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (IsAuthenticated/* && IsProfileComplete*/)
            {
                if (IsProfileComplete == false)
                {
                    //if (UserRole == Role.Stylish || UserRole == Role.User)
                    //{
                    //filterContext.Result =
                    //    new RedirectToRouteResult("ProfileSetup", new RouteValueDictionary(new { usertype = UserRole }));
                    //}
                    filterContext.Result =
                    new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index" }));
                }
                //else if (IsStripeConnected == false)
                //{
                //    if (UserRole == Role.Stylish)
                //    {
                //        filterContext.Result = new RedirectToRouteResult("StripeConnect", null);
                //    }
                //}
            }
            else
            {
                filterContext.Result =
                    new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "Unauthorized" }));
            }
        }
    }
}