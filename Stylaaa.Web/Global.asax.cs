﻿using Microsoft.SqlServer.Types;
using System;
using System.Data.Entity.SqlServer;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Stylaaa.Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            //**Add this line**
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            //**Add this line**
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
            SqlProviderServices.SqlServerTypesAssemblyName = typeof(SqlGeography).Assembly.FullName;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            if (!Request.IsSecureConnection && !Request.IsLocal)
            {
                if (Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTP://WWW"))
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
                else
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://www."));
            }
            else if (!Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTPS://WWW") && !Request.IsLocal)
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("https://", "https://www."));
            }
        }

        protected void Application_Error(object sender, EventArgs e)
        {
            HttpContextWrapper context = new HttpContextWrapper(Context);
            Exception exception = Server.GetLastError();
            HttpException httpException = exception as HttpException ?? new HttpException(500, exception.Message);
            Server.ClearError();

            IController controller = null;
            RouteData routeData = null;
            

            controller.Execute(new RequestContext(new HttpContextWrapper(Context), routeData));
        }
    }
}