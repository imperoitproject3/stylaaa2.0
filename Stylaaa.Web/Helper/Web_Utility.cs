﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Stylaaa.Web.Helper
{
    public static class Web_Utility
    {
        public static string[] UserTypes = { "users", "stylist" };
        public static Role[] UserRoles = { Role.User, Role.Stylist };
    }
}