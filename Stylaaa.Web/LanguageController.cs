﻿using Stylaaa.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Web
{
    public class LanguageController : Controller
    {
        protected override IAsyncResult BeginExecuteCore(AsyncCallback callback, object state)
        {
            string lang = null;

            HttpCookie langCookie = Request.Cookies["culture"];
            //langCookie.Expires = DateTime.Now;
            //langCookie.Expires = DateTime.Now.AddDays(-1);
            //HttpContext.Response.Cookies.Set(new HttpCookie("culture") { Value =null });
            //langCookie = null;
            //var langCookie = "da";
            if (langCookie != null)
            {
                lang = langCookie.Value;
                //lang = langCookie;
            }
            else
            {
                var userLanguage = Request.UserLanguages;
                var userLang = userLanguage != null ? userLanguage[0] : "";
                if (userLang != "")
                {
                    lang = userLang;
                }
                else
                {
                    lang = LanguageManager.GetDefaultLanguage();
                }
            }
            new LanguageManager().SetLanguage(lang);
            return base.BeginExecuteCore(callback, state);
        }
    }
}