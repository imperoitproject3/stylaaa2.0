﻿using Microsoft.AspNet.SignalR;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace SignalR.Chat
{
    public class ChatHub : Hub
    {
        private readonly MessageService _service;

        public ChatHub()
        {
            _service = new MessageService();
        }

        public override Task OnConnected()
        {
            string UserID = Context.QueryString["UserID"];
            if (!string.IsNullOrEmpty(UserID))
            {
                Groups.Add(Context.ConnectionId, UserID);
                AddUserInGroup(UserID, Context.ConnectionId);
                //AddUserInGroup(GlobalConfig.HubGroupName, UserID);
                Clients.All.receivedUserIsOnline(UserID);
                //Clients.Caller.receivedOnConnected("welcome " + Context.ConnectionId);
                //SendNotificationUpdates(UserID);
            }
            return base.OnConnected();
        }

        public void AddUserInGroup(string GroupName, string ConnectionId)
        {
            try
            {
                Dictionary<string, List<string>> dict = (Dictionary<string, List<string>>)HttpRuntime.Cache["cachedGroupList"];

                if (dict == null)
                {
                    dict = new Dictionary<string, List<string>>();
                    List<string> arr = new List<string>() { ConnectionId };
                    dict.Add(GroupName, arr);
                }
                else
                {
                    if (dict.ContainsKey(GroupName))
                    {
                        List<string> arr;
                        if (dict.TryGetValue(GroupName, out arr))
                        {
                            arr.Add(ConnectionId);
                            dict[GroupName] = arr;
                        }
                    }
                    else
                    {
                        List<string> arr = new List<string>() { ConnectionId };
                        dict.Add(GroupName, arr);
                    }
                }
                InitlizedGroupCached(dict);
            }
            catch (Exception err)
            {
                SendErrorMessage(err.Message);
            }
        }

        public void RemoveUserInGroup(string GroupName, string ConnectionId)
        {
            try
            {
                Dictionary<string, List<string>> dict = (Dictionary<string, List<string>>)HttpRuntime.Cache["cachedGroupList"];
                if (dict != null)
                {
                    if (dict.ContainsKey(GroupName))
                    {
                        List<string> arr;
                        if (dict.TryGetValue(GroupName, out arr))
                        {
                            arr.Remove(ConnectionId);
                            dict[GroupName] = arr;
                        }
                    }
                }
                InitlizedGroupCached(dict);
            }
            catch (Exception err)
            {
                SendErrorMessage(err.Message);
            }
        }

        public void InitlizedGroupCached(Dictionary<string, List<string>> dict)
        {
            HttpRuntime.Cache["cachedGroupList"] = dict;
        }

        public void ClearedGroupCached()
        {
            //-- Clear Cached Information --//
            HttpRuntime.Cache.Remove("cachedGroupList");
            Clients.All.fireDisconnect();
        }

        public void SendErrorMessage(string errorMsg)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult.Message = errorMsg;
            Clients.Caller.receivedError(mResult);
        }

        public async Task<bool> SendMessage(MessageSendModel model)
        {
            string UserID = Context.QueryString["UserID"];
            ResponseModel<List<ChatConversationViewModel>> nResult = new ResponseModel<List<ChatConversationViewModel>>();
            try
            {
                model.ConnectionIdCount = GetGroupUsersCount(model.ReceiverID);
                ResponseModel<ChatConversationViewModel> mResult = await _service.Send(model, UserID);
                mResult.Result.IsImage = model.IsImage;
                nResult.Status = mResult.Status;
                nResult.Message = mResult.Message;
                if (nResult.Status == ResponseStatus.Success)
                {
                    nResult.Result.Add(mResult.Result);
                    Clients.Group(model.ReceiverID.ToString()).receivedMessage(nResult);
                    int UnreadCount = await _service.GetUserUnreadMessageCount(model.ReceiverID);
                    Clients.All.checkUserUnreadMessageCount();
                    mResult.Result.ChatUserID = model.ReceiverID;
                    mResult.Result.IsSendByMe = true;
                    //Clients.Caller.receivedMessage(nResult);
                    Clients.Group(UserID.ToString()).receivedMessage(nResult, UserID);
                }
            }
            catch (Exception err)
            {
                nResult.Message = err.Message;
                Clients.Caller.receivedMessage(nResult);
            }
            return true;
        }

        public async Task<bool> ReadMessage(long ChatMessageID)
        {
            string UserID = Context.QueryString["UserID"];
            try
            {
                await _service.ReadChatMessage(ChatMessageID, UserID);
                int UnreadCount = await _service.GetUserUnreadMessageCount(UserID);
                Clients.All.updateUserUnreadMessageCount(UserID, UnreadCount);
            }
            catch (Exception err)
            {
                SendErrorMessage(err.Message);
            }
            return true;
        }

        public int GetGroupUsersCount(string GroupName)
        {
            int count = 0;
            try
            {
                Dictionary<string, List<string>> dict = (Dictionary<string, List<string>>)HttpRuntime.Cache["cachedGroupList"];
                if (dict != null)
                {
                    if (dict.ContainsKey(GroupName.ToString()))
                    {
                        List<string> arr;
                        if (dict.TryGetValue(GroupName.ToString(), out arr))
                        {
                            count = arr.Count;
                        }
                    }
                }
            }
            catch (Exception err)
            {
                SendErrorMessage(err.Message);
            }
            return count;
        }

        public void UserIsOnline(List<string> listUserIds)
        {
            string UserID = Context.QueryString["UserID"];
            if (!string.IsNullOrEmpty(UserID))
            {
                Clients.AllExcept(new string[] { UserID }).receivedUserIsOnline(UserID);
            }
        }

        public async Task<bool> CheckUserUnreadMessageCount(List<string> listUserIds)
        {
            string UserID = Context.QueryString["UserID"];
            if (!string.IsNullOrEmpty(UserID))
            {
                List<UnreadMessageCountModel> result = await _service.GetUserUnreadMessageCount(listUserIds, UserID);
                Clients.Group(UserID.ToString()).receivedCheckUserUnreadMessageCount(result);
            }
            return true;
        }

        public void GetConversation(ChatConversationGetModel model)
        {
            string UserID = Context.QueryString["UserID"];
            ResponseModel<List<ChatConversationViewModel>> mResult = new ResponseModel<List<ChatConversationViewModel>>();
            try
            {
                if (model.JobId == null)
                {
                    mResult.Result = _service.GetChatConversation(model, UserID);
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = mResult.Result.Count + " messages listed.";
                }
                else
                {
                    mResult.Result = _service.GetJobConversation(model, UserID);
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = mResult.Result.Count + " messages listed.";
                }

            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }

            Clients.Caller.receivedConversation(mResult);
        }

        public void GetjobConversation(ChatConversationGetModel model)
        {
            string UserID = Context.QueryString["UserID"];
            ResponseModel<List<ChatConversationViewModel>> mResult = new ResponseModel<List<ChatConversationViewModel>>();
            try
            {
                mResult.Result = _service.GetJobConversation(model, UserID);
                mResult.Status = ResponseStatus.Success;
                mResult.Message = mResult.Result.Count + " messages listed.";
            }
            catch (Exception err)
            {
                mResult.Message = err.Message;
            }
            Clients.Caller.receivedConversation(mResult);
        }

        public void CheckUserOnline(List<string> listUserIds)
        {
            //Clients.Groups(listUserIds).receivedCheckUserOnline();
            Clients.All.receivedCheckUserOnline();
        }
    }
}