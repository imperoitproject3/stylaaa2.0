﻿using Microsoft.SqlServer.Types;
using System;
using System.Data.Entity.SqlServer;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace SignalR.Chat
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            SqlServerTypes.Utilities.LoadNativeAssemblies(Server.MapPath("~/bin"));
            SqlProviderServices.SqlServerTypesAssemblyName = typeof(SqlGeography).Assembly.FullName;
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
            if (!Request.IsSecureConnection && !Request.IsLocal)
            {
                if (Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTP://WWW"))
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://"));
                else
                    Response.Redirect(Request.Url.AbsoluteUri.Replace("http://", "https://www."));
            }
            else if (!Request.Url.AbsoluteUri.ToUpper().StartsWith("HTTPS://WWW") && !Request.IsLocal)
            {
                Response.Redirect(Request.Url.AbsoluteUri.Replace("https://", "https://www."));
            }
        }
    }
}