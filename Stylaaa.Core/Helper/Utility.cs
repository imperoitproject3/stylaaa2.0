﻿using Stylaaa.Core.Models;
using System;
using System.Collections.Generic;
using System.Device.Location;
using System.Drawing;
using System.IO;

namespace Stylaaa.Core.Helper
{
    public static class Utility
    {
        public static bool IEquals(this string value, string compare)
        {
            if (value == null && compare == null)
                return true;
            if ((value == null && compare != null) || (value != null && compare == null))
                return false;
            return value.Equals(compare, StringComparison.InvariantCultureIgnoreCase);
        }

        public static DateTime GetSystemDateTimeUTC()
        {
            return DateTime.UtcNow;
        }

        public static DateTime GetAustriaDateTimeFromUTC()
        {
            return DateTime.UtcNow.AddHours(1);
        }

        public static ResponseModel<object> SaveCropImage(string imgBase64String, string saveFilePath, string extension)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                imgBase64String = imgBase64String.Replace("data:image/png;base64,", "");

                byte[] bytes = Convert.FromBase64String(imgBase64String);

                Image image;
                using (MemoryStream ms = new MemoryStream(bytes))
                {
                    image = Image.FromStream(ms);
                }
                if (extension.ToLower() != "png")
                    ImageOptimize.FixedSize(saveFilePath, image, 304, 304, true);
                
                    //ImageOptimize.OptimizeSaveJpeg(saveFilePath, image, 90);
                else
                {
                    image.Save(saveFilePath);
                }
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Image uploaded ";
            }
            catch (Exception err)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = err.Message;
            }
            return mResult;
        }

        public static string TimeAgo(DateTime dt)
        {
            TimeSpan span = DateTime.UtcNow - dt;
            if (span.Days > 365)
            {
                int years = (span.Days / 365);
                if (span.Days % 365 != 0)
                    years += 1;
                return String.Format("{0} {1} ago",
                years, years == 1 ? "year" : "years");
            }
            if (span.Days > 30)
            {
                int months = (span.Days / 30);
                if (span.Days % 31 != 0)
                    months += 1;
                return String.Format(Resource.ago + " {0} {1} ",
                months, months == 1 ? "month" : "months");
            }
            if (span.Days > 0)
                return String.Format(Resource.ago + " {0} {1}",
                span.Days, span.Days == 1 ? Resource.day : Resource.Days);
            if (span.Hours > 0)
                return String.Format(Resource.ago + " {0} {1} ",
                span.Hours, span.Hours == 1 ? Resource.hour : Resource.hours);
            if (span.Minutes > 0)
                return String.Format(Resource.ago + " {0} {1} ",
                span.Minutes, span.Minutes == 1 ? "minute" : "minutes");
            if (span.Seconds > 5)
                return String.Format(Resource.ago + " {0}  {1} ",
                        span.Seconds, span.Seconds == 1 ? "second" : "seconds");
            if (span.Seconds <= 5)
                return Resource.justNow;
            return string.Empty;
        }

        public static string GetDistanceAwayText(double? fromLatitude, double? fromLongitude, double? toLatitude, double? toLongitude)
        {
            string DistanceAwayText = string.Empty;
            try
            {
                double DistanceInMiles = GetDistanceInMiles(fromLatitude, fromLongitude, toLatitude, toLongitude);
                if (DistanceInMiles > 0.09)
                {
                    DistanceAwayText = string.Format("{0} miles away from you.", DistanceInMiles.ToString("0.00"));
                }
            }
            catch (Exception err)
            {

                DistanceAwayText = err.Message;
            }

            return DistanceAwayText;
        }

        public static double GetDistanceInMiles(double? fromLatitude, double? fromLongitude, double? toLatitude, double? toLongitude)
        {
            double DistanceInMiles = 0;
            double dfromLatitude = fromLatitude ?? 0;
            double dfromLongitude = fromLongitude ?? 0;
            double dtoLatitude = toLatitude ?? 0;
            double dtoLongitude = toLongitude ?? 0;
            var fromLocation = new GeoCoordinate(dfromLatitude, dfromLongitude);
            var toLocation = new GeoCoordinate(dtoLatitude, dtoLongitude);
            if (dfromLatitude == 0 || dfromLongitude == 0 || dtoLatitude == 0 || dtoLongitude == 0)
            {
            }
            else
            {
                DistanceInMiles = Math.Round(fromLocation.GetDistanceTo(toLocation) / GlobalConfig.MetersInOneMile, 5);
            }
            //Convert to kilometers
            var kilometer = DistanceInMiles * 1.609344;
            return kilometer;
        }

        public static double ConvertKilometersToMiles(double kilometers)
        {
            //
            // Multiply by this constant.
            //
            return kilometers * 0.621371192;
        }

        public static double ConvertMilesToKilometers(double miles)
        {
            //
            // Multiply by this constant and return the result.
            //
            return miles * 1.609344;
        }

        public static string GenerateEmailBody(EmailTemplate templateFileName, Dictionary<string, string> dicPlaceholders)
        {
            dicPlaceholders.Add("{{projectName}}", GlobalConfig.ProjectName);
            string templatePath = Path.Combine(GlobalConfig.EmailTemplatePath, templateFileName.ToString() + ".html");
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(templatePath))
            {
                body = reader.ReadToEnd();
            }
            if (body.Length > 0)
            {
                foreach (var item in dicPlaceholders)
                {
                    body = body.Replace(item.Key, item.Value);
                }
            }
            return body;
        }

        public static int ConvertEuroToCents(double Amount)
        {
            return int.Parse((100 * Amount).ToString("0"));
        }
        //public static List<GenderModel> GetGenderDropdown()
        //{
        //    return new List<GenderModel> {
        //        new GenderModel{ GenderId=Gender.Male, Name="Male" },
        //        new GenderModel{ GenderId=Gender.Female, Name="Female" },
        //        new GenderModel{ GenderId=Gender.Other, Name="Other" }
        //    };
        //}
        public static string TranslateDay(DayOfWeek DayOfWeek)
        {
            string DayofWeek = "";
            switch (DayOfWeek)
            {
                case DayOfWeek.Sunday:
                    DayofWeek = Resource.sunday;
                    break;
                case DayOfWeek.Monday:
                    DayofWeek = Resource.monday;
                    break;
                case DayOfWeek.Tuesday:
                    DayofWeek = Resource.tuesday;
                    break;
                case DayOfWeek.Wednesday:
                    DayofWeek = Resource.wednesday;
                    break;
                case DayOfWeek.Thursday:
                    DayofWeek = Resource.thursday;
                    break;
                case DayOfWeek.Friday:
                    DayofWeek = Resource.friday;
                    break;
                case DayOfWeek.Saturday:
                    DayofWeek = Resource.saturday;
                    break;
                default:
                    break;
            }
            return DayofWeek;
        }

        public static double GetUTCMilliSecond(DateTime dt)
        {
            dt = dt.ToUniversalTime();
            return dt.Subtract(DateTime.MinValue.AddYears(1969)).TotalMilliseconds;
        }

        public static string GetTimeSpan(DateTime datetime)
        {
            string strTimeSpan = "";
            int daySpan = Convert.ToInt32(Math.Floor((GlobalConfig.GetSystemDateTime() - datetime).TotalDays));
            int hourSpan = Convert.ToInt32(Math.Floor((GlobalConfig.GetSystemDateTime() - datetime).TotalHours));
            int minutesSpan = Convert.ToInt32(Math.Floor((GlobalConfig.GetSystemDateTime() - datetime).TotalMinutes));
            int timeSpan = Convert.ToInt32(Math.Floor((GlobalConfig.GetSystemDateTime() - datetime).TotalSeconds));


            if (timeSpan > 0)
            {
                strTimeSpan = timeSpan + " sec";
                if (System.Threading.Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper().Contains("DE"))
                {
                    strTimeSpan = timeSpan + " sek";
                }
            }
            if (minutesSpan > 0)
            {
                strTimeSpan = minutesSpan + " min";
            }

            if (hourSpan > 0)
            {
                strTimeSpan = hourSpan + " h";
            }

            if (daySpan == 1)
            {
                strTimeSpan = " yesterday";
                if (System.Threading.Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper().Contains("DE"))
                {
                    strTimeSpan = " gestern";
                }
            }

            if (daySpan > 1)
            {
                strTimeSpan = datetime.ToString("dd MMMM yyyy");

                if (System.Threading.Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper().Contains("DE"))
                {
                    strTimeSpan = strTimeSpan.Replace("January", Resource.January);
                    strTimeSpan = strTimeSpan.Replace("February", Resource.February);
                    strTimeSpan = strTimeSpan.Replace("March", Resource.March);
                    strTimeSpan = strTimeSpan.Replace("April", Resource.April);
                    strTimeSpan = strTimeSpan.Replace("May", Resource.May);
                    strTimeSpan = strTimeSpan.Replace("June", Resource.June);
                    strTimeSpan = strTimeSpan.Replace("July", Resource.July);
                    strTimeSpan = strTimeSpan.Replace("August", Resource.August);
                    strTimeSpan = strTimeSpan.Replace("September", Resource.September);
                    strTimeSpan = strTimeSpan.Replace("October", Resource.October);
                    strTimeSpan = strTimeSpan.Replace("November", Resource.November);
                    strTimeSpan = strTimeSpan.Replace("December", Resource.December);
                }
            }

            return strTimeSpan;
        }

        public static ChargeStatus getChargeStaus(string responceStatus)
        {
            var status = ChargeStatus.none;
            switch (responceStatus)
            {
                case "none":
                    status = ChargeStatus.none;
                    break;
                case "expired":
                    status = ChargeStatus.expired;
                    break;
                case "failed":
                    status = ChargeStatus.failed;
                    break;
                case "pending":
                    status = ChargeStatus.pending;
                    break;
                case "refundUpdated":
                    status = ChargeStatus.refundUpdated;
                    break;
                case "refunded":
                    status = ChargeStatus.refunded;
                    break;
                case "succeeded":
                    status = ChargeStatus.succeeded;
                    break;
                case "captured":
                    status = ChargeStatus.captured;
                    break;
                default:
                    status = ChargeStatus.none;
                    break;
            }
            return status;
        }
    }
}
