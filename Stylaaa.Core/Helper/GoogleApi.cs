﻿using Stylaaa.Core.Models;
using Stylaaa.Core.Models.GoogleModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Helper
{
    public static class GoogleApi
    {
        public static ResponseModel<GoogleMapModel> GetGooglaMapResult(GoogleQueryParameters queryParameter, string queryValue)
        {
            ResponseModel<GoogleMapModel> mResult = new ResponseModel<GoogleMapModel>() { Status = ResponseStatus.Failed };

            string googleUrl = string.Format("https://maps.googleapis.com/maps/api/geocode/json?address=" + queryValue, queryParameter.ToString(), queryValue, GlobalConfig.GoogleServerKey);
            string jsonString = new WebClient().DownloadString(googleUrl);

            mResult.Result = GoogleMapModel.FromJson(jsonString);
            if (mResult.Result != null)
            {
                mResult.Message = mResult.Result.Status;

                switch ((GoogelResultStatus)Enum.Parse(typeof(GoogelResultStatus), mResult.Result.Status))
                {
                    case GoogelResultStatus.OK:
                        mResult.Status = ResponseStatus.Success;
                        break;
                    case GoogelResultStatus.ZERO_RESULTS:
                        break;
                    case GoogelResultStatus.OVER_QUERY_LIMIT:
                        break;
                    case GoogelResultStatus.REQUEST_DENIED:
                        break;
                    case GoogelResultStatus.INVALID_REQUEST:
                        break;
                    case GoogelResultStatus.UNKNOWN_ERROR:
                        break;
                    default:
                        break;
                }
            }
            return mResult;
        }

        public static ResponseModel<GoogleTimezoneModel> GetTimezoneByLocation(string latitude, string longitude)
        {
            ResponseModel<GoogleTimezoneModel> mResult = new ResponseModel<GoogleTimezoneModel>() { Status = ResponseStatus.Failed };

            string googleUrl = string.Format("https://maps.googleapis.com/maps/api/timezone/json?location={0},{1}&timestamp=0&key={2}", latitude, longitude, GlobalConfig.GoogleServerKey);
            string jsonString = new WebClient().DownloadString(googleUrl);

            mResult.Result = GoogleTimezoneModel.FromJson(jsonString);
            if (mResult.Result != null)
            {
                mResult.Message = mResult.Result.Status;

                switch ((GoogelResultStatus)Enum.Parse(typeof(GoogelResultStatus), mResult.Result.Status))
                {
                    case GoogelResultStatus.OK:
                        mResult.Status = ResponseStatus.Success;
                        break;
                    case GoogelResultStatus.ZERO_RESULTS:
                        break;
                    case GoogelResultStatus.OVER_QUERY_LIMIT:
                        break;
                    case GoogelResultStatus.REQUEST_DENIED:
                        break;
                    case GoogelResultStatus.INVALID_REQUEST:
                        break;
                    case GoogelResultStatus.UNKNOWN_ERROR:
                        break;
                    default:
                        break;
                }
            }
            return mResult;
        }
    }
}
