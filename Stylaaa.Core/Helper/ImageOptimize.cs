﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;

namespace Stylaaa.Core.Helper
{
    public static class ImageOptimize
    {
        private static Dictionary<string,  ImageCodecInfo> encoders = null;

        public static Dictionary<string, ImageCodecInfo> Encoders
        {
            get
            {
                if (encoders == null)
                {
                    encoders = new Dictionary<string, ImageCodecInfo>();
                }

                if (encoders.Count == 0)
                {
                    foreach (ImageCodecInfo codec in ImageCodecInfo.GetImageEncoders())
                    {
                        encoders.Add(codec.MimeType.ToLower(), codec);
                    }
                }

                return encoders;
            }
        }

        public static System.Drawing.Bitmap CropImage(System.Drawing.Image sourceImage)
        {
            int Width = 0, Height = 0;
            // variable for percentage resize
            float percentageResize = 0;
            float percentageResizeW = 0;
            float percentageResizeH = 0;

            // variables for the dimension of source and cropped image
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            // Set the source dimension to the variables
            int sourceWidth = sourceImage.Width;
            int sourceHeight = sourceImage.Height;

            if (sourceWidth > sourceHeight)
            {
                Width = Height = sourceHeight;
            }
            else
            {
                Width = Height = sourceWidth;
            }
            // Calculate the percentage resize
            percentageResizeW = ((float)Width / (float)sourceWidth);
            percentageResizeH = ((float)Height / (float)sourceHeight);

            // Checking the resize percentage
            if (percentageResizeH < percentageResizeW)
            {
                percentageResize = percentageResizeW;
                destY = System.Convert.ToInt16((Height - (sourceHeight * percentageResize)) / 2);
            }
            else
            {
                percentageResize = percentageResizeH;
                destX = System.Convert.ToInt16((Width - (sourceWidth * percentageResize)) / 2);
            }

            // Set the new cropped percentage image
            int destWidth = (int)Math.Round(sourceWidth * percentageResize);
            int destHeight = (int)Math.Round(sourceHeight * percentageResize);

            // Create the image object
            using (Bitmap objBitmap = new Bitmap(Width, Height))
            {
                objBitmap.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);
                using (Graphics objGraphics = Graphics.FromImage(objBitmap))
                {
                    // Set the graphic format for better result cropping
                    objGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    objGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    objGraphics.DrawImage(sourceImage, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                }

                return objBitmap;
            }
        }

        public static System.Drawing.Bitmap CropImage(System.Drawing.Image sourceImage, int Width, int Height)
        {
            // variable for percentage resize
            float percentageResize = 0;
            float percentageResizeW = 0;
            float percentageResizeH = 0;

            // variables for the dimension of source and cropped image
            int sourceX = 0;
            int sourceY = 0;
            int destX = 0;
            int destY = 0;

            // Set the source dimension to the variables
            int sourceWidth = sourceImage.Width;
            int sourceHeight = sourceImage.Height;

            // Calculate the percentage resize
            percentageResizeW = ((float)Width / (float)sourceWidth);
            percentageResizeH = ((float)Height / (float)sourceHeight);

            // Checking the resize percentage
            if (percentageResizeH < percentageResizeW)
            {
                percentageResize = percentageResizeW;
                destY = System.Convert.ToInt16((Height - (sourceHeight * percentageResize)) / 2);
            }
            else
            {
                percentageResize = percentageResizeH;
                destX = System.Convert.ToInt16((Width - (sourceWidth * percentageResize)) / 2);
            }

            // Set the new cropped percentage image
            int destWidth = (int)Math.Round(sourceWidth * percentageResize);
            int destHeight = (int)Math.Round(sourceHeight * percentageResize);

            // Create the image object
            using (Bitmap objBitmap = new Bitmap(Width, Height))
            {
                objBitmap.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);
                using (Graphics objGraphics = Graphics.FromImage(objBitmap))
                {
                    // Set the graphic format for better result cropping
                    objGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                    objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                    objGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    objGraphics.DrawImage(sourceImage, new Rectangle(destX, destY, destWidth, destHeight), new Rectangle(sourceX, sourceY, sourceWidth, sourceHeight), GraphicsUnit.Pixel);
                }

                return objBitmap;
            }
        }

        public static System.Drawing.Bitmap ResizeImage(System.Drawing.Image sourceImage, int Width, int Height)
        {
            Bitmap objBitmap = new Bitmap(Width, Height);
            //set the resolutions the same to avoid cropping due to resolution differences
            objBitmap.SetResolution(sourceImage.HorizontalResolution, sourceImage.VerticalResolution);

            //use a graphics object to draw the resized image into the bitmap
            using (Graphics objGraphics = Graphics.FromImage(objBitmap))
            {
                //set the resize quality modes to high quality
                objGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                objGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.HighQuality;
                //draw the image into the target bitmap
                objGraphics.DrawImage(sourceImage, 0, 0, objBitmap.Width, objBitmap.Height);
            }

            return objBitmap;
        }

        public static System.Drawing.Image FixedSize(string saveFilePath, System.Drawing.Image image, int Width, int Height, bool needToFill)
        {
            System.Drawing.Bitmap objBitmap = null;
            int sourceWidth = image.Width;
            int sourceHeight = image.Height;
            int sourceX = 0;
            int sourceY = 0;
            double destX = 0;
            double destY = 0;

            double nScale = 0;

            double nScaleW = ((double)Width / (double)sourceWidth);
            double nScaleH = ((double)Height / (double)sourceHeight);
            if (!needToFill)
            {
                nScale = Math.Min(nScaleH, nScaleW);
            }
            else
            {
                nScale = Math.Max(nScaleH, nScaleW);
                destY = (Height - sourceHeight * nScale) / 2;
                destX = (Width - sourceWidth * nScale) / 2;
            }

            if (nScale > 1)
                nScale = 1;

            int destWidth = (int)Math.Round(sourceWidth * nScale);
            int destHeight = (int)Math.Round(sourceHeight * nScale);

            try
            {
                objBitmap = new System.Drawing.Bitmap(destWidth + (int)Math.Round(2 * destX), destHeight + (int)Math.Round(2 * destY));
            }
            catch (Exception ex)
            {
                throw new ApplicationException(string.Format("destWidth:{0}, destX:{1}, destHeight:{2}, desxtY:{3}, Width:{4}, Height:{5}",
                    destWidth, destX, destHeight, destY, Width, Height), ex);
            }
            using (System.Drawing.Graphics objGraphics = System.Drawing.Graphics.FromImage(objBitmap))
            {
                objGraphics.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                objGraphics.CompositingQuality = System.Drawing.Drawing2D.CompositingQuality.HighQuality;
                objGraphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
                objGraphics.Clear(Color.White);
                Rectangle to = new System.Drawing.Rectangle((int)Math.Round(destX), (int)Math.Round(destY), destWidth, destHeight);
                Rectangle from = new System.Drawing.Rectangle(sourceX, sourceY, sourceWidth, sourceHeight);

                objGraphics.DrawImage(image, to, from, System.Drawing.GraphicsUnit.Pixel);
                objBitmap.Save(saveFilePath, image.RawFormat);
            }
            return objBitmap;
        }

        public static void OptimizeSaveJpeg(string SaveFilePath, Image image, int quality)
        {
            if ((quality < 0) || (quality > 100))
            {
                string error = string.Format("Jpeg image quality must be between 0 and 100, with 100 being the highest quality.  A value of {0} was specified.", quality);
                throw new ArgumentOutOfRangeException(error);
            }

            EncoderParameter qualityParam = new EncoderParameter(System.Drawing.Imaging.Encoder.Quality, quality);
            ImageCodecInfo jpegCodec = GetEncoderInfo("image/jpeg");

            EncoderParameters encoderParams = new EncoderParameters(1);
            encoderParams.Param[0] = qualityParam;
            image.Save(SaveFilePath, jpegCodec, encoderParams);
        }

        public static ImageCodecInfo GetEncoderInfo(string mimeType)
        {
            string lookupKey = mimeType.ToLower();

            ImageCodecInfo foundCodec = null;

            if (Encoders.ContainsKey(lookupKey))
            {
                foundCodec = Encoders[lookupKey];
            }

            return foundCodec;
        }
    }
}