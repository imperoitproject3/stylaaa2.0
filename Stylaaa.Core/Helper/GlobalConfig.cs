﻿using System;
using System.Configuration;
using System.Web;

namespace Stylaaa.Core.Helper
{
    public static class GlobalConfig
    {
        public const string ProjectName = "Stylaaa";
        public const string DateFormat = "yyyy-MM-dd'T'HH:mm";
        public const string DisplayDateTime = "dd/MM/yyyy HH:mm";
        public const string FacebookUsernamePrefix = "FBU";
        public const string FacebookPasswordSuffix = "FBP";
        public const string MobileUsernamePrefix = "PHU";
        public const string MobilePasswordSuffix = "PHP";

        public static double MetersInOneMile = 1609.344;
        public static double MetersInOneKilometers = 1000;


        public static string GoogleServerKey = "AIzaSyA6F1Lufvt8UGVN8qIEm0A3RmwSdiBqpEg";

        #region Path and URL
        public static bool IsLive = Convert.ToBoolean(ConfigurationManager.AppSettings["IsLive"].ToString());

        public static object ResetPasswordRoute = IsLive ? ConfigurationManager.AppSettings["ResetPasswordRouteLive"].ToString() : ConfigurationManager.AppSettings["ResetPasswordRouteLocal"].ToString();

        public static string baseUrl = ConfigurationManager.AppSettings["BaseRouteUrl"].ToString();
        public static string baseFileUrl = ConfigurationManager.AppSettings["BaseRouteFileUrl"].ToString();
        public static string baseRoutePath = ConfigurationManager.AppSettings["BaseRoutePath"];

        public static string ForgetPasswordUrl = baseUrl + "/Account/ResetPassword/?UserId=";
        public static string ActiveAccount = baseUrl + "/Account/Activate/?UserId=";

        public static string fileRoutePath = baseRoutePath + @"Files\";
        public static string fileBaseUrl = baseFileUrl + "Files/";

        public static string EmailTemplatePath = fileRoutePath + @"EmailTemplate\";
        public static string EmailTemplateUrl = fileBaseUrl + "EmailTemplate/";

        public static string UserImagePath = fileRoutePath + @"UserImages\";
        public static string UserImageUrl = fileBaseUrl + "UserImages/";

        public static string UserCoverPicPath = fileRoutePath + @"CoverPic\";
        public static string UserCoverPicUrl = fileBaseUrl + "CoverPic/";

       
        public static string CategoryImagePath = fileRoutePath + @"CategoryImages\";
        public static string CategoryImageUrl = fileBaseUrl + "CategoryImages/";

        public static string ChatImagePath = fileRoutePath + @"ChatImages\";
        public static string ChatImageurl = fileBaseUrl + "ChatImages/";

        public static string UserDefaultImage = "DefaultImage.jpg";

        public static string PostImagePath = fileRoutePath + @"PostImages\";
        public static string PostImageUrl = fileBaseUrl + "PostImages/";

        public static string JobImagePath = fileRoutePath + @"JobImages\";
        public static string JobImageUrl = fileBaseUrl + "JobImages/";

        //public static string Certificatepath = fileRoutePath + @"CertificateFile\";
        //public static string CertificateUrl = fileBaseUrl + "CertificateFile/";

        public static string StripeIdentityFilePath = fileRoutePath + @"StripeIdentityFile\";
        public static string StripeIdentityFileUrl = fileBaseUrl + "StripeIdentityFile/";

        public static string PostDefaultImage = "DefaultImage.jpg";
        public static string DefaultProfileImage = "placeholder.jpg";

        public static string chatUrl = ConfigurationManager.AppSettings["ChatUrl"];
        public static string HubConnectionUrl = chatUrl + "signalr/hubs";
        public static string HubGroupName = "OnlineUsers";

        public static string[] mediaExtensions = {
                            //".PNG", ".JPG", ".JPEG", ".BMP", ".GIF", //etc
                            //".WAV", ".MID", ".MIDI", ".WMA", ".MP3", ".OGG", ".RMA", //etc
                            //".AVI", ".MP4", ".DIVX", ".WMV", //etc
                            ".wmv", ".mp4",".mov",".MOV"
                        };

        public static string[] imageExtensions = { ".PNG", ".JPG", ".JPEG" };


        //public static bool IsXeroLive = true;
        //public static string XeroInvoiceFilePath = fileRoutePath + @"XeroInvoiceFiles\";
        //public static string XeroInvoiceFileUrl = fileBaseUrl + @"XeroInvoiceFiles\";
        
        public static string XeroReceiptFilePath = fileRoutePath + @"XeroReceiptFiles\";
        public static string XeroReceiptFileUrl = fileBaseUrl + @"XeroReceiptFiles\";

        //public static string Xero_pfxFileName = IsXeroLive ? "public_privatekey.pfx" : "public_privatekey.pfx";
        //public static string Xero_pfxFilePath = fileRoutePath + @"XeroCertificate\" + Xero_pfxFileName;
        //public static string Xero_pfxPassword = IsXeroLive ? "ridvan" : "ridvan";
        //public static string Xero_ConsumerKey = ConfigurationManager.AppSettings["Xero_ConsumerKey"];
        //public static string Xero_ConsumerSecret = ConfigurationManager.AppSettings["Xero_ConsumerSecret"];

        #endregion

        #region Stripe Credentials
        public static string StripePublishableKey = ConfigurationManager.AppSettings["StripePublishableKey"].ToString();
        public static string StripeApiKey = ConfigurationManager.AppSettings["StripeApiKey"].ToString();
        public static string StripeCurrency = "EUR";
        public static double StripeAdminFeedInPer = 10;
        public static string StripeCountry = "AU";
        public static string StripeClientId = ConfigurationManager.AppSettings["StripeClientId"].ToString();

        public static string StripeRedirectUrl = HttpContext.Current.Request.IsLocal ? ConfigurationManager.AppSettings["StripeRedirectUrlLocal"].ToString() : ConfigurationManager.AppSettings["StripeRedirectUrlLive"].ToString();

        public static string StripeConnectURL = "https://dashboard.stripe.com/oauth/authorize?response_type=code&client_id=" + StripeClientId + "&scope=read_write&redirect_uri=" + StripeRedirectUrl;
        #endregion

        #region Pagination
        public const int PageSize = 8;
        public const int NotificationPageSize = 1000;
        #endregion

        #region Email        
        public static string AdminEmailID = ConfigurationManager.AppSettings["AdminEmailID"].ToString();

        public static string EmailUserName = ConfigurationManager.AppSettings["EmailUserName"].ToString();
        public static string EmailPassword = ConfigurationManager.AppSettings["EmailPassword"].ToString();
        public static string SMTPClient = ConfigurationManager.AppSettings["SMTPClient"].ToString();
        #endregion

        public static DateTime GetSystemDateTime()
        {
            //return DateTime.Now;
            return DateTime.UtcNow;
        }
    }
}
