﻿namespace Stylaaa.Core.Helper
{
    public enum Gender
    {
        Female = 0,
        Male = 1
    }

    public enum LandingGender
    {
        Woman = 0,
        Male = 1,
        Kids = 2
    }

    public enum FilterPostType
    {
        Alle = 0,
        Aktuelle = 1,
        Beliebte = 2
    }

    public enum Role
    {
        SuperAdmin = 1,
        Stylist = 2,
        User = 3,
        Admin = 4
    }
    public enum LocationType
    {
        User = 0,
        Stylish = 1
    }

    public enum RequestStatus
    {
        None = 0,
        Accepted = 1,
        Rejected = 2
    }

    public enum LikeStatus
    {
        like = 1,
        dislike = 0
    }

    public enum ResponseStatus
    {
        Failed = 0,
        Success = 1,
        Unauthorized = 401,
        UpgradeRequired = 426,
        InternalError = 500,
        NotFound = 400,
        ServiceUnavailable = 503
    }

    public enum Platform
    {
        Android = 1,
        iOS = 2,
        Web = 3
    }

    public enum EmailTemplate
    {
        ActiveAccount,
        ActiveAccountDe,
        ForgotPassword,
        ForgotPasswordDe,
        ConfirmOTP,
        ConfirmOTPDe,
        SendInvitation,
        ChangePassword,
        ChangePasswordDe,
        AcceptRequest,
        AcceptRequestDe,
        ReceiveMessageRequest,
        ReceiveMessageRequestDe,
        RejectRequest,
        RejectRequestDe,
        DeactiveAccount,
        DeactiveAccountDe,
        UserRegistrationSuccess,
        StylishRegistrationSuccess,
        AppoinmentBookedUser,
        AppoinmentBookedStylish,
        AppoinmentAccepted,
        AppoinmentCancelledUser,
        AppoinmentCancelledStylish,
        OneDayBeforeAppoinmentUser,
        OneDayBeforeAppoinmentStylish,
        DeleteUserEmail,
        DeleteUserEmailDe,
        JobAddedForStylist,
        JobAddedForStylistDe,
        jobRequestForUser,
        jobRequestForUserDe,
        StylistBooking,
        StylistBookingDe,
        BookingRequest,
        BookingRequestDe
    }

    public enum DistanceUnit
    {
        Miles = 1,
        Kilometers = 2
    }

    public enum GenericCompareOperator
    {
        GreaterThan,
        GreaterThanOrEqual,
        LessThan,
        LessThanOrEqual
    }

    public enum StripeRequestStatus
    {
        succeeded,
        pending,
        failed
    }

    public enum languageType
    {
        English,
        German
    }

    public enum NotificationType
    {
        Post,
        Rate,
        like,
        Comment,
        ReplyOfComment,
        Follow,
        Message,
        jobInquery
    }

    public enum BotFunctionEnum
    {
        Like = 1,
        Follow = 2,
        Comment = 3,
        Message = 4
    }

    public enum BotMessageType
    {
        Hi = 1,
        HowAreYou = 2,
        WhatsYourPrice = 3
    }

    public enum RoleForBot
    {
        Stylist = 1,
        Users = 2
    }

    public enum StripeType
    {
        none = 0,
        card = 1,
        eps = 2,
        sofort = 3
    }

    public enum ChargeStatus
    {
        none = 0,
        expired = 1,
        failed = 2,
        pending = 3,
        refundUpdated = 4,
        refunded = 5,
        succeeded = 6,
        captured = 7,
    }

    public enum JobStatus
    {
        Deactivate = 0,
        Activated = 1,
        Completed = 2
    }

    public enum PaymentType
    {
        PremiumPack = 1,
        Credit = 2
    }

    public enum SchedularType
    {
        Follow = 1,
        Like = 2,
        Comment = 3,
        Message = 4
    }

}