﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Stylaaa.Core.Models
{
    public class SubCategoriesModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Category Name required")]
        public string Name { get; set; }

        public long CategoryId { get; set; }

        public LandingGender GenderId { get; set; }

        public string CategoryName { get; set; }

        public string SubCategoryName { get; set; }

        public double Price { get; set; }
    }

    public class AdminSubCategoryModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please select Categories")]
        public long CategoryId { get; set; }

        public LandingGender GenderId { get; set; }

        [Required(ErrorMessage = "Please enter price")]
        public double Price { get; set; }

        public List<SubCategoryLanguageModel> CategoryLanguageModelList { get; set; }
    }

    public class SubCategoryLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public languageType LanguageId { get; set; }

        [Required(ErrorMessage = "Please enter category name")]
        public string SubCategoryName { get; set; }

        public long CategoryId { get; set; }

        public LandingGender GenderId { get; set; }

        public double Price { get; set; }

        public virtual string LanguageName { get; set; }
    }

    public class SubCategoryDropdownModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
