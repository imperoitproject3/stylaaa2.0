﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class Amount
    {
        public string currency { get; set; }
        public int value { get; set; }
    }

    public class Payment
    {
        private Client client;

        public Payment(Client client)
        {
            this.client = client;
        }

        public Amount amount { get; set; }
        public string countryCode { get; set; }
        public string reference { get; set; }
        public DateTime sessionValidity { get; set; }
    }

    public class Group
    {
        public string name { get; set; }
        public string paymentMethodData { get; set; }
        public string type { get; set; }
    }

    public class Configuration
    {
        public string cvcOptional { get; set; }
    }

    public class InputDetail
    {
        public string key { get; set; }
        public string type { get; set; }
        public Configuration configuration { get; set; }
    }

    public class PaymentMethod
    {
        public Group group { get; set; }
        public List<InputDetail> inputDetails { get; set; }
        public string name { get; set; }
        public string paymentMethodData { get; set; }
        public string type { get; set; }
    }

    public class ResponceObjectModel
    {
        public string disableRecurringDetailUrl { get; set; }
        public DateTime generationtime { get; set; }
        public string html { get; set; }
        public string initiationUrl { get; set; }
        public string logoBaseUrl { get; set; }
        public string origin { get; set; }
        public Payment payment { get; set; }
        public string paymentData { get; set; }
        public List<PaymentMethod> paymentMethods { get; set; }
        public string publicKey { get; set; }
        public string publicKeyToken { get; set; }
    }

    public class VerifyObjectModel
    {
        public AdditionalData additionalData { get; set; }
        public string disableRecurringDetailUrl { get; set; }
        public string pspReference { get; set; }
        public string authResponse { get; set; }
        public string merchantReference { get; set; }
        public string paymentMethod { get; set; }
        public string shopperLocale { get; set; }
    }

    public class AdditionalData
    {
        public string authorisedAmountCurrency { get; set; }
        public string authorisedAmountValue { get; set; }
    }

    public class PaymentResponseData
    {
        public string ResponseData { get; set; }
        public string ResponseHtmlData { get; set; }
        public string ResponseMessage { get; set; }
    }
}
