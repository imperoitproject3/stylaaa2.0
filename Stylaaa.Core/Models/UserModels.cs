﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Web;

namespace Stylaaa.Core.Models
{
    public class UserRegisterModel
    {
        public long Id { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "registerUsernameValidMsg")]
        [StringLength(50, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "registerUsernamestringLengthMsg", MinimumLength = 2)]
        public string UserName { get; set; }

        [Display(Name = "E-Mail")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailRequireMsg")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailvalid")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordRequireMsg")]
        [StringLength(100, ErrorMessage = " {0} must be {2} character long", MinimumLength = 6)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [DataType(DataType.Password)]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "confirmPassword")]
        [Compare("Password", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordAgainVal")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "user role")]
        public Role role { get; set; }


        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "registerCountryValMsg")]
        public int CountryId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "registerZipcodeValid")]
        public int ZipCode { get; set; }

        public DbGeography DbGeography { get; set; }

        [Required(ErrorMessage = "Latitude required")]
        public double LocationLatitude { get; set; }

        [Required(ErrorMessage = "Longitude required")]
        public double LocationLongitude { get; set; }
        
        [Display(Name = "Accept check terms and conditions")]
        [Range(typeof(bool), "true", "true", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "registerAcceptTermsValid")]
        public bool CheckTermsAndConditions { get; set; }   
        
        [Display(Name = "Accept privacy policy")]
        [Range(typeof(bool), "true", "true", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "registerAcceptPrivacyValid")]
        public bool CheckPrivacyPolicy { get; set; }
    }

    public class UserLoginModel
    {

        [Display(Name = "email address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailRequireMsg")]
        //[EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailvalid")]
        public string Email { get; set; }

        [Display(Name = "password")]
        [DataType(DataType.Password)]

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordRequireMsg")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordValid")]
        public string Password { get; set; }

        [Display(Name = "user role")]
        public Role role { get; set; }

    }

    public class UserResponseModel
    {
        public string UserId { get; set; }
        public bool IsProfileSetup { get; set; }
        public bool IsActive { get; set; }
        public bool IsEmailConfirmed { get; set; }
        public Role Role { get; set; }
    }

    public class ProfileImagesModel
    {
        public long Id { get; set; }
        public string ProfileImageName { get; set; }
        public bool isDefault { get; set; }
    }

    public class WebUserResponseModel
    {
        public bool IsProfileSetup { get; set; }
        public bool IsStripeConnected { get; set; }
        public Role Role { get; set; }
    }

    public class UserProfileSetupModel
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "pleaseEnterNickname")]
        public string UserName { get; set; }

        public string ProfileImageName { get; set; }

        public Gender GenderId { get; set; }

        public string TimeZoneName { get; set; }

        public string Location { get; set; }
        [Required(ErrorMessage = "latitude equired")]
        public double LocationLatitude { get; set; }
        [Required(ErrorMessage = "longtitude required")]
        public double LocationLongitude { get; set; }

        public bool IsMyPlace { get; set; }

        public bool IsCustomerplace { get; set; }

        //public string City { get; set; }

        [Required(ErrorMessage = "Adresse required")]
        public string Address { get; set; }

        public virtual HttpPostedFileBase fileBaseImage { get; set; }
    }

    public class EditPasswordModel
    {
        public ViewStylishProfileModel Stylish { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "editpasswordValid1")]
        public string oldPassWord { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "editpasswordvalid2")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordValid")]
        [DataType(DataType.Password)]
        public string newPassWord { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "edipasswordValid3")]
        [DataType(DataType.Password)]
        [Compare("newPassWord", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordAgainVal")]
        public string ConfirmPassword { get; set; }
    }

    public class UserDetailModel
    {
        public string UserId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string ProfileImage { get; set; }

        public string Gender { get; set; }

        public string Location { get; set; }

        public bool IsActive { get; set; }

        public bool IsCertified { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    //public class UserLoginResponseModel
    //{
    //    public string UserId { get; set; }
    //    public string Name { get; set; }
    //    public string Email { get; set; }
    //    public Role Role { get; set; }
    //    public bool isBankDetailSaved { get; set; }
    //    public bool IsProfileSetup { get; set; }
    //}

    /// <summary>
    /// user profile models
    /// </summary>
    public class ViewUserProfileModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Gender GenderId { get; set; }

        public string NickName { get; set; }

        [Required(ErrorMessage = "latitude required")]
        public double LocationLatitude { get; set; }
        [Required(ErrorMessage = "longtitude required")]
        public double LocationLongitude { get; set; }

        public string Address { get; set; }

        public string ProfileImageName { get; set; }

        public string Location { get; set; }

        public Role Role { get; set; }

        public int FollowingCount { get; set; }

        public int FollowerCount { get; set; }

        public virtual HttpPostedFileBase fileBaseImage { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    public class UserProfileModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string StreetAddress { get; set; }

        public string CityName { get; set; }

        public string ContactNumber { get; set; }

        public int zipcode { get; set; }

        public string Email { get; set; }
    }

    public class ViewUserFollowingModel
    {
        public ViewUserProfileModel User { get; set; }
        public List<ViewUsersFollowing> Following { get; set; }
    }

    public class ViewUsersFollowing
    {
        public string Id { get; set; }
        public string FollowingId { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public string NickName { get; set; }
        public string ProfileImageName { get; set; }
        public bool IsFollow { get; set; }
        public Role role { get; set; }
    }

    public class UserEditProfileModel
    {
        public ViewUserProfileModel User { get; set; }
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        public Gender GenderId { get; set; }
        [Required]
        public string NickName { get; set; }

        public double LocationLatitude { get; set; }

        public double LocationLongitude { get; set; }

        public string Address { get; set; }

        [Required(ErrorMessage = "Upload profile picture")]
        public string ProfileImageName { get; set; }

        public string Location { get; set; }
        public Role Role { get; set; }

        public virtual HttpPostedFileBase fileBaseImage { get; set; }
    }


    public class UserEditPasswordModel
    {
        public ViewUserDetail User { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "editpasswordValid1")]
        [Display(Name = "old password")]
        public string oldPassWord { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "editpasswordvalid2")]
        [Display(Name = "new password")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordValid")]
        [DataType(DataType.Password)]
        public string newPassWord { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "edipasswordValid3")]
        [DataType(DataType.Password)]
        [Display(Name = "confirm password")]
        [Compare("newPassWord", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordAgainVal")]
        public string ConfirmPassword { get; set; }

        public bool IsActive { get; set; }
    }

    public class ResetPasswordModel
    {
        public string UserId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "editpasswordvalid2")]
        [Display(Name = "new password")]
        [StringLength(100, ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordValid")]
        [DataType(DataType.Password)]
        public string newPassWord { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "edipasswordValid3")]
        [DataType(DataType.Password)]
        [Display(Name = "confirm password")]
        [Compare("newPassWord", ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "passwordAgainVal")]
        public string ConfirmPassword { get; set; }
    }


    public class UserForgotPasswordModel
    {

        [Display(Name = "email address")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailRequireMsg")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailvalid")]
        public string Email { get; set; }
    }

    public class ViewUserDetail
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public Role Role { get; set; }
        public string ProfileImageUrl { get; set; }

        public string ProfileImageName { get; set; }
        public string CoverPicUrl { get; set; }

        public string CoverPicName { get; set; }

        public DateTime CreatedDate { get; set; }

        public int PostCount { get; set; }

        public string AboutMe { get; set; }

        public int FollowingCount { get; set; }

        public int FollowerCount { get; set; }

        public string profileUrl { get; set; }

        public string userExpiryDate { get; set; }

        public bool IsUserExpire { get; set; }

        public bool IsPremium { get; set; }

        public string StripeAccountId { get; set; }

        public string Email { get; set; }
    }


    public class UserFollowTransactionModel
    {
        public ViewUserDetail User { get; set; }
        public List<UsersFollowModel> FollowModel { get; set; }

    }

    public class ViewAllFollowModel
    {
        public decimal PageCount { get; set; }
        public int PageIndex { get; set; }
        public List<UsersFollowModel> UsersList { get; set; }
    }
    public class UsersFollowModel
    {
        public string Id { get; set; }
        public string UserId { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public Role Role { get; set; }

        public string ProfileImageName { get; set; }

        public string Message { get; set; }

        public DateTime? Experiance { get; set; }

        public double Year { get; set; }

        public double Months { get; set; }

        public bool Isfollow { get; set; }

        public bool IsPremiumUser { get; set; }

        public string message { get; set; }

        public double? Ratings { get; set; }

        public double? TotalRatings { get; set; }
    }

    public class SendtoAllModel
    {
        public SendtoAllModel()
        {
            User = Stylist = new List<UserListModel>();
        }
        public string textSMS { get; set; }
        public List<UserListModel> User { get; set; }
        public List<UserListModel> Stylist { get; set; }
    }

    public class UserListModel
    {
        public UserListModel()
        {
            IsChecked = true;
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public bool IsChecked { get; set; }
    }

    public class UserTableModel
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public int ZipCode { get; set; }
    }
    
}