﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class AdminLoginModel
    {
        public AdminLoginModel()
        {
            role = Role.Admin;
            rememberMe = false;
        }
        [Required]
        [EmailAddress]
        [DisplayName("Email Address")]
        public string emailId { get; set; }

        [Required]
        [DisplayName("Password")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string password { get; set; }

        public Role role { get; set; }

        public bool rememberMe { get; set; }
    }

    public class AdminConfirmOtpModel
    {
        [Required(ErrorMessage = "Please enter {0}")]
        public string ConfirmOtp { get; set; }
    }

    public class indexpage
    {
        public string Stylish { get; set; }
        public string User { get; set; }
    }

    public class AdminChangePasswordModel
    {
        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 6)]
        public string currentPassword { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 6)]
        public string newPassword { get; set; }

        [Required(ErrorMessage = "Please enter {0}")]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long", MinimumLength = 6)]
        [Compare("newPassword", ErrorMessage = "Confirm password does nat match with new password.")]
        public string confirmPassword { get; set; }
    }

    public class AddAdminUserModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage = "Bitte Name eingeben")]
        [StringLength(50, ErrorMessage = "{0} muss mindestens {2} Zeichen lang sein!", MinimumLength = 2)]
        public string Name { get; set; }

        [Display(Name = "E-Mail")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailRequireMsg")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailvalid")]
        public string Email { get; set; }

        [Display(Name = "passwort")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Bitte {0} eingeben")]
        [StringLength(100, ErrorMessage = " {0} muss mindestens {2} Zeichen lang sein!", MinimumLength = 6)]
        public string Password { get; set; }

        [Display(Name = "Passwort bestätigen")]
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Bitte {0} erneut eingeben")]
        [Compare("Password", ErrorMessage = "Passwörter stimmen nicht überein!")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "user role")]
        public Role role { get; set; }

        public Gender GenderId { get; set; }

    }
}
