﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class StylishByFilter
    {
        public string StylishId { get; set; }

        public string Name { get; set; }

        public Gender GenderId { get; set; }

        public string NickName { get; set; }

        public string ProfileImageName { get; set; }

        public string Location { get; set; }

        public Role Role { get; set; }

        public double? Ratings { get; set; }

        public double? TotalRatings { get; set; }

        public long? CategoryId { get; set; }

        public double? rating { get; set; }

        public string SerachText { get; set; }
       
    }
}
