﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class PaymentMethodModel
    {
        public long PaymentMethodId { get; set; }

        public string PaymentMethod { get; set; }

        public string PaymentMethodText { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    public class AdminPaymentMethodModel
    {
        public long Id { get; set; }        
       
        public List<PaymentMethodLanguageModel> PaymentMethodLanguageList { get; set; }
    }

    public class PaymentMethodLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public languageType LanguageId { get; set; }

        [Required(ErrorMessage = "Please enter PaymentMethodName")]
        public string PaymentMethodName { get; set; }

        public string PaymentMethodText { get; set; }

        public virtual string LanguageName { get; set; }
    }

   
}
