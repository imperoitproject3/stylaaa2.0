﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class XeroReceiptUserModel
    {
        public string ContactNumber { get; set; }
        public string CompanyEmailID { get; set; }
        public string CompanyName { get; set; }
        public string FullAddress { get; set; }
        public string StreetAddress { get; set; }
        public string CityName { get; set; }
        public string Zipcode { get; set; }
        public string EmailID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
    public class XeroReceiptSendModel
    {
        public XeroReceiptUserModel userData { get; set; }
        public string Title { get; set; }
        public string TokenID { get; set; }
        public double Amount { get; set; }
        public double Vat { get; set; }
        public string ReceiptID { get; set; }
    }
}
