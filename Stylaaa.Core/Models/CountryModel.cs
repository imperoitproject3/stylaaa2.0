﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class CountryModel
    {
        public long CountryId { get; set; }

        [Required(ErrorMessage = "Country Name required")]
        public string Name { get; set; }

    }
}
