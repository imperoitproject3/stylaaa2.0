﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class AboutUsModel
    {
        public long Id { get; set; }

        public List<AboutUsLanguageModel> AboutUsLanguageModelList { get; set; }
    }
    public class AboutUsLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public languageType LanguageId { get; set; }

        [Required(ErrorMessage = "Please enter about us text")]
        public string AboutUs { get; set; }

        public virtual string LanguageName { get; set; }
    }

    public class LanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public languageType LanguageId { get; set; }

        [Required(ErrorMessage = "Please enter text")]
        public string Content { get; set; }

        public virtual string LanguageName { get; set; }
    }

    public class ContactUsModel
    {
        public long Id { get; set; }

        [Display(Name = "your name")]
        //[Required(ErrorMessage = "Please enter {0}")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "PleaseEnterName")]
        public string Name { get; set; }

        [Display(Name = "email address")]
        //[Required(ErrorMessage = "Please enter {0}")]
        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailRequireMsg")]
        //[EmailAddress(ErrorMessage = "Please enter valid email address")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailvalid")]
        public string Email { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "contactUsmessageValid")]
        [Column(TypeName = "varchar(MAX)")]
        public string Message { get; set; }

        public DateTime CreatedDate { get; set; }
    }

    public class ImprintsModel
    {
        public long Id { get; set; }

        public List<LanguageModel> ImprintLanguageModelList { get; set; }
    }

    public class PrivacyPolicyModel
    {
        public long Id { get; set; }

        public List<LanguageModel> PolicyLanguageModelList { get; set; }
    }

    public class TermsModel
    {
        public long Id { get; set; }

        public List<LanguageModel> TermsLanguageModelList { get; set; }
       
    }

    public class WhatIsStylaaaModel
    {
        public long Id { get; set; }

        public List<LanguageModel> WhatIsStylaaLanguageModelList { get; set; }
    }

    public class WhatIsStylaaaForUserModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter what is stylaaa for user")]
        public string WhatIsStylaaaForUserText { get; set; }
    }

    public class WhatIsStylaaaForStylistModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please enter what is stylaaa for stylist")]
        public string WhatIsStylaaaForStylistText { get; set; }
    }

    public class DashboardCount
    {
        public int TotalStylist { get; set; }
        public int TotalUser { get; set; }
        public int TotalPost { get; set; }
        public int TotalRequest { get; set; }
    }
}
