﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Stylaaa.Core.Models
{
    public class CategoryModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Category Name required")]
        public string Name { get; set; }

        //[Required(ErrorMessage = "Category Image required")]
        public string CategoryImage { get; set; }
    }

    public class CategoryDropdownModel
    {
        public long Id { get; set; }
               
        public string Name { get; set; }

        //public string selectedService { get; set; }
    }


    public class AdminCategoryDropdownmodel
    {
        public long Id { get; set; }

        public string Name { get; set; }
    }

    public class AdminCategoryModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Please upload category image")]
        public string ImageName { get; set; }       

        public virtual HttpPostedFileBase fileBaseImage { get; set; }

        public List<CategoryLanguageModel> CategoryLanguageModelList { get; set; }
    }

    public class CategoryLanguageModel
    {
        [Required(ErrorMessage = "Please select language")]
        public languageType LanguageId { get; set; }

        [Required(ErrorMessage = "Please enter category name")]
        public string CategoryName { get; set; }

        public virtual string LanguageName { get; set; }
    }



}
