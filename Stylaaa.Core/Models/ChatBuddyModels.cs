﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class BuddyViewModel
    {
        public bool IsSendByMe { get; set; }
        public string ChatUserID { get; set; }
        public string Name { get; set; }
        public string LastMessageText { get; set; }
        public string LastMessageTimeCaption { get; set; }
    }

    public class UnreadMessageCountModel
    {
        public string UserId { get; set; }
        public int Count { get; set; }
    }

    public class ChatModel
    {
        public IQueryable<UserModel> ChatMessageModel { get; set; }
        public List<JobChatModel> JobMessageModel { get; set; }

    }

    public class UserModel
    {
        public long ChatId { get; set; }

        public string Id { get; set; }

        [Required(ErrorMessage = "Please enter your name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Please enter your email")]
        public string Email { get; set; }

        public string ProfileImage { get; set; }

        public int UnreadCount { get; set; }

        public string LastMessage { get; set; }

        public bool isImage { get; set; }

        public int ChatCount { get; set; }

        public string senderUserName { get; set; }

        public string Senderid { get; set; }

        public DateTime createddate { get; set; }

        public bool isRequestAccepted { get; set; }
        public Role role { get; set; }
    }

    public class JobChatModel
    {
        public long ChatId { get; set; }

        public string Id { get; set; }

        public long JobId { get; set; }

        public string Name { get; set; }

        public string Email { get; set; }

        public string ProfileImage { get; set; }

        public int UnreadCount { get; set; }

        public string JobTitle { get; set; }

        public bool isImage { get; set; }

        public int ChatCount { get; set; }

        public string senderUserName { get; set; }

        public string Senderid { get; set; }

        public DateTime createddate { get; set; }

        public bool isRequestAccepted { get; set; }

        public Role role { get; set; }
    }
}
