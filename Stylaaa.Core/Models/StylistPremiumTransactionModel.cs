﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class GetStylistPaymenList
    {
        public int StylistHistoryPageIndex { get; set; }
        public decimal StylistHistoryPageCount { get; set; }

        public List<StylistPaymentHistoryModel> StylistHistoryList { get; set; }
    }

    public class StylistPaymentHistoryModel
    {
        public long PaymentHistoryId { get; set; }

        public string StylistId { get; set; }

        public string Package { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public bool isExpire { get; set; }

        public string PaymentMothod { get; set; }

        public string AmountPaid { get; set; }

        public string InvoiceLink { get; set; }

        public ChargeStatus PaymentStatus { get; set; }
    }

    public class StylistPremiumTransactionModel
    {
        public string StylistId { get; set; }

        public string PaymentMothod { get; set; }

        public int month { get; set; }

        public string invoicefilename { get; set; }

        public string receiptfilename { get; set; }

        public string stripeResponce { get; set; }

        public ChargeStatus paymentStatus { get; set; }

        public string transactionId { get; set; }
    }

    public class StylistCreditTransactionModel
    {
        public string StylistId { get; set; }

        public string PaymentMothod { get; set; }

        public int Credit { get; set; }

        //public string invoicefilename { get; set; }

        //public string receiptfilename { get; set; }

        public string stripeResponce { get; set; }

        public ChargeStatus paymentStatus { get; set; }

        public string transactionId { get; set; }
    }


    public class GetStylistCreditHistoryList
    {
        public int StylistHistoryPageIndex { get; set; }
        public decimal StylistHistoryPageCount { get; set; }
        public int RemainingCredit { get; set; }
        public List<StylistCreditPaymentHistory> CreditHistoryList { get; set; }
        public List<UpgradeCreditModel> CreditMasterDetail { get; set; }
    }

    public class StylistCreditPaymentHistory
    {
        public long JobId { get; set; }

        public string JobName { get; set; }

        public string StylistId { get; set; }

        public DateTime Date { get; set; }

        public string Location { get; set; }

        public string Country { get; set; }

        public int Zipcode { get; set; }

        public int CreditUsed { get; set; }

        public double Budget { get; set; }
    }
}
