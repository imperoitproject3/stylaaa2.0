﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class UserRoleMenuModel
    {
        public int Id { get; set; }
        public Role Role { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
    }
}
