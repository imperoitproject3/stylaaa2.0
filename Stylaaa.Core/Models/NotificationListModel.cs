﻿
using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class GetNotificationViewListModel
    {
        public int NotificationPageIndex { get; set; }
        public string NotificationPageCount { get; set; }
        public int UnReadNotificationCount { get; set; }

        public List<NotificationListModel> NotificationList { get; set; }
    }

    public class NotificationListModel
    {
        public long Id { get; set; }
        public string SenderId { get; set; }
        public Role SenderRole { get; set; }
        public string SenderUserName { get; set; }
        public string SenderName { get; set; }
        public string SenderProfileImage { get; set; }

        public string SenderLinkUrl { get; set; }
        public string SenderChatLinkUrl { get; set; }
        public string SenderMobileLinkUrl { get; set; }
        public string SenderFollowLinkUrl { get; set; }
        public string FollowText { get; set; }
        public string FollowTextClass { get; set; }
        public string PostlinkUrl { get; set; }
        
        public string ReceiverId { get; set; }
        public Role ReceiverRole { get; set; }
        public string ReceiverUserName { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverProfileImage { get; set; }

        public bool isRead { get; set; }
        public bool IsFollow { get; set; }
        
        public long PostId { get; set; }
        public string PostImage { get; set; }
        public bool IsVideo { get; set; }
        
        public string NotificationText { get; set; }
        public NotificationType NotificationTypeID { get; set; }
        public double Rating { get; set; }

        public DateTime AddedOn { get; set; }
        public string CommentedTimeSpan { get; set; }
    }
}
