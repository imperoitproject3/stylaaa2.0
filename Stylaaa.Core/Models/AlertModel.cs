﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public enum AlertStatus
    {
        danger = 0,
        success = 1,
        info = 2,
        warning = 3,
        primary = 4
    }

    public class AlertModel
    {
        public AlertModel(AlertStatus alertStatus, string alertMessage)
        {
            this.alertStatus = alertStatus;
            this.alertMessage = alertMessage;
        }
        public AlertStatus alertStatus { get; set; }
        public string alertMessage { get; set; }
    }
}
