﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class Client
    {
        public Config Config { get; set; }

        public Client(string username, string password, Environment environment, string applicationName)
        {
            Config = new Config
            {
                Username = username,
                Password = password,
                ApplicationName = applicationName,
                Environment = Environment.Test
            };
            this.SetEnviroment(environment);
        }

        public Client(Config config)
        {
            Config = config;
        }

        public void SetEnviroment(Environment environment)
        {
            Config.Environment = environment;
            switch (environment)
            {
                case Environment.Test:
                    Config.Endpoint = ClientConfig.EndpointTest;
                    Config.HppEndpoint = ClientConfig.HppTest;
                    break;

                case Environment.Live:
                    Config.Endpoint = ClientConfig.EndpointLive;
                    Config.HppEndpoint = ClientConfig.HppLive;
                    break;
            }
        }


        public string ApiVersion
        {
            get
            {
                return ClientConfig.ApiVersion;
            }
        }

        public string LibraryVersion
        {
            get
            {
                return ClientConfig.LibVersion;

            }
        }
    }

    public class Config
    {
        //Merchant details
        public string Username { get; set; }
        public string Password { get; set; }
        public string MerchantAccount { get; set; }

        public Environment Environment { get; set; }
        public string Endpoint { get; set; }
        public string ApplicationName { get; set; }

        //HPP specific
        public string HppEndpoint { get; set; }
        public string SkinCode { get; set; }
        public string HmacKey { get; set; }
    }

    public enum Environment
    {
        Test,
        Live
    }

    public class ClientConfig
    {
        public static string EndpointTest = "https://pal-test.adyen.com";
        public static string EndpointLive = "https://pal-live.adyen.com";
        public static string HppTest = "https://test.adyen.com/hpp";
        public static string HppLive = "https://live.adyen.com/hpp";
        public static string RecurringApiVersion = "v25";
        public static string ApiVersion = "v30";
        public static string UserAgentSuffix = "adyen-dotnet-api-library/";
        public static string LibVersion = "1.0.3";
    }
}
