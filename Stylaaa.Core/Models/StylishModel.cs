﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
//using System.Web.Mvc.RemoteAttribute;

namespace Stylaaa.Core.Models
{
    public class UserAboutModel
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }

        //[Required]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Adresse eingeben")]
        public long CountryId { get; set; }

        public string CountryName { get; set; }

        public Gender GenderId { get; set; }

        public double LocationLatitude { get; set; }

        public double LocationLongitude { get; set; }

        [Required]
        public int Zipcode { get; set; }

        public string About { get; set; }

    }

    public class StylistAboutModel
    {
        public string Id { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        //[System.Web.Mvc.Remote("IsUserExists", "Stylist", ErrorMessage = "User Name already in use")]
        public string UserName { get; set; }

        public long CountryID { get; set; }

        public string CountryName { get; set; }

        public Gender GenderId { get; set; }

        public DateTime? Experiance { get; set; }

        public int year { get; set; }

        public double month { get; set; }

        [Required]
        public int Zipcode { get; set; }
                
        public double LocationLatitude { get; set; }
               
        public double LocationLongitude { get; set; }

        public string About { get; set; }

        public string SelectedCategory { get; set; }

        public List<CategoryCheckbox> Services { get; set; }

        //public List<CategoryModel> categorylist { get; set; }

        public List<StylishCategoryModel> categorylist { get; set; }

    }

    public class StylishCategoryModel
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public bool Selected { get; set; }
        public string CategoryImageName { get; set; }
    }

    public class CategoryCheckbox
    {
        public long Value { get; set; }
        public string Text { get; set; }
        public bool IsChecked { get; set; }
    }


    public class ViewStylishProfileModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }

        public Role Role { get; set; }

        public string ProfileImageName { get; set; }

        public int PostCount { get; set; }

        public int WorkCount { get; set; }

        public bool Isfollw { get; set; }

        public int FollowingCount { get; set; }

        public int FollowerCount { get; set; }

        public List<ViewStylishPost> Postdetail { get; set; }
    }



    public class StylistCategoryServiceModel
    {
        public string CategoryName { get; set; }
    }

    //profile setup
    public class StylishProfileSetupModel
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        [Required]
        public string NickName { get; set; }

        [Required]
        public string ProfileImageName { get; set; }

        public string TimeZoneName { get; set; }

        [Required(ErrorMessage = "Please select gender")]
        public Gender GenderId { get; set; }

        [Required(ErrorMessage = "Ort auswählen")]
        public string Location { get; set; }
        public double LocationLatitude { get; set; }
        public double LocationLongitude { get; set; }

        //public List<SelectStylishCategoryModel> Category { get; set; }      
    }

    public class UploadStylishCategoryModel
    {
        public string Name { get; set; }
    }

    public class SelectStylishCategoryModel
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }

    public class ViewStylishModel
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public Gender GenderId { get; set; }

        public string ProfileImageName { get; set; }

        public string Location { get; set; }

        public Role Role { get; set; }

        public bool Isfollow { get; set; }

        public double? Ratings { get; set; }

        public double? TotalRatings { get; set; }

        public double? Miles { get; set; }
    }


    public class StylishProfileDetailModel : PaginationModel
    {
        public ViewStylishProfileModel Stylish { get; set; }
        public List<ViewStylishPost> PostList { get; set; }
    }

    public class ViewStylishPost
    {
        public long PostId { get; set; }

        public string StylishId { get; set; }

        public string FileName { get; set; }

        public bool IsVideo { get; set; }

        public int CountLike { get; set; }

        public int CountComment { get; set; }
    }

    public class ViewAllStylishModel
    {
        public string StylishId { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string ProfileImageName { get; set; }

        public string Location { get; set; }

        public string Message { get; set; }

        public DateTime? Experiance { get; set; }

        public Role Role { get; set; }

        public bool Isfollow { get; set; }

        public bool IsPremiumUser { get; set; }

        public double? Ratings { get; set; }

        public double? TotalRatings { get; set; }

        public double? distance { get; set; }

        public double? Latitutde { get; set; }

        public double? Longtitude { get; set; }

        public double Year { get; set; }

        public double Months { get; set; }

        public bool IsVarified { get; set; }
    }


    //used in AdminPanel
    public class ViewStylishDetailsModel
    {
        public string StylishId { get; set; }

        public string NickName { get; set; }

        public string AboutMe { get; set; }

        public string Name { get; set; }

        public DateTime? Experiance { get; set; }

        public int year { get; set; }

        public double? Ratings { get; set; }

        public double? Average { get; set; }

        public double? TotalRatings { get; set; }

        public string ProfileImageName { get; set; }

        public int PostCount { get; set; }

        public int? FollowingCount { get; set; }

        public int? FollowerCount { get; set; }

        public DateTime CreatedDate { get; set; }

        public List<StylistCategoriesModel> StylistCategories { get; set; }
    }

    public class StylistCategoriesModel
    {
        public string stylistId { get; set; }

        public string Name { get; set; }

        public string ImageName { get; set; }
    }



    public class StylishEditProfile
    {
        public string StylishId { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string Location { get; set; }
        public ViewStylishProfileModel Stylish { get; set; }
    }

    //Followers
    public class StylishFollowingModel
    {
        public ViewUserDetail Stylish { get; set; }
        public List<ViewAllStylishModel> Followings { get; set; }
    }
    //Followers
    public class StylishFollowerModel
    {
        public ViewStylishProfileModel Stylish { get; set; }
        public List<ViewStylishFollowerModel> Followers { get; set; }
    }

    public class ViewStylishFollowerModel
    {
        public string StylishId { get; set; }
        public string FollowerId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string NickName { get; set; }
        public string ProfileImageName { get; set; }
        public bool IsFollow { get; set; }
        public Role role { get; set; }
    }

    //Stylish List : Filter Stylish Page
    public class StylishFilterPostModel
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public int PageIndex { get; set; }
        public decimal UserRecordCount { get; set; }
        public Gender? Gender { get; set; }
        public int? Miles { get; set; }
        public int? Rating { get; set; }
        public long? CategoryId { get; set; }
    }
    public class SearchPopularStylishModel
    {
        public List<PopularStylishFilterModel> PopularStylishFilterModel { get; set; }
        public string PageSize { get; set; }
    }

    public class PopularStylishFilterModel
    {
        public string Name { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }
        public Role UserRole { get; set; }
        public string UserProfile { get; set; }
        public int PageIndex { get; set; }
        public IEnumerable<long> Categories { get; set; }

        public double Distance { get; set; }

        public double? Latitude { get; set; }
        public bool IsPremium { get; set; }
        public double? Longitude { get; set; }
        public string UserLinkUrl { get; set; }
    }
}
