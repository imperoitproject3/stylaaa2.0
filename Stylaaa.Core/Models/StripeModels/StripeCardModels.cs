﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models.StripeModels
{
    public class StripeCardRequestModel
    {
        public string stripeCustomerId { get; set; }
        public string stripeCardToken { get; set; }
    }
    public class StripeCardResultModel
    {
        public string stripeCardId { get; set; }
    }
}
