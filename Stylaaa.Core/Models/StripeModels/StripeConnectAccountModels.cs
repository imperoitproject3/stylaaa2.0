﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models.StripeModels
{
    public class StripeConnectAccountRequestModel
    {
        public string stripeOAuthCode { get; set; }
        public string identityProofName { get; set; }
    }
    public class StripeConnectAccountUpdateModel
    {
        public string identityProofName { get; set; }
        public string stripeConnectAccountId { get; set; }
    }
    public class StripeConnectAccountResultModel
    {
        public string stripeConnectAccountId { get; set; }
    }
}
