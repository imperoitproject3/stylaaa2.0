﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models.StripeModels
{
    public class StripePaymentChargeRequestModel
    {
        public string stripePaymentToken { get; set; }
        public int chargeAmount { get; set; }
        public string stripeCustomerId { get; set; }
        public string receiptEmail { get; set; }
    }

    public class StripePaymentChargeResultModel : StripePaymentSourceRequestModel
    {
        public string stripePaymentChargeToken { get; set; }
    }

    public class StripePaymentSourceRequestModel
    {
        public int chargeAmount { get; set; }
        public string stripeCustomerId { get; set; }
        //public string invoiceNo { get; set; }
        public string CustomerEmail { get; set; }
        public string CustomerName { get; set; }
        public StripeType stripeType { get; set; }
    }
}
