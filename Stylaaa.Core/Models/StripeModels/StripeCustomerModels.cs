﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models.StripeModels
{
    public class StripeCustomerRequestModel
    {
        public string name { get; set; }
        public string email { get; set; }
    }

    public class StripeCustomerResultModel
    {
        public string customerId { get; set; }
    }
}
