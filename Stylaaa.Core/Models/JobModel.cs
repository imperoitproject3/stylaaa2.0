﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class ViewJobListModel
    {
        public long JobId { get; set; }

        public string UserId { get; set; }

        public string Country { get; set; }

        public int Zipcode { get; set; }

        public string Title { get; set; }

        public double Price { get; set; }

        public int FileCount { get; set; }

        public bool IsAbleToSend { get; set; }

        public DateTime CreatedDate { get; set; }

        public List<JobFilesModel> JobFilesList { get; set; }
        public List<JobCategoryModel> JobCategoryList { get; set; }

    }

    public class GetUserJobs
    {
        public int RecentJobPageIndex { get; set; }
        public decimal RecentJobPageCount { get; set; }
        public List<GetJobListByUser> UserJobList { get; set; }
        public List<JobCategoryModel> jobCategoryList { get; set; }
    }

    public class GetJobListByUser
    {
        public long JobId { get; set; }

        public string UserId { get; set; }

        public string Country { get; set; }

        public int Zipcode { get; set; }

        public string Title { get; set; }

        public double Price { get; set; }

        public int FileCount { get; set; }

        public JobStatus Status { get; set; }

        public DateTime CreatedDate { get; set; }

        public List<JobFilesModel> JobFilesList { get; set; }
        public List<JobCategoryModel> JobCategoryList { get; set; }
    }

    public class AddJobModel
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        public string Title { get; set; }

        public int ZipCode { get; set; }

        public string Country { get; set; }

        public double LocationLatitude { get; set; }

        public double LocationLongitude { get; set; }

        public double Price { get; set; }

        public string HiddenJobFileName { get; set; }

        public string HiddenJobFilePath { get; set; }

        public string[] JobFilesNamesArray { get; set; }

        public List<JobFilesModel> JobFiles { get; set; }

        public List<JobCategoryModel> JobCategoryList { get; set; }

        public List<JobCategoryModel> SearchJobCategoryList { get; set; }
    }


    public class JobFilesModel
    {
        public long Id { get; set; }
        public long JobId { get; set; }
        public string FileName { get; set; }
        public bool IsVideo { get; set; }
    }

    public class JobCategoryModel
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public bool Selected { get; set; }
    }

    public class GetjobViewListModel
    {
        public List<JobCategoryModel> CategoryList { get; set; }

        public string metaTitle { get; set; }
        public string metaDescription { get; set; }
        public string metaImageUrl { get; set; }
    }

    public class jobFilterModel
    {
        public string UserId { get; set; }
        public decimal JobPageCount { get; set; }
        public int JobPageIndex { get; set; }

        public long? CategoryId { get; set; }
    }

    public class GetAllJobList
    {
        public long JobId { get; set; }

        public string UserId { get; set; }

        public string Title { get; set; }       

        public string Name { get; set; }

        public double  Amount { get; set; }

        public int Credit { get; set; }

        public int Zipcode { get; set; }

        public string Country { get; set; }

        public string JobStatus { get; set; }

        public bool IsActive { get; set; }

        public string ProfileImageNAme { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Timeago { get; set; }

        //public string PostPic { get; set; }
        
        public List<JobCategoryModel> jobCategoryModel { get; set; }
    }
}
