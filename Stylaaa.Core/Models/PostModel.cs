﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Stylaaa.Core.Models
{
    public class AddPostModel
    {
        public long Id { get; set; }

        public string UserId { get; set; }

        public string[] HashtagsArray { get; set; }

        public string HiddenHashtags { get; set; }

        public string HiddenPostFileName { get; set; }

        public string HiddenPostThumblineFile { get; set; }

        public string HiddenPostFilePath { get; set; }

        public string[] PostFilesNamesArray { get; set; }

        public string Description { get; set; }

        public List<PostHashtagsModel> PostHashtags { get; set; }

        public List<PostTransactionModel> PostFiles { get; set; }

        public List<PostCategoryModel> PostCategoryList { get; set; }

        public List<PostCategoryModel> SearchPostCategoryList { get; set; }
    }

    public class PostViewModel
    {
        public long PostId { get; set; }
        public List<PostTransactionModel> PostTransactions { get; set; }
    }

    public class PostTransactionModel
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public string FileName { get; set; }
        public bool IsVideo { get; set; }
    }

    public class PostHashtagsModel
    {
        public long Id { get; set; }
        public long PostId { get; set; }
        public string Hashtag { get; set; }
    }

    public class PostCategoryModel
    {
        public long Id { get; set; }
        public string CategoryName { get; set; }
        public bool Selected { get; set; }
    }

    public class homePagePostList : PaginationModel
    {
        public List<GetPostModelRecent> RecentPost { get; set; }
        public List<GetPostModelPopular> PopularPost { get; set; }
    }

    public class GetPostViewListModel
    {
        public int RecentPostPageIndex { get; set; }
        public decimal RecentPostPageCount { get; set; }
        public int PopularPostPageIndex { get; set; }
        public decimal PopularPostPageCount { get; set; }

        public List<GetPostModelRecent> RecentPost { get; set; }
        public List<GetPostModelPopular> PopularPost { get; set; }
        public List<PostCategoryModel> CategoryList { get; set; }
        public List<JobCategoryModel> JobCategoryList { get; set; }

        public string metaTitle { get; set; }
        public string metaDescription { get; set; }
        public string metaImageUrl { get; set; }

        public GetjobViewListModel JobModel { get; set; }
    }

    public class GetPostListByUser
    {
        public int RecentPostPageIndex { get; set; }
        public decimal RecentPostPageCount { get; set; }

        public ViewUserDetail User { get; set; }
        public List<GetPostModelRecent> RecentPost { get; set; }
        public List<PostCategoryModel> CategoryList { get; set; }
    }

    public class GetPostModelRecent
    {
        public long PostId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }

        public string NickName { get; set; }
        public bool isStylish { get; set; }
        public Role PostByRole { get; set; }
        public string Description { get; set; }
        public double TotalLikes { get; set; }
        public bool isUserLike { get; set; }
        public double TotalComment { get; set; }
        public double PostCountShare { get; set; }
        public string FileName { get; set; }
        public int FileCount { get; set; }
        public bool Isvideo { get; set; }
        public long? CategoryId { get; set; }
        public string HasTag { get; set; }
        public IEnumerable<string> SearchHashTag { get; set; }
        public DateTime date { get; set; }
        public bool Isfollow { get; set; }
    }

    public class SearchPopularPostModel
    {
        public List<GetPostModelPopular> GetPostModelPopular { get; set; }

        public string PageSize { get; set; }

        public int PageIndex { get; set; }
    }

    public class GetPostModelPopular
    {
        public long PostId { get; set; }
        public string UserId { get; set; }
        public string UserName { get; set; }

        public string PostLinkUrl { get; set; }
        public string UserLinkUrl { get; set; }

        public string NickName { get; set; }
        public Role PostByRole { get; set; }
        public double TotalLikes { get; set; }
        public bool isUserLike { get; set; }
        public double TotalComments { get; set; }
        public double PostCountShare { get; set; }
        public string FileName { get; set; }
        public int FileCount { get; set; }
        public bool IsVideo { get; set; }
        public string[] CategoryId { get; set; }
        public string HasTag { get; set; }
        public string Description { get; set; }
        public double Distance { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
        public IEnumerable<long> Categories { get; set; }
        public IEnumerable<string> SearchHashTag { get; set; }
    }

    public class PostCommentModel
    {
        public long CommentId { get; set; }
        public long PostId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "postdetailCommentPlace")]
        public string Message { get; set; }

        public string CommentedBy { get; set; }

        public string Name { get; set; }
        public string UserName { get; set; }
        public Role UserRole { get; set; }
        public string ProfileImage { get; set; }
        public DateTime CommentedOn { get; set; }
        public string CommentedTimeSpan { get; set; }
    }

    public class ReplyCommentsModel
    {
        public long ReplyId { get; set; }
        public long CommentId { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "postdetailCommentPlace")]
        public string Message { get; set; }

        public string CommentedBy { get; set; }

        public string Name { get; set; }
        public string UserName { get; set; }
        public Role UserRole { get; set; }
        public string ProfileImage { get; set; }
        public DateTime CommentedOn { get; set; }
        public string CommentedTimeSpan { get; set; }
    }

    public class PostFilesModel
    {
        public long PostId { get; set; }
        public string FileName { get; set; }
        public bool IsVideo { get; set; }
    }

    public class getAllPostlist
    {
        public long PostId { get; set; }

        public string StylishId { get; set; }

        public string Description { get; set; }

        //public string Hashtag { get; set; }

        public string Name { get; set; }

        public string ProfileImageNAme { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Timeago { get; set; }

        public string PostPic { get; set; }

        public List<PostHashTaglistModel> HasTag { get; set; }
    }

    public class ReportedPostList
    {
        public long PostId { get; set; }

        public string StylishId { get; set; }

        public string Description { get; set; }

        public string reportedbyUserId { get; set; }

        public string reporedbyName { get; set; }

        //public string Hashtag { get; set; }

        public string Name { get; set; }

        public string Reason { get; set; }

        public string ProfileImageNAme { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Timeago { get; set; }
    }

    public class SpamCommentsListModel
    {
        public long CoomentId { get; set; }

        public long PostId { get; set; }

        public string PostById { get; set; }

        public string PostByName { get; set; }

        public string CommentedById { get; set; }

        public string CommentedByName { get; set; }

        public string Commenttext { get; set; }
    }

    public class PostHashTaglistModel
    {
        //public long Id { get; set; }
        //public long  PostId { get; set; }
        public string hashtag { get; set; }
    }

    public class PostViewDetailModel
    {
        public long PostId { get; set; }
        public long ParentId { get; set; }
        public string UserId { get; set; }
        public Role UserRole { get; set; }
        public Role CurrentUserRole { get; set; }

        public string Name { get; set; }
        public string UserName { get; set; }
        public string Description { get; set; }
        public string Hashtag { get; set; }
        public string ProfileImageName { get; set; }
        public int PostCountLikes { get; set; }
        public int PostCountComments { get; set; }
        public int PostCountShare { get; set; }
        public bool IsLike { get; set; }
        public List<PostCommentModel> Comments { get; set; }
        public List<ReplyCommentsModel> ReplyComments { get; set; }
        public List<PostFilesModel> PostFiles { get; set; }
        public List<PostHashtagsModel> PostHashtag { get; set; }
    }

    public class PostDetailModel
    {
        public long PostId { get; set; }

        public string StylishId { get; set; }

        public string Description { get; set; }

        //public string Hashtag { get; set; }

        public string Name { get; set; }

        public string NickName { get; set; }

        public string ProfileImageNAme { get; set; }

        public int PostLikes { get; set; }

        public int PostComments { get; set; }

        public DateTime CreatedDate { get; set; }

        public string Timeago { get; set; }

        public bool IsFollow { get; set; }

        public bool IsLike { get; set; }

        public List<PostHashTaglistModel> HashTags { get; set; }

        public List<PostCommentModel> Comments { get; set; }

        public List<PostFilesModel> PostFiles { get; set; }
    }

    public class FindLikeModel
    {
        public long PostId { get; set; }
        public string UserId { get; set; }
        public int PageIndex { get; set; }
    }

    public class ViewLikeModel
    {
        public long PostId { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string UserImage { get; set; }
        public bool isFollow { get; set; }
    }

    public class AddVideoModel
    {
        public long PostId { get; set; }

        public string FileName { get; set; }

        public bool IsVideo { get; set; }

        public string extension { get; set; }
    }
    public class AddVideoRequestModel
    {
        public string FileName { get; set; }

        public string path { get; set; }
    }

    public class PostFilterModel
    {
        public int PageIndex { get; set; }

        public List<long> CategoryIds;

        public string SearchText { get; set; }
    }


    public class SearchHashtagModel
    {
        public string PageSize { get; set; }
        public List<HashtagFilterModel> HashtagFilterModel { get; set; }
    }

    public class HashtagFilterModel
    {
        public string Hashtag { get; set; }
        public long PostCount { get; set; }
        public int PageIndex { get; set; }
        public string PostLinkUrl { get; set; }
        public string PostText { get; set; }
        public IEnumerable<long> Categories { get; set; }
    }

    public class PostReportModel
    {
        public long PostId { get; set; }
        public string UserId { get; set; }
        public string Reason { get; set; }
    }
}