﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class UpgradeAccountModel
    {
        public long UpgradeAccountId { get; set; }

        public int Month { get; set; }

        public decimal Amount { get; set; }

        public int Credit { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserEmail { get; set; }

        public string InvoiceNo { get; set; }
    }

    public class UpgradeCreditModel
    {
        public long UpgradeCreditId { get; set; }

        public int Credit { get; set; }

        public decimal Amount { get; set; }

        public DateTime CreatedDate { get; set; }

        public string UserEmail { get; set; }

        public string InvoiceNo { get; set; }
    }

    public class UploadFileModel
    {
        public string FileName { get; set; }
        public string path { get; set; }
        public string image { get; set; }
        public long Id { get; set; }
        public int Credit { get; set; }
    }
}
