﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class UserReviewModel
    {
        public ViewUserDetail Stylish { get; set; }
        public AddUserReviewModel StylistPopupModel { get; set; }
        public IQueryable<StylistReviewModel> Reviews { get; set; }
        public double TotalRating { get; set; }
        public double Avarage { get; set; }
        public bool isShowRateHere { get; set; }
    }

    public class StylistReviewModel
    {
        public long Id { get; set; }

        public string StylishId { get; set; }           

        public string UserId { get; set; }

        public Role StylistRole { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public Role UserRole{ get; set; }

        public string ProfileImageName { get; set; }

        public int Rating { get; set; }

        public DateTime ReviewDate { get; set; }

        public string Comment { get; set; }

        public bool IsReply { get; set; }

        public bool IsSpam { get; set; }

        public List<ReviewReplyModel> ReplyList { get; set; }
    }

    public class ReviewReplyModel
    {
        public long id { get; set; }

        public long ReviewId { get; set; }

        public string UserId { get; set; }

        public string ProfileImageName { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public string ReplyText { get; set; }
        
    }

    public class AddUserReviewModel
    {
        public long Id { get; set; }

        public string StylishId { get; set; }

        public string UserId { get; set; }

        [Required(ErrorMessage = "Please select {0}")]
        public int Rating { get; set; }

        [Required]
        public DateTime ReviewDate { get; set; }

        public string Comment { get; set; }

        public static implicit operator AddUserReviewModel(List<AddUserReviewModel> v)
        {
            throw new NotImplementedException();
        }
    }

    public class ReplyToReviewModel
    {
        public long ReviewId { get; set; }

        [Required(ErrorMessage = "Please write Reply")]
        public string ReviewText { get; set; }

    }

    public class AddToSpamModel
    {
        public long ReviewId { get; set; }

        public string UserId { get; set; }

        public string SpamReasonTxt { get; set; }
    }

    public class SpamReviewModel
    {
        public long ReviewId { get; set; }

        public string ReviewById { get; set; }

        public string ReviewBy { get; set; }

        public string ReviewText { get; set; }

        public string ReasonText { get; set; }

        public string SpamById { get; set; }

        public string SpamByName { get; set; }

        public string SpamOnId { get; set; }

        public string SpamOnName { get; set; }
    }
}
