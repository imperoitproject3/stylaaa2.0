﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    //public class UpgradeAccountModel
    //{
    //    public long Id { get; set; }
    //    [Required(ErrorMessage = "Please ener name")]
    //    [StringLength(50, ErrorMessage = "The {0} must be contain atlist {2} Character", MinimumLength = 2)]
    //    public string Name { get; set; }

    //    [Required(ErrorMessage = "Please enter detail")]
    //    public string Detail { get; set; }

    //    [Required(ErrorMessage = "Please enter price")]
    //    public double Price { get; set; }

    //    [Required(ErrorMessage = "Please enter duration")]
    //    public double Duration { get; set; }

    //    public DateTime CreatedDate { get; set; }

    //}
    public class StylistTransactionHistory
    {
        public long Id { get; set; }

        public long PackageId { get; set; }

        public string StylistId { get; set; }

        public string PackageName { get; set; }

        public string Email { get; set; }

        public decimal Amount { get; set; }

        public string StylistName { get; set; }

        public string StylistUsername { get; set; }

        public double RemainingTime { get; set; }

        public DateTime SubscriptionStart { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public int Credit { get; set; }

        public bool IsPremiume { get; set; }

        public bool IsSended { get; set; }

    }



    public class SubscribedStylistModel
    {
        public long PackageId { get; set; }

        public string StylistId { get; set; }

        public string PackageName { get; set; }

        public string Email { get; set; }

        public decimal Price { get; set; }

        public string StylistName { get; set; }

        public string StylistUsername { get; set; }

        public double RemainingTime { get; set; }

        public DateTime SubscriptionStart { get; set; }

        public double Duration { get; set; }

        public DateTime EndDate { get; set; }

        public DateTime CreatedDate { get; set; }

        public double Days { get; set; }

    }

    public class SubscribedCreditStylist
    {
        public long PackageId { get; set; }

        public string StylistId { get; set; }

        public string Email { get; set; }

        public string PackageName { get; set; }

        public decimal Price { get; set; }

        public string StylistName { get; set; }

        public string StylistUsername { get; set; }

        public int Credit { get; set; }

        public DateTime CreatedDate { get; set; }

        public int CurrentCredit { get; set; }

    }
}
