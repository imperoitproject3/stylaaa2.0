﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class LandingPageModel
    {
        public long Id { get; set; }

        [Required(ErrorMessage = "Name required")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Address required")]
        public string Address { get; set; }

        public LandingGender Gender { get; set; }

        [Required(ErrorMessage = "Category required")]
        public List<string> CategoryId { get; set; }

        public List<string> SubCategoryId { get; set; }

        [Required(ErrorMessage = "Price required")]
        public double Price { get; set; } = 0;

        public double TotalPrice { get; set; } = 0;

        public string Notice { get; set; }

        [Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailRequireMsg")]
        [EmailAddress(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "emailvalid")]
        public string Email { get; set; }

        //[Required(ErrorMessageResourceType = typeof(Resource), ErrorMessageResourceName = "LandingPhoneNumPlace")]       
        public string PhoneNumber { get; set; }

        public string Categories { get; set; }

        public string Subcategories { get; set; }

        public DateTime DateTime { get; set; }

        public string date { get; set; }

        public string time { get; set; }

        public StripeType PaymentType { get; set; }

        public bool AcceptTerms { get; set; }

        public bool Privacy { get; set; }

        public bool IsDiscount { get; set; }

    }

    public class BookingSuccessModel
    {
        public string Date { get; set; }
        public string Time { get; set; }
        public string Gender { get; set; }

    }

    public class AdminViewBookingModel
    {
        public long Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string Gender { get; set; }

        public string CategoryName { get; set; }

        public string SubCategoryName { get; set; }

        //[Required(ErrorMessage = "Price required")]
        public double Price { get; set; } = 0;

        public string Notice { get; set; }

        public double RoadAccessCost { get; set; }

        public double Total { get; set; } = 0;

        [Required(ErrorMessage = "Email is required")]
        [EmailAddress(ErrorMessage = "please enter valid email")]
        public string Email { get; set; }

        public string PhoneNumber { get; set; }

        public DateTime DateTime { get; set; }

        public string PaymentType { get; set; }

        public bool AcceptTerms { get; set; }

        public string TransactionId { get; set; }

        public List<AdminSubcategoryList> SubCategoryList { get; set; }

        public List<AdminCategoryLIst> CategoryList { get; set; }
    }

    public class LandingPageStylistList
    {
        public string Sid { get; set; }

        public string Name { get; set; }

        public string UserName { get; set; }

        public DateTime? Experiance { get; set; }

        public int Year { get; set; }

        public string ProfileImage { get; set; }
    }

    public class AdminSubcategoryList
    {
        public long Id { get; set; }

        public string SubCategoryName { get; set; }
    }

    public class AdminCategoryLIst
    {
        public long Id { get; set; }

        public string CategoryName { get; set; }
    }

    public class SubcategoryList
    {
        public double Price { get; set; }
        public string Name { get; set; }
    }
}
