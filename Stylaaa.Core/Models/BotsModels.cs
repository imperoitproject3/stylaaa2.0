﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Spatial;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Stylaaa.Core.Models
{
    public class ViewBots
    {
        public long BotId { get; set; }

        public string BotTitle { get; set; }

        public RoleForBot UserType { get; set; }

        public int MaxFollowCunt { get; set; }

        public int MaxCommentCount { get; set; }

        public List<int> Timing { get; set; }

        public List<BotFunctionEnum> Functions { get; set; }

        public bool IsActive { get; set; }
    }

    public class AddBotModel
    {
        public long BotId { get; set; }

        [Required(ErrorMessage ="Please enter title name")]
        [StringLength(50, ErrorMessage = "{0} must be minimum {2} character long!", MinimumLength = 2)]
        public string Title { get; set; }

        [Required(ErrorMessage = "Please select Usertype")]
        public RoleForBot UserType { get; set; }

        [Required(ErrorMessage = "Please enter maximum follow count")]
        public int MaxFollow { get; set; }

        [Required(ErrorMessage = "Please enter maximum comment count")]
        public int maxComment { get; set; }

        [Required(ErrorMessage = "Please select atlist one time")]
        public List<int> Timing { get; set; }
        
        public string SelectedDummyUser { get; set; }

        [Required(ErrorMessage = "Please select atlist one function")]
        public List<BotFunctionEnum> Functions { get; set; }

        [Required(ErrorMessage = "Please select atlist one dummyuser")]
        public List<string> DummyUser { get; set; }
    }

    public class DummyUserList
    {
        public string Id { get; set; }

        public string Name { get; set; }

        public string Image { get; set; }

        public bool IsChecked { get; set; }
    }

    public class AddDummyUserModel
    {
        public string Id { get; set; }
        [Required(ErrorMessage ="Please enter name")]
        [StringLength(50, ErrorMessage = "{0} must be minimum {2} character long!", MinimumLength = 2)]
        public string Name { get; set; }
        public Gender GenderId { get; set; }
        [Required(ErrorMessage = "Please enter Email")]
        [EmailAddress(ErrorMessage ="Please enter valid email")]
        public string Email { get; set; }
        public string About { get; set; }
        public string ImageName { get; set; }
        public virtual HttpPostedFileBase fileBaseImage { get; set; }
        public double LocationLatitude { get; set; }
        public double LocationLongitude { get; set; }
        [Required(ErrorMessage = "Please enter Zip code")]
        public int ZipCode { get; set; }
        public bool CheckTermsAndConditions { get; set; }
        public bool CheckPrivacyPolicy { get; set; }
        public int CountryId { get; set; }
        public Role role { get; set; }
        public string ConfirmPassword { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        
    }

    public class ManageBoatModel
    {
        public bool IsFollow { get; set; }
        public bool IsLike { get; set; }
        public bool IsComment { get; set; }
        public bool IsMessage { get; set; }
    }

    public class ViewDummyUserList
    {
        public string DummyId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string ProfileImage { get; set; }
    }
}
