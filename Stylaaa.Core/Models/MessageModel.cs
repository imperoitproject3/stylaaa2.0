﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class MessageReceiverModel
    {
        public string UserID { get; set; }
        public string ReceiverID { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverImage { get; set; }
        public bool IsRequested { get; set; }
        public int MessageCount { get; set; }
        public bool isFirstTwoUser { get; set; }
        public bool IsRequestAccepted { get; set; }
        public Role SenderRole { get; set; }
        public long? JobId { get; set; }
    }
    public class HubConnectionModel : MessageReceiverModel
    {
        public string ConnectionUrl { get; set; }
    }
    public class MessageSendModel
    {
        public string ReceiverID { get; set; }
        public string MessageText { get; set; }
        public int ConnectionIdCount { get; set; }
        public bool IsImage { get; set; } = false;
        public long? JobId { get; set; }
    }

    public class JobRequestModel
    {
        public long JobId { get; set; }
        public string JobTextMessage { get; set; }
        public string RecId { get; set; }
    }

    public class ImageSendModel
    {
        public string FileName { get; set; }
        public string path { get; set; }
    }

    public class ChatConversationGetModel : PaginationModel
    {
        public string ReceiverID { get; set; }
    }

    public class ChatConversationViewModel
    {
        public string MessageText { get; set; }
        public long ChatMessageID { get; set; }
        public string ChatUserID { get; set; }
        public string Name { get; set; }
        public bool IsImage { get; set; }
        public bool IsRead { get; set; }
        public bool IsSendByMe { get; set; }
        public bool IsDeleted { get; set; }
        public string TimeCaption { get; set; }
        public double TotalMiliSecond { get; set; }
    }

    public class SendMessagesFromAdmin
    {
        public long MessageId { get; set; }
        public string MessageText { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string ProfileImage { get; set; }
        public DateTime Date { get; set; }
    }

    public class JobMessageReceiverModel
    {
        public string UserID { get; set; }
        public string ReceiverID { get; set; }
        public long JobId { get; set; }
        public string ReceiverName { get; set; }
        public string ReceiverImage { get; set; }
        public bool IsRequested { get; set; }
        public int MessageCount { get; set; }
        public bool isFirstTwoUser { get; set; }
        public bool IsRequestAccepted { get; set; }
        public Role SenderRole { get; set; }
    }

    public class JobHubConnectionModel : JobMessageReceiverModel
    {
        public string ConnectionUrl { get; set; }
    }

}
