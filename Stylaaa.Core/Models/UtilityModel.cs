﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class UploadImageModel
    {
        public string imageUrl { get; set; }
        public string saveFilePath { get; set; }
        public string hdnImageName { get; set; }
    }

    public class SaveImageModel
    {
        public string imgBase64String { get; set; }
        public string saveFilePath { get; set; }
        public string extension { get; set; }
        public string previousImageName { get; set; }
    }

    public class SaveVideoModel
    {
        public string saveFilePath { get; set; }
        public string extension { get; set; }
        public string previousImageName { get; set; }
    }
}
