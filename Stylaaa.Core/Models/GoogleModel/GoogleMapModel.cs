﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models.GoogleModel
{
    public enum GoogelResultStatus
    {
        //indicates that no errors occurred; the address was successfully parsed and at least one geocode was returned.
        OK = 1,
        //indicates that the geocode was successful but returned no results. This may occur if the geocoder was passed a non-existent address.
        ZERO_RESULTS = 2,
        //indicates that you are over your quota.
        OVER_QUERY_LIMIT = 3,
        //indicates that your request was denied.
        REQUEST_DENIED = 4,
        //generally indicates that the query (address, components or latlng) is missing.
        INVALID_REQUEST = 5,
        // indicates that the request could not be processed due to a server error. The request may succeed if you try again.
        UNKNOWN_ERROR = 6
    }

    public enum GoogleQueryParameters
    {
        address,
        bounds,
        components,
        latlng,
        place_id
    }

    public enum GoogleResultTypes
    {
        //indicates a precise street address.
        street_address,

        //indicates a named route (such as "US 101").
        route,

        //indicates a major intersection, usually of two major roads.
        intersection,

        //indicates a political entity. Usually, this type indicates a polygon of some civil administration.
        political,

        //indicates the national political entity, and is typically the highest order type returned by the Geocoder.
        country,

        //indicates a first-order civil entity below the country level. Within the United States, these administrative levels are states. Not all nations exhibit these administrative levels. In most cases, administrative_area_level_1 short names will closely match ISO 3166-2 subdivisions and other widely circulated lists; however this is not guaranteed as our geocoding results are based on a variety of signals and location data.
        administrative_area_level_1,

        //indicates a second-order civil entity below the country level. Within the United States, these administrative levels are counties. Not all nations exhibit these administrative levels.
        administrative_area_level_2,

        //indicates a third-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        administrative_area_level_3,

        //indicates a fourth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        administrative_area_level_4,

        //indicates a fifth-order civil entity below the country level. This type indicates a minor civil division. Not all nations exhibit these administrative levels.
        administrative_area_level_5,

        //indicates a commonly-used alternative name for the entity.
        colloquial_area,

        //indicates an incorporated city or town political entity.
        locality,

        //indicates a specific type of Japanese locality, to facilitate distinction between multiple locality components within a Japanese address.
        ward,

        //indicates a first-order civil entity below a locality. For some locations may receive one of the additional types: sublocality_level_1 to sublocality_level_5. Each sublocality level is a civil entity. Larger numbers indicate a smaller geographic area.
        sublocality,

        //indicates a named neighborhood
        neighborhood,

        //indicates a named location, usually a building or collection of buildings with a common name
        premise,

        //indicates a first-order entity below a named location, usually a singular building within a collection of buildings with a common name
        subpremise,

        //indicates a postal code as used to address postal mail within the country.
        postal_code,

        //indicates a prominent natural feature.
        natural_feature,

        //indicates an airport.
        airport,

        //indicates a named park.
        park,

        //indicates a named point of interest. Typically, these "POI"s are prominent local entities that don't easily fit in another category, such as "Empire State Building" or "Statue of Liberty."
        point_of_interest,
    }
    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    // var data = GoogleMapModel.FromJson(jsonString);
    public partial class GoogleMapModel
    {
        [JsonProperty("results")]
        public List<Result> Results { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }
    }

    public partial class Result
    {
        [JsonProperty("address_components")]
        public List<AddressComponent> AddressComponents { get; set; }

        [JsonProperty("formatted_address")]
        public string FormattedAddress { get; set; }

        [JsonProperty("geometry")]
        public Geometry Geometry { get; set; }

        [JsonProperty("place_id")]
        public string PlaceId { get; set; }

        [JsonProperty("types")]
        public List<string> Types { get; set; }
    }

    public partial class Geometry
    {
        [JsonProperty("bounds")]
        public Bounds Bounds { get; set; }

        [JsonProperty("location")]
        public Location Location { get; set; }

        [JsonProperty("location_type")]
        public string LocationType { get; set; }

        [JsonProperty("viewport")]
        public Bounds Viewport { get; set; }
    }

    public partial class Bounds
    {
        [JsonProperty("northeast")]
        public Location Northeast { get; set; }

        [JsonProperty("southwest")]
        public Location Southwest { get; set; }
    }

    public partial class Location
    {
        [JsonProperty("lat")]
        public double Lat { get; set; }

        [JsonProperty("lng")]
        public double Lng { get; set; }
    }

    public partial class AddressComponent
    {
        [JsonProperty("long_name")]
        public string LongName { get; set; }

        [JsonProperty("short_name")]
        public string ShortName { get; set; }

        [JsonProperty("types")]
        public List<string> Types { get; set; }
    }

    public partial class GoogleMapModel
    {
        public static GoogleMapModel FromJson(string json) => JsonConvert.DeserializeObject<GoogleMapModel>(json, Converter.Settings);
    }

    public static class GoogleMapSerialize
    {
        public static string ToJson(this GoogleMapModel self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    // To parse this JSON data, add NuGet 'Newtonsoft.Json' then do:
    // var data = GoogleTimezoneModel.FromJson(jsonString);
    public partial class GoogleTimezoneModel
    {
        [JsonProperty("dstOffset")]
        public long DstOffset { get; set; }

        [JsonProperty("rawOffset")]
        public long RawOffset { get; set; }

        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("timeZoneId")]
        public string TimeZoneId { get; set; }

        [JsonProperty("timeZoneName")]
        public string TimeZoneName { get; set; }
    }

    public partial class GoogleTimezoneModel
    {
        public static GoogleTimezoneModel FromJson(string json) => JsonConvert.DeserializeObject<GoogleTimezoneModel>(json, Converter.Settings);
    }

    public static class GoogleTimezoneSerialize
    {
        public static string ToJson(this GoogleTimezoneModel self) => JsonConvert.SerializeObject(self, Converter.Settings);
    }

    public class Converter
    {
        public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
        {
            MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
            DateParseHandling = DateParseHandling.None,
        };
    }
}
