﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Core.Models
{
    public class MediaResponseModel<T> where T : new()
    {
        public MediaResponseModel()
        {
            Result = new T();
            Status = ResponseStatus.Failed;
            Message = string.Empty;
            MediaPath = string.Empty;
            FileName = string.Empty;
        }

        public ResponseStatus Status { get; set; }
        public string Message { get; set; }
        public T Result { get; set; }
        public string MediaPath { get; set; }
        public string FileName { get; set; }
    }
}
