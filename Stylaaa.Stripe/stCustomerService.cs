﻿using Stripe;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Stripe
{
    public class stCustomerService
    {
        private readonly StripeCustomerService _service;

        public stCustomerService()
        {
            _service = new StripeCustomerService();
        }

        public async Task<ResponseModel<StripeCustomerResultModel>> Add(StripeCustomerRequestModel model)
        {
            ResponseModel<StripeCustomerResultModel> mResult = new ResponseModel<StripeCustomerResultModel>();
            StripeCustomerCreateOptions stripeRequest = new StripeCustomerCreateOptions()
            {
                Description = model.name,
                Email = model.email,
            };

            try
            {
                StripeCustomer stripeResult = await _service.CreateAsync(stripeRequest);
                mResult.Status = ResponseStatus.Success;
                mResult.Result = new StripeCustomerResultModel { customerId = stripeResult.Id };
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
            }
            return mResult;
        }
        public async Task<ResponseModel<object>> Delete(string customerId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                StripeDeleted stripeResult = await _service.DeleteAsync(customerId);
                if (stripeResult.Deleted)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Result = new StripeCustomerResultModel { customerId = stripeResult.Id };
                }
                else
                {
                    mResult.Message = "Stripe customer was not deleted.";
                }
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
            }
            return mResult;
        }
    }
}