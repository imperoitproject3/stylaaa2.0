﻿using Stripe;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Stylaaa.Stripe
{
    public class stConnectAccountService
    {
        private readonly StripeAccountService _service;

        public stConnectAccountService()
        {
            _service = new StripeAccountService();
        }

        public async Task<ResponseModel<StripeConnectAccountResultModel>> Add(StripeConnectAccountRequestModel model)
        {
            ResponseModel<StripeConnectAccountResultModel> mResult = new ResponseModel<StripeConnectAccountResultModel>();

            StripeOAuthTokenCreateOptions stripeOAuthRequest = new StripeOAuthTokenCreateOptions()
            {
                ClientSecret = GlobalConfig.StripeApiKey,
                Code = model.stripeOAuthCode,
                GrantType = "authorization_code",
            };

            try
            {
                StripeOAuthTokenService stripeOAuthTokenService = new StripeOAuthTokenService();
                StripeOAuthToken stripeOAuthTokenResult = await stripeOAuthTokenService.CreateAsync(stripeOAuthRequest);

                mResult.Result.stripeConnectAccountId = stripeOAuthTokenResult.StripeUserId;

                //TODO : Uncomment when live key
                //if (!HttpContext.Current.Request.IsLocal)
                //{
                //    StripeFileUpload fileUploadResult = await UploadIdentityProof(model.identityProofName);

                //    StripeAccountUpdateOptions stripeUpdateRequest = new StripeAccountUpdateOptions()
                //    {
                //        TosAcceptanceDate = DateTime.UtcNow,
                //        TosAcceptanceIp = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"].ToString(),
                //        LegalEntity = new StripeAccountLegalEntityOptions
                //        {
                //            VerificationDocumentFileId = fileUploadResult.Id
                //        }
                //    };
                //    StripeAccount stripeResult = await _service.UpdateAsync(stripeOAuthTokenResult.StripeUserId, stripeUpdateRequest);
                //}

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "OK";
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> Edit(StripeConnectAccountUpdateModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (!HttpContext.Current.Request.IsLocal)
                {
                    StripeFileUpload fileUploadResult = await UploadIdentityProof(model.identityProofName);

                    StripeAccountUpdateOptions stripeUpdateRequest = new StripeAccountUpdateOptions()
                    {
                        LegalEntity = new StripeAccountLegalEntityOptions
                        {
                            VerificationDocumentFileId = fileUploadResult.Id
                        }
                    };
                    StripeAccount stripeResult = await _service.UpdateAsync(model.stripeConnectAccountId, stripeUpdateRequest);
                }

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "OK";
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
            }
            return mResult;
        }

        private async Task<StripeFileUpload> UploadIdentityProof(string identityProofName)
        {
            string uploadFilePath = GlobalConfig.StripeIdentityFilePath + identityProofName;
            using (FileStream stream = File.Open(uploadFilePath, FileMode.Open))
            {
                StripeFileUploadService fileService = new StripeFileUploadService();
                StripeFileUpload fileUploadResult = await fileService.CreateAsync(uploadFilePath, stream, StripeFilePurpose.IdentityDocument);
                return fileUploadResult;
            }
        }
    }
}
