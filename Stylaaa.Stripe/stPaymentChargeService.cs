﻿using Newtonsoft.Json;
using Stripe;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Stripe
{
    public class stPaymentChargeService
    {
        private readonly StripeChargeService _service;

        public stPaymentChargeService()
        {
            _service = new StripeChargeService();
        }

        public async Task<ResponseModel<StripePaymentChargeResultModel>> AuthorizePaymentCharge(StripePaymentChargeRequestModel model)
        {
            ResponseModel<StripePaymentChargeResultModel> mResult = new ResponseModel<StripePaymentChargeResultModel>();
            stCreditCardService cardService = new stCreditCardService();
            ResponseModel<StripeCardResultModel> stripeCardResult = await cardService.Add(new StripeCardRequestModel
            {
                stripeCardToken = model.stripePaymentToken,
                stripeCustomerId = model.stripeCustomerId
            });

            if (stripeCardResult.Status == ResponseStatus.Success)
            {
                double adminChargeAmount = (model.chargeAmount * GlobalConfig.StripeAdminFeedInPer) / 100;
                int amountInCents = Utility.ConvertEuroToCents(model.chargeAmount);
                int adminFeesInCents = Utility.ConvertEuroToCents(adminChargeAmount);

                StripeChargeCreateOptions stripeRequest = new StripeChargeCreateOptions()
                {
                    Amount = amountInCents,
                    Currency = GlobalConfig.StripeCurrency,
                    SourceTokenOrExistingSourceId = stripeCardResult.Result.stripeCardId,
                    //ApplicationFee = adminFeesInCents,
                    //Destination = model.stripeConnectAccountId,
                    CustomerId = model.stripeCustomerId,
                    Capture = false
                };

                try
                {
                    StripeCharge stripeResult = await _service.CreateAsync(stripeRequest);
                    if (stripeResult.Status == "succeeded")
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "OK";
                        mResult.Result.stripePaymentChargeToken = stripeResult.Id;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(stripeResult.FailureMessage))
                            mResult.Message = stripeResult.FailureMessage;
                        else
                            mResult.Message = "Stripe payment process failed";
                    }
                }
                catch (Exception e)
                {
                    mResult.Message = e.Message;
                }
            }
            else
            {
                mResult.Message = stripeCardResult.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<StripePaymentChargeResultModel>> CapturePaymentCharge(StripePaymentChargeRequestModel model)
        {
            ResponseModel<StripePaymentChargeResultModel> mResult = new ResponseModel<StripePaymentChargeResultModel>();

            int amountInCents = Utility.ConvertEuroToCents(model.chargeAmount);
            StripeChargeCaptureOptions stripeChargeCaptureOption = new StripeChargeCaptureOptions()
            {
                Amount = amountInCents,
            };

            StripeChargeCreateOptions createOption = new StripeChargeCreateOptions()
            {
                Amount = amountInCents,
                //  CustomerId = model.stripeCustomerId,
                SourceTokenOrExistingSourceId = model.stripePaymentToken,
                Capture = true,
                Currency = GlobalConfig.StripeCurrency,
                ReceiptEmail=model.receiptEmail
            };

            try
            {
                // var stripeResult = await _service.CaptureAsync(model.stripePaymentToken, stripeChargeCaptureOption);
                var stripeResult = await _service.CreateAsync(createOption);
                if (stripeResult.Status == "succeeded")
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "OK";
                    mResult.Result.stripePaymentChargeToken = stripeResult.Id;
                }
                else
                {
                    if (!string.IsNullOrEmpty(stripeResult.FailureMessage))
                        mResult.Message = stripeResult.FailureMessage;
                    else
                        mResult.Message = "Stripe payment process failed";
                }
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
            }

            return mResult;
        }

        public async Task<ResponseModel<StripeSource>> CreatePaymentSource(StripePaymentSourceRequestModel model)
        {
            ResponseModel<StripeSource> mResult = new ResponseModel<StripeSource>();

            try
            {
                int amountInCents = Utility.ConvertEuroToCents(model.chargeAmount);
                //var RedirectReturnUrl = string.Format(GlobalConfig.baseUrl + "Stripe/PaymentSuccess?amount={0}&invoiceno={1}&stripetype={2}", amountInCents, model.invoiceNo, model.stripeType);
                var RedirectReturnUrl = string.Format(GlobalConfig.baseUrl + "Stripe/PaymentSuccess?amount={0}&stripetype={1}", amountInCents, model.stripeType);                
                var sourceOptions = new StripeSourceCreateOptions
                {
                    Type = model.stripeType.ToString(),
                    Currency = GlobalConfig.StripeCurrency,
                    Owner = new StripeSourceOwner
                    {
                        Email = model.CustomerEmail,
                        Name = model.CustomerName,
                    },
                    RedirectReturnUrl = RedirectReturnUrl,
                    Amount = amountInCents,
                    SofortCountry = "AT",
                };
                var sourceService = new StripeSourceService();
                var source = await sourceService.CreateAsync(sourceOptions);
                mResult.Status = ResponseStatus.Success;
                mResult.Result = source;
                mResult.Message = "Source created successfully!";
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<StripeSource>> CreatePaymentSourceForCredit(StripePaymentSourceRequestModel model)
        {
            ResponseModel<StripeSource> mResult = new ResponseModel<StripeSource>();

            try
            {
                int amountInCents = Utility.ConvertEuroToCents(model.chargeAmount);
                var RedirectReturnUrl = string.Format(GlobalConfig.baseUrl + "Stripe/CreditPaymentSuccess?amount={0}&stripetype={1}", amountInCents, model.stripeType);
                var sourceOptions = new StripeSourceCreateOptions
                {
                    Type = model.stripeType.ToString(),
                    Currency = GlobalConfig.StripeCurrency,
                    Owner = new StripeSourceOwner
                    {
                        Email = model.CustomerEmail,
                        Name = model.CustomerName,
                    },
                    RedirectReturnUrl = RedirectReturnUrl,
                    Amount = amountInCents,
                    SofortCountry = "AT",
                };
                var sourceService = new StripeSourceService();
                var source = await sourceService.CreateAsync(sourceOptions);
                mResult.Status = ResponseStatus.Success;
                mResult.Result = source;
                mResult.Message = "Source created successfully!";
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<StripeSource>> CreatePaymentSourceForBooking(StripePaymentSourceRequestModel model)
        {
            ResponseModel<StripeSource> mResult = new ResponseModel<StripeSource>();

            try
            {
                int amountInCents = Utility.ConvertEuroToCents(model.chargeAmount);
                //var RedirectReturnUrl = string.Format(GlobalConfig.baseUrl + "Stripe/PaymentSuccess?amount={0}&invoiceno={1}&stripetype={2}", amountInCents, model.invoiceNo, model.stripeType);
                var RedirectReturnUrl = string.Format(GlobalConfig.baseUrl + "Account/BookingPaymentSuccess?amount={0}&stripetype={1}", amountInCents, model.stripeType);
                var sourceOptions = new StripeSourceCreateOptions
                {
                    Type = model.stripeType.ToString(),
                    Currency = GlobalConfig.StripeCurrency,
                    Owner = new StripeSourceOwner
                    {
                        Email = model.CustomerEmail,
                        Name = model.CustomerName,
                    },
                    RedirectReturnUrl = RedirectReturnUrl,
                    Amount = amountInCents,
                    SofortCountry = "AT",
                };
                var sourceService = new StripeSourceService();
                var source = await sourceService.CreateAsync(sourceOptions);
                //var JsonResult = JsonConvert.SerializeObject(source);
                //var responceStatus = Utility.getChargeStaus(source.Status);
                mResult.Status = ResponseStatus.Success;
                mResult.Result = source;
                mResult.Message = "Source created successfully!";
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<StripeCharge>> CreatePaymentCharge(StripePaymentChargeResultModel model)
        {
            ResponseModel<StripeCharge> mResult = new ResponseModel<StripeCharge>();
            try
            {
                var chargeOptions = new StripeChargeCreateOptions()
                {
                    Amount = model.chargeAmount,
                    Currency = GlobalConfig.StripeCurrency,
                    Description = "Charge for upgrade monthly package",
                    SourceTokenOrExistingSourceId = model.stripePaymentChargeToken  // obtained with Stripe.js,
                };
                var chargeService = new StripeChargeService();
                StripeCharge charge = await chargeService.CreateAsync(chargeOptions);
                mResult.Result = charge;
                mResult.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<StripeCharge>> CreatePaymentChargeForCredit(StripePaymentChargeResultModel model)
        {
            ResponseModel<StripeCharge> mResult = new ResponseModel<StripeCharge>();
            try
            {
                var chargeOptions = new StripeChargeCreateOptions()
                {
                    Amount = model.chargeAmount,
                    Currency = GlobalConfig.StripeCurrency,
                    Description = "Charge for credit package",
                    SourceTokenOrExistingSourceId = model.stripePaymentChargeToken  // obtained with Stripe.js,
                };
                var chargeService = new StripeChargeService();
                StripeCharge charge = await chargeService.CreateAsync(chargeOptions);
                mResult.Result = charge;
                mResult.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<StripeCharge>> CreatePaymentChargeForBooking(StripePaymentChargeResultModel model)
        {
            ResponseModel<StripeCharge> mResult = new ResponseModel<StripeCharge>();
            try
            {
                var chargeOptions = new StripeChargeCreateOptions()
                {
                    Amount = model.chargeAmount,
                    Currency = GlobalConfig.StripeCurrency,
                    Description = "Charge for Booked Stylist",
                    SourceTokenOrExistingSourceId = model.stripePaymentChargeToken  // obtained with Stripe.js,
                };
                var chargeService = new StripeChargeService();
                StripeCharge charge = await chargeService.CreateAsync(chargeOptions);
                mResult.Result = charge;
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Booking Confirm And Payment charge created";
            }
            catch (Exception ex)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = ex.Message;
            }
            return mResult;
        }
    }
}
