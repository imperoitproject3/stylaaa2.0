﻿using Stripe;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Stripe
{
    public class stCreditCardService
    {
        private readonly StripeCardService _service;

        public stCreditCardService()
        {
            _service = new StripeCardService();
        }
        public async Task<ResponseModel<StripeCardResultModel>> Add(StripeCardRequestModel model)
        {
            ResponseModel<StripeCardResultModel> mResult = new ResponseModel<StripeCardResultModel>();
            StripeCardCreateOptions stripeRequest = new StripeCardCreateOptions()
            {
                SourceToken = model.stripeCardToken
            };
            try
            {
                StripeCard stripeResult = await _service.CreateAsync(model.stripeCustomerId, stripeRequest);
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Card " + stripeResult.Last4 + " added successfully.";
                mResult.Result.stripeCardId = stripeResult.Id;
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
            }
            return mResult;
        }

    }
}
