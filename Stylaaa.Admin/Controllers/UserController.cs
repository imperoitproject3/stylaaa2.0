﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class UserController : BaseController
    {
        private CoreUserServices _Users;
        private StylishServices _stylish;
        private UserService _service;


        public UserController()
        {
            _Users = new CoreUserServices();
            _stylish = new StylishServices();
            _service = new UserService();
        }
        // GET: User
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllUsers()
        { 
            IQueryable<UserDetailModel> objUsers = _Users.getAllUsers_IQueryable();
            ViewBag.active = "Users";
            return View(objUsers);
        }

        public ActionResult AllStylish()
        {
            IQueryable<UserDetailModel> objStylish = _stylish.GetAllStylishDetail();
            ViewBag.active = "Stylist";
            return View("AllUsers", objStylish);
        }

        [HttpPost]
        public async Task<JsonResult> ActiveToggle(string Id)
        {
            ResponseModel<bool> mResult = await _service.ActiveToggle(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Detail(string id)
        {
            Role role = _service.GetRoleById(id);
            if (role == Role.User)
            {
                ViewUserDetail objUser = await _Users.GetUserDetail(id);
                if (objUser == null)
                    return RedirectToAction("AllUsers");
                else
                    return View("UserDetail", objUser);
            }
            else
            {
                ViewStylishDetailsModel objStylish = await _stylish.GetStylishDetail(id);
                if (objStylish == null)
                    return RedirectToAction("AllStylish");
                else
                    return View(objStylish);
            }
        }

        public async Task<JsonResult> DeleteUser(string id)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            mResult = await _service.DeleteUser(id);
            if (mResult.Status == ResponseStatus.Success)
                return Json(mResult, JsonRequestBehavior.AllowGet);
            //if (mResult.Result)
            //    return RedirectToAction("AllStylish");
            //else
            //    return RedirectToAction("AllUsers");
            else
                return Json(mResult, JsonRequestBehavior.AllowGet);
            //return RedirectToAction("AllUsers");

        }
        
        public async Task<ActionResult> RemoveAll(string SelectedUserId)
        {
            if (SelectedUserId != null)
            {
                List<string> UsersId = SelectedUserId != null ? SelectedUserId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
                foreach (var item in UsersId)
                {
                    ResponseModel<bool> mResult = await _service.DeleteUser(item);
                }
                TempData["alertModel"] = new AlertModel(AlertStatus.success, string.Format("{0} Users delete successfully", UsersId.Count()));
            }
            else
                TempData["alertModel"] = new AlertModel(AlertStatus.success, "please select User");
            return RedirectToAction("AllStylish");
        }
    }
}