﻿using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class SchedularController : Controller
    {
        private BotsService _service;
        // GET: Schedular

        public SchedularController()
        {
            _service = new BotsService();
        }

        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> ScheduleFollower()
        {
            if (_service.checkFollowSchedular())
            {
                ResponseModel<bool> mResult = new ResponseModel<bool>();
                var isTrue = await _service.SceduleFollowUser();
                mResult.Result = true;
            }
            return View();
        }

        [AllowAnonymous]
        public async Task<ActionResult> LikeSchedular()
        {
            if (_service.checkLikeSchedular())
            {
                ResponseModel<bool> mResult = new ResponseModel<bool>();
                var isTrue = await _service.LikeSchedular();
                mResult.Result = true;
            }
            return View("ScheduleFollower");
        }

        [AllowAnonymous]
        public async Task<ActionResult> CommentSchedular()
        {
            if (_service.checkCommentSchedular())
            {
                ResponseModel<bool> mResult = new ResponseModel<bool>();
                var isTrue = await _service.CommentSchedular();
                mResult.Result = true;
            }
            return View("ScheduleFollower");
        }

        [AllowAnonymous]
        public async Task<ActionResult> MessageSchedule()
        {
            if (_service.checkMessageSchedular())
            {
                ResponseModel<bool> mResult = new ResponseModel<bool>();
                var isTrue = await _service.MessageSchedule();
                mResult.Result = true;
            }
            return View("ScheduleFollower");
        }
    }
}