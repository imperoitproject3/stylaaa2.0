﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class PostController : BaseController
    {
        private PostServices _service;

        public PostController()
        {
            _service = new PostServices();
        }

        // GET: Post
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllPost()
        {
            IQueryable<getAllPostlist> GetAllPost = _service.GetPostListForAdmin();
            return View(GetAllPost);
        }

        public async Task<JsonResult> DeletePostByAdmin(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            mResult = await _service.DeletePost(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            if (mResult.Status == ResponseStatus.Success)
                return Json(mResult, JsonRequestBehavior.AllowGet);
            //if (mResult.Result)
            //    return RedirectToAction("AllStylish");
            //else
            //    return RedirectToAction("AllUsers");
            else
                return Json(mResult, JsonRequestBehavior.AllowGet);
           
        }

        public async Task<ActionResult> Postdetail(long Id)
        {
            PostDetailModel model = await _service.PostDetailById(Id);
            return View(model);
        }

        public ActionResult ReportedPost()
        {
            IQueryable<ReportedPostList> reportedPost = _service.GetReportedPostlist();
            return View(reportedPost);
        }

        public async Task<ActionResult> DeleteRepoetedPost(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            mResult = await _service.DeletePost(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("ReportedPost");
        }

        public ActionResult SpamComments()
        {
            IQueryable<SpamCommentsListModel> model = _service.GetSpamComments();
            return View(model);               
        }

        public async Task<ActionResult> DeleteSpamComments(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.DeleteSpamComment(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("SpamComments");
        }

    }
}