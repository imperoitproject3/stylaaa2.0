﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PagedList;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace Stylaaa.Admin.Controllers
{
    public class PremiumController : BaseController
    {
        private PremiumMasterServices _service;
        public PremiumController()
        {
            _service = new PremiumMasterServices();
        }

        public ActionResult SoldPackages(int? page)
        {
            int pageIndex = 1;
            pageIndex = page.HasValue ? Convert.ToInt32(page) : 1;
            List<StylistTransactionHistory> model = _service.GetTransactionHistory();
            int PageSize = 10;
            IPagedList<StylistTransactionHistory> list = model.ToPagedList(pageIndex, PageSize);
            return View(list);
        }

        public ActionResult Index()
        {
            IQueryable<UpgradeAccountModel> model = _service.getPremiumpackages();
            return View(model);
        }

        public ActionResult Credit()
        {
            IQueryable<UpgradeCreditModel> model = _service.getCreditPackages();
            return View(model);
        }

        public ActionResult SubscribedStylist()
        {
            List<SubscribedStylistModel> model = _service.GetSubscribedStylist();
            return View(model);
        }

        public ActionResult SubscribedCreditStylist()
        {
            List<SubscribedCreditStylist> model = _service.GetSubscribedCreditStylist();
            return View(model);
        }

        public ActionResult AddPremiumPackage()
        {
            ViewBag.Action = "AddPremiumPackage";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddPremiumPackage(UpgradeAccountModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.AddPremiumPack(model);
            if (mResult.Status == ResponseStatus.Success)
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.success, mResult.Message);
                return RedirectToAction("AddPremiumPackage");
            }
            else
            {

                TempData["alertModel"] = new AlertModel(AlertStatus.danger, mResult.Message);
                ModelState.AddModelError("", mResult.Message);
            }
            ViewBag.Action = "AddPremiumPackage";
            return RedirectToAction("AddPremiumPackage");
        }

        public ActionResult AddCreditPackage()
        {
            ViewBag.Action = "AddCreditPackage";
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddCreditPackage(UpgradeCreditModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.InsertOrUpdateCredit(model);
            if (mResult.Status == ResponseStatus.Success)
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.success, mResult.Message);
                return RedirectToAction("AddCreditPackage");
            }
            else
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.danger, mResult.Message);
                ModelState.AddModelError("", mResult.Message);
            }
            ViewBag.Action = "AddCreditPackage";
            return RedirectToAction("AddCreditPackage");
        }

        public async Task<ActionResult> Edit(long Id)
        {
            UpgradeAccountModel objCategory = await _service.EditPackageById(Id);
            if (objCategory == null)
                return RedirectToActionPermanent("Index");
            ViewBag.Action = "Edit";
            return View("AddPremiumPackage", objCategory);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UpgradeAccountModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                mResult = await _service.AddPremiumPack(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    TempData["alertModel"] = new AlertModel(AlertStatus.success, "Package Edited successfully !");
                    return RedirectToAction("Index");
                }
                else
                {
                    string Msg = (!String.IsNullOrWhiteSpace(mResult.Message) ? mResult.Message : Resource.FailedToEditPackage);
                    TempData["alertModel"] = new AlertModel(AlertStatus.danger, Msg);
                    ModelState.AddModelError("", Msg);
                }
            }
            ViewBag.Action = "Edit";
            return View("AddPremiumPackage", model);
        }

        public async Task<ActionResult> EditCredit(long Id)
        {
            UpgradeCreditModel objCategory = await _service.EditCreditById(Id);
            if (objCategory == null)
                return RedirectToActionPermanent("Index");
            ViewBag.Action = "EditCredit";
            return View("AddCreditPackage", objCategory);
        }

        [HttpPost]
        public async Task<ActionResult> EditCredit(UpgradeCreditModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (ModelState.IsValid)
            {
                mResult = await _service.InsertOrUpdateCredit(model);
                if (mResult.Status == ResponseStatus.Success)
                {
                    TempData["alertModel"] = new AlertModel(AlertStatus.success, "Credit package Edited successfully !");
                    return RedirectToAction("Credit", "Premium");
                }
                else
                {
                    string Msg = (!String.IsNullOrWhiteSpace(mResult.Message) ? mResult.Message : Resource.FailedToEditPackage);
                    TempData["alertModel"] = new AlertModel(AlertStatus.danger, Msg);
                    ModelState.AddModelError("", Msg);
                }
            }
            ViewBag.Action = "EditCredit";
            return View("AddCreditPackage", model);
        }

        public async Task<ActionResult> Delete(long Id)
        {
            ResponseModel<object> mResult = await _service.DeletePackageById(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Credit", "Premium");
        }

        public async Task<ActionResult> CreditDelete(long Id)
        {
            ResponseModel<object> mResult = await _service.DeleteCreditById(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Credit", "Premium");
        }


        public async Task<ActionResult> DeleteTransaction(long Id, int Credit)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.deleteTransactionRecord(Id, Credit);
            return RedirectToAction("SoldPackages");
        }

        public async Task<ActionResult> MarkAsSent(long Id, int Credit)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.MarkAsSend(Id, Credit);
            return RedirectToAction("SoldPackages");
        }

        public async Task<JsonResult> AddImage(UploadFileModel model)
        {
            #region Upload video
            string shortGuid = ShortGuid.NewGuid().ToString();
            if (Request.Files.Count > 0)
            {
                HttpPostedFileBase file = Request.Files[0];
                //get filename
                string ogFileName = file.FileName.Trim('\"');
                var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                file.SaveAs(Path.Combine(GlobalConfig.XeroReceiptFilePath, thisFileName));
                model.FileName = thisFileName;
            }
            #endregion

            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.UploadReceiptFile(model.Id, model.Credit, model.FileName);
            //model.path = GlobalConfig.XeroReceiptFilePath + model.FileName;
            return Json(model, JsonRequestBehavior.AllowGet);
        }
    }
}