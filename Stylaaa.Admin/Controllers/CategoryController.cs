﻿using Microsoft.AspNet.Identity;
using Stylaaa.Admin.Filter;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    [CustomAuthorization]
    public class CategoryController : BaseController
    {
        private CategoryService _catService;
        private UserService _user;

        ResponseModel<object> mResult;

        public CategoryController()
        {
            _catService = new CategoryService();
            _user = new UserService();
        }
        public ActionResult Index()
        {
            string UserId = User.Identity.GetUserId();
            IQueryable<CategoryModel> Categorieslist = _catService.IQueryable_CategoryList(_user.GetLanguage(UserId));
            return View(Categorieslist);
        }

        public async Task<ActionResult> Add()
        {
            AdminCategoryModel model = new AdminCategoryModel();
            List<CategoryLanguageModel> CategoryLanguages = new List<CategoryLanguageModel>();
            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                LanguageId = languageType.English,
                LanguageName = languageType.English.ToString(),
            });

            CategoryLanguages.Add(new CategoryLanguageModel()
            {
                LanguageId = languageType.German,
                LanguageName = languageType.German.ToString(),
            });

            model.CategoryLanguageModelList = CategoryLanguages;
            return View("ManageCategory", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AdminCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _catService.isCategoryDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    if (model.fileBaseImage != null)
                    {
                        //Stream inStream = model.fileBaseImage.InputStream;
                        //Image PostedImage = Image.FromStream(inStream);

                        string ogFileName = model.fileBaseImage.FileName.Trim('\"');
                        string shortGuid = ShortGuid.NewGuid().ToString();
                        var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                        model.fileBaseImage.SaveAs(Path.Combine(GlobalConfig.CategoryImagePath, thisFileName));
                        model.ImageName = thisFileName;
                    }
                    mResult = await _catService.InsertOrUpdateCategory(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("ManageCategory", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            AdminCategoryModel model = await _catService.GetCategoryDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("ManageCategory", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(AdminCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _catService.isCategoryDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    if (model.fileBaseImage != null)
                    {
                        //Stream inStream = model.fileBaseImage.InputStream;
                        //Image PostedImage = Image.FromStream(inStream);

                        string ogFileName = model.fileBaseImage.FileName.Trim('\"');
                        string shortGuid = ShortGuid.NewGuid().ToString();
                        var thisFileName = shortGuid + Path.GetExtension(ogFileName);

                        model.fileBaseImage.SaveAs(Path.Combine(GlobalConfig.CategoryImagePath, thisFileName));
                        //}
                        model.ImageName = thisFileName;
                    }

                    mResult = await _catService.InsertOrUpdateCategory(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("ManageCategory", model);
        }
        

        public async Task<ActionResult> DeleteCategory(long Id)
        {
            mResult = await _catService.DeleteCategory(Id);
            OnBindMessage(mResult.Status, mResult.Message);
            return RedirectToActionPermanent("Index");
        }
    }
}