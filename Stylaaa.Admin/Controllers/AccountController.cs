﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class AccountController : Controller
    {
        private UserService _service;
        private StylishServices _stylish;
        private BotsService _bot;

        public AccountController()
        {
            _stylish = new StylishServices();
            _service = new UserService();
            _bot = new BotsService();
        }

        [AllowAnonymous]
        public ActionResult Login()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View();
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> Login(AdminLoginModel model)
        {
            _service = new UserService(Request.GetOwinContext());
            if (ModelState.IsValid)
            {
                ResponseModel<object> mResult = await _service.AdminLogin(model);

                if (mResult.Status == ResponseStatus.Success)
                {
                    TempData["Otp"] = mResult.Result.ToString();
                    TempData["alertModel"] = new AlertModel(AlertStatus.success, mResult.Message);
                    return RedirectToAction("ConfirmOTP");
                }
                ModelState.AddModelError("", mResult.Message);
            }
            return View(model);
        }


        public ActionResult ConfirmOTP()
        {
            //var UserIdentity = (ClaimsIdentity)User.Identity;
            //var claims = UserIdentity.Claims;
            //ViewBag.role = claims.Where(c => c.Type == "email").FirstOrDefault().Value;
            //string password = claims.Where(c => c.Type == "password").FirstOrDefault().Value;
            string OtpNumber = TempData["Otp"].ToString();
            TempData["OtpNumber"] = OtpNumber;
            //var otp2 = Session["otpnumber2"].ToString();
            return View();
        }

        [HttpPost]
        public ActionResult ConfirmOTP(AdminConfirmOtpModel model)
        {
            //var otp1 = TempData["OtpNumber"].ToString();
            if (model.ConfirmOtp != null)
            {
                return RedirectToAction("Index", "Home");
                //if (otp1 == model.ConfirmOtp)
                //{

                //}
            }
            else
            {
                return View();
            }

        }

        public ActionResult ChangePassword()
        {
            Role MyRole = Role.Admin;
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            string claimRole = userIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            MyRole = (Role)Enum.Parse(typeof(Role), claimRole, true);
            ViewBag.MyRole = MyRole;
            ViewBag.FullName = claims.Where(c => c.Type == "FullName").FirstOrDefault().Value;

            return View();
        }

        [HttpPost]
        public async Task<ActionResult> ChangePassword(AdminChangePasswordModel model)
        {
            if (ModelState.IsValid)
            {
                _service = new UserService(Request.GetOwinContext());
                ResponseModel<object> mResult = await _service.AdminChangePassword(model);

                TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

                if (mResult.Status == ResponseStatus.Success)
                    return RedirectToActionPermanent("Login", "Account");
                ModelState.AddModelError("", mResult.Message);
            }
            Role MyRole = Role.Admin;
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            string claimRole = userIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            MyRole = (Role)Enum.Parse(typeof(Role), claimRole, true);
            ViewBag.MyRole = MyRole;
            ViewBag.FullName = claims.Where(c => c.Type == "FullName").FirstOrDefault().Value;
            return View(model);
        }

        [HttpGet]
        public ActionResult Logout()
        {
            _service = new UserService(Request.GetOwinContext());
            _service.WebLogout();
            return RedirectToActionPermanent("Login");
        }

        public ActionResult AddAdminUser()
        {
            Role MyRole = Role.Admin;
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            string claimRole = userIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            MyRole = (Role)Enum.Parse(typeof(Role), claimRole, true);
            ViewBag.MyRole = MyRole;
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddAdminUser(AddAdminUserModel model)
        {
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    model.role = Role.Admin;
                    ResponseModel<object> mResult = await _service.AddAdminUser(model);
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                }
            }
            return RedirectToActionPermanent("Index", "Home");
        }

        public ActionResult ViewAdminUser()
        {
            Role MyRole = Role.Admin;
            var userIdentity = (ClaimsIdentity)User.Identity;
            var claims = userIdentity.Claims;
            string claimRole = userIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            MyRole = (Role)Enum.Parse(typeof(Role), claimRole, true);
            ViewBag.MyRole = MyRole;
            IQueryable<AddAdminUserModel> model = _service.ViewAdminUser();
            return View(model);
        }

       
    }
}