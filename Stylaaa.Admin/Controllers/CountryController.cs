﻿using Stylaaa.Admin.Filter;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    [CustomAuthorization]
    public class CountryController : BaseController
    {
        private CountryServices _country;
        public CountryController()
        {
            _country = new CountryServices();
        }

        public ActionResult Index()
        {
            IQueryable<CountryModel> Categorieslist = _country.IQueryable_Country();
            return View(Categorieslist);
        }

        public ActionResult Add()
        {
            ViewBag.Action = "Add";
            return View("ManageCountry");
        }

        [HttpPost]
        public async Task<ActionResult> Add(CountryModel cmodel)
        {
            if (ModelState.IsValid)
            {
                bool IsDuplicate = _country.IsDuplicateCountry(cmodel);
                if (IsDuplicate)
                {
                    string Error = "Another Country is Already  Exist with Same Name";
                    TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                    ModelState.AddModelError("", Error);
                }
                else
                {
                    long id = await _country.InsertOrUpdateCountry(cmodel);
                    if (id > 0)
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "Country added successfully !");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        string Msg = "Failed to add Country!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Msg);
                        ModelState.AddModelError("", Msg);
                    }
                }
            }
            return View("ManageCountry", cmodel);
        }

        public async Task<ActionResult> Edit(long Id)
        {
            CountryModel objCountry = await _country.EditCountryById(Id);
            if (objCountry == null)
                return RedirectToActionPermanent("Index");
            ViewBag.Action = "Edit";
            return View("ManageCountry", objCountry);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(CountryModel model)
        {
            if (ModelState.IsValid)
            {
                bool IsDuplicate = _country.IsDuplicateCountry(model);
                if (IsDuplicate)
                {
                    string Error = "Another Country is Already  Exist with Same Name";
                    TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                    ModelState.AddModelError("", Error);
                }
                else
                {
                    long id = await _country.InsertOrUpdateCountry(model);
                    if (id > 0)
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "Country Edited successfully !");
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        string Msg = "Failed to Edit Country!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Msg);
                        ModelState.AddModelError("", Msg);
                    }
                }
            }
            return View("ManageCountry", model);
        }

        public async Task<ActionResult> Delete(long Id)
        {
            ResponseModel<object> mResult = await _country.DeleteCountry(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Index");
        }
    }
}