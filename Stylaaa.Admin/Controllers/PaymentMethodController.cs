﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class PaymentMethodController : BaseController
    {
        private PaymentMethodServices _service;
        private UserService _user;
        public PaymentMethodController()
        {
            _service = new PaymentMethodServices();
            _user = new UserService();
        }
        public ActionResult Index()
        {
            string UserId = User.Identity.GetUserId();
            IQueryable<PaymentMethodModel> PaymentmethodList = _service.IQueryable_GetAllPaymentMethods(_user.GetLanguage(UserId));
            return View(PaymentmethodList);
        }

        public ActionResult AddPaymentMethod()
        {
            AdminPaymentMethodModel model = new AdminPaymentMethodModel();
            List<PaymentMethodLanguageModel> PaymentLanguage = new List<PaymentMethodLanguageModel>();
            PaymentLanguage.Add(new PaymentMethodLanguageModel()
            {
                LanguageId = languageType.English,
                LanguageName = languageType.English.ToString(),
            });

            PaymentLanguage.Add(new PaymentMethodLanguageModel()
            {
                LanguageId = languageType.German,
                LanguageName = languageType.German.ToString(),
            });

            model.PaymentMethodLanguageList = PaymentLanguage;           
            ViewBag.Action = "AddPaymentMethod";
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> InsertOrUpdatePayment(AdminPaymentMethodModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.IsPaymentMethodDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    ResponseModel<object> mResult = new ResponseModel<object>();
                    mResult = await _service.InsertOrUpdatePayment(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("AddPaymentMethod", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            AdminPaymentMethodModel model = await _service.GetPaymentmethodDetail(id);
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("AddPaymentMethod", model);
        }

        //[HttpPost]
        //public async Task<ActionResult> Edit(PaymentMethodModel model)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    if (ModelState.IsValid)
        //    {
        //        mResult = await _service.AddPaymentMethod(model);
        //        if (mResult.Status == ResponseStatus.Success)
        //        {
        //            TempData["alertModel"] = new AlertModel(AlertStatus.success, "Payment Method Edited successfully !");
        //            return RedirectToAction("Index");
        //        }
        //        else
        //        {
        //            string Msg = (!String.IsNullOrWhiteSpace(mResult.Message) ? mResult.Message : "Failed to Edit Payment Method!");
        //            TempData["alertModel"] = new AlertModel(AlertStatus.danger, Msg);
        //            ModelState.AddModelError("", Msg);
        //        }
        //    }
        //    ViewBag.Action = "Edit";
        //    return View("AddPaymentMethod", model);
        //}

        public async Task<ActionResult> Delete(long Id)
        {
            ResponseModel<object> mResult = await _service.DeleteMethodById(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("Index");
        }
    }
}