﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class SubCategoriesController : BaseController
    {
        private SubCategoriesService _service;
        private CategoryService _catService;
        private UserService _user;

        ResponseModel<object> mResult;

        public SubCategoriesController()
        {
            _service = new SubCategoriesService();
            _catService = new CategoryService();
            _user = new UserService();
        }
        // GET: SubCategories
        public ActionResult Index()
        {
            string UserId = User.Identity.GetUserId();
            IQueryable<SubCategoriesModel> Categorieslist = _service.IQueryable_GetAllCategories(_user.GetLanguage(UserId));
            return View(Categorieslist);
        }

        public async Task<ActionResult> Add()
        {
            AdminSubCategoryModel model = new AdminSubCategoryModel();
            List<SubCategoryLanguageModel> CategoryLanguages = new List<SubCategoryLanguageModel>();
            CategoryLanguages.Add(new SubCategoryLanguageModel()
            {
                LanguageId = languageType.English,
                LanguageName = languageType.English.ToString(),
            });

            CategoryLanguages.Add(new SubCategoryLanguageModel()
            {
                LanguageId = languageType.German,
                LanguageName = languageType.German.ToString(),
            });
            ViewData["category"] = _catService.GetCategoryDropdown();
            model.CategoryLanguageModelList = CategoryLanguages;
            return View("ManageSubCategory", model);
        }

        [HttpPost]
        public async Task<ActionResult> Add(AdminSubCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.isSubCategoryDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    
                    mResult = await _service.InsertOrUpdateSubCategory(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("ManageSubCategory", model);
        }

        public async Task<ActionResult> Edit(long id)
        {
            AdminSubCategoryModel model = await _service.GetSubCategoryDetail(id);
            ViewData["category"] = _catService.GetCategoryDropdown();
            if (model == null)
                return RedirectToActionPermanent("Index");
            return View("ManageSubCategory", model);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(AdminSubCategoryModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.isSubCategoryDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate SubCategory Name");
                    ModelState.AddModelError("", "Duplicate SubCategory Name");
                }
                else
                {                   

                    mResult = await _service.InsertOrUpdateSubCategory(model);
                    OnBindMessage(mResult.Status, mResult.Message);

                    if (mResult.Status == ResponseStatus.Success)
                        return RedirectToActionPermanent("Index");
                }
            }
            return View("ManageSubCategory", model);
        }

        public async Task<ActionResult> DeleteSubCategory(long Id)
        {
            mResult = await _service.DeleteSubCategory(Id);
            OnBindMessage(mResult.Status, mResult.Message);
            return RedirectToActionPermanent("Index");
        }
    }
}