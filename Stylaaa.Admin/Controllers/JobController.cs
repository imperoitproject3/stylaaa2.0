﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class JobController : BaseController
    {
        // GET: Job
        private JobServices _service;

        public JobController()
        {
            _service = new JobServices();
        }
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult AllJob()
        {
            IQueryable<GetAllJobList> GetAllPost = _service.GetJobListForAdmin();
            return View(GetAllPost);
        }

        public async Task<JsonResult> DeletejobByAdmin(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            mResult = await _service.DeleteJob(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            if (mResult.Status == ResponseStatus.Success)
                return Json(mResult, JsonRequestBehavior.AllowGet);
            //if (mResult.Result)
            //    return RedirectToAction("AllStylish");
            //else
            //    return RedirectToAction("AllUsers");
            else
                return Json(mResult, JsonRequestBehavior.AllowGet);

        }
    }
}