﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stylaaa.Core.Models;
using Stylaaa.Core.Helper;
using Stylaaa.Admin.Filter;
using System.Threading.Tasks;
using Stylaaa.Services;
using Microsoft.AspNet.Identity;

namespace Stylaaa.Admin.Controllers
{
    [CustomAuthorization]
    public class HomeController : BaseController
    {
        private CoreUserServices _Users;
        private StylishServices _stylish;
        private GeneralServices _service;
        private MessageService _chat;
        private BookStylistService _booking;
        public HomeController()
        {
            _chat = new MessageService();
            _Users = new CoreUserServices();
            _stylish = new StylishServices();
            _service = new GeneralServices();
            _booking = new BookStylistService();
        }

        public ActionResult Index()
        {
            DashboardCount model = _service.GetDashboardCount();
            if (model != null)
            {
                ViewBag.stylish = model.TotalStylist;
                ViewBag.user = model.TotalUser;
                ViewBag.post = model.TotalPost;
                ViewBag.request = model.TotalRequest;
            }
            return View();
        }

        public ActionResult Bookings()
        {
            IQueryable<AdminViewBookingModel> model = _booking.GetBookingDetails();
            return View(model);
        }

        public async Task<JsonResult> DeleteBooking(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _booking.DeleteBookingById(id);

            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);

            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        public async Task<ActionResult> Terms()
        {
            TermsModel model = await _service.GetTerms();
            if (model == null || model.TermsLanguageModelList.Count == 0)
            {
                model = new TermsModel();
                List<LanguageModel> TermsLanguage = new List<LanguageModel>();
                TermsLanguage.Add(new LanguageModel()
                {
                    LanguageId = languageType.English,
                    LanguageName = languageType.English.ToString(),
                });

                TermsLanguage.Add(new LanguageModel()
                {
                    LanguageId = languageType.German,
                    LanguageName = languageType.German.ToString(),
                });
                model.TermsLanguageModelList = TermsLanguage;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> UpdateTerms(TermsModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.IstermsDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    ResponseModel<object> mResult = await _service.UpdateTermsConditions(model);
                    if (mResult.Status == ResponseStatus.Failed)
                    {
                        string Error = "Failed to update Terms & Condition!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                        ModelState.AddModelError("", Error);
                    }
                    else
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "Terms & condition updated successfully.");
                    }
                }
            }
            return RedirectToAction("Terms");

        }

        public async Task<ActionResult> AboutUs()
        {
            AboutUsModel model = await _service.GetAboutUs();
            if (model == null || model.AboutUsLanguageModelList.Count == 0)
            {
                model = new AboutUsModel();
                List<AboutUsLanguageModel> AboutUsLanguages = new List<AboutUsLanguageModel>();
                AboutUsLanguages.Add(new AboutUsLanguageModel()
                {
                    LanguageId = languageType.English,
                    LanguageName = languageType.English.ToString(),
                });

                AboutUsLanguages.Add(new AboutUsLanguageModel()
                {
                    LanguageId = languageType.German,
                    LanguageName = languageType.German.ToString(),
                });
                model.AboutUsLanguageModelList = AboutUsLanguages;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> AboutUs(AboutUsModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.isAboutUsDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    ResponseModel<object> mResult = await _service.UpdateAboutUs(model);
                    if (mResult.Status == ResponseStatus.Failed)
                    {
                        string Error = "Failed to update About Us!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                        ModelState.AddModelError("", Error);
                    }
                    else
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "AboutUs updated successfully.");
                    }
                }
                return RedirectToAction("AboutUs");
                //return View(model);
            }
            return View(model);
        }

        public async Task<ActionResult> WhatIsStylaaa()
        {
            WhatIsStylaaaModel model = await _service.GetWhatIsStylaaa();
            if (model == null || model.WhatIsStylaaLanguageModelList.Count == 0)
            {
                model = new WhatIsStylaaaModel();
                List<LanguageModel> WhatIsStylaaLanguages = new List<LanguageModel>();
                WhatIsStylaaLanguages.Add(new LanguageModel()
                {
                    LanguageId = languageType.English,
                    LanguageName = languageType.English.ToString(),
                });

                WhatIsStylaaLanguages.Add(new LanguageModel()
                {
                    LanguageId = languageType.German,
                    LanguageName = languageType.German.ToString(),
                });
                model.WhatIsStylaaLanguageModelList = WhatIsStylaaLanguages;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> WhatIsStylaaa(WhatIsStylaaaModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.IsWhatIsStylaaDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    ResponseModel<object> mResult = await _service.UpdateWhatIsStylaaa(model);
                    if (mResult.Status == ResponseStatus.Failed)
                    {
                        string Error = "Failed to update What is Stylaaa!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                        ModelState.AddModelError("", Error);
                    }
                    else
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "What Is Stylaa Content updated successfully.");
                    }
                }
                return RedirectToAction("WhatIsStylaaa");
                //return View(model);
            }
            return View(model);
        }

        public async Task<ActionResult> WhatIsStylaaaForUser()
        {
            WhatIsStylaaaForUserModel model = await _service.GetWhatIsStylaaaForUser();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> WhatIsStylaaaForUser(WhatIsStylaaaForUserModel model)
        {
            long id = await _service.UpdateWhatIsStylaaaForUser(model);
            if (id < 1)
            {
                string Error = "Failed to update What is stylaaa for user text!";
                TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                ModelState.AddModelError("", Error);
            }
            else
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.success, "What is stylaaa for user text updated successfully.");
            }
            return RedirectToAction("WhatIsStylaaaForUser");
        }

        public async Task<ActionResult> WhatIsStylaaaForStylist()
        {
            WhatIsStylaaaForStylistModel model = await _service.GetWhatIsStylaaaForStylist();
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> WhatIsStylaaaForStylist(WhatIsStylaaaForStylistModel model)
        {
            long id = await _service.UpdateWhatIsStylaaaForStylist(model);
            if (id < 1)
            {
                string Error = "Failed to update What is stylaaa for stylist text!";
                TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                ModelState.AddModelError("", Error);
            }
            else
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.success, "What is stylaaa for stylist text updated successfully.");
            }
            return RedirectToAction("WhatIsStylaaaForStylist");
        }

        public ActionResult ContactUs()
        {
            IQueryable<ContactUsModel> ContactUsDetail = _service.GetContactUsDetail();
            return View(ContactUsDetail);
        }

        public async Task<ActionResult> DeleteContactUs(long Id)
        {
            ResponseModel<object> mResult = await _service.DeleteContactUs(Id);
            TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("ContactUs");
        }

        public async Task<ActionResult> Imprints()
        {
            ImprintsModel model = await _service.GetImprints();
            if (model == null || model.ImprintLanguageModelList.Count == 0)
            {
                model = new ImprintsModel();
                List<LanguageModel> TermsLanguage = new List<LanguageModel>();
                TermsLanguage.Add(new LanguageModel()
                {
                    LanguageId = languageType.English,
                    LanguageName = languageType.English.ToString(),
                });

                TermsLanguage.Add(new LanguageModel()
                {
                    LanguageId = languageType.German,
                    LanguageName = languageType.German.ToString(),
                });
                model.ImprintLanguageModelList = TermsLanguage;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> Imprints(ImprintsModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.IsImprintDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    ResponseModel<object> mResult = await _service.UpdateImprints(model);
                    if (mResult.Status == ResponseStatus.Failed)
                    {
                        string Error = "Failed to update Imprints!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                        ModelState.AddModelError("", Error);
                    }
                    else
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "Imprints updated successfully.");
                    }
                }
                return RedirectToAction("Imprints");
                //return View(model);
            }
            return RedirectToAction("Imprints");
        }

        public async Task<ActionResult> PrivacyPolicy()
        {
            PrivacyPolicyModel model = await _service.GetPrivacyPolicy();
            if (model == null || model.PolicyLanguageModelList.Count == 0)
            {
                model = new PrivacyPolicyModel();
                List<LanguageModel> PolicyLanguage = new List<LanguageModel>();
                PolicyLanguage.Add(new LanguageModel()
                {
                    LanguageId = languageType.English,
                    LanguageName = languageType.English.ToString(),
                });

                PolicyLanguage.Add(new LanguageModel()
                {
                    LanguageId = languageType.German,
                    LanguageName = languageType.German.ToString(),
                });
                model.PolicyLanguageModelList = PolicyLanguage;
            }
            return View(model);
        }

        [HttpPost]
        [ValidateInput(false)]
        public async Task<ActionResult> PrivacyPolicy(PrivacyPolicyModel model)
        {
            if (ModelState.IsValid)
            {
                bool isDuplicate = _service.IspolicyDuplicate(model);
                if (isDuplicate)
                {
                    OnBindMessage(AlertStatus.danger, "Duplicate Category Name");
                    ModelState.AddModelError("", "Duplicate Category Name");
                }
                else
                {
                    ResponseModel<object> mResult = await _service.UpdatePrivacyPolicy(model);
                    if (mResult.Status == ResponseStatus.Failed)
                    {
                        string Error = "Failed to update Policy!";
                        TempData["alertModel"] = new AlertModel(AlertStatus.danger, Error);
                        ModelState.AddModelError("", Error);
                    }
                    else
                    {
                        TempData["alertModel"] = new AlertModel(AlertStatus.success, "Policy updated successfully.");
                    }
                }
                return RedirectToAction("PrivacyPolicy");
                //return View(model);
            }
            return View(model);
        }

        public ActionResult SendToStylist()
        {
            IQueryable<UserDetailModel> objStylish = _stylish.GetAllStylishDetail();
            ViewBag.active = "Stylist";
            return View(objStylish);
        }

        public ActionResult SendToUser()
        {
            string id = User.Identity.GetUserId();
            IQueryable<UserDetailModel> objUsers = _Users.getAllUsers_IQueryable();
            ViewBag.active = "Users";
            return View("SendToStylist", objUsers);
        }

        public ActionResult AllUsers()
        {
            string id = User.Identity.GetUserId();
            IQueryable<UserDetailModel> objAll = _Users.getAllUserAndStylist_IQueryable();
            ViewBag.active = "Stylist & User";
            return View("SendToStylist", objAll);
        }

        public async Task<ActionResult> SendToAll(string SelectedUserId, string MessageText)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string senderId = User.Identity.GetUserId();
            if (SelectedUserId != null)
            {
                List<string> UsersId = SelectedUserId != null ? SelectedUserId.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries).ToList() : null;
                foreach (var id in UsersId)
                {
                    MessageSendModel model = new MessageSendModel();
                    model.ReceiverID = id;
                    model.IsImage = false;
                    model.MessageText = MessageText;
                    mResult = await _chat.SendMessageFromAdmin(model, senderId);
                    //ResponseModel<bool> mResult = await _service.DeleteUser(item);
                }
                TempData["alertModel"] = new AlertModel(AlertStatus.success, string.Format("Send message to {0} Users", UsersId.Count()));
            }
            return RedirectToAction("SendToStylist");
        }

        public ActionResult AdminMessages(Role role)
        {
            string roleAction = "Stylist";
            if (role == Role.User)
            {
                roleAction = "users";
            }
            ViewBag.action = roleAction;
            IQueryable<SendMessagesFromAdmin> model = _chat.MessagesListSendByAdmin(role);
            return View(model);
        }

        public async Task<ActionResult> DeleteMessage(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _chat.DeleteMessage(Id);
            TempData["alertmodel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("AdminMessages", new { role = Role.Stylist });
        }

        public ActionResult SpamReview()
        {
            IQueryable<SpamReviewModel> spamreview = _service.GetSpamReviews();
            return View(spamreview);
        }

        public async Task<ActionResult> DeleteSpamREview(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.DeleteSpamReview(Id);
            TempData["alertmodel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
            return RedirectToAction("SpamReview");

        }


    }
}