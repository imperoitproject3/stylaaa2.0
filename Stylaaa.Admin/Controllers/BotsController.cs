﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class BotsController : BaseController
    {
        private BotsService _service;
        private UserService _userservice;

        public BotsController()
        {
            _service = new BotsService();
            _userservice = new UserService();
        }
        // GET: Bots
        public ActionResult Index()
        {
            IQueryable<ViewBots> model = _service.ViewBots();
            return View(model);
        }

        public async Task<ActionResult> ViewDummyUsers()
        {
            List<ViewDummyUserList> model = await _service.getAllDummyUsers();
            return View(model);
        }

        public async Task<ActionResult> CreateBot()
        {
            List<DummyUserList> model = await _service.GetDummyUserList();
            List<SelectListItem> dropDownList = (from x in model
                                                 select new SelectListItem
                                                 {
                                                     Text = x.Name,
                                                     Value = x.Id
                                                 }).ToList();
            ViewData["dummyuserlist"] = dropDownList;
            ViewBag.action = "AddBot";
            return View("AddBot");
        }

        public async Task<ActionResult> BotManage()
        {
            ManageBoatModel model =await _service.ManageBoat();
            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> AddBot(AddBotModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.AddBot(model);
            if (mResult.Status == ResponseStatus.Success)
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.danger, mResult.Message);
            }
            return RedirectToAction("Index");
        }

        public async Task<ActionResult> DeleteBot(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            mResult = await _service.DeleteBot(Id);
            if (mResult.Status == ResponseStatus.Success)
            {
                TempData["alertModel"] = new AlertModel(AlertStatus.success, mResult.Message);
            }
            return RedirectToAction("Index");
        }

        [HttpPost]
        public async Task<JsonResult> BotActiveToggle(long Id)
        {
            ResponseModel<bool> mResult = await _service.BotActiveToggle(Id);
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<JsonResult> ActiveFollowBot(int id)
        {
            ResponseModel<bool> mResult = await _service.ActiveFollowBot(id);
            if (mResult.Result)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Bot Activated";
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Bot Activated";
            }
            return Json(mResult, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AddDummyUser()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> AddDummyUser(AddDummyUserModel model)
        {
            if (model.fileBaseImage != null)
            {
                //Stream inStream = model.fileBaseImage.InputStream;
                //Image PostedImage = Image.FromStream(inStream);

                string ogFileName = model.fileBaseImage.FileName.Trim('\"');
                string shortGuid = ShortGuid.NewGuid().ToString();
                var thisFileName = shortGuid + Path.GetExtension(ogFileName);
                model.fileBaseImage.SaveAs(Path.Combine(GlobalConfig.UserImagePath, thisFileName));
                model.ImageName = thisFileName;
            }
            if (User.Identity.IsAuthenticated)
            {
                if (ModelState.IsValid)
                {
                    model.role = Role.User;
                    ResponseModel<object> mResult = await _userservice.AddDummyUser(model);
                    TempData["alertModel"] = new AlertModel(mResult.Status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger, mResult.Message);
                }
            }
            return RedirectToAction("AddDummyUser");
        }



        /// <summary>
        /// Bots Schedulars
        /// </summary>
        /// <param name="Hour"></param>
        /// <returns></returns>
        
    }
}