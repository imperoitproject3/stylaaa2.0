﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin.Controllers
{
    public class BaseController : Controller
    {        
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
            Role myRole = Role.Admin;

            var UserIdentity = (ClaimsIdentity)User.Identity;
            var claims = UserIdentity.Claims;
            string claimRole = UserIdentity.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            myRole = (Role)Enum.Parse(typeof(Role), claimRole, true);
            ViewBag.MyRole = myRole;
            ViewBag.FullName = claims.Where(c => c.Type == "FullName").FirstOrDefault().Value;
        }

        public void BindLanguage()
        {
            var Languagelist = new List<SelectListItem>();
            Languagelist.Add(new SelectListItem { Text = languageType.English.ToString(), Value = languageType.English.ToString() });
            Languagelist.Add(new SelectListItem { Text = languageType.German.ToString(), Value = languageType.German.ToString() });

            ViewData["LanguageList"] = Languagelist;
        }

        public void OnBindMessage(ResponseStatus status, string message)
        {
            OnBindMessage((status == ResponseStatus.Success ? AlertStatus.success : AlertStatus.danger), message);
        }

        public void OnBindMessage(AlertStatus status, string message)
        {
            TempData["alertModel"] = new AlertModel(status, message);
        }
    }
}