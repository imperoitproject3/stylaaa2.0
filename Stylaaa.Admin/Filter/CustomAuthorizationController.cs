﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Stylaaa.Admin.Filter
{
    public static class UserRoleMenu
    {
        public static List<UserRoleMenuModel> GetUserRoleMenu(string UserId, Role UserRole)
        {
            List<UserRoleMenuModel> objUserRoles = new List<UserRoleMenuModel>
            {
                new UserRoleMenuModel{Role=UserRole,Controller="Home",Action="Index" }
            };
            return objUserRoles;
        }
    }

    public class CustomAuthorization : AuthorizeAttribute
    {
        //private UserService _service;
        private bool isLoggedIn = false;

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            base.AuthorizeCore(httpContext);
            ClaimsIdentity Identity = (ClaimsIdentity)httpContext.User.Identity;
            if (!Identity.IsAuthenticated)
                return false;

            isLoggedIn = true;

            string claimRole = ((ClaimsIdentity)httpContext.User.Identity).Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role).Value;
            Role UserRole = (Role)Enum.Parse(typeof(Role), claimRole, true);

            string UserId = httpContext.User.Identity.GetUserId();

            string Controller = httpContext.Request.RequestContext.RouteData.GetRequiredString("controller");
            string Action = httpContext.Request.RequestContext.RouteData.GetRequiredString("action");

            List<UserRoleMenuModel> UserRoleMenus = UserRoleMenu.GetUserRoleMenu(UserId, UserRole);
            if (UserRole == Role.SuperAdmin|| UserRole==Role.Admin || (UserRole == Role.Stylist && UserRoleMenus.Any(x => x.Controller.IEquals(Controller) && x.Action.IEquals(Action))) || (UserRole == Role.User && UserRoleMenus.Any(x => x.Controller.IEquals(Controller) && x.Action.IEquals(Action))))
                return true;
            return false;
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (isLoggedIn)
            {
                filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Home",
                                    action = "Index"
                                }));
            }
            else
            {
                filterContext.Result =
                        new RedirectToRouteResult(
                            new RouteValueDictionary(
                                new
                                {
                                    controller = "Account",
                                    action = "Login"
                                    //controller = "Error",
                                    //action = "Unauthorized"
                                }));
            }
        }
    }
}