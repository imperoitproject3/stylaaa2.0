﻿var autocomplete;
var autocompleteAddress;
var PrimaryID = 0;
var currentLatitude = 53.7702356, currentLongitude = -2.7671912;
var CurrentAddress = "";
var map;
//Get Current Location

$(document).ready(function () {
    //GetCurrentLocation();
    $('.googlepostcode').on('focus', document, function () {
        console.log('focus')
        BindCurrentLatLong();
    });
    //BindCurrentLatLong();
});

function pan() {
    var panPoint = new google.maps.LatLng($('#LocationLatitude').val(), $('#LocationLongitude').val());
    map.panTo(panPoint)
}

function GetCurrentLocation() {
    try {
        if (navigator.geolocation) {
            var positionOptions = {
                enableHighAccuracy: true,
                timeout: 60000 //1 minutes
            };
            navigator.geolocation.getCurrentPosition(geolocationSuccess, geolocationError, positionOptions);
        }
        else
            console.log("Your browser doesn't support the Geolocation API");
    }
    catch (err) {
        console.log(err);
        BindCurrentLatLong();
    }
}

function geolocationSuccess(position) {
    currentLatitude = position.coords.latitude;
    currentLongitude = position.coords.longitude;
    BindCurrentLatLong();
}

function GetCurrentPosition(latitude, longitude) {
    source = new google.maps.LatLng(latitude, longitude);
}

function geolocationError(positionError) {
    console.log("error: " + positionError.message);
    BindCurrentLatLong();
}

function BindCurrentLatLong() {
    //var input = document.getElementById("txtAddressSearch");
    ////autocomplete = new google.maps.places.Autocomplete(input);

    //autocomplete = new google.maps.places.Autocomplete((input), {
    //    //types: ['establishment'],
    //    componentRestrictions: { 'country': 'uk' }
    //});

    //zipcode autocomplete functionality

    var txtZipcode = document.getElementsByClassName('googlepostcode')[0];
    if (txtZipcode) {
        var zipcode_autocomplete = new google.maps.places.Autocomplete((txtZipcode), {
            types: ['(regions)'],
            componentRestrictions: { 'country': 'uk' }
        });
        google.maps.event.addListener(zipcode_autocomplete, 'place_changed', function () {
            var place = zipcode_autocomplete.getPlace();
            currentLatitude = place.geometry.location.lat();
            currentLongitude = place.geometry.location.lng();
            var Address = place.formatted_address;
            AutoFillAddressGeomatry(Address);
        });
    }
}


//Open Map Modal
function CloseMapModal() {
    $('#Map_Modal').modal('hide');
    var pathname = window.location.pathname.toLocaleLowerCase();
    if (pathname != "/company/profile.aspx" && pathname != "/contractor/editprofile.aspx" && pathname != "/company/managejob.aspx")
        $("body").addClass("modal-open-custom");
}

function OpenMapModal() {
    return false;
    $("#Map_Modal").modal().on('shown.bs.modal',
        function () {
            InitilizeMap();
        });
}

function InitilizeMap() {
    var Latitude = $('#LocationLatitude').val();
    var Longitude = $('#LocationLongitude').val();
    if (Latitude == 0 && Longitude == 0) {
        Latitude = currentLatitude;
        Longitude = currentLongitude;
    }

    var source = new google.maps.LatLng(Latitude, Longitude);
    var map_options = {
        center: source,
        zoom: 16,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    map = new google.maps.Map(document.getElementById("map"), map_options);

    marker = new google.maps.Marker({
        position: source,
        map: map,
        draggable: true,
        animation: google.maps.Animation.DROP,
    });

    //google.maps.event.addListener(autocomplete, 'place_changed', function () {
    //    var place = autocomplete.getPlace();
    //    currentLatitude = place.geometry.location.lat();
    //    currentLongitude = place.geometry.location.lng();
    //    var Address = $("#txtAddressSearch").val();//place.formatted_address;
    //    AutoFillAddressGeomatry(Address);

    //    map.setCenter(place.geometry.location);
    //    marker.setPosition(place.geometry.location);
    //    marker.setAnimation(google.maps.Animation.DROP);
    //});

    var geocoder = new google.maps.Geocoder();

    google.maps.event.addListener(marker, 'dragend', function () {
        geocoder.geocode({ 'latLng': marker.getPosition() }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                if (results[0]) {
                    currentLatitude = marker.getPosition().lat();
                    currentLongitude = marker.getPosition().lng();
                    var Address = results[0].formatted_address;
                    $('#LocationLatitude').val(currentLatitude);
                    $('#LocationLongitude').val(currentLongitude);
                    //$('input.googleaddress').val(Address);
                    //AutoFillAddressGeomatry(Address);
                }
            }
        });
    });
}


//Set Address
{
    function AutoFillAddressGeomatry(Address) {
        var latlng = new google.maps.LatLng(currentLatitude, currentLongitude);
        var geocoder = new google.maps.Geocoder();

        //geocoder.geocode({ 'latLng': latlng }, function (results, status) {
        geocoder.geocode({ 'address': Address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results);
                var address_components = results[0].address_components;

                if (Address == "")
                    CurrentAddress = results[0].formatted_address;
                else
                    CurrentAddress = Address;
                var Street = "";
                var City = "";
                var Country = "";
                var Zipcode = "";

                for (var i = 0; i < address_components.length; i++) {
                    var long_name = address_components[i].long_name;

                    var types = address_components[i].types;

                    for (var j = 0; j < types.length; j++) {
                        var addressType = address_components[i].types[j];

                        switch (addressType) {
                            case 'route':
                            case 'sublocality_level_1':
                            case 'sublocality':
                                {
                                    Street = long_name;
                                    break;
                                }

                            case 'postal_town':
                            case 'locality':
                                {
                                    City = long_name;
                                    break;
                                }

                            case 'country':
                            case 'administrative_area_level_1':
                                {
                                    Country = long_name;
                                    break;
                                }

                            case 'postal_code':
                                {
                                    Zipcode = long_name;
                                    break;
                                }
                        }
                    }
                }
                $("#LocationLatitude").val(currentLatitude);
                $('.googleLatitude').val(currentLatitude);
                $("#LocationLongitude").val(currentLongitude);
                $(".googleLongitude").val(currentLongitude);

                //$('#txtAddressSearch').val(CurrentAddress);
                $(".googleaddress").val(CurrentAddress);
                //$('.googlestreet').val(Street);
                $('.googlepostcode').val(Zipcode);
                //if (Zipcode)
                //    $('.googlepostcode').val(Zipcode);
                //else
                //    $('.googlepostcode').val(CurrentAddress);
                $('.googlecity').val(City);                
                InitilizeMap();
            }
        });
    }
}

//Set Address Components
{
    function SetAddressComponents(argID, Latitude, Longitude, EncodeAddress) {
        var Address = Base64Decode(EncodeAddress);
        PrimaryID = argID;
        currentLatitude = Latitude;
        currentLongitude = Longitude;
        CurrentAddress = Address;

        $("#Latitude").val(currentLatitude);
        $("#Longitude").val(currentLongitude);
        //$('#txtAddressSearch').val(CurrentAddress);
    }
}
function BindAddressSearch() {
    var txtAddress = document.getElementsByClassName('googleaddress')[0];
    if (txtAddress) {
        autocompleteAddress = new google.maps.places.Autocomplete((txtAddress), {
            //types: ['establishment'],
            //componentRestrictions: { 'country': 'uk' }
        });
        google.maps.event.addListener(autocompleteAddress, 'place_changed', function () {
            var place = autocompleteAddress.getPlace();
            currentLatitude = place.geometry.location.lat();
            currentLongitude = place.geometry.location.lng();
            var Address = $(txtAddress).val();
            AutoFillAddressGeomatry(Address);
        });
    }
}