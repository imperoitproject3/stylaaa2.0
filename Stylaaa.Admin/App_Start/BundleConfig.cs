﻿using System.Web;
using System.Web.Optimization;

namespace Stylaaa.Admin
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //************************ CSS ************************
            bundles.Add(new StyleBundle("~/css/bundles").Include(
                      "~/Content/Plugins/Bootstrap/CSS/bootstrap.css",
                      "~/Content/Plugins/Bootstrap/CSS/bootstrap-extended.css",
                      "~/Content/CSS/Core/app.css",
                      "~/Content/CSS/Core/colors.css",
                      "~/Content/CSS/Fonts/icomoon.css",
                      "~/Content/Plugins/pace/pace.css",
                      "~/Content/CSS/Custom/custom.css",
                      "~/Content/Plugins/animate/animate.css",
                       "~/Content/Plugins/sweetalert/sweetalert.css",
                       "~/Content/Plugins/Toaster/toastr.css",
                       "~/Content/Plugins/Toaster/toastr.min.css"
                      ));

            bundles.Add(new StyleBundle("~/css/menu").Include(
                    "~/Content/CSS/Menu/vertical-menu.css",
                    "~/Content/CSS/Menu/vertical-overlay-menu.css"
                    ));

            bundles.Add(new StyleBundle("~/css/icheck").Include(
                    "~/Content/Plugins/iCheck/icheck-blue.css"
                    ));

            bundles.Add(new StyleBundle("~/css/login-register").Include(
                    "~/Content/Pages/login-register/login-register.css"
                    ));

            bundles.Add(new StyleBundle("~/css/gridMvc").Include(
                    "~/Content/Plugins/Gridmvc/CSS/Gridmvc.css",
                    "~/Content/Plugins/Gridmvc/CSS/PagedList.css"
                    ));

            bundles.Add(new StyleBundle("~/css/uploadImage").Include(
                    "~/Content/Plugins/UploadImage/cropper.css",
                    "~/Content/Plugins/UploadImage/uploadImage.css"
                    ));

            bundles.Add(new StyleBundle("~/css/wizard").Include(
                    "~/Content/Plugins/wizard/wizard.css"
                    ));

            bundles.Add(new StyleBundle("~/css/jsgrid").Include(
                   "~/Content/Plugins/jsGrid/jsgrid-theme.min.css",
                   "~/Content/Plugins/jsGrid/jsgrid.min.css"
                   ));

            bundles.Add(new StyleBundle("~/css/summernote").Include(
                   "~/Content/Plugins/Summernote/summernote.css"
                   ));

            bundles.Add(new StyleBundle("~/css/select2").Include(
                   "~/Content/Plugins/Select2/select2.css"
                   ));


            //bundles.Add(new StyleBundle("~/css/toaster").Include(
            //        "~/Content/Plugins/Toaster/toastr.css",
            //        "~/Content/Plugins/Toaster/toastr.min.css"
            //        ));


            //************************ jquery ************************

            bundles.Add(new ScriptBundle("~/jQuery/bundles").Include(
                    "~/Content/Jquery/main/jquery.min.js",
                    "~/Content/Jquery/ui/tether.min.js",
                    "~/Content/Plugins/Bootstrap/Jquery/bootstrap.min.js",
                    "~/Content/Plugins/pace/pace.min.js",
                    "~/Content/Jquery/scripts.js"
                    ));

            bundles.Add(new ScriptBundle("~/jQuery/core").Include(
                   "~/Content/Jquery/Core/app-menu.js",
                   "~/Content/Jquery/Core/app.js"
                   ));

            bundles.Add(new ScriptBundle("~/jQuery/ui").Include(
                  "~/Content/Jquery/ui/blockUI.min.js",
                  "~/Content/Jquery/ui/jquery-sliding-menu.js",
                  "~/Content/Jquery/ui/jquery.matchHeight-min.js",
                  "~/Content/Jquery/ui/perfect-scrollbar.jquery.min.js",
                  "~/Content/Jquery/ui/screenfull.min.js",
                  "~/Content/Jquery/ui/unison.min.js",
                  "~/Content/Plugins/blockUI/blockUI.min.js",
                  "~/Content/Plugins/sweetalert/sweetalert.min.js",
                  "~/Content/Plugins/Toaster/toastr.min.js"
                  ));

            bundles.Add(new ScriptBundle("~/jQuery/validate").Include(
                    "~/Content/Plugins/Validate/jquery.validate.min.js",
                    "~/Content/Plugins/Validate/jquery.validate.unobtrusive.min.js",
                    "~/Content/Plugins/Validate/jquery.unobtrusive-ajax.min.js"
                    ));

            bundles.Add(new ScriptBundle("~/jQuery/iCheck").Include(
                   "~/Content/Plugins/iCheck/icheck.min.js"
                   ));

            bundles.Add(new ScriptBundle("~/jQuery/login-register").Include(
                   "~/Content/Pages/login-register/form-login-register.js"
                   ));

            bundles.Add(new ScriptBundle("~/jQuery/gridMvc").Include(
                  "~/Content/Plugins/Gridmvc/Jquery/gridmvc.js"
                  ));

            bundles.Add(new ScriptBundle("~/jQuery/uploadImage").Include(
                 "~/Content/Plugins/UploadImage/cropper.js",
                 "~/Content/Plugins/UploadImage/uploadImage.js"
                 ));

            bundles.Add(new ScriptBundle("~/jQuery/stepWizard").Include(
                 "~/Content/Plugins/wizard/jquery.steps.min.js",
                 "~/Content/Plugins/wizard/wizard-steps.js"
                 ));

            bundles.Add(new ScriptBundle("~/jQuery/jsgrid").Include(
                 "~/Content/Plugins/jsgrid/jsgrid.min.js"
                 ));

            bundles.Add(new ScriptBundle("~/jQuery/select2").Include(
                "~/Content/Plugins/Select2/form-select2.js",
                "~/Content/Plugins/Select2/select2.js"
                ));

            bundles.Add(new ScriptBundle("~/jQuery/summernote").Include(
                 "~/Content/Plugins/Summernote/summernote.js",
                 "~/Content/Plugins/Summernote/summernote.min.js"
                 ));

            bundles.Add(new ScriptBundle("~/bundles/GoogleMap").Include(
                    "~/Content/Plugins/GoogleMap/mapmodal.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/TimePicker").Include(
                    "~/Content/Plugins/Timepicker/picker.js",
                    "~/Content/Plugins/Timepicker/picker.time.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/DateTimePicker").Include(
                    "~/Content/Plugins/Timepicker/picker.js",
                    "~/Content/Plugins/Timepicker/bootstrap-datetimepicker.min.js"
                    ));

            //bundles.Add(new ScriptBundle("~/jQuery/toaster").Include(
            //     "~/Content/Plugins/Toaster/toastr.min.js"
            //     ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
