﻿using Stylaaa.Admin.Filter;
using System.Web;
using System.Web.Mvc;

namespace Stylaaa.Admin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomAuthorization());
        }        
    }
}
