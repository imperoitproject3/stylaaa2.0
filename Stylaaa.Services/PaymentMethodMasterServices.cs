﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class PaymentMethodServices
    {
        private PaymentMethodRepository _repo;

        public PaymentMethodServices()
        {
            _repo = new PaymentMethodRepository();
        }

        public IQueryable<PaymentMethodModel> IQueryable_GetAllPaymentMethods(languageType languageid)
        {
            return _repo.IQueryable_GetAllPaymentMethods(languageid);
        }

        public async Task<ResponseModel<object>> InsertOrUpdatePayment(AdminPaymentMethodModel model)
        {
            return await _repo.InsertOrUpdatePayment(model);
        }
        //ResponseModel<object> mResult = new ResponseModel<object>();

        //bool IsSamePack = _ctx.PaymentMethodMasterData.Any(x => x.PaymentMethod == model.PaymentMethod && x.Id != model.PaymentMethodId);
        //if (IsSamePack == false)
        //{
        //    try
        //    {
        //        PaymentMethodMaster objPayment = _ctx.PaymentMethodMasterData.FirstOrDefault(x => x.Id.Equals(model.PaymentMethodId));
        //        bool isNew = false;
        //        if (objPayment == null)
        //        {
        //            objPayment = new PaymentMethodMaster();
        //            isNew = true;
        //        }
        //        objPayment.PaymentMethod = model.PaymentMethod;
        //        objPayment.PaymentMethodText = model.PaymentMethodText;
        //        objPayment.CreatedDateTimeUTC = Utility.GetSystemDateTimeUTC();
        //        if (isNew)
        //            _ctx.PaymentMethodMasterData.Add(objPayment);
        //        int id = await _ctx.SaveChangesAsync();
        //        if (id > 0)
        //        {
        //            mResult.Message = "Payment method created Successfully!";
        //            mResult.Status = ResponseStatus.Success;
        //            mResult.Result = objPayment;
        //            return mResult;
        //        }
        //        else
        //        {
        //            mResult.Message = "Payment Method Not created";
        //            mResult.Status = ResponseStatus.Failed;
        //            return mResult;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}
        //else
        //{
        //    mResult.Message = "Same payment method is already exist";
        //    mResult.Status = ResponseStatus.Failed;
        //    return mResult;
        //}



        //public async Task<PaymentMethodModel> EditPaymentMethodId(long Id)
        //{
        //    return await (from payment in _ctx.PaymentMethodMasterData
        //                  where payment.Id == Id
        //                  select new PaymentMethodModel
        //                  {
        //                      PaymentMethodId = payment.Id,
        //                      PaymentMethod = payment.PaymentMethod,
        //                      PaymentMethodText = payment.PaymentMethodText,
        //                      CreatedDate = payment.CreatedDateTimeUTC
        //                  }).FirstOrDefaultAsync();
        //}
       

        public async Task<AdminPaymentMethodModel> GetPaymentmethodDetail(long PaymentId)
        {
            return await _repo.GetPaymentmethodDetail(PaymentId);
        }

        public async Task<PaymentMethodModel> EditCategoryById(languageType languageid, long Id)
        {
            return await _repo.EditCategoryById(languageid, Id);
        }

        public bool IsPaymentMethodDuplicate(AdminPaymentMethodModel model)
        {
            return _repo.IsPaymentMethodDuplicate(model);
        }

        public async Task<ResponseModel<object>> DeleteMethodById(long PaymentId)
        {
            return await _repo.DeleteMethodById(PaymentId);
        }
    }
}
