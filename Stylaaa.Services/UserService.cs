﻿using Microsoft.Owin;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class UserService : IDisposable
    {
        private UserRepository _repo;
        private readonly IOwinContext _owin;

        public UserService()
        {
            _repo = new UserRepository();
        }

        public UserService(IOwinContext owin)
        {
            _repo = new UserRepository(owin);
            this._owin = owin;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            return await _repo.AdminLogin(model);
        }

        public async Task<ResponseModel<UserResponseModel>> Register(UserRegisterModel model)
        {
            return await _repo.Register(model);
        }

        public async Task<ResponseModel<UserResponseModel>> Login(UserLoginModel model)
        {
            return await _repo.Login(model);
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            return await _repo.AdminChangePassword(model);
        }

        public async Task<ResponseModel<object>> AddAdminUser(AddAdminUserModel model)
        {
            return await _repo.AddAdminUser(model);
        }

        public async Task<ResponseModel<object>> AddDummyUser(AddDummyUserModel model)
        {
            return await _repo.AddDummyUser(model);
        }

        public IQueryable<AddAdminUserModel> ViewAdminUser()
        {
            return _repo.ViewAdminUser();
        }

        public void WebLogout()
        {
            _repo.WebLogout();
        }

        public void Dispose()
        {
            _repo.Dispose();
        }

        public languageType GetLanguage(string userId)
        {
            return _repo.GetLanguage(userId);
        }

        public ResponseModel<WebUserResponseModel> CheckProfileSetupCompleted(string UserId)
        {
            return _repo.CheckProfileSetupCompleted(UserId);
        }

        public object IsEmailExists(string Email)
        {
            return _repo.IsEmailExists(Email);
        }

        public bool checkprofilesetup(string UserId)
        {
            return _repo.checkprofilesetup(UserId);

        }
        public async Task<string> GetStripeAccoutId(string Userid)
        {
            return await _repo.GetStripeAccoutId(Userid);
        }

        public async Task<ResponseModel<EditPasswordModel>> EditPassword(string StylishId)
        {
            return await _repo.EditPassword(StylishId);
        }

        public async Task<ResponseModel<EditPasswordModel>> UpdatePassword(EditPasswordModel model, string StylishId)
        {
            return await _repo.UpdatePassword(model, StylishId);
        }

        ///user edit password
        ///
        public async Task<ResponseModel<UserEditPasswordModel>> UserEditPassword(string UserId)
        {
            return await _repo.UserEditPassword(UserId);
        }

        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            return await _repo.ForgotPassword(model);
        }

        public async Task<ResponseModel<object>> SetPassword(ResetPasswordModel model)
        {
            return await _repo.SetPassword(model);
        }

        public async Task<ResponseModel<UserEditPasswordModel>> UserUpdatePassword(UserEditPasswordModel model, string UserId)
        {
            return await _repo.UserUpdatePassword(model, UserId);
        }

        public ResponseModel<bool> ActivateAccount(string userId)
        {
            return _repo.ActivateAccount(userId);
        }

        public async Task<ResponseModel<bool>> DeactiveAccount(string userId)
        {
            return await _repo.DeactiveAccount(userId);
        }

        public async Task<ResponseModel<bool>> ActiveAccount(string userId)
        {
            return await _repo.ActiveAccount(userId);
        }

        public async Task<ResponseModel<bool>> ActiveToggle(string userId)
        {
            return await _repo.ActiveToggle(userId);
        }

        public Role GetRoleById(string UserId)
        {
            return _repo.GetRoleById(UserId);
        }
        //public async Task<ResponseModel<object>> StripeConnect(string stripeOAuthCode, string UserId)
        //{
        //    return await _repo.StripeConnect(stripeOAuthCode, UserId);
        //}

        public async Task<ResponseModel<bool>> DeleteUser(string UserId)
        {
            return await _repo.DeleteUser(UserId);
        }

        public async Task<ResponseModel<object>> AddCoverPic(string Filename, string UserId)
        {
            return await _repo.AddCoverPic(Filename, UserId);
        }

        public async Task<ResponseModel<object>> UpdateProfileImage(string strFileName, string userid)
        {
            return await _repo.UpdateProfileImage(strFileName, userid);
        }

        public async Task<ResponseModel<object>> UpdateCoverPic(string strFileName, string userid)
        {
            return await _repo.UpdateCoverPic(strFileName, userid);
        }

        public bool CheckValidUserName(string strUserName)
        {
            return _repo.CheckValidUserName(strUserName);
        }

        public string GetUserID(string strUserName)
        {
            return _repo.GetUserID(strUserName);
        }

        public string GetName(string userid)
        {
            return _repo.GetName(userid);
        }

        public string GetEmail(string userid)
        {
            return _repo.GetEmail(userid);
        }

        public string GetProfileImage(string userid)
        {
            return _repo.GetProfileImage(userid);
        }
        public async Task<bool> SetUserLanguage(languageType Language, string userId)
        {
            return await _repo.SetUserLanguage(Language, userId);
        }
        public IQueryable<UserModel> GetAllUsers_IQueryable(string UserId, bool isUserExpire)
        {
            return _repo.GetAllUsers_IQueryable(UserId, isUserExpire);
        }

        public async Task<List<JobChatModel>> GetAllJobRequest_IQueryable(string UserId)
        {
            return await _repo.GetAllJobRequest_IQueryable(UserId);
        }
    }
}
