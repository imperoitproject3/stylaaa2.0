﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Stylaaa.Data.Repository;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;


namespace Stylaaa.Services
{
    public class JobServices
    {
        private readonly JobRepository _repo;

        public JobServices()
        {
            _repo = new JobRepository();
        }

        public async Task<ResponseModel<object>> AddEditJob(AddJobModel model)
        {
            return await _repo.AddEditJob(model);
        }

        public async Task<List<ViewJobListModel>> GetJobList(jobFilterModel model)
        {
            return await _repo.GetJobList(model);
        }

        public async Task<GetUserJobs> JoblistByUser(PaginationModel model)
        {
            return await _repo.JoblistByUser(model);
        }

        public async Task<ResponseModel<object>> DeleteJob(long id)
        {
            return await _repo.DeleteJob(id);
        }

        public async Task<AddJobModel> GetJobdetailEdit(languageType languageid, long Id, string UserId)
        {
            return await _repo.GetJobdetailEdit(languageid, Id, UserId);
        }

        public async Task<ResponseModel<object>> JobActiveToggle(long JobId)
        {
            return await _repo.JobActiveToggle(JobId);
        }

        public IQueryable<GetAllJobList> GetJobListForAdmin()
        {
            return _repo.GetJobListForAdmin();
        }

    }
}
