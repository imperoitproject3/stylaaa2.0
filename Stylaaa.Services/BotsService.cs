﻿using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class BotsService
    {
        private BotsRepository _repo;
        public BotsService()
        {
            _repo = new BotsRepository();
        }

        public async Task<List<DummyUserList>> GetDummyUserList()
        {
            return await _repo.GetDummyUserList();
        }

        public IQueryable<ViewBots> ViewBots()
        {
            return _repo.ViewBots();
        }

        public async Task<ResponseModel<object>> AddBot(AddBotModel model)
        {
            return await _repo.AddBot(model);
        }

        public async Task<ResponseModel<object>> DeleteBot(long BotId)
        {
            return await _repo.DeleteBot(BotId);
        }

        public async Task<ResponseModel<bool>> BotActiveToggle(long BotId)
        {
            return await _repo.BotActiveToggle(BotId);
        }

        public async Task<ResponseModel<bool>> SceduleFollowUser()
        {
            return await _repo.FollowSchedular();
        }

        public async Task<bool> LikeSchedular()
        {
            return await _repo.LikeSchedular();
        }

        public async Task<bool> CommentSchedular()
        {
            return await _repo.CommentSchedular();
        }

        public async Task<bool> MessageSchedule()
        {
            return await _repo.MessageSchedule();
        }

        public async Task<ResponseModel<bool>> ActiveFollowBot(int id)
        {
            return await _repo.ActiveFollowBot(id);
        }

        public bool checkFollowSchedular()
        {
            return _repo.checkFollowSchedular();
        }

        public bool checkLikeSchedular()
        {
            return _repo.checkLikeSchedular();
        }

        public bool checkCommentSchedular()
        {
            return _repo.checkCommentSchedular();
        }

        public bool checkMessageSchedular()
        {
            return _repo.checkMessageSchedular();
        }

        public async Task<ManageBoatModel> ManageBoat()
        {
            return await _repo.ManageBoat();
        }

        public async Task<List<ViewDummyUserList>> getAllDummyUsers()
        {
            return await _repo.getAllDummyUsers();
        }
    }
}
