﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class BookStylistService
    {
        private BookStylistRepo _repo;
        public BookStylistService()
        {
            _repo = new BookStylistRepo();
        }

        public async Task<ResponseModel<object>> AddBookStylist(LandingPageModel model)
        {
            return await _repo.AddBookStylist(model);
        }

        public async Task<ResponseModel<object>> AddBookStylistWithPayment(LandingPageModel model, string tranId, ChargeStatus status, string striperesponse)
        {

            return await _repo.AddBookStylistWithPayment(model, tranId, status, striperesponse);
        }

        public IQueryable<AdminViewBookingModel> GetBookingDetails()
        {
            return _repo.GetBookingDetails();
        }

        public async Task<ResponseModel<object>> DeleteBookingById(long Id)
        {
            return await _repo.DeleteBookingById(Id);
        }

        public async Task<List<LandingPageStylistList>> GetTopStylist()
        {
            return await _repo.GetTopStylist();
        }
    }
}
