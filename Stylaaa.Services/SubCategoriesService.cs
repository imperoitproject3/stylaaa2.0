﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class SubCategoriesService
    {
        private SubCategoryRepo _repo;
        public SubCategoriesService()
        {
            _repo = new SubCategoryRepo();
        }

        public IQueryable<SubCategoriesModel> IQueryable_GetAllCategories(languageType languageid)
        {
            return _repo.IQueryable_GetAllSubCategories(languageid);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateSubCategory(AdminSubCategoryModel model)
        {
            return await _repo.InsertOrUpdateSubCategory(model);
        }

        public bool isSubCategoryDuplicate(AdminSubCategoryModel model)
        {
            return _repo.isSubCategoryDuplicate(model);
        }

        public async Task<ResponseModel<object>> DeleteSubCategory(long SubCategoryId)
        {
            return await _repo.DeleteSubCategory(SubCategoryId);
        }

        public async Task<AdminSubCategoryModel> GetSubCategoryDetail(long SubCatrgoryId)
        {
            return await _repo.GetSubCategoryDetail(SubCatrgoryId);
        }

        public List<SubCategoryDropdownModel> GetSubCategroies(languageType languageId, List<long> CatId, LandingGender gender)
        {
            return _repo.GetSubCategroies(languageId, CatId, gender);
        }

        public async Task<double> PriceBySubCategory(List<long> SubCatId)
        {
            return await _repo.PriceBySubCategory(SubCatId);
        }

        public async Task<List<SubcategoryList>> PriceListBySubCategory(List<long> SubCatIds)
        {
            return await _repo.PriceListBySubCategory(SubCatIds);
        }
    }
}
