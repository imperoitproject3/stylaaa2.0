﻿using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{

    public class PremiumMasterServices
    {
        private PremiumPackRepository _repo;

        public PremiumMasterServices()
        {
            _repo = new PremiumPackRepository();
        }

        public IQueryable<UpgradeAccountModel> getPremiumpackages()
        {
            return _repo.getPremiumpackages();
        }

        public async Task<ResponseModel<object>> AddPremiumPack(UpgradeAccountModel model)
        {
            return await _repo.InsertOrUpdatePremium(model);
        }

        public IQueryable<UpgradeCreditModel> getCreditPackages()
        {
            return _repo.getCreditPackages();
        }

        public async Task<UpgradeAccountModel> EditPackageById(long Id)
        {
            return await _repo.EditPackageById(Id);
        }

        public async Task<UpgradeCreditModel> EditCreditById(long Id)
        {
            return await _repo.EditCreditById(Id);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateCredit(UpgradeCreditModel model)
        {
            return await _repo.InsertOrUpdateCredit(model);
        }

        public async Task<ResponseModel<object>> DeletePackageById(long Id)
        {
            return await _repo.DeletePackageById(Id);
        }

        public async Task<ResponseModel<object>> DeleteCreditById(long Id)
        {
            return await _repo.DeleteCreditById(Id);
        }

        public List<SubscribedStylistModel> GetSubscribedStylist()
        {
            return _repo.GetSubscribedStylist();
        }

        public List<SubscribedCreditStylist> GetSubscribedCreditStylist()
        {
            return _repo.GetSubscribedCreditStylist();
        }

        public List<StylistTransactionHistory> GetTransactionHistory()
        {
            return _repo.GetTransactionHistory();

        }

        public async Task<ResponseModel<object>> deleteTransactionRecord(long Id, int Credit)
        {
            return await _repo.deleteTransactionRecord(Id, Credit);
        }

        public async Task<ResponseModel<object>> UploadReceiptFile(long Id, int Credit, string filename)
        {
            return await _repo.UploadReceiptFile(Id, Credit, filename);
        }

        public async Task<ResponseModel<object>> MarkAsSend(long Id, int Credit)
        {
            return await _repo.MarkAsSend(Id, Credit);
        }

        
    }
}
