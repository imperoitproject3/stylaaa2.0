﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class StylishServices
    {
        private StylishRepository _repo;

        public StylishServices()
        {
            _repo = new StylishRepository();
        }

        //public async Task<ViewUserDetail> GetUserDetail(string UserId)
        //{
        //    return  await _repo.GetUserDetail(UserId);
        //}

        public async Task<ResponseModel<StylistAboutModel>> GetStylistAbout(string UserId, string CurrentUserId)
        {
            return await _repo.GetStylistAbout(UserId, CurrentUserId);
        }

        public async Task<ResponseModel<StylistAboutModel>> StylistCompleteProfile(StylistAboutModel model)
        {
            return await _repo.StylistCompleteProfile(model);
        }


        public async Task<List<PopularStylishFilterModel>> FilterStylistlist(PostFilterModel model, string userId, bool isCount, string strLocalLatitude, string strLocalLongitude)
        {
            return await _repo.FilterStylistlist(model, userId, isCount, strLocalLatitude, strLocalLongitude);
        }

        public async Task<List<ViewAllStylishModel>> getAllStylish(StylishFilterPostModel model)
        {
            return await _repo.getAllStylish(model);
        }

        public async Task<ViewStylishProfileModel> StylishById(string Id)
        {
            return await _repo.StylishById(Id);
        }


        public async Task<ResponseModel<UserFollowTransactionModel>> Followings(string Id, string UserId)
        {
            return await _repo.Following(Id, UserId);
        }

        public async Task<ResponseModel<UserFollowTransactionModel>> Followers(string Id, string UserId)
        {
            return await _repo.Followers(Id, UserId);
        }
        

        public async Task<ResponseModel<UserReviewModel>> MyReview(string StylishId, string UserId)
        {
            return await _repo.MyReview(StylishId, UserId);
        }

        public IQueryable<StylistReviewModel> MyReviewByRate(string StylishId, int Rate)
        {
            return _repo.MyReviewByRate(StylishId, Rate);
        }

        public async Task<ResponseModel<object>> AddReply(ReplyToReviewModel model)
        {
            return await _repo.AddReply(model);
        }
               
        public async Task<ResponseModel<object>> SpamReview(AddToSpamModel model)
        {
            return await _repo.SpamReview(model);
        }

        public async Task<ResponseModel<UserReviewModel>> AddEditReview(AddUserReviewModel model)
        {
            return await _repo.AddEditReview(model);
        }

        public async Task<ResponseModel<object>> DeleteReviewById(long id)
        {
            return await _repo.DeleteReviewById(id);
        }

        public bool IsDuplicateReview(AddUserReviewModel model)
        {
            return _repo.IsDuplicateReview(model);
        }

        public object IsUserExists(string userName, string currentUserName)
        {
            return _repo.IsUserExists(userName, currentUserName);
        }


        public IQueryable<UserDetailModel> GetAllStylishDetail()
        {
            return _repo.GetAllStylishDetail();
        }

        public async Task<ViewStylishDetailsModel> GetStylishDetail(string UserId)
        {
            return await _repo.GetStylishDetail(UserId);
        }

        public async Task<ResponseModel<object>> DeleteReply(long replyid)
        {
            return await _repo.DeleteReply(replyid);
        }

        public async Task<List<StylistPaymentHistoryModel>> GetStylistPremiumTransaction(PaginationModel model)
        {
            return await _repo.GetStylistPremiumTransaction(model);
        }

        public async Task<AddUserReviewModel> GetReview(long reviewId)
        {
            return await _repo.GetReview(reviewId);
        }


        public async Task<List<StylistCreditPaymentHistory>> GetStylistCreditHistory(PaginationModel model)
        {
            return await _repo.GetStylistCreditHistory(model);
        }

        public int GetStylistCredit(string stylistId)
        {
            return _repo.GetStylistCredit(stylistId);
        }

    }
}
