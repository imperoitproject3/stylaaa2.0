﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class CategoryService
    {
        private CategoryRepository _repo;

        public CategoryService()
        {
            _repo = new CategoryRepository();
        }

        public IQueryable<CategoryModel> IQueryable_CategoryList(languageType languageid)
        {
            return _repo.IQueryable_GetAllCategories(languageid);
        }

        //public async Task<long> InsertOrUpdatecategory(languageType languageId, CategoryModel model)
        //{
        //    return await _repo.InsertOrUpdateCategory(languageId, model);
        //}

        public async Task<ResponseModel<object>> InsertOrUpdateCategory(AdminCategoryModel model)
        {
            return await _repo.InsertOrUpdateCategory(model);
        }

        //public bool IsDuplicateCategory(languageType languageid,CategoryModel cmodel)
        //{
        //    return _repo.IsDuplicateCategory(languageid, cmodel);
        //}

        public bool isCategoryDuplicate(AdminCategoryModel model)
        {
            return _repo.isCategoryDuplicate(model);
        }

        public async Task<CategoryModel> EditCategoryById(languageType languageid, long Id)
        {
            return await _repo.EditCategoryById(languageid, Id);
        }

        //public async Task<ResponseModel<object>> DeleteCategory(long Id)
        //{
        //    return await _repo.DeleteCategory(Id);
        //}

        public async Task<ResponseModel<object>> DeleteCategory(long CategoryId)
        {
            return await _repo.DeleteCategory(CategoryId);
        }

        public async Task<AdminCategoryModel> GetCategoryDetail(long CatrgoryId)
        {
            return await _repo.GetCategoryDetail(CatrgoryId);
        }
        
        public List<PostCategoryModel> GetPostCategoryList(languageType languageId)
        {
            return _repo.GetPostCategoryList(languageId);
        }

        public List<JobCategoryModel> GetJobCategoryList(languageType languageId)
        {
            return _repo.GetJobCategoryList(languageId);
        }

        public IQueryable<CategoryDropdownModel> GetCategroies(languageType languageId)
        {
            return _repo.GetCategroies(languageId);
        }

        public IQueryable<AdminCategoryDropdownmodel> GetCategoryDropdown()
        {
            return _repo.GetCategoryDropdown();
        }

        public IQueryable<CategoryDropdownModel> GetCategroiesByGender(languageType languageId, LandingGender GenderId)
        {
            return _repo.GetCategroiesByGender(languageId,GenderId);
        }
    }
}
