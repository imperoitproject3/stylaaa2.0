﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class PostServices
    {
        private PostRepository _repo;

        public PostServices()
        {
            _repo = new PostRepository();
        }

        public async Task<ResponseModel<object>> AddEditPost(AddPostModel model)
        {
            return await _repo.AddEditPost(model);
        }

        public async Task<ResponseModel<object>> EditPost(AddPostModel model)
        {
            return await _repo.EditPost(model);
        }

        public async Task<ResponseModel<object>> DeletePost(long Id)
        {
            return await _repo.DeletePost(Id);
        }

        public async Task<List<GetPostModelPopular>> FilterPostlist(PostFilterModel model, string UserId, bool isCount, string strLocalLatitude, string strLocalLongitude)
        {
            return await _repo.FilterPostlist(model, UserId, isCount, strLocalLatitude, strLocalLongitude);
        }

        public async Task<List<GetPostModelRecent>> RecentPost(PaginationModel model, List<string> strHashtag, long id = 0)
        {
            return await _repo.RecentPost(model, strHashtag, id);
        }

        public async Task<List<GetPostModelPopular>> PopularPost(PaginationModel model, List<string> strHashtag, long id = 0)
        {
            return await _repo.PopularPost(model, strHashtag, id);
        }

        public async Task<PostDetailModel> GetPostDetail(long Id, string UserId)
        {
            return await _repo.GetPostDetail(Id, UserId);
        }

        public async Task<AddPostModel> GetPostdetailEdit(languageType languageid, long Id, string UserId)
        {
            return await _repo.GetPostdetailEdit(languageid, Id, UserId);
        }

        public async Task<ResponseModel<object>> AddComment(PostCommentModel model)
        {
            return await _repo.AddComment(model);
        }

        public List<PostCommentModel> PostCommentList(long PostId)
        {
            return _repo.getPostComments(PostId);
        }

        public async Task<int> addOrRemoveLike(long PostId, string UserId)
        {
            return await _repo.addOrRemoveLike(PostId, UserId);
        }

        public double CountLikebypost(long PostId)
        {
            return _repo.CountLikebypost(PostId);
        }

        public IQueryable<getAllPostlist> GetPostListForAdmin()
        {
            return _repo.GetPostListForAdmin();
        }

        public IQueryable<ReportedPostList> GetReportedPostlist()
        {
            return _repo.GetReportedPostlist();
        }

        public async Task<PostDetailModel> PostDetailById(long Id)
        {
            return await _repo.PostDetailById(Id);
        }

        public async Task<List<HashtagFilterModel>> FilterHashtaglist(PostFilterModel model, bool isCount, string strLocalLatitude, string strLocalLongitude)
        {
            return await _repo.FilterHashtaglist(model, isCount, strLocalLatitude, strLocalLongitude);
        }

        public async Task<List<ViewLikeModel>> ViewLikeActivity(FindLikeModel model, bool isCount)
        {
            return await _repo.ViewLikeActivity(model, isCount);
        }

        public async Task<ResponseModel<object>> AddPostReportByUser(PostReportModel model)
        {
            return await _repo.AddPostReportByUser(model);
        }

        public async Task<PostViewDetailModel> GetPostViewDetailModel(languageType languageid, long postID, string userId)
        {
            return await _repo.GetPostViewDetailModel(languageid, postID, userId);
        }

        public async Task<ResponseModel<object>> AddPostCommentByUser(long postid, string userId, string message, long CommentId)
        {
            return await _repo.AddPostCommentByUser(postid, userId, message, CommentId);
        }

        public async Task<ResponseModel<object>> DeleteComment(long commentid, long replyid)
        {
            return await _repo.DeleteComment(commentid, replyid);
        }

        public async Task<ResponseModel<object>> SpamComment(long commentid)
        {
            return await _repo.SpamComment(commentid);
        }

        public async Task<GetPostListByUser> PostListByUser(PaginationModel model)
        {
            return await _repo.PostListByUser(model);
        }

        public async Task<ResponseModel<object>> AddSharePost(long id, string UserId)
        {
            return await _repo.AddSharePost(id, UserId);
        }

        public IQueryable<SpamCommentsListModel> GetSpamComments()
        {
            return _repo.GetSpamComments();
        }

        public async Task<ResponseModel<object>> DeleteSpamComment(long id)
        {
            return await _repo.DeleteSpamComment(id);
        }
    }
}