﻿using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class CreditServices
    {
        private CreditRepository _repo;
        public CreditServices()
        {
            _repo = new CreditRepository();
        }

        public int GetStylistCedit(string UserId)
        {
            return _repo.GetStylistCedit(UserId);
        }
    }
}
