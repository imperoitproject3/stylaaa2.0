﻿using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class CoreUserServices
    {
        private CoreUserRepository _repo;

        public CoreUserServices()
        {
            _repo = new CoreUserRepository();
        }
        public async Task<ResponseModel<ViewUserProfileModel>> getUserProfile(string userId)
        {
            return await _repo.getUserProfile(userId);
        }

        public async Task<ResponseModel<UserAboutModel>> GetUserAbout(string UserId)
        {
            return await _repo.GetUserAbout(UserId);
        }

        public async Task<ResponseModel<UserAboutModel>> UserCompleteProfile(UserAboutModel model)
        {

            return await _repo.UserCompleteProfile(model);
        }

        public async Task<ResponseModel<ViewUserFollowingModel>> Following(string Id, string UserId)
        {
            return await _repo.Following(Id,UserId);
        }

        public async Task<ResponseModel<ViewUserFollowingModel>> Followers(string Id, string UserId)
        {
            return await _repo.Followers(Id, UserId);
        }


        public async Task<ViewUserProfileModel> GetUserDetailForEdit(string Id)
        {
            return await _repo.GetUserDetailForEdit(Id);
        }

        public UserProfileModel GetUserProfileModel(string Id)
        {
            return _repo.GetUserProfileModel(Id);
        }
        

        //public async Task<ResponseModel<UserAppointmentModel>> Appointments(string userId)
        //{
        //    return await _repo.Appointments(userId);
        //}

        public async Task<ResponseModel<object>> UpdateUserDetail(UserEditProfileModel model)
        {
            return await _repo.UpdateUserDetail(model);
        }

        public IQueryable<UserDetailModel> getAllUsers_IQueryable()
        {
            return _repo.getAllUsers_IQueryable();           
        }

        public IQueryable<UserDetailModel> getAllUserAndStylist_IQueryable()
        {

            return _repo.getAllUserAndStylist_IQueryable();
        }

        public async Task<ViewUserDetail> GetUserDetail(string UserId)
        {
            return await _repo.GetUserDetail(UserId);
        }
    }
}
