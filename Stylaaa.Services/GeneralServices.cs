﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{

    public class GeneralServices
    {
        private GeneralRepository _repo;
        public GeneralServices()
        {
            _repo = new GeneralRepository();
        }

        public DashboardCount GetDashboardCount()
        {
            return _repo.GetDashboardCount();
        }

        public async Task<AboutUsModel> GetAboutUs()
        {
            return await _repo.GetAboutUs();
        }

        public async Task<AboutUsModel> GetAboutUs(languageType languageId)
        {
            return await _repo.GetAboutUs(languageId);
        }

        public bool isAboutUsDuplicate(AboutUsModel model)
        {
            return _repo.isAboutUsDuplicate(model);
        }

        public async Task<ResponseModel<object>> UpdateAboutUs(AboutUsModel model)
        {
            return await _repo.UpdateAboutUs(model);
        }

        public async Task<long> UpdateWhatIsStylaaaForUser(WhatIsStylaaaForUserModel model)
        {
            return await _repo.UpdateWhatIsStylaaaForUser(model);
        }

        public async Task<long> UpdateWhatIsStylaaaForStylist(WhatIsStylaaaForStylistModel model)
        {
            return await _repo.UpdateWhatIsStylaaaForStylist(model);
        }

        public async Task<ResponseModel<object>> AddContactUs(ContactUsModel model)
        {
            return await _repo.AddContactUs(model);
        }

        public async Task<WhatIsStylaaaForUserModel> GetWhatIsStylaaaForUser()
        {
            return await _repo.GetWhatIsStylaaaForUser();
        }

        public async Task<WhatIsStylaaaForStylistModel> GetWhatIsStylaaaForStylist()
        {
            return await _repo.GetWhatIsStylaaaForStylist();
        }

        public IQueryable<ContactUsModel> GetContactUsDetail()
        {
            return _repo.GetContactUsDetail();
        }

        public async Task<ResponseModel<object>> DeleteContactUs(long Id)
        {
            return await _repo.DeleteContactUs(Id);
        }

        public async Task<ImprintsModel> GetImprints()
        {
            return await _repo.GetImprints();
        }

        public async Task<ImprintsModel> GetImprints(languageType languageId)
        {
            return await _repo.GetImprints(languageId);
        }

        public bool IsImprintDuplicate(ImprintsModel model)
        {
            return _repo.IsImprintDuplicate(model);
        }

        public async Task<ResponseModel<object>> UpdateImprints(ImprintsModel model)
        {
            return await _repo.UpdateImprints(model);
        }

        //Follow And Following
        public async Task<ResponseModel<object>> followthis(string StylishId, string UserId)
        {
            return await _repo.followthis(StylishId, UserId);
        }

        public async Task<List<UsersFollowModel>> ViewAllFollowings(PaginationModel model)
        {
            return await _repo.ViewAllFollowings(model);
        }

        public async Task<List<UsersFollowModel>> ViewAllFollowers(PaginationModel model)
        {

            return await _repo.ViewAllFollowers(model);
        }

        public async Task<List<UpgradeAccountModel>> GetUpgradeAccountMasterDetail(int month)
        {
            return await _repo.GetUpgradeAccountMasterDetail(month);
        }

        public async Task<List<UpgradeCreditModel>> GetUpgradeCreditMasterDetail(int credit)
        {
            return await _repo.GetUpgradeCreditMasterDetail(credit);
        }

        public UpgradeAccountModel GetUpgradeAccountDetailByID(int UpgradeAccountId)
        {
            return _repo.GetUpgradeAccountDetailByID(UpgradeAccountId);
        }

        public UpgradeCreditModel GetUpgradeCreditDetailById(int UpgradeCreditId)
        {
            return _repo.GetUpgradeCreditDetailById(UpgradeCreditId);
        }

        public UpgradeAccountModel GetUpgradeAccountDetail(int month)
        {
            return _repo.GetUpgradeAccountDetail(month);
        }

        public bool CheckValidMonthForUpgradeAccount(int month)
        {
            return _repo.CheckValidMonthForUpgradeAccount(month);
        }

        public List<NotificationListModel> GetNotificationList(PaginationModel model, bool isUnreadCount = false)
        {
            return _repo.GetNotificationList(model, isUnreadCount);
        }

        public async Task<ResponseModel<object>> ReadAllNotificationList(string userId)
        {
            return await _repo.ReadAllNotificationList(userId);
        }

        public async Task<bool> AddStylistPremiumTransaction(StylistPremiumTransactionModel objectModel)
        {
            return await _repo.AddStylistPremiumTransaction(objectModel);
        }

        public async Task<bool> UpdateStylistPremiumTransactionStatus(StripeResponceModel objectModel)
        {
            return await _repo.UpdateStylistPremiumTransactionStatus(objectModel);
        }

        public async Task<bool> UpdateStylistPremiumTransaction(StylistPremiumTransactionModel objectModel)
        {
            return await _repo.UpdateStylistPremiumTransaction(objectModel);
        }

        //Terms & conditions
        public async Task<TermsModel> GetTerms()
        {
            return await _repo.GetTerms();
        }

        public async Task<TermsModel> GetTerms(languageType languageId)
        {
            return await _repo.GetTerms(languageId);
        }

        public bool IstermsDuplicate(TermsModel model)
        {
            return _repo.IstermsDuplicate(model);
        }

        public async Task<ResponseModel<object>> UpdateTermsConditions(TermsModel model)
        {
            return await _repo.UpdateTermsConditions(model);
        }

        //Privacy Policy
        public async Task<PrivacyPolicyModel> GetPrivacyPolicy()
        {
            return await _repo.GetPrivacyPolicy();
        }

        public async Task<PrivacyPolicyModel> GetPrivacyPolicy(languageType languageId)
        {
            return await _repo.GetPrivacyPolicy(languageId);
        }

        public bool IspolicyDuplicate(PrivacyPolicyModel model)
        {
            return _repo.IspolicyDuplicate(model);
        }

        public async Task<ResponseModel<object>> UpdatePrivacyPolicy(PrivacyPolicyModel model)
        {
            return await _repo.UpdatePrivacyPolicy(model);
        }

        //What Is Stylaaa
        public async Task<WhatIsStylaaaModel> GetWhatIsStylaaa()
        {
            return await _repo.GetWhatIsStylaaa();
        }

        public async Task<WhatIsStylaaaModel> GetWhatIsStylaaa(languageType languageId)
        {
            return await _repo.GetWhatIsStylaaa(languageId);
        }

        public bool IsWhatIsStylaaDuplicate(WhatIsStylaaaModel model)
        {
            return _repo.IsWhatIsStylaaDuplicate(model);
        }


        public async Task<ResponseModel<object>> UpdateWhatIsStylaaa(WhatIsStylaaaModel model)
        {
            return await _repo.UpdateWhatIsStylaaa(model);
        }

        public async Task<List<UserListModel>> GetUsers()
        {
            return await _repo.GetUsers();
        }

        public async Task<List<UserListModel>> GetStylist()
        {
            return await _repo.GetStylist();
        }

        public bool CheckFollowing(string StylishId, string UserId)
        {
            return _repo.CheckFollowing(StylishId, UserId);
        }

        public IQueryable<SpamReviewModel> GetSpamReviews()
        {
            return _repo.GetSpamReviews();
        }

        public async Task<ResponseModel<object>> DeleteSpamReview(long id)
        {
            return await _repo.DeleteSpamReview(id);
        }

        public async Task<bool> ISAbleToBuy(string UserId)
        {
            return await _repo.ISAbleToBuy(UserId);
        }


        public async Task<bool> AddStylistCreditTransaction(StylistCreditTransactionModel objectModel)
        {
            return await _repo.AddStylistCreditTransaction(objectModel);
        }

        public async Task<bool> UpdateStylistCreditTransaction(StylistCreditTransactionModel objectModel)
        {
            return await _repo.UpdateStylistCreditTransaction(objectModel);
        }

        public async Task<bool> UpdateStylistCreditTransactionStatus(StripeResponceModel objectModel)
        {
            return await _repo.UpdateStylistCreditTransactionStatus(objectModel);
        }

        public async Task<List<UserTableModel>> UserList(PageModel model)
        {
            return await _repo.UserList(model);
        }

        public async Task<List<UserTableModel>> UserList()
        {
            return await _repo.UserList();
        }

        public IQueryable<UserTableModel> GetUserList()
        {
            return _repo.GetUserList();
        }

        public int UserCount()
        {
            return _repo.UserCount();
        }
    }
}
