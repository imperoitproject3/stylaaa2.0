﻿using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class CountryServices:IDisposable
    {
        private CountryRepo _repo;

        public CountryServices()
        {
            _repo = new CountryRepo();
        }

        public IQueryable<CountryModel> IQueryable_Country()
        {
            return _repo.IQueryable_Country();
        }

        public IQueryable<CountryModel> GetCountryById(long Id)
        {
            return _repo.GetCountryById(Id);

        }

        public async Task<long> InsertOrUpdateCountry(CountryModel cmodel)
        {
            return await _repo.InsertOrUpdateCountry(cmodel);
        }

        public bool IsDuplicateCountry(CountryModel cmodel)
        {
            return _repo.IsDuplicateCountry(cmodel);
        }

        public async Task<CountryModel> EditCountryById(long Id)
        {
            return await _repo.EditCountryById(Id);
        }

        public async Task<ResponseModel<object>> DeleteCountry(long Id)
        {
            return await _repo.DeleteCountry(Id);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
