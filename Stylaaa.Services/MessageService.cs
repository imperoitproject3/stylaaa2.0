﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Services
{
    public class MessageService : IDisposable
    {
        private ChatRepository _repo;

        public MessageService()
        {
            _repo = new ChatRepository();
        }
        public List<ChatConversationViewModel> GetChatConversation(ChatConversationGetModel model, string UserID)
        {
            return _repo.GetChatConversation(model, UserID);
        }

        public List<ChatConversationViewModel> GetJobConversation(ChatConversationGetModel model, string UserID)
        {
            return _repo.GetJobConversation(model, UserID);
        }

        public async Task<ResponseModel<ChatConversationViewModel>> Send(MessageSendModel model, string UserID)
        {
            return await _repo.Send(model, UserID);
        }

        public async Task<ResponseModel<object>> SendMessage(MessageSendModel model, string userId)
        {
            return await _repo.SendMessage(model, userId);
        }

        public async Task<ResponseModel<object>> SendMessageFromAdmin(MessageSendModel model, string userId)
        {
            return await _repo.SendMessageFromAdmin(model, userId);
        }

        public async Task<bool> ReadChatMessage(long ChatMessageID, string UserID)
        {
            return await _repo.ReadChatMessage(ChatMessageID, UserID);
        }

        public async Task<int> GetUserUnreadMessageCount(string UserId)
        {
            return await _repo.GetUserUnreadMessageCount(UserId);
        }

        public async Task<List<UnreadMessageCountModel>> GetUserUnreadMessageCount(List<string> listUserIds, string UserId)
        {
            return await _repo.GetUserUnreadMessageCount(listUserIds, UserId);
        }

        public async Task<bool> IsRequested(string userId, string RecieverId)
        {
            return await _repo.IsRequested(userId, RecieverId);
        }

        public async Task<int> CountChat(string userId, string RecieverId)
        {
            return await _repo.CountChat(userId, RecieverId);
        }

        public async Task<ResponseModel<object>> ApproveRequest(string UserId, string ReceiverId)
        {
            return await _repo.ApproveRequest(UserId, ReceiverId);
        }

        public async Task<bool> IsRequestedAccepted(string userId, string RecieverId)
        {
            return await _repo.IsRequestedAccepted(userId, RecieverId);
        }

        public IQueryable<SendMessagesFromAdmin> MessagesListSendByAdmin(Role role)
        {
            return _repo.MessagesListSendByAdmin(role);
        }

        public async Task<ResponseModel<object>> DeleteMessage(long MessageId)
        {
            return await _repo.DeleteMessage(MessageId);
        }

        public async Task<ResponseModel<object>> SendJobRequest(JobRequestModel model, string userId)
        {
            return await _repo.SendJobRequest(model, userId);
        }

        public async Task<ResponseModel<object>> DeleteChatMessage(long ChatId)
        {
            return await _repo.DeleteChatMessage(ChatId);
        }

        public async Task<ResponseModel<object>> DeleteConversastionForJob(string senderId, string receiverId, long JobId)
        {
            return await _repo.DeleteConversastionForJob(senderId, receiverId, JobId);
        }

        public async Task<ResponseModel<object>> DeleteConversationForChat(string senderId, string receiverId)
        {
            return await _repo.DeleteConversationForChat(senderId, receiverId);
        }

        public void Dispose()
        {
            _repo.Dispose();
        }
    }
}
