namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using Stylaaa.Data.Identity;
    using Stylaaa.Core.Helper;
    using Stylaaa.Data.Entities;
    using Stylaaa.Data.Helper;
    using System.Linq;
    using Microsoft.AspNet.Identity.EntityFramework;

    internal sealed class Configuration : DbMigrationsConfiguration<Stylaaa.Data.Identity.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
            AutomaticMigrationDataLossAllowed = true;
        }

        //protected override void Seed(ApplicationDbContext context)
        //{
        //    var userManager = new ApplicationUserManager(new ApplicationStore(context));
        //    var roleManager = new ApplicationRoleManager(new RoleStore<IdentityRole>(context));
        //    //  This method will be called after migrating to the latest version.

        //    //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
        //    //  to avoid creating duplicate seed data.
        //    #region Insert Role If Not Exist
        //    string roleName = Role.User.ToString();
        //    var role = roleManager.FindByNameAsync(roleName).Result;
        //    if (role == null)
        //    {
        //        role = new IdentityRole(roleName);
        //        var roleResult = roleManager.CreateAsync(role).Result;
        //    }

        //    roleName = Role.SuperAdmin.ToString();
        //    role = roleManager.FindByNameAsync(roleName).Result;
        //    if (role == null)
        //    {
        //        role = new IdentityRole(roleName);
        //        var roleresult = roleManager.CreateAsync(role).Result;
        //    }

        //    roleName = Role.Admin.ToString();
        //    role = roleManager.FindByNameAsync(roleName).Result;
        //    if (role == null)
        //    {
        //        role = new IdentityRole(roleName);
        //        var roleresult = roleManager.CreateAsync(role).Result;
        //    }

        //    roleName = Role.Stylist.ToString();
        //    role = roleManager.FindByNameAsync(roleName).Result;
        //    if (role == null)
        //    {
        //        role = new IdentityRole(roleName);
        //        var roleresult = roleManager.CreateAsync(role).Result;
        //    }

        //    //roleName = Role.Visitor.ToString();
        //    //role = roleManager.FindByNameAsync(roleName).Result;
        //    //if (role == null)
        //    //{
        //    //    role = new IdentityRole(roleName);
        //    //    var roleresult = roleManager.CreateAsync(role).Result;
        //    //}
        //    #endregion
        //    context.Countries.AddOrUpdate(
        //    p => p.Id,
        //    new Countries { Id = 1, Name = "India" },
        //    new Countries { Id = 2, Name = "UK" }
        //  );
        //    #region Add Admin User
        //    const string emailId = "admin@stylaa.com";
        //    const string userName = "Admin";
        //    const string password = "Admin@123";
        //    var user = userManager.FindByNameAsync(userName).Result;
        //    if (user == null)
        //    {
        //        user = new ApplicationUser()
        //        {
        //            UserName = userName,
        //            Email = emailId,
        //            Name = "Admin",
        //            GenderId = Gender.Male,
        //            Role = Role.SuperAdmin,
        //            //Address = "Admin",
        //            GeoLocation = Data_Utility.CreatePoint(0, 0),
        //            CountryId=1
        //        };
        //        try
        //        {
        //            var result = userManager.CreateAsync(user, password).Result;
        //            result = userManager.SetLockoutEnabledAsync(user.Id, false).Result;
        //        }
        //        catch (Exception ex)
        //        {

        //            throw ex;
        //        }          
               
        //    }
        //    #endregion

        //    #region Add Admin Role to Admin User
        //    var roleForUser = userManager.GetRolesAsync(user.Id).Result;
        //    if (!roleForUser.Contains(Role.SuperAdmin.ToString()))
        //    {
        //        var result = userManager.AddToRoleAsync(user.Id, Role.SuperAdmin.ToString()).Result;
        //    }
        //    #endregion
        //}
    }
}
