namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationChangesMesssageTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChatBuddy",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ReceiverId = c.String(nullable: false, maxLength: 128),
                        IsRequestAccepted = c.Boolean(nullable: false),
                        ChatId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ChatMessage", t => t.ChatId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.ReceiverId, cascadeDelete: false)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.ReceiverId)
                .Index(t => t.ChatId);
            
            CreateTable(
                "dbo.ChatMessage",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ReceiverId = c.String(nullable: false, maxLength: 128),
                        Message = c.String(),
                        IsRead = c.Boolean(nullable: false),
                        IsImage = c.Boolean(nullable: false),
                        ImageName = c.String(),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.ReceiverId, cascadeDelete: false)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: false)
                .Index(t => t.UserId)
                .Index(t => t.ReceiverId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChatBuddy", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatBuddy", "ReceiverId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatMessage", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatMessage", "ReceiverId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ChatBuddy", "ChatId", "dbo.ChatMessage");
            DropIndex("dbo.ChatMessage", new[] { "ReceiverId" });
            DropIndex("dbo.ChatMessage", new[] { "UserId" });
            DropIndex("dbo.ChatBuddy", new[] { "ChatId" });
            DropIndex("dbo.ChatBuddy", new[] { "ReceiverId" });
            DropIndex("dbo.ChatBuddy", new[] { "UserId" });
            DropTable("dbo.ChatMessage");
            DropTable("dbo.ChatBuddy");
        }
    }
}
