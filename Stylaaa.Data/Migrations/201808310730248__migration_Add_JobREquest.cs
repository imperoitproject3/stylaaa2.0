namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _migration_Add_JobREquest : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChatBuddy", "JobId", "dbo.Jobs");
            DropIndex("dbo.ChatBuddy", new[] { "JobId" });
            CreateTable(
                "dbo.JobRequest",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        JobId = c.Long(nullable: false),
                        StylistId = c.String(maxLength: 128),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Jobs", t => t.JobId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.StylistId)
                .Index(t => t.JobId)
                .Index(t => t.StylistId);
            
            AddColumn("dbo.ChatMessage", "JobId", c => c.Long());
            CreateIndex("dbo.ChatMessage", "JobId");
            AddForeignKey("dbo.ChatMessage", "JobId", "dbo.Jobs", "Id");
            DropColumn("dbo.ChatBuddy", "JobId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChatBuddy", "JobId", c => c.Long());
            DropForeignKey("dbo.JobRequest", "StylistId", "dbo.ApplicationUser");
            DropForeignKey("dbo.JobRequest", "JobId", "dbo.Jobs");
            DropForeignKey("dbo.ChatMessage", "JobId", "dbo.Jobs");
            DropIndex("dbo.JobRequest", new[] { "StylistId" });
            DropIndex("dbo.JobRequest", new[] { "JobId" });
            DropIndex("dbo.ChatMessage", new[] { "JobId" });
            DropColumn("dbo.ChatMessage", "JobId");
            DropTable("dbo.JobRequest");
            CreateIndex("dbo.ChatBuddy", "JobId");
            AddForeignKey("dbo.ChatBuddy", "JobId", "dbo.Jobs", "Id");
        }
    }
}
