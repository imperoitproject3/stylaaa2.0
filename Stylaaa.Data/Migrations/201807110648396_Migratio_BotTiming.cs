namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migratio_BotTiming : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BotTiming",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BotId = c.Long(nullable: false),
                        Timing = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bots", t => t.BotId, cascadeDelete: true)
                .Index(t => t.BotId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BotTiming", "BotId", "dbo.Bots");
            DropIndex("dbo.BotTiming", new[] { "BotId" });
            DropTable("dbo.BotTiming");
        }
    }
}
