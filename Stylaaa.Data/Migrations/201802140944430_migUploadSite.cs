namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class migUploadSite : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.About",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        AboutUs = c.String(nullable: false, unicode: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ApplicationSetting",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Maintenance = c.Boolean(nullable: false),
                        ForceUpdate = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BookingServices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookingId = c.Long(nullable: false),
                        ServiceId = c.Long(nullable: false),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.StylishBookings", t => t.BookingId, cascadeDelete: true)
                .ForeignKey("dbo.CategoryService", t => t.ServiceId, cascadeDelete: true)
                .Index(t => t.BookingId)
                .Index(t => t.ServiceId);
            
            CreateTable(
                "dbo.StylishBookings",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylishId = c.String(maxLength: 128),
                        UserId = c.String(maxLength: 128),
                        ServiceTime = c.DateTime(nullable: false),
                        LocationType = c.Int(nullable: false),
                        TransportCharge = c.Double(nullable: false),
                        Total = c.Double(nullable: false),
                        Notes = c.String(nullable: false, unicode: false),
                        RequestStatus = c.Int(nullable: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId)
                .Index(t => t.StylishId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ApplicationUser",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        GenderId = c.Int(nullable: false),
                        NickName = c.String(),
                        ProfileImageName = c.String(),
                        GeoLocation = c.Geography(),
                        TimeZoneName = c.String(),
                        Location = c.String(maxLength: 8000, unicode: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        Role = c.Int(nullable: false),
                        isProfileCompleted = c.Boolean(nullable: false),
                        StripeAccountId = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.Certificates",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylishId = c.String(maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                        FileName = c.String(maxLength: 100, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId)
                .Index(t => t.StylishId);
            
            CreateTable(
                "dbo.UserClaim",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserLogin",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.UserRole",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.ApplicationUser", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.RoleMaster", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.ServiceCharge",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ServiceId = c.Long(nullable: false),
                        StylishId = c.String(nullable: false, maxLength: 128),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.CategoryService", t => t.ServiceId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId, cascadeDelete: true)
                .Index(t => t.ServiceId)
                .Index(t => t.StylishId);
            
            CreateTable(
                "dbo.CategoryService",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CatId = c.Long(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CatId, cascadeDelete: true)
                .Index(t => t.CatId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.StylishWorkTime",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylishId = c.String(maxLength: 128),
                        Dayofweek = c.Int(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId)
                .Index(t => t.StylishId);
            
            CreateTable(
                "dbo.Comments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PostId = c.Long(nullable: false),
                        Message = c.String(),
                        UserId = c.String(maxLength: 128),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .ForeignKey("dbo.Post", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Post",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylishId = c.String(nullable: false, maxLength: 128),
                        Description = c.String(),
                        Hashtag = c.String(),
                        CategoryId = c.Long(nullable: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId, cascadeDelete: true)
                .Index(t => t.StylishId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.PostTransaction",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PostId = c.Long(nullable: false),
                        FileName = c.String(),
                        IsVideo = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Post", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId);
            
            CreateTable(
                "dbo.Likes",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        PostId = c.Long(nullable: false),
                        UserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .ForeignKey("dbo.Post", t => t.PostId, cascadeDelete: true)
                .Index(t => t.PostId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.ContactUs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Email = c.String(nullable: false),
                        Mobile = c.String(),
                        Message = c.String(nullable: false, unicode: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserFollow",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        FollowingId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .ForeignKey("dbo.ApplicationUser", t => t.FollowingId)
                .Index(t => t.UserId)
                .Index(t => t.FollowingId);
            
            CreateTable(
                "dbo.RoleMaster",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Terms",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        TermsConditions = c.String(nullable: false, unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.UserReview",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylishId = c.String(maxLength: 128),
                        UserId = c.String(maxLength: 128),
                        Rating = c.Int(nullable: false),
                        ReviewDate = c.DateTime(nullable: false),
                        Comment = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId)
                .Index(t => t.StylishId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserReview", "StylishId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserReview", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserRole", "RoleId", "dbo.RoleMaster");
            DropForeignKey("dbo.UserFollow", "FollowingId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserFollow", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Likes", "PostId", "dbo.Post");
            DropForeignKey("dbo.Likes", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.PostTransaction", "PostId", "dbo.Post");
            DropForeignKey("dbo.Comments", "PostId", "dbo.Post");
            DropForeignKey("dbo.Post", "StylishId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Post", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Comments", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.BookingServices", "ServiceId", "dbo.CategoryService");
            DropForeignKey("dbo.StylishBookings", "StylishId", "dbo.ApplicationUser");
            DropForeignKey("dbo.BookingServices", "BookingId", "dbo.StylishBookings");
            DropForeignKey("dbo.StylishBookings", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.StylishWorkTime", "StylishId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ServiceCharge", "StylishId", "dbo.ApplicationUser");
            DropForeignKey("dbo.ServiceCharge", "ServiceId", "dbo.CategoryService");
            DropForeignKey("dbo.CategoryService", "CatId", "dbo.Categories");
            DropForeignKey("dbo.UserRole", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserLogin", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.UserClaim", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.Certificates", "StylishId", "dbo.ApplicationUser");
            DropIndex("dbo.UserReview", new[] { "UserId" });
            DropIndex("dbo.UserReview", new[] { "StylishId" });
            DropIndex("dbo.RoleMaster", "RoleNameIndex");
            DropIndex("dbo.UserFollow", new[] { "FollowingId" });
            DropIndex("dbo.UserFollow", new[] { "UserId" });
            DropIndex("dbo.Likes", new[] { "UserId" });
            DropIndex("dbo.Likes", new[] { "PostId" });
            DropIndex("dbo.PostTransaction", new[] { "PostId" });
            DropIndex("dbo.Post", new[] { "CategoryId" });
            DropIndex("dbo.Post", new[] { "StylishId" });
            DropIndex("dbo.Comments", new[] { "UserId" });
            DropIndex("dbo.Comments", new[] { "PostId" });
            DropIndex("dbo.StylishWorkTime", new[] { "StylishId" });
            DropIndex("dbo.CategoryService", new[] { "CatId" });
            DropIndex("dbo.ServiceCharge", new[] { "StylishId" });
            DropIndex("dbo.ServiceCharge", new[] { "ServiceId" });
            DropIndex("dbo.UserRole", new[] { "RoleId" });
            DropIndex("dbo.UserRole", new[] { "UserId" });
            DropIndex("dbo.UserLogin", new[] { "UserId" });
            DropIndex("dbo.UserClaim", new[] { "UserId" });
            DropIndex("dbo.Certificates", new[] { "StylishId" });
            DropIndex("dbo.ApplicationUser", "UserNameIndex");
            DropIndex("dbo.StylishBookings", new[] { "UserId" });
            DropIndex("dbo.StylishBookings", new[] { "StylishId" });
            DropIndex("dbo.BookingServices", new[] { "ServiceId" });
            DropIndex("dbo.BookingServices", new[] { "BookingId" });
            DropTable("dbo.UserReview");
            DropTable("dbo.Terms");
            DropTable("dbo.RoleMaster");
            DropTable("dbo.UserFollow");
            DropTable("dbo.ContactUs");
            DropTable("dbo.Likes");
            DropTable("dbo.PostTransaction");
            DropTable("dbo.Post");
            DropTable("dbo.Comments");
            DropTable("dbo.StylishWorkTime");
            DropTable("dbo.Categories");
            DropTable("dbo.CategoryService");
            DropTable("dbo.ServiceCharge");
            DropTable("dbo.UserRole");
            DropTable("dbo.UserLogin");
            DropTable("dbo.UserClaim");
            DropTable("dbo.Certificates");
            DropTable("dbo.ApplicationUser");
            DropTable("dbo.StylishBookings");
            DropTable("dbo.BookingServices");
            DropTable("dbo.ApplicationSetting");
            DropTable("dbo.About");
        }
    }
}
