namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _migraionAddBookingTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BookedServices",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BookingId = c.Long(nullable: false),
                        SubCategoryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BookStylist", t => t.BookingId, cascadeDelete: false)
                .ForeignKey("dbo.SubCategories", t => t.SubCategoryId, cascadeDelete: false)
                .Index(t => t.BookingId)
                .Index(t => t.SubCategoryId);
            
            CreateTable(
                "dbo.BookStylist",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Address = c.String(),
                        Email = c.String(),
                        PhoneNumber = c.String(),
                        Gender = c.Int(nullable: false),
                        Notice = c.String(),
                        DateTime = c.DateTime(nullable: false),
                        PaymentMethod = c.Int(nullable: false),
                        Amount = c.Double(nullable: false),
                        CategoryId = c.Long(nullable: false),
                        isPaymentDone = c.Boolean(nullable: false),
                        stripeResponce = c.String(),
                        transactionId = c.String(),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: false)
                .Index(t => t.CategoryId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BookedServices", "SubCategoryId", "dbo.SubCategories");
            DropForeignKey("dbo.BookedServices", "BookingId", "dbo.BookStylist");
            DropForeignKey("dbo.BookStylist", "CategoryId", "dbo.Categories");
            DropIndex("dbo.BookStylist", new[] { "CategoryId" });
            DropIndex("dbo.BookedServices", new[] { "SubCategoryId" });
            DropIndex("dbo.BookedServices", new[] { "BookingId" });
            DropTable("dbo.BookStylist");
            DropTable("dbo.BookedServices");
        }
    }
}
