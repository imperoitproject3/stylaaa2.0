namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class dataTypeChange : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.StylistPremiumTransaction", "paymentStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.StylistPremiumTransaction", "paymentStatus", c => c.String());
        }
    }
}
