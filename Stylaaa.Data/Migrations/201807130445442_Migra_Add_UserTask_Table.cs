namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migra_Add_UserTask_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserTask",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserId = c.String(maxLength: 128),
                        FollowTask = c.Int(nullable: false),
                        CommentTask = c.Int(nullable: false),
                        LikeTask = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.UserId)
                .Index(t => t.UserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.UserTask", "UserId", "dbo.ApplicationUser");
            DropIndex("dbo.UserTask", new[] { "UserId" });
            DropTable("dbo.UserTask");
        }
    }
}
