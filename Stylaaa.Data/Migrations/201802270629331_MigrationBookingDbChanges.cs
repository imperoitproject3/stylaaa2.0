namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class MigrationBookingDbChanges : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.StylishBookings", "UserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.StylishBookings", "StylishId", "dbo.ApplicationUser");
            DropIndex("dbo.StylishBookings", new[] { "StylishId" });
            DropIndex("dbo.StylishBookings", new[] { "UserId" });
            AlterColumn("dbo.StylishBookings", "StylishId", c => c.String(nullable: false, maxLength: 128));
            AlterColumn("dbo.StylishBookings", "UserId", c => c.String(nullable: false, maxLength: 128));
            CreateIndex("dbo.StylishBookings", "StylishId");
            CreateIndex("dbo.StylishBookings", "UserId");
            AddForeignKey("dbo.StylishBookings", "UserId", "dbo.ApplicationUser", "Id", cascadeDelete: false);
            AddForeignKey("dbo.StylishBookings", "StylishId", "dbo.ApplicationUser", "Id", cascadeDelete: false);
        }

        public override void Down()
        {
            DropForeignKey("dbo.StylishBookings", "StylishId", "dbo.ApplicationUser");
            DropForeignKey("dbo.StylishBookings", "UserId", "dbo.ApplicationUser");
            DropIndex("dbo.StylishBookings", new[] { "UserId" });
            DropIndex("dbo.StylishBookings", new[] { "StylishId" });
            AlterColumn("dbo.StylishBookings", "UserId", c => c.String(maxLength: 128));
            AlterColumn("dbo.StylishBookings", "StylishId", c => c.String(maxLength: 128));
            CreateIndex("dbo.StylishBookings", "UserId");
            CreateIndex("dbo.StylishBookings", "StylishId");
            AddForeignKey("dbo.StylishBookings", "StylishId", "dbo.ApplicationUser", "Id");
            AddForeignKey("dbo.StylishBookings", "UserId", "dbo.ApplicationUser", "Id");
        }
    }
}
