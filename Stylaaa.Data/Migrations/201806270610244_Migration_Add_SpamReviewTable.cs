namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration_Add_SpamReviewTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SpamReview",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ReviewId = c.Long(nullable: false),
                        ReasonText = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.UserReview", t => t.ReviewId, cascadeDelete: false)
                .Index(t => t.ReviewId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SpamReview", "ReviewId", "dbo.UserReview");
            DropIndex("dbo.SpamReview", new[] { "ReviewId" });
            DropTable("dbo.SpamReview");
        }
    }
}
