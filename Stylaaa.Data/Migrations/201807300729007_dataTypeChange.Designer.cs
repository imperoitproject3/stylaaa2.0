// <auto-generated />
namespace Stylaaa.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class dataTypeChange : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(dataTypeChange));
        
        string IMigrationMetadata.Id
        {
            get { return "201807300729007_dataTypeChange"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return Resources.GetString("Source"); }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
