namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _migra_change_Job_ZipcodeDatatatype : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Jobs", "ZipCode", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Jobs", "ZipCode", c => c.Double(nullable: false));
        }
    }
}
