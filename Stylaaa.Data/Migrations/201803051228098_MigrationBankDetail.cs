namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MigrationBankDetail : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StylishBankDetail",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylishId = c.String(nullable: false, maxLength: 128),
                        IBAN = c.String(nullable: false),
                        BIC = c.String(nullable: false),
                        BankName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.StylishId, cascadeDelete: true)
                .Index(t => t.StylishId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StylishBankDetail", "StylishId", "dbo.ApplicationUser");
            DropIndex("dbo.StylishBankDetail", new[] { "StylishId" });
            DropTable("dbo.StylishBankDetail");
        }
    }
}
