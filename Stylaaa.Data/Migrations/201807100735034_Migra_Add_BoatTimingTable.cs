namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migra_Add_BoatTimingTable : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Bots", "StartTime");
            DropColumn("dbo.Bots", "EndTime");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Bots", "EndTime", c => c.Time(nullable: false, precision: 7));
            AddColumn("dbo.Bots", "StartTime", c => c.Time(nullable: false, precision: 7));
        }
    }
}
