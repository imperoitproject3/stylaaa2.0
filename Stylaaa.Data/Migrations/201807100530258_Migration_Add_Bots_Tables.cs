namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Migration_Add_Bots_Tables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BotComments",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CategoryId = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.BotFunctions",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BotId = c.Long(nullable: false),
                        FunctionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bots", t => t.BotId, cascadeDelete: true)
                .Index(t => t.BotId);
            
            CreateTable(
                "dbo.Bots",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Title = c.Long(nullable: false),
                        UserType = c.Int(nullable: false),
                        MaxFollow = c.Int(nullable: false),
                        MaxComment = c.Int(nullable: false),
                        StartTime = c.Time(nullable: false, precision: 7),
                        EndTime = c.Time(nullable: false, precision: 7),
                        IsActive = c.Boolean(nullable: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BotMessages",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        MessageTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.BotMessageTran",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BotMessageId = c.Long(nullable: false),
                        LanguageId = c.Int(nullable: false),
                        MessageText = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BotMessages", t => t.BotMessageId, cascadeDelete: true)
                .Index(t => t.BotMessageId);
            
            CreateTable(
                "dbo.BotsCommentLangTran",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BotCommentId = c.Long(nullable: false),
                        LanguageId = c.Int(nullable: false),
                        CommentText = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.BotComments", t => t.BotCommentId, cascadeDelete: true)
                .Index(t => t.BotCommentId);
            
            CreateTable(
                "dbo.BotsDummyUsers",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        BotId = c.Long(nullable: false),
                        DummyUserId = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Bots", t => t.BotId, cascadeDelete: true)
                .ForeignKey("dbo.ApplicationUser", t => t.DummyUserId)
                .Index(t => t.BotId)
                .Index(t => t.DummyUserId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BotsDummyUsers", "DummyUserId", "dbo.ApplicationUser");
            DropForeignKey("dbo.BotsDummyUsers", "BotId", "dbo.Bots");
            DropForeignKey("dbo.BotsCommentLangTran", "BotCommentId", "dbo.BotComments");
            DropForeignKey("dbo.BotMessageTran", "BotMessageId", "dbo.BotMessages");
            DropForeignKey("dbo.BotFunctions", "BotId", "dbo.Bots");
            DropForeignKey("dbo.BotComments", "CategoryId", "dbo.Categories");
            DropIndex("dbo.BotsDummyUsers", new[] { "DummyUserId" });
            DropIndex("dbo.BotsDummyUsers", new[] { "BotId" });
            DropIndex("dbo.BotsCommentLangTran", new[] { "BotCommentId" });
            DropIndex("dbo.BotMessageTran", new[] { "BotMessageId" });
            DropIndex("dbo.BotFunctions", new[] { "BotId" });
            DropIndex("dbo.BotComments", new[] { "CategoryId" });
            DropTable("dbo.BotsDummyUsers");
            DropTable("dbo.BotsCommentLangTran");
            DropTable("dbo.BotMessageTran");
            DropTable("dbo.BotMessages");
            DropTable("dbo.Bots");
            DropTable("dbo.BotFunctions");
            DropTable("dbo.BotComments");
        }
    }
}
