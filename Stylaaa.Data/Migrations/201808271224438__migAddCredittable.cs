namespace Stylaaa.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _migAddCredittable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.StylistCredit",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        StylistId = c.String(maxLength: 128),
                        Credit = c.Int(nullable: false),
                        CreatedDateTimeUTC = c.DateTime(nullable: false),
                        UpdatedDateTimeUTC = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ApplicationUser", t => t.StylistId)
                .Index(t => t.StylistId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.StylistCredit", "StylistId", "dbo.ApplicationUser");
            DropIndex("dbo.StylistCredit", new[] { "StylistId" });
            DropTable("dbo.StylistCredit");
        }
    }
}
