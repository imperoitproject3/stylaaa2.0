﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class SubCategoryRepo : IDisposable
    {
        private ApplicationDbContext _ctx;

        public SubCategoryRepo()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<SubCategoriesModel> IQueryable_GetAllSubCategories(languageType languageid)
        {
            return (from ct in _ctx.SubCategories
                    join ctlng in _ctx.SubCategoriesLanguage.Where(x => x.LanguageId == languageid) on ct.Id equals ctlng.SubCategoryId into HasSubCategory
                    where ct.IsDelete == false
                    select new SubCategoriesModel
                    {
                        Id = ct.Id,
                        Name = HasSubCategory.FirstOrDefault().Name,
                        CategoryName = ct.Categories.CategoryLanguageList.FirstOrDefault().Name,
                        SubCategoryName = HasSubCategory.FirstOrDefault().Name,
                        Price = HasSubCategory.FirstOrDefault().Price,
                        GenderId = HasSubCategory.FirstOrDefault().GenderId
                    }).OrderBy(x => x.Name);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateSubCategory(AdminSubCategoryModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await _ctx.SubCategories.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (data == null)
                {
                    isNew = true;
                    data = new SubCategories();
                }
                //data.CategoryImage = model.ImageName ?? data.CategoryImage;
                data.CategoryId = model.CategoryId;


                if (isNew)
                {
                    List<SubCategoriesLanguage> lstSubCategoryLanguage = new List<SubCategoriesLanguage>();
                    foreach (var item in model.CategoryLanguageModelList)
                    {
                        lstSubCategoryLanguage.Add(new SubCategoriesLanguage
                        {
                            SubCategoryId = data.Id,
                            //CategoryId = item.CategoryId,
                            Name = item.SubCategoryName,
                            LanguageId = item.LanguageId,
                            Price = model.Price,
                            GenderId = model.GenderId
                        });
                    }
                    data.SubCategoryLanguageList = lstSubCategoryLanguage;
                    _ctx.SubCategories.Add(data);
                    await _ctx.SaveChangesAsync();
                }
                else
                {
                    foreach (var item in data.SubCategoryLanguageList)
                    {
                        SubCategoryLanguageModel categoryLanguage = model.CategoryLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                        item.Name = categoryLanguage.SubCategoryName;
                        item.Price = model.Price;
                        item.GenderId = model.GenderId;

                        //item.CategoryId = categoryLanguage.CategoryId;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Category saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }


        public bool isSubCategoryDuplicate(AdminSubCategoryModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.CategoryLanguageModelList)
            {
                isDuplicate = _ctx.SubCategoriesLanguage.Any(x => x.LanguageId == item.LanguageId && x.Name == item.SubCategoryName && x.SubCategoryId != model.Id && x.GenderId == item.GenderId);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<AdminSubCategoryModel> GetSubCategoryDetail(long SubCatrgoryId)
        {
            return await (from c in _ctx.SubCategories
                          where c.Id == SubCatrgoryId
                          select new AdminSubCategoryModel
                          {
                              Id = c.Id,
                              CategoryId = c.CategoryId,
                              Price = c.SubCategoryLanguageList.FirstOrDefault().Price,
                              GenderId = c.SubCategoryLanguageList.FirstOrDefault().GenderId,
                              CategoryLanguageModelList = c.SubCategoryLanguageList.Select(x => new SubCategoryLanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  SubCategoryName = x.Name,
                                  Price = x.Price,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> DeleteSubCategory(long SubCategoryId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                SubCategories data = await _ctx.SubCategories.FirstOrDefaultAsync(x => x.Id == SubCategoryId);
                if (data != null)
                {
                    data.IsDelete = true;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "SubCategory deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Category found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public List<SubCategoryDropdownModel> GetSubCategroies(languageType languageId, List<long> Categories, LandingGender gender)
        {
            return (from ct in _ctx.SubCategories
                    where Categories.Contains(ct.CategoryId)
                    && ct.IsDelete == false
                    && ct.SubCategoryLanguageList.Any(x => x.LanguageId == languageId && x.GenderId == gender)
                    let sub = ct.SubCategoryLanguageList.Where(y => y.LanguageId == languageId).FirstOrDefault()
                    select new SubCategoryDropdownModel
                    {
                        Id = ct.Id,
                        //Name=sub.Name.Where(x=>)
                        Name = sub.Name + " (" + sub.Price + "€)"
                    }).ToList();

            //select new SubCategoryDropdownModel
            //{
            //    Id = ct.Id,
            //    Name = hasCategory.FirstOrDefault().Name + "  (€" + hasCategory.FirstOrDefault().Price.ToString() + ")",
            //}).OrderBy(x => x.Name);
        }

        public async Task<double> PriceBySubCategory(List<long> SubCatIds)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            double price = 0;
            double totalprice = 0;
            try
            {
                foreach (var itm in SubCatIds)
                {
                    SubCategories subcat = await _ctx.SubCategories.FirstOrDefaultAsync(x => x.Id == itm);
                    if (subcat != null)
                    {
                        price = subcat.SubCategoryLanguageList.FirstOrDefault().Price;
                        if (price > 0)
                        {
                            totalprice += price;
                        }
                    }
                }

                mResult.Result = totalprice;

            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return totalprice;
        }

        public async Task<List<SubcategoryList>> PriceListBySubCategory(List<long> SubCatIds)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }
            ResponseModel<object> mResult = new ResponseModel<object>();
            double price = 0;
            List<SubcategoryList> pricesList = new List<SubcategoryList>();
            try
            {
                foreach (var itm in SubCatIds)
                {
                    SubCategories subcat = await _ctx.SubCategories.FirstOrDefaultAsync(x => x.Id == itm);
                    if (subcat != null)
                    {
                        price = subcat.SubCategoryLanguageList.FirstOrDefault().Price;
                        if (price > 0)
                        {
                            pricesList.Add(new SubcategoryList() { Price = price, Name = subcat.SubCategoryLanguageList.Where(x => x.LanguageId == lng).FirstOrDefault().Name });
                        }
                    }
                }
                mResult.Result = price;

            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return pricesList;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
