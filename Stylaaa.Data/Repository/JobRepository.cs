﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


namespace Stylaaa.Data.Repository
{
    public class JobRepository
    {
        private ApplicationDbContext _ctx;
        private UserRepository _user;
        private CoreUserRepository _coreuser;
        private CategoryRepository _catrepo;

        public JobRepository()
        {
            _catrepo = new CategoryRepository();
            _ctx = new ApplicationDbContext();
            _user = new UserRepository();
            _coreuser = new CoreUserRepository();
        }

        public async Task<List<ViewJobListModel>> GetJobList(jobFilterModel model)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }
            if (String.IsNullOrWhiteSpace(CurrentLang))
            {
                if (!String.IsNullOrWhiteSpace(model.UserId))
                {
                    lng = _ctx.Users.Where(x => x.Id == model.UserId).Select(x => x.Language).FirstOrDefault();
                }
                else
                {
                    lng = _ctx.Users.Where(x => x.Id == model.UserId).Select(x => x.Language).FirstOrDefault();
                }
            }
            List<ViewJobListModel> JobList = new List<ViewJobListModel>();
            JobList = await (from job in _ctx.Jobs
                             where job.JobStatus == JobStatus.Activated && job.MarkAsDelete == false
                             select new ViewJobListModel
                             {
                                 JobId = job.Id,
                                 UserId = job.UserId,
                                 Title = job.JobTitle,
                                 Price = job.Price,
                                 Zipcode = job.ZipCode,
                                 Country = job.Country,
                                 IsAbleToSend = _ctx.JobRequests.Any(x => x.StylistId == model.UserId && x.JobId == job.Id),
                                 CreatedDate = job.CreatedDateTimeUTC,
                                 JobCategoryList = job.JobCategories.Where(x => x.JobId == job.Id).Select(x => new JobCategoryModel
                                 {
                                     CategoryName = x.Categories.CategoryLanguageList.Where(y => y.LanguageId == lng).FirstOrDefault().Name,
                                     Id = x.Id,
                                     Selected = false
                                 }).ToList(),
                                 JobFilesList = job.JobFiles.Where(x => x.JobId == job.Id).Select(x => new JobFilesModel
                                 {
                                     Id = x.Id,
                                     FileName = x.FileName,
                                     IsVideo = x.IsVideo
                                 }).ToList()
                             }).OrderByDescending(x => x.CreatedDate).Skip(((model.JobPageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();
            return JobList;
        }

        public async Task<GetUserJobs> JoblistByUser(PaginationModel model)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }
            if (String.IsNullOrWhiteSpace(CurrentLang))
            {
                if (!String.IsNullOrWhiteSpace(model.UserId))
                {
                    lng = _ctx.Users.Where(x => x.Id == model.UserId).Select(x => x.Language).FirstOrDefault();
                }
                else
                {
                    lng = _ctx.Users.Where(x => x.Id == model.UserId).Select(x => x.Language).FirstOrDefault();
                }
            }
            GetUserJobs joblistmodel = new GetUserJobs();
            if (model.PageIndex > 0)
            {
                var joblist = await (from job in _ctx.Jobs
                                     where job.UserId == model.UserId && job.MarkAsDelete == false
                                     select new GetJobListByUser
                                     {
                                         JobId = job.Id,
                                         UserId = job.UserId,
                                         Title = job.JobTitle,
                                         Price = job.Price,
                                         Zipcode = job.ZipCode,
                                         Country = job.Country,
                                         CreatedDate = job.CreatedDateTimeUTC,
                                         Status = job.JobStatus,
                                         JobCategoryList = job.JobCategories.Where(x => x.JobId == job.Id).Select(x => new JobCategoryModel
                                         {
                                             CategoryName = x.Categories.CategoryLanguageList.Where(y => y.LanguageId == lng).FirstOrDefault().Name,
                                             Id = x.Id,
                                             Selected = false
                                         }).ToList(),
                                         JobFilesList = job.JobFiles.Where(x => x.JobId == job.Id).Select(x => new JobFilesModel
                                         {
                                             Id = x.Id,
                                             FileName = x.FileName,
                                             IsVideo = x.IsVideo
                                         }).ToList()
                                     }).OrderByDescending(x => x.CreatedDate).Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();

                joblistmodel.UserJobList = joblist;

                joblistmodel.jobCategoryList = _catrepo.GetJobCategoryList(_user.GetLanguage(model.UserId));

                return joblistmodel;
            }
            else
            {
                GetUserJobs modelData = new GetUserJobs();
                var query = await (from job in _ctx.Jobs
                                   where job.UserId == model.UserId
                                   select new GetJobListByUser
                                   {
                                       JobId = job.Id,
                                       UserId = job.UserId
                                   })
                              .ToListAsync();

                modelData.UserJobList = query.ToList();
                return modelData;
            }

        }

        public async Task<ResponseModel<object>> AddEditJob(AddJobModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Jobs ObjJob = await _ctx.Jobs.FirstOrDefaultAsync(x => x.Id == model.Id);
                bool isNew = false;
                if (ObjJob == null)
                {
                    ObjJob = new Jobs();
                    isNew = true;
                };
                ObjJob.UserId = model.UserId;
                ObjJob.JobTitle = model.Title;
                ObjJob.Price = model.Price;
                ObjJob.Country = model.Country;
                ObjJob.ZipCode = model.ZipCode;
                ObjJob.JobStatus = JobStatus.Activated;
                if (!isNew)
                {
                    _ctx.JobCategories.RemoveRange(ObjJob.JobCategories);
                    await _ctx.SaveChangesAsync();
                }
                ObjJob.JobCategories = (from fl in model.JobCategoryList
                                        where fl.Selected == true
                                        select new JobCategories
                                        {
                                            JobId = ObjJob.Id,
                                            CategoryId = fl.Id
                                        }).ToList();
                if (!isNew)
                {
                    _ctx.JobFiles.RemoveRange(ObjJob.JobFiles);
                    await _ctx.SaveChangesAsync();
                }
                if (model.JobFilesNamesArray != null)
                {
                    ObjJob.JobFiles = (from fl in model.JobFilesNamesArray
                                       select new JobFiles
                                       {
                                           JobId = ObjJob.Id,
                                           FileName = fl,
                                           IsVideo = checkVideoFile(fl)
                                       }).ToList();
                }

                if (isNew)
                {
                    _ctx.Jobs.Add(ObjJob);
                }
                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    if (isNew)
                    {
                        mResult.Message = Resource.jobSuccessUpdate;
                        bool send = await SendJobEmail(model.JobCategoryList, model.Title);
                    }
                    else
                    {
                        mResult.Message = Resource.EditPostSuccess;
                    }
                }
                mResult.Result = new { };
                mResult.Status = ResponseStatus.Success;
            }
            catch (DbEntityValidationException ex)
            {

            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<bool> SendJobEmail(List<JobCategoryModel> CatList, string jobtitle)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var list = CatList.Where(x => x.Selected == true).Select(x => x.Id).ToList();

            var stylistlist = _ctx.StylistCategory.Where(x => list.Contains(x.CategoryId)).ToList().Distinct();
            //mResult.Message = "good";
            foreach (var u in stylistlist)
            {
                //var User = _coreuser.GetUserDetail(u.StylistId);
                try
                {
                    string subject = "New Job for you-" + jobtitle;
                    var emailTemplate = EmailTemplate.JobAddedForStylist;
                    if (u.Stylist.Language == languageType.German)
                    {
                        emailTemplate = EmailTemplate.JobAddedForStylistDe;
                        subject = "Neuer Job für Dich-" + jobtitle;
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", u.Stylist.Name);
                    //dicPlaceholders.Add("{{password}}", password);
                    dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                    dicPlaceholders.Add("{{link}}", "https://www.stylaaa.com/Home/Index");

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(subject);
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = u.Stylist.Email;
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                    // mResult.Message = Resource.forgotpasswordSendLinkSuccess;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            return true;

        }

        public bool checkVideoFile(string filename)
        {
            bool isVideo = false;
            foreach (string x in GlobalConfig.mediaExtensions)
            {
                if (filename.Contains(x))
                {
                    isVideo = true;
                    break;
                }
            }
            return isVideo;
        }

        public async Task<ResponseModel<object>> DeleteJob(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Jobs objJob = _ctx.Jobs.FirstOrDefault(x => x.Id == id);
                if (objJob == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = "Job Not Found";
                    mResult.Status = ResponseStatus.Failed;
                }
                else
                {
                    objJob.JobStatus = JobStatus.Deactivate;
                    objJob.MarkAsDelete = true;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Post gelöscht";
                    //var existjobfiles = _ctx.JobFiles.Where(x => x.JobId == id).ToList();

                    //foreach (var item in existjobfiles)
                    //{
                    //    string FilePath = GlobalConfig.JobImagePath + item.FileName;
                    //    if (System.IO.File.Exists(FilePath))
                    //    {
                    //        System.IO.File.Delete(FilePath);
                    //    }
                    //}
                    //_ctx.JobFiles.RemoveRange(existjobfiles);

                    //_ctx.JobCategories.RemoveRange(_ctx.JobCategories.Where(x => x.JobId == id).ToList());

                    //var notification = _ctx.Notification.Where(x => x.JobId == id).ToList();
                    //if (notification != null)
                    //{
                    //    _ctx.Notification.RemoveRange(notification);
                    //}
                    //// _ctx.Notification.RemoveRange(_ctx.Notification.Where(x => x.jo == id));

                    //_ctx.Jobs.Remove(objJob);
                }
                //long objId = await _ctx.SaveChangesAsync();
                //if (objId > 0)
                //{
                //    mResult.Status = ResponseStatus.Success;
                //    mResult.Message = "Post gelöscht";
                //}
                //else
                //{
                //    mResult.Result = new object { };
                //    mResult.Status = ResponseStatus.Failed;
                //    mResult.Message = Resource.deletePostFailed;
                //}
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<AddJobModel> GetJobdetailEdit(languageType languageid, long Id, string UserId)
        {
            AddJobModel result = await (from job in _ctx.Jobs
                                        where job.Id == Id
                                        && job.UserId == UserId
                                        select new AddJobModel
                                        {
                                            Id = job.Id,
                                            UserId = job.UserId,
                                            Title = job.JobTitle,
                                            Price = job.Price,
                                            ZipCode = job.ZipCode,
                                            Country = job.Country,

                                            JobCategoryList = (from cat in _ctx.Categories
                                                               join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageid) on cat.Id equals ctlng.CategoryId into HasCategory
                                                               let jobcat = _ctx.JobCategories.Where(x => x.JobId == Id)
                                                               select new JobCategoryModel
                                                               {
                                                                   Id = cat.Id,
                                                                   CategoryName = HasCategory.FirstOrDefault().Name,
                                                                   Selected = (jobcat.Where(x => x.CategoryId == cat.Id).Any() ? true : false)
                                                               }).OrderBy(x => x.CategoryName).ToList(),
                                            JobFiles = job.JobFiles.Where(x => x.JobId == Id).Select(x => new JobFilesModel
                                            {
                                                JobId = job.Id,
                                                FileName = x.FileName,
                                                IsVideo = x.IsVideo,
                                            }).ToList(),
                                        }).FirstOrDefaultAsync();
            if (result != null)
            {
                //result.HashtagsArray = _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).Any() ? _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).ToArray() : null;
                //result.HiddenHashtags = String.Join(",", _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).ToArray());
                result.JobFilesNamesArray = _ctx.JobFiles.Where(x => x.JobId == Id).Select(x => x.FileName).ToArray();
                result.HiddenJobFileName = String.Join(",", _ctx.JobFiles.Where(x => x.JobId == Id).Select(x => x.FileName).ToArray());
                result.HiddenJobFilePath = String.Join(",", _ctx.JobFiles.Where(x => x.JobId == Id).Select(x => GlobalConfig.JobImageUrl + x.FileName).ToArray());

                //result.HashtagsArray = new[] { String.Join(",", _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).ToArray()) };
            }
            return result;
        }

        public async Task<ResponseModel<object>> JobActiveToggle(long JobId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var job = await _ctx.Jobs.FirstOrDefaultAsync(x => x.Id == JobId);
            if (job != null)
            {
                if (job.JobStatus == JobStatus.Activated)
                {
                    job.JobStatus = JobStatus.Deactivate;
                }
                else if (job.JobStatus == JobStatus.Deactivate)
                {
                    job.JobStatus = JobStatus.Activated;
                }
                else
                {
                    job.JobStatus = JobStatus.Activated;
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                if (job.JobStatus == JobStatus.Activated)
                {
                    mResult.Message = Resource.JobActivatedmsg;
                }
                else if (job.JobStatus == JobStatus.Deactivate)
                {
                    mResult.Message = "job status deactivated";
                }
                else
                {
                    mResult.Message = Resource.JobActivatedmsg;
                }
                mResult.Result = job.JobStatus;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "job not found";
            }
            return mResult;
        }

        public IQueryable<GetAllJobList> GetJobListForAdmin()
        {
            return (from pst in _ctx.Jobs
                    where pst.MarkAsDelete == false
                    //join hastag in _ctx.PostHashtags on pst.Id equals hastag.PostId
                    select new GetAllJobList
                    {
                        JobId = pst.Id,
                        UserId = pst.UserId,
                        Title = pst.JobTitle,
                        ProfileImageNAme = pst.JobBy.ProfileImageName,
                        CreatedDate = pst.CreatedDateTimeUTC,
                        Amount = pst.Price,
                        Country = pst.Country,
                        Zipcode = pst.ZipCode,
                        Name = pst.JobBy.Name,
                        JobStatus = pst.JobStatus.ToString()

                        //jobCategoryModel = pst.Select(x => new JobCategoryModel
                        //{

                        //}).ToList()
                    }).OrderBy(x => x.CreatedDate);
        }
    }
}
