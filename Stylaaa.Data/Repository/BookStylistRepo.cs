﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class BookStylistRepo
    {
        private ApplicationDbContext _ctx;

        public BookStylistRepo()
        {
            _ctx = new ApplicationDbContext();
        }

        public async Task<ResponseModel<object>> AddBookStylist(LandingPageModel model)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }

            List<long> SubcatIdList = model.SubCategoryId.Select(Int64.Parse).ToList();
            List<long> Categories = model.CategoryId.Select(Int64.Parse).ToList();


            ResponseModel<object> mResult = new ResponseModel<object>();
            BookStylist ObjBook = new BookStylist();
            ObjBook.Address = model.Address;
            ObjBook.Email = model.Email;
            ObjBook.Name = model.Name;
            ObjBook.Notice = model.Notice;
            //ObjBook.CategoryId = model.CategoryId;
            ObjBook.Amount = model.TotalPrice;
            ObjBook.PhoneNumber = model.PhoneNumber;
            ObjBook.Gender = model.Gender;
            ObjBook.DateTime = model.DateTime;
            ObjBook.isPaymentDone = true;
            ObjBook.PaymentMethod = model.PaymentType;
            ObjBook.transactionId = string.Empty;
            ObjBook.stripeResponce = string.Empty;

            ObjBook.BookedCategories = (from s in Categories
                                        select new BookedCategories
                                        {
                                            BookingId = ObjBook.Id,
                                            CategoryId = s
                                        }).ToList();

            ObjBook.BookedServices = (from bs in SubcatIdList
                                      select new BookedServices
                                      {
                                          BookingId = ObjBook.Id,
                                          SubCategoryId = bs
                                      }).ToList();

            _ctx.BookStylist.Add(ObjBook);
            int id = 0;
            try
            {
                id = await _ctx.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                string abc = ex.Message;
            }           
            if (id > 0)
            {
                mResult.Message = Resource.BookingConfirmed;
                mResult.Status = ResponseStatus.Success;
                try
                {
                    string subject = "Stylaaa Booking Confirmation";
                    var emailTemplate = EmailTemplate.StylistBooking;
                    if (lng == languageType.German)
                    {
                        emailTemplate = EmailTemplate.StylistBookingDe;
                        subject = "Stylaaa Buchung bestätigt";
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", model.Name);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(subject);
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = model.Email;
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                try
                {
                    string subject = "New Booking Received";
                    var emailTemplate = EmailTemplate.BookingRequest;
                    if (lng == languageType.German)
                    {
                        emailTemplate = EmailTemplate.BookingRequestDe;
                        subject = "New Booking Received";
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", model.Name);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(subject);
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = "hello@stylaaa.com";
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                mResult.Message = Resource.BookingFailed;
                mResult.Status = ResponseStatus.Failed;
            }

            return mResult;
            //ObjBook.BookedServices = (from obj in model.SubCategoryId
            //                          select new BookedServices
            //                          {
            //                              BookingId=ObjBook.Id,
            //                              SubCategoryId=model.SubCategoryId
            //                          }).tolist();


        }


        public async Task<ResponseModel<object>> AddBookStylistWithPayment(LandingPageModel model, string tranId, ChargeStatus status, string striperesponse)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }

            List<long> SubcatIdList = model.SubCategoryId.Select(Int64.Parse).ToList();
            List<long> Categories = model.CategoryId.Select(Int64.Parse).ToList();
           
            BookStylist ObjBook = new BookStylist();
            ObjBook.Address = model.Address;
            ObjBook.Email = model.Email;
            ObjBook.Name = model.Name;
            ObjBook.Notice = model.Notice;
            //ObjBook.CategoryId = model.CategoryId;
            ObjBook.Amount = model.TotalPrice;
            ObjBook.PhoneNumber = model.PhoneNumber;
            ObjBook.Gender = model.Gender;
            ObjBook.DateTime = model.DateTime;
            ObjBook.PaymentMethod = model.PaymentType;
            ObjBook.isPaymentDone = true;
            ObjBook.PaymentStatus = status;
            ObjBook.transactionId = tranId != string.Empty ? tranId : string.Empty; ;
            ObjBook.stripeResponce = striperesponse != string.Empty ? striperesponse : string.Empty;

            ObjBook.BookedCategories = (from s in Categories
                                        select new BookedCategories
                                        {
                                            BookingId = ObjBook.Id,
                                            CategoryId = s
                                        }).ToList();

            ObjBook.BookedServices = (from bs in SubcatIdList
                                      select new BookedServices
                                      {
                                          BookingId = ObjBook.Id,
                                          SubCategoryId = bs
                                      }).ToList();



            _ctx.BookStylist.Add(ObjBook);
            int id = await _ctx.SaveChangesAsync();
            if (id > 0)
            {
                try
                {
                    string subject = "Stylist Booking Confirmed";
                    var emailTemplate = EmailTemplate.StylistBooking;
                    if (lng == languageType.German)
                    {
                        emailTemplate = EmailTemplate.StylistBookingDe;
                        subject = "Stylist Booking Confirmed";
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", model.Name);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(subject);
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = model.Email;
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                }
                catch (Exception ex)
                {
                    throw ex;
                }


                try
                {
                    string subject = "New Booking Received";
                    var emailTemplate = EmailTemplate.BookingRequest;
                    if (lng == languageType.German)
                    {
                        emailTemplate = EmailTemplate.BookingRequestDe;
                        subject = "New Booking Received";
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", model.Name);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(subject);
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = "hello@stylaaa.com";
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                mResult.Message = " Your Booking Confirmed";
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Message = "Failed to Book Stylist";
                mResult.Status = ResponseStatus.Failed;
            }

            return mResult;            

        }

        public IQueryable<AdminViewBookingModel> GetBookingDetails()
        {
            return (from book in _ctx.BookStylist
                    where book.IsDeleted == false
                    select new AdminViewBookingModel
                    {
                        Id = book.Id,
                        DateTime = book.DateTime,
                        Email = book.Email,
                        Name = book.Name,
                        PhoneNumber = book.PhoneNumber,
                        Address = book.Address,
                        Gender = book.Gender.ToString(),
                        //CategoryName = book.BookedCategories.CategoryLanguageList.FirstOrDefault().Name,
                       // SubCategoryName = sub.SubCategories.SubCategoryLanguageList.FirstOrDefault().Name,
                        Price = book.Amount,
                        RoadAccessCost = 5,
                        Total = book.Amount + 5,
                        PaymentType = book.PaymentMethod == StripeType.none ? "cash Payment" : book.PaymentMethod.ToString(),
                        CategoryList = book.BookedCategories.Select(x => new AdminCategoryLIst
                        {
                            Id = x.Id,
                            CategoryName = x.Categories.CategoryLanguageList.FirstOrDefault().Name
                        }).ToList(),
                        SubCategoryList = book.BookedServices.Select(x => new AdminSubcategoryList
                        {
                            Id = x.Id,
                            SubCategoryName = x.SubCategories.SubCategoryLanguageList.FirstOrDefault().Name
                        }).ToList(),
                        Notice = book.Notice
                    }).OrderByDescending(x => x.Id);
        }


        public async Task<ResponseModel<object>> DeleteBookingById(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            BookStylist stylist = _ctx.BookStylist.FirstOrDefault(x => x.Id == Id);
            if (stylist != null)
            {
                //var StylistService = await _ctx.BookedServices.Where(x => x.Id == Id).ToListAsync();
                //if (StylistService != null)
                //{
                //    _ctx.BookedServices.RemoveRange(StylistService);
                //}
                stylist.IsDeleted = true;
                //_ctx.BookStylist.Remove(stylist);
                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    mResult.Message = "booking deleted successfully!";
                    mResult.Status = ResponseStatus.Success;
                }
                else
                {
                    mResult.Message = "Failed to delete Booking Record"; ;
                    mResult.Status = ResponseStatus.Failed;
                }
            }
            else
            {
                mResult.Message = "No record found"; ;
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }


        public async Task<List<LandingPageStylistList>> GetTopStylist()
        {
            List<LandingPageStylistList> stylistlist = new List<LandingPageStylistList>();
            List<string> stylist = new List<string>();
            stylist.Add("Diksi.Beautyy");
            stylist.Add("Sena_Bektas");
            stylist.Add("jasminstalzer");
            stylist.Add("kölnmakeupartist");
            stylistlist = await (from st in _ctx.Users
                                 where st.Role == Role.Stylist && stylist.Contains(st.UserName)/*st.UserName.Contains(stylist.ToString())*/
                                 select new LandingPageStylistList
                                 {
                                     Sid = st.Id,
                                     Name = st.Name,
                                     Experiance = st.Experiance,
                                     ProfileImage = st.ProfileImageName,
                                     UserName = st.UserName
                                 }).OrderBy(x => x.Experiance).Take(4).ToListAsync();

            if (stylistlist != null)
            {
                foreach (var item in stylistlist)
                {
                    int year = 0;
                    double month = 0;
                    DateTime current = DateTime.UtcNow;
                    DateTime dt = Convert.ToDateTime(item.Experiance);
                    if (item.Experiance != null)
                    {
                        TimeSpan ts = current - dt;
                        year = ts.Days / 364;
                        month = (ts.Days % 365) / 31;
                    }
                    item.Year = year;
                    //item.Months = month + 1;
                }
            }

            return stylistlist;
        }
    }
}
