﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public static class IdentityGetDetail
    {
        public static ApplicationDbContext _ctx = new ApplicationDbContext();

        public static string GetEmailAdress(this IIdentity identity)
        {
            _ctx = new ApplicationDbContext();

            var userId = identity.GetUserId();
            var user = _ctx.Users.FirstOrDefault(u => u.Id == userId);
            return user.Email;
        }

        public static void Dispose()
        {
            throw new NotImplementedException();
        }
    }
}
