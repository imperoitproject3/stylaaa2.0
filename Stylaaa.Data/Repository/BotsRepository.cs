﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class BotsRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        private ChatRepository _chatrepo;

        public BotsRepository()
        {
            _ctx = new ApplicationDbContext();
            _chatrepo = new ChatRepository();
        }

        public async Task<List<ViewDummyUserList>> getAllDummyUsers()
        {
            return await (from du in _ctx.Users
                          where du.IsDummy == true
                          select new ViewDummyUserList
                          {
                              DummyId = du.Id,
                              Name = du.Name,
                              Email = du.Email,
                              ProfileImage = du.ProfileImageName
                          }).ToListAsync();
        }

        public async Task<ManageBoatModel> ManageBoat()
        {
            return await (from bt in _ctx.AppSetting
                          select new ManageBoatModel
                          {
                              IsFollow = bt.IsFollow,
                              IsLike = bt.IsLike,
                              IsComment = bt.IsComment,
                              IsMessage = bt.IsMessage
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> AddBot(AddBotModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Bots bot = await _ctx.Bots.FirstOrDefaultAsync(x => x.Title == model.Title);
                if (bot == null)
                {
                    bot = new Bots
                    {
                        Title = model.Title,
                        MaxComment = model.maxComment,
                        MaxFollow = model.MaxFollow,
                        IsActive = false,
                        UserType = model.UserType,
                        CreatedDateTimeUTC = Utility.GetSystemDateTimeUTC(),
                    };
                    _ctx.Bots.Add(bot);

                    foreach (var itm in model.Functions)
                    {
                        var botfun = (from bf in model.Functions
                                      select new BotFunctions
                                      {
                                          BotId = bot.Id,
                                          FunctionId = itm
                                      }).FirstOrDefault();
                        _ctx.BotFunctions.Add(botfun);
                    }

                    foreach (var item in model.Timing)
                    {
                        var botfun = (from bf in model.Timing
                                      select new BotTiming
                                      {
                                          BotId = bot.Id,
                                          Timing = item
                                      }).FirstOrDefault();
                        _ctx.BotTiming.Add(botfun);
                    }

                    foreach (var item in model.DummyUser)
                    {
                        var Dummyuser = (from bf in model.DummyUser
                                         select new BotsDummyUsers
                                         {
                                             BotId = bot.Id,
                                             DummyUserId = item
                                         }).FirstOrDefault();
                        _ctx.BotsDummyUsers.Add(Dummyuser);
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "already added bot with same name";
                    return mResult;
                }
                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "Bot added successfully";
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "failed to add bot";
                }
            }
            catch (Exception)
            {

                throw;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteBot(long BotId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var bots = await _ctx.Bots.FirstOrDefaultAsync(x => x.Id == BotId);
            if (bots != null)
            {
                var botTimes = await _ctx.BotTiming.Where(x => x.BotId == BotId).ToListAsync();
                _ctx.BotTiming.RemoveRange(botTimes);

                var botfunction = await _ctx.BotFunctions.Where(x => x.BotId == BotId).ToListAsync();
                _ctx.BotFunctions.RemoveRange(botfunction);

                var botdummyuser = await _ctx.BotsDummyUsers.Where(x => x.BotId == BotId).ToListAsync();
                _ctx.BotsDummyUsers.RemoveRange(botdummyuser);

                _ctx.Bots.Remove(bots);
                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    mResult.Message = "bot removed";
                    mResult.Status = ResponseStatus.Success;
                }
            }
            else
            {
                mResult.Message = "bot not fount";
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }

        public async Task<ResponseModel<bool>> BotActiveToggle(long BotId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var bot = await _ctx.Bots.FirstOrDefaultAsync(x => x.Id == BotId);
            if (bot != null)
            {
                bot.IsActive = !bot.IsActive;
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                if (bot.IsActive)
                {
                    mResult.Message = Resource.ActiveSuccessText;
                }
                else
                {
                    mResult.Message = Resource.DeactiveAccountSuccess;
                }
                mResult.Result = bot.IsActive;
            }
            else
            {
                mResult.Message = Resource.useNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<bool>> ActiveFollowBot(int id)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            if (id > 0)
            {
                var bots = await _ctx.AppSetting.FirstOrDefaultAsync();
                if (bots != null)
                {
                    if (id == 1)
                    {
                        bots.IsFollow = !bots.IsFollow;
                        mResult.Result = bots.IsFollow;
                        await _ctx.SaveChangesAsync();
                    }
                    else if (id == 2)
                    {
                        bots.IsLike = !bots.IsLike;
                        mResult.Result = bots.IsLike;
                        await _ctx.SaveChangesAsync();
                    }
                    else if (id == 3)
                    {
                        bots.IsComment = !bots.IsComment;
                        mResult.Result = bots.IsComment;
                        await _ctx.SaveChangesAsync();
                    }
                    else
                    {
                        bots.IsMessage = !bots.IsMessage;
                        mResult.Result = bots.IsMessage;
                        await _ctx.SaveChangesAsync();
                    }
                }
            }
            return mResult;
        }

        public async Task<AddBotModel> BotById(long Id)
        {
            return await (from bot in _ctx.Bots
                          join botuser in _ctx.BotsDummyUsers on bot.Id equals botuser.BotId into hasuser
                          join time in _ctx.BotTiming on bot.Id equals time.BotId into hasTime
                          join botfun in _ctx.BotFunctions on bot.Id equals botfun.BotId into hasFun
                          where bot.Id == Id
                          select new AddBotModel
                          {
                              BotId = bot.Id,
                              Title = bot.Title,
                              MaxFollow = bot.MaxFollow,
                              maxComment = bot.MaxComment,
                              Timing = hasTime.Select(x => x.Timing).ToList(),
                              Functions = hasFun.Select(x => x.FunctionId).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<List<DummyUserList>> GetDummyUserList()
        {
            return await (from ct in _ctx.Users
                          where ct.IsDummy == true
                          select new DummyUserList
                          {
                              Id = ct.Id,
                              Name = ct.Name,
                              Image = GlobalConfig.UserImageUrl + ct.ProfileImageName
                          }).OrderBy(x => x.Name).ToListAsync();
        }

        public async Task<List<DummyUserList>> GetDummyUserListEdit()
        {
            return await (from ct in _ctx.Users
                          join botDummy in _ctx.BotsDummyUsers on ct.Id equals botDummy.DummyUserId into HasCategory
                          where ct.IsDummy == true
                          select new DummyUserList
                          {
                              Id = ct.Id,
                              Name = ct.Name,
                              Image = GlobalConfig.UserImageUrl + ct.ProfileImageName,
                              IsChecked = (HasCategory.Any() ? true : false)
                          }).OrderBy(x => x.Name).ToListAsync();
        }

        public IQueryable<ViewBots> ViewBots()
        {
            return (from bot in _ctx.Bots
                    join Botfun in _ctx.BotFunctions on bot.Id equals Botfun.BotId into function
                    join botTime in _ctx.BotTiming on bot.Id equals botTime.BotId into time
                    select new ViewBots
                    {
                        BotId = bot.Id,
                        BotTitle = bot.Title,
                        MaxCommentCount = bot.MaxComment,
                        MaxFollowCunt = bot.MaxFollow,
                        Timing = time.Select(x => x.Timing).ToList(),
                        Functions = function.Select(x => x.FunctionId).ToList(),
                        IsActive = bot.IsActive
                    }).OrderByDescending(x => x.BotTitle);
        }

        public int generateRandomDummyUserCount()
        {
            var DummyCount = _ctx.Users.Count(x => x.IsDummy == true);
            Random rnd = new Random();
            var number = (int)(rnd.Next(1, DummyCount));
            //number = 3;
            return number;
        }

        public async Task<ResponseModel<bool>> FollowSchedular()
        {
            //DateTime RegisterTime = Utility.GetSystemDateTimeUTC().AddHours(-2);
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            try
            {
                var dummyUser = await _ctx.Users.Where(x => x.IsDummy).ToListAsync();

                var StylistList = await (from user in _ctx.Users
                                         where user.Role == Role.Stylist || user.Role == Role.User && user.IsDummy == false
                                         //&& user.CreatedDateTimeUTC <= RegisterTime
                                         select user).ToListAsync();

                foreach (var stylst in StylistList)
                {
                    var objStylist = await _ctx.UserTask.FirstOrDefaultAsync(x => x.UserId == stylst.Id);
                    if (objStylist == null)
                    {
                        objStylist = await AddTask(stylst.Id);
                    }

                    //int task = 0;
                    //int maxTask = Math.Max(objStylist.FollowTask, objStylist.LikeTask);
                    //int FollowCount = objStylist.CommentTask;

                    //if (maxTask > FollowCount)
                    //    task = maxTask;
                    //else
                    //    task = FollowCount;
                    var followtask = objStylist.FollowTask;
                    var followingUser = _ctx.Followers.Count(x => x.FollowingId == stylst.Id && x.FollowedBy.IsDummy == true);
                    if (followtask != followingUser)
                    {
                        var followingDummyUsers = await _ctx.Followers.Where(x => x.FollowingId == stylst.Id && x.FollowedBy.IsDummy).Select(x => x.UserId).ToListAsync();

                        var Dlist = (from d in dummyUser
                                     where !followingDummyUsers.Contains(d.Id)
                                     select new
                                     {
                                         d.Id
                                     }).ToList();

                        var DummyCount = Dlist.Count();
                        Random rnd = new Random();
                        var number = (int)(rnd.Next(1, DummyCount));
                        //number = 3;


                        // var rendomVal = generateRandomDummyUserCount();
                        var UserId = Dlist.Skip(number).FirstOrDefault().Id;
                        bool isFollow = true;
                        isFollow = await _ctx.Followers.AnyAsync(x => x.UserId == UserId && x.FollowingId == stylst.Id);
                        if (isFollow == false)
                        {
                            var follow = new UserFollow
                            {
                                UserId = UserId,
                                FollowingId = stylst.Id
                            };
                            _ctx.Followers.Add(follow);

                            Notification notification = new Notification
                            {
                                SenderId = UserId,
                                ReceiverId = stylst.Id,
                                IsRead = false,
                                NotificationType = NotificationType.Follow,
                                NotificationText = Resource.notificationFollowyou,
                            };
                            _ctx.Notification.Add(notification);
                            await _ctx.SaveChangesAsync();
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return mResult;
        }

        public async Task<bool> LikeSchedular()
        {
            try
            {
                var dummyUser = _ctx.Users.Where(x => x.IsDummy).ToList();

                //THIS IS MAIN
                var StylistList = await (from user in _ctx.Users
                                         where user.Role == Role.Stylist || user.Role == Role.User && user.IsDummy == false
                                         select user).ToListAsync();

                foreach (var stylst in StylistList)
                {
                    var objStylist = await _ctx.UserTask.FirstOrDefaultAsync(x => x.UserId == stylst.Id);
                    if (objStylist == null)
                    {
                        objStylist = await AddTask(stylst.Id);
                    }


                    var postIdlist = await (from pst in _ctx.Post
                                            where pst.UserId == stylst.Id
                                            select pst).Select(x => x.Id).ToListAsync();

                    if (postIdlist != null && postIdlist.Count > 0)
                    {
                        foreach (var postid in postIdlist)
                        {
                            int task = 0;
                            int maxTask = Math.Max(objStylist.FollowTask, objStylist.LikeTask);
                            int FollowCount = objStylist.CommentTask;

                            if (maxTask > FollowCount)
                                task = maxTask;
                            else
                                task = FollowCount;

                            var LikesDone = _ctx.Likes.Count(x => x.LikeBy.IsDummy == true && x.Post.PostBy.Id == stylst.Id);
                            if (task != LikesDone)
                            {
                                var AlreadyLikeList = await _ctx.Likes.Where(x => x.LikeBy.IsDummy == true && x.PostId == postid).Select(x => x.LikeBy.Id).ToListAsync();

                                var Dlist = (from d in dummyUser
                                             where !AlreadyLikeList.Contains(d.Id)
                                             select new
                                             {
                                                 d.Id
                                             }).ToList();

                                var DummyCount = Dlist.Count();
                                Random rnd = new Random();
                                var number = (int)(rnd.Next(1, DummyCount));

                                //take random dummy user
                                //var rendomVal = generateRandomDummyUserCount();
                                var UserId = Dlist.Skip(number).FirstOrDefault().Id;

                                //check is Like?
                                bool IsLike = true;
                                IsLike = _ctx.Likes.Any(x => x.UserId == UserId && x.PostId == postid);
                                if (IsLike == false)
                                {
                                    var Like = new Likes
                                    {
                                        UserId = UserId,
                                        PostId = postid
                                    };
                                    _ctx.Likes.Add(Like);

                                    Notification notification = new Notification
                                    {
                                        SenderId = UserId,
                                        ReceiverId = stylst.Id,
                                        IsRead = false,
                                        NotificationType = NotificationType.like,
                                        NotificationText = Resource.NotificationLikePost,
                                        PostId = postid
                                    };
                                    _ctx.Notification.Add(notification);
                                    await _ctx.SaveChangesAsync();
                                    //break;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public int[] RandomDummyUserCount()
        {
            var DummyCount = _ctx.Users.Count(x => x.IsDummy == true);
            Random rnd = new Random();
            int[] counts = new int[3];
            for (int i = 0; i <= 2; i++)
            {
                counts[i] = (int)(rnd.Next(1, DummyCount));
            }
            return counts;
        }

        public async Task<UserTask> AddTask(string stylstId)
        {
            int[] counts = new int[3];
            counts = RandomDummyUserCount();
            UserTask user = new UserTask
            {
                UserId = stylstId,
                FollowTask = counts[0],
                CommentTask = counts[1],
                LikeTask = counts[2],
                MessageTask = 3
            };
            _ctx.UserTask.Add(user);
            await _ctx.SaveChangesAsync();
            //System.Threading.Thread.Sleep(2000);
            return user;
        }

        public async Task<bool> CommentSchedular()
        {
            //DateTime RegisterTime = Utility.GetSystemDateTimeUTC().AddHours(-2);
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                //dummy userlist
                var DummyUserList = await _ctx.Users.Where(x => x.IsDummy).ToListAsync();

                var StylistList = await (from user in _ctx.Users
                                         where user.Role == Role.Stylist || user.Role == Role.User && user.IsDummy == false
                                         //&& user.CreatedDateTimeUTC <= RegisterTime
                                         select user).ToListAsync();

                foreach (var stylst in StylistList)
                {
                    var objStylist = await _ctx.UserTask.FirstOrDefaultAsync(x => x.UserId == stylst.Id);
                    if (objStylist == null)
                    {
                        objStylist = await AddTask(stylst.Id);
                    }

                    var postlist = await (from pst in _ctx.Post
                                          where pst.UserId == stylst.Id
                                          select pst).ToListAsync();
                    if (postlist != null && postlist.Count > 0)
                    {
                        foreach (var itm in postlist)
                        {
                            //int task = 0;
                            //int maxTask = Math.Max(objStylist.FollowTask, objStylist.LikeTask);
                            int commentTask = objStylist.CommentTask;

                            //if (maxTask > FollowCount)
                            //    task = maxTask;
                            //else
                            //    task = FollowCount;
                            //var commented = objStylist.CommentTask;
                            var CommentsDone = await _ctx.Comments.CountAsync(x => x.CommentedBy.IsDummy == true && x.Post.PostBy.Id == stylst.Id);
                            if (commentTask != CommentsDone)
                            {
                                var AlreadyCommented = await _ctx.Comments.Where(x => x.PostId == itm.Id && x.CommentedBy.IsDummy == true).Select(x => x.CommentedBy.Id).ToListAsync();

                                var Dlist = (from d in DummyUserList
                                             where !AlreadyCommented.Contains(d.Id)
                                             select new
                                             {
                                                 d.Id
                                             }).ToList();

                                var DummyCount = Dlist.Count();
                                Random rnd = new Random();
                                var number = (int)(rnd.Next(1, DummyCount));
                                //take random dummy user
                                //var rendomVal = generateRandomDummyUserCount();
                                var UserId = DummyUserList.Skip(number).FirstOrDefault().Id;

                                //is already Commented?
                                bool comment = await _ctx.Comments.AnyAsync(x => x.PostId == itm.Id && x.CommentedBy.Id == UserId);
                                if (comment)
                                {
                                    break;
                                }
                                else
                                {
                                    //find category of post
                                    List<PostCategory> categorylist = await _ctx.PostCategory.Where(x => x.PostId == itm.Id).ToListAsync();
                                    var No = categorylist.Count();
                                    Random r = new Random();
                                    var rn = (int)(r.Next(No));
                                    long category = categorylist.Skip(rn).FirstOrDefault().CategoryId;
                                    //find commentList of above category with Post by user Language
                                    List<string> commentTextList = await (from langtran in _ctx.BotsCommentLangTran join lng in _ctx.BotComments on langtran.BotCommentId equals lng.Id where lng.CategoryId == category && langtran.LanguageId == itm.PostBy.Language select langtran.CommentText).ToListAsync();

                                    //select Random Comment
                                    var num = commentTextList.Count();
                                    Random randm = new Random();
                                    var rendom = (int)(randm.Next(0, num));
                                    string CommentText = commentTextList.Skip(rendom).FirstOrDefault();

                                    //create comment
                                    Comments objComments = new Comments();
                                    objComments.PostId = itm.Id;
                                    objComments.Message = CommentText;
                                    objComments.UserId = UserId;
                                    objComments.IsSpam = false;
                                    _ctx.Comments.Add(objComments);
                                    int id = await _ctx.SaveChangesAsync();

                                    //create notification
                                    if (await _ctx.Post.Where(x => x.Id == itm.Id).Select(x => x.PostBy.Id).FirstOrDefaultAsync() != UserId)
                                    {
                                        Notification notification = new Notification
                                        {
                                            SenderId = UserId,
                                            ReceiverId = _ctx.Post.Where(x => x.Id == itm.Id).Select(x => x.PostBy.Id).FirstOrDefault(),
                                            NotificationType = NotificationType.Comment,
                                            rating = 0,
                                            IsRead = false,
                                            PostId = itm.Id,
                                            CommentId = objComments.Id,
                                            NotificationText = Resource.CommentedOnYourPost,
                                        };
                                        _ctx.Notification.Add(notification);
                                        await _ctx.SaveChangesAsync();
                                    }
                                }
                            }
                        }
                    }

                }
            }
            catch (Exception)
            {
                throw;
            }
            return true;
        }

        public async Task<bool> MessageSchedule()
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            //var RegisterTime = Utility.GetSystemDateTimeUTC().AddHours(-2);

            //Get List of dummy users            
            var dummyUser = await _ctx.Users.Where(x => x.IsDummy).ToListAsync();

            //find stylist by time
            string id = "3d6bdbe7-db3b-442a-a88c-db903a4a7162";
            var StylistList = await (from user in _ctx.Users
                                     where user.Role == Role.Stylist
                                     && user.Id == id
                                     //&& user.CreatedDateTimeUTC <= RegisterTime
                                     select user).ToListAsync();

            var count = dummyUser.Count();

            //if stylist list not null
            if (StylistList != null && StylistList.Count > 0)
            {
                foreach (var stylst in StylistList)
                {
                    var objStylist = await _ctx.UserTask.FirstOrDefaultAsync(x => x.UserId == stylst.Id);

                    if (objStylist == null)
                        objStylist = await AddTask(stylst.Id);

                    MessageSendModel MessageModel = new MessageSendModel();

                    var AlreadyMessaged = await _ctx.ChatBuddy.Where(x => x.ReceiverId == stylst.Id && x.SenderUser.IsDummy == true).Select(x => x.SenderUser.Id).ToListAsync();

                    var Dlist = (from d in dummyUser
                                 where !AlreadyMessaged.Contains(d.Id)
                                 select new
                                 {
                                     d.Id
                                 }).ToList();

                    var DummyCount = Dlist.Count();
                    Random mrnd = new Random();
                    var number = (int)(mrnd.Next(1, DummyCount));
                    //select random Dummy user
                    //var rendomVal = generateRandomDummyUserCount();
                    var UserId = dummyUser.Skip(number).FirstOrDefault().Id;


                    //check is message send?
                    bool IsMessageSended = await _ctx.ChatMessage.AnyAsync(x => x.UserId == UserId && x.ReceiverId == stylst.Id);

                    if (IsMessageSended)
                    {
                        var sendMessageCount = await _ctx.ChatMessage.CountAsync(x => x.UserId == UserId && x.ReceiverId == stylst.Id);
                        var receiverMessageCount = await _ctx.ChatMessage.CountAsync(x => x.UserId == stylst.Id && x.ReceiverId == UserId);
                        if (sendMessageCount == 1 && receiverMessageCount >= 1)
                        {
                            var messageText = await (from btmsg in _ctx.BotMessageTran
                                                     join tran in _ctx.BotMessages on btmsg.BotMessageId equals tran.Id
                                                     where tran.MessageTypeId == BotMessageType.HowAreYou && btmsg.LanguageId == stylst.Language
                                                     select btmsg.MessageText).FirstOrDefaultAsync();

                            MessageModel.ReceiverID = stylst.Id;
                            MessageModel.MessageText = messageText;
                            mResult = await _chatrepo.SendMessage(MessageModel, UserId);
                        }
                        else if (sendMessageCount == 2)
                        {
                            var Lastmessage = await _ctx.ChatMessage.OrderByDescending(x => x.Id).FirstOrDefaultAsync(x => (x.UserId == UserId && x.ReceiverId == stylst.Id) || (x.UserId == stylst.Id && x.ReceiverId == UserId));

                            if (Lastmessage.UserId != UserId)
                            {
                                //find categories list of stylist
                                var stylistCategorylist = _ctx.StylistCategory.Where(x => x.StylistId == stylst.Id).ToList();

                                //select random category Name
                                var num = stylistCategorylist.Count();
                                Random rnd = new Random();
                                var randomNum = (int)(rnd.Next(0, num - 1));
                                var categoryName = stylistCategorylist.Skip(randomNum).FirstOrDefault().Categories.CategoryLanguageList.FirstOrDefault(x => x.LanguageId == stylst.Language).Name;

                                //select message by receiver language
                                string messageText = (from btmsg in _ctx.BotMessageTran
                                                      join tran in _ctx.BotMessages on btmsg.BotMessageId equals tran.Id
                                                      where tran.MessageTypeId == BotMessageType.WhatsYourPrice && btmsg.LanguageId == stylst.Language
                                                      select btmsg.MessageText).FirstOrDefault();

                                MessageModel.ReceiverID = stylst.Id;
                                MessageModel.MessageText = messageText + " " + categoryName;
                                MessageModel.IsImage = false;

                                mResult = await _chatrepo.SendMessage(MessageModel, UserId);
                            }
                        }
                    }
                    else
                    {
                        var message = objStylist.MessageTask;
                        var MessagesDone = await _ctx.ChatMessage.CountAsync(x => x.SenderUser.IsDummy == true && x.ReceiverId == stylst.Id);

                        if (message > MessagesDone)
                        {
                            //select message by receiver language
                            string messageText = (from btmsg in _ctx.BotMessageTran
                                                  join tran in _ctx.BotMessages on btmsg.BotMessageId equals tran.Id
                                                  where tran.MessageTypeId == BotMessageType.Hi && btmsg.LanguageId == stylst.Language
                                                  select btmsg.MessageText).FirstOrDefault();

                            MessageModel.ReceiverID = stylst.Id;
                            MessageModel.MessageText = messageText + " " + stylst.Name;
                            MessageModel.IsImage = false;

                            mResult = await _chatrepo.SendMessage(MessageModel, UserId);
                        }
                    }
                }
            }
            return true;
        }

        public bool checkFollowSchedular()
        {
            if (_ctx.AppSetting.FirstOrDefault() != null)
            {
                return _ctx.AppSetting.FirstOrDefault().IsFollow;
            }
            else
            {
                return false;
            }
        }

        public bool checkLikeSchedular()
        {
            if (_ctx.AppSetting.FirstOrDefault() != null)
            {
                return _ctx.AppSetting.FirstOrDefault().IsLike;
            }            
            else
            {
                return false;
            }
        }

        public bool checkCommentSchedular()
        {
            if (_ctx.AppSetting.FirstOrDefault() != null)
            {
                return _ctx.AppSetting.FirstOrDefault().IsComment;
            }
            else
            {
                return false;
            }
        }

        public bool checkMessageSchedular()
        {
            if (_ctx.AppSetting.FirstOrDefault() != null)
            {
                return _ctx.AppSetting.FirstOrDefault().IsMessage;
            }
            else
            {
                return false;
            }
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
