﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.StripeModels;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class GeneralRepository : IDisposable
    {
        private ApplicationDbContext _ctx;
        private PostRepository _postservice;
        public GeneralRepository()
        {
            _ctx = new ApplicationDbContext();
            _postservice = new PostRepository();
        }

        public DashboardCount GetDashboardCount()
        {
            DashboardCount model = new DashboardCount();
            model.TotalStylist = _ctx.Users.Count(x => x.Role == Role.Stylist);
            model.TotalUser = _ctx.Users.Count(x => x.Role == Role.User);
            model.TotalPost = _ctx.Post.Count();
            //model.TotalRequest = _app.StylishBookings.Count(x => x.RequestStatus == RequestStatus.None);
            return model;
        }
        #region About Us
        public async Task<AboutUsModel> GetAboutUs()
        {
            return await (from c in _ctx.About
                          select new AboutUsModel
                          {
                              Id = c.Id,
                              AboutUsLanguageModelList = c.AboutUsLanguageList.Select(x => new AboutUsLanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  AboutUs = x.AboutUs,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<AboutUsModel> GetAboutUs(languageType languageId)
        {
            return await (from ct in _ctx.About
                          join ctlng in _ctx.AboutLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.AboutUsId into HasCategory
                          select new AboutUsModel
                          {
                              Id = ct.Id,
                              AboutUsLanguageModelList = HasCategory.Select(x => new AboutUsLanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  AboutUs = x.AboutUs,
                                  LanguageName = (x.LanguageId == 0 ? "English" : "German").ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool isAboutUsDuplicate(AboutUsModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.AboutUsLanguageModelList)
            {
                isDuplicate = _ctx.AboutLanguage.Any(x => x.LanguageId == item.LanguageId && x.AboutUsId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<ResponseModel<object>> UpdateAboutUs(AboutUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool isNew = false;
            var data = await _ctx.About.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (data == null)
            {
                isNew = true;
                data = new About();
            }

            if (isNew)
            {
                List<AboutUsLanguage> lstAboutUsLanguage = new List<AboutUsLanguage>();
                foreach (var item in model.AboutUsLanguageModelList)
                {
                    lstAboutUsLanguage.Add(new AboutUsLanguage
                    {
                        AboutUsId = data.Id,
                        AboutUs = item.AboutUs,
                        LanguageId = item.LanguageId
                    });
                }
                data.AboutUsLanguageList = lstAboutUsLanguage;
                _ctx.About.Add(data);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                foreach (var item in data.AboutUsLanguageList)
                {
                    AboutUsLanguageModel AboutUsLanguage = model.AboutUsLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                    item.AboutUs = AboutUsLanguage.AboutUs;
                }
            }
            int i = await _ctx.SaveChangesAsync();
            if (i > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "AboutUs updated successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "AboutUs saved successfully";
            }
            return mResult;
        }

        #endregion

        #region Terms & Condition
        public async Task<TermsModel> GetTerms()
        {
            return await (from t in _ctx.Terms
                          select new TermsModel
                          {
                              Id = t.Id,
                              TermsLanguageModelList = t.TermsLanguageList.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.TermsText,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<TermsModel> GetTerms(languageType languageId)
        {
            return await (from ct in _ctx.Terms
                          join ctlng in _ctx.TermsLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.TermsId into HasCategory
                          select new TermsModel
                          {
                              Id = ct.Id,
                              TermsLanguageModelList = HasCategory.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.TermsText,
                                  LanguageName = (x.LanguageId == 0 ? "English" : "German").ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool IstermsDuplicate(TermsModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.TermsLanguageModelList)
            {
                isDuplicate = _ctx.TermsLanguage.Any(x => x.LanguageId == item.LanguageId && x.TermsId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<ResponseModel<object>> UpdateTermsConditions(TermsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool isNew = false;
            var data = await _ctx.Terms.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (data == null)
            {
                isNew = true;
                data = new Terms();
            }

            if (isNew)
            {
                List<TermsLanguage> lstAboutUsLanguage = new List<TermsLanguage>();
                foreach (var item in model.TermsLanguageModelList)
                {
                    lstAboutUsLanguage.Add(new TermsLanguage
                    {
                        TermsId = data.Id,
                        TermsText = item.Content,
                        LanguageId = item.LanguageId
                    });
                }
                data.TermsLanguageList = lstAboutUsLanguage;
                _ctx.Terms.Add(data);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                foreach (var item in data.TermsLanguageList)
                {
                    LanguageModel TermsLanguage = model.TermsLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                    item.TermsText = TermsLanguage.Content;
                }
            }
            int i = await _ctx.SaveChangesAsync();
            if (i > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Terms & Conditions updated successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Terms & Conditions saved successfully";
            }
            return mResult;
        }
        #endregion

        #region Imprints
        public async Task<ImprintsModel> GetImprints()
        {
            return await (from imp in _ctx.Imprints
                          select new ImprintsModel
                          {
                              Id = imp.Id,
                              ImprintLanguageModelList = imp.ImprintsLanguageList.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.ImprintText,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ImprintsModel> GetImprints(languageType languageId)
        {
            return await (from ct in _ctx.Imprints
                          join ctlng in _ctx.ImprintsLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.ImprintId into HasCategory
                          select new ImprintsModel
                          {
                              Id = ct.Id,
                              ImprintLanguageModelList = HasCategory.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.ImprintText,
                                  LanguageName = (x.LanguageId == 0 ? "English" : "German").ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool IsImprintDuplicate(ImprintsModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.ImprintLanguageModelList)
            {
                isDuplicate = _ctx.ImprintsLanguage.Any(x => x.LanguageId == item.LanguageId && x.ImprintId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<ResponseModel<object>> UpdateImprints(ImprintsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool isNew = false;
            var data = await _ctx.Imprints.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (data == null)
            {
                isNew = true;
                data = new Imprints();
            }

            if (isNew)
            {
                List<ImprintsLanguage> imprintLanguage = new List<ImprintsLanguage>();
                foreach (var item in model.ImprintLanguageModelList)
                {
                    imprintLanguage.Add(new ImprintsLanguage
                    {
                        ImprintId = data.Id,
                        ImprintText = item.Content,
                        LanguageId = item.LanguageId
                    });
                }
                data.ImprintsLanguageList = imprintLanguage;
                _ctx.Imprints.Add(data);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                foreach (var item in data.ImprintsLanguageList)
                {
                    LanguageModel ImprintLanguage = model.ImprintLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                    item.ImprintText = ImprintLanguage.Content;
                }
            }
            int i = await _ctx.SaveChangesAsync();
            if (i > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Imprints updated successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Imprints saved successfully";
            }
            return mResult;
        }
        #endregion

        #region Privacy policy
        public async Task<PrivacyPolicyModel> GetPrivacyPolicy()
        {
            return await (from pp in _ctx.PrivacyPolicy
                          select new PrivacyPolicyModel
                          {
                              Id = pp.Id,
                              PolicyLanguageModelList = pp.PrivacyPolicyLanguage.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.PrivacyPolicyText,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<PrivacyPolicyModel> GetPrivacyPolicy(languageType languageId)
        {
            return await (from ct in _ctx.PrivacyPolicy
                          join ctlng in _ctx.PrivacyPolicyLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.PolicyId into HasCategory
                          select new PrivacyPolicyModel
                          {
                              Id = ct.Id,
                              PolicyLanguageModelList = HasCategory.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.PrivacyPolicyText,
                                  LanguageName = (x.LanguageId == 0 ? "English" : "German").ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool IspolicyDuplicate(PrivacyPolicyModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.PolicyLanguageModelList)
            {
                isDuplicate = _ctx.PrivacyPolicyLanguage.Any(x => x.LanguageId == item.LanguageId && x.PolicyId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<ResponseModel<object>> UpdatePrivacyPolicy(PrivacyPolicyModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool isNew = false;
            var data = await _ctx.PrivacyPolicy.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (data == null)
            {
                isNew = true;
                data = new PrivacyPolicy();
            }

            if (isNew)
            {
                List<PrivacyPolicyLanguage> PolicyLanguage = new List<PrivacyPolicyLanguage>();
                foreach (var item in model.PolicyLanguageModelList)
                {
                    PolicyLanguage.Add(new PrivacyPolicyLanguage
                    {
                        PolicyId = data.Id,
                        PrivacyPolicyText = item.Content,
                        LanguageId = item.LanguageId
                    });
                }
                data.PrivacyPolicyLanguage = PolicyLanguage;
                _ctx.PrivacyPolicy.Add(data);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                foreach (var item in data.PrivacyPolicyLanguage)
                {
                    LanguageModel ImprintLanguage = model.PolicyLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                    item.PrivacyPolicyText = ImprintLanguage.Content;
                }
            }
            int i = await _ctx.SaveChangesAsync();
            if (i > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Privacy & Policy updated successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Privacy & Policy saved successfully";
            }
            return mResult;
        }
        #endregion

        #region What Is Stylaaa
        public async Task<WhatIsStylaaaModel> GetWhatIsStylaaa()
        {
            return await (from t in _ctx.WhatIsStylaaa
                          select new WhatIsStylaaaModel
                          {
                              Id = t.Id,
                              WhatIsStylaaLanguageModelList = t.WhatIsStylaaaLanguageList.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.WhatIsStylaaaText,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<WhatIsStylaaaModel> GetWhatIsStylaaa(languageType languageId)
        {
            return await (from ct in _ctx.WhatIsStylaaa
                          join ctlng in _ctx.WhatIsStylaaaLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.WhatIsStylaaId into HasCategory
                          select new WhatIsStylaaaModel
                          {
                              Id = ct.Id,
                              WhatIsStylaaLanguageModelList = HasCategory.Select(x => new LanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  Content = x.WhatIsStylaaaText,
                                  LanguageName = (x.LanguageId == 0 ? "English" : "German").ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public bool IsWhatIsStylaaDuplicate(WhatIsStylaaaModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.WhatIsStylaaLanguageModelList)
            {
                isDuplicate = _ctx.WhatIsStylaaaLanguage.Any(x => x.LanguageId == item.LanguageId && x.WhatIsStylaaId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }


        public async Task<ResponseModel<object>> UpdateWhatIsStylaaa(WhatIsStylaaaModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            bool isNew = false;
            var data = await _ctx.WhatIsStylaaa.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (data == null)
            {
                isNew = true;
                data = new WhatIsStylaaa();
            }

            if (isNew)
            {
                List<WhatIsStylaaaLanguage> WhatsIsStylaaa = new List<WhatIsStylaaaLanguage>();
                foreach (var item in model.WhatIsStylaaLanguageModelList)
                {
                    WhatsIsStylaaa.Add(new WhatIsStylaaaLanguage
                    {
                        WhatIsStylaaId = data.Id,
                        WhatIsStylaaaText = item.Content,
                        LanguageId = item.LanguageId
                    });
                }
                data.WhatIsStylaaaLanguageList = WhatsIsStylaaa;
                _ctx.WhatIsStylaaa.Add(data);
                await _ctx.SaveChangesAsync();
            }
            else
            {
                foreach (var item in data.WhatIsStylaaaLanguageList)
                {
                    LanguageModel WhatIsStylaaa = model.WhatIsStylaaLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                    item.WhatIsStylaaaText = WhatIsStylaaa.Content;
                }
            }
            int i = await _ctx.SaveChangesAsync();
            if (i > 0)
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "What Is Stylaaa updated successfully";
            }
            else
            {
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "What Is Stylaaa saved successfully";
            }
            return mResult;
        }

        #endregion



        public async Task<long> UpdateWhatIsStylaaaForUser(WhatIsStylaaaForUserModel model)
        {
            WhatIsStylaaaForUser objWhatIsStylaaa = await _ctx.WhatIsStylaaaForUser.FirstOrDefaultAsync();
            bool isNew = false;
            if (objWhatIsStylaaa == null)
            {
                objWhatIsStylaaa = new WhatIsStylaaaForUser();
                isNew = true;
            }
            objWhatIsStylaaa.WhatIsStylaaaText = model.WhatIsStylaaaForUserText;
            if (isNew)
                _ctx.WhatIsStylaaaForUser.Add(objWhatIsStylaaa);
            await _ctx.SaveChangesAsync();
            return objWhatIsStylaaa.Id;
        }

        public async Task<long> UpdateWhatIsStylaaaForStylist(WhatIsStylaaaForStylistModel model)
        {
            WhatIsStylaaaForStylist objWhatIsStylaaa = await _ctx.WhatIsStylaaaForStylist.FirstOrDefaultAsync();
            bool isNew = false;
            if (objWhatIsStylaaa == null)
            {
                objWhatIsStylaaa = new WhatIsStylaaaForStylist();
                isNew = true;
            }
            objWhatIsStylaaa.WhatIsStylaaaText = model.WhatIsStylaaaForStylistText;
            if (isNew)
                _ctx.WhatIsStylaaaForStylist.Add(objWhatIsStylaaa);
            await _ctx.SaveChangesAsync();
            return objWhatIsStylaaa.Id;
        }


        public async Task<List<UpgradeAccountModel>> GetUpgradeAccountMasterDetail(int month)
        {
            return await (from acc in _ctx.UpgradeAccountData
                          where acc.Month == (month > 0 ? month : acc.Month)
                          select new UpgradeAccountModel
                          {
                              UpgradeAccountId = acc.Id,
                              Month = acc.Month,
                              Amount = acc.Amount,
                              Credit = acc.Credit
                          }).ToListAsync();
        }

        public async Task<List<UpgradeCreditModel>> GetUpgradeCreditMasterDetail(int credit)
        {
            return await (from acc in _ctx.UpgradeCreditData
                          where acc.Credit == (credit > 0 ? credit : acc.Credit)
                          select new UpgradeCreditModel
                          {
                              UpgradeCreditId = acc.Id,
                              Credit = acc.Credit,
                              Amount = acc.Amount
                          }).ToListAsync();
        }


        public async Task<bool> ISAbleToBuy(string UserId)
        {
            bool ISAbleToBuy = false;
            var stylist = await (from premium in _ctx.StylistPremiunTran
                                 where premium.StylistId == UserId
                                 select new
                                 {
                                     premium.paymentStatus,
                                     premium.PaymentMothod,
                                     premium.CreatedDateTimeUTC
                                 }).OrderByDescending(x => x.CreatedDateTimeUTC).FirstOrDefaultAsync();
            if (stylist == null)
            {
                ISAbleToBuy = true;
            }
            else if (stylist.paymentStatus == ChargeStatus.succeeded)
            {
                ISAbleToBuy = true;
            }
            else
            {
                ISAbleToBuy = false;
            }
            return ISAbleToBuy;
        }

        public UpgradeAccountModel GetUpgradeAccountDetail(int month)
        {
            return (from acc in _ctx.UpgradeAccountData
                    where acc.Month == month
                    select new UpgradeAccountModel
                    {
                        UpgradeAccountId = acc.Id,
                        Month = acc.Month,
                        Amount = acc.Amount
                    }).FirstOrDefault();
        }

        public UpgradeAccountModel GetUpgradeAccountDetailByID(int UpgradeAccountId)
        {
            return (from acc in _ctx.UpgradeAccountData
                    where acc.Id == UpgradeAccountId
                    select new UpgradeAccountModel
                    {
                        UpgradeAccountId = acc.Id,
                        Month = acc.Month,
                        Amount = acc.Amount
                    }).FirstOrDefault();
        }

        public UpgradeCreditModel GetUpgradeCreditDetailById(int UpgradeCreditId)
        {
            return (from credit in _ctx.UpgradeCreditData
                    where credit.Id == UpgradeCreditId
                    select new UpgradeCreditModel
                    {
                        UpgradeCreditId = credit.Id,
                        Credit = credit.Credit,
                        Amount = credit.Amount
                    }).FirstOrDefault();
        }

        public async Task<bool> AddStylistPremiumTransaction(StylistPremiumTransactionModel objectModel)
        {
            try
            {
                var todaynowUtc = Utility.GetSystemDateTimeUTC();
                var ExpireDate = Utility.GetSystemDateTimeUTC();
                var SubscribeDate = todaynowUtc;
                var oldsubdate = _ctx.StylistPremiunTran.OrderByDescending(x => x.Id).FirstOrDefault(x => (x.paymentStatus == ChargeStatus.pending || x.paymentStatus == ChargeStatus.succeeded) && (x.ExpireDate > todaynowUtc));
                if (oldsubdate != null)
                {
                    ExpireDate = oldsubdate.ExpireDate.AddMonths(objectModel.month);
                    SubscribeDate = oldsubdate.ExpireDate;
                }
                else
                {
                    ExpireDate = Utility.GetSystemDateTimeUTC().AddMonths(objectModel.month);
                }

                StylistPremiumTransaction objStylist = new StylistPremiumTransaction();
                objStylist.StylistId = objectModel.StylistId;
                objStylist.SubscribeDate = SubscribeDate;
                objStylist.CreatedDateTimeUTC = todaynowUtc;
                objStylist.UpgradeAccountId = _ctx.UpgradeAccountData.Where(x => x.Month == objectModel.month).Select(x => x.Id).FirstOrDefault();
                objStylist.ExpireDate = ExpireDate;
                objStylist.PaymentMothodId = await _ctx.PaymentMethodMasterData.Select(x => x.Id).FirstOrDefaultAsync();
                objStylist.paymentStatus = ChargeStatus.none;
                objStylist.invoiceFileName = objectModel.invoicefilename;

                _ctx.StylistPremiunTran.Add(objStylist);
                await _ctx.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateStylistPremiumTransaction(StylistPremiumTransactionModel objectModel)
        {
            var intPaymentMothodId = _ctx.PaymentMethodLanguage.FirstOrDefault(x => x.PaymentMethod == objectModel.PaymentMothod.ToString()).PaymentMethodId;

            var objStylist = await _ctx.StylistPremiunTran.Where(x => x.StylistId == objectModel.StylistId && x.UpgradeAccount.Month == objectModel.month).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

            objStylist.UpdatedDateTimeUTC = DateTime.UtcNow;
            objStylist.PaymentMothodId = (intPaymentMothodId > 0 ? intPaymentMothodId : objStylist.PaymentMothodId);
            objStylist.isPaymentDone = true;
            objStylist.receiptFileName = objectModel.receiptfilename;
            objStylist.stripeResponce = objectModel.stripeResponce;
            objStylist.paymentStatus = objectModel.paymentStatus;
            objStylist.transactionId = objectModel.transactionId;
            await _ctx.SaveChangesAsync();

            var PremiumTable = await _ctx.UpgradeAccountData.Where(x => x.Id == objStylist.UpgradeAccountId).FirstOrDefaultAsync();
            await UpdateCredit(PremiumTable.Credit, objectModel.StylistId);

            return true;
        }

        public async Task<bool> UpdateStylistPremiumTransactionStatus(StripeResponceModel objectModel)
        {
            if (objectModel.data.@object != null)
            {
                var JsonResult = JsonConvert.SerializeObject(objectModel);
                var tranId = objectModel.data.@object.id;
                var status = Utility.getChargeStaus(objectModel.data.@object.status);
                var todayNowUtc = Utility.GetSystemDateTimeUTC();
                var objPremiunTran = await _ctx.StylistPremiunTran.FirstOrDefaultAsync(x => x.transactionId == tranId);
                var ObjStylistBooking = await _ctx.BookStylist.FirstOrDefaultAsync(x => x.transactionId == tranId);

                if (objPremiunTran != null)
                {
                    objPremiunTran.paymentStatus = status;
                    objPremiunTran.stripeResponce = JsonResult;
                    objPremiunTran.UpdatedDateTimeUTC = todayNowUtc;
                    await _ctx.SaveChangesAsync();
                }
                else if (ObjStylistBooking != null)
                {
                    //ObjStylistBooking.paymentStatus = status;
                    ObjStylistBooking.stripeResponce = JsonResult;
                    ObjStylistBooking.UpdatedDateTimeUTC = todayNowUtc;
                    await _ctx.SaveChangesAsync();

                }
                else
                {
                    var ObjCreditTran = await _ctx.StylistCreditTrans.FirstOrDefaultAsync(x => x.transactionId == tranId);
                    if (ObjCreditTran != null)
                    {
                        ObjCreditTran.paymentStatus = status;
                        ObjCreditTran.stripeResponce = JsonResult;
                        objPremiunTran.UpdatedDateTimeUTC = todayNowUtc;
                        await _ctx.SaveChangesAsync();
                    }
                }
            }
            return true;
        }

        public async Task<ResponseModel<object>> ReadAllNotificationList(string userId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                _ctx.Notification.Where(x => x.ReceiverId == userId).ToList().ForEach(x => x.IsRead = true);
                await _ctx.SaveChangesAsync();
            }

            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public bool CheckValidMonthForUpgradeAccount(int month)
        {
            var result = _ctx.UpgradeAccountData.Where(x => x.Month == month).FirstOrDefault();
            if (result == null)
            {
                return false;
            }
            return true;
        }

        public async Task<WhatIsStylaaaForUserModel> GetWhatIsStylaaaForUser()
        {
            return await (from t in _ctx.WhatIsStylaaaForUser
                          select new WhatIsStylaaaForUserModel
                          {
                              Id = t.Id,
                              WhatIsStylaaaForUserText = t.WhatIsStylaaaText
                          }).FirstOrDefaultAsync();
        }

        public async Task<WhatIsStylaaaForStylistModel> GetWhatIsStylaaaForStylist()
        {
            return await (from t in _ctx.WhatIsStylaaaForStylist
                          select new WhatIsStylaaaForStylistModel
                          {
                              Id = t.Id,
                              WhatIsStylaaaForStylistText = t.WhatIsStylaaaText
                          }).FirstOrDefaultAsync();
        }

        //contact Us
        public async Task<ResponseModel<object>> AddContactUs(ContactUsModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ContactUs ObjContact = new ContactUs();
                ObjContact = new ContactUs
                {
                    Name = model.Name,
                    Email = model.Email,
                    Message = model.Message,
                    CreatedDateTimeUTC = System.DateTime.UtcNow
                };
                _ctx.ContactUs.Add(ObjContact);
                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    //#region send email
                    ////send email
                    //var emailTemplate = EmailTemplate.ActiveAccount;

                    //Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    //dicPlaceholders.Add("{{name}}", model.Name);
                    //dicPlaceholders.Add("{{email}}", model.Email);
                    //dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);

                    //IdentityMessage message = new IdentityMessage();
                    //message.Subject = string.Format("Welcome to {0}", GlobalConfig.ProjectName + ".com!");
                    //message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    //message.Destination = model.Email;

                    //await new EmailService().SendAsync(message);
                    //#endregion send email

                    mResult.Result = new { };
                    mResult.Message = Resource.contactUsSendMessageSuccess;
                    mResult.Status = ResponseStatus.Success;
                }
                else
                {
                    mResult.Result = new { };
                    mResult.Message = Resource.addContactUsFailed;
                    mResult.Status = ResponseStatus.Failed;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        public IQueryable<ContactUsModel> GetContactUsDetail()
        {
            return (from contact in _ctx.ContactUs
                    select new ContactUsModel
                    {
                        Id = contact.Id,
                        Name = contact.Name,
                        Email = contact.Email,
                        Message = contact.Message,
                        CreatedDate = contact.CreatedDateTimeUTC
                    }).OrderBy(x => x.CreatedDate);
        }

        public async Task<ResponseModel<object>> DeleteContactUs(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ContactUs catObj = await _ctx.ContactUs.FirstOrDefaultAsync(x => x.Id == Id);
                if (catObj == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    _ctx.ContactUs.Remove(catObj);
                    long id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Contact deleted Successfully";
                    }
                    else
                    {
                        mResult.Result = new object { };
                        mResult.Message = "Failed to Delete Detail";
                    }
                }
            }

            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public bool CheckFollowing(string StylishId, string UserId)
        {
            return (from flw in _ctx.Followers
                    where flw.UserId == UserId && flw.FollowingId == StylishId
                    select flw).Any();
        }

        public async Task<ResponseModel<object>> followthis(string StylishId, string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                UserFollow follow = new UserFollow();
                bool isfollow = CheckFollowing(StylishId, UserId);
                if (isfollow)
                {
                    follow = await (from f in _ctx.Followers
                                    where f.UserId == UserId && f.FollowingId == StylishId
                                    select f).FirstOrDefaultAsync();

                    Notification notification = _ctx.Notification.FirstOrDefault(x => x.SenderId == UserId && x.ReceiverId == StylishId);
                    if (notification != null)
                    {
                        _ctx.Notification.Remove(notification);
                    }

                    _ctx.Followers.Remove(follow);
                    int id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        mResult.Result = Resource.follow;
                    }
                }
                else
                {

                    follow = new UserFollow
                    {
                        UserId = UserId,
                        FollowingId = StylishId
                    };
                    _ctx.Followers.Add(follow);

                    Notification notification = new Notification
                    {
                        SenderId = UserId,
                        ReceiverId = StylishId,
                        IsRead = false,
                        NotificationType = NotificationType.Follow,
                        NotificationText = Resource.notificationFollowyou,
                    };

                    _ctx.Notification.Add(notification);
                    int id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        mResult.Result = @Resource.following;
                    }
                }
                mResult.Status = ResponseStatus.Success;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<List<UsersFollowModel>> ViewAllFollowings(PaginationModel model)
        {
            if (model.PageIndex > 0)
            {
                var userlist = await (from following in _ctx.Followers
                                      join rt in _ctx.UserReviews on following.Following.Id equals rt.StylishId into Hasreview
                                      join loginfollow in _ctx.Followers
                                      on new { following.FollowingId, model.UserId }
                                      equals new { loginfollow.FollowingId, loginfollow.UserId }
                                       into has_followingbyme
                                      where following.UserId == model.Id && following.Following.IsActive == true
                                      select new UsersFollowModel
                                      {
                                          Id = following.FollowingId,
                                          Name = following.Following.Name,
                                          UserName = following.Following.UserName,
                                          ProfileImageName = following.Following.ProfileImageName,
                                          Role = following.Following.Role,
                                          Isfollow = has_followingbyme.Any(),
                                          IsPremiumUser = _ctx.StylistPremiunTran.Any(x => x.StylistId == following.FollowingId),
                                          Ratings = Hasreview.Average(x => x.Rating),
                                          Experiance = following.Following.Experiance,
                                      }).OrderBy(x => x.Ratings)
                          .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();
                if (userlist != null)
                {
                    foreach (var item in userlist)
                    {
                        DateTime current = DateTime.UtcNow;
                        DateTime dt = Convert.ToDateTime(item.Experiance);

                        float year = (current - dt).Days / 365;
                        double month = (current - dt).Days / 30;
                        month = 10 * month / 12;
                        item.Months = month;
                    }
                }
                return userlist;
            }
            else
            {
                return await (from following in _ctx.Followers
                              where following.UserId == model.Id && following.Following.IsActive == true
                              select new UsersFollowModel
                              {
                                  Id = following.Following.Id
                              }).ToListAsync();
            }

        }


        public async Task<List<UsersFollowModel>> ViewAllFollowers(PaginationModel model)
        {
            if (model.PageIndex > 0)
            {
                var userlist = await (from follower in _ctx.Followers

                                      join loginfollow in _ctx.Followers
                                      on new { FollowingId = follower.UserId, model.UserId }
                                      equals new { loginfollow.FollowingId, loginfollow.UserId }
                                      into has_followingbyme

                                      join rt in _ctx.UserReviews on follower.FollowedBy.Id equals rt.StylishId into Hasreview

                                      where follower.FollowingId == model.Id && follower.FollowedBy.IsActive == true
                                      select new UsersFollowModel
                                      {
                                          Id = follower.UserId,
                                          Name = follower.FollowedBy.Name,
                                          UserName = follower.FollowedBy.UserName,
                                          ProfileImageName = follower.FollowedBy.ProfileImageName,
                                          Role = follower.FollowedBy.Role,
                                          Isfollow = has_followingbyme.Any(),
                                          Experiance = follower.FollowedBy.Experiance,
                                          IsPremiumUser = _ctx.StylistPremiunTran.Any(x => x.StylistId == follower.UserId),
                                          Ratings = Hasreview.Average(x => x.Rating)
                                      }).OrderBy(x => x.Ratings)
                                  .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();

                if (userlist != null)
                {
                    foreach (var item in userlist)
                    {
                        DateTime current = DateTime.UtcNow;
                        DateTime dt = Convert.ToDateTime(item.Experiance);

                        float year = (current - dt).Days / 365;
                        double month = (current - dt).Days / 30;
                        month = 10 * month / 12;
                        item.Months = month;
                    }

                }
                return userlist;
            }
            else
            {
                return await (from follower in _ctx.Followers
                              where follower.FollowingId == model.Id && follower.FollowedBy.IsActive == true
                              select new UsersFollowModel
                              {
                                  Id = follower.UserId,
                              }).ToListAsync();

            }
        }

        public List<NotificationListModel> GetNotificationList(PaginationModel model, bool isUnreadCount)
        {
            try
            {
                if (model.PageIndex > 0)
                {
                    var query = (from ntf in _ctx.Notification
                                 join pst in _ctx.Post on ntf.PostId equals pst.Id into HasPost
                                 join job in _ctx.Jobs on ntf.JobId equals job.Id into HasJob
                                 where ntf.ReceiverId == model.UserId
                                 //&& ntf.IsRead == false
                                 select new
                                 {
                                     ntf.Id,
                                     ntf.PostId,
                                     ntf.ReceiverId,
                                     ntf.SenderId,
                                     SenderName = (ntf.Sender != null ? ntf.Sender.Name : ""),
                                     SenderUserName = (ntf.Sender != null ? ntf.Sender.UserName : ""),
                                     SenderUserRole = (ntf.Sender != null ? ntf.Sender.Role : Role.User),
                                     SenderProfileImage = (ntf.Sender != null ? ntf.Sender.ProfileImageName : ""),
                                     PostImage = ntf.Post.PostFiles.Select(x => x.FileName).FirstOrDefault(),
                                     IsVideo = ntf.Post.PostFiles.Select(x => x.IsVideo).FirstOrDefault(),
                                     ntf.NotificationText,
                                     ntf.NotificationType,
                                     ntf.rating,
                                     ntf.CreatedDateTimeUTC
                                 }).ToList();

                    query = query.OrderByDescending(x => x.CreatedDateTimeUTC)
                            .Skip((model.PageIndex - 1) * 20)
                            .Take(20)
                            .ToList();

                    List<NotificationListModel> result = query.Select(ntf => new NotificationListModel()
                    {
                        Id = ntf.Id,
                        PostId = ntf.PostId ?? 0,
                        SenderId = ntf.SenderId,
                        SenderName = ntf.SenderName,
                        SenderUserName = ntf.SenderUserName,
                        SenderRole = ntf.SenderUserRole,
                        SenderProfileImage = ntf.SenderProfileImage != null ? ntf.SenderProfileImage : "DefaultProfile.jpg",
                        ReceiverId = ntf.ReceiverId,
                        PostImage = ntf.PostImage,
                        IsVideo = ntf.IsVideo,
                        IsFollow = _postservice.CheckFollow(ntf.SenderId, ntf.ReceiverId),
                        NotificationText = ntf.NotificationText,
                        NotificationTypeID = ntf.NotificationType,
                        Rating = ntf.rating,
                        AddedOn = ntf.CreatedDateTimeUTC,
                        CommentedTimeSpan = Utility.GetTimeSpan(ntf.CreatedDateTimeUTC)
                    }).ToList();

                    return result;
                }
                else
                {
                    var result = (from ntf in _ctx.Notification
                                  where ntf.ReceiverId == model.UserId
                                  //&& ntf.IsRead == false
                                  select new NotificationListModel
                                  {
                                      PostId = ntf.Id
                                  })
                                      .ToList();
                    if (isUnreadCount == true)
                    {
                        result = (from ntf in _ctx.Notification
                                  where ntf.ReceiverId == model.UserId
                                  && ntf.IsRead == false
                                  select new NotificationListModel
                                  {
                                      PostId = ntf.Id
                                  })
                                          .ToList();

                    }

                    return result;
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public async Task<List<UserListModel>> GetUsers()
        {
            var data = await (from user in _ctx.Users
                              where user.Role == Role.User
                              select new UserListModel
                              {
                                  Id = user.Id,
                                  Name = user.Name
                              }).ToListAsync();

            return data;
        }

        public async Task<List<UserListModel>> GetStylist()
        {
            var stylistlist = await (from stylist in _ctx.Users
                                     where stylist.Role == Role.Stylist
                                     select new UserListModel
                                     {
                                         Id = stylist.Id,
                                         Name = stylist.Name
                                     }).ToListAsync();

            return stylistlist;
        }

        public IQueryable<SpamReviewModel> GetSpamReviews()
        {
            return (from spamreview in _ctx.SpamReview
                    select new SpamReviewModel
                    {
                        ReviewId = spamreview.ReviewId,
                        ReviewById = spamreview.Reviews.ReviewBy.Id,
                        ReviewBy = spamreview.Reviews.ReviewBy.Name,
                        SpamById = spamreview.Spamby.Id,
                        SpamByName = spamreview.Spamby.Name,
                        SpamOnId = spamreview.Reviews.StylishId,
                        SpamOnName = spamreview.Reviews.Stylish.Name,
                        ReviewText = spamreview.Reviews.Comment,
                        ReasonText = spamreview.ReasonText
                    }).OrderBy(x => x.ReviewId);
        }

        public async Task<ResponseModel<object>> DeleteSpamReview(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                UserReview review = _ctx.UserReviews.FirstOrDefault(x => x.Id == id);
                if (review == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = "Review Not Found";
                }
                else
                {
                    var replyReview = _ctx.ReviewReplay.Where(x => x.ReviewId == id).ToList();
                    _ctx.ReviewReplay.RemoveRange(replyReview);

                    var notificaion = _ctx.Notification.Where(x => x.ReviewId == id).ToList();
                    _ctx.Notification.RemoveRange(notificaion);

                    var spamReview = _ctx.SpamReview.Where(x => x.ReviewId == id).ToList();
                    _ctx.SpamReview.RemoveRange(spamReview);

                    _ctx.UserReviews.Remove(review);
                }
                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Review Deleted";
                }
                else
                {
                    mResult.Result = new object { };
                    mResult.Message = "Failed to delete Review";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }


        /// <summary>
        /// credit management
        /// </summary>
        /// 


        public async Task<bool> AddStylistCreditTransaction(StylistCreditTransactionModel objectModel)
        {
            try
            {
                var todaynowUtc = Utility.GetSystemDateTimeUTC();
                StylistCreditTransaction objStylist = new StylistCreditTransaction();
                objStylist.StylistId = objectModel.StylistId;
                objStylist.CreatedDateTimeUTC = todaynowUtc;
                objStylist.UpgradeCreditId = await _ctx.UpgradeCreditData.Where(x => x.Credit == objectModel.Credit).Select(x => x.Id).FirstOrDefaultAsync();
                objStylist.PaymentMothodId = await _ctx.PaymentMethodMasterData.Select(x => x.Id).FirstOrDefaultAsync();
                objStylist.paymentStatus = ChargeStatus.none;
                //objStylist.invoiceFileName = objectModel.invoicefilename;

                _ctx.StylistCreditTrans.Add(objStylist);
                await _ctx.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }

        public async Task<bool> UpdateStylistCreditTransaction(StylistCreditTransactionModel objectModel)
        {
            var intPaymentMothodId = _ctx.PaymentMethodLanguage.FirstOrDefault(x => x.PaymentMethod == objectModel.PaymentMothod.ToString()).PaymentMethodId;

            var objStylist = await _ctx.StylistCreditTrans.Where(x => x.StylistId == objectModel.StylistId).OrderByDescending(x => x.Id).FirstOrDefaultAsync();

            objStylist.UpdatedDateTimeUTC = DateTime.UtcNow;
            objStylist.PaymentMothodId = (intPaymentMothodId > 0 ? intPaymentMothodId : objStylist.PaymentMothodId);
            //objStylist.receiptFileName = objectModel.receiptfilename;
            objStylist.stripeResponce = objectModel.stripeResponce;
            objStylist.paymentStatus = objectModel.paymentStatus;
            objStylist.transactionId = objectModel.transactionId;
            await _ctx.SaveChangesAsync();
            return await UpdateCredit(objectModel.Credit, objectModel.StylistId);
        }

        public async Task<bool> UpdateCredit(int credit, string stylistId)
        {
            if (credit > 0)
            {
                var stcredit = await _ctx.StylistCredit.FirstOrDefaultAsync(x => x.StylistId == stylistId);
                if (stcredit != null)
                {
                    stcredit.Credit = stcredit.Credit + credit;
                    await _ctx.SaveChangesAsync();
                }
                else
                {
                    StylistCredit Stylist = new StylistCredit
                    {
                        StylistId = stylistId,
                        Credit = credit
                    };
                    _ctx.StylistCredit.Add(Stylist);
                    await _ctx.SaveChangesAsync();
                }
            }
            return true;
        }

        public async Task<bool> UpdateStylistCreditTransactionStatus(StripeResponceModel objectModel)
        {
            if (objectModel.data.@object != null)
            {
                var JsonResult = JsonConvert.SerializeObject(objectModel);
                var tranId = objectModel.data.@object.id;
                var status = Utility.getChargeStaus(objectModel.data.@object.status);
                var todayNowUtc = Utility.GetSystemDateTimeUTC();
                var objPremiunTran = await _ctx.StylistCreditTrans.FirstOrDefaultAsync(x => x.transactionId == tranId);
                if (objPremiunTran != null)
                {
                    objPremiunTran.paymentStatus = status;
                    objPremiunTran.stripeResponce = JsonResult;
                    objPremiunTran.UpdatedDateTimeUTC = todayNowUtc;
                    await _ctx.SaveChangesAsync();
                }
            }
            return true;
        }


        /// <summary>
        /// DATA TABLE
        /// </summary>
        /// 
        public async Task<List<UserTableModel>> UserList(PageModel model)
        {
            return await (from usr in _ctx.Users
                          select new UserTableModel
                          {
                              Name = usr.Name,
                              UserName = usr.UserName,
                              Email = usr.Email,
                              ZipCode = usr.ZipCode
                          }).OrderBy(x => x.Name).Skip(model.skip).Take(model.pagesize).ToListAsync();
        }

        public async Task<List<UserTableModel>> UserList()
        {
            return await (from usr in _ctx.Users
                          select new UserTableModel
                          {
                              Name = usr.Name,
                              UserName = usr.UserName,
                              Email = usr.Email,
                              ZipCode = usr.ZipCode
                          }).ToListAsync();
        }

        public IQueryable<UserTableModel> GetUserList()
        {
            return (from usr in _ctx.Users
                    select new UserTableModel
                    {
                        Name = usr.Name,
                        UserName = usr.UserName,
                        Email = usr.Email,
                        ZipCode = usr.ZipCode
                    }).OrderBy(x => x.Name);
        }

        public int UserCount()
        {
            return _ctx.Users.Count();
        }
        public void Dispose()
        {
            _ctx.Dispose();
        }

    }
}
