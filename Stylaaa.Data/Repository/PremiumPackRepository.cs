﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class PremiumPackRepository
    {
        private readonly ApplicationDbContext _ctx;
        //const
        public PremiumPackRepository()
        {
            _ctx = new ApplicationDbContext();
        }
        /// <summary>
        /// For Premium Package
        /// </summary>
        /// <returns></returns>
        public IQueryable<UpgradeAccountModel> getPremiumpackages()
        {
            return (from premium in _ctx.UpgradeAccountData
                    select new UpgradeAccountModel
                    {
                        UpgradeAccountId = premium.Id,
                        Amount = premium.Amount,
                        Month = premium.Month,
                        Credit = premium.Credit,
                        CreatedDate = premium.CreatedDateTimeUTC
                    }).OrderBy(x => x.Amount);
        }

        public async Task<ResponseModel<object>> InsertOrUpdatePremium(UpgradeAccountModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            bool IsSamePack = _ctx.UpgradeAccountData.Any(x => x.Amount == model.Amount && x.Month == model.Month);
            if (IsSamePack == false)
            {
                try
                {
                    UpgradeAccountMaster objpremium = _ctx.UpgradeAccountData.FirstOrDefault(x => x.Id.Equals(model.UpgradeAccountId));
                    bool isNew = false;
                    if (objpremium == null)
                    {
                        objpremium = new UpgradeAccountMaster();
                        isNew = true;
                    }
                    objpremium.Month = model.Month;
                    objpremium.Amount = model.Amount;
                    objpremium.Credit = model.Credit;
                    //objpremium.Month = model.Month;
                    objpremium.CreatedDateTimeUTC = Utility.GetSystemDateTimeUTC();
                    if (isNew)
                        _ctx.UpgradeAccountData.Add(objpremium);
                    int id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        mResult.Message = "Premium Package created Successfully!";
                        mResult.Status = ResponseStatus.Success;
                        mResult.Result = objpremium;
                        return mResult;
                    }
                    else
                    {
                        mResult.Message = "Package Not created";
                        mResult.Status = ResponseStatus.Failed;
                        return mResult;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                mResult.Message = "Same Amount Package is Already Exist";
                mResult.Status = ResponseStatus.Failed;
                return mResult;
            }
        }

        public async Task<UpgradeAccountModel> EditPackageById(long Id)
        {
            return await (from premium in _ctx.UpgradeAccountData
                          where premium.Id == Id
                          select new UpgradeAccountModel
                          {
                              UpgradeAccountId = premium.Id,
                              Amount = premium.Amount,
                              Month = premium.Month,
                              Credit = premium.Credit,
                              CreatedDate = premium.CreatedDateTimeUTC
                          }).FirstOrDefaultAsync();
        }
        
        public async Task<ResponseModel<object>> DeletePackageById(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (_ctx.StylistPremiunTran.Any(x => x.UpgradeAccountId == Id))
                {
                    mResult.Result = new object { };
                    mResult.Message = "This package is Currently in use by Stylist";
                }
                else
                {
                    UpgradeAccountMaster ObjPremium = await _ctx.UpgradeAccountData.FirstOrDefaultAsync(x => x.Id == Id);
                    if (ObjPremium == null)
                    {
                        mResult.Result = new object { };
                        mResult.Message = "No Package found";
                    }
                    else
                    {
                        _ctx.UpgradeAccountData.Remove(ObjPremium);
                        long id = await _ctx.SaveChangesAsync();
                        if (id > 0)
                        {
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = "Package deleted Successfully";
                        }
                        else
                        {
                            mResult.Result = new object { };
                            mResult.Message = "Failed to Delete Package";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public List<SubscribedStylistModel> GetSubscribedStylist()
        {
            return (from pcgs in _ctx.StylistPremiunTran
                    where pcgs.paymentStatus == ChargeStatus.pending || pcgs.paymentStatus == ChargeStatus.succeeded || pcgs.paymentStatus == ChargeStatus.none
                    select new
                    {
                        pcgs.UpgradeAccountId,
                        pcgs.StylistId,
                        pcgs.Stylist.Name,
                        pcgs.Stylist.Email,
                        PremiumName = pcgs.UpgradeAccount.Month + " Month",
                        pcgs.UpgradeAccount.Amount,
                        pcgs.UpgradeAccount.Month,
                        SubscriptionStart = pcgs.SubscribeDate,
                        EndDate = pcgs.ExpireDate,
                        pcgs.CreatedDateTimeUTC,
                        pcgs.Stylist.UserName
                    }).Select(x => new SubscribedStylistModel
                    {
                        PackageId = x.UpgradeAccountId,
                        StylistId = x.StylistId,
                        StylistName = x.Name,
                        Email = x.Email,
                        PackageName = x.PremiumName,
                        Price = x.Amount,
                        Duration = x.Month,
                        SubscriptionStart = x.SubscriptionStart,
                        EndDate = x.EndDate,
                        StylistUsername = x.UserName,
                        CreatedDate = x.CreatedDateTimeUTC
                    }).ToList();
        }


        /// <summary>
        /// For Credit Packages
        /// </summary>
        /// <returns></returns>
        public IQueryable<UpgradeCreditModel> getCreditPackages()
        {
            return (from credit in _ctx.UpgradeCreditData
                    select new UpgradeCreditModel
                    {
                        UpgradeCreditId = credit.Id,
                        Amount = credit.Amount,
                        Credit = credit.Credit
                    }).OrderBy(x => x.Amount);
        }

        public async Task<ResponseModel<object>> InsertOrUpdateCredit(UpgradeCreditModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            bool IsSamePack = _ctx.UpgradeCreditData.Any(x => x.Amount == model.Amount && x.Credit == model.Credit);
            if (IsSamePack == false)
            {
                try
                {
                    UpgradeCreditMaster objpremium = _ctx.UpgradeCreditData.FirstOrDefault(x => x.Id.Equals(model.UpgradeCreditId));
                    bool isNew = false;
                    if (objpremium == null)
                    {
                        objpremium = new UpgradeCreditMaster();
                        isNew = true;
                    }
                    //objpremium.Month = model.Credit;
                    objpremium.Amount = model.Amount;
                    objpremium.Credit = model.Credit;
                    //objpremium.Month = model.Month;
                    //objpremium. = Utility.GetSystemDateTimeUTC();
                    if (isNew)
                        _ctx.UpgradeCreditData.Add(objpremium);
                    int id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        mResult.Message = "credit Package created Successfully!";
                        mResult.Status = ResponseStatus.Success;
                        mResult.Result = objpremium;
                        return mResult;
                    }
                    else
                    {
                        mResult.Message = "credit Not created";
                        mResult.Status = ResponseStatus.Failed;
                        return mResult;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
            {
                mResult.Message = "Same credit Package is Already Exist";
                mResult.Status = ResponseStatus.Failed;
                return mResult;
            }
        }
           
        public async Task<UpgradeCreditModel> EditCreditById(long Id)
        {
            return await (from premium in _ctx.UpgradeCreditData
                          where premium.Id == Id
                          select new UpgradeCreditModel
                          {
                              UpgradeCreditId = premium.Id,
                              Amount = premium.Amount,
                              // Month = premium.Month,
                              Credit = premium.Credit,
                              //CreatedDate = premium.CreatedDateTimeUTC
                          }).FirstOrDefaultAsync();
        }
           
        public async Task<ResponseModel<object>> DeleteCreditById(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (_ctx.StylistCreditTrans.Any(x => x.UpgradeCreditId == Id))
                {
                    mResult.Result = new object { };
                    mResult.Message = "This package is Currently in use by Stylist";
                }
                else
                {
                    UpgradeCreditMaster ObjPremium = await _ctx.UpgradeCreditData.FirstOrDefaultAsync(x => x.Id == Id);
                    if (ObjPremium == null)
                    {
                        mResult.Result = new object { };
                        mResult.Message = "No Credit Package found";
                    }
                    else
                    {
                        _ctx.UpgradeCreditData.Remove(ObjPremium);
                        long id = await _ctx.SaveChangesAsync();
                        if (id > 0)
                        {
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = "Credit package deleted successfully";
                        }
                        else
                        {
                            mResult.Result = new object { };
                            mResult.Message = "Failed to delete Credit package";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }
           
        public List<SubscribedCreditStylist> GetSubscribedCreditStylist()
        {
            return (from pcgs in _ctx.StylistCreditTrans
                    where pcgs.paymentStatus == ChargeStatus.pending || pcgs.paymentStatus == ChargeStatus.succeeded || pcgs.paymentStatus == ChargeStatus.none
                    select new
                    {
                        pcgs.UpgradeCreditId,
                        pcgs.StylistId,
                        pcgs.Stylist.Name,
                        pcgs.Stylist.Email,
                        pcgs.UpgradeAccount.Amount,
                        pcgs.UpgradeAccount.Credit,
                        pcgs.Stylist.UserName,
                        pcgs.CreatedDateTimeUTC
                    }).Select(x => new SubscribedCreditStylist
                    {
                        PackageId = x.UpgradeCreditId,
                        StylistId = x.StylistId,
                        StylistName = x.Name,
                        Price = x.Amount,
                        Email = x.Email,
                        Credit = x.Credit,
                        CreatedDate = x.CreatedDateTimeUTC,
                        StylistUsername = x.UserName,
                        CurrentCredit = _ctx.StylistCredit.Where(y => y.StylistId == x.StylistId).FirstOrDefault().Credit

                        //RemainingTime = (x.SubscriptionStart.AddMonths(Convert.ToInt32(x.Month)) - x.SubscriptionStart).TotalDays
                    }).ToList();
        }


        /// <summary>
        /// for Admin Transaction list and mark as sent receipt
        /// </summary>
        /// <returns></returns>
        public List<StylistTransactionHistory> GetTransactionHistory()
        {
            List<StylistTransactionHistory> responce = new List<StylistTransactionHistory>();
            var premiiumList = (from pcgs in _ctx.StylistPremiunTran
                                where pcgs.paymentStatus == ChargeStatus.pending || pcgs.paymentStatus == ChargeStatus.succeeded || pcgs.paymentStatus == ChargeStatus.none
                                select new
                                {
                                    pcgs.Id,
                                    pcgs.UpgradeAccountId,
                                    pcgs.StylistId,
                                    pcgs.Stylist.Name,
                                    pcgs.Stylist.Email,
                                    PremiumName = pcgs.UpgradeAccount.Month + " Month Premium",
                                    pcgs.UpgradeAccount.Amount,
                                    pcgs.UpgradeAccount.Month,
                                    SubscriptionStart = pcgs.SubscribeDate,
                                    EndDate = pcgs.ExpireDate,
                                    pcgs.CreatedDateTimeUTC,
                                    pcgs.Stylist.UserName,
                                    pcgs.IsInvoiceSended
                                }).Select(x => new StylistTransactionHistory
                                {
                                    Id = x.Id,
                                    PackageId = x.UpgradeAccountId,
                                    StylistId = x.StylistId,
                                    StylistName = x.Name,
                                    Email = x.Email,
                                    PackageName = x.PremiumName,
                                    Amount = x.Amount,
                                    //Duration = x.Month,
                                    SubscriptionStart = x.SubscriptionStart,
                                    EndDate = x.EndDate,
                                    StylistUsername = x.UserName,
                                    CreatedDate = x.CreatedDateTimeUTC,
                                    IsPremiume = true,
                                    IsSended=x.IsInvoiceSended
                                }).ToList();

            var creditlist = (from pcgs in _ctx.StylistCreditTrans
                              where pcgs.paymentStatus == ChargeStatus.pending || pcgs.paymentStatus == ChargeStatus.succeeded || pcgs.paymentStatus == ChargeStatus.none
                              select new
                              {
                                  pcgs.Id,
                                  pcgs.UpgradeCreditId,
                                  pcgs.StylistId,
                                  pcgs.Stylist.Name,
                                  pcgs.Stylist.Email,
                                  PacknName = pcgs.UpgradeAccount.Credit + " Credit Package",
                                  pcgs.UpgradeAccount.Amount,
                                  pcgs.UpgradeAccount.Credit,
                                  pcgs.Stylist.UserName,
                                  pcgs.CreatedDateTimeUTC,
                                  pcgs.IsInvoiceSended
                              }).Select(x => new StylistTransactionHistory
                              {
                                  Id = x.Id,
                                  PackageId = x.UpgradeCreditId,
                                  StylistId = x.StylistId,
                                  StylistName = x.Name,
                                  PackageName = x.PacknName,
                                  Amount = x.Amount,
                                  Email = x.Email,
                                  Credit = x.Credit,
                                  CreatedDate = x.CreatedDateTimeUTC,
                                  StylistUsername = x.UserName,
                                  IsPremiume = false,
                                  IsSended=x.IsInvoiceSended

                                  // CurrentCredit = _ctx.StylistCredit.Where(y => y.StylistId == x.StylistId).FirstOrDefault().Credit

                                  //RemainingTime = (x.SubscriptionStart.AddMonths(Convert.ToInt32(x.Month)) - x.SubscriptionStart).TotalDays
                              }).ToList();
            responce.AddRange(premiiumList);
            responce.AddRange(creditlist);

            return responce.OrderByDescending(x => x.CreatedDate).ToList();

        }

        public async Task<ResponseModel<object>> deleteTransactionRecord(long Id, int Credit)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (Credit > 0)
            {
                var creditRecore = _ctx.StylistCreditTrans.FirstOrDefault(x => x.Id == Id);
                if (creditRecore != null)
                {
                    _ctx.StylistCreditTrans.Remove(creditRecore);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
            }
            else
            {
                var record = _ctx.StylistPremiunTran.FirstOrDefault(x => x.Id == Id);
                if (record != null)
                {
                    _ctx.StylistPremiunTran.Remove(record);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> UploadReceiptFile(long Id, int Credit, string filename)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (Credit > 0)
            {
                var creditRecore = _ctx.StylistCreditTrans.FirstOrDefault(x => x.Id == Id);
                if (creditRecore != null)
                {
                    creditRecore.receiptFileName = filename;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
            }
            else
            {
                var record = _ctx.StylistPremiunTran.FirstOrDefault(x => x.Id == Id);
                if (record != null)
                {
                    record.receiptFileName = filename;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
            }

            return mResult;
        }
        
        public async Task<ResponseModel<object>> MarkAsSend(long Id, int Credit)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            if (Credit > 0)
            {
                var creditRecord = _ctx.StylistCreditTrans.FirstOrDefault(x => x.Id == Id);
                if (creditRecord != null)
                {
                    creditRecord.IsInvoiceSended = !creditRecord.IsInvoiceSended;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
            }
            else
            {
                var record = _ctx.StylistPremiunTran.FirstOrDefault(x => x.Id == Id);
                if (record != null)
                {
                    record.IsInvoiceSended = !record.IsInvoiceSended;
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                }
            }

            return mResult;
        }

        

    }
}
