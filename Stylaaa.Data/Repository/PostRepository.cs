﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class PostRepository
    {
        private ApplicationDbContext _ctx;
        private UserRepository _user;
        private CoreUserRepository _coreuser;
        private CategoryRepository _catrepo;

        public PostRepository()
        {
            _catrepo = new CategoryRepository();
            _ctx = new ApplicationDbContext();
            _user = new UserRepository();
            _coreuser = new CoreUserRepository();
        }

        public async Task<List<GetPostModelPopular>> PopularPost(PaginationModel model, List<string> strHashtag, long id)
        {
            try
            {
                if (model.PageIndex > 0)
                {
                    var query = await (from pst in _ctx.Post
                                       join fil in _ctx.PostTransactions on pst.Id equals fil.PostId into HasFile
                                       where pst.PostBy.IsActive && pst.Id == (id > 0 ? id : pst.Id)
                                       select new
                                       {
                                           PostId = pst.Id,
                                           pst.UserId,
                                           UserName = pst.PostBy.Name,
                                           NickName = pst.PostBy.UserName,
                                           UserRole = pst.PostBy.Role,
                                           TotalLikes = pst.PostLikes.Count,
                                           PostCountShare = pst.PostShare.Count,
                                           isUserLike = pst.PostLikes.Where(x => x.UserId == model.UserId && x.PostId == pst.Id).Any(),
                                           TotalComment = pst.PostComments.Count,
                                           Isvideo = (HasFile.Any() ? HasFile.FirstOrDefault().IsVideo : false),
                                           HasFile.FirstOrDefault().FileName,
                                           FileCount = (HasFile.Any() ? HasFile.Count() : 0),
                                           pst.Description,
                                           SearchHashTag = pst.PostHashTags.Select(x => x.Hashtag)
                                       })
                            //.Where(x => (!String.IsNullOrWhiteSpace(strHashtag[0]) ? strHashtag.Intersect(x.SearchHashTag).Any() : x.FileCount == x.FileCount))
                            //.OrderByDescending(x => x.TotalLikes)
                            //.Skip((model.PageIndex - 1) * model.PageSize)
                            //.Take(GlobalConfig.PageSize)
                            .ToListAsync();

                    if (strHashtag != null && strHashtag.Count > 0 && !string.IsNullOrWhiteSpace(strHashtag[0]))
                    {
                        query = query.Where(x => strHashtag.Intersect(x.SearchHashTag).Any())
                                .OrderByDescending(x => x.TotalLikes).ThenByDescending(x => x.TotalComment)
                                .Skip((model.PageIndex - 1) * model.PageSize)
                                .Take(GlobalConfig.PageSize)
                                .ToList();
                    }
                    else
                    {
                        query = query
                                .OrderByDescending(x => x.TotalLikes).ThenByDescending(x => x.TotalComment)
                                .Skip((model.PageIndex - 1) * model.PageSize)
                                .Take(GlobalConfig.PageSize)
                                .ToList();

                    }

                    List<GetPostModelPopular> searchPostModel = query.Select(post => new GetPostModelPopular()
                    {
                        PostId = post.PostId,
                        UserId = post.UserId,
                        UserName = post.UserName,
                        NickName = post.NickName,
                        PostByRole = _user.GetRoleById(post.UserId),
                        TotalLikes = post.TotalLikes,
                        isUserLike = post.isUserLike,
                        TotalComments = post.TotalComment,
                        PostCountShare = post.PostCountShare,
                        IsVideo = post.Isvideo,
                        FileName = post.FileName,
                        FileCount = post.FileCount,
                        Description = post.Description,
                        SearchHashTag = post.SearchHashTag
                    }).ToList();

                    return searchPostModel;
                }
                else
                {
                    //return await (from pst in _ctx.Post
                    //              where pst.PostBy.IsActive
                    //              select new GetPostModelPopular
                    //              {
                    //                  PostId = pst.Id,
                    //                  SearchHashTag = pst.PostHashTags.Select(x => x.Hashtag)
                    //              }).Where(x => (!String.IsNullOrWhiteSpace(strHashtag[0]) ? strHashtag.Intersect(x.SearchHashTag).Any() : x.PostId == x.PostId))
                    //              .ToListAsync();

                    var query = await (from pst in _ctx.Post
                                       where pst.PostBy.IsActive
                                       select new GetPostModelPopular
                                       {
                                           PostId = pst.Id,
                                           SearchHashTag = pst.PostHashTags.Select(x => x.Hashtag)
                                       })
                                      .ToListAsync();

                    if (strHashtag != null && strHashtag.Count > 0 && !String.IsNullOrWhiteSpace(strHashtag[0]))
                    {
                        return query.Where(x => strHashtag.Intersect(x.SearchHashTag).Any()).ToList();
                    }
                    else
                    {
                        return query.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public async Task<List<GetPostModelRecent>> RecentPost(PaginationModel model, List<string> strHashtag, long id)
        {
            try
            {
                if (model.PageIndex > 0)
                {
                    var query = await (from pst in _ctx.Post
                                       join fil in _ctx.PostTransactions on pst.Id equals fil.PostId into HasFile
                                       where pst.PostBy.IsActive && pst.Id == (id > 0 ? id : pst.Id)
                                       select new
                                       {
                                           PostId = pst.Id,
                                           pst.UserId,
                                           UserName = pst.PostBy.Name,
                                           NickName = pst.PostBy.UserName,
                                           TotalLikes = pst.PostLikes.Count,
                                           PostCountShare = pst.PostShare.Count,
                                           isUserLike = pst.PostLikes.Where(x => x.UserId == model.UserId && x.PostId == pst.Id).Any(),
                                           TotalComment = pst.PostComments.Count,
                                           Isvideo = (HasFile.Any() ? HasFile.FirstOrDefault().IsVideo : false),
                                           date = pst.CreatedDateTimeUTC,
                                           HasFile.FirstOrDefault().FileName,
                                           FileCount = (HasFile.Any() ? HasFile.Count() : 0),
                                           pst.Description,
                                           SearchHashTag = pst.PostHashTags.Select(x => x.Hashtag)
                                       })
                                .ToListAsync();

                    if (strHashtag != null && strHashtag.Count > 0 && !string.IsNullOrWhiteSpace(strHashtag[0]))
                    {
                        query = query.Where(x => strHashtag.Intersect(x.SearchHashTag).Any())
                                .OrderByDescending(x => x.date)
                                .Skip((model.PageIndex - 1) * model.PageSize)
                                .Take(GlobalConfig.PageSize)
                                .ToList();
                    }
                    else
                    {
                        query = query
                                .OrderByDescending(x => x.date)
                                .Skip((model.PageIndex - 1) * model.PageSize)
                                .Take(GlobalConfig.PageSize)
                                .ToList();

                    }

                    List<GetPostModelRecent> searchPostModel = query.Select(post => new GetPostModelRecent()
                    {
                        PostId = post.PostId,
                        UserId = post.UserId,
                        UserName = post.UserName,
                        NickName = post.NickName,
                        PostByRole = _user.GetRoleById(post.UserId),
                        TotalLikes = post.TotalLikes,
                        isUserLike = post.isUserLike,
                        TotalComment = post.TotalComment,
                        PostCountShare = post.PostCountShare,
                        Isvideo = post.Isvideo,
                        date = post.date,
                        FileCount = post.FileCount,
                        FileName = post.FileName,
                        Description = post.Description,
                        SearchHashTag = post.SearchHashTag
                    }).ToList();

                    return searchPostModel;
                }
                else
                {
                    var query = await (from pst in _ctx.Post
                                       where pst.PostBy.IsActive
                                       select new GetPostModelRecent
                                       {
                                           PostId = pst.Id,
                                           UserId = pst.UserId,
                                           SearchHashTag = pst.PostHashTags.Select(x => x.Hashtag)
                                       })
                                  .ToListAsync();

                    if (strHashtag != null && strHashtag.Count > 0 && !String.IsNullOrWhiteSpace(strHashtag[0]))
                    {
                        return query.Where(x => strHashtag.Intersect(x.SearchHashTag).Any()).ToList();
                    }
                    else
                    {
                        return query.ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public async Task<GetPostListByUser> PostListByUser(PaginationModel model)
        {
            if (model.PageIndex > 0)
            {
                GetPostListByUser modelData = new GetPostListByUser();
                modelData.User = await _coreuser.GetUserDetail(model.UserId);
                var query = await (from pst in _ctx.Post
                                   join fil in _ctx.PostTransactions on pst.Id equals fil.PostId into HasFile
                                   where pst.PostBy.Id == model.UserId
                                   select new
                                   {
                                       PostId = pst.Id,
                                       pst.UserId,
                                       UserName = pst.PostBy.Name,
                                       NickName = pst.PostBy.UserName,
                                       TotalLikes = pst.PostLikes.Count,
                                       isUserLike = pst.PostLikes.Where(x => x.UserId == model.UserId && x.PostId == pst.Id).Any(),
                                       TotalComment = pst.PostComments.Count,
                                       Isvideo = (HasFile.Any() ? HasFile.FirstOrDefault().IsVideo : false),
                                       date = pst.CreatedDateTimeUTC,
                                       HasFile.FirstOrDefault().FileName,
                                       FileCount = (HasFile.Any() ? HasFile.Count() : 0),
                                       pst.Description
                                   }).OrderByDescending(x => x.date)
                                   .Skip((model.PageIndex - 1) * model.PageSize)
                                .Take(GlobalConfig.PageSize)
                                .ToListAsync();

                modelData.RecentPost = query.Select(post => new GetPostModelRecent()
                {
                    PostId = post.PostId,
                    UserId = post.UserId,
                    UserName = post.UserName,
                    NickName = post.NickName,
                    PostByRole = _user.GetRoleById(post.UserId),
                    TotalLikes = post.TotalLikes,
                    isUserLike = post.isUserLike,
                    TotalComment = post.TotalComment,
                    Isvideo = post.Isvideo,
                    date = post.date,
                    FileCount = post.FileCount,
                    FileName = post.FileName,
                    Description = post.Description
                }).ToList();

                modelData.CategoryList = _catrepo.GetPostCategoryList(_user.GetLanguage(model.UserId));

                return modelData;
            }
            else
            {
                GetPostListByUser modelData = new GetPostListByUser();
                var query = await (from pst in _ctx.Post
                                   where pst.PostBy.IsActive && pst.PostBy.Id == model.UserId
                                   select new GetPostModelRecent
                                   {
                                       PostId = pst.Id,
                                       UserId = pst.UserId,
                                       SearchHashTag = pst.PostHashTags.Select(x => x.Hashtag)
                                   })
                              .ToListAsync();

                modelData.RecentPost = query.ToList();

                return modelData;
            }
        }

        public async Task<ResponseModel<object>> DeleteComment(long commentid, long replyid)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (replyid > 0)
                {
                    ReplyComments objComments = await _ctx.ReplyComments.FirstOrDefaultAsync(x => x.Id == replyid);
                    if (objComments == null)
                    {
                        mResult.Message = "";
                        mResult.Status = ResponseStatus.Failed;
                    }
                    else
                    {
                        Notification notification = _ctx.Notification
                                              .Where(x => x.ReplyCommentId == replyid)
                                              .FirstOrDefault();

                        if (notification != null)
                        {
                            _ctx.Notification.Remove(notification);
                            _ctx.SaveChanges();
                        }

                        _ctx.ReplyComments.Remove(objComments);
                        await _ctx.SaveChangesAsync();

                        mResult.Result = "";
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "gepostet";
                    }
                }
                else
                {
                    Comments objComments = await _ctx.Comments.FirstOrDefaultAsync(x => x.Id == commentid);
                    if (objComments == null)
                    {
                        mResult.Message = "";
                        mResult.Status = ResponseStatus.Failed;
                    }
                    else
                    {
                        Notification notification = _ctx.Notification
                                                .Where(x => x.CommentId == commentid)
                                               .FirstOrDefault();
                        if (notification != null)
                        {
                            _ctx.Notification.Remove(notification);
                            _ctx.SaveChanges();
                        }

                        _ctx.Comments.Remove(objComments);
                        await _ctx.SaveChangesAsync();


                        mResult.Result = "";
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "gepostet";
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SpamComment(long commentid)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Comments objComments = await _ctx.Comments.FirstOrDefaultAsync(x => x.Id == commentid);
                if (objComments == null)
                {
                    mResult.Message = "";
                    mResult.Status = ResponseStatus.Failed;
                }
                else
                {
                    objComments.IsSpam = true;
                    await _ctx.SaveChangesAsync();
                    mResult.Result = "";
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "gepostet";
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddPostCommentByUser(long postid, string userId, string message, long CommentId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (CommentId > 0)
                {
                    ReplyComments objComments = new ReplyComments();

                    objComments.CommentId = CommentId;
                    objComments.Message = message;
                    objComments.UserId = userId;

                    _ctx.ReplyComments.Add(objComments);
                    int id = await _ctx.SaveChangesAsync();
                    mResult.Result = objComments.Id;

                    if (_ctx.Post.Where(x => x.Id == postid).Select(x => x.PostBy.Id).FirstOrDefault() != userId)
                    {
                        Notification notification = new Notification
                        {
                            SenderId = userId,
                            ReceiverId = _ctx.Comments.Where(x => x.Id == CommentId).Select(x => x.CommentedBy.Id).FirstOrDefault(),
                            NotificationType = NotificationType.ReplyOfComment,
                            rating = 0,
                            IsRead = false,
                            PostId = postid,
                            ReplyCommentId = objComments.Id,
                            NotificationText = "reply of your comment on post",
                        };
                        _ctx.Notification.Add(notification);
                        _ctx.SaveChanges();
                    }
                }
                else
                {
                    Comments objComments = new Comments();

                    objComments.PostId = postid;
                    objComments.Message = message;
                    objComments.UserId = userId;
                    objComments.IsSpam = false;

                    _ctx.Comments.Add(objComments);
                    int id = await _ctx.SaveChangesAsync();
                    mResult.Result = objComments.Id;

                    if (_ctx.Post.Where(x => x.Id == postid).Select(x => x.PostBy.Id).FirstOrDefault() != userId)
                    {
                        Notification notification = new Notification
                        {
                            SenderId = userId,
                            ReceiverId = _ctx.Post.Where(x => x.Id == postid).Select(x => x.PostBy.Id).FirstOrDefault(),
                            NotificationType = NotificationType.Comment,
                            rating = 0,
                            IsRead = false,
                            PostId = postid,
                            CommentId = objComments.Id,
                            NotificationText = Resource.CommentedOnYourPost,
                        };
                        _ctx.Notification.Add(notification);
                        _ctx.SaveChanges();
                    }
                }
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "gepostet";
            }
            catch (DbEntityValidationException ex)
            {

            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddSharePost(long id, string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                PostShare objPostShare = new PostShare();
                objPostShare.PostId = id;
                objPostShare.UserId = UserId;
                _ctx.PostShare.Add(objPostShare);
                await _ctx.SaveChangesAsync();

                mResult.Result = _ctx.PostShare.Where(x => x.PostId == id).Count();
                mResult.Status = ResponseStatus.Success;
                mResult.Message = "sharepost";
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<List<HashtagFilterModel>> FilterHashtaglist(PostFilterModel model, bool isCount, string strLocalLatitude, string strLocalLongitude)
        {
            try
            {
                if (isCount)
                {
                    if (model.CategoryIds != null)
                    {
                        return await (from pst in _ctx.PostHashtags
                                      where model.CategoryIds.Intersect(pst.Post.PostCategories.Select(x => x.CategoryId)).Any()
                                      group pst by new { pst.Hashtag } into g
                                      select new HashtagFilterModel
                                      {
                                          Hashtag = g.Key.Hashtag,
                                          PostCount = g.Count()
                                      }).Where(x => x.Hashtag.Contains(model.SearchText)).ToListAsync();
                    }
                    else
                    {
                        return await (from pst in _ctx.PostHashtags
                                          //where model.CategoryIds.Intersect(pst.Post.PostCategories.Select(x => x.CategoryId)).Any()
                                      group pst by new { pst.Hashtag } into g

                                      select new HashtagFilterModel
                                      {
                                          Hashtag = g.Key.Hashtag,
                                          PostCount = g.Count()
                                      }).Where(x => x.Hashtag.Contains(model.SearchText)).ToListAsync();
                    }
                }
                else
                {
                    if (model.CategoryIds != null)
                    {
                        var result = await (from pst in _ctx.PostHashtags
                                            where model.CategoryIds.Intersect(pst.Post.PostCategories.Select(x => x.CategoryId)).Any()
                                            group pst by new { pst.Hashtag } into g
                                            select new HashtagFilterModel
                                            {
                                                Hashtag = g.Key.Hashtag,
                                                PostCount = g.Count()
                                            }).Where(x => x.Hashtag.Contains(model.SearchText))
                                      .OrderByDescending(g => g.PostCount)
                                       .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();
                        return result;
                    }
                    else
                    {
                        var result = await (from pst in _ctx.PostHashtags
                                            group pst by new { pst.Hashtag } into g
                                            //orderby g.Count() descending
                                            select new HashtagFilterModel
                                            {
                                                Hashtag = g.Key.Hashtag,
                                                PostCount = g.Count()
                                            }).Where(x => x.Hashtag.Contains(model.SearchText))
                                        .OrderByDescending(x => x.PostCount)
                                         .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();
                        return result;

                    }
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public async Task<List<GetPostModelPopular>> FilterPostlist(PostFilterModel model, string UserId, bool isCount, string strLocalLatitude, string strLocalLongitude)
        {
            try
            {
                if (isCount)
                {
                    var result = (from pst in _ctx.Post
                                  join ctry in _ctx.PostCategory on pst.Id equals ctry.PostId into HasCategory
                                  where (pst.PostBy.IsActive) && pst.Description.Contains(model.SearchText)
                                  select new GetPostModelPopular
                                  {
                                      PostId = pst.Id,
                                      Categories = HasCategory.Select(x => x.CategoryId)
                                  });

                    if (result != null && model.CategoryIds != null)
                    {
                        return await result.Where(x => model.CategoryIds.Intersect(x.Categories).Any()).ToListAsync();
                    }
                    else
                    {
                        return await result.ToListAsync();
                    }
                }
                else
                {
                    ApplicationUser objUser = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == UserId);

                    double? userLatitude = (objUser.GeoLocation != null ? objUser.GeoLocation.Latitude : 0);
                    double? userLongitude = (objUser.GeoLocation != null ? objUser.GeoLocation.Longitude : 0);

                    if (!String.IsNullOrEmpty(strLocalLatitude))
                    {
                        userLatitude = Convert.ToDouble(strLocalLatitude);
                    }
                    if (!String.IsNullOrEmpty(strLocalLongitude))
                    {
                        userLongitude = Convert.ToDouble(strLocalLongitude);
                    }

                    var query = (from pst in _ctx.Post
                                 join ctry in _ctx.PostCategory on pst.Id equals ctry.PostId into HasCategory
                                 join fil in _ctx.PostTransactions on pst.Id equals fil.PostId into HasFile
                                 where (pst.PostBy.IsActive) && pst.Description.Contains(model.SearchText)

                                 select new GetPostModelPopular
                                 {
                                     PostId = pst.Id,
                                     UserId = pst.UserId,
                                     UserName = pst.PostBy.Name,
                                     NickName = pst.PostBy.UserName,
                                     Description = pst.Description,
                                     IsVideo = HasFile.Any() ? HasFile.FirstOrDefault().IsVideo : false,
                                     FileName = HasFile.Any() ? HasFile.FirstOrDefault().FileName : "Default-Image.jpg",
                                     Latitude = (pst.PostBy.GeoLocation != null ? pst.PostBy.GeoLocation.Latitude : 0),
                                     Longitude = (pst.PostBy.GeoLocation != null ? pst.PostBy.GeoLocation.Longitude : 0),
                                     Categories = HasCategory.Select(x => x.CategoryId),
                                     TotalLikes = pst.PostLikes.Count
                                 });

                    var result = await query.ToListAsync();

                    if (query != null && model.CategoryIds != null)
                    {
                        result = await query.Where(x => model.CategoryIds.Intersect(x.Categories).Any()).ToListAsync();
                    }

                    List<GetPostModelPopular> data = result.Select(post => new GetPostModelPopular()
                    {
                        PostId = post.PostId,
                        UserId = post.UserId,
                        UserName = post.UserName,
                        NickName = post.NickName,
                        PostByRole = _user.GetRoleById(post.UserId),
                        Description = post.Description,
                        //IsVideo = post.IsVideo,
                        FileName = (post.IsVideo == true ? "DefaultVideoIcon.jpg" : post.FileName),
                        Distance = Utility.GetDistanceInMiles(userLatitude, userLatitude, post.Latitude, post.Longitude),
                        TotalLikes = post.TotalLikes
                    }).OrderBy(x => x.Distance).ThenByDescending(x => x.TotalLikes)
                    .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize)
                    .ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public async Task<ResponseModel<object>> AddEditPost(AddPostModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Post objPost = await _ctx.Post.FirstOrDefaultAsync(x => x.Id == model.Id);
                bool isNew = false;
                if (objPost == null)
                {
                    objPost = new Post();
                    isNew = true;
                };
                objPost.UserId = model.UserId;
                objPost.Description = model.Description;

                if (!isNew)
                {
                    _ctx.PostCategory.RemoveRange(objPost.PostCategories);
                    await _ctx.SaveChangesAsync();
                }
                objPost.PostCategories = (from fl in model.PostCategoryList
                                          where fl.Selected == true
                                          select new PostCategory
                                          {
                                              PostId = objPost.Id,
                                              CategoryId = fl.Id
                                          }).ToList();

                if (!isNew)
                {
                    _ctx.PostHashtags.RemoveRange(objPost.PostHashTags);
                    await _ctx.SaveChangesAsync();
                }
                if (model.HashtagsArray != null)
                {
                    //var regex = new Regex(Regex.Escape("#"));
                    objPost.PostHashTags = (from fl in model.HashtagsArray
                                            select new PostHashtags
                                            {
                                                PostId = objPost.Id,
                                                //Hashtag = regex.Replace(fl, "", 1)
                                                Hashtag = fl.Replace("#", "")
                                            }).ToList();
                }

                if (!isNew)
                {
                    _ctx.PostTransactions.RemoveRange(objPost.PostFiles);
                    await _ctx.SaveChangesAsync();
                }
                if (model.PostFilesNamesArray != null)
                {
                    objPost.PostFiles = (from fl in model.PostFilesNamesArray
                                         select new PostTransaction
                                         {
                                             PostId = objPost.Id,
                                             FileName = fl,
                                             IsVideo = checkVideoFile(fl)
                                         }).ToList();
                }

                if (isNew)
                {
                    _ctx.Post.Add(objPost);
                }
                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    if (isNew)
                    {
                        mResult.Message = Resource.postAddSuccess;
                    }
                    else
                    {
                        mResult.Message = Resource.EditPostSuccess;
                    }
                }
                mResult.Result = new { };
                mResult.Status = ResponseStatus.Success;
            }
            catch (DbEntityValidationException ex)
            {

            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public bool checkVideoFile(string filename)
        {
            bool isVideo = false;
            foreach (string x in GlobalConfig.mediaExtensions)
            {
                if (filename.Contains(x))
                {
                    isVideo = true;
                    break;
                }
            }
            return isVideo;
        }

        public async Task<ResponseModel<object>> EditPost(AddPostModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object> { };

            Post objPost = await _ctx.Post.FirstOrDefaultAsync(x => x.Id == model.Id);
            if (objPost == null)
            {
                mResult.Message = Resource.editPostNotFoundError;
                mResult.Result = model;
            }
            else
            {
                objPost.UserId = model.UserId;
                objPost.Description = model.Description;

                _ctx.PostCategory.RemoveRange(objPost.PostCategories);
                await _ctx.SaveChangesAsync();
                objPost.PostCategories = (from fl in model.PostCategoryList
                                          where fl.Selected == true
                                          select new PostCategory
                                          {
                                              PostId = objPost.Id,
                                              CategoryId = fl.Id
                                          }).ToList();

                _ctx.PostHashtags.RemoveRange(objPost.PostHashTags);
                await _ctx.SaveChangesAsync();
                if (model.HashtagsArray != null)
                {
                    objPost.PostHashTags = (from fl in model.HashtagsArray
                                            select new PostHashtags
                                            {
                                                PostId = objPost.Id,
                                                Hashtag = fl
                                            }).ToList();
                }

                _ctx.PostTransactions.RemoveRange(objPost.PostFiles);
                await _ctx.SaveChangesAsync();
                if (model.PostFilesNamesArray != null)
                {
                    objPost.PostFiles = (from fl in model.PostFilesNamesArray
                                         select new PostTransaction
                                         {
                                             PostId = objPost.Id,
                                             FileName = fl,
                                             IsVideo = checkVideoFile(fl)
                                         }).ToList();
                }
                await _ctx.SaveChangesAsync();
                mResult.Result = new { };
                mResult.Status = ResponseStatus.Success;
                mResult.Message = Resource.EditPostSuccess;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteSpamComment(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Comments cmnt = _ctx.Comments.FirstOrDefault(x => x.Id == id);
                if (cmnt == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = "no comment found";
                }
                else
                {
                    var replycomment = _ctx.ReplyComments.Where(x => x.CommentId == id).ToList();
                    _ctx.ReplyComments.RemoveRange(replycomment);

                    var notificaion = _ctx.Notification.Where(x => x.CommentId == id).ToList();
                    _ctx.Notification.RemoveRange(notificaion);
                    _ctx.Comments.Remove(cmnt);
                }
                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Comment deleted";
                }
                else
                {
                    mResult.Result = new object { };
                    mResult.Message = "Failed to delete comment";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeletePost(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Post objpost = _ctx.Post.FirstOrDefault(x => x.Id == id);
                if (objpost == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = Resource.editPostNotFoundError;
                    mResult.Status = ResponseStatus.Failed;
                }
                else
                {
                    var existingPostTransaction = _ctx.PostTransactions.Where(x => x.PostId == id).ToList();

                    foreach (var item in existingPostTransaction)
                    {
                        string FilePath = GlobalConfig.PostImagePath + item.FileName;
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }

                        if (System.IO.File.Exists(FilePath + ".jpg"))
                        {
                            System.IO.File.Delete(FilePath + ".jpg");
                        }
                    }
                    _ctx.PostTransactions.RemoveRange(existingPostTransaction);
                    _ctx.PostHashtags.RemoveRange(_ctx.PostHashtags.Where(x => x.PostId == id).ToList());
                    _ctx.PostCategory.RemoveRange(_ctx.PostCategory.Where(x => x.PostId == id).ToList());

                    _ctx.ReplyComments.RemoveRange(_ctx.ReplyComments.Where(x => x.Comment.PostId == id).ToList());

                    _ctx.PostReport.RemoveRange(_ctx.PostReport.Where(x => x.PostId == id).ToList());
                    _ctx.Comments.RemoveRange(_ctx.Comments.Where(x => x.PostId == id).ToList());
                    _ctx.Likes.RemoveRange(_ctx.Likes.Where(x => x.PostId == id).ToList());

                    _ctx.Notification.RemoveRange(_ctx.Notification.Where(x => x.PostId == id));

                    _ctx.Post.Remove(objpost);
                }
                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Post gelöscht";
                }
                else
                {
                    mResult.Result = new object { };
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.deletePostFailed;
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<AddPostModel> GetPostdetailEdit(languageType languageid, long Id, string UserId)
        {
            AddPostModel result = await (from p in _ctx.Post
                                         where p.Id == Id
                                         && p.UserId == UserId
                                         select new AddPostModel
                                         {
                                             Id = p.Id,
                                             UserId = p.UserId,
                                             Description = p.Description,
                                             PostHashtags = p.PostHashTags.Where(x => x.PostId == Id).Select(x => new PostHashtagsModel
                                             {
                                                 Id = x.Id,
                                                 PostId = p.Id,
                                                 Hashtag = x.Hashtag
                                             }).ToList(),
                                             PostCategoryList = (from cat in _ctx.Categories
                                                                 join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageid) on cat.Id equals ctlng.CategoryId into HasCategory
                                                                 let pstcat = _ctx.PostCategory.Where(x => x.PostId == Id)
                                                                 select new PostCategoryModel
                                                                 {
                                                                     Id = cat.Id,
                                                                     CategoryName = HasCategory.FirstOrDefault().Name,
                                                                     Selected = (pstcat.Where(x => x.CategoryId == cat.Id).Any() ? true : false)
                                                                 }).OrderBy(x => x.CategoryName).ToList(),
                                             PostFiles = p.PostFiles.Where(x => x.PostId == Id).Select(x => new PostTransactionModel
                                             {
                                                 PostId = p.Id,
                                                 FileName = x.FileName,
                                                 IsVideo = x.IsVideo,
                                             }).ToList(),
                                         }).FirstOrDefaultAsync();
            if (result != null)
            {
                //result.HashtagsArray = _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).Any() ? _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).ToArray() : null;
                result.HiddenHashtags = String.Join(",", _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).ToArray());
                result.PostFilesNamesArray = _ctx.PostTransactions.Where(x => x.PostId == Id).Select(x => x.FileName).ToArray();
                result.HiddenPostFileName = String.Join(",", _ctx.PostTransactions.Where(x => x.PostId == Id).Select(x => x.FileName).ToArray());
                result.HiddenPostFilePath = String.Join(",", _ctx.PostTransactions.Where(x => x.PostId == Id).Select(x => GlobalConfig.PostImageUrl + x.FileName).ToArray());

                result.HashtagsArray = new[] { String.Join(",", _ctx.PostHashtags.Where(x => x.PostId == Id).Select(x => x.Hashtag).ToArray()) };
            }
            return result;
        }

        //Admin
        public async Task<PostDetailModel> GetPostDetail(long Id, string UserId)
        {
            try
            {
                PostDetailModel Postdetail = new PostDetailModel();
                Postdetail = await (from post in _ctx.Post
                                    where post.Id == Id
                                    select new PostDetailModel
                                    {
                                        PostId = Id,
                                        StylishId = post.UserId,
                                        Name = post.PostBy.Name,
                                        NickName = post.PostBy.UserName,
                                        Description = post.Description,
                                        ProfileImageNAme = post.PostBy.ProfileImageName,
                                        PostComments = post.PostComments.Count(),
                                        PostLikes = post.PostLikes.Count(),
                                        CreatedDate = post.CreatedDateTimeUTC
                                    }).FirstOrDefaultAsync();

                Postdetail.Timeago = Utility.TimeAgo(Postdetail.CreatedDate);
                Postdetail.PostFiles = getPostFiles(Id);
                Postdetail.Comments = getPostComments(Id);
                if (UserId != null)
                {
                    Postdetail.IsFollow = CheckFollow(Postdetail.StylishId, UserId);
                    Postdetail.IsLike = CheckLikes(Id, UserId);
                }
                return Postdetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<PostViewDetailModel> GetPostViewDetailModel(languageType languageid, long postID, string userId)
        {
            try
            {
                PostViewDetailModel Postdetail = new PostViewDetailModel();
                Postdetail = await (from post in _ctx.Post
                                    where post.Id == postID
                                    select new PostViewDetailModel
                                    {
                                        PostId = postID,
                                        UserId = post.UserId,
                                        UserRole = post.PostBy.Role,
                                        Name = post.PostBy.Name,
                                        UserName = "@" + post.PostBy.UserName,
                                        Description = post.Description,
                                        ProfileImageName = post.PostBy.ProfileImageName,
                                        PostCountComments = post.PostComments.Count(),
                                        PostCountLikes = post.PostLikes.Count(),
                                        PostCountShare = post.PostShare.Count(),
                                    }).FirstOrDefaultAsync();

                Postdetail.PostFiles = getPostFiles(postID);
                Postdetail.Comments = getPostComments(postID);
                Postdetail.ReplyComments = getPostReply(postID);
                Postdetail.PostHashtag = getPostHashtag(postID);
                if (userId != null)
                {
                    Postdetail.IsLike = CheckLikes(postID, userId);
                }
                return Postdetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool CheckFollow(string StylishId, string UserId)
        {
            return (from flw in _ctx.Followers
                    where flw.UserId == UserId && flw.FollowingId == StylishId
                    select flw).Any();
        }

        public async Task<PostDetailModel> PostDetailById(long Id)
        {
            try
            {
                PostDetailModel Postdetail = new PostDetailModel();
                Postdetail = await (from post in _ctx.Post
                                    where post.Id == Id
                                    select new PostDetailModel
                                    {
                                        PostId = Id,
                                        StylishId = post.UserId,
                                        Name = post.PostBy.Name,
                                        NickName = post.PostBy.UserName,
                                        Description = post.Description,
                                        ProfileImageNAme = post.PostBy.ProfileImageName,
                                        PostComments = post.PostComments.Count(),
                                        PostLikes = post.PostLikes.Count(),
                                        CreatedDate = post.CreatedDateTimeUTC,
                                        HashTags = post.PostHashTags.Select(x => new PostHashTaglistModel
                                        {
                                            hashtag = x.Hashtag
                                        }).ToList()
                                    }).FirstOrDefaultAsync();

                Postdetail.Timeago = Utility.TimeAgo(Postdetail.CreatedDate);
                Postdetail.PostFiles = getPostFiles(Id);
                Postdetail.Comments = getPostComments(Id);
                return Postdetail;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<List<ViewLikeModel>> ViewLikeActivity(FindLikeModel model, bool isCount)
        {
            try
            {
                if (isCount)
                {
                    return await (from like in _ctx.Likes
                                  join usr in _ctx.Users on like.UserId equals usr.Id into HasUser
                                  join flw in _ctx.Followers on like.UserId equals flw.FollowingId into HasFollower
                                  where like.PostId == model.PostId
                                  select new ViewLikeModel
                                  {
                                      UserId = like.LikeBy.Id,
                                      Name = like.LikeBy.Name,
                                      UserName = like.LikeBy.UserName,
                                      UserImage = like.LikeBy.ProfileImageName,
                                      isFollow = _ctx.Followers.Any(x => x.FollowingId == like.UserId && x.UserId == model.UserId)
                                  }).ToListAsync();
                }
                else
                {
                    return await (from like in _ctx.Likes
                                  join usr in _ctx.Users on like.UserId equals usr.Id into HasUser
                                  join flw in _ctx.Followers on like.UserId equals flw.FollowingId into HasFollower
                                  where like.PostId == model.PostId
                                  select new ViewLikeModel
                                  {
                                      UserId = like.LikeBy.Id,
                                      Name = like.LikeBy.Name,
                                      UserName = like.LikeBy.UserName,
                                      UserImage = like.LikeBy.ProfileImageName,
                                      isFollow = _ctx.Followers.Any(x => x.FollowingId == like.UserId && x.UserId == model.UserId)
                                  }).OrderBy(x => x.Name)
                                       .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public async Task<ResponseModel<object>> AddPostReportByUser(PostReportModel model)
        {

            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                PostReport rprt = new PostReport();
                rprt = new PostReport
                {
                    PostId = model.PostId,
                    UserId = model.UserId,
                    Reason = model.Reason,
                };
                _ctx.PostReport.Add(rprt);

                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    mResult.Result = new { };
                    mResult.Status = ResponseStatus.Success;
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        public IQueryable<getAllPostlist> GetPostListForAdmin()
        {
            return (from pst in _ctx.Post
                        //join hastag in _ctx.PostHashtags on pst.Id equals hastag.PostId
                    select new getAllPostlist
                    {
                        PostId = pst.Id,
                        StylishId = pst.UserId,
                        Name = pst.PostBy.Name,
                        ProfileImageNAme = pst.PostBy.ProfileImageName,
                        Description = pst.Description,
                        CreatedDate = pst.CreatedDateTimeUTC,
                        PostPic = pst.PostFiles.FirstOrDefault(x => x.IsVideo == false).FileName,
                        HasTag = pst.PostHashTags.Select(x => new PostHashTaglistModel
                        {
                            hashtag = x.Hashtag
                        }).ToList()
                    }).OrderBy(x => x.CreatedDate);
        }

        public IQueryable<ReportedPostList> GetReportedPostlist()
        {
            return (from report in _ctx.PostReport
                    select new ReportedPostList
                    {
                        PostId = report.PostId,
                        StylishId = report.Post.PostBy.Id,
                        Name = report.Post.PostBy.Name,
                        reportedbyUserId = report.ReportBy.Id,
                        reporedbyName = report.ReportBy.Name,
                        ProfileImageNAme = report.ReportBy.ProfileImageName,
                        CreatedDate = report.CreatedDateTimeUTC,
                        Reason = report.Reason
                    }).OrderBy(x => x.CreatedDate);
        }

        public IQueryable<SpamCommentsListModel> GetSpamComments()
        {
            return (from Cmnt in _ctx.Comments
                    where Cmnt.IsSpam == true
                    select new SpamCommentsListModel
                    {
                        CoomentId = Cmnt.Id,
                        PostId = Cmnt.PostId,
                        PostByName = Cmnt.Post.PostBy.Name,
                        CommentedById = Cmnt.CommentedBy.Id,
                        CommentedByName = Cmnt.CommentedBy.Name,
                        Commenttext = Cmnt.Message
                    }).OrderBy(x => x.PostByName);
        }

        public List<PostCommentModel> getPostComments(long PostId)
        {
            var query = (from pst in _ctx.Comments
                         join usr in _ctx.Users on pst.UserId equals usr.Id
                         where pst.PostId == PostId
                         select new
                         {
                             CommentId = pst.Id,
                             PostId,
                             pst.Message,
                             CommentedBy = pst.UserId,
                             usr.Name,
                             usr.UserName,
                             usr.Role,
                             ProfileImage = usr.ProfileImageName,
                             CommentedOn = pst.CreatedDateTimeUTC
                         }).OrderByDescending(x => x.CommentedOn).ToList();

            List<PostCommentModel> postComments = query.Select(post => new PostCommentModel()
            {
                CommentId = post.CommentId,
                PostId = post.PostId,
                Message = post.Message,
                CommentedBy = post.CommentedBy,
                Name = post.Name,
                UserName = post.UserName,
                UserRole = post.Role,
                ProfileImage = post.ProfileImage,
                CommentedOn = post.CommentedOn,
                CommentedTimeSpan = Utility.GetTimeSpan(post.CommentedOn)
            }).ToList();

            return postComments;
        }
        
        private List<ReplyCommentsModel> getPostReply(long postID)
        {
            var query = (from pst in _ctx.ReplyComments
                         join cmt in _ctx.Comments on pst.CommentId equals cmt.Id
                         join usr in _ctx.Users on pst.UserId equals usr.Id
                         //where pst.PostId == PostId
                         select new
                         {
                             ReplyId = pst.Id,
                             pst.CommentId,
                             pst.Message,
                             CommentedBy = pst.UserId,
                             usr.Role,
                             usr.Name,
                             usr.UserName,
                             ProfileImage = usr.ProfileImageName,
                             CommentedOn = pst.CreatedDateTimeUTC
                         }).OrderByDescending(x => x.CommentedOn).ToList();

            List<ReplyCommentsModel> postComments = query.Select(post => new ReplyCommentsModel()
            {
                ReplyId = post.ReplyId,
                CommentId = post.CommentId,
                Message = post.Message,
                CommentedBy = post.CommentedBy,
                Name = post.Name,
                UserName = post.UserName,
                UserRole = post.Role,
                ProfileImage = post.ProfileImage,
                CommentedOn = post.CommentedOn,
                CommentedTimeSpan = Utility.GetTimeSpan(post.CommentedOn)
            }).ToList();

            return postComments;
        }

        public List<PostFilesModel> getPostFiles(long PostId)
        {
            return (from postTran in _ctx.PostTransactions
                    where postTran.PostId == PostId
                    select new PostFilesModel
                    {
                        PostId = PostId,
                        FileName = postTran.FileName,
                        IsVideo = postTran.IsVideo
                    }).ToList();
        }

        private List<PostHashtagsModel> getPostHashtag(long postID)
        {
            return (from postTran in _ctx.PostHashtags
                    where postTran.PostId == postID
                    select new PostHashtagsModel
                    {
                        PostId = postID,
                        Hashtag = postTran.Hashtag
                    }).ToList();
        }
        
        public bool CheckLikes(long PostId, string UserId)
        {
            return (from lik in _ctx.Likes
                    where lik.PostId == PostId && lik.UserId == UserId
                    select lik).Any();
        }

        public async Task<ResponseModel<object>> AddComment(PostCommentModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Comments cmnt = new Comments();
                cmnt = new Comments
                {
                    PostId = model.PostId,
                    UserId = model.CommentedBy,
                    Message = model.Message,
                };
                _ctx.Comments.Add(cmnt);

                int id = await _ctx.SaveChangesAsync();
                if (id > 0)
                {
                    mResult.Result = new { };
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "erfolgreich erstellt";
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = "";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        public async Task<int> addOrRemoveLike(long PostId, string UserId)
        {
            int likeid = 0;
            try
            {
                Likes like = new Likes();
                bool isLike = CheckLikes(PostId, UserId);
                if (isLike)
                {
                    like = await (from likes in _ctx.Likes
                                  where likes.PostId == PostId && likes.UserId == UserId
                                  select likes).FirstOrDefaultAsync();
                    _ctx.Likes.Remove(like);
                    likeid = await _ctx.SaveChangesAsync();

                    Notification notification = _ctx.Notification
                                                .Where(x => x.SenderId == UserId
                                                && x.ReceiverId == _ctx.Post.Where(y => y.Id == PostId).Select(y => y.PostBy.Id).FirstOrDefault()
                                                && x.PostId == PostId
                                                && x.NotificationType == NotificationType.like)
                                                .FirstOrDefault();
                    if (notification != null)
                    {
                        _ctx.Notification.Remove(notification);
                        int notify = await _ctx.SaveChangesAsync();
                    }
                    if (likeid > 0)
                    {
                        likeid = 1;
                        return likeid;
                    }
                }
                else
                {
                    like = new Likes
                    {
                        UserId = UserId,
                        PostId = PostId
                    };
                    _ctx.Likes.Add(like);
                    likeid = await _ctx.SaveChangesAsync();
                    if (likeid > 0)
                    {
                        likeid = 2;

                        if (_ctx.Post.Where(x => x.Id == PostId).Select(x => x.PostBy.Id).FirstOrDefault() != UserId)
                        {
                            Notification notification = new Notification
                            {
                                SenderId = UserId,
                                ReceiverId = _ctx.Post.Where(x => x.Id == PostId).Select(x => x.PostBy.Id).FirstOrDefault(),
                                NotificationType = NotificationType.like,
                                rating = 0,
                                IsRead = false,
                                PostId = PostId,
                                NotificationText = Resource.NotificationLikePost,
                            };
                            _ctx.Notification.Add(notification);
                            int notify = await _ctx.SaveChangesAsync();
                        }
                        return likeid;
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return likeid;
        }

        public double CountLikebypost(long PostId)
        {
            return _ctx.Likes.Count(x => x.PostId == PostId);
        }
    }
}




