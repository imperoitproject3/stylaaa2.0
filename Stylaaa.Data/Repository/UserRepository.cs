﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Core.Models.GoogleModel;
using Stylaaa.Core.Models.StripeModels;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Helper;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using Stylaaa.Stripe;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Stylaaa.Data.Repository
{
    public class UserRepository : IDisposable
    {
        private ApplicationDbContext _app;
        private readonly IOwinContext _owin;

        private readonly ApplicationUserManager _userManager;
        private readonly IAuthenticationManager _authenticationmanager;
        private readonly StylishRepository _userrepo;
        private readonly CoreUserRepository _coreUserRepo;
        private ChatRepository _chat;

        public UserRepository()
        {
            _coreUserRepo = new CoreUserRepository();
            _userrepo = new StylishRepository();
            _app = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_app));
            _userManager.EmailService = new EmailService();
            _chat = new ChatRepository();
        }

        public UserRepository(IOwinContext owin)
        {
            _owin = owin;
            _app = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_app));
            _userManager.EmailService = new EmailService();
            _authenticationmanager = owin.Authentication;
        }

        private async Task SignInAsync(ApplicationUser user, bool isPersistent, bool rememberBrowser)
        {
            WebLogout();
            var userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
            userIdentity.AddClaim(new Claim("FullName", user.Name));
            userIdentity.AddClaim(new Claim("UserRole", user.Role.ToString()));
            _authenticationmanager.SignIn(
                new AuthenticationProperties { IsPersistent = isPersistent },
                userIdentity);
        }

        public object IsEmailExists(string email)
        {
            return _app.Users.Where(x => x.Email == email).Any();
        }

        public languageType GetLanguage(string userId)
        {
            var user = _userManager.FindById(userId);

            return user.Language;
        }

        public async Task<string> GetStripeAccoutId(string Userid)
        {
            var user = _app.Users.FirstOrDefault(x => x.Id == Userid);
            if (user != null)
            {
                if (user.StripeAccountId != null)
                {
                    return user.StripeAccountId;
                }
                else
                {
                    stCustomerService stripeService = new stCustomerService();
                    var stripeResult = await stripeService.Add(new StripeCustomerRequestModel()
                    {
                        email = user.Email,
                        name = user.Name
                    });
                    if (stripeResult.Status == ResponseStatus.Success)
                    {
                        user.StripeAccountId = stripeResult.Result.customerId;
                        await _userManager.UpdateAsync(user);
                        await _app.SaveChangesAsync();
                        return user.StripeAccountId;
                    }
                }

            }

            return null;
        }

        public async Task<ResponseModel<object>> AdminLogin(AdminLoginModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            //var user = _app.Users.Where(x => x.Email == model.emailId && (x.Role == Role.SuperAdmin || x.Role == Role.Admin)).FirstOrDefault();
            var user = await _userManager.FindByEmailAsync(model.emailId);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = Resource.invalidusername;
            }
            else
            {
                bool validRole = await _userManager.IsInRoleAsync(user.Id, Role.SuperAdmin.ToString());
                if (!validRole)
                    validRole = await _userManager.IsInRoleAsync(user.Id, Role.Admin.ToString());

                bool validPassword = await _userManager.CheckPasswordAsync(user, model.password);

                if (validRole)
                {
                    if (validPassword)
                    {
                        await SignInAsync(user, false, model.rememberMe);

                        //ClaimsIdentity userIdentity = await _userManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
                        //userIdentity.AddClaim(new Claim("email", user.Email));
                        //userIdentity.AddClaim(new Claim("password", model.password));                       

                        Random generator = new Random();
                        String otp = generator.Next(0, 999999).ToString("D6");
                        try
                        {
                            var emailTemplate = EmailTemplate.ConfirmOTP;
                            if (user.Language == languageType.German)
                            {
                                emailTemplate = EmailTemplate.ConfirmOTPDe;
                            }

                            Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                            dicPlaceholders.Add("{{name}}", user.Name);
                            dicPlaceholders.Add("{{otp}}", otp);
                            dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                            dicPlaceholders.Add("{{link}}", GlobalConfig.ActiveAccount + user.Id);

                            IdentityMessage message = new IdentityMessage();
                            message.Subject = string.Format("Welcome to {0}", GlobalConfig.ProjectName + ".com!");
                            message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                            message.Destination = user.Email;
                            await new EmailService().SendAsync(message);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                        mResult.Status = ResponseStatus.Success;
                        //mResult.Message = string.Format("User {0} Logged in successfully!", user.UserName);
                        mResult.Message = string.Format("OTP has been sent To E-mail: {0}", user.Email);
                        mResult.Result = otp;
                    }
                    else
                    {
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = Resource.invalidpassword;
                    }
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.invalidaccess;
                }
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AdminChangePassword(AdminChangePasswordModel model)
        {
            var userName = _owin.Authentication.User.Identity.Name;
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByNameAsync(userName);
            if (user == null)
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Invalid username or password";
            }
            else
            {
                IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.currentPassword, model.newPassword);
                if (result.Succeeded)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.updatePasswordSuccess;
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.invalidpassword;
                }
            }
            return mResult;
        }

        public string GetUserID(string strUserName)
        {
            ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.UserName == strUserName);
            return objUser.Id;
        }

        public string GetName(string userid)
        {
            ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.Id == userid);
            return objUser.Name;
        }

        public string GetEmail(string userid)
        {
            ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.Id == userid);
            return objUser.Email;
        }

        public string GetProfileImage(string userid)
        {
            ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.Id == userid);
            return objUser.ProfileImageName;
        }

        public bool CheckValidUserName(string strUserName)
        {
            ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.UserName == strUserName);
            if (objUser != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<ResponseModel<object>> UpdateCoverPic(string strFileName, string userid)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.Id == userid);
                if (objUser == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    //string FilePath = GlobalConfig.UserCoverPicPath + objUser.CoverImageName;
                    //if (System.IO.File.Exists(FilePath))
                    //{
                    //    System.IO.File.Delete(FilePath);
                    //}

                    objUser.CoverImageName = strFileName;
                    int i = await _app.SaveChangesAsync();
                    if (i > 0)
                    {
                        mResult.Result = new { };
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.CoverpicUploadSuccess;
                    }
                    else
                    {
                        mResult.Result = new { };
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = Resource.coverpicuploadfailed;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> UpdateProfileImage(string strFileName, string userid)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.Id == userid);
                if (objUser == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    string FilePath = GlobalConfig.UserImagePath + objUser.ProfileImageName;
                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Delete(FilePath);
                    }

                    objUser.ProfileImageName = strFileName;
                    int i = await _app.SaveChangesAsync();
                    if (i > 0)
                    {
                        mResult.Result = new { };
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.profilepicUploadsuccess;
                    }
                    else
                    {
                        mResult.Result = new { };
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = Resource.profilepicuploadfailed;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> AddAdminUser(AddAdminUserModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser data = await _userManager.FindByEmailAsync(model.Email);
            if (data == null)
            {
                data = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Email,
                    Name = model.Name,
                    GenderId = Gender.Male,
                    PasswordHash = model.Password,
                    Role = model.role,
                    CountryId = 17,
                    GeoLocation = Data_Utility.CreatePoint(0, 0)
                };
                IdentityResult result = await _userManager.CreateAsync(data, model.Password);
                if (result.Succeeded)
                {
                    var roleResult = await _userManager.AddToRoleAsync(data.Id, model.role.ToString());
                    if (roleResult.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        if (mResult.Status == ResponseStatus.Success)
                        {
                            mResult.Result = new UserResponseModel { IsProfileSetup = false, Role = model.role };
                            mResult.Message = Resource.registerSuccessMessage + GlobalConfig.ProjectName + ".";

                            //await SignInAsync(data, false, false);

                        }
                    }
                }
            }
            else
            {
                mResult.Message = "E-Mail Adresse existiert bereits!";
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddDummyUser(AddDummyUserModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ApplicationUser user = await _userManager.FindByEmailAsync(model.Email);
            if (user == null)
            {
                try
                {
                    if (model.ZipCode > 0)
                    {
                        ResponseModel<GoogleMapModel> googleResult = GoogleApi.GetGooglaMapResult(GoogleQueryParameters.address, model.ZipCode.ToString());
                        if (googleResult.Status == ResponseStatus.Success)
                        {
                            if (googleResult.Result.Results.Any())
                            {
                                Location loc = googleResult.Result.Results[0].Geometry.Location;
                                if (loc != null)
                                {
                                    model.LocationLatitude = loc.Lat;
                                    model.LocationLongitude = loc.Lng;
                                }
                            }
                        }
                    }
                }
                catch (Exception err)
                {
                    mResult.Status = ResponseStatus.Failed;
                }
                user = new ApplicationUser
                {
                    Email = model.Email,
                    UserName = model.Name,
                    Name = model.Name,
                    PasswordHash = model.Password,
                    Role = Role.User,
                    CountryId = 16,
                    ProfileImageName = model.ImageName,
                    isProfileCompleted = true,
                    GenderId = Gender.Male,
                    IsActive = true,
                    EmailConfirmed = true,
                    IsDummy = true,
                    GeoLocation = Data_Utility.CreatePoint(model.LocationLatitude, model.LocationLongitude)
                };

                model.Password = "123456";
                IdentityResult result = await _userManager.CreateAsync(user, model.Password);
                if (result.Succeeded)
                {
                    var roleResult = await _userManager.AddToRoleAsync(user.Id, model.role.ToString());
                    if (roleResult.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        if (mResult.Status == ResponseStatus.Success)
                        {
                            mResult.Result = new UserResponseModel { IsProfileSetup = true, Role = model.role };
                            mResult.Message = "Dummy user Created Successfully";
                            //await SignInAsync(data, false, false);
                        }
                    }
                }
            }
            else
            {
                mResult.Message = "E-Mail Adresse existiert bereits!";
            }
            return mResult;
        }

        public IQueryable<AddAdminUserModel> ViewAdminUser()
        {
            return (from admin in _app.Users
                    where admin.Role == Role.Admin
                    select new AddAdminUserModel
                    {
                        Id = admin.Id,
                        Name = admin.Name,
                        GenderId = admin.GenderId,
                        Email = admin.Email
                    }).OrderBy(x => x.Name);
        }

        public ResponseModel<WebUserResponseModel> CheckProfileSetupCompleted(string UserID)
        {
            ApplicationUser objUser = _userManager.FindById(UserID);

            ResponseModel<WebUserResponseModel> mResult = new ResponseModel<WebUserResponseModel>();

            if (objUser != null)
            {
                mResult.Result = new WebUserResponseModel
                {
                    IsProfileSetup = objUser.isProfileCompleted,
                    //IsStripeConnected = !string.IsNullOrWhiteSpace(objUser.StripeAccountId),
                    Role = objUser.Role
                };
            }

            return mResult;
        }

        public bool checkprofilesetup(string UserId)
        {
            return _app.Users.Where(x => x.isProfileCompleted == true && x.Id == UserId).Any();

        }

        public async Task<ResponseModel<UserResponseModel>> Register(UserRegisterModel model)
        {
            ResponseModel<UserResponseModel> mResult = new ResponseModel<UserResponseModel>();
            try
            {
                if (model.role != Role.Admin && model.role != Role.SuperAdmin)
                {
                    ApplicationUser data = await _userManager.FindByEmailAsync(model.Email);
                    if (data == null)
                    {
                        data = new ApplicationUser
                        {
                            Email = model.Email,
                            UserName = model.UserName,
                            Name = model.UserName,
                            ZipCode = model.ZipCode,
                            CountryId = model.CountryId,
                            PasswordHash = model.Password,
                            Role = model.role,
                            Language = languageType.German,
                            GeoLocation = Data_Utility.CreatePoint(model.LocationLatitude, model.LocationLongitude)
                        };
                        IdentityResult result = await _userManager.CreateAsync(data, model.Password);
                        if (result.Succeeded)
                        {
                            var roleResult = await _userManager.AddToRoleAsync(data.Id, model.role.ToString());
                            if (roleResult.Succeeded)
                            {
                                if (model.role == Role.Stylist)
                                {
                                    stCustomerService stripeService = new stCustomerService();
                                    ResponseModel<StripeCustomerResultModel> stripeResult = await stripeService.Add(new StripeCustomerRequestModel()
                                    {
                                        email = model.Email,
                                        name = model.UserName
                                    });
                                    mResult.Status = stripeResult.Status;
                                    if (stripeResult.Status == ResponseStatus.Success)
                                    {
                                        data.StripeAccountId = stripeResult.Result.customerId;
                                        await _userManager.UpdateAsync(data);
                                    }
                                    await AddRegisterCredit(data.Id);
                                }
                                mResult.Status = ResponseStatus.Success;
                                if (mResult.Status == ResponseStatus.Success)
                                {
                                    mResult.Result = new UserResponseModel { IsProfileSetup = false, Role = model.role, IsEmailConfirmed = false };
                                    mResult.Message = Resource.ActiveEmailText;
                                    //await SignInAsync(data, false, false);
                                }

                                try
                                {
                                    var emailTemplate = EmailTemplate.ActiveAccount;
                                    if (data.Language == languageType.German)
                                    {
                                        emailTemplate = EmailTemplate.ActiveAccountDe;
                                    }

                                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                                    dicPlaceholders.Add("{{name}}", data.Name);
                                    //dicPlaceholders.Add("{{password}}", password);
                                    dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                                    dicPlaceholders.Add("{{link}}", GlobalConfig.ActiveAccount + data.Id);

                                    IdentityMessage message = new IdentityMessage();
                                    message.Subject = string.Format(Resource.emailActiveAccountSub);
                                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                                    message.Destination = data.Email;
                                    await new EmailService().SendAsync(message);
                                }
                                catch (Exception ex)
                                {
                                }
                            }
                        }
                        else
                        {
                            mResult.Message = string.Join("; ", result.Errors);
                        }

                    }
                    else
                    {
                        mResult.Message = Resource.registrationErrorEmailDuplicate;
                    }
                }
                else
                {
                    mResult.Message = Resource.registationErrorInvalidRole;
                }

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}", validationError.PropertyName, validationError.ErrorMessage);
                    }
                }
            }
            return mResult;
        }

        public async Task<int> AddRegisterCredit(string UserId)
        {
            int i = 0;
            StylistCredit ObjStylist = await _app.StylistCredit.FirstOrDefaultAsync(x => x.StylistId == UserId);
            if (ObjStylist == null)
            {
                return i;
            }
            else
            {
                StylistCredit obj = new StylistCredit
                {
                    StylistId = UserId,
                    Credit = 1
                };
                _app.StylistCredit.Add(obj);
                i = await _app.SaveChangesAsync();
                return i;
            }
        }

        public async Task<bool> SetUserLanguage(languageType Language, string userId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _app.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user != null)
            {
                user.Language = Language;
            }
            await _app.SaveChangesAsync();
            return true;
        }

        public ResponseModel<bool> ActivateAccount(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var user = _app.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.EmailConfirmed = true;
                _app.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                if (user.IsActive)
                {
                    mResult.Message = Resource.EmailConfirmedSuccess;
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = Resource.useNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<UserResponseModel>> Login(UserLoginModel model)
        {
            ResponseModel<UserResponseModel> mResult = new ResponseModel<UserResponseModel>();
            var users = await _userManager.FindByNameAsync(model.Email);
            if (users == null)
            {
                users = await _userManager.FindByEmailAsync(model.Email);
                if (users != null)
                    model.Email = users.UserName;
            }

            ApplicationUser user = await _userManager.FindAsync(model.Email, model.Password);

            if (user != null && user.Role == model.role)
            {
                if (user.EmailConfirmed == true)
                {
                    if (user.IsActive)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = string.Format(" " + Resource.loginSuccesMessage + " {0}", user.Name);
                        mResult.Result = new UserResponseModel() { IsProfileSetup = user.isProfileCompleted, Role = user.Role, IsActive = user.IsActive, UserId = user.Id };

                        await SignInAsync(user, false, false);
                    }
                    else
                    {
                        mResult.Result = new UserResponseModel() { IsProfileSetup = user.isProfileCompleted, Role = user.Role, IsActive = user.IsActive, UserId = user.Id };
                        mResult.Message = string.Format(Resource.yourAccountDeactivatedText, GlobalConfig.EmailUserName);
                    }

                }
                else
                {
                    mResult.Result = new UserResponseModel() { IsProfileSetup = user.isProfileCompleted, Role = user.Role, IsActive = user.IsActive, UserId = user.Id };
                    mResult.Message = string.Format(Resource.ActiveAccountText, GlobalConfig.EmailUserName);

                }
            }
            else
            {
                mResult.Message = Resource.loginErrorInvalidData;
            }
            return mResult;
        }

        public void WebLogout()
        {
            _authenticationmanager.SignOut(DefaultAuthenticationTypes.ApplicationCookie,
               DefaultAuthenticationTypes.TwoFactorCookie);
        }

        public async Task<ResponseModel<EditPasswordModel>> EditPassword(string StylishId)
        {
            ResponseModel<EditPasswordModel> mResult = new ResponseModel<EditPasswordModel>();
            EditPasswordModel emodel = new EditPasswordModel();
            emodel.Stylish = await _userrepo.StylishById(StylishId);
            if (emodel.Stylish != null)
            {
                //mResult.Message = "profile detail";
                mResult.Result = emodel;
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        public async Task<ResponseModel<EditPasswordModel>> UpdatePassword(EditPasswordModel model, string StylishId)
        {
            ResponseModel<EditPasswordModel> mResult = new ResponseModel<EditPasswordModel>();
            try
            {
                var user = await _userManager.FindByIdAsync(StylishId);
                if (user == null)
                {
                    mResult.Status = ResponseStatus.Unauthorized;
                    mResult.Message = Resource.updatePasswordErrorUnauthorize;
                }
                else
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.oldPassWord, model.newPassWord);
                    if (result.Succeeded)
                    {
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.updatePasswordSuccess;
                    }
                    else
                    {
                        model.Stylish = await _userrepo.StylishById(StylishId);
                        mResult.Result = model;
                        mResult.Message = Resource.updatepswdFail;
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        /// <summary>
        /// PASSWORD
        /// </summary>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<ResponseModel<UserEditPasswordModel>> UserEditPassword(string UserId)
        {
            ResponseModel<UserEditPasswordModel> mResult = new ResponseModel<UserEditPasswordModel>();
            UserEditPasswordModel emodel = new UserEditPasswordModel();
            emodel.User = await _coreUserRepo.GetUserDetail(UserId);
            emodel.IsActive = _app.Users.Any(x => x.Id == UserId && x.IsActive == true);
            if (emodel.User != null)
            {
                //mResult.Message = " Usder profile detail";
                mResult.Result = emodel;
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        public async Task<ResponseModel<UserEditPasswordModel>> UserUpdatePassword(UserEditPasswordModel model, string UserId)
        {
            ResponseModel<UserEditPasswordModel> mResult = new ResponseModel<UserEditPasswordModel>();
            try
            {
                var user = await _userManager.FindByIdAsync(UserId);
                if (user == null)
                {
                    mResult.Status = ResponseStatus.Unauthorized;
                    mResult.Message = Resource.updatePasswordErrorUnauthorize;
                }
                else
                {
                    IdentityResult result = await _userManager.ChangePasswordAsync(user.Id, model.oldPassWord, model.newPassWord);
                    if (result.Succeeded)
                    {
                        model.User = await _coreUserRepo.GetUserDetail(UserId);
                        mResult.Result = model;
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.updatePasswordSuccess;
                        try
                        {
                            var emailTemplate = EmailTemplate.ChangePassword;
                            if (user.Language == languageType.German)
                            {
                                emailTemplate = EmailTemplate.ChangePasswordDe;
                            }

                            Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                            dicPlaceholders.Add("{{name}}", user.Name);
                            //dicPlaceholders.Add("{{password}}", password);
                            dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                            dicPlaceholders.Add("{{link}}", "https://www.stylaaa.com/");

                            IdentityMessage message = new IdentityMessage();
                            message.Subject = string.Format(Resource.emailChangePasswordSub, GlobalConfig.ProjectName);
                            message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                            message.Destination = user.Email;
                            await new EmailService().SendAsync(message);
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = Resource.forgotpasswordSendLinkSuccess;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    else
                    {
                        model.User = await _coreUserRepo.GetUserDetail(UserId);
                        mResult.Result = model;
                        mResult.Message = Resource.updatepswdFail;
                        mResult.Message = string.Join("; ", result.Errors);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> ForgotPassword(UserForgotPasswordModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            var user = await _userManager.FindByEmailAsync(model.Email);
            //if (user.Role == Role.SuperAdmin || user.Role == Role.Admin)
            //{
            //    mResult.Message = Resource.forgotpasswordSendLinkFailed;
            //}
            //else
            //{
            if (user != null)
            {
                model.Email = user.Email;

                try
                {
                    var emailTemplate = EmailTemplate.ForgotPassword;
                    if (user.Language == languageType.German)
                    {
                        emailTemplate = EmailTemplate.ForgotPasswordDe;
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", user.Name);
                    //dicPlaceholders.Add("{{password}}", password);
                    dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                    dicPlaceholders.Add("{{link}}", GlobalConfig.ForgetPasswordUrl + user.Id);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(Resource.emailForgotPasswordSub, GlobalConfig.ProjectName);
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = user.Email;
                    await new EmailService().SendAsync(message);
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.forgotpasswordSendLinkSuccess;
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                mResult.Message = Resource.forgotpasswordSendLinkFailed;
            //}
            return mResult;
        }

        public async Task<ResponseModel<ApplicationUser>> ForgotPassword(string userName, string password)
        {
            ResponseModel<ApplicationUser> mResult = new ResponseModel<ApplicationUser>();
            var user = await _userManager.FindByEmailAsync(userName);
            mResult = new ResponseModel<ApplicationUser>();

            if (user != null)
            {
                user.PasswordHash = _userManager.PasswordHasher.HashPassword(password);
                var result = await _userManager.UpdateAsync(user);
                if (result.Succeeded)
                    mResult.Status = ResponseStatus.Success;
                else
                {
                    mResult.Status = ResponseStatus.NotFound;
                    mResult.Message = result.Errors.ToString();
                    return mResult;
                }
            }
            else
                mResult.Message = Resource.noReviewFound;
            return mResult;
        }

        public async Task<ResponseModel<object>> SetPassword(ResetPasswordModel model)
        {
            //ResponseModel<UserResponseModel> kResult = new ResponseModel<UserResponseModel>();
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _userManager.FindByIdAsync(model.UserId);
            if (user != null)
            {
                var result = await ForgotPassword(user.Email, model.newPassWord);
                if (result.Status == ResponseStatus.Success)
                {
                    mResult.Message = Resource.updatePasswordSuccess;
                    try
                    {
                        var emailTemplate = EmailTemplate.ChangePassword;
                        if (user.Language == languageType.German)
                        {
                            emailTemplate = EmailTemplate.ChangePasswordDe;
                        }
                        Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                        dicPlaceholders.Add("{{name}}", user.Name);
                        //dicPlaceholders.Add("{{password}}", password);
                        dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                        dicPlaceholders.Add("{{link}}", "https://www.stylaaa.com/");

                        IdentityMessage message = new IdentityMessage();
                        message.Subject = string.Format(Resource.emailChangePasswordSub, GlobalConfig.ProjectName);
                        message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                        message.Destination = user.Email;
                        await new EmailService().SendAsync(message);
                        mResult.Status = ResponseStatus.Success;
                        //mResult.Message = Resource.forgotpasswordSendLinkSuccess;
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    //UserLoginModel loginmodel = new UserLoginModel { Email = user.Email, Password = model.newPassWord };
                    //kResult = await Login(loginmodel);
                    //if(kResult.Status==ResponseStatus.Success)
                    //{
                    //    mResult.Message = "Passwort geändert!";
                    //}                    
                }
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        public async Task<ResponseModel<bool>> DeactiveAccount(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var user = _app.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.IsActive = false;
                _app.SaveChanges();
                mResult.Status = ResponseStatus.Success;
                try
                {
                    var emailTemplate = EmailTemplate.DeactiveAccount;
                    if (user.Language == languageType.German)
                    {
                        emailTemplate = EmailTemplate.DeactiveAccountDe;
                    }

                    Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    dicPlaceholders.Add("{{name}}", user.Name);
                    //dicPlaceholders.Add("{{password}}", password);
                    dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                    //dicPlaceholders.Add("{{link}}", GlobalConfig.ActiveAccount + user.Id);

                    IdentityMessage message = new IdentityMessage();
                    message.Subject = string.Format(Resource.emailDeactiveAccSub, GlobalConfig.ProjectName + ".com!");
                    message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    message.Destination = user.Email;
                    await new EmailService().SendAsync(message);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                if (user.IsActive)
                {
                    mResult.Message = Resource.ActiveSuccessText;
                }
                else
                {
                    mResult.Message = Resource.DeactiveAccountSuccess;
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = Resource.useNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<bool>> ActiveAccount(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var user = _app.Users.FirstOrDefault(x => x.Id == userId);
            if (user != null)
            {
                user.IsActive = true;
                await _app.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
                if (user.IsActive)
                {
                    mResult.Message = Resource.ActiveSuccessText;
                }
                else
                {
                    mResult.Message = Resource.DeactiveAccountSuccess;
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = Resource.useNotFound;
            }
            return mResult;
        }
        
        public async Task<ResponseModel<bool>> ActiveToggle(string userId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            var user = await _app.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user != null)
            {
                user.IsActive = !user.IsActive;
                await _app.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                if (user.IsActive)
                {
                    mResult.Message = Resource.ActiveSuccessText;
                }
                else
                {
                    try
                    {
                        var emailTemplate = EmailTemplate.DeactiveAccount;
                        if (user.Language == languageType.German)
                        {
                            emailTemplate = EmailTemplate.DeactiveAccountDe;
                        }

                        Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                        dicPlaceholders.Add("{{name}}", user.Name);
                        //dicPlaceholders.Add("{{password}}", password);
                        dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                        //dicPlaceholders.Add("{{link}}", GlobalConfig.ActiveAccount + user.Id);

                        IdentityMessage message = new IdentityMessage();
                        message.Subject = string.Format(Resource.emailDeactiveAccSub, GlobalConfig.ProjectName + ".com!");
                        message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                        message.Destination = user.Email;
                        await new EmailService().SendAsync(message);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    mResult.Message = Resource.DeactiveAccountSuccess;
                }
                mResult.Result = user.IsActive;
            }
            else
            {
                mResult.Message = Resource.useNotFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<bool>> DeleteUser(string UserId)
        {
            ResponseModel<bool> mResult = new ResponseModel<bool>();
            bool IsStylist = false;
            try
            {
                ApplicationUser objUser = await _app.Users.FirstOrDefaultAsync(x => x.Id == UserId);
                if (objUser == null)
                {
                    mResult.Result = new bool { };
                    mResult.Message = Resource.useNotFound;
                }
                else
                {
                    if (objUser.Role == Role.Stylist)
                    {
                        IsStylist = true;
                    }

                    var user = await _app.Users.Where(x => x.Id == UserId).ToListAsync();
                    foreach (var itm in user)
                    {
                        string FilePath = GlobalConfig.UserImagePath + itm.ProfileImageName;
                        if (System.IO.File.Exists(FilePath))
                        {
                            System.IO.File.Delete(FilePath);
                        }
                    }
                    var review = await _app.UserReviews.Where(x => x.ReviewBy.Id == UserId || x.StylishId == UserId).ToListAsync();
                     _app.UserReviews.RemoveRange(review);

                    var follow = await _app.Followers.Where(x => x.UserId == UserId || x.FollowingId == UserId).ToListAsync();
                    _app.Followers.RemoveRange(follow);

                    var postreport = await _app.PostReport.Where(x => x.UserId == UserId).ToListAsync();
                    _app.PostReport.RemoveRange(postreport);

                    var postshare = await _app.PostShare.Where(x => x.UserId == UserId).ToListAsync();
                    _app.PostShare.RemoveRange(postshare);

                    var stylistcategory = await _app.StylistCategory.Where(x => x.StylistId == UserId).ToListAsync();
                    _app.StylistCategory.RemoveRange(stylistcategory);

                    var replyComment = await _app.ReplyComments.Where(x => x.UserId == UserId).ToListAsync();
                    _app.ReplyComments.RemoveRange(replyComment);

                    var ChatMessage = await _app.ChatMessage.Where(x => x.UserId == UserId || x.ReceiverId == UserId).ToListAsync();
                    _app.ChatMessage.RemoveRange(ChatMessage);

                    var chatbuddy = await _app.ChatBuddy.Where(x => x.UserId == UserId || x.ReceiverId == UserId).ToListAsync();
                    _app.ChatBuddy.RemoveRange(chatbuddy);
                    
                   
                    var notification = await _app.Notification.Where(x => x.SenderId == UserId || x.ReceiverId == UserId).ToListAsync();
                    _app.Notification.RemoveRange(notification);

                    var StylistPremiumTren = await _app.StylistPremiunTran.Where(x => x.StylistId == UserId).ToListAsync();
                    _app.StylistPremiunTran.RemoveRange(StylistPremiumTren);

                    var StylistCatTran = await _app.StylistCreditTrans.Where(x => x.StylistId == UserId).ToListAsync();
                    _app.StylistCreditTrans.RemoveRange(StylistCatTran);

                    var comment = await _app.Comments.Where(x => x.UserId == UserId).ToListAsync(); 
                    _app.Comments.RemoveRange(comment);

                    var like = await _app.Likes.Where(x => x.UserId == UserId).ToListAsync();
                    _app.Likes.RemoveRange(like);

                    var ReviewSpam = await _app.SpamReview.Where(x => x.UserId == UserId).ToListAsync();
                    _app.SpamReview.RemoveRange(ReviewSpam);

                    var stylistCradit = await _app.StylistCredit.Where(x => x.StylistId == UserId).ToListAsync();
                    _app.StylistCredit.RemoveRange(stylistCradit);

                    var task = await _app.UserTask.Where(x => x.UserId == UserId).ToListAsync();
                    _app.UserTask.RemoveRange(task);

                    var jobreq = await _app.JobRequests.Where(x => x.StylistId == UserId).ToListAsync();
                    _app.JobRequests.RemoveRange(jobreq);
                    var post = await _app.Post.Where(x => x.UserId == UserId).ToListAsync();
                    if (post != null)
                    {
                        foreach (var itm in post)
                        {
                            var postfile = await _app.PostTransactions.Where(x => x.PostId == itm.Id).ToListAsync();
                            foreach (var file in postfile)
                            {
                                string FilePath = GlobalConfig.PostImagePath + file.FileName;
                                if (System.IO.File.Exists(FilePath))
                                {
                                    System.IO.File.Delete(FilePath);
                                }
                            }
                            _app.PostTransactions.RemoveRange(postfile);
                        }
                    }
                    _app.Post.RemoveRange(post);

                    var job =await _app.Jobs.Where(x => x.UserId == UserId).ToListAsync();
                    if(job!=null)
                    {
                        foreach (var itm in job)
                        {
                            var jobfile =await _app.JobFiles.Where(x => x.JobId == itm.Id).ToListAsync();
                            foreach (var file in jobfile)
                            {
                                string FilePath = GlobalConfig.JobImagePath + file.FileName;
                                if (System.IO.File.Exists(FilePath))
                                {
                                    System.IO.File.Delete(FilePath);
                                }
                            }
                            _app.JobFiles.RemoveRange(jobfile);
                        }
                    }

                    _app.Users.Remove(objUser);
                }
                long objId = await _app.SaveChangesAsync();
                if (objId > 0)
                {
                    try
                    {
                        var emailtemplate = EmailTemplate.DeleteUserEmail;
                        if (objUser.Language == languageType.German)
                        {
                            emailtemplate = EmailTemplate.DeleteUserEmailDe;
                        }
                        StringBuilder objServices = new StringBuilder();
                        Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                        dicPlaceholders.Add("{{name}}", objUser.Name);
                        dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);

                        IdentityMessage message = new IdentityMessage();
                        message.Subject = string.Format("Dein Stylaaa-Account wurde hiermit erfolgreich gelöscht");
                        message.Body = Utility.GenerateEmailBody(emailtemplate, dicPlaceholders);
                        message.Destination = objUser.Email;
                        await new EmailService().SendAsync(message);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }

                    mResult.Result = IsStylist;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "User Deleted Successfully";
                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Result = new bool { };
                    mResult.Message = "user cannot deleted";
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }
                
        public Role GetRoleById(string UserId)
        {
            return (from ct in _app.Users
                    where ct.Id == UserId
                    select ct.Role).FirstOrDefault();
        }

        public static string GenerateEmailBody(EmailTemplate templateFileName, Dictionary<string, string> dicPlaceholders)
        {
            dicPlaceholders.Add("{{projectName}}", GlobalConfig.ProjectName);
            string templatePath = System.IO.Path.Combine(HttpRuntime.AppDomainAppPath, GlobalConfig.EmailTemplatePath, templateFileName.ToString() + ".html");
            string body = string.Empty;
            using (System.IO.StreamReader reader = new System.IO.StreamReader(templatePath))
            {
                body = reader.ReadToEnd();
            }
            if (body.Length > 0)
            {
                foreach (var item in dicPlaceholders)
                {
                    body = body.Replace(item.Key, item.Value);
                }
            }
            return body;
        }

        public async Task<ResponseModel<object>> AddCoverPic(string Filename, string UserId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ApplicationUser objUser = _app.Users.FirstOrDefault(x => x.Id == UserId);
                if (objUser == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    string FilePath = GlobalConfig.UserCoverPicPath + objUser.CoverImageName;
                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Delete(FilePath);
                    }

                    objUser.CoverImageName = Filename;
                    int i = await _app.SaveChangesAsync();
                    if (i > 0)
                    {
                        mResult.Result = new { };
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.CoverpicUploadSuccess;
                    }
                    else
                    {
                        mResult.Result = new { };
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = Resource.coverpicuploadfailed;
                    }

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mResult;
        }

        public IQueryable<UserModel> GetAllUsers_IQueryable(string UserId, bool isUserExpire)
        {
            IQueryable<UserModel> fisrttwo = (from c in _app.ChatBuddy
                                              where (c.UserId == UserId || c.ReceiverId == UserId)
                                              let user1 = c.UserId == UserId ? c.ReceiverUser : c.SenderUser
                                              join msg in _app.ChatMessage
                                              on new { SenderId = user1.Id, ReceiverId = UserId, IsRead = false }
                                              equals new { SenderId = msg.UserId, ReceiverId = msg.ReceiverId, IsRead = msg.IsRead }
                                              into has_message
                                              select new UserModel
                                              {
                                                  ChatId = c.Id,
                                                  Id = user1.Id,
                                                  Name = user1.Name,
                                                  Email = user1.Email,
                                                  ProfileImage = user1.ProfileImageName,
                                                  LastMessage = c.Chat.Message,
                                                  isImage = c.Chat.IsImage,
                                                  UnreadCount = has_message.Count(),
                                                  ChatCount = _app.ChatMessage.Count(x => x.UserId == UserId || x.ReceiverId == UserId),
                                                  senderUserName = c.Chat.SenderUser.Name,
                                                  Senderid = c.Chat.SenderUser.Id,
                                                  isRequestAccepted = c.IsRequestAccepted,
                                                  role = c.SenderUser.Role,
                                                  createddate = c.Chat.CreatedDateTimeUTC
                                                  //IsRequested = await _chat.IsRequested(UserId, user1.Id)
                                              }).OrderByDescending(x => x.createddate);

            IQueryable<UserModel> anotherdata = (from c in _app.ChatBuddy
                                                 where (c.UserId == UserId || c.ReceiverId == UserId)
                                                 let user1 = c.UserId == UserId ? c.ReceiverUser : c.SenderUser
                                                 join msg in _app.ChatMessage
                                                 on new { SenderId = user1.Id, ReceiverId = UserId, IsRead = false }
                                                 equals new { SenderId = msg.UserId, ReceiverId = msg.ReceiverId, IsRead = msg.IsRead }
                                                 into has_message
                                                 select new UserModel
                                                 {
                                                     ChatId = c.Id,
                                                     Id = user1.Id,
                                                     Name = user1.Name,
                                                     Email = user1.Email,
                                                     ProfileImage = user1.ProfileImageName,
                                                     LastMessage = c.Chat.Message,
                                                     isImage = c.Chat.IsImage,
                                                     UnreadCount = has_message.Count(),
                                                     ChatCount = _app.ChatMessage.Count(x => x.UserId == UserId || x.ReceiverId == UserId),
                                                     senderUserName = c.Chat.SenderUser.Name,
                                                     Senderid = c.Chat.SenderUser.Id,
                                                     isRequestAccepted = c.IsRequestAccepted,
                                                     role = c.ReceiverUser.Role
                                                 }).OrderBy(x => x.Name);

            return fisrttwo;
        }
        
        public async Task<List<JobChatModel>> GetAllJobRequest_IQueryable(string UserId)
        {
            List<JobChatModel> JobBuddyList = await (from chat in _app.JobRequests
                                                     where ((chat.StylistId == UserId || chat.job.UserId == UserId))
                                                     let user1 = chat.StylistId == UserId ? chat.job.JobBy : chat.Stylist
                                                     select new JobChatModel
                                                     {
                                                         Id = user1.Id,
                                                         JobId=chat.JobId,
                                                         Name = user1.Name,
                                                         JobTitle = chat.job.JobTitle,
                                                         ProfileImage = user1.ProfileImageName,
                                                         senderUserName = user1.Name,
                                                         Senderid = UserId,
                                                         role = user1.Role
                                                     }).ToListAsync();

            return JobBuddyList;
            //List<JobChatModel> JobBuddyList = await (from chat in _app.ChatMessage
            //                                         where ((chat.UserId == UserId || chat.ReceiverId == UserId) && chat.JobId != null)
            //                                         let user1 = chat.UserId == UserId ? chat.ReceiverUser : chat.SenderUser
            //                                         join job in _app.Jobs on chat.JobId equals job.Id
            //                                         orderby chat.CreatedDateTimeUTC descending
            //                                         select new JobChatModel
            //                                         {
            //                                             ChatId = chat.Id,
            //                                             Id = user1.Id,
            //                                             Name = user1.Name,
            //                                             Email = job.JobBy.Email,
            //                                             ProfileImage = job.JobBy.ProfileImageName,
            //                                             JobTitle = job.JobTitle,
            //                                             isImage = chat.IsImage,
            //                                             senderUserName = chat.SenderUser.Name,
            //                                             Senderid = chat.SenderUser.Id
            //                                         }).ToListAsync();

            //var test = JobBuddyList.GroupBy(u => u.JobId).Select(grp => grp.ToList()).ToList();

            //return JobBuddyList;
        }
              
        public void Dispose()
        {
            _app.Dispose();
        }
    }
}