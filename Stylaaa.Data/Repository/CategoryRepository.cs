﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class CategoryRepository : IDisposable
    {
        private ApplicationDbContext _ctx;

        public CategoryRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<CategoryModel> IQueryable_GetAllCategories(languageType languageid)
        {
            return (from ct in _ctx.Categories
                    join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageid) on ct.Id equals ctlng.CategoryId into HasCategory
                    select new CategoryModel
                    {
                        Id = ct.Id,
                        Name = HasCategory.FirstOrDefault().Name,
                        CategoryImage = ct.CategoryImage
                    }).OrderBy(x => x.Name);
        }

        public List<PostCategoryModel> GetPostCategoryList(languageType languageId)
        {
            //return (from cat in _ctx.Categories
            //        select new PostCategoryModel
            //        {
            //            Id = cat.Id,
            //            CategoryName = cat.Name,
            //            Selected = false
            //        }).ToList();

            return (from ct in _ctx.Categories
                    join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.CategoryId into HasCategory
                    select new PostCategoryModel
                    {
                        Id = ct.Id,
                        CategoryName = HasCategory.FirstOrDefault().Name,
                        Selected = false
                    }).OrderBy(x => x.CategoryName).ToList();
        }

        public IQueryable<CategoryDropdownModel> GetCategroies(languageType languageId)
        {
            //return (from ct in _ctx.Categories
            //        join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageId) on ct.Id equals ctlng.CategoryId into hasCategory
            //        select new CategoryDropdownModel
            //        {
            //            Id = ct.Id,
            //            Name = hasCategory.FirstOrDefault().Name,
            //        }).OrderBy(x => x.Name);

            return (from ct in _ctx.Categories
                    join sub in _ctx.SubCategories on ct.Id equals sub.CategoryId
                    where sub.CategoryId == ct.Id && sub.IsDelete == false && sub.SubCategoryLanguageList.FirstOrDefault().GenderId == LandingGender.Male
                    group sub by sub.CategoryId into g
                    select new CategoryDropdownModel
                    {
                        Id = g.FirstOrDefault().CategoryId,
                        Name = g.FirstOrDefault().Categories.CategoryLanguageList.Where(x => x.LanguageId == languageId).FirstOrDefault().Name
                    }).OrderBy(x => x.Name);
        }

        public IQueryable<CategoryDropdownModel> GetCategroiesByGender(languageType languageId, LandingGender GenderId)
        {   
            return (from ct in _ctx.Categories
                    join sub in _ctx.SubCategories on ct.Id equals sub.CategoryId
                    where sub.CategoryId == ct.Id && sub.IsDelete == false && sub.SubCategoryLanguageList.FirstOrDefault().GenderId == GenderId
                    group sub by sub.CategoryId into g
                    select new CategoryDropdownModel
                    {
                        Id = g.FirstOrDefault().CategoryId,
                        Name = g.FirstOrDefault().Categories.CategoryLanguageList.Where(x => x.LanguageId == languageId).FirstOrDefault().Name
                    }).OrderBy(x => x.Name);
        }

        public IQueryable<AdminCategoryDropdownmodel> GetCategoryDropdown()
        {
            return (from ct in _ctx.Categories
                    join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageType.English) on ct.Id equals ctlng.CategoryId into hasCategory
                    select new AdminCategoryDropdownmodel
                    {
                        Id = ct.Id,
                        Name = ct.CategoryLanguageList.FirstOrDefault().Name
                    }).OrderBy(x => x.Name);
        }

        public List<JobCategoryModel> GetJobCategoryList(languageType languageId)
        {
            return (from jobcat in _ctx.Categories
                    join catlang in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageId) on jobcat.Id equals catlang.CategoryId into Hascategory
                    select new JobCategoryModel
                    {
                        Id = jobcat.Id,
                        CategoryName = Hascategory.FirstOrDefault().Name,
                        Selected = false
                    }).OrderBy(x => x.CategoryName).ToList();
        }


        public async Task<ResponseModel<object>> InsertOrUpdateCategory(AdminCategoryModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await _ctx.Categories.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (data == null)
                {
                    isNew = true;
                    data = new Categories();
                }
                data.CategoryImage = model.ImageName ?? data.CategoryImage;
                if (isNew)
                {
                    List<CategoryLanguage> lstCategoryLanguage = new List<CategoryLanguage>();
                    foreach (var item in model.CategoryLanguageModelList)
                    {
                        lstCategoryLanguage.Add(new CategoryLanguage
                        {
                            CategoryId = data.Id,
                            Name = item.CategoryName,
                            LanguageId = item.LanguageId
                        });
                    }
                    data.CategoryLanguageList = lstCategoryLanguage;
                    _ctx.Categories.Add(data);
                    await _ctx.SaveChangesAsync();
                }
                else
                {
                    foreach (var item in data.CategoryLanguageList)
                    {
                        CategoryLanguageModel categoryLanguage = model.CategoryLanguageModelList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                        item.Name = categoryLanguage.CategoryName;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Category saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }


        public bool isCategoryDuplicate(AdminCategoryModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.CategoryLanguageModelList)
            {
                isDuplicate = _ctx.CategoryLanguage.Any(x => x.LanguageId == item.LanguageId && x.Name == item.CategoryName && x.CategoryId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }

        public async Task<AdminCategoryModel> GetCategoryDetail(long CatrgoryId)
        {
            return await (from c in _ctx.Categories
                          where c.Id == CatrgoryId
                          select new AdminCategoryModel
                          {
                              Id = c.Id,
                              ImageName = c.CategoryImage,
                              CategoryLanguageModelList = c.CategoryLanguageList.Select(x => new CategoryLanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  CategoryName = x.Name,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<CategoryModel> EditCategoryById(languageType languageid, long Id)
        {
            return await (from ct in _ctx.Categories
                          join ctlng in _ctx.CategoryLanguage.Where(x => x.LanguageId == languageid) on ct.Id equals ctlng.CategoryId into HasCategory
                          where ct.Id == Id
                          select new CategoryModel
                          {
                              Id = ct.Id,
                              Name = HasCategory.FirstOrDefault().Name,
                              CategoryImage = ct.CategoryImage
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> DeleteCategory(long CategoryId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                Categories data = await _ctx.Categories.FirstOrDefaultAsync(x => x.Id == CategoryId);
                if (data != null)
                {
                    string FilePath = GlobalConfig.CategoryImagePath + data.CategoryImage;
                    if (System.IO.File.Exists(FilePath))
                    {
                        System.IO.File.Delete(FilePath);
                    }
                    _ctx.Categories.Remove(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Category deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Category found or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
