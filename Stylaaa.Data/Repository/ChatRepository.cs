﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class ChatRepository : IDisposable
    {
        public ApplicationDbContext _ctx;
        private CoreUserRepository _coreuser;
        public ChatRepository()
        {
            _ctx = new ApplicationDbContext();
            _coreuser = new CoreUserRepository();
        }


        public async Task<List<BuddyViewModel>> GetBuddyList(string UserID)
        {
            var data = await (from c in _ctx.ChatBuddy
                              where (c.UserId == UserID || c.ReceiverId == UserID)
                              let user = c.UserId == UserID ? c.ReceiverUser : c.SenderUser
                              select new
                              {
                                  user.Id,
                                  user.Name,
                                  LastMessage = c.Chat
                              }).ToListAsync();

            List<BuddyViewModel> buddyList = data.Select(x => new BuddyViewModel
            {
                IsSendByMe = x.Id == UserID,
                ChatUserID = x.Id,
                LastMessageText = x.LastMessage.Message,
                LastMessageTimeCaption = Utility.TimeAgo(x.LastMessage.CreatedDateTimeUTC),
                Name = x.Name
            }).ToList();

            return buddyList;
        }

        public async Task<bool> IsRequested(string userId, string RecieverId)
        {
            return await _ctx.ChatBuddy.AnyAsync(x => (x.UserId == userId && x.ReceiverId == RecieverId) || (x.UserId == RecieverId && x.ReceiverId == userId) && x.IsRequestAccepted && x.ReceiverId == userId);
        }

        public bool IsUserRequested(string userId, string RecieverId)
        {
            return _ctx.ChatBuddy.Any(x => (x.UserId == userId && x.ReceiverId == RecieverId) || (x.UserId == RecieverId && x.ReceiverId == userId) && x.IsRequestAccepted && x.ReceiverId == userId);
        }

        public async Task<bool> IsRequestedAccepted(string userId, string RecieverId)
        {
            bool isacccepted = false;
            var data = await _ctx.ChatBuddy.FirstOrDefaultAsync(x => (x.UserId == userId && x.ReceiverId == RecieverId) && x.IsRequestAccepted == false);
            if (data == null)
            {
                isacccepted = false;
                return isacccepted;
            }
            else
            {
                if (data.SenderUser.Role == Role.Stylist && data.ReceiverUser.Role == Role.Stylist)
                    isacccepted = true;
                else if (data.SenderUser.Role == Role.Stylist && data.ReceiverUser.Role == Role.User)
                    isacccepted = false;
                else if (data.SenderUser.Role == Role.User && data.ReceiverUser.Role == Role.Stylist)
                    isacccepted = true;
                else if (data.SenderUser.Role == Role.User && data.ReceiverUser.Role == Role.User)
                    isacccepted = false;
            }
            return isacccepted;
        }


        public bool isFirstTwoUser(string userId, string RecieverId)
        {
            //List<ChatBuddy> chatlist = _ctx.ChatBuddy.Where(x => (x.ReceiverId == userId || x.UserId == userId) && (x.SenderUser.Role == Role.Stylist || x.SenderUser.Role == Role.User) && x.ReceiverId == RecieverId).Take(2).ToList();
            //return chatlist.Where(x => x.UserId == RecieverId || x.ReceiverId == RecieverId).Any();

            List<ChatBuddy> chatlist = _ctx.ChatBuddy.Where(x => x.ReceiverId == userId || x.UserId == userId).Take(2).ToList();
            return chatlist.Where(x => x.UserId == RecieverId || x.ReceiverId == RecieverId).Any();
        }

        public async Task<int> CountChat(string userId, string RecieverId)
        {
            return await _ctx.ChatMessage.CountAsync(x => (x.UserId == userId && x.ReceiverId == RecieverId) || (x.UserId == RecieverId && x.ReceiverId == userId));
        }

        public async Task<ResponseModel<object>> ApproveRequest(string UserId, string ReceiverId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ChatBuddy chatbuddy = _ctx.ChatBuddy.FirstOrDefault(x => x.UserId == ReceiverId && x.ReceiverId == UserId);
            if (chatbuddy != null)
            {
                chatbuddy.IsRequestAccepted = true;
                int i = await _ctx.SaveChangesAsync();
                if (i > 0)
                {
                    var Receiver = _ctx.Users.FirstOrDefault(x => x.Id == ReceiverId);
                    var sender = _ctx.Users.FirstOrDefault(x => x.Id == UserId);
                    try
                    {
                        var emailTemplate = EmailTemplate.AcceptRequest;
                        if (Receiver.Language == languageType.German)
                        {
                            emailTemplate = EmailTemplate.AcceptRequestDe;
                        }
                        Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                        dicPlaceholders.Add("{{name}}", Receiver.Name);
                        dicPlaceholders.Add("{{SenderName}}", sender.Name);
                        //dicPlaceholders.Add("{{password}}", password);
                        dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                        dicPlaceholders.Add("{{link}}", "https://www.stylaaa.com");

                        IdentityMessage message = new IdentityMessage();
                        message.Subject = string.Format(Resource.emailAcceptRequestSub);
                        message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                        message.Destination = Receiver.Email;
                        await new EmailService().SendAsync(message);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                    mResult.Message = Resource.requestedAccepted;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Result = Receiver.Id;

                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.requestnotAccepted;
                }
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = Resource.noReviewFound;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteChatMessage(long ChatId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            ChatMessage chatmesssage = await _ctx.ChatMessage.FirstOrDefaultAsync(x => x.Id == ChatId);
            if (chatmesssage != null)
            {
                chatmesssage.IsDeleted = true;
                //_ctx.ChatMessage.Remove(chatmesssage);
                await _ctx.SaveChangesAsync();
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Status = ResponseStatus.Failed;
                mResult.Message = "Message not found";
            }
            return mResult;
        }

        public async Task<int> GetUserUnreadMessageCount(string UserId)
        {
            return await _ctx.ChatMessage.CountAsync(x => x.ReceiverId == UserId && x.IsRead == false);
        }

        public async Task<List<UnreadMessageCountModel>> GetUserUnreadMessageCount(List<string> UserIds, string UserId)
        {
            return await (from u in _ctx.Users.Where(x => UserIds.Contains(x.Id))
                          join m in _ctx.ChatMessage
                          on new { SenderId = u.Id, ReceiverId = UserId, IsRead = false }
                          equals new { SenderId = m.UserId, m.ReceiverId, m.IsRead }
                          into has_message
                          where u.Id != UserId
                          select new UnreadMessageCountModel
                          {
                              UserId = u.Id,
                              Count = has_message.Count()
                          }).ToListAsync();
        }

        public async Task<ResponseModel<ChatConversationViewModel>> Send(MessageSendModel model, string UserID)
        {
            ResponseModel<ChatConversationViewModel> mResult = new ResponseModel<ChatConversationViewModel>() { Status = ResponseStatus.Success };
            try
            {
                var todayNowUtc = Utility.GetSystemDateTimeUTC();
                ApplicationUser user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == UserID);
                if (model.JobId != null)
                {
                    ChatMessage data = new ChatMessage
                    {
                        Message = model.MessageText,
                        ReceiverId = model.ReceiverID,
                        UserId = UserID,
                        IsImage = false,
                        JobId = model.JobId
                    };
                    _ctx.ChatMessage.Add(data);

                    Notification notification = await _ctx.Notification.FirstOrDefaultAsync(x => x.SenderId == UserID && x.ReceiverId == model.ReceiverID);
                    if (notification != null)
                    {
                        _ctx.Notification.Remove(notification);
                    }

                    Notification Notification = new Notification
                    {
                        SenderId = UserID,
                        ReceiverId = model.ReceiverID,
                        IsRead = false,
                        JobId = model.JobId,
                        NotificationType = NotificationType.jobInquery,
                        NotificationText = Resource.notificationSentmessage,
                    };
                    _ctx.Notification.Add(Notification);
                    await _ctx.SaveChangesAsync();

                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.SendMessageSuccess;
                    mResult.Result = new ChatConversationViewModel()
                    {
                        MessageText = model.MessageText,
                        ChatMessageID = data.Id,
                        IsRead = false,
                        ChatUserID = UserID,
                        IsSendByMe = false,
                        Name = user.Name,
                        TimeCaption = Utility.TimeAgo(data.CreatedDateTimeUTC),
                        TotalMiliSecond = Utility.GetUTCMilliSecond(data.CreatedDateTimeUTC)
                    };
                }
                else
                {
                    if (!string.IsNullOrEmpty(model.MessageText))
                    {
                        ChatMessage data = new ChatMessage
                        {
                            CreatedDateTimeUTC = Utility.GetSystemDateTimeUTC(),
                            Message = model.MessageText,
                            ReceiverId = model.ReceiverID,
                            UserId = UserID,
                            IsImage = model.IsImage
                        };
                        _ctx.ChatMessage.Add(data);

                        bool isNew = false;
                        var chatbuddy = _ctx.ChatBuddy.FirstOrDefault(x => (x.UserId == UserID && x.ReceiverId == model.ReceiverID) || (x.UserId == model.ReceiverID && x.ReceiverId == UserID));
                        if (chatbuddy == null)
                        {
                            isNew = true;
                            chatbuddy = new ChatBuddy();
                        }
                        chatbuddy.UserId = UserID;
                        chatbuddy.ReceiverId = model.ReceiverID;
                        //chatbuddy.IsRequestAccepted = false;
                        chatbuddy.ChatId = data.Id;
                        if (isNew)
                        {
                            _ctx.ChatBuddy.Add(chatbuddy);
                        }
                        Notification notification = _ctx.Notification.FirstOrDefault(x => x.SenderId == UserID && x.ReceiverId == model.ReceiverID);
                        if (notification != null)
                        {
                            _ctx.Notification.Remove(notification);
                        }

                        Notification Notification = new Notification
                        {
                            SenderId = UserID,
                            ReceiverId = model.ReceiverID,
                            IsRead = false,
                            NotificationType = NotificationType.Message,
                            NotificationText = Resource.notificationSentmessage,
                        };

                        _ctx.Notification.Add(Notification);
                        await _ctx.SaveChangesAsync();
                        var reciever = _ctx.Users.FirstOrDefault(x => x.Id == model.ReceiverID);
                        var messagecount = await _ctx.ChatMessage.CountAsync(x => x.UserId == UserID && x.ReceiverId == model.ReceiverID && x.CreatedDateTimeUTC == todayNowUtc);
                        if (isNew || messagecount == 1)
                        {
                            try
                            {
                                var emailTemplate = EmailTemplate.ReceiveMessageRequest;
                                if (reciever.Language == languageType.German)
                                {
                                    emailTemplate = EmailTemplate.ReceiveMessageRequestDe;
                                }
                                Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                                //dicPlaceholders.Add("{{name}}", reciever.Name);
                                //dicPlaceholders.Add("{{password}}", password);
                                dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                                dicPlaceholders.Add("{{link}}", GlobalConfig.baseUrl + "Chat/" + reciever.UserName + "/Chat");

                                IdentityMessage message = new IdentityMessage();
                                message.Subject = string.Format(Resource.emailMessageRequestSub);
                                message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                                message.Destination = reciever.Email;
                                await new EmailService().SendAsync(message);
                            }
                            catch (Exception ex)
                            {
                                throw ex;
                            }
                        }
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = user.Name + " : " + model.MessageText;
                        mResult.Result = new ChatConversationViewModel()
                        {
                            MessageText = model.MessageText,
                            ChatMessageID = data.Id,
                            IsRead = false,
                            ChatUserID = UserID,
                            IsSendByMe = false,
                            Name = user.Name,
                            TimeCaption = Utility.TimeAgo(data.CreatedDateTimeUTC),
                            TotalMiliSecond = Utility.GetUTCMilliSecond(data.CreatedDateTimeUTC)
                        };
                    }
                }

            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SendMessage(MessageSendModel model, string userId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var todayNowUtc = Utility.GetSystemDateTimeUTC();
                var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == userId);
                var ReceiverID = model.ReceiverID;
                var MessageText = model.MessageText;
                if (!string.IsNullOrEmpty(model.MessageText))
                {
                    ChatMessage data = new ChatMessage
                    {
                        Message = model.MessageText,
                        ReceiverId = ReceiverID,
                        UserId = userId,
                        IsImage = false
                    };
                    _ctx.ChatMessage.Add(data);

                    bool isNew = false;
                    var chatbuddy = await _ctx.ChatBuddy.FirstOrDefaultAsync(x => x.UserId == userId && x.ReceiverId == ReceiverID || x.UserId == model.ReceiverID && x.ReceiverId == userId);
                    if (chatbuddy == null)
                    {
                        isNew = true;
                        chatbuddy = new ChatBuddy();
                    }
                    chatbuddy.ReceiverId = ReceiverID;
                    chatbuddy.UserId = userId;
                    chatbuddy.ChatId = data.Id;
                    //chatbuddy.IsRequestAccepted = false;
                    if (isNew)
                    {
                        _ctx.ChatBuddy.Add(chatbuddy);
                    }

                    Notification notification = await _ctx.Notification.FirstOrDefaultAsync(x => x.SenderId == userId && x.ReceiverId == model.ReceiverID);
                    if (notification != null)
                    {
                        _ctx.Notification.Remove(notification);
                    }

                    Notification Notification = new Notification
                    {
                        SenderId = userId,
                        ReceiverId = model.ReceiverID,
                        IsRead = false,
                        NotificationType = NotificationType.Message,
                        NotificationText = Resource.notificationSentmessage,
                    };

                    _ctx.Notification.Add(Notification);
                    await _ctx.SaveChangesAsync();

                    var reciever = _ctx.Users.FirstOrDefault(x => x.Id == model.ReceiverID);
                    var messagecount = await _ctx.ChatMessage.CountAsync(x => x.UserId == userId && x.ReceiverId == model.ReceiverID && x.CreatedDateTimeUTC == todayNowUtc);
                    if (isNew || messagecount == 1)
                    {
                        try
                        {
                            var emailTemplate = EmailTemplate.ReceiveMessageRequest;
                            if (reciever.Language == languageType.German)
                            {
                                emailTemplate = EmailTemplate.ReceiveMessageRequestDe;
                            }
                            Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                            dicPlaceholders.Add("{{name}}", reciever.Name);
                            //dicPlaceholders.Add("{{sender}}", user.Name);
                            //dicPlaceholders.Add("{{password}}", password);
                            dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                            dicPlaceholders.Add("{{link}}", GlobalConfig.baseUrl + "Chat/" + reciever.UserName + "/Chat");

                            IdentityMessage message = new IdentityMessage();
                            message.Subject = string.Format(Resource.emailMessageRequestSub);
                            message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                            message.Destination = reciever.Email;
                            await new EmailService().SendAsync(message);
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.SendMessageSuccess;

                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.SendMessageTextValid;
                    //mResult.Result = "please enter message";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SendMessageFromAdmin(MessageSendModel model, string userId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var todayNowUtc = Utility.GetSystemDateTimeUTC();
                var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == userId);

                if (!string.IsNullOrEmpty(model.MessageText))
                {
                    ChatMessage data = new ChatMessage
                    {
                        CreatedDateTimeUTC = Utility.GetSystemDateTimeUTC(),
                        Message = model.MessageText,
                        ReceiverId = model.ReceiverID,
                        UserId = userId,
                        IsImage = false
                    };
                    _ctx.ChatMessage.Add(data);

                    bool isNew = false;
                    var chatbuddy = await _ctx.ChatBuddy.FirstOrDefaultAsync(x => x.UserId == userId && x.ReceiverId == model.ReceiverID || x.UserId == model.ReceiverID && x.ReceiverId == userId);
                    if (chatbuddy == null)
                    {
                        isNew = true;
                        chatbuddy = new ChatBuddy();
                    }
                    chatbuddy.ReceiverId = model.ReceiverID;
                    chatbuddy.UserId = userId;
                    chatbuddy.ChatId = data.Id;
                    chatbuddy.IsRequestAccepted = true;
                    if (isNew)
                    {
                        _ctx.ChatBuddy.Add(chatbuddy);
                    }

                    Notification notification = await _ctx.Notification.FirstOrDefaultAsync(x => x.SenderId == userId && x.ReceiverId == model.ReceiverID);
                    if (notification != null)
                    {
                        _ctx.Notification.Remove(notification);
                    }

                    Notification Notification = new Notification
                    {
                        SenderId = userId,
                        ReceiverId = model.ReceiverID,
                        IsRead = false,
                        NotificationType = NotificationType.Message,
                        NotificationText = Resource.notificationSentmessage,
                    };

                    _ctx.Notification.Add(Notification);
                    await _ctx.SaveChangesAsync();
                    //var reciever = _ctx.Users.FirstOrDefault(x => x.Id == model.ReceiverID);
                    //var messagecount = await _ctx.ChatMessage.CountAsync(x => x.UserId == userId && x.ReceiverId == model.ReceiverID && x.CreatedDateTimeUTC == todayNowUtc);
                    //if (isNew || messagecount == 1)
                    //{
                    //    try
                    //    {
                    //        var emailTemplate = EmailTemplate.ReceiveMessageRequest;
                    //        Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                    //        dicPlaceholders.Add("{{name}}", reciever.Name);
                    //        dicPlaceholders.Add("{{sender}}", user.Name);
                    //        //dicPlaceholders.Add("{{password}}", password);
                    //        dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                    //        dicPlaceholders.Add("{{link}}", "ghttps://www.stylaaa.com/Account/Login");

                    //        IdentityMessage message = new IdentityMessage();
                    //        message.Subject = string.Format("Welcome to {0}", GlobalConfig.ProjectName + ".com!");
                    //        message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                    //        message.Destination = reciever.Email;
                    //        await new EmailService().SendAsync(message);
                    //    }
                    //    catch (Exception ex)
                    //    {
                    //        throw ex;
                    //    }
                    //}
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.SendMessageSuccess;

                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.SendMessageTextValid;
                    //mResult.Result = "please enter message";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }


        public int CountMessage(string userId, string receiverId)
        {
            return _ctx.ChatMessage.Count(x => (x.UserId == userId && x.ReceiverId == receiverId) || (x.UserId == userId && x.ReceiverId == receiverId));
        }

        public List<ChatConversationViewModel> GetChatConversation(ChatConversationGetModel model, string UserID)
        {
            var data = (from x in _ctx.ChatMessage
                        where (((x.UserId == UserID && x.ReceiverId == model.ReceiverID)
                        || (x.UserId == model.ReceiverID && x.ReceiverId == UserID)) && x.JobId == null)
                        orderby x.Id descending
                        //let user = x.UserId == UserID ? x.UserMaster1 : x.UserMaster
                        select new
                        {
                            x.Id,
                            x.UserId,
                            x.ReceiverId,
                            x.Message,
                            x.IsRead,
                            x.CreatedDateTimeUTC,
                            x.IsImage,
                            x.IsDeleted
                            //user,
                            //user.Name,
                            //UserID = user.Id,
                        }).OrderByDescending(SB => SB.CreatedDateTimeUTC).Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToList();

            return data.Select(x => new ChatConversationViewModel
            {
                ChatMessageID = x.Id,
                IsDeleted = x.IsDeleted,
                MessageText = x.IsDeleted == true ? " this message is deleted" : x.Message,
                IsImage = x.IsImage,
                IsRead = x.IsRead,
                IsSendByMe = x.UserId == UserID,
                TotalMiliSecond = Utility.GetUTCMilliSecond(x.CreatedDateTimeUTC),
                //Name = x.Name,
                ChatUserID = x.UserId,
                TimeCaption = Utility.TimeAgo(x.CreatedDateTimeUTC),
            }).OrderBy(TBL => TBL.TotalMiliSecond).ToList();
        }

        public List<ChatConversationViewModel> GetJobConversation(ChatConversationGetModel model, string UserID)
        {
            var data = (from x in _ctx.ChatMessage
                        where (((x.UserId == UserID && x.ReceiverId == model.ReceiverID)
                        || (x.UserId == model.ReceiverID && x.ReceiverId == UserID)) && x.JobId == model.JobId)
                        orderby x.Id descending
                        //let user = x.UserId == UserID ? x.UserMaster1 : x.UserMaster
                        select new
                        {
                            x.Id,
                            x.UserId,
                            x.ReceiverId,
                            x.Message,
                            x.IsRead,
                            x.CreatedDateTimeUTC,
                            x.IsImage,
                            //user,
                            //user.Name,
                            //UserID = user.Id,
                        }).OrderByDescending(SB => SB.CreatedDateTimeUTC).Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToList();

            return data.Select(x => new ChatConversationViewModel
            {
                ChatMessageID = x.Id,
                MessageText = x.Message,
                IsImage = x.IsImage,
                IsRead = x.IsRead,
                IsSendByMe = x.UserId == UserID,
                TotalMiliSecond = Utility.GetUTCMilliSecond(x.CreatedDateTimeUTC),
                //Name = x.Name,
                ChatUserID = x.UserId,
                TimeCaption = Utility.TimeAgo(x.CreatedDateTimeUTC),
            }).OrderBy(TBL => TBL.TotalMiliSecond).ToList();
        }

        public async Task<bool> ReadChatMessage(long ChatMessageID, string UserID)
        {
            _ctx.ChatMessage.Where(x => x.Id <= ChatMessageID && x.ReceiverId == UserID && !x.IsRead).ToList()
                   .ForEach(TBL => TBL.IsRead = true);
            await _ctx.SaveChangesAsync();
            return true;
        }

        public IQueryable<SendMessagesFromAdmin> MessagesListSendByAdmin(Role role)
        {
            return (from msg in _ctx.ChatMessage
                    where msg.ReceiverUser.Role == role && (msg.SenderUser.Role == Role.SuperAdmin || msg.SenderUser.Role == Role.Admin)
                    select new SendMessagesFromAdmin
                    {
                        MessageId = msg.Id,
                        MessageText = msg.Message,
                        UserId = msg.ReceiverUser.Id,
                        Name = msg.ReceiverUser.Name,
                        ProfileImage = msg.ReceiverUser.ProfileImageName,
                        Date = msg.CreatedDateTimeUTC
                    }).OrderBy(x => x.Name);
        }

        public async Task<ResponseModel<object>> DeleteMessage(long MessageId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ChatMessage message = _ctx.ChatMessage.FirstOrDefault(x => x.Id == MessageId);
                if (message == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = "Message Not Found";
                }
                else
                {
                    _ctx.ChatMessage.Remove(message);
                }
                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Message Deleted";
                }
                else
                {
                    mResult.Result = new object { };
                    mResult.Message = "Failed to delete Message";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteConversastionForJob(string senderId, string receiverId, long JobId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == receiverId);

            var messages = await _ctx.ChatMessage.Where(x => ((x.UserId == senderId && x.ReceiverId == receiverId) || (x.UserId == receiverId && x.ReceiverId == senderId)) && (x.JobId == JobId)).ToListAsync();

            var notification = await _ctx.Notification.Where(x => ((x.SenderId == senderId && x.ReceiverId == receiverId) || (x.SenderId == receiverId && x.ReceiverId == senderId)) && x.NotificationType == NotificationType.jobInquery && x.JobId == JobId).FirstOrDefaultAsync();

            if (notification != null)
            {
                _ctx.Notification.Remove(notification);
            }

            var request = await _ctx.JobRequests.Where(x => (x.StylistId == senderId && x.JobId == JobId) || (x.StylistId == receiverId && x.JobId == JobId)).FirstOrDefaultAsync();
            if (request != null)
            {
                _ctx.JobRequests.Remove(request);
            }

            if (messages != null)
            {

                _ctx.ChatMessage.RemoveRange(messages);
                await _ctx.SaveChangesAsync();
                mResult.Message = "Conversation deleted successfully";
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteConversationForChat(string senderId, string receiverId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            //var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == receiverId);

            var messages = await _ctx.ChatMessage.Where(x => ((x.UserId == senderId && x.ReceiverId == receiverId) || (x.UserId == receiverId && x.ReceiverId == senderId)) && (x.JobId == null)).ToListAsync();
            var chatbuddy = await _ctx.ChatBuddy.Where(x => (x.UserId == senderId && x.ReceiverId == receiverId) || (x.UserId == receiverId && x.ReceiverId == senderId)).FirstOrDefaultAsync();
            if (messages != null && chatbuddy != null)
            {
                _ctx.ChatMessage.RemoveRange(messages);
                _ctx.ChatBuddy.Remove(chatbuddy);
                await _ctx.SaveChangesAsync();
                mResult.Message = "Conversation deleted successfully";
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SendJobRequest(JobRequestModel model, string userId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                var todayNowUtc = Utility.GetSystemDateTimeUTC();
                var user = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == userId);
                var ReceiverID = model.RecId;
                var JobId = model.JobId;
                var MessageText = model.JobTextMessage;
                if (!string.IsNullOrEmpty(model.JobTextMessage))
                {
                    ChatMessage data = new ChatMessage
                    {
                        Message = model.JobTextMessage,
                        ReceiverId = ReceiverID,
                        UserId = userId,
                        IsImage = false,
                        JobId = model.JobId,
                    };
                    _ctx.ChatMessage.Add(data);

                    bool isNew = false;

                    JobRequest ObjJobReq = await _ctx.JobRequests.FirstOrDefaultAsync(x => x.JobId == JobId && x.StylistId == userId);
                    if (ObjJobReq == null)
                    {
                        isNew = true;
                        JobRequest jobreq = new JobRequest
                        {
                            JobId = JobId,
                            StylistId = userId
                        };
                        _ctx.JobRequests.Add(jobreq);
                        await _ctx.SaveChangesAsync();
                    }

                    //Notification notification = await _ctx.Notification.FirstOrDefaultAsync(x => x.SenderId == userId && x.ReceiverId == model.RecId);
                    //if (notification != null)
                    //{
                    //    _ctx.Notification.Remove(notification);
                    //}

                    //Notification Notification = new Notification
                    //{
                    //    SenderId = userId,
                    //    ReceiverId = model.RecId,
                    //    IsRead = false,
                    //    JobId = model.JobId,
                    //    NotificationType = NotificationType.jobInquery,
                    //    NotificationText = "someone interesting on your job",
                    //};

                    //_ctx.Notification.Add(Notification);

                    if (isNew)
                    {
                        Notification notification = new Notification
                        {
                            SenderId = userId,
                            ReceiverId = model.RecId,
                            IsRead = false,
                            NotificationType = NotificationType.jobInquery,
                            NotificationText = Resource.JobRequestNotification,
                            JobId = JobId
                        };
                        _ctx.Notification.Add(notification);
                        await _ctx.SaveChangesAsync();

                        StylistCredit objCredit = _ctx.StylistCredit.FirstOrDefault(x => x.StylistId == userId);
                        if (objCredit != null)
                        {
                            objCredit.StylistId = userId;
                            objCredit.Credit = objCredit.Credit - 1;
                            await _ctx.SaveChangesAsync();
                        }

                        var Receiver = _ctx.Users.FirstOrDefault(x => x.Id == ReceiverID);
                        try
                        {
                            string subject = "New message from stylist";
                            var emailTemplate = EmailTemplate.jobRequestForUser;
                            if (Receiver.Language == languageType.German)
                            {
                                emailTemplate = EmailTemplate.jobRequestForUserDe;
                                subject = "Neues Angebot für Dich!";
                            }

                            Dictionary<string, string> dicPlaceholders = new Dictionary<string, string>();
                            dicPlaceholders.Add("{{name}}", Receiver.Name);
                            //dicPlaceholders.Add("{{password}}", password);
                            dicPlaceholders.Add("{{url}}", GlobalConfig.EmailTemplateUrl);
                            dicPlaceholders.Add("{{link}}", "https://www.stylaaa.com/Chat/" + Receiver.Name + "/ChatJob");

                            IdentityMessage message = new IdentityMessage();
                            message.Subject = string.Format(subject);
                            message.Body = Utility.GenerateEmailBody(emailTemplate, dicPlaceholders);
                            message.Destination = Receiver.Email;
                            await new EmailService().SendAsync(message);
                            mResult.Status = ResponseStatus.Success;
                            // mResult.Message = Resource.forgotpasswordSendLinkSuccess;
                        }
                        catch (Exception ex)
                        {
                            throw ex;
                        }
                    }
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.SendMessageSuccess;

                }
                else
                {
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.SendMessageTextValid;
                    //mResult.Result = "please enter message";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"",
                            ve.PropertyName, ve.ErrorMessage);
                    }
                }
                return mResult;
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
