﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Helper;
using Stylaaa.Data.Identity;
using Stylaaa.Data.Services;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Spatial;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class StylishRepository : IDisposable
    {
        private ApplicationDbContext _ctx;
        private CategoryRepository _cat;
        private CoreUserRepository _user;
       
        public StylishRepository()
        {
            _user = new CoreUserRepository();
            _ctx = new ApplicationDbContext();
            _cat = new CategoryRepository();
            
        }

        public async Task<List<ViewAllStylishModel>> getAllStylish(StylishFilterPostModel model)
        {
            DbGeography currentLocation = null;
            if (!string.IsNullOrWhiteSpace(model.UserId))
            {
                currentLocation = _ctx.Users.FirstOrDefault(x => x.Id == model.UserId).GeoLocation;
                //distance = model.Miles.Value * GlobalConfig.MetersInOneKilometers;
            }

            List<ViewAllStylishModel> stylistlist = new List<ViewAllStylishModel>();

            stylistlist = await (from user in _ctx.Users
                                 join rt in _ctx.UserReviews on user.Id equals rt.StylishId into HasReview
                                 join follow in _ctx.Followers
                                 on new { model.UserId, FollowingId = user.Id } equals new { follow.UserId, follow.FollowingId } into has_follow
                                 where user.Role == Role.Stylist && user.isProfileCompleted == true && user.IsActive == true
                                 && (!string.IsNullOrEmpty(model.UserId) ? user.Id != model.UserId : true)
                                 && (!string.IsNullOrEmpty(model.Name) ? user.Name.Contains(model.Name) : true)
                                 select new ViewAllStylishModel
                                 {
                                     StylishId = user.Id,
                                     UserName = user.UserName,
                                     Name = user.Name,
                                     ProfileImageName = user.ProfileImageName,
                                     Role = user.Role,
                                     Isfollow = has_follow.Any(x => x.FollowingId == user.Id),
                                     TotalRatings = HasReview.Count(),
                                     IsVarified = user.IsVarified,
                                     Ratings = HasReview.Average(x => x.Rating),
                                     Experiance = user.Experiance,
                                     IsPremiumUser = _ctx.StylistPremiunTran.Any(x => x.StylistId == user.Id && (x.paymentStatus == ChargeStatus.succeeded || x.paymentStatus == ChargeStatus.pending)),
                                     //Year = (ct.Experiance != null ? Convert.ToDateTime(ct.Experiance).Year : 1)
                                     distance = user.GeoLocation.Distance(currentLocation)
                                 })
                          .Where(x =>
                            (model.Rating.HasValue ? model.Rating >= x.Ratings : true)
                          ).OrderBy(x => x.distance).ThenByDescending(x => x.IsPremiumUser)
                          //.OrderBy(x=>x.distance).OrderByDescending(x => x.IsPremiumUser).ThenByDescending(x => x.TotalRatings).ThenByDescending(x => x.Ratings).ThenBy(x => !x.Experiance.HasValue).ThenBy(p => p.Experiance)
                          //.OrderByDescending(x => (x.Experiance != null ? Convert.ToDateTime(x.Experiance).Year : 1))
                          .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();

            if (stylistlist != null)
            {
                foreach (var item in stylistlist)
                {
                    int year = 0;
                    double month = 0;
                    DateTime current = DateTime.UtcNow;
                    DateTime dt = Convert.ToDateTime(item.Experiance);
                    if (item.Experiance != null)
                    {
                        TimeSpan ts = current - dt;
                        year = ts.Days / 364;
                        month = (ts.Days % 365) / 31;
                    }
                    item.Year = year;
                    item.Months = month + 1;
                }
            }
            //stylistlist = stylistlist.OrderByDescending(x => x.IsPremiumUser).ThenByDescending(x => x.Ratings).OrderByDescending(x => x.Experiance).ToListAsync(); 
            return stylistlist;
        }
       
        public async Task<List<PopularStylishFilterModel>> FilterStylistlist(PostFilterModel model, string userId, bool isCount, string strLocalLatitude, string strLocalLongitude)
        {
            try
            {
                if (isCount)
                {
                    var result = (from user in _ctx.Users
                                  join ctry in _ctx.StylistCategory on user.Id equals ctry.StylistId into HasCategory
                                  where user.IsActive && (user.Name.Contains(model.SearchText) || user.UserName.Contains(model.SearchText))
                                  && user.Role == Role.Stylist && user.EmailConfirmed == true && user.isProfileCompleted == true && user.Id != userId
                                  select new PopularStylishFilterModel
                                  {
                                      UserId = user.Id,
                                      Categories = HasCategory.Select(x => x.CategoryId)
                                  });
                    //.Where(x => model.CategoryIds.Intersect(x.Categories).Any())
                    //.ToListAsync();
                    if (model.CategoryIds != null)
                    {
                        return await result.Where(x => model.CategoryIds.Intersect(x.Categories).Any()).ToListAsync();
                    }
                    else
                    {
                        return await result.ToListAsync();
                    }
                }
                else
                {
                    ApplicationUser objUser = await _ctx.Users.FirstOrDefaultAsync(x => x.Id == userId);

                    double? userLatitude = (objUser.GeoLocation != null ? objUser.GeoLocation.Latitude : 0);
                    double? userLongitude = (objUser.GeoLocation != null ? objUser.GeoLocation.Longitude : 0);

                    if (!String.IsNullOrEmpty(strLocalLatitude))
                    {
                        userLatitude = Convert.ToDouble(strLocalLatitude);
                    }
                    if (!String.IsNullOrEmpty(strLocalLongitude))
                    {
                        userLongitude = Convert.ToDouble(strLocalLongitude);
                    }

                    var query = (from user in _ctx.Users
                                 join ctry in _ctx.StylistCategory on user.Id equals ctry.StylistId into HasCategory
                                 where user.IsActive && (user.Name.Contains(model.SearchText) || user.UserName.Contains(model.SearchText))
                                 && user.Role == Role.Stylist && user.EmailConfirmed == true && user.isProfileCompleted == true && user.Id != userId
                                 select new PopularStylishFilterModel
                                 {
                                     UserId = user.Id,
                                     UserName = "@" + user.UserName,
                                     Name = user.Name,
                                     UserRole = user.Role,
                                     UserProfile = user.ProfileImageName,
                                     Latitude = user.GeoLocation.Latitude,
                                     Longitude = user.GeoLocation.Longitude,
                                     Categories = HasCategory.Select(x => x.CategoryId),
                                     IsPremium = _ctx.StylistPremiunTran.Any(x => x.StylistId == user.Id && (x.paymentStatus == ChargeStatus.succeeded || x.paymentStatus == ChargeStatus.pending))
                                 });
                    //.Where(x => model.CategoryIds.Intersect(x.Categories).Any()).ToListAsync();
                    //.OrderBy(x => x.TotalLikes)
                    // .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();
                    var result = await query.ToListAsync();
                    if (model.CategoryIds != null)
                    {
                        result = await query.Where(x => model.CategoryIds.Intersect(x.Categories).Any()).ToListAsync();
                    }

                    List<PopularStylishFilterModel> data = result.Select(user => new PopularStylishFilterModel()
                    {
                        UserId = user.UserId,
                        UserName = user.UserName,
                        UserRole = user.UserRole,
                        IsPremium = user.IsPremium,
                        Name = user.Name,
                        UserProfile = user.UserProfile,
                        Distance = Math.Round(Utility.GetDistanceInMiles(userLatitude, userLongitude, user.Latitude, user.Longitude), 1)
                    }).OrderBy(x => x.Distance)
                    //.Where(x => x.Distance < 100 || (!string.IsNullOrEmpty(model.SearchText) && x.Name.Contains(model.SearchText)))
                    .Skip(((model.PageIndex - 1) * 20)).Take(20).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                throw null;
            }
        }

        public IQueryable<UserDetailModel> GetAllStylishDetail()
        {
            return (from u in _ctx.Users
                    where u.Role == Role.Stylist
                    select new UserDetailModel
                    {
                        UserId = u.Id,
                        Name = u.Name,
                        Email = u.Email,
                        IsActive = u.IsActive,
                        ProfileImage = u.ProfileImageName,
                        Gender = u.GenderId.ToString(),
                        CreatedDate = u.CreatedDateTimeUTC
                        //Location = u.Location
                    }).OrderByDescending(x => x.CreatedDate);
        }

        public object IsUserExists(string userName, string currentUserName)
        {
            return _ctx.Users.Where(x => x.UserName == userName && x.UserName != currentUserName).Any();
        }

        public async Task<ViewStylishDetailsModel> GetStylishDetail(string UserId)
        {
            var date = Utility.GetAustriaDateTimeFromUTC();

            ViewStylishDetailsModel model = new ViewStylishDetailsModel();

            model = await (from user in _ctx.Users
                           join rtng in _ctx.UserReviews on user.Id equals rtng.StylishId into HasRating
                           join pst in _ctx.Post on user.Id equals pst.UserId into HasPost
                           //join Appoint in _ctx.StylishBookings on user.Id equals Appoint.StylishId into HasWork
                           where user.Id == UserId && user.Role == Role.Stylist
                           select new ViewStylishDetailsModel
                           {
                               StylishId = user.Id,
                               NickName = user.UserName,
                               Name = user.Name,
                               AboutMe = user.AboutMe,
                               ProfileImageName = user.ProfileImageName,
                               Experiance = user.Experiance,
                               CreatedDate = user.CreatedDateTimeUTC,
                               TotalRatings = HasRating.Count(),
                               PostCount = HasPost.Count(),
                               Average = HasRating.Average(x => x.Rating),
                               FollowerCount = _ctx.Followers.Count(x => x.FollowingId == UserId),
                               FollowingCount = _ctx.Followers.Count(x => x.UserId == UserId),
                           }).FirstOrDefaultAsync();

            if (model != null)
            {
                int year = 0;
                //double month = 0;
                DateTime current = DateTime.UtcNow;
                DateTime dt = Convert.ToDateTime(model.Experiance);
                if (model.Experiance != null)
                {
                    TimeSpan ts = current - dt;
                    year = ts.Days / 364;
                    //month = (ts.Days % 365) / 31;
                }
                model.year = year;
            }

            return model;
            //if (data != null)
            //{
            //    ViewStylishDetailsModel model = new ViewStylishDetailsModel()
            //    {
            //        StylishId = data.StylishId,
            //        NickName = data.NickName,
            //        Name = data.Name,
            //        ProfileImageName = data.ProfileImageName,
            //        TotalRatings = data.TotalRatings,
            //        Ratings = data.Ratings,
            //        PostCount = data.PostCount,
            //        Average = data.Average,
            //        WorkCount = data.WorkCount,
            //        FollowerCount = data.FollowerCount,
            //        FollowingCount = data.FollowingCount,
            //        StylishServices = data.StylishServices,
            //    };
            //    model.WorkTime = await StylishWorkingTimeById(UserId);
            //    return model;
            //}
            //return null;
        }

        public async Task<ViewStylishProfileModel> StylishById(string Id, string UserId = null)
        {
            var date = Utility.GetAustriaDateTimeFromUTC();

            var data = await (from ct in _ctx.Users
                              join pst in _ctx.Post on ct.Id equals pst.UserId into HasPost
                              //join wrk in _ctx.StylishBookings on ct.Id equals wrk.StylishId into HasWork
                              //join flw in _ctx.Followers on ct.Id equals flw.UserId into HasFollow
                              where ct.Id == Id && ct.Role == Role.Stylist && ct.isProfileCompleted == true
                              select new ViewStylishProfileModel
                              {
                                  Id = ct.Id,
                                  Name = ct.Name,
                                  NickName = ct.UserName,
                                  Role = ct.Role,
                                  ProfileImageName = ct.ProfileImageName,
                                  PostCount = HasPost.Count(),
                                  Isfollw = (!string.IsNullOrEmpty(UserId) && _ctx.Followers.Any(x => x.UserId == UserId && x.FollowingId == ct.Id)),
                                  //WorkCount = HasWork.Count(x => x.RequestStatus == RequestStatus.Accepted && x.ServiceTime <= date),
                                  FollowerCount = _ctx.Followers.Where(x => x.FollowingId == Id).Count(),
                                  FollowingCount = _ctx.Followers.Where(x => x.UserId == Id).Count()
                              }).FirstOrDefaultAsync();

            return data;
        }


        #region Stylist About
        public async Task<ResponseModel<StylistAboutModel>> GetStylistAbout(string UserId, string CurrentUserId)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType lng = languageType.German;
            if (CurrentLang == "EN")
            {
                lng = languageType.English;
            }
            if (String.IsNullOrWhiteSpace(CurrentLang))
            {
                if (!String.IsNullOrWhiteSpace(CurrentUserId))
                {
                    lng = _ctx.Users.Where(x => x.Id == CurrentUserId).Select(x => x.Language).FirstOrDefault();
                }
                else
                {
                    lng = _ctx.Users.Where(x => x.Id == UserId).Select(x => x.Language).FirstOrDefault();
                }
            }

            ResponseModel<StylistAboutModel> mResult = new ResponseModel<StylistAboutModel>();
            StylistAboutModel about = new StylistAboutModel();
            about = await (from user in _ctx.Users
                           where user.Id == UserId
                           select new StylistAboutModel
                           {
                               Id = user.Id,
                               Name = user.Name,
                               UserName = user.UserName,
                               GenderId = user.GenderId,
                               CountryID = user.Countries.Id,
                               CountryName = user.Countries.Name,
                               About = user.AboutMe,
                               Zipcode = user.ZipCode,
                               Experiance = user.Experiance,
                               LocationLatitude = user.GeoLocation.Latitude.Value,
                               LocationLongitude = user.GeoLocation.Longitude.Value,
                               Services = user.StylistCategoryTran.Select(x => new CategoryCheckbox
                               {
                                   Text = x.Categories.CategoryLanguageList.Where(y => y.LanguageId == lng).FirstOrDefault().Name,
                                   Value = x.CategoryId,
                                   IsChecked = true
                               }).ToList()
                           }).FirstOrDefaultAsync();
            if (about != null)
            {
                int year = 0;
                double month = 0;
                DateTime current = DateTime.UtcNow;
                DateTime dt = Convert.ToDateTime(about.Experiance);
                if (about.Experiance != null)
                {
                    TimeSpan ts = current - dt;
                    year = ts.Days / 364;
                    //month = (ts.Days % 365) / 31;
                }
                about.year = year;
                about.month = month;
                about.categorylist = GetUserCategoryList(UserId, CurrentUserId);
                mResult.Result = about;
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        private List<StylishCategoryModel> GetUserCategoryList(string UserId, string CurrentUserId)
        {
            string CurrentLang = Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper();
            languageType languageID = languageType.German;
            if (CurrentLang == "EN")
            {
                languageID = languageType.English;
            }
            if (String.IsNullOrWhiteSpace(CurrentLang))
            {
                if (!String.IsNullOrWhiteSpace(CurrentUserId))
                {
                    languageID = _ctx.Users.FirstOrDefault(s => s.Id == CurrentUserId).Language;
                }
                else
                {
                    languageID = _ctx.Users.Where(x => x.Id == UserId).Select(x => x.Language).FirstOrDefault();
                }
            }
            return (from cat in _ctx.Categories
                    join stycat in _ctx.StylistCategory.Where(x => x.StylistId == UserId) on cat.Id equals stycat.CategoryId into HasCategory
                    select new StylishCategoryModel
                    {
                        Id = cat.Id,
                        CategoryName = cat.CategoryLanguageList.Where(y => y.LanguageId == languageID).FirstOrDefault().Name,
                        CategoryImageName = cat.CategoryImage,
                        Selected = (HasCategory.Any() ? true : false)
                    }).ToList();
        }

        public async Task<ResponseModel<StylistAboutModel>> StylistCompleteProfile(StylistAboutModel model)
        {
            ResponseModel<StylistAboutModel> mResult = new ResponseModel<StylistAboutModel>();
            try
            {
                int year = model.year;
                int month = Convert.ToInt32(model.month);
                DateTime myDate = DateTime.Now;

                if (year > 0)
                {
                    DateTime newDate = myDate.AddYears(-year);
                    model.Experiance = newDate;
                }
                //if (year > 0 || month > 0)
                //{
                //    DateTime newDate = myDate.AddYears(-year).AddMonths(-month);
                //    model.Experiance = newDate;
                //}

                ApplicationUser objUser = _ctx.Users.FirstOrDefault(x => x.Id == model.Id);
                if (objUser == null)
                {
                    mResult.Result = model;
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    _ctx.StylistCategory.RemoveRange(_ctx.StylistCategory.Where(x => x.StylistId == model.Id));
                    await _ctx.SaveChangesAsync();
                    if (model.LocationLatitude == 0 || model.LocationLongitude == 0)
                    {
                        model.LocationLongitude = Convert.ToDouble(objUser.GeoLocation.Latitude);
                        model.LocationLatitude = Convert.ToDouble(objUser.GeoLocation.Latitude);
                    }
                    objUser.Name = model.Name;
                    objUser.UserName = model.UserName;
                    objUser.GenderId = model.GenderId;
                    objUser.CountryId = model.CountryID;
                    objUser.ZipCode = model.Zipcode;
                    objUser.AboutMe = model.About;
                    objUser.Experiance = model.Experiance;
                    objUser.GeoLocation = Data_Utility.CreatePoint(model.LocationLatitude, model.LocationLongitude);

                    if (model.categorylist != null)
                    {
                        objUser.StylistCategoryTran = (from fl in model.categorylist
                                                       where fl.Selected == true
                                                       select new StylistCategoryTran
                                                       {
                                                           StylistId = model.Id,
                                                           CategoryId = fl.Id
                                                       }).ToList();
                    }


                    objUser.isProfileCompleted = true;
                    int i = await _ctx.SaveChangesAsync();
                    if (i > 0)
                    {
                        var objAbout = await GetStylistAbout(objUser.Id, objUser.Id);
                        mResult.Result = objAbout.Result;
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = "Profile Completed";
                    }
                    else
                    {
                        var objAbout = await GetStylistAbout(objUser.Id, objUser.Id);
                        mResult.Result = objAbout.Result;
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "Profile Failed";
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mResult;
        }

        #endregion

        #region Stylist Followers and Following
        /// <summary>
        /// 
        /// </summary>
        /// <param name="Id"></param>
        /// <param name="UserId">
        /// UserId contains Logged in User
        /// </param>
        /// <returns></returns>
        public async Task<ResponseModel<UserFollowTransactionModel>> Following(string Id, string UserId)
        {
            ResponseModel<UserFollowTransactionModel> mResult = new ResponseModel<UserFollowTransactionModel>();
            UserFollowTransactionModel followingModel = new UserFollowTransactionModel();
            followingModel.User = await _user.GetUserDetail(Id);
            if (followingModel.User != null)
                followingModel.FollowModel = await (from following in _ctx.Followers
                                                    join rt in _ctx.UserReviews on following.Following.Id equals rt.StylishId into Hasreview
                                                    join loginfollow in _ctx.Followers
                                                    on new { following.FollowingId, UserId }
                                                    equals new { loginfollow.FollowingId, loginfollow.UserId }
                                                     into has_followingbyme
                                                    where following.UserId == Id && following.Following.IsActive == true
                                                    select new UsersFollowModel
                                                    {
                                                        Id = following.FollowingId,
                                                        Name = following.Following.Name,
                                                        UserId = following.Following.Id,
                                                        UserName = following.Following.UserName,
                                                        ProfileImageName = following.Following.ProfileImageName,
                                                        Role = following.Following.Role,
                                                        Isfollow = has_followingbyme.Any(),
                                                        IsPremiumUser = _ctx.StylistPremiunTran.Any(x => x.StylistId == following.FollowingId),
                                                        Ratings = Hasreview.Average(x => x.Rating)
                                                    }).Take(6)
                                                .ToListAsync();
            if (followingModel.FollowModel != null)
            {
                mResult.Message = "profile detail";
                mResult.Result = followingModel;
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        public async Task<ResponseModel<UserFollowTransactionModel>> Followers(string Id, string UserId)
        {
            ResponseModel<UserFollowTransactionModel> mResult = new ResponseModel<UserFollowTransactionModel>();
            UserFollowTransactionModel Followermodel = new UserFollowTransactionModel();
            Followermodel.User = await _user.GetUserDetail(Id);
            if (Followermodel.User != null)
                Followermodel.FollowModel = await (from follower in _ctx.Followers

                                                   join loginfollow in _ctx.Followers
                                                   on new { FollowingId = follower.UserId, UserId }
                                                   equals new { loginfollow.FollowingId, loginfollow.UserId }
                                                   into has_followingbyme

                                                   join rt in _ctx.UserReviews on follower.FollowedBy.Id equals rt.StylishId into Hasreview

                                                   where follower.FollowingId == Id && follower.FollowedBy.IsActive == true
                                                   select new UsersFollowModel
                                                   {
                                                       Id = follower.UserId,
                                                       Name = follower.FollowedBy.Name,
                                                       UserId = follower.FollowedBy.Id,
                                                       UserName = follower.FollowedBy.UserName,
                                                       ProfileImageName = follower.FollowedBy.ProfileImageName,
                                                       Role = follower.FollowedBy.Role,
                                                       Isfollow = has_followingbyme.Any(),
                                                       IsPremiumUser = _ctx.StylistPremiunTran.Any(x => x.StylistId == follower.UserId),
                                                       Ratings = Hasreview.Average(x => x.Rating)
                                                   }).Take(6)
                                                 .ToListAsync();
            if (Followermodel != null)
            {
                mResult.Message = "Not Found";
                mResult.Result = Followermodel;
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }

        #endregion

        #region Stylist Rating And review
        /// <summary>
        /// Stylist review and rating
        /// </summary>
        /// <param name=""></param>
        /// <param name=""></param>
        /// <returns></returns>
        public async Task<ResponseModel<UserReviewModel>> MyReview(string StylishId, string UserId)
        {
            ResponseModel<UserReviewModel> mResult = new ResponseModel<UserReviewModel>();

            UserReviewModel objReview = new UserReviewModel { StylistPopupModel = new AddUserReviewModel { } };
            objReview.Stylish = await _user.GetUserDetail(StylishId);
            objReview.StylistPopupModel.StylishId = StylishId;
            objReview.StylistPopupModel.UserId = UserId;
            if (objReview.Stylish != null)
                objReview.Reviews = (from ur in _ctx.UserReviews
                                     where ur.StylishId == StylishId
                                     select new StylistReviewModel
                                     {
                                         Id = ur.Id,
                                         StylishId = ur.StylishId,
                                         UserId = ur.ReviewBy.Id,
                                         Name = ur.ReviewBy.Name,
                                         UserName = ur.ReviewBy.UserName,
                                         ProfileImageName = ur.ReviewBy.ProfileImageName,
                                         UserRole = ur.ReviewBy.Role,
                                         Rating = ur.Rating,
                                         Comment = ur.Comment,
                                         ReviewDate = ur.ReviewDate,
                                         IsReply = ur.IsReply,
                                         IsSpam = ur.IsSpam,
                                         ReplyList = ur.Replies.Select(x => new ReviewReplyModel
                                         {
                                             id = x.Id,
                                             ReviewId = x.ReviewId,
                                             ReplyText = x.ReplyText,
                                             ProfileImageName = x.Reviews.Stylish.ProfileImageName,
                                             UserName = x.Reviews.Stylish.UserName,
                                             Name = x.Reviews.Stylish.Name,
                                             UserId = x.Reviews.Stylish.Id,
                                         }).ToList()
                                     }).OrderByDescending(x => x.ReviewDate);

            objReview.TotalRating = objReview.Reviews.Count();
            if (objReview.TotalRating != 0)
            {
                objReview.Avarage = objReview.Reviews.Average(x => x.Rating);
            }
            objReview.isShowRateHere = !objReview.Reviews.Where(x => x.UserId == UserId).Any();

            if (objReview != null)
            {
                mResult.Result = objReview;
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Message = Resource.noReviewFound;
                mResult.Result = new UserReviewModel { };
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> AddReply(ReplyToReviewModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            UserReview objreview = await _ctx.UserReviews.FirstOrDefaultAsync(x => x.Id == model.ReviewId);
            if (objreview != null)
            {
                objreview.IsReply = true;
                await _ctx.SaveChangesAsync();
                ReviewReply objReply = new ReviewReply();
                objReply = new ReviewReply
                {
                    ReviewId = model.ReviewId,
                    ReplyText = model.ReviewText
                };
                _ctx.ReviewReplay.Add(objReply);
                int i = await _ctx.SaveChangesAsync();
                if (i > 0)
                {
                    mResult.Message = Resource.replySuccess;
                    mResult.Status = ResponseStatus.Success;
                }
                else
                {
                    mResult.Message = Resource.replySuccess;
                    mResult.Status = ResponseStatus.Failed;
                }
            }
            else
            {
                mResult.Message = Resource.noReviewFound;
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> SpamReview(AddToSpamModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            UserReview objreview = await _ctx.UserReviews.FirstOrDefaultAsync(x => x.Id == model.ReviewId);
            if (objreview != null)
            {
                if (!objreview.IsSpam)
                {
                    objreview.IsSpam = true;
                    await _ctx.SaveChangesAsync();
                    SpamReview objspam = new SpamReview();
                    objspam = new SpamReview
                    {
                        ReviewId = model.ReviewId,
                        ReasonText = model.SpamReasonTxt,
                        UserId = model.UserId
                    };
                    _ctx.SpamReview.Add(objspam);
                    int i = await _ctx.SaveChangesAsync();
                    if (i > 0)
                    {
                        mResult.Message = Resource.ReviewAddedSpam;
                        mResult.Status = ResponseStatus.Success;
                    }
                    else
                    {
                        mResult.Message = Resource.AlreadySpamlisttext;
                        mResult.Status = ResponseStatus.Failed;
                    }
                }
                else
                {
                    mResult.Message = Resource.AlreadySpamlisttext;
                    mResult.Status = ResponseStatus.Failed;
                }
            }
            else
            {
                mResult.Message = Resource.AlreadySpamlisttext;
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteReviewById(long id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            UserReview objreview = await _ctx.UserReviews.FirstOrDefaultAsync(x => x.Id == id);
            if (objreview != null)
            {
                var ReplyReview = _ctx.ReviewReplay.Where(x => x.ReviewId == id);
                _ctx.ReviewReplay.RemoveRange(ReplyReview);

                _ctx.UserReviews.Remove(objreview);

                long objId = await _ctx.SaveChangesAsync();
                if (objId > 0)
                {
                    mResult.Message = Resource.reviewDeletedSuccess;
                    mResult.Status = ResponseStatus.Success;
                }
            }
            else
            {
                mResult.Message = "Ooops! Failed To Delete";
                mResult.Status = ResponseStatus.Failed;
            }
            return mResult;
        }

        public async Task<ResponseModel<object>> DeleteReply(long replyid)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            var replyData = _ctx.ReviewReplay.Where(x => x.Id == replyid).FirstOrDefault();
            if (replyData != null)
            {
                _ctx.ReviewReplay.Remove(replyData);
                int i = await _ctx.SaveChangesAsync();
                if (i > 0)
                {
                    mResult.Result = "1";
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.replySuccess;

                }
                else
                {
                    mResult.Result = "0";
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.replydeleted;
                }
            }
            return mResult;
        }

        public async Task<AddUserReviewModel> GetReview(long reviewId)
        {
            AddUserReviewModel result = await (from rvw in _ctx.UserReviews
                                               where rvw.Id == reviewId
                                               select new AddUserReviewModel
                                               {
                                                   Id = reviewId,
                                                   Comment = rvw.Comment,
                                                   Rating = rvw.Rating,
                                                   StylishId = rvw.StylishId,
                                                   UserId = rvw.UserId
                                               }).FirstOrDefaultAsync();
            return result;
        }

        public IQueryable<StylistReviewModel> MyReviewByRate(string StylishId, int Rate)
        {
            return (from ur in _ctx.UserReviews
                    join user in _ctx.Users on ur.UserId equals user.Id
                    where ur.StylishId == StylishId && ur.Rating == Rate
                    select new StylistReviewModel
                    {
                        Id = ur.Id,
                        StylishId = ur.StylishId,
                        UserId = ur.UserId,
                        Name = user.Name,
                        ProfileImageName = user.ProfileImageName,
                        Rating = ur.Rating,
                        Comment = ur.Comment,
                        ReviewDate = ur.ReviewDate,
                    }).OrderBy(x => x.ReviewDate);
        }

        public async Task<ResponseModel<UserReviewModel>> AddEditReview(AddUserReviewModel model)
        {
            ResponseModel<UserReviewModel> mResult = new ResponseModel<UserReviewModel>();
            try
            {
                bool isDuplicate = IsDuplicateReview(model);
                if (!isDuplicate)
                {
                    UserReview objReview = new UserReview();
                    objReview = new UserReview
                    {
                        StylishId = model.StylishId,
                        UserId = model.UserId,
                        Rating = model.Rating,
                        Comment = model.Comment,
                        ReviewDate = DateTime.UtcNow
                    };
                    _ctx.UserReviews.Add(objReview);
                    int id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        Notification notification = new Notification
                        {
                            SenderId = model.UserId,
                            ReceiverId = model.StylishId,
                            NotificationType = NotificationType.Rate,
                            rating = model.Rating,
                            IsRead = false,
                            NotificationText = Resource.RatingNotificationtext,
                        };
                        _ctx.Notification.Add(notification);
                        int notify = await _ctx.SaveChangesAsync();

                        var objMyReview = await MyReview(model.StylishId, model.UserId);
                        mResult.Result = objMyReview.Result;
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.rating;
                    }
                    else
                    {
                        var objMyReview = await MyReview(model.StylishId, model.UserId);
                        mResult.Result = objMyReview.Result;
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "failed to rate!";
                    }
                }
                else
                {
                    UserReview objReview = _ctx.UserReviews.Where(x => x.Id == model.Id).FirstOrDefault();

                    objReview.Rating = model.Rating;
                    objReview.Comment = model.Comment;
                    objReview.ReviewDate = DateTime.UtcNow;

                    int id = await _ctx.SaveChangesAsync();
                    if (id > 0)
                    {
                        Notification notification = new Notification
                        {
                            SenderId = model.UserId,
                            ReceiverId = model.StylishId,
                            NotificationType = NotificationType.Rate,
                            rating = model.Rating,
                            IsRead = false,
                            NotificationText = Resource.RatingNotificationtext,
                        };
                        _ctx.Notification.Add(notification);
                        int notify = await _ctx.SaveChangesAsync();

                        var objMyReview = await MyReview(model.StylishId, model.UserId);
                        mResult.Result = objMyReview.Result;
                        mResult.Status = ResponseStatus.Success;
                        mResult.Message = Resource.ratingSuccess;
                    }
                    else
                    {
                        var objMyReview = await MyReview(model.StylishId, model.UserId);
                        mResult.Result = objMyReview.Result;
                        mResult.Status = ResponseStatus.Failed;
                        mResult.Message = "failed to rate!";
                    }

                    //var objMyReview = await MyReview(model.StylishId, model.UserId);
                    //mResult.Result = objMyReview.Result;
                    //mResult.Status = ResponseStatus.Failed;
                    //mResult.Message = "You have already rated this Stylist!";
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        public bool IsDuplicateReview(AddUserReviewModel model)
        {
            return _ctx.UserReviews.Any(x => x.UserId == model.UserId && x.StylishId == model.StylishId);
        }
        #endregion

        #region Payment History
        public async Task<List<StylistPaymentHistoryModel>> GetStylistPremiumTransaction(PaginationModel model)
        {
            if (model.PageIndex > 0)
            {
                var query = (from stprm in _ctx.StylistPremiunTran
                             where stprm.StylistId == model.UserId && (stprm.paymentStatus == ChargeStatus.succeeded || stprm.paymentStatus == ChargeStatus.pending || stprm.paymentStatus == ChargeStatus.none)
                             select new
                             {
                                 PaymentHistoryId = stprm.Id,
                                 stprm.UpgradeAccount.Month,
                                 StartDate = stprm.SubscribeDate,
                                 EndDate = stprm.ExpireDate,
                                 PaymentMothod = stprm.PaymentMothod.PaymentMethodLanguageList.FirstOrDefault().PaymentMethod,
                                 stprm.UpgradeAccount.Amount,
                                 stprm.receiptFileName,
                                 stprm.paymentStatus
                             });

                var result = await query.ToListAsync();

                List<StylistPaymentHistoryModel> data = result.Select(stprm => new StylistPaymentHistoryModel()
                {
                    PaymentHistoryId = stprm.PaymentHistoryId,
                    Package = stprm.Month + (stprm.Month > 1 ? " " + Resource.months : " " + Resource.month),
                    StartDate = stprm.StartDate,
                    EndDate = stprm.EndDate,
                    PaymentMothod = stprm.PaymentMothod.ToUpper(),
                    AmountPaid = Math.Floor(stprm.Amount) + " €",
                    InvoiceLink = GlobalConfig.XeroReceiptFileUrl + stprm.receiptFileName,
                    PaymentStatus = stprm.paymentStatus,
                    isExpire = (stprm.EndDate > DateTime.UtcNow ? ((stprm.EndDate - DateTime.UtcNow).Days > 6 ? false : true) : true)
                }).OrderByDescending(x => x.StartDate)
                .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToList();

                return data;
            }
            else
            {
                var result = (from stprm in _ctx.StylistPremiunTran
                              where stprm.StylistId == model.UserId && (stprm.paymentStatus == ChargeStatus.succeeded || stprm.paymentStatus == ChargeStatus.pending)
                              select new StylistPaymentHistoryModel
                              {
                                  PaymentHistoryId = stprm.Id
                              });


                return await result.ToListAsync();
            }
        }
        #endregion

        #region JobCredit
        public async Task<List<StylistCreditPaymentHistory>> GetStylistCreditHistory(PaginationModel model)
        {
            List<StylistCreditPaymentHistory> creditHistory = new List<StylistCreditPaymentHistory>();
            if (model.PageIndex > 0)
            {
                creditHistory = await (from jobreq in _ctx.JobRequests
                                       where jobreq.StylistId == model.UserId
                                       select new StylistCreditPaymentHistory
                                       {
                                           JobId = jobreq.job.Id,
                                           JobName = jobreq.job.JobTitle,
                                           StylistId = model.UserId,
                                           Date = jobreq.CreatedDateTimeUTC,
                                           Country = jobreq.job.Country,
                                           Zipcode = jobreq.job.ZipCode,
                                           CreditUsed = 1,
                                           Budget = jobreq.job.Price
                                       }).OrderByDescending(x => x.JobId)
               .Skip(((model.PageIndex - 1) * GlobalConfig.PageSize)).Take(GlobalConfig.PageSize).ToListAsync();

            }
            else
            {
                creditHistory = await (from jobreq in _ctx.JobRequests
                                       where jobreq.StylistId == model.UserId
                                       select new StylistCreditPaymentHistory
                                       {
                                           JobId = jobreq.job.Id,
                                           StylistId = model.UserId,
                                       }).ToListAsync();

            }
            return creditHistory;
        }

        public int GetStylistCredit(string stylistId)
        {
            return _ctx.StylistCredit.FirstOrDefault(x => x.StylistId == stylistId)?.Credit ?? 0;
        }
        #endregion
               
           
        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}