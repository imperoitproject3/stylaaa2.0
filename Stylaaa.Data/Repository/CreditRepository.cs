﻿using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class CreditRepository
    {
        private ApplicationDbContext _ctx;
        public CreditRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public int GetStylistCedit(string UserId)
        {
            int credit = _ctx.StylistCredit.Where(x => x.StylistId == UserId).FirstOrDefault()?.Credit ?? 0;
            if (credit > 0)
            {
                return credit;
            }
            else
            {
                return 0;
            }
        }

    }
}
