﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class CountryRepo:IDisposable
    {
        private ApplicationDbContext _ctx;

        public CountryRepo()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<CountryModel> IQueryable_Country()
        {
            return (from ct in _ctx.Countries
                    select new CountryModel
                    {
                        CountryId = ct.Id,
                        Name = ct.Name
                    }).OrderBy(x => x.Name);
        }

        public IQueryable<CountryModel> GetCountryById(long Id)
        {
            return (from ct in _ctx.Countries
                    where ct.Id == Id
                    select new CountryModel
                    {
                        CountryId = ct.Id,
                        Name = ct.Name
                    }).OrderBy(x => x.Name);

        }

        public async Task<long> InsertOrUpdateCountry(CountryModel cmodel)
        {
            try
            {
                Countries objCountry = await _ctx.Countries.FirstOrDefaultAsync(x => x.Id == cmodel.CountryId);
                bool isNew = false;
                if (objCountry == null)
                {
                    objCountry = new Countries();
                    isNew = true;
                }
                objCountry.Name = cmodel.Name;
                if (isNew)
                    _ctx.Countries.Add(objCountry);
                await _ctx.SaveChangesAsync();
                return objCountry.Id;
            }
            catch
            {
                return 0;
            }
        }

        public bool IsDuplicateCountry(CountryModel cmodel)
        {
            return _ctx.Countries.Any(x => x.Name.Equals(cmodel.Name) && x.Id != cmodel.CountryId);
        }

        public async Task<CountryModel> EditCountryById(long Id)
        {
            return await (from ct in _ctx.Countries
                          where ct.Id == Id
                          select new CountryModel
                          {
                              CountryId = ct.Id,
                              Name = ct.Name
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<object>> DeleteCountry(long Id)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                if (_ctx.StylistCategory.Any(x => x.CategoryId == Id))
                {
                    mResult.Result = new object { };
                    mResult.Message = "This Category is Assigned to Some Stylist";
                }
                else
                {
                    Countries catObj = await _ctx.Countries.FirstOrDefaultAsync(x => x.Id == Id);
                    if (catObj == null)
                    {
                        mResult.Result = new object { };
                        mResult.Message = "No category found";
                    }
                    else
                    {
                        _ctx.Countries.Remove(catObj);
                        long id = await _ctx.SaveChangesAsync();
                        if (id > 0)
                        {
                            mResult.Status = ResponseStatus.Success;
                            mResult.Message = "Country deleted Successfully";
                        }
                        else
                        {
                            mResult.Result = new object { };
                            mResult.Message = "Failed to Delete Category";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }
            return mResult;
        }

        //public IQueryable<CategoryDropdownModel> GetAllCategories()
        //{
        //    return (from ct in _ctx.Categories
        //            select new CategoryDropdownModel
        //            {
        //                Id = ct.Id,
        //                Name = ct.Name
        //            }).OrderBy(x => x.Name);
        //}

        public void Dispose()
        {
            _ctx.Dispose();
        }
    }
}
