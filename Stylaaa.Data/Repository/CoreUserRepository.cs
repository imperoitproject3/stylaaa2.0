﻿using Microsoft.AspNet.Identity.EntityFramework;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Helper;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class CoreUserRepository
    {
        public ApplicationDbContext _ctx;
        private readonly ApplicationUserManager _userManager;

        public CoreUserRepository()
        {
            _ctx = new ApplicationDbContext();
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>(_ctx));
        }

        public IQueryable<UserDetailModel> getAllUsers_IQueryable()
        {

            return (from u in _ctx.Users
                    where u.Role == Role.User
                    select new UserDetailModel
                    {
                        UserId = u.Id,
                        Name = u.Name,
                        Email = u.Email,
                        IsActive = u.IsActive,
                        ProfileImage = u.ProfileImageName,
                        Gender = u.GenderId.ToString(),
                        CreatedDate=u.CreatedDateTimeUTC
                        //Location = u.Address,
                    }).OrderByDescending(x => x.CreatedDate);
        }
        
        public IQueryable<UserDetailModel> getAllUserAndStylist_IQueryable()
        {
            return (from u in _ctx.Users
                    where u.Role == Role.User || u.Role == Role.Stylist
                    select new UserDetailModel
                    {
                        UserId = u.Id,
                        Name = u.Name,
                        Email = u.Email,
                        IsActive = u.IsActive,
                        ProfileImage = u.ProfileImageName,
                        Gender = u.GenderId.ToString(),
                        CreatedDate=u.CreatedDateTimeUTC
                        //Location = u.Address,
                    }).OrderByDescending(x => x.CreatedDate);
        }

        public async Task<ViewUserDetail> GetUserDetail(string UserId)
        {
            bool blnIsUserExpire = false;
            string strUserExpiryDate = "";
            if (_ctx.Users.Where(x => x.Id == UserId && x.Role == Role.User).Any())
            {
                blnIsUserExpire = false;
            }
            else
            {
                blnIsUserExpire = (_ctx.StylistPremiunTran.Where(x => x.StylistId == UserId && x.Stylist.Role == Role.Stylist && (x.paymentStatus == ChargeStatus.pending || x.paymentStatus == ChargeStatus.succeeded)).Any() ? ((_ctx.StylistPremiunTran.Where(x => x.StylistId == UserId && (x.paymentStatus == ChargeStatus.pending || x.paymentStatus == ChargeStatus.succeeded)).OrderByDescending(x => x.Id).Select(x => x.ExpireDate).FirstOrDefault().Date >= DateTime.Now.Date) ? false : true) : false);
                //blnIsUserExpire = _ctx.Users.Where(x => x.Id == UserId).Select(x => x.Role == Role.User).Any() ? false : true;
                strUserExpiryDate = (_ctx.StylistPremiunTran.Where(x => x.StylistId == UserId && x.Stylist.Role == Role.Stylist && (x.paymentStatus == ChargeStatus.pending || x.paymentStatus == ChargeStatus.succeeded)).Any() ? _ctx.StylistPremiunTran.Where(x => x.StylistId == UserId && (x.paymentStatus == ChargeStatus.pending || x.paymentStatus == ChargeStatus.succeeded)).OrderByDescending(x => x.Id).Select(x => x.ExpireDate).FirstOrDefault().ToString("dd MMMM yyyy") : "");
                if (System.Threading.Thread.CurrentThread.CurrentUICulture.CompareInfo.Name.ToUpper().Contains("DE"))
                {
                    strUserExpiryDate = strUserExpiryDate.Replace("January", Resource.January);
                    strUserExpiryDate = strUserExpiryDate.Replace("February", Resource.February);
                    strUserExpiryDate = strUserExpiryDate.Replace("March", Resource.March);
                    strUserExpiryDate = strUserExpiryDate.Replace("April", Resource.April);
                    strUserExpiryDate = strUserExpiryDate.Replace("May", Resource.May);
                    strUserExpiryDate = strUserExpiryDate.Replace("June", Resource.June);
                    strUserExpiryDate = strUserExpiryDate.Replace("July", Resource.July);
                    strUserExpiryDate = strUserExpiryDate.Replace("August", Resource.August);
                    strUserExpiryDate = strUserExpiryDate.Replace("September", Resource.September);
                    strUserExpiryDate = strUserExpiryDate.Replace("October", Resource.October);
                    strUserExpiryDate = strUserExpiryDate.Replace("November", Resource.November);
                    strUserExpiryDate = strUserExpiryDate.Replace("December", Resource.December);
                }
            }

            return await (from user in _ctx.Users
                          where user.Id == UserId
                          select new ViewUserDetail
                          {
                              Id = user.Id,
                              UserName = user.UserName,
                              Name = user.Name,
                              AboutMe = user.AboutMe,
                              ProfileImageUrl = GlobalConfig.UserImageUrl + user.ProfileImageName,
                              ProfileImageName = user.ProfileImageName,
                              CoverPicUrl = GlobalConfig.UserCoverPicUrl + user.CoverImageName,
                              CoverPicName = user.CoverImageName,
                              PostCount = user.Post.Count(x => x.PostBy.Id == UserId),
                              Role = user.Role,
                              CreatedDate = user.CreatedDateTimeUTC,
                              FollowingCount = _ctx.Followers.Count(x => x.UserId == UserId),
                              FollowerCount = _ctx.Followers.Count(x => x.FollowingId == user.Id),
                              profileUrl = GlobalConfig.baseUrl + user.Role + "/" + user.UserName,
                              IsUserExpire = blnIsUserExpire,
                              userExpiryDate = strUserExpiryDate,
                              IsPremium = _ctx.StylistPremiunTran.Any(x => x.StylistId == user.Id && (x.paymentStatus == ChargeStatus.pending || x.paymentStatus == ChargeStatus.succeeded)),
                              StripeAccountId = user.StripeAccountId,
                              Email = user.Email
                          }).FirstOrDefaultAsync();
        }

        public async Task<ResponseModel<UserAboutModel>> GetUserAbout(string UserId)
        {
            ResponseModel<UserAboutModel> mResult = new ResponseModel<UserAboutModel>();
            UserAboutModel about = new UserAboutModel();
            about = await (from user in _ctx.Users
                           where user.Id == UserId && user.Role == Role.User
                           select new UserAboutModel
                           {
                               Id = user.Id,
                               Name = user.Name,
                               UserName = user.UserName,
                               CountryId = user.Countries.Id,
                               CountryName = user.Countries.Name,
                               About = user.AboutMe,
                               GenderId = user.GenderId,
                               Zipcode = user.ZipCode,
                               LocationLatitude = user.GeoLocation.Latitude.Value,
                               LocationLongitude = user.GeoLocation.Longitude.Value,
                           }).FirstOrDefaultAsync();
            if (about != null)
            {
                mResult.Message = "About Detail";
                mResult.Result = about;
                mResult.Status = ResponseStatus.Success;
            }
            return mResult;
        }
               
        public async Task<ResponseModel<ViewUserProfileModel>> getUserProfile(string userId)
        {
            ResponseModel<ViewUserProfileModel> mResult = new ResponseModel<ViewUserProfileModel>();

            ViewUserProfileModel model = await (from ct in _ctx.Users
                                                join flw in _ctx.Followers on ct.Id equals flw.UserId into HasFollow
                                                where ct.Id == userId && ct.Role == Role.User && ct.isProfileCompleted == true
                                                select new ViewUserProfileModel
                                                {
                                                    Id = ct.Id,
                                                    Name = ct.Name,
                                                    NickName = ct.UserName,
                                                    Role = ct.Role,
                                                    GenderId = ct.GenderId,
                                                    //Location = ct.Address,
                                                    ProfileImageName = ct.ProfileImageName,
                                                    //Address = ct.Address,
                                                    FollowingCount = _ctx.Followers.Count(x => x.UserId == userId)
                                                }).FirstOrDefaultAsync();

            if (model != null)
            {
                mResult.Message = "profile detail";
                mResult.Result = model;
                mResult.Status = ResponseStatus.Success;
            }
            else
            {
                mResult.Message = "No data found!";
                mResult.Result = new ViewUserProfileModel { };
            }
            return mResult;
        }

        public async Task<ResponseModel<ViewUserFollowingModel>> Following(string Id, string UserId)
        {
            ResponseModel<ViewUserFollowingModel> mResult = new ResponseModel<ViewUserFollowingModel>();
            ViewUserFollowingModel model = new ViewUserFollowingModel();
            model.User = await UserById(Id);
            model.Following = await (from flw in _ctx.Followers
                                     join loginfollow in _ctx.Followers
                                     on new { flw.FollowingId, UserId }
                                     equals new { loginfollow.FollowingId, loginfollow.UserId }
                                     into has_followbyme
                                     where flw.UserId == Id
                                     select new ViewUsersFollowing
                                     {
                                         UserId = flw.FollowingId,
                                         Name = flw.Following.Name,
                                         NickName = flw.Following.UserName,
                                         ProfileImageName = flw.Following.ProfileImageName,
                                         role = flw.Following.Role,
                                         IsFollow = has_followbyme.Any()
                                     }).ToListAsync();
            if (model.User != null)
            {
                mResult.Message = "profile detail";
                mResult.Result = model;
                mResult.Status = ResponseStatus.Success;
            }

            return mResult;
        }

        public async Task<ResponseModel<ViewUserFollowingModel>> Followers(string Id, string UserId)
        {
            ResponseModel<ViewUserFollowingModel> mResult = new ResponseModel<ViewUserFollowingModel>();
            ViewUserFollowingModel model = new ViewUserFollowingModel();
            model.User = await UserById(Id);
            model.Following = await (from flw in _ctx.Followers

                                     join loginfollow in _ctx.Followers
                                     on new { FollowingId = flw.UserId, UserId }
                                     equals new { loginfollow.FollowingId, loginfollow.UserId }
                                     into has_followingbyme

                                     where flw.FollowingId == Id && flw.FollowedBy.IsActive == true
                                     select new ViewUsersFollowing
                                     {
                                         UserId = flw.FollowingId,
                                         Name = flw.Following.Name,
                                         NickName = flw.Following.UserName,
                                         ProfileImageName = flw.Following.ProfileImageName,
                                         role = flw.Following.Role,
                                         IsFollow = has_followingbyme.Any()
                                     }).ToListAsync();

            if (model.User != null)
            {
                mResult.Message = "profile detail";
                mResult.Result = model;
                mResult.Status = ResponseStatus.Success;
            }

            return mResult;
        }
               

        public async Task<ViewUserProfileModel> UserById(string Id)
        {
            return await (from ct in _ctx.Users
                          join flw in _ctx.Followers on ct.Id equals flw.UserId into HasFollow
                          where ct.Id == Id && ct.Role == Role.User && ct.isProfileCompleted == true
                          select new ViewUserProfileModel
                          {
                              Id = ct.Id,
                              Name = ct.Name,
                              NickName = ct.UserName,
                              Role = ct.Role,
                              GenderId = ct.GenderId,
                              //Location = ct.Address,
                              ProfileImageName = ct.ProfileImageName,
                              FollowingCount = _ctx.Followers.Where(x => x.UserId == Id).Count()
                          }).FirstOrDefaultAsync();
        }

        public async Task<ViewUserProfileModel> GetUserDetailForEdit(string Id)
        {
            var data = await _userManager.FindByIdAsync(Id);
            ViewUserProfileModel editUser = new ViewUserProfileModel();
            if (data != null)
            {
                editUser = await (from ct in _ctx.Users
                                  join flw in _ctx.Followers on ct.Id equals flw.UserId into HasFollow
                                  where ct.Id == Id && ct.Role == Role.User && ct.isProfileCompleted == true
                                  select new ViewUserProfileModel
                                  {
                                      Id = ct.Id,
                                      Name = ct.Name,
                                      NickName = ct.UserName,
                                      Role = ct.Role,
                                      GenderId = ct.GenderId,
                                      //Location = ct.Address,
                                      //City=ct.City,
                                      //Address = ct.Address,
                                      ProfileImageName = ct.ProfileImageName
                                  }).FirstOrDefaultAsync();
            }
            return editUser;
        }

        public UserProfileModel GetUserProfileModel(string Id)
        {
            UserProfileModel editUser = (from ct in _ctx.Users
                                         where ct.Id == Id
                                         select new UserProfileModel
                                         {
                                             Id = ct.Id,
                                             Name = ct.Name,
                                             Address = ct.Countries.Name,
                                             Email = ct.Email,
                                             zipcode = ct.ZipCode
                                         }).FirstOrDefault();
            //}
            return editUser;
        }

        public async Task<ResponseModel<UserAboutModel>> UserCompleteProfile(UserAboutModel model)
        {
            ResponseModel<UserAboutModel> mResult = new ResponseModel<UserAboutModel>();
            try
            {
                ApplicationUser objUser = _ctx.Users.FirstOrDefault(x => x.Id == model.Id);
                if (objUser == null)
                {
                    mResult.Result = model;
                    mResult.Status = ResponseStatus.Failed;
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    if (model.LocationLatitude == 0 || model.LocationLongitude == 0)
                    {
                        model.LocationLongitude = Convert.ToDouble(objUser.GeoLocation.Latitude);
                        model.LocationLatitude = Convert.ToDouble(objUser.GeoLocation.Latitude);
                    }
                    objUser.Name = model.Name;
                    objUser.UserName = model.UserName;
                    objUser.CountryId = model.CountryId;
                    objUser.ZipCode = model.Zipcode;
                    objUser.AboutMe = model.About;
                    objUser.GenderId = model.GenderId;
                    objUser.isProfileCompleted = true;
                    objUser.GeoLocation = Data_Utility.CreatePoint(model.LocationLatitude, model.LocationLongitude);
                    int i = await _ctx.SaveChangesAsync();
                    //if (i > 0)
                    //{
                    var objAbout = await GetUserAbout(objUser.Id);
                    mResult.Result = objAbout.Result;
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.profilecompleted;
                    //}
                    //else
                    //{
                    //    var objAbout = await GetUserAbout(objUser.Id);
                    //    mResult.Result = objAbout.Result;
                    //    mResult.Status = ResponseStatus.Failed;
                    //    mResult.Message = "failed to Profil Complete.";
                    //}

                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return mResult;
        }

        public async Task<ResponseModel<object>> UpdateUserDetail(UserEditProfileModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                ApplicationUser objUser = _ctx.Users.FirstOrDefault(x => x.Id == model.Id);
                if (objUser == null)
                {
                    mResult.Result = new object { };
                    mResult.Message = Resource.noReviewFound;
                }
                else
                {
                    objUser.Name = model.Name;
                    objUser.UserName = model.NickName;
                    //objUser.Address = model.Location;
                    objUser.GeoLocation = Data_Utility.CreatePoint(model.LocationLatitude, model.LocationLongitude);
                    //objUser.Address = model.Address;
                    objUser.ProfileImageName = string.IsNullOrWhiteSpace(model.ProfileImageName) ? objUser.ProfileImageName : model.ProfileImageName;

                    await _ctx.SaveChangesAsync();
                    mResult.Result = new { };
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = Resource.profilecompleted;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return mResult;
        }


    }
}