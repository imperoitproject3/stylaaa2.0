﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Repository
{
    public class PaymentMethodRepository
    {
        private readonly ApplicationDbContext _ctx;

        public PaymentMethodRepository()
        {
            _ctx = new ApplicationDbContext();
        }

        public IQueryable<PaymentMethodModel> IQueryable_GetAllPaymentMethods(languageType languageid)
        {
            return (from ct in _ctx.PaymentMethodMasterData
                    join ctlng in _ctx.PaymentMethodLanguage.Where(x => x.LanguageId == languageid) on ct.Id equals ctlng.PaymentMethodId into HasCategory
                    select new PaymentMethodModel
                    {
                        PaymentMethodId = ct.Id,
                        PaymentMethod = HasCategory.FirstOrDefault().PaymentMethod,
                        PaymentMethodText = HasCategory.FirstOrDefault().PaymentMethodText
                    }).OrderBy(x => x.PaymentMethodText);
        }

        public async Task<ResponseModel<object>> InsertOrUpdatePayment(AdminPaymentMethodModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                bool isNew = false;
                var data = await _ctx.PaymentMethodMasterData.FirstOrDefaultAsync(x => x.Id == model.Id);
                if (data == null)
                {
                    isNew = true;
                    data = new PaymentMethodMaster();
                }
                if (isNew)
                {
                    List<PaymentMethodLanguage> PaymentMethodLanguageList = new List<PaymentMethodLanguage>();
                    foreach (var item in model.PaymentMethodLanguageList)
                    {
                        PaymentMethodLanguageList.Add(new PaymentMethodLanguage
                        {
                            PaymentMethodId = data.Id,
                            PaymentMethod = item.PaymentMethodName,
                            PaymentMethodText = item.PaymentMethodText,
                            LanguageId = item.LanguageId
                        });
                    }
                    data.PaymentMethodLanguageList = PaymentMethodLanguageList;
                    _ctx.PaymentMethodMasterData.Add(data);
                    await _ctx.SaveChangesAsync();
                }
                else
                {
                    foreach (var item in data.PaymentMethodLanguageList)
                    {
                        PaymentMethodLanguageModel PaymentLanguage = model.PaymentMethodLanguageList.FirstOrDefault(x => x.LanguageId == item.LanguageId);
                        item.PaymentMethod = PaymentLanguage.PaymentMethodName;
                        item.PaymentMethodText = PaymentLanguage.PaymentMethodText;
                    }
                }
                await _ctx.SaveChangesAsync();

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "Payment Method saved successfully";
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }
       
        public async Task<ResponseModel<object>> DeleteMethodById(long PaymentId)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();

            try
            {
                PaymentMethodMaster data = await _ctx.PaymentMethodMasterData.FirstOrDefaultAsync(x => x.Id == PaymentId);
                if (data != null)
                {                    
                    _ctx.PaymentMethodMasterData.Remove(data);
                    await _ctx.SaveChangesAsync();
                    mResult.Status = ResponseStatus.Success;
                    mResult.Message = "Payment method deleted successfully!";
                }
                else
                {
                    mResult.Message = "No Payment method or already deleted!";
                }
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    foreach (var ve in eve.ValidationErrors)
                    {
                        mResult.Message = string.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                    }
                }
            }
            catch (Exception ex)
            {
                mResult.Message = ex.Message;
            }

            return mResult;
        }

        public async Task<AdminPaymentMethodModel> GetPaymentmethodDetail(long PaymentId)
        {
            return await (from c in _ctx.PaymentMethodMasterData
                          where c.Id == PaymentId
                          select new AdminPaymentMethodModel
                          {
                              Id = c.Id,                             
                              PaymentMethodLanguageList = c.PaymentMethodLanguageList.Select(x => new PaymentMethodLanguageModel
                              {
                                  LanguageId = x.LanguageId,
                                  PaymentMethodName = x.PaymentMethod,
                                  PaymentMethodText=x.PaymentMethodText,
                                  LanguageName = x.LanguageId.ToString()
                              }).ToList()
                          }).FirstOrDefaultAsync();
        }

        public async Task<PaymentMethodModel> EditCategoryById(languageType languageid, long Id)
        {
            //return await (from ct in _ctx.Categories
            //              where ct.Id == Id
            //              select new CategoryModel
            //              {
            //                  Id = ct.Id,
            //                  Name = ct.Name,
            //                  CategoryImage = ct.CategoryImage
            //              }).FirstOrDefaultAsync();

            return await (from ct in _ctx.PaymentMethodMasterData
                          join ctlng in _ctx.PaymentMethodLanguage.Where(x => x.LanguageId == languageid) on ct.Id equals ctlng.PaymentMethodId into HasPayment
                          where ct.Id == Id
                          select new PaymentMethodModel
                          {
                              PaymentMethodId = ct.Id,
                              PaymentMethod = HasPayment.FirstOrDefault().PaymentMethod,
                              PaymentMethodText=HasPayment.FirstOrDefault().PaymentMethodText                        
                          }).FirstOrDefaultAsync();
        }

        public bool IsPaymentMethodDuplicate(AdminPaymentMethodModel model)
        {
            bool isDuplicate = false;

            foreach (var item in model.PaymentMethodLanguageList)
            {
                isDuplicate = _ctx.PaymentMethodLanguage.Any(x => x.LanguageId == item.LanguageId && x.PaymentMethod == item.PaymentMethodName && x.PaymentMethodId != model.Id);
                if (isDuplicate)
                    break;
            }
            return isDuplicate;
        }
    }

}
