﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class Notification:EntityBase
    {
        public NotificationType NotificationType { get; set; }

        
        public string SenderId { get; set; }
        [ForeignKey("SenderId")]
        public virtual ApplicationUser Sender { get; set; }

        
        public string ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual ApplicationUser Receiver { get; set; }

        public string NotificationText { get; set; }

        public bool IsRead { get; set; }
        
        public long? PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

        public long? JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }

        public long? CommentId { get; set; }
        [ForeignKey("CommentId")]
        public virtual Comments Comment { get; set; }

        public long? ReplyCommentId { get; set; }
        [ForeignKey("ReplyCommentId")]
        public virtual ReplyComments ReplyComment { get; set; }

        public double rating { get; set; }

        public long? ReviewId { get; set; }
        [ForeignKey("ReviewId")]
        public virtual UserReview Reviews { get; set; }
    }
}
