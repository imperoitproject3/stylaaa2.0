﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class UpgradeAccountMaster : EntityBase
    {
        [Required]
        [Column(TypeName = "int")]
        public int Month { get; set; }

        [Required]
        [Column(TypeName = "decimal")]
        public decimal Amount { get; set; }

        public int Credit { get; set; }
    }
}
