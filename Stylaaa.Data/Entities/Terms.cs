﻿using Stylaaa.Core.Helper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class Terms:EntityBase
    {
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public virtual ICollection<TermsLanguage> TermsLanguageList { get; set; }
    }

    public class TermsLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "AboutUsId Required")]
        public long TermsId { get; set; }
        [ForeignKey("TermsId")]
        public virtual Terms Terms { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string TermsText { get; set; }
    }
}
