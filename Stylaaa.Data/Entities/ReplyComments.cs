﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class ReplyComments : EntityBase
    {
        public long CommentId { get; set; }
        [ForeignKey("CommentId")]
        public virtual Comments Comment { get; set; }

        public string Message { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser CommentedBy { get; set; }
    }
}
