﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Stylaaa.Core.Helper;

namespace Stylaaa.Data.Entities
{
    public class StylistCreditTransaction : EntityBase
    {
        public string StylistId { get; set; }
        [ForeignKey("StylistId")]
        public virtual ApplicationUser Stylist { get; set; }

        public long UpgradeCreditId { get; set; }
        [ForeignKey("UpgradeCreditId")]
        public virtual UpgradeCreditMaster UpgradeAccount { get; set; }

        public long PaymentMothodId { get; set; }
        [ForeignKey("PaymentMothodId")]
        public virtual PaymentMethodMaster PaymentMothod { get; set; }

        public string invoiceFileName { get; set; }

        public string receiptFileName { get; set; }

        [Required]
        public ChargeStatus paymentStatus { get; set; } = ChargeStatus.none;

        public string stripeResponce { get; set; }

        public string transactionId { get; set; }

        public bool IsInvoiceSended { get; set; }
    }
}
