﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class JobRequest : EntityBase
    {
        public long JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs job { get; set; }

        public string StylistId { get; set; }
        [ForeignKey("StylistId")]
        public virtual ApplicationUser Stylist { get; set; }
    }
}
