﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class UserReview : EntityBaseId
    {
        public string StylishId { get; set; }

        [ForeignKey("StylishId")]
        public virtual ApplicationUser Stylish { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ReviewBy { get; set; }

        [Required]
        public int Rating { get; set; }

        public DateTime ReviewDate { get; set; }

        public string Comment { get; set; }

        public bool IsReply { get; set; }

        public bool IsSpam { get; set; }

        public virtual ICollection<ReviewReply> Replies { get; set; }

    }

    public class ReviewReply : EntityBaseId
    {
        public long ReviewId { get; set; }
        [ForeignKey("ReviewId")]
        public virtual UserReview Reviews { get; set; }

        [Required(ErrorMessage = "Replay text Required")]
        public string ReplyText { get; set; }
    }

    public class SpamReview:EntityBaseId
    {
        public long ReviewId { get; set; }
        [ForeignKey("ReviewId")]
        public virtual UserReview Reviews { get; set; }

        [Required(ErrorMessage = "Reason text Required")]
        public string ReasonText { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser Spamby { get; set; }
    }
}
