﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class Bots : EntityBase
    {
        public string Title { get; set; }

        public RoleForBot UserType { get; set; }

        public int MaxFollow { get; set; }

        public int MaxComment { get; set; }       

        public bool IsActive { get; set; }
    }

    public class BotsDummyUsers : EntityBaseId
    {
        public long BotId { get; set; }
        [ForeignKey("BotId")]
        public virtual Bots Bot { get; set; }

        public string DummyUserId { get; set; }
        [ForeignKey("DummyUserId")]
        public virtual ApplicationUser DummyUser { get; set; }
    }

    public class BotFunctions : EntityBaseId
    {
        public long BotId { get; set; }
        [ForeignKey("BotId")]
        public virtual Bots Bot { get; set; }

        public BotFunctionEnum FunctionId { get; set; }
    }

    public class BotTiming:EntityBaseId
    {
        public long BotId { get; set; }
        [ForeignKey("BotId")]
        public virtual Bots Bot { get; set; }

        public int Timing { get; set; }
    }

    public class BotComments : EntityBaseId
    {
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Categories Categories { get; set; }
    }

    public class BotsCommentLangTran : EntityBaseId
    {
        public long BotCommentId { get; set; }
        [ForeignKey("BotCommentId")]
        public virtual BotComments BotComments { get; set; }

        public languageType LanguageId { get; set; }

        public string CommentText { get; set; }
    }

    public class BotMessages : EntityBaseId
    {
        public BotMessageType MessageTypeId { get; set; }
    }

    public class BotMessageTran:EntityBaseId
    {
        public long BotMessageId { get; set; }
        [ForeignKey("BotMessageId")]
        public virtual BotMessages BotMessage{ get; set; }

        public languageType LanguageId { get; set; }

        public string MessageText { get; set; }
    }

    public class UserTask:EntityBaseId
    {
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser User { get; set; }

        public int FollowTask { get; set; }

        public int CommentTask { get; set; }

        public int LikeTask { get; set; }

        public int MessageTask { get; set; }
    }
}
