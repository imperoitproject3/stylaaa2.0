﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class ContactUs : EntityBase
    {
        [Required]
        public string Name { get; set; } 

        [Required]
        public string Email { get; set; }

        public string Mobile { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string Message { get; set; }
    }
}