﻿using Stylaaa.Core.Helper;
using System;
using System.ComponentModel.DataAnnotations;

namespace Stylaaa.Data.Entities
{
    public class EntityBaseId
    {
        [Key]
        public long Id { get; set; }
    }

    public class EntityBase : EntityBaseId
    {
        [Required]
        public DateTime CreatedDateTimeUTC { get; set; } = Utility.GetSystemDateTimeUTC();

        public DateTime? UpdatedDateTimeUTC { get; set; }
    }
}
