﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class UserFollow : EntityBaseId
    {
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser FollowedBy { get; set; }

        public string FollowingId { get; set; }

        [ForeignKey("FollowingId")]
        public virtual ApplicationUser Following { get; set; }
    }
}
