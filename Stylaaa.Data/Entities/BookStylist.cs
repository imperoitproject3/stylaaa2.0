﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class BookStylist : EntityBase
    {
        public string Address { get; set; }

        public string Email { get; set; }

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public LandingGender Gender { get; set; }

        public string Notice { get; set; }

        public DateTime DateTime { get; set; }

        public StripeType PaymentMethod { get; set; }

        public double Amount { get; set; }

        public bool isPaymentDone { get; set; }

        public bool IsDeleted { get; set; }

        public string stripeResponce { get; set; }

        public string transactionId { get; set; }

        public ChargeStatus PaymentStatus { get; set; } = ChargeStatus.none;

        public virtual ICollection<BookedCategories> BookedCategories { get; set; }
        public virtual ICollection<BookedServices> BookedServices { get; set; }

    }

    public class BookedCategories : EntityBaseId
    {
        public long BookingId { get; set; }
        [ForeignKey("BookingId")]
        public virtual BookStylist BookStylist { get; set; }

        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Categories Categories { get; set; }
    }

    public class BookedServices : EntityBaseId
    {
        public long BookingId { get; set; }
        [ForeignKey("BookingId")]
        public virtual BookStylist BookStylist { get; set; }

        public long SubCategoryId { get; set; }
        [ForeignKey("SubCategoryId")]
        public virtual SubCategories SubCategories { get; set; }
    }
}
