﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class PostHashtags:EntityBaseId
    {
        [Required(ErrorMessage ="PostId Required")]
        public long PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }
        [Required(ErrorMessage ="Hashtag Required")]
        public string Hashtag { get; set; }
    }
}
