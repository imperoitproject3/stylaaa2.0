﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class Comments : EntityBase
    {
        public long PostId { get; set; }
        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

        public string Message { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser CommentedBy { get; set; }

        public bool IsSpam { get; set; }

        //public long? ParentId { get; set; }
        //[ForeignKey("ParentId")]
        //public virtual Comments Comment { get; set; }
    }
}
