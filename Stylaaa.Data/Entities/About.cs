﻿using Stylaaa.Core.Helper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class About : EntityBase
    {
        [Required]
        //[Column(TypeName = "varchar(MAX)")]
        //public string AboutUs { get; set; }
        public virtual ICollection<AboutUsLanguage> AboutUsLanguageList { get; set; }
    }
    public class AboutUsLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "AboutUsId Required")]
        public long AboutUsId { get; set; }
        [ForeignKey("AboutUsId")]
        public virtual About About { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string AboutUs { get; set; }
    }


    public class Imprints:EntityBase
    {
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public virtual ICollection<ImprintsLanguage> ImprintsLanguageList { get; set; }
    }
    public class ImprintsLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "ImprintId Required")]
        public long ImprintId { get; set; }
        [ForeignKey("ImprintId")]
        public virtual Imprints Imprints { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string ImprintText { get; set; }
    }

    public class PrivacyPolicy:EntityBase
    {
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public virtual ICollection<PrivacyPolicyLanguage> PrivacyPolicyLanguage { get; set; }
    }

    public class PrivacyPolicyLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "PolicyId Required")]
        public long PolicyId { get; set; }
        [ForeignKey("PolicyId")]
        public virtual PrivacyPolicy PrivacyPolicy { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string PrivacyPolicyText { get; set; }
    }

    public class WhatIsStylaaa : EntityBase
    {
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public virtual ICollection<WhatIsStylaaaLanguage> WhatIsStylaaaLanguageList { get; set; }
    }

    public class WhatIsStylaaaLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "WhatIsStylaaId Required")]
        public long WhatIsStylaaId { get; set; }
        [ForeignKey("WhatIsStylaaId")]
        public virtual WhatIsStylaaa WhatIsStylaaa { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string WhatIsStylaaaText { get; set; }
    }

    public class WhatIsStylaaaForUser : EntityBase
    {
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string WhatIsStylaaaText { get; set; }
    }

    public class WhatIsStylaaaForStylist : EntityBase
    {
        [Required]
        [Column(TypeName = "varchar(MAX)")]
        public string WhatIsStylaaaText { get; set; }
    }
    
}
