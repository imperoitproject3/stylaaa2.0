﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class SubCategories : EntityBaseId
    {
        [Required(ErrorMessage = "CategoryId Required")]
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Categories Categories { get; set; }

        public bool IsDelete { get; set; }

        public virtual ICollection<SubCategoriesLanguage> SubCategoryLanguageList { get; set; }
    }

    public class SubCategoriesLanguage : EntityBaseId
    {

        public languageType LanguageId { get; set; }

        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Price Required")]
        public double Price { get; set; }

        [Required(ErrorMessage = "Price Required")]
        public LandingGender GenderId { get; set; }

        [Required(ErrorMessage = "CategoryId Required")]
        public long SubCategoryId { get; set; }
        [ForeignKey("SubCategoryId")]
        public virtual SubCategories SubCategories { get; set; }
    }

    public class SubCategoriesPrice : EntityBaseId
    {
        public long SubCategoryId { get; set; }
        [ForeignKey("SubCategoryId")]
        public virtual SubCategories SubCategories { get; set; }

        public double Price { get; set; }
    }
}
