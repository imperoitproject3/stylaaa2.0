﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class Post : EntityBase
    {
        [Required]
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser PostBy { get; set; }

        public string Description { get; set; }
        
        public virtual ICollection<PostCategory> PostCategories { get; set; }
        public virtual ICollection<PostTransaction> PostFiles { get; set; }
        public virtual ICollection<Likes> PostLikes { get; set; }
        public virtual ICollection<PostShare> PostShare { get; set; }
        public virtual ICollection<Comments> PostComments { get; set; }
        public virtual ICollection<PostHashtags> PostHashTags { get; set; }
        //public virtual ICollection<PostServiceTransaction> PostServices { get; set; }
    }

    public class PostTransaction : EntityBaseId
    {
        public long PostId { get; set; }

        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

        public string FileName { get; set; }

        public bool IsVideo { get; set; }
    }

}
