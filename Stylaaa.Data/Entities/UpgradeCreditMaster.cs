﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Stylaaa.Data.Entities
{
    public class UpgradeCreditMaster:EntityBaseId
    {
        [Required]
        [Column(TypeName = "int")]
        public int Credit { get; set; }

        [Required]
        [Column(TypeName = "decimal")]
        public decimal Amount { get; set; }
    }
}
