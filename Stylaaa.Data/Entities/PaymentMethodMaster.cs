﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{

    public class PaymentMethodMaster : EntityBase
    {
        [Required(ErrorMessage = "Image Required")]

        public virtual ICollection<PaymentMethodLanguage> PaymentMethodLanguageList { get; set; }
    }

    public class PaymentMethodLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "CategoryId Required")]
        public long PaymentMethodId { get; set; }
        [ForeignKey("PaymentMethodId")]
        public virtual PaymentMethodMaster PaymentMethodMaster { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string PaymentMethod { get; set; }

        [Required]
        public string PaymentMethodText { get; set; }
    }
    
}
