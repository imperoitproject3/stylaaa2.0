﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class AppSetting : EntityBaseId
    {
        public bool IsFollow { get; set; }

        public bool IsLike { get; set; }

        public bool IsComment { get; set; }

        public bool IsMessage { get; set; }

    }
}
