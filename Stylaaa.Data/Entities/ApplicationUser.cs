﻿using Microsoft.AspNet.Identity.EntityFramework;
using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Stylaaa.Data.Entities
{
    public class ApplicationUser : IdentityUser
    {
        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Name { get; set; }

        [EnumDataType(typeof(Gender))]
        public Gender GenderId { get; set; } = Gender.Male;

        [Column(TypeName = "varchar(MAX)")]
        public string AboutMe { get; set; }

        public string ProfileImageName { get; set; }

        public DbGeography GeoLocation { get; set; }

        public string TimeZoneName { get; set; }


        public long CountryId { get; set; }
        [ForeignKey("CountryId")]
        public virtual Countries Countries { get; set; }

        public int ZipCode { get; set; }

        public DateTime CreatedDateTimeUTC { get; set; } = Utility.GetSystemDateTimeUTC();

        [EnumDataType(typeof(Role))]
        public Role Role { get; set; }

        public DateTime? Experiance { get; set; }
        //public string Address { get; set; }
        public string CoverImageName { get; set; }

        public bool isProfileCompleted { get; set; }

        public bool IsActive { get; set; } = true;

        public string IdentityProofName { get; set; }

        public bool IsVarified { get; set; }

        public bool IsDeactivated { get; set; }

        public bool IsDummy { get; set; }

        public languageType Language { get; set; }

        public string StripeAccountId { get; set; }

        public virtual ICollection<UserFollow> UserFollow { get; set; }
        public virtual ICollection<Post> Post { get; set; }
        public virtual ICollection<StylistCategoryTran> StylistCategoryTran { get; set; }
        //public virtual ICollection<ChatMessage> ChatMessage { get; set; }
        //public virtual ICollection<ChatMessage> ChatMessagereceiver { get; set; }
        //public virtual ICollection<ChatBuddy> ChatBuddySender { get; set; }
        //public virtual ICollection<ChatBuddy> ChatBuddyReceiver { get; set; }
    }
}