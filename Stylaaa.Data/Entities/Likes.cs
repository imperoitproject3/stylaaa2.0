﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class Likes : EntityBaseId
    {
        public long PostId { get; set; }

        [ForeignKey("PostId")]
        public virtual Post Post { get; set; }

        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public virtual ApplicationUser LikeBy { get; set; }

    }
}