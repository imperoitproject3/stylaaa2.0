﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using Stylaaa.Core.Helper;

namespace Stylaaa.Data.Entities
{
    public class Jobs : EntityBase
    {
        public string JobTitle { get; set; }

        public double Price { get; set; }

        public int ZipCode { get; set; }

        public string Country { get; set; }

        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser JobBy { get; set; }

        public JobStatus JobStatus { get; set; }

        public bool MarkAsDelete { get; set; }

        public virtual ICollection<JobCategories> JobCategories { get; set; }
        public virtual ICollection<JobFiles> JobFiles { get; set; }
    }


    public class JobCategories : EntityBaseId
    {
        public long JobId { get; set; }

        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }

        [Required(ErrorMessage = "CategoryId Required")]
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Categories Categories { get; set; }

        public bool IsActive { get; set; }

    }

    public class JobFiles : EntityBaseId
    {
        public long JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }

        public string FileName { get; set; }

        public bool IsVideo { get; set; }
    }
}
