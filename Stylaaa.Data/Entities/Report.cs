﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class Report:EntityBase
    {
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser ReportBy { get; set; }

        public string Reason { get; set; }
    }
}
