﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace Stylaaa.Data.Entities
{
    public class StylistCredit : EntityBase
    {
        public string StylistId { get; set; }
        [ForeignKey("StylistId")]
        public virtual ApplicationUser Stylist { get; set; }

        public int Credit { get; set; }
    }
}
