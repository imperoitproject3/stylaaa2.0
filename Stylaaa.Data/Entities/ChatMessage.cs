﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class ChatMessage:EntityBase
    {
        [Required(ErrorMessage ="UserId Required")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser SenderUser { get; set; }

        [Required(ErrorMessage = "ReceiverId Required")]
        public string ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual ApplicationUser ReceiverUser { get; set; }

        public string Message { get; set; }

        public bool IsRead { get; set; }

        public bool IsImage { get; set; }

        public string ImageName { get; set; }

        public bool IsDeleted { get; set; }

        public long? JobId { get; set; }
        [ForeignKey("JobId")]
        public virtual Jobs Job { get; set; }

        public virtual ICollection<ChatBuddy> ChatBuddy { get; set; }
    }


    public class ChatBuddy:EntityBaseId
    {
        [Required(ErrorMessage = "UserId Required")]
        public string UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual ApplicationUser SenderUser { get; set; }

        [Required(ErrorMessage = "ReceiverId Required")]
        public string ReceiverId { get; set; }
        [ForeignKey("ReceiverId")]
        public virtual ApplicationUser ReceiverUser { get; set; }

        public bool IsRequestAccepted { get; set; }
               
        [Required(ErrorMessage = "ChatId Required")]
        public long ChatId { get; set; }
        [ForeignKey("ChatId")]
        public virtual ChatMessage Chat { get; set; }

    }
}
