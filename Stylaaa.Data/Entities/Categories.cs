﻿using Stylaaa.Core.Helper;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Stylaaa.Data.Entities
{
    public class Categories : EntityBaseId
    {
        //[Required]
        //[MaxLength(50)]
        //[Column(TypeName = "varchar")]
        //public string Name { get; set; }

        [Required(ErrorMessage = "Image Required")]
        public string CategoryImage { get; set; }
        public virtual ICollection<CategoryLanguage> CategoryLanguageList { get; set; }
    }

    public class CategoryLanguage : EntityBaseId
    {
        [Required(ErrorMessage = "CategoryId Required")]
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Categories Categories { get; set; }

        public languageType LanguageId { get; set; }

        [Required]
        [MaxLength(50)]
        [Column(TypeName = "varchar")]
        public string Name { get; set; }
    }

    public class StylistCategoryTran:EntityBaseId
    {
        [Required(ErrorMessage ="Stylistid Required")]
        public string StylistId { get; set; }
        [ForeignKey("StylistId")]
        public virtual ApplicationUser Stylist { get; set; }


        [Required(ErrorMessage ="CategoryId Required")]
        public long CategoryId { get; set; }
        [ForeignKey("CategoryId")]
        public virtual Categories Categories { get; set; }        
    }
}
