﻿namespace Stylaaa.Data.Entities
{
    public class ApplicationSetting : EntityBaseId
    {       
        public bool Maintenance { get; set; }
        public bool ForceUpdate { get; set; }
    }
}