﻿using Stylaaa.Core.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Entities
{
    public class StylistPremiumTransaction : EntityBase
    {
        public string StylistId { get; set; }
        [ForeignKey("StylistId")]
        public virtual ApplicationUser Stylist { get; set; }

        public long UpgradeAccountId { get; set; }
        [ForeignKey("UpgradeAccountId")]
        public virtual UpgradeAccountMaster UpgradeAccount { get; set; }

        [Required]
        public DateTime SubscribeDate { get; set; }

        [Required]
        public DateTime ExpireDate { get; set; }

        public long PaymentMothodId { get; set; }
        [ForeignKey("PaymentMothodId")]
        public virtual PaymentMethodMaster PaymentMothod { get; set; }

        public bool isPaymentDone { get; set; }

        public string invoiceFileName { get; set; }

        public string receiptFileName { get; set; }

        [Required]
        public ChargeStatus paymentStatus { get; set; } = ChargeStatus.none;

        public string stripeResponce { get; set; }

        public string transactionId { get; set; }

        public bool IsInvoiceSended { get; set; }
    }
}
