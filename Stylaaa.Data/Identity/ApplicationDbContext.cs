﻿using Microsoft.AspNet.Identity.EntityFramework;
using Stylaaa.Data.Entities;
using Stylaaa.Data.Migrations;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stylaaa.Data.Identity
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext() : base("connectionString")
        {
            //Configuration.ProxyCreationEnabled = false;
            //Configuration.LazyLoadingEnabled = false;
            Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ApplicationUser>().ToTable("ApplicationUser");
            modelBuilder.Entity<IdentityRole>().ToTable("RoleMaster");
            modelBuilder.Entity<IdentityUserRole>().ToTable("UserRole");
            modelBuilder.Entity<IdentityUserClaim>().ToTable("UserClaim");
            modelBuilder.Entity<IdentityUserLogin>().ToTable("UserLogin");
        }
        public virtual DbSet<ApplicationSetting> ApplicationSettings { get; set; }
        public virtual DbSet<Categories> Categories { get; set; }
        public virtual DbSet<CategoryLanguage> CategoryLanguage { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<StylistCategoryTran> StylistCategory { get; set; }
        public virtual DbSet<WhatIsStylaaa> WhatIsStylaaa { get; set; }
        public virtual DbSet<WhatIsStylaaaLanguage> WhatIsStylaaaLanguage { get; set; }
        public virtual DbSet<WhatIsStylaaaForUser> WhatIsStylaaaForUser { get; set; }
        public virtual DbSet<WhatIsStylaaaForStylist> WhatIsStylaaaForStylist { get; set; }
        public virtual DbSet<Terms> Terms { get; set; }
        public virtual DbSet<TermsLanguage> TermsLanguage { get; set; }
        public virtual DbSet<About> About { get; set; }
        public virtual DbSet<AboutUsLanguage> AboutLanguage { get; set; }
        public virtual DbSet<Imprints> Imprints { get; set; }
        public virtual DbSet<ImprintsLanguage> ImprintsLanguage { get; set; }
        public virtual DbSet<PrivacyPolicy> PrivacyPolicy { get; set; }
        public virtual DbSet<PrivacyPolicyLanguage> PrivacyPolicyLanguage { get; set; }
        public virtual DbSet<Comments> Comments { get; set; }
        public virtual DbSet<ReplyComments> ReplyComments { get; set; }
        public virtual DbSet<Notification> Notification { get; set; }
        public virtual DbSet<ContactUs> ContactUs { get; set; }
        public virtual DbSet<Likes> Likes { get; set; }
        public virtual DbSet<Post> Post { get; set; }
        public virtual DbSet<PostReport> PostReport { get; set; }
        public virtual DbSet<PostShare> PostShare { get; set; }
        public virtual DbSet<PostTransaction> PostTransactions { get; set; }
        public virtual DbSet<PostHashtags> PostHashtags { get; set; }
        public virtual DbSet<PostCategory> PostCategory { get; set; }
        public virtual DbSet<UserReview> UserReviews { get; set; }
        public virtual DbSet<UpgradeAccountMaster> UpgradeAccountData { get; set; }
        public virtual DbSet<UpgradeCreditMaster> UpgradeCreditData { get; set; }
        public virtual DbSet<ReviewReply> ReviewReplay { get; set; }
        public virtual DbSet<SpamReview> SpamReview { get; set; }
        public virtual DbSet<UserFollow> Followers { get; set; }
        public virtual DbSet<ChatMessage> ChatMessage { get; set; }
        public virtual DbSet<ChatBuddy> ChatBuddy { get; set; }
        public virtual DbSet<StylistPremiumTransaction> StylistPremiunTran { get; set; }
        public virtual DbSet<StylistCreditTransaction> StylistCreditTrans { get; set; }
        public virtual DbSet<PaymentMethodMaster> PaymentMethodMasterData { get; set; }
        public virtual DbSet<PaymentMethodLanguage> PaymentMethodLanguage { get; set; }
        public virtual DbSet<Bots> Bots { get; set; }
        public virtual DbSet<BotsDummyUsers> BotsDummyUsers { get; set; }
        public virtual DbSet<BotFunctions> BotFunctions { get; set; }
        public virtual DbSet<BotTiming> BotTiming { get; set; }
        public virtual DbSet<BotComments> BotComments { get; set; }
        public virtual DbSet<BotsCommentLangTran> BotsCommentLangTran { get; set; }
        public virtual DbSet<BotMessages> BotMessages { get; set; }
        public virtual DbSet<BotMessageTran> BotMessageTran { get; set; }
        public virtual DbSet<UserTask> UserTask { get; set; }
        public virtual DbSet<Jobs> Jobs { get; set; }
        public virtual DbSet<JobRequest> JobRequests { get; set; }
        public virtual DbSet<JobCategories> JobCategories { get; set; }
        public virtual DbSet<JobFiles> JobFiles { get; set; }
        public virtual DbSet<StylistCredit> StylistCredit { get; set; }
        public virtual DbSet<AppSetting> AppSetting { get; set; }
        public virtual DbSet<SubCategories> SubCategories { get; set; }
        public virtual DbSet<SubCategoriesLanguage> SubCategoriesLanguage { get; set; }
        public virtual DbSet<BookStylist> BookStylist { get; set; }
        public virtual DbSet<BookedServices> BookedServices { get; set; }
        public object Country { get; internal set; }
    }
}
