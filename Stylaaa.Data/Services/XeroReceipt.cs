﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xero.Api.Core;
using Xero.Api.Core.File;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Status;
using Xero.Api.Core.Model.Types;
using Xero.Api.Example.Applications.Private;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;
using static Stylaaa.Data.Services.EmailService;

namespace Stylaaa.Data.Services
{
    public class XeroReceipt
    {
        //public static async Task<ResponseModel<object>> Add(XeroReceiptSendModel model)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    DateTime NowDate = GlobalConfig.GetSystemDateTime();
            
        //    var private_app_api = GetXeroApi();
        //    var user = private_app_api.Users.Find();
        //    var allreceipt = private_app_api.Receipts.Find().OrderByDescending(x => x.Reference);
        //    Receipt receipt = new Receipt
        //    {
        //        //ReceiptNumber = "RCT-" + Convert.ToInt32(allreceipt.First().ReceiptNumber.Replace("RCT-", "")) + 1,
        //        Date = NowDate,
        //        Contact = new Contact { Name = model.userData.FirstName },
        //        LineAmountTypes = LineAmountType.Inclusive,
        //        LineItems = new List<LineItem>
        //        {
        //            new LineItem
        //            {
        //                AccountCode = "420",
        //                Description =  model.Title,
        //                UnitAmount =  (decimal)model.Amount,
        //                //TaxAmount =(decimal)model.Vat,
        //                Quantity = 1,
        //                //TaxType="INPUT",
        //                //DiscountRate=(decimal)model.Vat,
        //            }
        //        },
        //        Total = (decimal)model.Amount,
        //        //TotalTax = (decimal)model.Vat,
        //        User = new User
        //        {
        //            Id = user.First().Id
        //        },

        //        HasAttachments = true,
        //        Status = ReceiptStatus.Draft,
        //        Reference = model.TokenID
        //    };

        //    try
        //    {
        //        if (private_app_api != null)
        //        {
        //            var XeroResponse = private_app_api.Receipts.Create(receipt);

        //            if (XeroResponse != null)
        //            {
        //                mResult.Status = ResponseStatus.Success;
        //                mResult.Message = "Receipt generated successfully.";
        //                if (mResult.Status == ResponseStatus.Success)
        //                {
        //                    mResult = await ReceiptMarkAsSent(XeroResponse, model);
        //                }
        //            }
        //            else
        //            {
        //                mResult.Message = "Receipt not created.";
        //            }
        //        }
        //        else
        //        {
        //            mResult.Message = "GetXeroApi = null, Xero_pfxFilePath=" + GlobalConfig.Xero_pfxFilePath;
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        mResult.Message = "Add(), " + err.Message;
        //    }

        //    return mResult;
        //}

        //private static async Task<ResponseModel<object>> ReceiptMarkAsSent(Receipt XeroResponse, XeroReceiptSendModel model)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    var private_app_api = GetXeroApi();
        //    var user = private_app_api.Users.Find().First();

        //    try
        //    {
        //        if (XeroResponse != null)
        //        {
        //            StringBuilder sbMailContent = new StringBuilder();
        //            sbMailContent.Append("Dear " + XeroResponse.Contact.Name);
        //            sbMailContent.Append("<BR/><BR/>");
        //            sbMailContent.Append("Attached is a receipt for your payment of Euro " + XeroResponse.Total);
        //            sbMailContent.Append("<BR/><BR/>");
        //            sbMailContent.Append("If you have any questions, please let us know.");
        //            sbMailContent.Append("<BR/><BR/>");
        //            sbMailContent.Append("Thanks,");
        //            sbMailContent.Append("<BR/>" + GlobalConfig.ProjectName + "<BR/>");

        //            string ToMailID = XeroResponse.Contact.EmailAddress;
        //            string Subject = "Receipt of Invoice from " + GlobalConfig.ProjectName + " for " + XeroResponse.Contact.Name;
        //            string pdfFilePath = getReceiptPdf(XeroResponse.Id, XeroResponse.ReceiptNumber, model);

        //            mResult = await new EmailService().SendMailAsync(new EmailSendModel()
        //            {
        //                attachedFilePath = new List<string>() { pdfFilePath },
        //                mailContent = sbMailContent.ToString(),
        //                subject = Subject,
        //                toEmailId = ToMailID
        //            });

        //            if (mResult.Status == ResponseStatus.Success)
        //            {
        //                mResult.Message = "Receipt & payment created, and sent mail to customer.";
        //                mResult.Result = pdfFilePath.Replace(GlobalConfig.XeroReceiptFilePath, "");
        //            }
        //        }
        //        else
        //        {
        //            mResult.Message = "Receipt & payment Created, mail not send.";
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        mResult.Message = "ReceiptMarkAsSent(), " + err.Message;
        //    }
        //    return mResult;
        //}

        ////private static XeroCoreApi GetXeroApi()
        ////{
        ////    try
        ////    {
        ////        X509Certificate2 cert = new X509Certificate2(GlobalConfig.Xero_pfxFilePath, GlobalConfig.Xero_pfxPassword);
        ////        var private_app_api = new XeroCoreApi("https://api.xero.com", new PrivateAuthenticator(cert),
        ////            new Consumer(GlobalConfig.Xero_ConsumerKey, GlobalConfig.Xero_ConsumerSecret), null,
        ////            new DefaultMapper(), new DefaultMapper());
        ////        var org = private_app_api.Organisation;
        ////        Console.WriteLine("Org Name: " + org.Name);
        ////        var user = new ApiUser { Name = System.Environment.MachineName };
        ////        return private_app_api;
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        return null;
        ////    }
        ////}

        ////private static string getReceiptPdf(Guid ReceiptId, string ReceiptNumber, XeroReceiptSendModel model)
        ////{
        ////    var dateTimeNowUtc = Utility.GetSystemDateTimeUTC();
        ////    try
        ////    {
        ////        var private_app_api = GetXeroApi();
        ////        var creditNote = private_app_api.CreditNotes.Create(new CreditNote
        ////        {
        ////            Contact = new Contact { Name = model.userData.FirstName },
        ////            Type = CreditNoteType.AccountsReceivable,
        ////            Date = dateTimeNowUtc,
        ////            FullyPaidOnDate = dateTimeNowUtc,
        ////            LineAmountTypes = LineAmountType.Exclusive,
        ////            Status = InvoiceStatus.Authorised,
        ////            SentToContact = true,
        ////            LineItems = new List<LineItem>
        ////            {
        ////                new LineItem
        ////                {
        ////                    AccountCode = "720",
        ////                    Description = model.Title,
        ////                    UnitAmount = (decimal)model.Amount
        ////                }
        ////            }
        ////        });

        ////        BinaryFile pdfFile = private_app_api.PdfFiles.Get(PdfEndpointType.CreditNotes, creditNote.Id);
        ////        string PdfFileName = ReceiptNumber + ".pdf";
        ////        //string PdfFileName = "RECEIPT.pdf";
        ////        string PdfFileSavePath = GlobalConfig.XeroReceiptFilePath + PdfFileName;
        ////        pdfFile.Save(PdfFileSavePath);
        ////        return PdfFileSavePath;
        ////    }
        ////    catch (Exception err)
        ////    {
        ////        return "getReceiptPdf(), " + err.Message;
        ////    }
        ////}

        //public string GetRandomString(int length)
        //{
        //    var numArray = new byte[length];
        //    new RNGCryptoServiceProvider().GetBytes(numArray);
        //    return SanitiseBase64String(Convert.ToBase64String(numArray), length);
        //}
        //private string SanitiseBase64String(string input, int maxLength)
        //{
        //    input = input.Replace("-", "");
        //    input = input.Replace("=", "");
        //    input = input.Replace("/", "");
        //    input = input.Replace("+", "");
        //    input = input.Replace(" ", "");
        //    while (input.Length < maxLength)
        //        input = input + GetRandomString(maxLength);
        //    return input.Length <= maxLength ?
        //        input.ToUpper() :
        //        input.ToUpper().Substring(0, maxLength);
        //}
    }
}