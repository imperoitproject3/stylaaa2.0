﻿using Microsoft.AspNet.Identity;
using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Configuration;

namespace Stylaaa.Data.Services
{
    public class EmailService: IIdentityMessageService
    {
        public class EmailSendModel
        {
            public string mailContent { get; set; }
            public string toEmailId { get; set; }
            public string subject { get; set; }
            public List<string> attachedFilePath { get; set; }
        }


        public async Task SendAsync(IdentityMessage message)
        {
            string fromAddress = GlobalConfig.ProjectName + "<" + GlobalConfig.EmailUserName + ">";
            using (MailMessage mailMessage = new MailMessage(fromAddress, message.Destination))
            {
                try
                {
                    mailMessage.Subject = message.Subject;
                    mailMessage.Body = message.Body;
                    mailMessage.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = GlobalConfig.SMTPClient;
                    smtp.EnableSsl = true;
                    NetworkCredential NetworkCred = new NetworkCredential(GlobalConfig.EmailUserName, GlobalConfig.EmailPassword);
                    smtp.UseDefaultCredentials = true;
                    smtp.Credentials = NetworkCred;
                    smtp.Port = 587;
                    await smtp.SendMailAsync(mailMessage);
                }
                catch (Exception err)
                {
                    string errorMessage = err.Message;
                }
            }
        }

        public async Task<ResponseModel<object>> SendMailAsync(EmailSendModel model)
        {
            ResponseModel<object> mResult = new ResponseModel<object>();
            try
            {
                string fromAddress = GlobalConfig.ProjectName + "<" + GlobalConfig.EmailUserName + ">";
                MailMessage mailMessage = new MailMessage(fromAddress, model.toEmailId);
                mailMessage.Subject = model.subject;
                mailMessage.Body = model.mailContent.ToString();
                mailMessage.IsBodyHtml = true;

                if (model.attachedFilePath != null && model.attachedFilePath.Any())
                {
                    foreach (var item in model.attachedFilePath)
                    {
                        Attachment PDFfile = new Attachment(item);
                        mailMessage.Attachments.Add(PDFfile);
                    }
                }

                SmtpClient smtp = new SmtpClient();
                smtp.Host = GlobalConfig.SMTPClient;
                smtp.EnableSsl = true;
                NetworkCredential NetworkCred = new NetworkCredential(GlobalConfig.EmailUserName, GlobalConfig.EmailPassword);
                smtp.UseDefaultCredentials = true;
                smtp.Credentials = NetworkCred;
                smtp.Port = 587;
                await smtp.SendMailAsync(mailMessage);

                mResult.Status = ResponseStatus.Success;
                mResult.Message = "OK";
            }
            catch (Exception e)
            {
                mResult.Message = e.Message;
                mResult.Result = e;
            }
            return mResult;
        }
    }
}
