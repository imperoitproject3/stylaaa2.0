﻿using Stylaaa.Core.Helper;
using Stylaaa.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Xero.Api.Core;
using Xero.Api.Core.File;
using Xero.Api.Core.Model;
using Xero.Api.Core.Model.Status;
using Xero.Api.Core.Model.Types;
using Xero.Api.Example.Applications.Private;
using Xero.Api.Infrastructure.Exceptions;
using Xero.Api.Infrastructure.OAuth;
using Xero.Api.Serialization;
using static Stylaaa.Data.Services.EmailService;

namespace Stylaaa.Data.Services
{
    public class XeroInvoice
    {
        //public static async Task<ResponseModel<object>> Add(XeroInvoiceSendModel model)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    DateTime NowDate = GlobalConfig.GetSystemDateTime();

        //    Invoice invoice = new Invoice
        //    {
        //        Contact = new Contact
        //        {
        //            ContactNumber = model.userData.ContactNumber,
        //            DefaultCurrency = "Euro",
        //            EmailAddress = model.userData.CompanyEmailID,
        //            IsCustomer = true,
        //            Name = model.userData.FirstName,

        //            Addresses = new List<Address>
        //            {
        //                new Address {
        //                    AddressLine1 =model.userData.StreetAddress,
        //                    AddressLine2 = model.userData.FullAddress,
        //                    City = model.userData.CityName,
        //                    PostalCode = model.userData.Zipcode
        //                }
        //            },

        //            ContactPersons = new List<ContactPerson>
        //            {
        //                new ContactPerson {
        //                    EmailAddress = model.userData.EmailID,
        //                    FirstName = model.userData.FirstName,
        //                    LastName = model.userData.LastName,
        //                }
        //            },
        //        },

        //        Date = NowDate,
        //        DueDate = NowDate,
        //        Type = InvoiceType.AccountsReceivable,

        //        LineAmountTypes = LineAmountType.Exclusive,
        //        Reference = model.TokenID,
        //        Status = InvoiceStatus.Authorised,
        //        LineItems = new List<LineItem>
        //            {
        //                new LineItem
        //                {
        //                    AccountCode = "200",
        //                    Description = model.Title,
        //                    UnitAmount = (decimal)model.Amount,
        //                    TaxAmount = (decimal)model.Vat,
        //                    Quantity = 1,
        //                }
        //        }
        //    };

        //    try
        //    {
        //        var private_app_api = GetXeroApi();


        //        if (private_app_api != null)
        //        {
        //            var XeroResponse = private_app_api.Invoices.Create(invoice);

        //            if (XeroResponse != null)
        //            {
        //                mResult.Status = ResponseStatus.Success;
        //                mResult.Message = Resource.InvoicegenerateSuccess;
        //                //mResult = InvoiceMarkAsPaid(XeroResponse.Id, XeroResponse.Total, XeroResponse.Reference);
        //                if (mResult.Status == ResponseStatus.Success)
        //                {
        //                    mResult = await InvoiceMarkAsSent(XeroResponse);
        //                }
        //            }
        //            else
        //            {
        //                mResult.Message = Resource.InvoicenotcreateText;
        //            }
        //        }
        //        else
        //        {
        //            mResult.Message = "GetXeroApi = null, Xero_pfxFilePath=" + GlobalConfig.Xero_pfxFilePath;
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        mResult.Message = "Add(), " + err.Message;
        //    }

        //    return mResult;
        //}

        //public static string FindByInvoiceNumber(string strInvoiceNo)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    var private_app_api = GetXeroApi();
        //    var invoice = private_app_api.Invoices.InvoiceNumbers(new[] { strInvoiceNo }).Find().ToList().FirstOrDefault();
        //    string strMonth = invoice.LineItems.First().Description.ToString().Replace(" month subscription", "");
        //    return strMonth.Replace(" months subscription", "");
        //}

        //public static string FindByInvoiceNumberForCredit(string strInvoiceNo)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    var private_app_api = GetXeroApi();
        //    var invoice = private_app_api.Invoices.InvoiceNumbers(new[] { strInvoiceNo }).Find().ToList().FirstOrDefault();
        //    string strCredit = invoice.LineItems.First().Description.ToString().Replace(" Credits", "");
        //    return strCredit.Replace(" Credits", "");
        //}

        //public static string FindByInvoiceAndMarkAsPaid(string strInvoiceNo)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    var private_app_api = GetXeroApi();
        //    var invoice = private_app_api.Invoices.InvoiceNumbers(new[] { strInvoiceNo }).Find().ToList().FirstOrDefault();
        //    mResult = InvoiceMarkAsPaid(invoice.Id, invoice.Total, invoice.Reference);
        //    return invoice.LineItems.First().Description.ToString();
        //}

        //private static async Task<ResponseModel<object>> InvoiceMarkAsSent(Invoice XeroResponse)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    try
        //    {
        //        if (XeroResponse != null)
        //        {
        //            StringBuilder sbMailContent = new StringBuilder();
        //            sbMailContent.Append("Dear " + XeroResponse.Contact.Name);
        //            sbMailContent.Append("<BR/><BR/>");
        //            sbMailContent.Append("Please find attached invoice for your recent purchase on stylaaa");
        //            sbMailContent.Append("<BR/><BR/>");
        //            sbMailContent.Append("Thanks,");
        //            sbMailContent.Append("<BR/>");
        //            sbMailContent.Append(GlobalConfig.ProjectName);
        //            sbMailContent.Append("<BR/><BR/>");

        //            string ToMailID = XeroResponse.Contact.EmailAddress;
        //            string Subject = "Invoice " + XeroResponse.Number + " from " + GlobalConfig.ProjectName + " for " + XeroResponse.Contact.Name;
        //            string pdfFilePath = getInvoicePdf(XeroResponse.Id, XeroResponse.Number);

        //            mResult = await new EmailService().SendMailAsync(new EmailSendModel()
        //            {
        //                attachedFilePath = new List<string>() { pdfFilePath },
        //                mailContent = sbMailContent.ToString(),
        //                subject = Subject,
        //                toEmailId = ToMailID
        //            });


        //            if (mResult.Status == ResponseStatus.Success)
        //            {
        //                mResult.Message = Resource.onvoicemailsend;

        //                mResult.Result = pdfFilePath.Replace(GlobalConfig.XeroInvoiceFilePath, "");
        //            }
        //        }
        //        else
        //        {
        //            mResult.Message = Resource.invoicecreatemailnot;
        //        }
        //    }
        //    catch (Exception err)
        //    {
        //        mResult.Message = "InvoiceMarkAsSent(), " + err.Message;
        //    }
        //    return mResult;
        //}

        //private static ResponseModel<object> InvoiceMarkAsPaid(Guid InvoiceId, decimal? TotalAmount, string PaymentReference)
        //{
        //    ResponseModel<object> mResult = new ResponseModel<object>();
        //    DateTime NowDate = GlobalConfig.GetSystemDateTime();

        //    var private_app_api = GetXeroApi();
        //    var account = private_app_api.Accounts.Find();

        //    var payment = new Xero.Api.Core.Model.Payment
        //    {
        //        //CreditNote = new CreditNote { Number = note.Number },
        //        Invoice = new Invoice
        //        {
        //            Id = InvoiceId,
        //        },
        //        Account = new Account
        //        {
        //            Id = account.First().Id,
        //            Code = "090",
        //        },
        //        Amount = TotalAmount,
        //        Date = NowDate,
        //        Reference = PaymentReference,
        //        Status = PaymentStatus.Authorised,
        //    };

        //    payment.IsReconciled = true;

        //    try
        //    {
        //        var XeroResponse = private_app_api.Payments.Create(payment);
        //        if (XeroResponse != null)
        //        {
        //            mResult.Status = ResponseStatus.Success;
        //            mResult.Message = Resource.XeroInvoiceSuccess;
        //        }
        //        else
        //        {
        //            mResult.Message = Resource.XeroInvoiceSuccesspaymentNot;
        //        }
        //    }
        //    catch (ValidationException err)
        //    {
        //        mResult.Message = "InvoiceMarkAsPaid(), " + err.Message;
        //    }
        //    catch (Exception err)
        //    {
        //        mResult.Message = "InvoiceMarkAsPaid(), " + err.Message;
        //    }
        //    return mResult;
        //}

        //private static XeroCoreApi GetXeroApi()
        //{
        //    try
        //    {
        //        X509Certificate2 cert = new X509Certificate2(GlobalConfig.Xero_pfxFilePath, GlobalConfig.Xero_pfxPassword);
        //        var private_app_api = new XeroCoreApi("https://api.xero.com", new PrivateAuthenticator(cert),
        //            new Consumer(GlobalConfig.Xero_ConsumerKey, GlobalConfig.Xero_ConsumerSecret), null,
        //            new DefaultMapper(), new DefaultMapper());
        //        var org = private_app_api.Organisation;
        //        Console.WriteLine("Org Name: " + org.Name);
        //        var user = new ApiUser { Name = System.Environment.MachineName };
        //        return private_app_api;
        //    }
        //    catch (Exception ex)
        //    {
        //        return null;
        //    }
        //}

        //private static string getInvoicePdf(Guid InvoiceId, string InvoiceNumber)
        //{
        //    var private_app_api = GetXeroApi();
        //    BinaryFile pdfFile = private_app_api.PdfFiles.Get(PdfEndpointType.Invoices, InvoiceId);
        //    string PdfFileName = InvoiceNumber + ".pdf";
        //    string PdfFileSavePath = GlobalConfig.XeroInvoiceFilePath + PdfFileName;
        //    pdfFile.Save(PdfFileSavePath);
        //    return PdfFileSavePath;
        //}
    }
}